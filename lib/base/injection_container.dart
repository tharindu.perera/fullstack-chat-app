import 'package:get_it/get_it.dart';
import 'package:mbase/base/app/injection_container.dart' as authentication;

final sl = GetIt.instance;

init() async {
 await authentication.init();
}
