//import 'dart:async';
//
//import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:couchbase_lite/couchbase_lite.dart';
//import 'package:firebase_core/firebase_core.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
//import 'package:mbase/base/core/database_provider/database_provider.dart';
//import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
//import 'package:mbase/base/modules/listview/actions/comment/model/comment_model.dart';
//
//void main() async{
//  WidgetsFlutterBinding.ensureInitialized();
//  FirebaseApp firebaseApp = await Firebase.initializeApp();
//  FirebaseFirestore _firestore = DatabaseProvider.firestore;
//     CollectionReference collection = _firestore.collection("inspection_railcar");
//    List<InspectionRailcarModel> tempList = (await collection.where("id",isEqualTo: "77589080-d3e0-11eb-953c-236637746786")
//         .get())
//        .docs
//        .map((e) => InspectionRailcarModel.fromJson(e.data()))
//        .toList();
//    print(">>>>>>${tempList.length}");
//  collection.where("id",isEqualTo: "77589080-d3e0-11eb-953c-236637746786").get().then((value) {
//    print( "?????? ${value.docs[0].data()}");
//    value.docs[0].reference.collection("images").add({"aa":12444443});
//      value.docs[0].reference.collection("images").get().then((value) {
//    print(">>>>@@@@@@@>>${value.docs.length}");
//  });
//  });
//
//
////  runApp(ExampleApp());
//}
////void main() => runApp(ExampleApp());
//
//class ExampleApp extends StatefulWidget {
//  @override
//  _ExampleAppState createState() => _ExampleAppState();
//}
//
//class _ExampleAppState extends State<ExampleApp> {
//  String _displayString = 'Initializing';
//  Database database;
//  Replicator replicator;
//  ListenerToken _listenerToken;
//
//  @override
//  void initState() {
//    super.initState();
//    initPlatformState();
//  }
//
//  Future<String> runExample() async {
//    try {
//      database = await Database.initWithName("1233");
//      Comment comment = Comment();
//      comment.comment="dd";
//      comment.source="coment";
//      var mutableDoc = MutableDocument().setDouble("version", 2.0).setString("type1", "SDK");
//
//      await database.saveDocument(mutableDoc);
//
//      mutableDoc = (await database.document(mutableDoc.id))?.toMutable()?.setString("language", "Dart2");
//
//      if (mutableDoc != null) {
//        await database.saveDocument(mutableDoc);
//        var document = await database.document(mutableDoc.id);
//        print("Document ID :: ${document.id}");
//        print("Learning ${document.toMap()}");
//      }
//      // Create a query to fetch documents of type SDK.
//      var query = QueryBuilder.select([ SelectResult.all()]).from("1233")
//          .where(Expression.property("type1").equalTo(Expression.string("SDK")));
//
//      await query.execute().asStream().asBroadcastStream().listen((event) {      print(">>>>>Number of rows :: ${event.allResults().length}");;});
//
//      Future.delayed(Duration(milliseconds: 5000),() async{
//        var mutableDoc = MutableDocument().setDouble("version", 2.0).setString("type1", "SDK");
//        await database.saveDocument(mutableDoc);
//        print(">>>>>");
//      });
//      return "Database and Replicator Started";
//    } on PlatformException {
//      return "Error running the query";
//    }
//  }
//
//  // Platform messages are asynchronous, so we initialize in an async method.
//  Future<void> initPlatformState() async {
//    var result = await runExample();
//    if (!mounted) return;
//    setState(() {
//      _displayString = result;
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//      home: Scaffold(
//        appBar: AppBar(
//          title: const Text('Plugin example app'),
//        ),
//        body: Center(
//          child: Text(_displayString),
//        ),
//      ),
//    );
//  }
//
//  @override
//  void dispose() async {
//    await replicator?.removeChangeListener(_listenerToken);
//    await replicator?.stop();
//    await replicator?.dispose();
//    await database?.close();
//    super.dispose();
//  }
//}
