import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_data_table_souce.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_paginated_datatable.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/base/modules/switchlist/edit_switchlist/ui/edit_switchlists.dart';
import 'package:mbase/base/modules/switchlist/switchlist/bloc/switchlist_bloc.dart';
import 'package:mbase/base/modules/switchlist/switchlist/bloc/switchlist_event.dart';
import 'package:mbase/base/modules/switchlist/switchlist/bloc/switchlist_state.dart';
import 'package:mbase/base/modules/switchlist/switchlist/repository/switchlist_repository.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class SwitchListDetails extends StatefulWidget {
  NewSwitchList switchList;

  SwitchListDetails({Key key, this.switchList}) : super(key: key) {}

  @override
  SwitchListDetailsState createState() => SwitchListDetailsState(switchList);
}

class SwitchListDetailsState extends State<SwitchListDetails> {
  final DateTime now = DateTime.now();
  final DateFormat formatter = DateFormat('MM/dd/yyyy HH:mm');

  final debouncer = Debouncer(milliseconds: 500);
  String searchQuery = '';
  bool fetchComplete = false;
  int _sortColumnIndex = 0;
  bool _sortAscending = true;
  TextEditingController _searchController = TextEditingController();
  String percentage;
  List<YardModel> yardList = [];
  List<TrackModel> trackList = [];
  List<SpotModel> spotList = [];
  FToast fToast;
  NewSwitchList switchList;

  SwitchListDetailsState(this.switchList) {}

  @override
  void initState() {
    super.initState();
    yardList = MemoryData.inMemoryYardMap.values.toList();
    trackList = MemoryData.inMemoryTrackMap.values.toList();
    spotList = MemoryData.inMemorySpotMap.values.toList();
    fToast = FToast(context);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    return BlocProvider<SwitchListBloc>(
        create: (context) => SwitchListBloc()
          ..add(FetchSwitchListEquipments(switchListModel: switchList)),
        child: BlocBuilder<SwitchListBloc, SwitchListState>(
          builder: (context, state) {
            if (state is SwitchListEquipmentLoaded) {
              print("state.switchLists.length:${state.switchLists.length}");
              if (state.switchLists.length > 0) {
                switchList = state.switchLists.first;
              }
              int completedRailcarCount = 0;
              switchList.railcars?.forEach((element) {
                if (element.switch_list_equipment_status ==
                        APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED ||
                    element.switch_list_equipment_status ==
                        APP_CONST.SWITCH_LIST_EQUIPMENT_STATUS_OUTBOUNDED) {
                  completedRailcarCount++;
                }
                switchList.complete =
                    ((completedRailcarCount / switchList.railcars?.length) *
                            100)
                        .toString();
              });
            }
            return Scaffold(
              appBar: MainAppBar(),
              drawer: AppDrawer(),
              body: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: CardUI(
                        content: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                          padding: EdgeInsets.all(1),
                                          child: Container(
                                              child: ListTile(
                                            leading: GestureDetector(
                                                onTap: () {
                                                  Navigator.of(context).pop();
                                                },
                                                child: Icon(
                                                  Icons.arrow_back,
                                                )),

                                            title: RichText(
                                              text: TextSpan(
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline5,
                                                  children: <InlineSpan>[
                                                    WidgetSpan(
                                                        child: Container(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              2, 0, 2, 10),
                                                      child: Text(
                                                          switchList
                                                              .switch_list_name,
                                                          style: TextStyle(
                                                              fontSize:
                                                                  ScreenUtil()
                                                                      .setHeight(
                                                                          24),
                                                              fontFamily:
                                                                  'Roboto',
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal,
                                                              color: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText2
                                                                  .color)),
                                                    )),
                                                    WidgetSpan(
                                                      child: Padding(
                                                        padding:
                                                            EdgeInsets.fromLTRB(
                                                                10, 30, 0, 0),
                                                        child: CircleAvatar(
                                                          radius: 36.0,
                                                          backgroundColor:
                                                              Color(0xFF3e8aeb),
                                                          child: Text(
                                                            '${double.parse(switchList.complete ?? "0").toStringAsFixed(2)} %'
                                                                .toString(),
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]),
                                            ),
//                                            subtitle: Text('SwitchList 1',style:Theme.of(context).textTheme.headline ),
                                          ))),
                                    ],
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(1),
                                            child: Container(
                                                child: GestureDetector(
                                              onTap: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: RichText(
                                                text: TextSpan(
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText2,
                                                    children: <InlineSpan>[
                                                      TextSpan(
                                                          text: 'CANCEL',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Roboto',
                                                              fontSize:
                                                                  ScreenUtil()
                                                                      .setHeight(
                                                                          14),
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                              letterSpacing:
                                                                  ScreenUtil()
                                                                      .setWidth(
                                                                          1.25),
                                                              color: Color(
                                                                  0xFF3e8aeb)))
                                                    ]),
                                              ),
                                            ))),
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0,
                                                top: 14,
                                                right: 24,
                                                bottom: 14),
                                            child: Container(
                                              width: ScreenUtil().setWidth(180),
                                              height:
                                                  ScreenUtil().setHeight(48),
                                              child: FlatButton(
                                                  onPressed: switchList
                                                              .switch_list_status ==
                                                          APP_CONST
                                                              .SWITCHLIST_EQUIPMENT_STATUS_NEW
                                                      ? () async {
                                                          bool statusChanged =
                                                              await SwitchListRepository()
                                                                  .updateSwitchListStatusToInProgress(
                                                                      switchList);
                                                          if (statusChanged) {
                                                            _showToast(
                                                                toastColor: Color(
                                                                    0xff7fae1b),
                                                                toastMessage:
                                                                    "Switch List has started successfully");
                                                          } else {
                                                            _showToast(
                                                                toastColor: Color(
                                                                    0xfff34336),
                                                                toastMessage:
                                                                    "Something went wrong !");
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          }

                                                          setState(() {
                                                            statusChanged
                                                                ? switchList
                                                                        .switch_list_status =
                                                                    APP_CONST
                                                                        .SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS
                                                                : null;
                                                          });
                                                        }
                                                      : null,
                                                  child: Text(
                                                    'START',
                                                    style: TextStyle(
                                                        fontFamily: 'Roboto',
                                                        fontSize: ScreenUtil()
                                                            .setHeight(14),
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        letterSpacing:
                                                            ScreenUtil()
                                                                .setWidth(1.25),
                                                        color: Colors.white),
                                                  ),
                                                  color: Color(0xFF508be4),
                                                  textColor: Colors.white,
                                                  disabledColor: Colors.grey,
                                                  disabledTextColor:
                                                      Colors.black,
                                                  splashColor:
                                                      Color(0xFF3e8aeb)),
                                            )),
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(1),
                            ),
                            Divider(
                              thickness: 1.0,
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(1),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(14)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text('Status',
                                          style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize: ScreenUtil().setSp(16,
                                                  allowFontScalingSelf: true),
                                              letterSpacing:
                                                  ScreenUtil().setWidth(0.5),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.normal)),
                                      SizedBox(
                                        height: ScreenUtil().setHeight(1),
                                      ),
                                      Text(
                                        switchList.switch_list_status,
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(20,
                                                allowFontScalingSelf: true),
                                            letterSpacing:
                                                ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(14)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text('Crew',
                                          style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize: ScreenUtil().setSp(16,
                                                  allowFontScalingSelf: true),
                                              letterSpacing:
                                                  ScreenUtil().setWidth(0.5),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.normal)),
                                      SizedBox(
                                        height: 1,
                                      ),
                                      Text(
                                          switchList.switch_crew_name
                                                  ?.toString() ??
                                              "",
                                          style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize: ScreenUtil().setSp(20,
                                                  allowFontScalingSelf: true),
                                              letterSpacing:
                                                  ScreenUtil().setWidth(0.5),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500))
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(14)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text('Target Date',
                                          style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize: ScreenUtil().setSp(16,
                                                  allowFontScalingSelf: true),
                                              letterSpacing:
                                                  ScreenUtil().setWidth(0.5),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.normal)),
                                      SizedBox(
                                        height: 1,
                                      ),
                                      Text(
                                        switchList.target_complete_date == null
                                            ? "-"
                                            : formatter.format(DateTime.parse(
                                                switchList
                                                    .target_complete_date)),
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(20,
                                                allowFontScalingSelf: true),
                                            letterSpacing:
                                                ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  width: ScreenUtil().setWidth(200),
                                  padding: EdgeInsets.symmetric(vertical: 4.0),
                                  alignment: Alignment.centerLeft,
                                  child: TextField(
                                    controller: _searchController,
                                    onChanged: (string) {
                                      setState(() {
                                        searchQuery = _searchController.text;
                                      });
                                      debouncer.run(() {
                                        BlocProvider.of<SwitchListBloc>(context)
                                            .add(FetchSwitchListEquipments(
                                                switchListModel: switchList,
                                                searchQuery: searchQuery));
                                      });
                                    },
                                    style: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          .color,
                                    ),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(1.0),
                                      suffixIcon: Icon(
                                        Icons.search,
                                        color: _themeChanger
                                                    .getTheme()
                                                    .primaryColor ==
                                                Color(0xff182e42)
                                            ? Color(0xfff5f5f5)
                                            : _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xfff5f5f5)
                                                ? Color(0xff172636)
                                                : null,
                                      ),
                                      errorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(1)),
                                          borderSide: BorderSide(
                                              width: 1, color: Colors.red)),
                                      focusedErrorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(1)),
                                          borderSide: BorderSide(
                                              width: 1, color: Colors.red)),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(1.0),
                                        borderSide: BorderSide(
                                          width: 1,
                                          color: _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xff182e42)
                                              ? Color(0xfff5f5f5)
                                              : _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xfff5f5f5)
                                                  ? Color(0xff172636)
                                                  : null,
                                        ),
                                      ),
                                      enabledBorder: const OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.black38, width: 1),
                                      ),
                                      filled: true,
                                      fillColor: _themeChanger
                                                  .getTheme()
                                                  .primaryColor ==
                                              Color(0xff182e42)
                                          ? Color(0xff172636)
                                          : _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xfff5f5f5)
                                              ? Color(0xfff5f5f5)
                                              : null,
                                      hintText: 'Search by Equipment',
                                      hintStyle: TextStyle(
                                        fontFamily: 'Roboto',
                                        fontSize: ScreenUtil().setHeight(14),
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.normal,
                                        letterSpacing:
                                            ScreenUtil().setWidth(0.25),
                                        color: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            .color,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(5),
                            ),
                            Container(
                              width: ScreenUtil().setWidth(928),
                              child: CustomPaginatedDataTable(
                                sortArrowColor: Colors.white,
                                cardBackgroundColor:
                                    Theme.of(context).canvasColor,
                                footerTextColor: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                rowColor: _themeChanger
                                            .getTheme()
                                            .primaryColor ==
                                        Color(0xff182e42)
                                    ? Color(0xff1d2e40)
                                    : _themeChanger.getTheme().primaryColor ==
                                            Color(0xfff5f5f5)
                                        ? Color(0xffffffff)
                                        : null,

                                columnColor: Color(0xff274060),
                                checkboxBorderColor: _themeChanger
                                            .getTheme()
                                            .primaryColor ==
                                        Color(0xff182e42)
                                    ? Colors.white
                                    : _themeChanger.getTheme().primaryColor ==
                                            Color(0xfff5f5f5)
                                        ? Color.fromRGBO(0, 0, 0, 0.54)
                                        : null,
                                dataRowHeight: ScreenUtil().setHeight(60),
                                // headingRowHeight: 60,
                                horizontalMargin: ScreenUtil().setWidth(19),
                                columnSpacing: switchList?.railcars == null
                                    ? ScreenUtil().setWidth(140)
                                    : ScreenUtil().setWidth(60),
                                header: Text(''),
                                columns: [
                                  CustomDataColumn(
                                      label: Text("EQUIPMENT",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: 'Roboto',
                                            fontSize:
                                                ScreenUtil().setHeight(14),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing:
                                                ScreenUtil().setWidth(1.43),
                                          )),
                                      numeric: false,
                                      onSort: (columnIndex, ascending) {
                                        setState(() {
                                          _sortColumnIndex = columnIndex;
                                          _sortAscending = !_sortAscending;
                                        });
                                        onSortColumn(columnIndex, ascending);
                                      }),
                                  CustomDataColumn(
                                    label: Text("STATUS",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing:
                                              ScreenUtil().setWidth(1.43),
                                        )),
                                    numeric: false,
                                  ),
                                  CustomDataColumn(
                                    label: Text("FROM",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing:
                                              ScreenUtil().setWidth(1.43),
                                        )),
                                    numeric: false,
                                  ),
                                  CustomDataColumn(
                                    label: Text("TO",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing:
                                              ScreenUtil().setWidth(1.43),
                                        )),
                                    numeric: false,

//                                  }
                                  ),
                                  CustomDataColumn(
                                    label: Text(''),
                                  ),
                                ],
                                source: SwitchListEquipmentDataSource(
                                    context: context,
                                    switchListModel: switchList),
                                sortAscending: _sortAscending,
                                sortColumnIndex: _sortColumnIndex,
                                rowsPerPage: 5,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          },
        ));
  }

  _showToast({String toastMessage, Color toastColor}) {
    fToast.showToast(
      child: CustomToast(
        toastMessage: toastMessage,
        toastColor: toastColor,
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  onSortColumn(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        SwitchListEquipmentDataSource()
            .switchListModel
            .railcars
            .sort((a, b) => a.equipment_initial.compareTo(b.equipment_initial));
      } else {
        SwitchListEquipmentDataSource()
            .switchListModel
            .railcars
            .sort((a, b) => b.equipment_initial.compareTo(a.equipment_initial));
      }
    }
  }
}

class SwitchListEquipmentDataSource extends CustomDataTableSource {
  //final List<SwitchListEquipmentModel> switchListEquipmentData;
  final DateTime now = DateTime.now();
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final BuildContext context;
  final NewSwitchList switchListModel;
  int _selectedCount = 0;
  FToast fToast;

  SwitchListEquipmentDataSource({this.context, this.switchListModel}) {
    fToast = FToast(context);
  }

  @override
  CustomDataRow getRow(int index) {
    NewSwitchListEquipment cellVal = switchListModel.railcars[index];

    List<YardModel> yardList = MemoryData.inMemoryYardMap.values.toList();
    List<TrackModel> trackList = MemoryData.inMemoryTrackMap.values.toList();
    List<SpotModel> spotList = MemoryData.inMemorySpotMap.values.toList();

    YardModel fromYardModel = yardList.firstWhere((element) => cellVal.from_yard_id == element.id, orElse: () => null);
    String fromYardName = fromYardModel != null ? fromYardModel.yardName : "-";
    TrackModel fromTrackModel = trackList.firstWhere((element) => cellVal.from_track_id == element.id, orElse: () => null);
    String fromTrackName = fromTrackModel != null ? fromTrackModel.trackName : "-";

    return CustomDataRow.byIndex(index: index, cells: [
      CustomDataCell(Text(
        '${cellVal.equipment_initial + " " + cellVal.equipment_number}',
        style: TextStyle(
            fontFamily: 'Roboto',
            fontSize: ScreenUtil().setHeight(15),
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.normal,
            letterSpacing: ScreenUtil().setWidth(1.75),
            color: Theme.of(context).textTheme.bodyText1.color),
      )),

      CustomDataCell(Text(
        '${cellVal.switch_list_equipment_status ?? " - "}',
        style: TextStyle(
            fontFamily: 'Roboto',
            fontSize: ScreenUtil().setHeight(15),
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.normal,
            letterSpacing: ScreenUtil().setWidth(1.75),
            color: Theme.of(context).textTheme.bodyText1.color),
      )),

      CustomDataCell(
        ListTile(
          title: Text(
            '${fromYardName}',
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(15),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.75),
                color: Theme.of(context).textTheme.bodyText1.color),
          ),
          subtitle: Text(
            '${fromTrackName}' +
                " / " +
                "${cellVal.from_spot_id != null ? spotList.where((element) => cellVal.from_spot_id == element.id).first.spotName : " - "}",
          ),
        ),
      ),
      CustomDataCell(
        ListTile(
          title: Text(
            yardList
                        .where((element) => cellVal.to_yard_id == element.id)
                        .toList()
                        .isNotEmpty &&
                    yardList
                            .where(
                                (element) => cellVal.to_yard_id == element.id)
                            .first
                            .yardName !=
                        null
                ? '${yardList.where((element) => cellVal.to_yard_id == element?.id).first.yardName ?? " - "}'
                : "",
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(15),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.75),
                color: Theme.of(context).textTheme.bodyText1.color),
          ),
          subtitle: Text(
            '${cellVal.to_track_id != null ? trackList.where((element) => cellVal.to_track_id == element.id).first.trackName : " - "} ' +
                " / " +
                '${cellVal.to_spot_id != null ? spotList.where((element) => cellVal.to_spot_id == element.id).first.spotName : " - "}',
          ),
        ),
      ),

      CustomDataCell(ListTile(
          leading: FlatButton(
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: Theme.of(context).textTheme.bodyText1.color,
                      width: 1,
                      style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(1)),
              onPressed: switchListModel.switch_list_status == APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS && cellVal.switch_list_equipment_status == APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS ||
                      cellVal.switch_list_equipment_status ==
                          APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_NOT_STARTED ||
                      cellVal.switch_list_equipment_status == null
                  ? () async {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditSwitchList(
                                    newSwitchList: switchListModel,
                                    newSwitchListEquipment: cellVal,
                                  ))).then((value) {
                        notifyListeners();
                      });
                    }
                  : null,
              child: Text('Edit',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: ScreenUtil().setHeight(14),
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w500,
                      letterSpacing: ScreenUtil().setWidth(1.25),
                      color:
                          switchListModel.switch_list_status == APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS && cellVal.switch_list_equipment_status == APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS || cellVal.switch_list_equipment_status == APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_NOT_STARTED || cellVal.switch_list_equipment_status == null
                              ? Color(0xFF3e8aeb)
                              : Colors.white)),
              color: Colors.white,
              //textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              splashColor: Color(0xFF3e8aeb)),
          title: FlatButton(
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: Theme.of(context).textTheme.bodyText1.color,
                      width: 1,
                      style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(1)),
              onPressed: cellVal.to_yard_id != null &&
                          cellVal.to_track_id != null &&
                          switchListModel.switch_list_status ==
                              APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS &&
                          cellVal.switch_list_equipment_status == APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS ||
                      cellVal.switch_list_equipment_status == APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_NOT_STARTED ||
                      cellVal.switch_list_equipment_status == null
                  ? () {
//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) => CompleteSwitchListPage(
//                                    newSwitchListEquipment: cellVal,
//                                    newSwitchList: switchListModel,
//                                  ))).then((value) {
//                        notifyListeners();
//                      });
                      _complete(cellVal, switchListModel);
                    }
                  : null,
              child: Text('Complete',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: ScreenUtil().setHeight(14),
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                    letterSpacing: ScreenUtil().setWidth(1.25),
                    color: Colors.white,
                  )),
              color: Color(0xFF3e8aeb),
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              splashColor: Color(0xFF3e8aeb))))
//          DataCell(Text('${cellVal.switchListStatus}')),
//          DataCell(SideArrowButton(name: cellVal.load_status))
    ]);
  }

  _complete(NewSwitchListEquipment newSwitchListEquipment,
      NewSwitchList switchListModel) async {
    NotificationRepository notificationRepository = NotificationRepository();

    bool railcar_complete_date_validation = false;
    NewSwitchList switchList = switchListModel;
    switchList.action_type = APP_CONST.SW_RAILCAR_COMPLETE;
    switchList.parent_id = switchListModel?.parent_id ?? switchListModel.id;
    switchList.immediateParentId = switchListModel.id;
    switchList.operation_type = APP_CONST.SWITCHLIST_OPERATION;
    switchList.scy_pk = switchListModel.scy_pk ?? switchListModel.id;
    // switchList.scy_pk = switchListModel.id;
    switchList.transaction_status = APP_CONST.TRANSACTION_STATUS_PENDING;
    switchList.source = APP_CONST.MOBILITY;
    switchList.id = Uuid().v1();
    newSwitchListEquipment.railcar_action_type = APP_CONST.RAILCAR_COMPLETE;
//    newSwitchListEquipment.to_yard_id = newSwitchListEquipment.from_yard_id;
//        railcar.to_track_id = assingedSwitchlistModel.to_track_id;
//        railcar.to_spot_id = assingedSwitchlistModel.to_spot_id;
    newSwitchListEquipment.switch_list_equipment_status =
        APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED;
    newSwitchListEquipment.railcar_completed_date =
        DateTime.now().toIso8601String();

    bool allCOmpleted = true;
    switchListModel.railcars?.forEach((e) {
      if (e.switch_list_equipment_status !=
              APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED &&
          e.switch_list_equipment_status !=
              APP_CONST.SWITCH_LIST_EQUIPMENT_STATUS_OUTBOUNDED) {
        allCOmpleted = false;
      }
    });

    if (newSwitchListEquipment.to_yard_id == null ||
        newSwitchListEquipment.to_track_id == null) {
      _showToast(
          toastColor: Colors.red,
          toastMessage:
              "SwitchList Equipment does not have a to Track and To Yard info");
      return;
    }

    if (allCOmpleted) {
      switchList.switch_list_status =
          APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED;
      switchList.railcars.forEach((element) {
        if (element.equipment_initial ==
                newSwitchListEquipment.equipment_initial &&
            element.equipment_number ==
                newSwitchListEquipment.equipment_number &&
            DateTime.parse(element.placed_date.toString())
                .isBefore(DateTime.now())) {
          railcar_complete_date_validation = true;
        }
      });

      if (railcar_complete_date_validation) {
        // SwitchListRepository().completeSwitchListEquipment(newSwitchList: switchList).then((value) {
        //
        //   notificationRepository.addNotification(switchList, APP_CONST.SWITCHLIST_COMPLETE_RAILCAR);
        //
        //   return _showToast(toastMessage: "Railcar completed successfully.", toastColor: Color(0xff7fae1b));
        // });

        SwitchListRepository()
            .completeSwitchListEquipment(newSwitchList: switchList);
        notificationRepository.addNotification(
            switchList, APP_CONST.SWITCHLIST_COMPLETE_RAILCAR);
        return _showToast(
            toastMessage: "Record(s) added successfully.",
            toastColor: Color(0xff7fae1b));
      } else {
        _showToast(
            toastColor: Colors.red,
            toastMessage:
                "Please assign a future date for the railcar completion date");
      }
    } else {
      switchList.railcars.forEach((element) {
        if (element.equipment_initial ==
                newSwitchListEquipment.equipment_initial &&
            element.equipment_number ==
                newSwitchListEquipment.equipment_number &&
            DateTime.parse(element.placed_date.toString())
                .isBefore(DateTime.now())) {
          railcar_complete_date_validation = true;
        }
      });

      if (railcar_complete_date_validation) {
        // SwitchListRepository().completeSwitchListEquipment(newSwitchList: switchList).then((value) {
        //   notificationRepository.addNotification(switchList, APP_CONST.SWITCHLIST_COMPLETE_RAILCAR);
        //   return _showToast(toastMessage: "Record(s) added successfully.", toastColor: Color(0xff7fae1b));
        // });
        SwitchListRepository()
            .completeSwitchListEquipment(newSwitchList: switchList);
        notificationRepository.addNotification(
            switchList, APP_CONST.SWITCHLIST_COMPLETE_RAILCAR);
        return _showToast(
            toastMessage: "Record(s) added successfully.",
            toastColor: Color(0xff7fae1b));
      } else {
        _showToast(
            toastColor: Colors.red,
            toastMessage:
                "Please assign a future date for the railcar completion date");
      }
    }
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => (switchListModel?.railcars?.length ?? 0).ceil();

  @override
  int get selectedRowCount => _selectedCount;

  _showToast({String toastMessage, Color toastColor}) {
    fToast.showToast(
      child: CustomToast(
        toastColor: toastColor,
        toastMessage: toastMessage,
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
    return;
  }
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}
