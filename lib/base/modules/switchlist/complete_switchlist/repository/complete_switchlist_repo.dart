class CompleteSwitchListRepo {
  Future<List<String>> fetchYards() async {
    await Future.delayed(Duration(milliseconds: 200));
    return [
      'All Yards',
      'Yard A',
      'Yard B',
    ];
  }

  Future<List<String>> fetchModels({String brand}) async {
    await Future.delayed(Duration(milliseconds: 200));
    switch (brand) {
      case 'All Yards':
        return ['All Tracks', 'Track A', 'Track B'];
      case 'Yard A':
        return ['Track A'];
      case 'Yard B':
        return ['Track B'];
      default:
        return [];
    }
  }

  Future<List<String>> fetchSpots({String brand, String model}) async {
    await Future.delayed(Duration(milliseconds: 200));
    switch (brand) {
      case 'All Yards':
        switch (model) {
          case 'All Tracks':
            return ['SPOT 1', 'SPOT 2'];
          case 'Track A':
            return ['SPOT 3', 'SPOT 4'];
          case 'Track B':
            return ['SPOT 5', 'SPOT 6'];
          default:
            return [];
        }
        break;
      case 'Yard A':
        switch (model) {
          case 'All Tracks':
            return ['SPOT 7', 'SPOT 8'];
          case 'Track A':
            return ['SPOT 9', 'SPOT 10'];
          case 'Track B':
            return ['SPOT 11', 'SPOT 12'];
          default:
            return [];
        }
        break;
      case 'Yard B':
        switch (model) {
          case 'All Tracks':
            return ['SPOT 13', 'SPOT 14'];
          case 'Track A':
            return ['SPOT 15', 'SPOT 16'];
          case 'Track B':
            return ['SPOT 17', 'SPOT 19'];
          default:
            return [];
        }
        break;
      default:
        return [];
    }
  }
}
