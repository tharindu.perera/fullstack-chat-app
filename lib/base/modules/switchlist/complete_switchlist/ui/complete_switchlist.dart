import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/model/assigned_switchlist_model.dart';
import 'package:mbase/base/modules/switchlist/complete_switchlist/bloc/complete_switchlist_bloc.dart';
import 'package:mbase/base/modules/switchlist/complete_switchlist/ui/CompleteSwitchlistBloc.dart';
import 'package:mbase/base/modules/switchlist/switchlist/repository/switchlist_repository.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class CompleteSwitchListPage extends StatelessWidget {
  final NewSwitchList newSwitchList;
  final NewSwitchListEquipment newSwitchListEquipment;

  const CompleteSwitchListPage({Key key, this.newSwitchList, this.newSwitchListEquipment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar(),
      drawer: AppDrawer(),
      body: BlocProvider(
        create: (context) => CompleteSwitchListBloc(
          switchListRepository: SwitchListRepository(),
        ),
        child: CompleteSwitchList(
          newSwitchList: newSwitchList,
          newSwitchListEquipment: newSwitchListEquipment,
        ),
      ),
    );
  }
}

class CompleteSwitchList extends StatefulWidget {
  final NewSwitchList newSwitchList;
  final NewSwitchListEquipment newSwitchListEquipment;

  const CompleteSwitchList({Key key, this.newSwitchList, this.newSwitchListEquipment}) : super(key: key);

  @override
  _CompleteSwitchListState createState() => _CompleteSwitchListState();
}

class _CompleteSwitchListState extends State<CompleteSwitchList> {
  final _CompleteSwitchListBloc = CompleteSwitchListBloc();
  bool selectedYard = false;

  FToast fToast;

  final commentController = TextEditingController();
  final globalKey = GlobalKey<ScaffoldState>();
  String yardName = " ";

  String initialYardID;
  String initialTrackID;
  String initialSpotID;

  List<YardModel> yardList = [];

  List<TrackModel> trackList = [];
  List<TrackModel> filteredTracksList = [];

  List<SpotModel> spotList = [];
  List<SpotModel> filteredSpotsList = [];
  SwitchListEquipmentModel assingedSwitchlistModel = SwitchListEquipmentModel();

  final dateFormat = new DateFormat('yyyy-MM-dd hh:mm');

  var formBloc;
  var railcarCompletedDateValidation;

  SwitchListRepository switchListRepository = new SwitchListRepository();

  String railcarCompletedDate;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    setState(() {
      fToast = FToast(context);
    });

    yardList = MemoryData.inMemoryYardMap.values.toList();
    trackList = MemoryData.inMemoryTrackMap.values.toList();
    spotList = [];

    setState(() {
      filteredTracksList = trackList;
      filteredSpotsList = spotList;
      initialYardID = widget.newSwitchListEquipment.to_yard_id ?? null;
      initialTrackID = widget.newSwitchListEquipment.to_track_id ?? null;
      initialSpotID = widget.newSwitchListEquipment.to_spot_id ?? null;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _CompleteSwitchListBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);

    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    _showToast({String toastMessage, Color toastColor}) {
      fToast.showToast(
        child: CustomToast(
          toastColor: toastColor,
          toastMessage: toastMessage,
        ),
        gravity: ToastGravity.TOP,
        toastDuration: Duration(seconds: 2),
      );
    }

    onYardChange(String value) {
      assingedSwitchlistModel.to_track_id = null;
      assingedSwitchlistModel.to_spot_id = null;

      assingedSwitchlistModel.to_yard_id = yardList.singleWhere((element) => element.yardName == value).id;

      var matchedYardObject = yardList.where((element) => element.yardName == value).map((e) => e.id).toList();

      var filteredTracks = trackList.where((element) => element.yardId == matchedYardObject[0]).toList();
      filteredTracksList = [];

      setState(() {
        filteredTracksList = filteredTracks;
        initialYardID = assingedSwitchlistModel.to_yard_id;
        initialTrackID = assingedSwitchlistModel.to_track_id;
        initialSpotID = assingedSwitchlistModel.to_spot_id;
        spotList = [];
      });
    }

    onTrackChange(String value) {
      assingedSwitchlistModel.to_spot_id = null;
      assingedSwitchlistModel.to_track_id = trackList.singleWhere((element) => element.trackName == value).id;
      var matchedTrackObject = trackList.where((element) => element.trackName == value).map((e) => e.id).toList();

      var filteredSpots = spotList.where((element) => element.trackId == matchedTrackObject[0]).toList();

      setState(() {
        filteredSpotsList = filteredSpots;
        spotList = MemoryData.inMemorySpotMap.values.toList();
        initialYardID = assingedSwitchlistModel.to_yard_id;
        initialTrackID = assingedSwitchlistModel.to_track_id;
        initialSpotID = assingedSwitchlistModel.to_spot_id;
      });
    }

    onSpotChange(String value) {
      assingedSwitchlistModel.to_spot_id = spotList.singleWhere((element) => element.spotName == value).id;

      setState(() {
        initialYardID = assingedSwitchlistModel.to_yard_id;
        initialTrackID = assingedSwitchlistModel.to_track_id;
        initialSpotID = assingedSwitchlistModel.to_spot_id;
      });
    }

    _complete() async {
      int count = 0;
      bool railcar_complete_date_validation = false;
      InputFieldBloc<DateTime, Object> railcar_completed_date_time = formBloc.dateTime;
      NewSwitchList switchList = NewSwitchList.fromJson(widget.newSwitchList.toMap());
      EquipmentDAO eq_dao = EquipmentDAO();
      switchList.action_type = APP_CONST.SW_RAILCAR_COMPLETE;
      switchList.parent_id = widget.newSwitchList?.parent_id ?? widget.newSwitchList.id;
      switchList.immediateParentId = widget.newSwitchList.id;
      switchList.operation_type = APP_CONST.SWITCHLIST_OPERATION;
      switchList.scy_pk = widget.newSwitchList.id;
      switchList.transaction_status = APP_CONST.TRANSACTION_STATUS_PENDING;
      switchList.source = APP_CONST.MOBILITY;
      switchList.id = Uuid().v1();
      for (int i = 0; i < switchList.railcars.length; i++) {
        NewSwitchListEquipment railcar = switchList.railcars[i];
        if (railcar.equipment_initial == widget.newSwitchListEquipment.equipment_initial && railcar.equipment_number == widget.newSwitchListEquipment.equipment_number) {
          railcar.switch_list_comment = commentController.text;
          railcar.asset_master_id = (await eq_dao.getEquipmentByInitAndNumber(railcar.equipment_initial, railcar.equipment_number)).assetMasterId;
          railcar.scy_pk = railcar.id;
          railcar.railcar_action_type = APP_CONST.RAILCAR_COMPLETE;
          railcar.to_yard_id = assingedSwitchlistModel.to_yard_id;
          railcar.to_track_id = assingedSwitchlistModel.to_track_id;
          railcar.to_spot_id = assingedSwitchlistModel.to_spot_id;
          //  railcar.railcar_completed_date = DateTime.now().toString();
          railcar.switch_list_equipment_status = APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED;
          railcar.railcar_completed_date = railcar_completed_date_time.value.toIso8601String();
        }
        railcar.switch_list_equipment_status == APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED ? count++ : null;
      }

      widget.newSwitchList.railcars?.forEach((e) {
        e.switch_list_equipment_status == APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED ? count++ : null;
      });

      if (count == widget.newSwitchList.railcars?.length) {
        switchList.switch_list_status = APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED;
        switchList.railcars.forEach((element) {
          if (element.equipment_initial == widget.newSwitchListEquipment.equipment_initial &&
              element.equipment_number == widget.newSwitchListEquipment.equipment_number &&
              DateTime.parse(element.placed_date.toString()).isBefore(DateTime.parse(railcar_completed_date_time.value.toIso8601String()))) {
            railcar_complete_date_validation = true;
          }
        });

        if (railcar_complete_date_validation) {
          await switchListRepository.completeSwitchListEquipment(newSwitchList: switchList).then((value) => _showToast(toastMessage: "Railcar completed successfully.", toastColor: Color(0xff7fae1b)));
          Navigator.of(context).pop();
          Navigator.of(context).pop();
        }

        _showToast(toastColor: Colors.red, toastMessage: "Please assign a future date for the railcar completion date");
      } else {
        switchList.railcars.forEach((element) {
          if (element.equipment_initial == widget.newSwitchListEquipment.equipment_initial &&
              element.equipment_number == widget.newSwitchListEquipment.equipment_number &&
              DateTime.parse(element.placed_date.toString()).isBefore(DateTime.parse(railcar_completed_date_time.value.toIso8601String()))) {
            railcar_complete_date_validation = true;
          }
        });

        if (railcar_complete_date_validation) {
          await switchListRepository.completeSwitchListEquipment(newSwitchList: switchList).then((value) => _showToast(toastMessage: "Record(s) added successfully.", toastColor: Color(0xff7fae1b)));
          Navigator.of(context).pop();
          Navigator.of(context).pop();
        }

        _showToast(toastColor: Colors.red, toastMessage: "Please assign a future date for the railcar completion date");
      }
    }

    _validateSelection() {
      InputFieldBloc<DateTime, Object> completed_date_time = formBloc.dateTime;

      if (assingedSwitchlistModel.to_yard_id != null && assingedSwitchlistModel.to_track_id != null && completed_date_time.value != null) {
        _complete();
      } else {
        _showToast(toastMessage: "Please Select A Yard, Track & DateTime To Complete ", toastColor: Colors.red);
      }
    }

    return BlocProvider(
      create: (context) => CompleteSwitchlistBloc(),
      child: Builder(
        builder: (context) {
          formBloc = context.bloc<CompleteSwitchlistBloc>();
          return FormBlocListener<CompleteSwitchlistBloc, String, String>(
            child: Scaffold(
              key: globalKey,
              body: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: CardUI(
                        content: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                          padding: EdgeInsets.all(20),
                                          child: Container(
                                              child: ListTile(
                                            leading: GestureDetector(
                                                onTap: () {
                                                  Navigator.of(context).pop();
                                                },
                                                child: Icon(
                                                  Icons.arrow_back,
                                                )),
                                            title: Text('Complete Switchlist', style: Theme.of(context).textTheme.bodyText2),
                                            subtitle: Text(widget.newSwitchListEquipment.equipment_initial + " " + widget.newSwitchListEquipment.equipment_number,
                                                style: Theme.of(context).textTheme.headline5),
                                          ))),
                                    ],
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(20),
                                            child: Container(
                                                child: GestureDetector(
                                              onTap: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: RichText(
                                                text: TextSpan(style: Theme.of(context).textTheme.bodyText1, children: <InlineSpan>[
                                                  TextSpan(
                                                      text: 'CANCEL',
                                                      style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: ScreenUtil().setHeight(14),
                                                          fontStyle: FontStyle.normal,
                                                          fontWeight: FontWeight.w500,
                                                          letterSpacing: ScreenUtil().setWidth(1.25),
                                                          color: Color(0xFF3e8aeb)))
                                                ]),
                                              ),
                                            ))),
                                        Padding(
                                            padding: EdgeInsets.only(left: 0, top: 14, right: 24, bottom: 14),
                                            child: Container(
                                              width: ScreenUtil().setWidth(180),
                                              height: ScreenUtil().setHeight(48),
                                              child: FlatButton(
                                                  onPressed: _validateSelection,
                                                  child: Text(
                                                    'COMPLETE',
                                                    style: TextStyle(
                                                        fontFamily: 'Roboto',
                                                        fontSize: ScreenUtil().setHeight(14),
                                                        fontStyle: FontStyle.normal,
                                                        fontWeight: FontWeight.w500,
                                                        letterSpacing: ScreenUtil().setWidth(1.25),
                                                        color: Colors.white),
                                                  ),
                                                  color: Color(0xFF508be4),
                                                  textColor: Colors.white,
                                                  disabledColor: Color(0xFF3e8aeb),
                                                  disabledTextColor: Colors.black,
                                                  splashColor: Color(0xFF3e8aeb)),
                                            )),
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(24),
                            ),
                            Divider(
                              thickness: 5.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    CustomDropdown(
                                        hintText: "Yard",
                                        itemList: yardList.isNotEmpty == true ? yardList.map((e) => e.yardName).toList() : const [],
                                        SelectedValue: initialYardID == null ? null : MemoryData.inMemoryYardMap[initialYardID].yardName,
                                        Ontap: (str) => onYardChange(str)),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    CustomDropdown(
                                        hintText: "Track",
                                        itemList: filteredTracksList.isNotEmpty == true ? filteredTracksList.map((e) => e.trackName).toList() : const [],
                                        SelectedValue: initialTrackID == null ? null : MemoryData.inMemoryTrackMap[initialTrackID].trackName,
                                        Ontap: (str) => onTrackChange(str)),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    filteredSpotsList.isNotEmpty
                                        ? CustomDropdown(
                                            hintText: "Spot",
                                            itemList: filteredSpotsList.isNotEmpty == true ? filteredSpotsList.map((e) => e.spotName).toList() : const [],
                                            SelectedValue: initialSpotID == null ? null : MemoryData.inMemorySpotMap[initialSpotID].spotName,
                                            Ontap: (str) => onSpotChange(str))
                                        : Container(
                                            width: ScreenUtil().setWidth(213),
                                            height: ScreenUtil().setHeight(48),
                                          ),
                                  ],
                                ),
                                Container(
                                  width: ScreenUtil().setWidth(214),
                                  child: DateTimeFieldBlocBuilder(
                                    style: TextStyle(
                                      color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                          ? Color(0xfff5f5f5)
                                          : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                                    ),
                                    format: dateFormat,
                                    dateTimeFieldBloc: formBloc.dateTime,
                                    canSelectTime: true,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(1900),
                                    lastDate: DateTime(2100),
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(
                                        Icons.timer,
                                        color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                            ? Color(0xfff5f5f5)
                                            : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                                      ),
                                      contentPadding: EdgeInsets.symmetric(vertical: 15),
                                      errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                                      focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(1.0),
                                        borderSide: BorderSide(width: 1, color: Colors.black38),
                                      ),
                                      enabledBorder: const OutlineInputBorder(
                                        borderSide: const BorderSide(color: Colors.black38, width: 1),
                                      ),
                                      filled: true,
                                      fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                          ? Color(0xff172636)
                                          : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                      hintText: 'Select Date Time',
                                      hintStyle: TextStyle(
                                        fontSize: ScreenUtil().setHeight(14),
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.normal,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: ScreenUtil().setWidth(0.25),
                                        color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                            ? Color(0xfff5f5f5)
                                            : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
