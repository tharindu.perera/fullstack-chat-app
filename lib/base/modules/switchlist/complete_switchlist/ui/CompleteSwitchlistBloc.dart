import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class CompleteSwitchlistBloc extends FormBloc<String, String> {


  final dateTime = InputFieldBloc<DateTime, Object>(name: 'inboundDateTime', toJson: (value) => value.toUtc(), validators: [FieldBlocValidators.required]);

  CompleteSwitchlistBloc() {
    addFieldBlocs(
      fieldBlocs: [
        dateTime,
      ],
    );
  }

  @override
  void onSubmitting() async {
    try {
      await Future<void>.delayed(Duration(milliseconds: 500));

      emitSuccess(canSubmitAgain: true);
    } catch (e) {
      emitFailure();
    }
  }
}