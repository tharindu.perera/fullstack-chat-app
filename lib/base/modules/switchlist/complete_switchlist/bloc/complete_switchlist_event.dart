part of 'complete_switchlist_bloc.dart';

@immutable
abstract class CompleteSwitchListEvent {
  const CompleteSwitchListEvent();
}

class CompleteSwitchListLoaded extends CompleteSwitchListEvent {}

class CompleteSwitchListYardChanged extends CompleteSwitchListEvent {
  final String yard;

  const CompleteSwitchListYardChanged({this.yard});
}

class CompleteSwitchListTrackChanged extends CompleteSwitchListEvent {
  final String track;

  const CompleteSwitchListTrackChanged({this.track});
}

class CompleteSwitchListSpotChanged extends CompleteSwitchListEvent {
  final String spot;

  const CompleteSwitchListSpotChanged({this.spot});
}
