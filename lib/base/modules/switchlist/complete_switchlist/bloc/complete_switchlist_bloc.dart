import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
//import 'package:mbase/base/modules/switchlist/switchlist/model/spot_model.dart';
import 'package:mbase/base/modules/switchlist/switchlist/repository/switchlist_repository.dart';
import 'package:meta/meta.dart';

part 'complete_switchlist_event.dart';
part 'complete_switchlist_state.dart';

class CompleteSwitchListBloc
    extends Bloc<CompleteSwitchListEvent, CompleteSwitchListState> {
  final SwitchListRepository switchListRepository;

  CompleteSwitchListBloc({SwitchListRepository switchListRepository})
      : switchListRepository = switchListRepository, super(CompleteSwitchListState.initial());

  // @override
  // CompleteSwitchListState get initialState => CompleteSwitchListState.initial();

  @override
  Stream<CompleteSwitchListState> mapEventToState(
    CompleteSwitchListEvent event,
  ) async* {
    if (event is CompleteSwitchListLoaded) {
      yield* _mapCompleteSwitchListLoadedToState();
    } else if (event is CompleteSwitchListYardChanged) {
      yield* _mapCompleteSwitchListYardChangedToState(event);
    } else if (event is CompleteSwitchListTrackChanged) {
      yield* _mapCompleteSwitchListTracksChangedToState(event);
    } else if (event is CompleteSwitchListSpotChanged) {
      yield* _mapCompleteSwitchListSpotChangedToState(event);
    }
  }

  Stream<CompleteSwitchListState> _mapCompleteSwitchListLoadedToState() async* {
    yield CompleteSwitchListState.yardsLoadInProgress();
    final yards = await switchListRepository.fetchYards();
    yield CompleteSwitchListState.yardsLoadSuccess(yards: yards);
  }

  Stream<CompleteSwitchListState> _mapCompleteSwitchListYardChangedToState(
      CompleteSwitchListYardChanged event) async* {
    final currentState = state;
    yield CompleteSwitchListState.tracksLoadInProgress(
      yards: currentState.yards,
      yard: event.yard,
    );
    final tracks = await switchListRepository.fetchTracks(event.yard);
    yield CompleteSwitchListState.tracksLoadSuccess(
      yards: currentState.yards,
      yard: event.yard,
      tracks: tracks,
    );
  }

  Stream<CompleteSwitchListState> _mapCompleteSwitchListTracksChangedToState(
      CompleteSwitchListTrackChanged event) async* {
    final currentState = state;
    yield CompleteSwitchListState.spotsLoadInProgress(
      yards: currentState.yards,
      yard: currentState.yard,
      tracks: currentState.tracks,
      track: event.track,
    );
    final spots = await switchListRepository.fetchSpots(
      yard: currentState.yard,
      track: event.track,
    );
    yield CompleteSwitchListState.spotsLoadSuccess(
      yards: currentState.yards,
      yard: currentState.yard,
      tracks: currentState.tracks,
      track: event.track,
     spots: []
     // spots: spots,
    );
  }

  Stream<CompleteSwitchListState> _mapCompleteSwitchListSpotChangedToState(
      CompleteSwitchListSpotChanged event) async* {
    yield state.copyWith(spot: event.spot);
  }
}
