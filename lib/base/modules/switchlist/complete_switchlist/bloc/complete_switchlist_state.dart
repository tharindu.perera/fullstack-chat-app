part of 'complete_switchlist_bloc.dart';

class CompleteSwitchListState {
  final List<YardModel> yards;
  final String yard;

  final List<TrackModel> tracks;
  final String track;

  final List<SpotModel> spots;
  final String spot;

  const CompleteSwitchListState({
    @required this.yards,
    @required this.yard,
    @required this.tracks,
    @required this.track,
    @required this.spots,
    @required this.spot,
  });

  bool get isComplete => yard != null && track != null && spot != null;

  factory CompleteSwitchListState.initial() => CompleteSwitchListState(
        yards: <YardModel>[],
        yard: null,
        tracks: <TrackModel>[],
        track: null,
        spots: <SpotModel>[],
        spot: null,
      );

  factory CompleteSwitchListState.yardsLoadInProgress() =>
      CompleteSwitchListState(
        yards: <YardModel>[],
        yard: null,
        tracks: <TrackModel>[],
        track: null,
        spots: <SpotModel>[],
        spot: null,
      );

  factory CompleteSwitchListState.yardsLoadSuccess({
    @required List<YardModel> yards,
  }) =>
      CompleteSwitchListState(
        yards: yards,
        yard: null,
        tracks: <TrackModel>[],
        track: null,
        spots: <SpotModel>[],
        spot: null,
      );

  factory CompleteSwitchListState.tracksLoadInProgress({
    @required List<YardModel> yards,
    @required String yard,
  }) =>
      CompleteSwitchListState(
        yards: yards,
        yard: yard,
        tracks: <TrackModel>[],
        track: null,
        spots: <SpotModel>[],
        spot: null,
      );

  factory CompleteSwitchListState.tracksLoadSuccess({
    @required List<YardModel> yards,
    @required String yard,
    @required List<TrackModel> tracks,
  }) =>
      CompleteSwitchListState(
        yards: yards,
        yard: yard,
        tracks: tracks,
        track: null,
        spots: <SpotModel>[],
        spot: null,
      );

  factory CompleteSwitchListState.spotsLoadInProgress({
    @required List<YardModel> yards,
    @required String yard,
    @required List<TrackModel> tracks,
    @required String track,
  }) =>
      CompleteSwitchListState(
        yards: yards,
        yard: yard,
        tracks: tracks,
        track: track,
        spots: <SpotModel>[],
        spot: null,
      );

  factory CompleteSwitchListState.spotsLoadSuccess({
    @required List<YardModel> yards,
    @required String yard,
    @required List<TrackModel> tracks,
    @required String track,
    @required List<SpotModel> spots,
  }) =>
      CompleteSwitchListState(
        yards: yards,
        yard: yard,
        tracks: tracks,
        track: track,
        spots: spots,
        spot: null,
      );

  CompleteSwitchListState copyWith({
    List<YardModel> yards,
    String yard,
    List<TrackModel> tracks,
    String track,
    List<SpotModel> spots,
    String spot,
  }) {
    return CompleteSwitchListState(
      yards: yards ?? this.yards,
      yard: yard ?? this.yard,
      tracks: tracks ?? this.tracks,
      track: track ?? this.track,
      spots: spots ?? this.spots,
      spot: spot ?? this.spot,
    );
  }
}
