import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/common_base/repositories/yard_repository.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_data_table_souce.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_paginated_datatable.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/switchlist/switchlist/bloc/switchlist_bloc.dart';
import 'package:mbase/base/modules/switchlist/switchlist/bloc/switchlist_event.dart';
import 'package:mbase/base/modules/switchlist/switchlist/bloc/switchlist_state.dart';
import 'package:mbase/base/modules/switchlist/switchlist_detail/ui/switchlist_details.dart';
import 'package:provider/provider.dart';

class SwitchListsBlocProvider extends StatelessWidget {
  const SwitchListsBlocProvider({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var switchLisbloc = SwitchListBloc()..add(FetchSwitchListDataEvent(tabIndex: 0));
    return BlocProvider(create: (context) => switchLisbloc, child: SwitchLists());
  }
}

class SwitchLists extends StatefulWidget {
  @override
  SwitchListsState createState() => SwitchListsState();
}

class SwitchListsState extends State<SwitchLists> {
  GlobalKey<CustomTabBarState> customTabBarGlobalKey = new GlobalKey<CustomTabBarState>();

  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  final _debouncer = Debouncer(milliseconds: 001);
  String searchQuery = '';
  TextEditingController _searchController = TextEditingController();
  bool fetchComplete = false;
  int _sortColumnIndex = 0;
  bool _sortAscending = true;
  YardRepository yardRepository = new YardRepository();
  List<YardModel> yardModels;
  LinkedHashMap<String, String> yardMap = LinkedHashMap();
  bool yardsLoaded;
  SwitchListBloc switchlistbloc;
  SwitchListDataSource source;

  List<NewSwitchList> allSwitchListData = [];
  List<NewSwitchList> newSwitchListData = [];
  List<NewSwitchList> inprogressSwitchListData = [];
  List<NewSwitchList> completedSwitchListData = [];

  List<String> names = ["New", "In Progress", "Completed", "All"];

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 500), () {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    ScreenUtil.init(context, width: 1024, height: 768, allowFontScaling: true);
    List<Widget> tabbody = [
      Container(
        child: Center(child: getPaginatedTable("New", context)),
      ),
      Container(
        child: Center(child: getPaginatedTable("In Progress", context)),
      ),
      Container(
        child: Center(child: getPaginatedTable("Completed", context)),
      ),
      Container(
        child: Center(child: getPaginatedTable("All", context)),
      )
    ];

    customTabBarGlobalKey.currentState != null  ? customTabBarGlobalKey.currentState.controller.addListener(() {
            if (customTabBarGlobalKey.currentState.controller.indexIsChanging) {
              print("index is changing------------------------------${customTabBarGlobalKey.currentState.controller.index}");
              fetchSwitchListData(tabIndex: customTabBarGlobalKey.currentState.controller.index, searchQuery: searchQuery);
            }
          })
        : print("customTabBarGlobalKey.currentState null");

    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24), vertical: ScreenUtil().setHeight(24)),
              child: CardUI(
                content: Container(
                  height: ScreenUtil().setHeight(600),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(20),
                                  child: ConstrainedBox(
                                      constraints: BoxConstraints(minHeight: ScreenUtil().setHeight(28), minWidth: ScreenUtil().setWidth(104)),
                                      child: Text(
                                        'Switch List',
                                        style: TextStyle(fontSize: ScreenUtil().setHeight(24), color: Theme.of(context).textTheme.bodyText1.color),
                                      ))),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    height: ScreenUtil().setHeight(48),
                                    width: ScreenUtil().setWidth(452),
                                    padding: EdgeInsets.symmetric(
                                      vertical: 4.0,
                                    ),
                                    margin: EdgeInsets.symmetric(
                                      horizontal: 5.0,
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                    alignment: Alignment.center,
                                    child: TextField(
                                      style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Theme.of(context).textTheme.bodyText1.color,
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(1.0),
                                        suffixIcon: Icon(
                                          Icons.search,
                                          color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                              ? Color(0xfff5f5f5)
                                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                                        ),
                                        errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                                        focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(1.0),
                                          borderSide: BorderSide(
                                            width: 1,
                                            color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                ? Color(0xfff5f5f5)
                                                : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                                          ),
                                        ),
                                        enabledBorder: const OutlineInputBorder(
                                          borderSide: const BorderSide(color: Colors.black38, width: 1),
                                        ),
                                        filled: true,
                                        fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                            ? Color(0xff172636)
                                            : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                        hintText: 'Search by name, crew, type..',
                                        hintStyle: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.italic,
                                          fontWeight: FontWeight.normal,
                                          letterSpacing: ScreenUtil().setWidth(0.25),
                                          color: Theme.of(context).textTheme.bodyText1.color,
                                        ),
                                      ),
                                      controller: _searchController,
                                      onChanged: (string) {
                                        setState(() {
                                          searchQuery = _searchController.text;
                                        });

                                        allSwitchListData = [];
                                        newSwitchListData = [];
                                        inprogressSwitchListData = [];
                                        completedSwitchListData = [];
                                        fetchSwitchListData(tabIndex: customTabBarGlobalKey.currentState.controller.index, searchQuery: searchQuery);
                                      },
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                      Divider(
                        thickness: 5.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Expanded(
                        child: CustomTabBarNav(key: customTabBarGlobalKey, tabitem: names, tabbody: tabbody),
//                              child: tabBar,
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  fetchSwitchListData({int tabIndex, String searchQuery}) async {
    switch (tabIndex) {
      case 0:
        BlocProvider.of<SwitchListBloc>(context).add(FetchSwitchListDataEvent(tabIndex: 0, searchQuery: searchQuery));
        setState(() {
          allSwitchListData = [];
          // newSwitchListData = newSwitchListModels;
          inprogressSwitchListData = [];
          completedSwitchListData = [];
          fetchComplete = true;
        });

        break;
      case 1:
        BlocProvider.of<SwitchListBloc>(context).add(FetchSwitchListDataEvent(tabIndex: 1, searchQuery: searchQuery));
        setState(() {
          allSwitchListData = [];
          newSwitchListData = [];
          //inprogressSwitchListData = inprogressSwitchListModels;
          completedSwitchListData = [];
          fetchComplete = true;
        });
        break;
      case 2:
        BlocProvider.of<SwitchListBloc>(context).add(FetchSwitchListDataEvent(tabIndex: 2, searchQuery: searchQuery));

        //List<SwitchListModel> completedSwitchListModels =
        //   await switchListRepository.fetchSwitchLists(APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED, _sortColumnIndex, _sortAscending, selectedYard, searchQuery);

        setState(() {
          allSwitchListData = [];
          newSwitchListData = [];
          inprogressSwitchListData = [];
          //completedSwitchListData = completedSwitchListModels;
          fetchComplete = true;
        });

        break;
      case 3:
        BlocProvider.of<SwitchListBloc>(context).add(FetchSwitchListDataEvent(tabIndex: 3, searchQuery: searchQuery));

        //List<SwitchListModel> allSwitchListModels = await switchListRepository.fetchSwitchLists("All", _sortColumnIndex, _sortAscending, selectedYard, searchQuery);

        setState(() {
          //allSwitchListData = allSwitchListModels;
          newSwitchListData = [];
          inprogressSwitchListData = [];
          completedSwitchListData = [];
          fetchComplete = true;
        });

        break;
      default:
        break;
    }
  }

  SingleChildScrollView getPaginatedTable(String status, BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    switch (status) {
      case 'All':
        setState(() {
          source = new SwitchListDataSource(switchListData: allSwitchListData, context: context);
        });
        break;
      case 'New':
        setState(() {
          source = new SwitchListDataSource(switchListData: newSwitchListData, context: context);
        });
        break;
      case 'In Progress':
        setState(() {
          source = new SwitchListDataSource(switchListData: inprogressSwitchListData, context: context);
        });
        break;
      case 'Completed':
        setState(() {
          source = new SwitchListDataSource(switchListData: completedSwitchListData, context: context);
        });
        break;
      default:
        break;
    }

    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: BlocBuilder<SwitchListBloc, SwitchListState>(
            // ignore: missing_return
            builder: (context, state) {
          print(">>>>>$state");
          if (state is SwitchListDataLoaded) {

            return CustomPaginatedDataTable(
              sortArrowColor: Colors.white,
              cardBackgroundColor: Theme.of(context).canvasColor,
              footerTextColor: Theme.of(context)
                  .textTheme
                  // ignore: deprecated_member_use
                  .body1
                  .color,
              rowColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff1d2e40) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xffffffff) : null,

              columnColor: Color(0xff274060),
              checkboxBorderColor:
                  _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color.fromRGBO(0, 0, 0, 0.54) : null,
              dataRowHeight: ScreenUtil().setHeight(47),
              // headingRowHeight: 60,
              horizontalMargin: ScreenUtil().setWidth(19),
              columnSpacing: ScreenUtil().setWidth(30),
              header: Text(''),
              columns: [
                CustomDataColumn(
                    label: Text("SWITCH LIST",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      if (ascending) {
                        state.switchList.sort((a,b)=> a.switch_list_name.compareTo(b.switch_list_name));
                      } else {
                        state.switchList.sort((a,b)=> b.switch_list_name.compareTo(a.switch_list_name));
                      }


                    }),
                CustomDataColumn(
                    label: Text("CARS",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      if (ascending) {
                        state.switchList.sort((a,b)=> a.Cars.compareTo(b.Cars));
                      } else {
                        state.switchList.sort((a, b) =>
                            b.Cars.compareTo(a.Cars));
                      }
                    }),
                CustomDataColumn(
                    label: Text("%",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      if (ascending) {
                        state.switchList.sort((a,b)=> a.complete.compareTo(b.complete));
                      } else {
                        state.switchList.sort((a,b)=> b.complete.compareTo(a.complete));
                      }
                    }),
                CustomDataColumn(
                    label: Text("CREW",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      if (ascending) {
                        state.switchList.sort((a,b)=> a.switch_crew_name.compareTo(b.switch_crew_name));
                      } else {
                        state.switchList.sort((a,b)=> b.switch_crew_name.compareTo(a.switch_crew_name));
                      }                    }),
                CustomDataColumn(
                    label: Text("START DATE",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      if (ascending) {
                        state.switchList.sort((a,b)=> a.target_start_date.compareTo(b.target_start_date));
                      } else {
                        state.switchList.sort((a,b)=> b.target_start_date.compareTo(a.target_start_date));
                      }                    }),
                CustomDataColumn(
                    label: Text("TARGET DATE",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      if (ascending) {
                        state.switchList.sort((a,b)=> a.target_complete_date.compareTo(b.target_complete_date));
                      } else {
                        state.switchList.sort((a,b)=> b.target_complete_date.compareTo(a.target_complete_date));
                      }
                    }),
                CustomDataColumn(
                    label: Text("STATUS",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      if (ascending) {
                        state.switchList.sort((a,b)=> a.switch_list_status.compareTo(b.switch_list_status));
                      } else {
                        state.switchList.sort((a,b)=> b.switch_list_status.compareTo(a.switch_list_status));
                      }
                    }),
              ],
              source: SwitchListDataSource(context: context, switchListData: state.switchList),
              sortAscending: _sortAscending,
              sortColumnIndex: _sortColumnIndex,
              rowsPerPage: 5,
            );
          } else {
            return CircularProgressIndicator();
          }
        }));
  }
}

class SwitchListDataSource extends CustomDataTableSource {
  final DateTime now = DateTime.now();
  final DateFormat formatter = DateFormat('MM/dd/yyyy HH:mm');

  final List<NewSwitchList> switchListData;
  final GlobalKey<SwitchListDetailsState> paginatedDataTableGlobalKey = new GlobalKey<SwitchListDetailsState>();
  int _selectedCount = 0;
  BuildContext context;

  SwitchListDataSource({this.switchListData, this.context});

  @override
  CustomDataRow getRow(int index) {
    NewSwitchList cellVal = switchListData[index];
    return CustomDataRow.byIndex(index: index, cells: [
      CustomDataCell(Text(
        '${cellVal.switch_list_name}',
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: ScreenUtil().setHeight(15),
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          letterSpacing: ScreenUtil().setWidth(1.75),
        ),
      )),
      CustomDataCell(Text(
        '${cellVal.Cars}',
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: ScreenUtil().setHeight(15),
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          letterSpacing: ScreenUtil().setWidth(1.75),
        ),
      )),
      CustomDataCell(Text(
        cellVal.complete.toString() == "null" ? "0" : '${double.parse(cellVal.complete).toStringAsFixed(2)} %',
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: ScreenUtil().setHeight(15),
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          letterSpacing: ScreenUtil().setWidth(1.75),
        ),
      )),
      CustomDataCell(Container(
        width: 100,
        child: Text(
          '${cellVal.switch_crew_name ?? ""}',
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontFamily: 'Roboto',
            fontSize: ScreenUtil().setHeight(15),
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.normal,
            letterSpacing: ScreenUtil().setWidth(1.75),
          ),
        ),
      )),
      CustomDataCell(Text(
        // cellVal.target_start_date??"",
        cellVal.target_start_date == null ? " - " : formatter.format(DateTime.parse(cellVal.target_start_date)),

        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: ScreenUtil().setHeight(15),
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          letterSpacing: ScreenUtil().setWidth(1.75),
        ),
      )),
      CustomDataCell(Text(
        cellVal.target_complete_date == null ? "-" : formatter.format(DateTime.parse(cellVal.target_complete_date)),
        //DateTime.parse(cellVal?.target_complete_date??"").toUtc().toString(),
        // cellVal.target_start_date.toString(),
        // formatter.format(cellVal.target_start_date) ?? "-",
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: ScreenUtil().setHeight(15),
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          letterSpacing: ScreenUtil().setWidth(1.75),
        ),
      )),
      CustomDataCell(Row(
        children: [
          Container(
            child: Icon(
              Icons.lens,
              color: Color(0xff57657a),
              size: 10,
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(8),
          ),
          Container(
            child: Text(
              cellVal.switch_list_status ?? '-',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(15),
                // ignore: deprecated_member_use
                //color: Theme.of(context).textTheme.body1.color,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.75),
                //color:Theme.of(context).textTheme.body1.color
              ),
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(40),
          ),
          Container(
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SwitchListDetails(
                              key: paginatedDataTableGlobalKey,
                              switchList: cellVal,
                            )));
              },
              child: Icon(
                Icons.keyboard_arrow_right,
                //   color:Theme.of(context).textTheme.body1.color,
              ),
            ),
          )
        ],
      )
          // SwitchlistNavigatorButton(
          // status: cellVal.Status,
          // switchListModel: cellVal)
          )
    ]);
  }

  @override
  // TODO: implement isRowCountApproximate
  bool get isRowCountApproximate => false;

  @override
  // TODO: implement rowCount
  int get rowCount => (switchListData.length ?? 0).ceil();

  @override
  // TODO: implement selectedRowCount
  int get selectedRowCount => _selectedCount;
}

class CustomTabBarNav extends StatefulWidget {
  List<String> tabitem;
  List<Widget> tabbody;

  CustomTabBarNav({Key key, this.tabitem, this.tabbody}) : super(key: key);

  @override
  CustomTabBarState createState() => CustomTabBarState();
}

class CustomTabBarState extends State<CustomTabBarNav> with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(length: widget.tabitem.length, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768, allowFontScaling: true);

    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: TabBar(
              controller: controller,
              isScrollable: true,
              tabs: widget.tabitem.map((tabitem) {
                return Tab(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(minWidth: ScreenUtil().setWidth(41), minHeight: ScreenUtil().setHeight(24)),
                    child: Text(
                      tabitem,
                      style: TextStyle(fontSize: ScreenUtil().setHeight(20), letterSpacing: ScreenUtil().setWidth(0.25), color: Theme.of(context).textTheme.bodyText1.color),
                    ),
                  ),
                );
              }).toList()),
        ),
        Expanded(
          child: TabBarView(physics: NeverScrollableScrollPhysics(), controller: controller, children: widget.tabbody),
        )
      ],
    );
  }
}
