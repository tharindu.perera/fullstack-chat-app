import 'dart:async';

import 'package:mbase/base/modules/switchlist/switchlist/bloc/switchlist_event.dart';
import 'package:mbase/base/modules/switchlist/switchlist/bloc/switchlist_state.dart';
import 'package:mbase/base/modules/switchlist/switchlist/repository/switchlist_repository.dart';

import '../../../../core/common_base/bloc/scy_bloc.dart';

class SwitchListBloc extends SCYBloC<SwitchlistEvent, SwitchListState> {
  StreamSubscription streamSubscription;
  SwitchListRepository switchListRepository = SwitchListRepository();

  SwitchListBloc() : super(SwitchListLoadedState(switchlistdata: []));

  @override
  Stream<SwitchListState> mapEventToState(SwitchlistEvent event) async* {
    if (event is FetchSwitchListEquipments) {
      yield* _mapSwitchListEquipmentFetchData(event);
    } else if (event is SwitchListEquipmentsLoadedEvent) {
      yield SwitchListEquipmentLoaded(switchLists: event.swtchlisteq);
    } else if (event is FetchSwitchListDataEvent) {
      yield* _mapSwitchListFetchData(event);
    } else if (event is FetchSwitchListDataLoadedEvent) {
      yield SwitchListDataLoaded(switchList: event.switchListData);
    }
  }

  Stream<SwitchListState> _mapSwitchListEquipmentFetchData(FetchSwitchListEquipments event) async* {
    streamSubscription?.cancel();
    yield SwitchListEquipmentInProgress();
    streamSubscription = switchListRepository.fetchRailcarsAssignedToSwitchList(event.switchListModel, event.searchQuery).listen((eve) async {
      add(SwitchListEquipmentsLoadedEvent(swtchlisteq: eve));
    });
  }

  Stream<SwitchListState> _mapSwitchListFetchData(event) async* {
    streamSubscription?.cancel();
    yield SwitchListDataInProgress();
    streamSubscription = switchListRepository.fetchSwitchListData(tabIndex: event.tabIndex, searchQuery: event.searchQuery).listen((eve) async {
      add(FetchSwitchListDataLoadedEvent(switchListData: eve));
    });
  }
}
