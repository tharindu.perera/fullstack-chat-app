import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/components/switchlist_table/tempory_model/switchlist_model.dart';

abstract class SwitchListState extends Equatable {}

class SwitchListInitialState extends SwitchListState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SwitchListLoadingState extends SwitchListState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SwitchListLoadedState extends SwitchListState {
  List<SwitchListModel> switchlistdata;

  SwitchListLoadedState({@required this.switchlistdata});

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class SwitchListErrorState extends SwitchListState {
  String message;

  SwitchListErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class SwitchListEquipmentInProgress extends SwitchListState{

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SwitchListEquipmentLoaded extends SwitchListState{
   List<NewSwitchList> switchLists;

  SwitchListEquipmentLoaded({@required this.switchLists});


  @override
  // TODO: implement props
  List<Object> get props =>  switchLists;
}


class SwitchListDataInProgress extends SwitchListState{

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SwitchListDataLoaded extends SwitchListState{
  List<NewSwitchList> switchList;

  SwitchListDataLoaded({@required this.switchList});


  @override
  // TODO: implement props
  List<Object> get props =>  switchList;
}




class SwitchListEquipmentErrorState extends SwitchListState {
  String message;

  SwitchListEquipmentErrorState({@required this.message});
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}