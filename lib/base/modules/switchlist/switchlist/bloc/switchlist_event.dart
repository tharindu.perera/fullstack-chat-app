import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/components/switchlist_table/tempory_model/switchlist_model.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/model/assigned_switchlist_model.dart';

abstract class SwitchlistEvent extends Equatable {}

// ignore: must_be_immutable
class FetchSwitchListEquipments extends SwitchlistEvent {
  NewSwitchList  switchListModel;
  String searchQuery;

  FetchSwitchListEquipments({@required this.switchListModel,@required this.searchQuery});

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}

class SwitchListEquipmentsLoadedEvent extends SwitchlistEvent {
  List<NewSwitchList>  swtchlisteq;

  SwitchListEquipmentsLoadedEvent({@required this.swtchlisteq});

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}


class FetchSwitchListDataEvent extends SwitchlistEvent {
  int tabIndex;
  String searchQuery;

  FetchSwitchListDataEvent({@required this.tabIndex,this.searchQuery});

  @override
  // TODO: implement props
  List<Object> get props => [tabIndex];

}

class FetchSwitchListDataLoadedEvent extends SwitchlistEvent {
  List<NewSwitchList>  switchListData;

  FetchSwitchListDataLoadedEvent({@required this.switchListData});

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}


