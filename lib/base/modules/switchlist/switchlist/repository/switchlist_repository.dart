import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/common_base/repositories/spot_repository.dart';
import 'package:mbase/base/core/common_base/repositories/track_repository.dart';
import 'package:mbase/base/core/common_base/repositories/yard_repository.dart';
import 'package:mbase/base/core/components/switchlist_table/tempory_model/switchlist_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/model/assigned_switchlist_model.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';
import 'package:uuid/uuid.dart';

import '../../../../core/constants/app_constants.dart';

class SwitchListRepository {
  FirebaseFirestore _firestore = DatabaseProvider.firestore;
  NotificationRepository notificationRepository = NotificationRepository();

  Future<String> loadSwitchlistJsonFile() async {
    return await rootBundle.loadString('assets/json_files/switchlist.json');
  }

  @override
  Future<List<SwitchListModel>> loadSwitchlist() async {
    return [];
  }

  Future convert(thing) {
    return new Future.value(thing);
  }

  Stream<List<SwitchListModel>> streamParser(Iterable things) async* {
    for (var t in things) {
      var r = await convert(t);

      if (r != null) {
        yield r;
      }
    }
  }

  Stream<List<NewSwitchList>> fetchSwitchListData({int tabIndex, String status, int sortColumnIndex, bool sortAscending, String selectedYard, String searchQuery}) {
    List<NewSwitchList> switchList;
    int completedRailcarCount = 0;

    tabIndex == 0
        ? status = APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_NEW
        : tabIndex == 1 ? status = APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS : tabIndex == 2 ? status = APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED : tabIndex == 3 ? status = "All" : null;

    print("tabIndexesss----> ${tabIndex}");

    CollectionReference switchListCollection = _firestore.collection("switchlist");

    var switchListDataCollection = status == "All"
            ? switchListCollection.where("permission_id", whereIn: [APP_CONST.PUBLIC, env.userProfile.id]).where("facility_id", isEqualTo: env.userProfile.facilityId)
    : switchListCollection.where("permission_id", whereIn: [APP_CONST.PUBLIC, env.userProfile.id]).where("facility_id", isEqualTo: env.userProfile.facilityId);
    return switchListDataCollection.snapshots().map((snapshot) {
      switchList = snapshot.docs.map((doc) => NewSwitchList.fromJson(doc.data())).toList();

      List removableIdList = [];

      switchList.forEach((sw) {
        removableIdList.add(sw.parent_id);
        removableIdList.add(sw.immediateParentId);
        if (sw.switch_list_status != status && status != "All") {
          removableIdList.add(sw.id);
        }
        //filter data based on search criteria
        if (searchQuery != null && searchQuery.trim().isNotEmpty) {
          if (!sw.switch_list_name.toUpperCase().startsWith(searchQuery.toUpperCase()) && !(sw.switch_crew ?? "").toUpperCase().startsWith(searchQuery.toUpperCase()) && (sw.switch_list_status?? "").toUpperCase() !=(searchQuery.toUpperCase())) {
            removableIdList.add(sw.id);
          }
        }
      });

      switchList.removeWhere((value) {
        return removableIdList.contains(value.id);
      });

      switchList.forEach((switchListDoc) {
        switchListDoc.railcars?.forEach((element) {
          if (element.switch_list_equipment_status == APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED || element.switch_list_equipment_status == APP_CONST.SWITCH_LIST_EQUIPMENT_STATUS_OUTBOUNDED) {
            completedRailcarCount++;
            switchListDoc.complete = ((completedRailcarCount / switchListDoc.railcars?.length) * 100).toString();
          } else {
            switchListDoc.complete = ((completedRailcarCount / switchListDoc.railcars?.length) * 100).toString();
          }
        });
        switchListDoc.Cars = switchListDoc.railcars?.length ?? 0;
        completedRailcarCount = 0;
      });

      // Sorts switchlist by target start date and moves the target start date null values to the end of the list
      // Note :  In your case just swap -1 and 1 to have them at the front of your List.
      switchList.sort((a, b) => b.target_start_date ==null ? -1: a.target_start_date ==null? 1 : b.target_start_date.compareTo(a.target_start_date));

      return switchList;
    });
  }

  @override
  Future<List<SwitchListModel>> fetchSwitchLists(String status, int sortColumnIndex, bool sortAscending, String selectedYard, String searchQuery) async {
    List<SwitchListModel> switchList = [];
    List<SwitchListEquipmentModel> publicSwitchListEquipment = [];
    List<SwitchListEquipmentModel> nonPublicSwitchListEquipment = [];

    int count = 0;
    int completedRailcarCount = 0;

    CollectionReference switchListCollection = _firestore.collection("switchlist");
    CollectionReference switchListEquipmentCollection = _firestore.collection("switchlist_equipment");

    var switchListData = status == "All"
        ? await switchListCollection.where("permission_id", whereIn: [APP_CONST.PUBLIC, env.userProfile.id]).where("facility_id", isEqualTo: env.userProfile.facilityId)
        .get()
        : await switchListCollection
            .where("permission_id", whereIn: [APP_CONST.PUBLIC, env.userProfile.id])
            .where("facility_id", isEqualTo: env.userProfile.facilityId)
            .where("switch_list_status", isEqualTo: status)
            .orderBy("target_start_date", descending: true).get();

    QuerySnapshot switchListEquipmentData = await switchListEquipmentCollection
        .where("transaction_status", isEqualTo: APP_CONST.TRANSACTION_STATUS_ACCEPTED)
        .where("permission_id", isEqualTo: APP_CONST.PUBLIC)
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .get();

    QuerySnapshot switchListEqSnapshot = await switchListEquipmentCollection.where("permission_id", isEqualTo: env.userProfile.id).where("facility_id", isEqualTo: env.userProfile.facilityId).get();

    switchList = switchListData.docs.map((e) {
      return SwitchListModel.fromMap(e.data());
    }).toList();

    List removableIdlist = [];

    switchList.forEach((element) {
      removableIdlist.add(element.parent_id);
      removableIdlist.add(element.immediateParentId);
    });

    switchList.removeWhere((element) {
      return removableIdlist.contains(element.id);
    });

    publicSwitchListEquipment = switchListEquipmentData.docs.map((e) => SwitchListEquipmentModel.fromMap(e.data())).toList();
    nonPublicSwitchListEquipment = switchListEqSnapshot.docs.map((e) => SwitchListEquipmentModel.fromMap(e.data())).toList();
    publicSwitchListEquipment.addAll(nonPublicSwitchListEquipment);

    List<SwitchListModel> tempList = [];

    switchList.forEach((element) {
      publicSwitchListEquipment.forEach((value) {
        if (element.id == value.switch_list_id || element.immediateParentId == value.switch_list_id) {
          count++;

          element.switchListEquipmentList.add(value);

          if (value.switch_list_equipment_status == "COMPLETED") {
            completedRailcarCount++;
          }
        }
      });

      if (element.SwitchCrew.toUpperCase().startsWith(searchQuery.toUpperCase()) ||
          element.SwitchListName.toUpperCase().startsWith(searchQuery.toUpperCase()) ||
          element.SwitchType.toUpperCase().startsWith(searchQuery)) {
        tempList.add(SwitchListModel(
          id: element.id,
          SwitchListName: element.SwitchListName,
          SwitchCrew: element.SwitchCrew,
          SwitchType: element.SwitchType,
          Status: element.Status,
          StartDate: element.StartDate,
          TargetDate: element.TargetDate,
          switchListEquipmentList: element.switchListEquipmentList,
          Complete: ((completedRailcarCount / count) * 100).toStringAsFixed(0),
          Cars: count,
        ));
      }
      count = 0;
      completedRailcarCount = 0;
    });
    return tempList;
  }

  Future<List<SwitchListEquipmentModel>> fetchRailCarsBySwitchList(SwitchListModel switchListModel, String searchQuery) async {
    List<SwitchListEquipmentModel> switchListEquipment = [];

    switchListModel.switchListEquipmentList.forEach((element) {
      if (element.switch_list_id == switchListModel.SwitchListName) {
        if (element.equipment_id.toUpperCase().startsWith(searchQuery.toUpperCase())) {
          switchListEquipment.add(element);
        }
      }
    });

    return switchListEquipment;
  }

  Stream<List<NewSwitchList>> fetchRailcarsAssignedToSwitchList(NewSwitchList switchListModel, String searchQuery) {
    CollectionReference switchListEquipmentCollection = _firestore.collection("switchlist");
    var query = switchListEquipmentCollection
        .where("switch_list_name", isEqualTo: switchListModel.switch_list_name)
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .where("permission_id", whereIn: [env.userProfile.id, APP_CONST.PUBLIC]);

    return query.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return NewSwitchList.fromJson(doc.data());
      }).toList();
    }).map((list) {
      List<String> removableIdlist = List();
      list.forEach((element) {
        removableIdlist.add(element.parent_id);
        removableIdlist.add(element.immediateParentId);
      });

      list.removeWhere((element) {
        return removableIdlist.contains(element.id);
      });
      if(searchQuery !=null && searchQuery.trim().isNotEmpty){
        list[0].railcars.removeWhere((element) => !(element.equipment_initial+" "+element.equipment_number).toUpperCase().startsWith(searchQuery.toUpperCase()));
      }
      return list;
    });
  }

  @override
  Future<List<SwitchListEquipmentModel>> fetchSwitchListEquipments(String switchlistID, String searchQuery, String selectedStatus) {
    return null;
  }

  @override
  Future<bool> updateSwitchListStatusToInProgress(NewSwitchList switchListModel) async {
    switchListModel.parent_id = switchListModel?.parent_id ?? switchListModel.id;
    switchListModel.immediateParentId = switchListModel.id;
    switchListModel.scy_pk = switchListModel.id;
    switchListModel.action_type = APP_CONST.SWL_START;
    switchListModel.operation_type = APP_CONST.SWITCHLIST_OPERATION;
    //switchListModel.id = Uuid().v1();
    switchListModel.source = APP_CONST.MOBILITY;
    switchListModel.permission_id = env.userProfile.id;
    switchListModel.transaction_status = APP_CONST.TRANSACTION_STATUS_PENDING;
    switchListModel.switch_list_status = APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS;
    switchListModel.switch_crew_name = switchListModel.switch_crew_name;
    switchListModel.switch_crew = switchListModel.switch_crew;
    switchListModel.facility_id = env.userProfile.facilityId;

    return notificationRepository.addNotification(switchListModel, APP_CONST.SWITCHLIST_START_OPERATION)

        //     .update({
        //   "switch_list_status":APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS,
        //
        // })
        .then((value) async {
      print("Successfully added");
      _firestore.collection("switchlist").doc(switchListModel.id).set(switchListModel.toMap());
      return true;
    }).catchError((onError) {
      print("onError----> ${onError}");
      return false;
    });
    // .then((value) => true)
    // .catchError((onError) => false);
  }

  Future<bool> editSwitchListEquipment(NewSwitchList newSwitchList) async {
    print(" scypk ------> ${newSwitchList.scy_pk}  || scy id ---> ${newSwitchList.id}");

    // return await _firestore.collection("switchlist").doc(newSwitchList.id).set(newSwitchList.toMap()).then((value) async {
    //   await notificationRepository.addNotification(newSwitchList, APP_CONST.SWITCHLIST_EDIT_RAILCAR);
    //   return true;
    // }).catchError((onError) => false);
     await _firestore.collection("switchlist").doc(newSwitchList.id).set(newSwitchList.toMap()).catchError((onError)=>debugPrint("Error : ${onError}"));
  }

  Future<void> completeSwitchListEquipment({
    NewSwitchList newSwitchList,
  }) async {
    // return await _firestore.collection("switchlist").doc(newSwitchList.id).set(newSwitchList.toMap()).then((value) async {
    //   // await notificationRepository.addNotification(newSwitchList, APP_CONST.SWITCHLIST_COMPLETE_RAILCAR);
    //
    //   return true;
    // }).catchError((onError) => false);
     await _firestore.collection("switchlist").doc(newSwitchList.id).set(newSwitchList.toMap()).catchError((onError) => debugPrint("Error : ${onError}"));
  }

  Future<bool> updateSwitchListStatusToComplete(NewSwitchList switchListModel) async {
    NewSwitchList switchList = NewSwitchList.fromJson(switchListModel.toMap());

    switchList.action_type = APP_CONST.SW_RAILCAR_COMPLETE;
    switchList.parent_id = switchListModel?.parent_id ?? switchListModel.id;
    switchList.immediateParentId = switchListModel.id;
    switchList.operation_type = APP_CONST.SWITCHLIST_OPERATION;
    switchList.scy_pk = switchListModel.id;
    switchList.transaction_status = APP_CONST.TRANSACTION_STATUS_PENDING;
    switchList.source = APP_CONST.MOBILITY;
    switchList.id = Uuid().v1();

    switchListModel.switch_list_status = APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_COMPLETED;

    return await _firestore.collection("switchlist").doc(switchList.id).set(switchList.toMap()).then((value) {
      return true;
    }).catchError((onError) {
      print("onError----> ${onError}");
      return false;
    });
  }

  Future<bool> validateEquipmentExistingLocation(SwitchListEquipmentModel switchListEquipmentModel) async {
    return true;
  }

  Future<bool> validateSpotAvailability(SwitchListEquipmentModel switchListEquipmentModel) async {
    return true;
  }

  YardRepository yardRepository = new YardRepository();
  TrackRepository trackRepository = new TrackRepository();
  SpotRepository spotRepository = new SpotRepository();

  Future<List<YardModel>> fetchYards() async {
    return [];
  }

  Future<List<TrackModel>> fetchTracks(String yard) async {
    return [];
  }

  Future<List<SpotModel>> fetchSpots({String yard, String track}) async {
    return [];
  }

  Future<String> fetchYardNameByID(String yardID) async {
    return null;
  }

  Future<String> fetchTrackNameByID(String trackID) async {
    return null;
  }

  Future<String> fetchSpotNameByID(String spotID) async {
    return null;
  }
}
