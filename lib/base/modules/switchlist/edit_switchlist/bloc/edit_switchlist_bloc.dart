import 'dart:async';

import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/modules/switchlist/switchlist/repository/switchlist_repository.dart';
import 'package:meta/meta.dart';

import '../../../../core/common_base/bloc/scy_bloc.dart';

part 'edit_switchlist_event.dart';
part 'edit_switchlist_state.dart';

class EditSwitchListsBloc
    extends SCYBloC<EditSwitchListEvent, EditSwitchListState> {
  final SwitchListRepository switchListRepository;

  EditSwitchListsBloc({SwitchListRepository switchListRepository})
      : switchListRepository = switchListRepository, super(EditSwitchListState.initial());

  // @override
  // EditSwitchListState get initialState => EditSwitchListState.initial();

  @override
  Stream<EditSwitchListState> mapEventToState(
    EditSwitchListEvent event,
  ) async* {
    if (event is EditSwitchListLoaded) {
      yield* _mapEditSwitchListLoadedToState();
    } else if (event is EditSwitchListYardChanged) {
      yield* _mapEditSwitchListYardChangedToState(event);
    } else if (event is EditSwitchListTrackChanged) {
      yield* _mapEditSwitchListTracksChangedToState(event);
    } else if (event is EditSwitchListSpotChanged) {
      yield* _mapEditSwitchListSpotChangedToState(event);
    }
  }

  Stream<EditSwitchListState> _mapEditSwitchListLoadedToState() async* {
    yield EditSwitchListState.yardsLoadInProgress();
    final yards = await switchListRepository.fetchYards();
    yield EditSwitchListState.yardsLoadSuccess(yards: yards);
  }

  Stream<EditSwitchListState> _mapEditSwitchListYardChangedToState(
      EditSwitchListYardChanged event) async* {
    final currentState = state;
    yield EditSwitchListState.tracksLoadInProgress(
      yards: currentState.yards,
      yard: event.yard,
    );
    final tracks = await switchListRepository.fetchTracks(event.yard);
    yield EditSwitchListState.tracksLoadSuccess(
      yards: currentState.yards,
      yard: currentState.yard,
      tracks: tracks,
    );
  }

  Stream<EditSwitchListState> _mapEditSwitchListTracksChangedToState(
      EditSwitchListTrackChanged event) async* {
    final currentState = state;
    yield EditSwitchListState.spotsLoadInProgress(
      yards: currentState.yards,
      yard: currentState.yard,
      tracks: currentState.tracks,
      track: event.track,
    );
    final spots = await switchListRepository.fetchSpots(
      yard: currentState.yard,
      track: event.track,
    );
    yield EditSwitchListState.spotsLoadSuccess(
      yards: currentState.yards,
      yard: currentState.yard,
      tracks: currentState.tracks,
      track: event.track,
      spots: spots,
    );
  }

  Stream<EditSwitchListState> _mapEditSwitchListSpotChangedToState(
      EditSwitchListSpotChanged event) async* {
    yield state.copyWith(spot: event.spot);
  }
}
