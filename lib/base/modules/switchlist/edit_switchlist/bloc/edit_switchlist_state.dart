part of 'edit_switchlist_bloc.dart';

class EditSwitchListState {
  final List<YardModel> yards;
  final String yard;

  final List<TrackModel> tracks;
  final String track;

  final List<SpotModel> spots;
  final String spot;

  const EditSwitchListState({
    @required this.yards,
    @required this.yard,
    @required this.tracks,
    @required this.track,
    @required this.spots,
    @required this.spot,
  });

  bool get isComplete => yard != null && track != null && spot != null;

  factory EditSwitchListState.initial() => EditSwitchListState(
        yards: <YardModel>[],
        yard: null,
        tracks: <TrackModel>[],
        track: null,
        spots: <SpotModel>[],
        spot: null,
      );

  factory EditSwitchListState.yardsLoadInProgress() => EditSwitchListState(
        yards: <YardModel>[],
        yard: null,
        tracks: <TrackModel>[],
        track: null,
        spots: <SpotModel>[],
        spot: null,
      );

  factory EditSwitchListState.yardsLoadSuccess({
    @required List<YardModel> yards,
  }) =>
      EditSwitchListState(
        yards: yards,
        yard: null,
        tracks: <TrackModel>[],
        track: null,
        spots: <SpotModel>[],
        spot: null,
      );

  factory EditSwitchListState.tracksLoadInProgress({
    @required List<YardModel> yards,
    @required String yard,
  }) =>
      EditSwitchListState(
        yards: yards,
        yard: yard,
        tracks: <TrackModel>[],
        track: null,
        spots: <SpotModel>[],
        spot: null,
      );

  factory EditSwitchListState.tracksLoadSuccess({
    @required List<YardModel> yards,
    @required String yard,
    @required List<TrackModel> tracks,
  }) =>
      EditSwitchListState(
        yards: yards,
        yard: yard,
        tracks: tracks,
        track: null,
        spots: <SpotModel>[],
        spot: null,
      );

  factory EditSwitchListState.spotsLoadInProgress({
    @required List<YardModel> yards,
    @required String yard,
    @required List<TrackModel> tracks,
    @required String track,
  }) =>
      EditSwitchListState(
        yards: yards,
        yard: yard,
        tracks: tracks,
        track: track,
        spots: <SpotModel>[],
        spot: null,
      );

  factory EditSwitchListState.spotsLoadSuccess({
    @required List<YardModel> yards,
    @required String yard,
    @required List<TrackModel> tracks,
    @required String track,
    @required List<SpotModel> spots,
  }) =>
      EditSwitchListState(
        yards: yards,
        yard: yard,
        tracks: tracks,
        track: track,
        spots: spots,
        spot: null,
      );

  EditSwitchListState copyWith({
    List<YardModel> yards,
    String yard,
    List<TrackModel> tracks,
    String track,
    List<String> spots,
    String spot,
  }) {
    return EditSwitchListState(
      yards: yards ?? this.yards,
      yard: yard ?? this.yard,
      tracks: tracks ?? this.tracks,
      track: track ?? this.track,
      spots: spots ?? this.spots,
      spot: spot ?? this.spot,
    );
  }
}
