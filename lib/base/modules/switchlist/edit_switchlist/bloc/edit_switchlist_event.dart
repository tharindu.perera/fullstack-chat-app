part of 'edit_switchlist_bloc.dart';

@immutable
abstract class EditSwitchListEvent {
  const EditSwitchListEvent();
}

class EditSwitchListLoaded extends EditSwitchListEvent {}

class EditSwitchListYardChanged extends EditSwitchListEvent {
  final String yard;

  const EditSwitchListYardChanged({this.yard});
}

class EditSwitchListTrackChanged extends EditSwitchListEvent {
  final String track;

  const EditSwitchListTrackChanged({this.track});
}

class EditSwitchListSpotChanged extends EditSwitchListEvent {
  final String spot;

  const EditSwitchListSpotChanged({this.spot});
}
