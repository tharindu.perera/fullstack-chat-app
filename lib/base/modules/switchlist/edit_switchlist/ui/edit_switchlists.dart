import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/common_base/repositories/yard_repository.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/model/assigned_switchlist_model.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/base/modules/switchlist/edit_switchlist/bloc/edit_switchlist_bloc.dart';
import 'package:mbase/base/modules/switchlist/switchlist/repository/switchlist_repository.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

import '../../../../core/components/custom_dialog/custom_dialog.dart';
import '../../../../core/constants/app_constants.dart';

class EditSwitchList extends StatelessWidget {
  final NewSwitchListEquipment newSwitchListEquipment;
  final NewSwitchList newSwitchList;

  const EditSwitchList(
      {Key key, this.newSwitchListEquipment, this.newSwitchList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar(),
      drawer: AppDrawer(),
      body: BlocProvider(
        create: (context) => EditSwitchListsBloc(
          switchListRepository: SwitchListRepository(),
        ),
        child: EditSwitchLists(
          switchListEquipments: newSwitchListEquipment,
          newSwitchList: newSwitchList,
        ),
      ),
    );
  }
}

class EditSwitchLists extends StatefulWidget {
  final NewSwitchListEquipment switchListEquipments;
  final NewSwitchList newSwitchList;

  const EditSwitchLists(
      {Key key, this.switchListEquipments, this.newSwitchList})
      : super(key: key);

  @override
  _EditSwitchListsState createState() => _EditSwitchListsState();
}

class _EditSwitchListsState extends State<EditSwitchLists> {
  final _editSwitchListBloc = EditSwitchListsBloc();
  List<bool> isSelected;
  bool selectedYard = false;
  final commentController = TextEditingController();
  final globalKey = GlobalKey<ScaffoldState>();
  YardRepository yardRepository = new YardRepository();
  String yardName = " ";
  String selectedYardID;

  String selectedTrackID;

  String selectedSpotID;

  String railcarComment;

  String initialYardID;
  String initialTrackID;
  String initialSpotID;
  NotificationRepository notificationRepository = NotificationRepository();

  SwitchListRepository switchListRepository = new SwitchListRepository();
  List<String> initialFieldValues = List<String>();
  List<String> checkValues = List<String>();
  Function alteredListChecker = const ListEquality().equals;
  FToast fToast;

  List<YardModel> yardList = [];

  List<TrackModel> trackList = [];
  List<TrackModel> filteredTracksList = [];

  List<SpotModel> spotList = [];
  List<SpotModel> filteredSpotsList = [];
  SwitchListEquipmentModel assignedSwitchListModel = SwitchListEquipmentModel();

  @override
  void initState() {
    super.initState();
    isSelected = [true, false];
    fToast = FToast(context);
    yardList = MemoryData.inMemoryYardMap.values.toList();
    trackList = MemoryData.inMemoryTrackMap.values.toList();
    yardList.sort((a, b) => a.yardName.compareTo(b.yardName));
    spotList = MemoryData.inMemorySpotMap.values.toList();
    spotList = spotList
        .where((element) =>
            element.trackId == widget.switchListEquipments.to_track_id)
        .toList();

    sortYardTrackData();
    print("my comment ---> ${ widget.switchListEquipments?.switch_list_comment }");
    setState(() {
      widget.switchListEquipments?.switch_list_comment = "heyy";
      widget.switchListEquipments?.switch_list_comment = commentController.value.text;
      filteredTracksList = trackList;
      filteredSpotsList = spotList;
      initialYardID = widget.switchListEquipments.to_yard_id ?? null;
      initialTrackID = widget.switchListEquipments.to_track_id ?? null;
      initialSpotID = widget.switchListEquipments.to_spot_id ?? null;
    });
  }



  sortYardTrackData() {
    yardList.sort((a, b) => a.yardName.compareTo(b.yardName));
    trackList.sort((a, b) => a.trackName.compareTo(b.trackName));
  }



  onYardChange(String value) {
    assignedSwitchListModel.to_track_id = null;
    assignedSwitchListModel.to_spot_id = null;

    assignedSwitchListModel.to_yard_id =
        yardList.singleWhere((element) => element.yardName == value).id;

    var matchedYardObject = yardList
        .where((element) => element.yardName == value)
        .map((e) => e.id)
        .toList();

    var filteredTracks = trackList
        .where((element) => element.yardId == matchedYardObject[0])
        .toList();
    filteredTracksList = [];

    setState(() {
      filteredTracksList = filteredTracks;
      initialYardID = assignedSwitchListModel.to_yard_id;
      initialTrackID = assignedSwitchListModel.to_track_id;
      initialSpotID = assignedSwitchListModel.to_spot_id;
    });
  }

  onTrackChange(String value) {
    assignedSwitchListModel.to_spot_id = null;
    assignedSwitchListModel.to_track_id =
        trackList.singleWhere((element) => element.trackName == value).id;
    var matchedTrackObject = trackList
        .where((element) => element.trackName == value)
        .map((e) => e.id)
        .toList();

    var filteredSpots = spotList
        .where((element) => element.trackId == matchedTrackObject[0])
        .toList();

    filteredSpots.sort((a, b) => a.spotName.compareTo(b.spotName));

    setState(() {
      filteredSpotsList = filteredSpots;
      spotList = MemoryData.inMemorySpotMap.values.toList();
      initialYardID = assignedSwitchListModel.to_yard_id;
      initialTrackID = assignedSwitchListModel.to_track_id;
      initialSpotID = assignedSwitchListModel.to_spot_id;
    });
  }

  onSpotChange(String value) {
    assignedSwitchListModel.to_spot_id =
        spotList.where((element) => element.spotName == value).first.id;

    setState(() {
      initialSpotID = assignedSwitchListModel.to_spot_id;
//      initialYardID = assignedSwitchListModel.to_yard_id;
//      initialTrackID = assignedSwitchListModel.to_track_id;
//      initialSpotID = assignedSwitchListModel.to_spot_id;
    });
  }

  popUpDialog() async {
    checkValues.add(selectedYardID ?? "");
    checkValues.add(selectedTrackID ?? "");
    checkValues.add(selectedSpotID ?? "");
    checkValues.add(commentController.text ?? "");

    var result = alteredListChecker(initialFieldValues, checkValues);
    NotificationRepository notificationRepository = NotificationRepository();

    if (!result) {
      final action = await CustomDialog.cstmDialog(
          context, "add_railcar", "Unsaved Changes", "");
      if (action[0] == DialogAction.yes) {
        Navigator.pop(context);
      }
    } else {
      Navigator.of(context).pop();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _editSwitchListBloc.close();
    fToast = FToast(context);
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    final err_msg = SnackBar(content: Text('Oops, Something Went Wrong'));

    _showToast() {
      fToast.showToast(
        child: CustomToast(
          toastColor: Color(0xff7fae1b),
          toastMessage: "Railcar edited successfully.",
        ),
        gravity: ToastGravity.TOP,
        toastDuration: Duration(seconds: 2),
      );
    }

    saveEditRailcar() async {
      NewSwitchList switchList =
          NewSwitchList.fromJson(widget.newSwitchList.toMap());
      EquipmentDAO eq_dao = EquipmentDAO();
      switchList.action_type = APP_CONST.SW_UPDATE_RAILCAR;
      switchList.operation_type = APP_CONST.SWITCHLIST_OPERATION;
      switchList.parent_id =
          widget.newSwitchList?.parent_id ?? widget.newSwitchList.id;
      switchList.immediateParentId = widget.newSwitchList.id;

      switchList.scy_pk = widget.newSwitchList.id;

      //   switchList.scy_pk = widget.newSwitchList.scy_pk;

      switchList.source = APP_CONST.MOBILITY;
      switchList.transaction_status = APP_CONST.TRANSACTION_STATUS_PENDING;
      switchList.id = Uuid().v1();

      for (int i = 0; i < switchList.railcars.length; i++) {
        NewSwitchListEquipment railcar = switchList.railcars[i];

        if (railcar.equipment_initial ==
                widget.switchListEquipments.equipment_initial &&
            railcar.equipment_number ==
                widget.switchListEquipments.equipment_number) {
          railcar.switch_list_comment = commentController.text;
          railcar.asset_master_id = (await eq_dao.getEquipmentByInitAndNumber(
                  railcar.equipment_initial, railcar.equipment_number))
              .assetMasterId;
          railcar.scy_pk = railcar.id;
          railcar.railcar_action_type = APP_CONST.RAILCAR_UPDATE;
          railcar.to_yard_id = assignedSwitchListModel.to_yard_id;
          railcar.to_track_id = assignedSwitchListModel.to_track_id;
          railcar.to_spot_id = assignedSwitchListModel.to_spot_id;
          railcar.switch_list_comment = commentController.value.text;
        }
      }

      //  bool saveSuccess = await switchListRepository.editSwitchListEquipment(switchList);
      switchListRepository.editSwitchListEquipment(switchList);
      notificationRepository.addNotification(
          switchList, APP_CONST.SWITCHLIST_EDIT_RAILCAR);
      _showToast();
      Navigator.pop(context);
      // if (saveSuccess == true) {
      //   _showToast();
      //   Navigator.pop(context);
      // } else {
      //   globalKey.currentState.showSnackBar(err_msg);
      // }
    }

    return Builder(
      builder: (context) {
        return (Scaffold(
          key: globalKey,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: CardUI(
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.all(20),
                                      child: Container(
                                          child: ListTile(
                                        leading: GestureDetector(
                                            onTap: () {
                                              popUpDialog();
                                            },
                                            child: Icon(
                                              Icons.arrow_back,
                                            )),
                                        title: Text('Edit Switch List',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText2),
                                        subtitle: Text(
                                            widget.switchListEquipments
                                                    .equipment_initial +
                                                " " +
                                                widget.switchListEquipments
                                                    .equipment_number,
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline5),
                                      ))),
                                ],
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.all(20),
                                        child: Container(
                                            child: GestureDetector(
                                          onTap: () {
                                            popUpDialog();
                                          },
                                          child: RichText(
                                            text: TextSpan(
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText2,
                                                children: <InlineSpan>[
                                                  TextSpan(
                                                      text: 'CANCEL',
                                                      style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: ScreenUtil()
                                                              .setHeight(14),
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          letterSpacing:
                                                              ScreenUtil()
                                                                  .setWidth(
                                                                      1.25),
                                                          color: Color(
                                                              0xFF3e8aeb)))
                                                ]),
                                          ),
                                        ))),
                                    Padding(
                                        padding: EdgeInsets.only(
                                            left: 0,
                                            top: 14,
                                            right: 24,
                                            bottom: 14),
                                        child: Container(
                                          width: ScreenUtil().setWidth(180),
                                          height: ScreenUtil().setHeight(48),
                                          child: FlatButton(
                                              //  onPressed:() => Navigator.push(context, MaterialPageRoute(builder: (context)=> CompleteSwitchListPage() )),
                                              onPressed: saveEditRailcar,
                                              child: Text(
                                                'SAVE',
                                                style: TextStyle(
                                                    fontFamily: 'Roboto',
                                                    fontSize: ScreenUtil()
                                                        .setHeight(14),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(1.25),
                                                    color: Colors.white),
                                              ),
                                              color: Color(0xFF508be4),
                                              textColor: Colors.white,
                                              disabledColor: Color(0xFF3e8aeb),
                                              disabledTextColor: Colors.black,
                                              splashColor: Color(0xFF3e8aeb)),
                                        )),
                                  ],
                                )
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(14),
                        ),
                        Divider(
                          thickness: 5.0,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                CustomDropdown(
                                    hintText: "Yard",
                                    itemList: yardList.isNotEmpty == true
                                        ? yardList
                                            .map((e) => e.yardName)
                                            .toList()
                                        : const [],
                                    SelectedValue: initialYardID == null
                                        ? null
                                        : MemoryData
                                            .inMemoryYardMap[initialYardID]
                                            .yardName
                                    //   widget.switchListEquipments.to_yard_id == null ? null : MemoryData.inMemoryYardMap[widget.switchListEquipments.to_yard_id].yardName
                                    ,
                                    Ontap: (str) => onYardChange(str)),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                CustomDropdown(
                                    hintText: "Track",
                                    itemList:
                                        filteredTracksList.isNotEmpty == true
                                            ? filteredTracksList
                                                .map((e) => e.trackName)
                                                .toList()
                                            : const [],
                                    SelectedValue: initialTrackID == null
                                        ? null
                                        : MemoryData
                                            .inMemoryTrackMap[initialTrackID]
                                            .trackName,

                                    //  SelectedValue: assingedSwitchlistModel.to_track_id == null ? null : MemoryData.inMemoryTrackMap[assingedSwitchlistModel.to_track_id].trackName  ,
                                    Ontap: (str) => onTrackChange(str)),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                filteredSpotsList.isNotEmpty
                                    ? CustomDropdown(
                                        hintText: "Spot",
                                        itemList:
                                            filteredSpotsList.isNotEmpty == true
                                                ? filteredSpotsList
                                                    .map((e) => e.spotName)
                                                    .toList()
                                                : const [],
                                        //     SelectedValue:assingedSwitchlistModel.to_spot_id == null ? null : MemoryData.inMemorySpotMap[assingedSwitchlistModel.to_spot_id].spotName  ,
                                        SelectedValue: initialSpotID == null
                                            ? null
                                            : MemoryData
                                                .inMemorySpotMap[initialSpotID]
                                                .spotName,
                                        Ontap: (str) => onSpotChange(str))
                                    : Container(
                                        width: ScreenUtil().setWidth(213),
                                        height: ScreenUtil().setHeight(48),
                                      ),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Container(
                                    width: 300,
                                    padding:
                                        EdgeInsets.symmetric(vertical: 4.0),
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      title: Text('Comments',
                                          style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize:
                                                  ScreenUtil().setHeight(16),
                                              fontWeight: FontWeight.normal,
                                              fontStyle: FontStyle.normal,
                                              letterSpacing:
                                                  ScreenUtil().setWidth(0.5),
                                              color: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xFFffffff)
                                                  : Color.fromRGBO(
                                                      0, 0, 0, 0.6))),
                                    ))),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Container(
                                    color: Theme.of(context)
                                        .scaffoldBackgroundColor,
                                    padding:
                                        EdgeInsets.symmetric(vertical: 1.0),
                                    alignment: Alignment.center,
                                    child: TextField(
                                      controller: commentController ,
                                      decoration: InputDecoration(
                                          border: new OutlineInputBorder(
                                              borderSide: new BorderSide(
                                                  color: Colors.teal))),
                                      style:
                                          Theme.of(context).textTheme.bodyText2,
                                      maxLines: 10,
                                      keyboardType: TextInputType.multiline,
                                    ),
                                  )),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ));
      },
    );
  }
}
