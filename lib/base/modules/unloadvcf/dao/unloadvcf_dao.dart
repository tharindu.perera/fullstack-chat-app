import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/repositories/offline_data_repository.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/unloadvcf/model/unloadvcf_model.dart';
import 'package:mbase/env.dart';
import 'package:uuid/uuid.dart';

class UnloadVcfDAO extends BaseDao {
  UnloadVcfDAO() : super("unload_vcf");

  Future<void> add({UnloadVcfModel unloadVcfModel}) async {
    if (unloadVcfModel.id == null) {
      unloadVcfModel.id = Uuid().v1();
    }
    return OfflineDataRepository.persistToDataStore(unloadVcfModel, collection);
  }

  Future<UnloadVcfModel> getExistingUnloadVcf(UnloadVcfModel unloadVcfModel) async {
    try {
     print(" fetching Load UnloadVcfModel data ....");
      QuerySnapshot qShotUser = await BaseQuery()
          .where("facility_visit_id", isEqualTo: unloadVcfModel.facility_visit_id).where("asset_master_id", isEqualTo: unloadVcfModel.asset_master_id)
          .where("permission_id", isEqualTo: env.userProfile.id).get();
      if (qShotUser.docs.isNotEmpty) {
        return UnloadVcfModel.fromMap(qShotUser.docs[0].data());
      } else {
        QuerySnapshot qShotPublic = await BaseQuery()
            .where("facility_visit_id", isEqualTo: unloadVcfModel.facility_visit_id).where("asset_master_id", isEqualTo: unloadVcfModel.asset_master_id)
            .where("permission_id", isEqualTo: APP_CONST.PUBLIC).get();
        if (qShotPublic.docs.isNotEmpty) {
          UnloadVcfModel toReturn = UnloadVcfModel.fromMap(qShotPublic.docs[0].data());
          toReturn.parent_id = toReturn.id;
          toReturn.immediateParentId = toReturn.id;
          toReturn.id = null;
          return toReturn;
        } else {
          return null;
        }
      }
    } on Exception {
      throw Exception('Error in fetching unloadVCF Equipment Number');
    }
  }
}
