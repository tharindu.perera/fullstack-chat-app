import 'dart:convert';

UnloadVcfModel unloadVcfModelFromMap(String str) => UnloadVcfModel.fromMap(json.decode(str));

String unloadVcfModelToMap(UnloadVcfModel data) => json.encode(data.toMap());

class UnloadVcfModel {
  UnloadVcfModel(
      {this.id, this.asset_master_id, this.facility_visit_id, this.product_id, this.product_name, this.opening_outage,
        this.observed_temp, this.gravity, this.permission_id, this.gross_gallons, this.net_gallons, this.facility_id,
        this.parent_id, this.created_dt, this.updated_dt, this.user, this.operation_type, this.vcf_header_description, this.scy_pk,
        this.immediateParentId, this.transaction_status, this.source, this.observed_temperature_scale, this.actual_outage_in, this.variance_gallons,
      this.variance_percent});

  String id;
  String asset_master_id;
  String facility_visit_id;
  String product_id;
  String product_name;
  String opening_outage;
  String observed_temp;
  String gravity;
  String permission_id;
  String gross_gallons;
  String net_gallons;
  String facility_id;
  String parent_id;
  DateTime created_dt;
  DateTime updated_dt;
  String user;
  String operation_type;
  String vcf_header_description;
  String scy_pk;
  String immediateParentId;
  String transaction_status;
  String observed_temperature_scale;
  String source;
  String actual_outage_in;
  String variance_gallons;
  String variance_percent;
  String equipment_id; //to display in notification

  UnloadVcfModel.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        asset_master_id = map['asset_master_id'],
        facility_visit_id = map['facility_visit_id'],
        product_id = map['product_id'],
        product_name = map['product_name'],
        opening_outage = map['opening_outage_in'],
        permission_id = map['permission_id'],
        observed_temp = map['observed_temp'],
        facility_id = map['facility_id'],
        parent_id = map['parent_id'],
        gravity = map['gravity'],
        gross_gallons = map['gross_gallons'],
        net_gallons = map['net_gallons'],
        user = map['user'],
        operation_type = map['operation_type'],
        vcf_header_description = map['vcf_header_description'],
        immediateParentId = map["immediateParentId"],
        transaction_status = map["transaction_status"],
        observed_temperature_scale = map["observed_temperature_scale"],
        source = map["source"],
        scy_pk = map['scy_pk'],
        actual_outage_in = map['actual_outage_in'],
        variance_gallons = map["variance_gallons"],
        variance_percent = map["variance_percent"],
        created_dt = map['created_dt'] == null ? null : DateTime.parse(map['created_dt']),
        updated_dt = map['updated_dt'] == null ? null : DateTime.parse(map['updated_dt']);

  Map<String, dynamic> toMap() => {
        "id": id,
        "asset_master_id": asset_master_id,
        "facility_visit_id": facility_visit_id,
        "product_id": product_id,
        "product_name": product_name,
        "permission_id": permission_id,
        "opening_outage_in": opening_outage,
        "observed_temp": observed_temp,
        "gross_gallons": gross_gallons,
        "net_gallons": net_gallons,
        "gravity": gravity,
        "facility_id" : facility_id,
        "parent_id" : parent_id,
        "user": user,
        "operation_type": operation_type,
        "vcf_header_description": vcf_header_description,
        "scy_pk": scy_pk,
        "immediateParentId": immediateParentId,
        "transaction_status": transaction_status,
        "observed_temperature_scale": observed_temperature_scale,
        "actual_outage_in": actual_outage_in,
        "variance_gallons": variance_gallons,
        "variance_percent": variance_percent,
        "source": source,
        "created_dt": created_dt == null ? null : created_dt.toString(),
        "updated_dt": updated_dt == null ? null : updated_dt.toString(),
      };

  @override
  String toString() {
    return 'UnloadVcfModel{id: $id, equipment_id: $asset_master_id, facility_visit_id: $facility_visit_id}';
  }
}
