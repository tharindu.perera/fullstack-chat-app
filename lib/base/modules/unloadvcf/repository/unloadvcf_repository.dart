import 'package:mbase/base/core/common_base/dao/asset_master_data_dao.dart';
import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/dao/outage_dao.dart';
import 'package:mbase/base/core/common_base/dao/product_attributes_dao.dart';
import 'package:mbase/base/core/common_base/dao/vcf_header_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/outage_model.dart';
import 'package:mbase/base/core/common_base/model/product_attributes_model.dart';
import 'package:mbase/base/core/common_base/model/vcf_header_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';

class UnloadvcfRepository {

  EquipmentDAO equipmentDAO = new EquipmentDAO();
  var commonDao = CommonDao();
  NotificationRepository notificationRepository = NotificationRepository();

  Future<List<Equipment>> fetchRailcars() async {
    Map<String, dynamic> criteria = Map();
    criteria.putIfAbsent("permission_id", () => [env.userProfile.id, APP_CONST.PUBLIC]);
    List<Equipment> equipmentList = await equipmentDAO.getRecordsByCriteria(criteria);
    return equipmentList.where((element) => element.compartmentList.isNotEmpty && double.parse(element.compartmentList[0].currentAmount ?? "0") > 0).toList();
  }

  Future<List<ProductAttributesModel>> fetchProductAttributeList () async {
    return ProductAttributesDao().fetchAllProductAttributes(env.userProfile.facilityId);
  }

  Future<AssetMasterDataModel> fetchAssetMasterModel(String asset_master_id) async {
    List<AssetMasterDataModel> assetMasterDataModelList = await AssetMasterDataDao().fetchAssetById(asset_master_id);
    if (assetMasterDataModelList.isNotEmpty) {
      return assetMasterDataModelList[0];
    } else {
      return null;
    }
  }

  Future<OutageModel> getOutageAmountForTheOutageInches(String asset_master_id) async {
    return await OutageDao().fetchOutageById(asset_master_id);
  }

  Future<VCFHeaderModel> getVcfDetailsForProduct(String vcf_header_id) async {
    return await VCFHeaderDao().fetchVCFById(vcf_header_id);
  }
}
