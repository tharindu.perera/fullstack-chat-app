import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/vcf_header_model.dart';
import 'package:mbase/base/core/util/common_vcf.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/outage_model.dart';
import 'package:mbase/base/core/common_base/model/product_attributes_model.dart';
import 'package:mbase/base/core/common_base/model/vcf_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/util/nullCheckUtil.dart';
import 'package:mbase/base/core/util/util_functions.dart';
import 'package:mbase/base/modules/listview/actions/unloadcar/ui/unloadcar.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/base/modules/unloadvcf/dao/unloadvcf_dao.dart';
import 'package:mbase/base/modules/unloadvcf/model/unloadvcf_model.dart';
import 'package:mbase/base/modules/unloadvcf/repository/unloadvcf_repository.dart';
import 'package:mbase/base/modules/vcf_table/vcf_handler.dart';
import 'package:provider/provider.dart';
import 'package:mbase/env.dart';

// ignore: must_be_immutable
class EditUnloadVcf extends StatefulWidget {
  Equipment equipment;
  AssetMasterDataModel assetMasterModel;

  EditUnloadVcf(Equipment equipment, AssetMasterDataModel assetMasterModel) {
    this.equipment = equipment;
    this.assetMasterModel =  assetMasterModel;
  }

  @override
  _EditUnloadVcfState createState() => _EditUnloadVcfState(equipment, assetMasterModel);
}

class _EditUnloadVcfState extends State<EditUnloadVcf> {

 // final formatter = new NumberFormat('#,##,000');
  final formatter = new NumberFormat('###,000');

  Equipment equipment;
  AssetMasterDataModel assetMasterModel;
  final globalKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  FToast fToast;
  List<ProductAttributesModel> productAttributeList = [];
  UnloadvcfRepository unloadVcfrepo = new UnloadvcfRepository();
  UnloadVcfDAO unloadVcfDAO = new UnloadVcfDAO();
  VCFHandler vcfHandler = new VCFHandler();

  ProductAttributesModel selectedProduct = new ProductAttributesModel();
  UnloadVcfModel unloadVcfModel = new UnloadVcfModel();
  String selectedProductName = null;
  bool canProductDropDownSelect = false;
  bool isCalculationSuccess = true;
  bool autoValidate = false;
  String openingOutage = null;
  String observedTemp = null;
  String gravity = null;
  String gravityType = null;
  String grossGallons = null;
  String netGallons  = null;
  String currentAmountUomCnvrtd = "";
  String tareWeightUomCnvrtd = "";
  String vcf_header_description = null;
  String get currentAmountUomCnvrtdUi => !isNullOrEmpty(this.currentAmountUomCnvrtd) ? formatter.format(double.parse(this.currentAmountUomCnvrtd)) : '';
  String get tareWeightUomCnvrtdUi => !isNullOrEmpty(this.tareWeightUomCnvrtd) ? formatter.format(double.parse(this.tareWeightUomCnvrtd)) : '';
  String get loadLimitUi => !isNullOrEmpty(this.assetMasterModel?.load_limit) ? formatter.format(double.parse(this.assetMasterModel?.load_limit)) : '';
  String get gallonsCapacityUi => !isNullOrEmpty(this.assetMasterModel?.gallon_capacity) ? formatter.format(double.parse(this.assetMasterModel?.gallon_capacity)) : '';
  String get grossGallonsUi => !isNullOrEmpty(this.grossGallons) ? formatter.format(double.parse(this.grossGallons)) : '';
  String get netGallonsUi => !isNullOrEmpty(this.netGallons) ? formatter.format(double.parse(this.netGallons)) : '';
  final TextEditingController _gravityController = new TextEditingController();
  final TextEditingController _openingOutageController = new TextEditingController();
  final TextEditingController _observedTempController = new TextEditingController();

  _EditUnloadVcfState(Equipment equipment, AssetMasterDataModel assetMasterModel) {
    this.equipment = equipment;
    this.assetMasterModel = assetMasterModel;
  }

  popUpDialog() async {
      if (this.isCalculationSuccess) {
        Navigator.of(context).pop();
      } else {
        final action = await CustomDialog.cstmDialog(context, "add_railcar", "Unsaved Changes", "");
        if (action[0] == DialogAction.yes) {
          Navigator.pop(context);
        }
      }
  }

  _showToast({String toastMessage, Color toastColor, IconData icon}) {
    fToast = FToast(context);
    fToast.showToast(
      child: CustomToast(
        toastColor: toastColor,
        toastMessage: toastMessage,
        icon: icon,
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);
    _fetchRelevantData();
  }

  _fetchRelevantData() async {
    Loader.show(context);
    Future(() async {
      this.productAttributeList = await unloadVcfrepo.fetchProductAttributeList();
      UnloadVcfModel unloadVcfModel = new UnloadVcfModel();
      unloadVcfModel.asset_master_id = this.equipment.assetMasterId;
      unloadVcfModel.facility_visit_id = this.equipment.facilityVisitId;
      var unloadVcfModelObj = await unloadVcfDAO.getExistingUnloadVcf(unloadVcfModel);
      if (null != unloadVcfModelObj) {
        this.unloadVcfModel = unloadVcfModelObj;
      }
      setInitialData();
      Loader.hide();
      setState(() {});
    });
  }

  setInitialData() {
    this.canProductDropDownSelect = true;
    // if UnloadVCF already saved for selected equipment for the current cycle
    if (this.unloadVcfModel.id != null || this.unloadVcfModel.parent_id != null) {
      if (this.unloadVcfModel.product_id != null) {
        try {
          this.selectedProduct = productAttributeList.singleWhere((element) => element.product_id == this.unloadVcfModel.product_id);
          this.selectedProductName = this.selectedProduct?.product_name;
          this.gravityType = this.selectedProduct?.gravity_type;
        } on StateError {}
      }
      this.gravity = this.unloadVcfModel.gravity;
      this._gravityController.text = this.unloadVcfModel.gravity;
      this.grossGallons = this.unloadVcfModel.gross_gallons;
      this.netGallons = this.unloadVcfModel.net_gallons;
      if (!isNullOrEmpty(this.unloadVcfModel.observed_temperature_scale) && this.unloadVcfModel.observed_temperature_scale == APP_CONST.C) {
        String fvalue = Util.convertCelciusToFarenhite(this.unloadVcfModel.observed_temp);
        this.observedTemp = fvalue;
        this._observedTempController.text = fvalue;
      } else {
        this.observedTemp = this.unloadVcfModel.observed_temp;
        this._observedTempController.text = this.unloadVcfModel.observed_temp;
      }
      this.openingOutage = this.unloadVcfModel.opening_outage;
      this._openingOutageController.text = this.unloadVcfModel.opening_outage;
      this.vcf_header_description = this.unloadVcfModel.vcf_header_description;
      this.autoValidate = true;
    } else { // If no Unload VCF saved for the equipment for the current cycle
      try {
        this.selectedProduct = (!equipment.compartmentList.isEmpty && null != equipment.compartmentList[0].productName) ? productAttributeList.singleWhere((element) => element.product_name == equipment.compartmentList[0].productName)  : new ProductAttributesModel();
        this.selectedProductName = this.selectedProduct?.product_name;
        this.gravityType = this.selectedProduct?.gravity_type;
      } on StateError {}
      if (this.selectedProduct?.gravity != null && "True" == MemoryData.facilityConfiguration.allowToSetGravityFromProductAttributeInUnloadVCF) {
        this.gravity = this.selectedProduct?.gravity;
        _gravityController.text = this.selectedProduct?.gravity;
      }
    }

    // UOM conversions
    if (!equipment.compartmentList.isEmpty) {
      var currentAmountValue = Util.convertValueFromToUom(equipment.compartmentList[0].currentAmount, equipment.compartmentList[0].uomId, "3", 3);
      this.currentAmountUomCnvrtd = currentAmountValue != null ? currentAmountValue : "";
    } else {
      this.currentAmountUomCnvrtd = "";
    }

    if (this.assetMasterModel != null) {
      var tareWeightValue = Util.convertValueFromToUom(this.assetMasterModel.tare_weight, this.assetMasterModel.tare_weight_uom_id, "2", 3);
      this.tareWeightUomCnvrtd = tareWeightValue != null ? tareWeightValue : "";
    } else {
      this.tareWeightUomCnvrtd = "";
    }
  }

  _calculate() async {
    this.autoValidate = true;
    final FormState form = _formKey.currentState;
    if (form.validate() && this.selectedProductName != null) {
      Loader.show(context);
      Future(() async {
        bool isGrossCalOk = false;
        bool isNetCalOk = false;
        isGrossCalOk = await _calculateGrossGallons();
        if (isGrossCalOk) {
          isNetCalOk = await _calculateNetGallons();
        }
        this.isCalculationSuccess = isGrossCalOk && isNetCalOk;
        Loader.hide();
        setState(() {});
      });
    } else {
      setState(() {});
    }

  }

  Future<bool> _calculateNetGallons() async {
    if (isNullOrEmpty(this.grossGallons)) {
      return false;
    }
    VCFHeaderModel vcfHeaderModel = await unloadVcfrepo.getVcfDetailsForProduct(this.selectedProduct.vcf_header_id);
    if (null == vcfHeaderModel) {
      _showToast(toastMessage: 'No VCF details found for this product', toastColor: Colors.red, icon: Icons.warning);
      return false;
    }


    String specificGravity = "API" == this.gravityType ? Util.convertApiToSpecificGravity(this.gravity): this.gravity;
    double vcfValue = await this.vcfHandler.getVCF(vcfHeaderModel, specificGravity, this.observedTemp);

    if (null == vcfValue || 0 == vcfValue) {
      _showToast(toastMessage: 'Gravity does not exist for the Temperature', toastColor: Colors.red, icon: Icons.warning);
      return false;
    }
    var grossGal = double.parse(this.grossGallons);
    var vcfCoefficient = vcfValue;
    this.netGallons = (grossGal * vcfCoefficient).round().toString();
    this.vcf_header_description = vcfHeaderModel.description;
    return true;
  }

  Future<bool> _calculateGrossGallons() async {
    if (isNullOrEmpty(this.assetMasterModel?.gallon_capacity)) {
      _showToast(toastMessage: 'Gallon Capacity is empty for this Railcar', toastColor: Colors.red, icon: Icons.warning);
      return false;
    }
    OutageModel outageModel = await unloadVcfrepo.getOutageAmountForTheOutageInches(equipment.assetMasterId);
    if (null == outageModel || outageModel.details == null || outageModel.details.length == 0) {
      _showToast(toastMessage: 'No outage details found for this Railcar', toastColor: Colors.red, icon: Icons.warning);
      return false;
    }
    OutageDetails outageDetailsModel = CommonFunctions().getTheOutageAmount(outageModel, this.openingOutage);
    if (null == outageDetailsModel) {
      _showToast(toastMessage: 'Outage Amount does not exist for the Opening Outage', toastColor: Colors.red, icon: Icons.warning);
      return false;
    }
    var capacity = double.parse(this.assetMasterModel?.gallon_capacity);
    var outageAmount = double.parse(outageDetailsModel.outage_amount);
    this.grossGallons = (capacity - outageAmount).round().toString();
    return true;
  }

  _saveUnloadVcf() async {
    final FormState form = _formKey.currentState;
    if (_checkCalculationDone() && form.validate()) {
      await _persistData();
      Navigator.of(context).pop();
    } else {
      setState(() {});
    }

  }

  _saveUnloadVcfAndMoveToUnload() async {
    final FormState form = _formKey.currentState;
    if (_checkCalculationDone() && form.validate() && _validateOtherDependencies()) {
      await _persistData();
      List<Equipment> selectedEquipments = List<Equipment>();
      selectedEquipments.add(this.equipment);
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => UnLoadRailCar(selectedEquipments, true, this.netGallons),
          ));
    } else {
      setState(() {});
    }

  }

  _persistData() async{
    _setDataToModel();
    await unloadVcfDAO.add(unloadVcfModel: this.unloadVcfModel);
    await NotificationRepository().addNotification(this.unloadVcfModel, APP_CONST.UNLOAD_VCF);
    _showToast(toastMessage: 'Unload VCF saved Successfully', toastColor: Color(0xff7fae1b), icon: Icons.check);
  }

  _checkCalculationDone() {
    if (this.grossGallons != null && this.netGallons != null && this.isCalculationSuccess && this.selectedProductName != null) {
      return true;
    } else {
      _showToast(toastMessage: 'Calculation not done', toastColor: Colors.red, icon: Icons.warning);
      return false;
    }
  }

  _validateOtherDependencies() {
    if (equipment.compartmentList.isEmpty || equipment.compartmentList[0].productName != this.selectedProductName) {
      _showToast(toastMessage: 'Selected Product does not match with Loaded Product',
          toastColor: Colors.red,
          icon: Icons.warning);
      return false;
    }
    return true;
  }

  _setDataToModel() {
    this.unloadVcfModel.gravity = this.gravity;
    this.unloadVcfModel.asset_master_id = this.equipment.assetMasterId;
    this.unloadVcfModel.facility_visit_id = this.equipment.facilityVisitId;
    this.unloadVcfModel.gross_gallons = this.grossGallons;
    this.unloadVcfModel.net_gallons = this.netGallons;
    this.unloadVcfModel.observed_temp = this.observedTemp;
    this.unloadVcfModel.opening_outage = this.openingOutage;
    this.unloadVcfModel.product_id = this.selectedProduct.product_id;
    this.unloadVcfModel.product_name = this.selectedProduct.product_name;
    this.unloadVcfModel.permission_id = env.userProfile.id;
    this.unloadVcfModel.facility_id = env.userProfile.facilityId;
    this.unloadVcfModel.created_dt = this.unloadVcfModel.created_dt != null? this.unloadVcfModel.created_dt : DateTime.now();
    this.unloadVcfModel.updated_dt = DateTime.now();
    this.unloadVcfModel.user = env.userProfile.userName;
    this.unloadVcfModel.operation_type = APP_CONST.UNLOAD_VCF;
    this.unloadVcfModel.transaction_status = APP_CONST.TRANSACTION_STATUS_PENDING;
    this.unloadVcfModel.source = APP_CONST.MOBILITY;
    this.unloadVcfModel.observed_temperature_scale = APP_CONST.F;
    this.unloadVcfModel.vcf_header_description = this.vcf_header_description;
    this.unloadVcfModel.equipment_id = this.equipment.equipmentInitial + ' ' + this.equipment.equipmentNumber;
  }

  onProductChanged(String product) {
    if (product != this.selectedProduct?.product_name) {
      this.isCalculationSuccess = false;
    }
    this.selectedProduct = this.productAttributeList.singleWhere((element) => element.product_name == product);
    this.selectedProductName = this.selectedProduct.product_name;
    if (this.selectedProduct?.gravity != null && "True" == MemoryData.facilityConfiguration.allowToSetGravityFromProductAttributeInUnloadVCF) {
      this.gravity = this.selectedProduct?.gravity;
      this.gravityType = this.selectedProduct?.gravity_type;
      _gravityController.text = this.selectedProduct?.gravity;
    } else {
      this.gravity = null;
      _gravityController.text = '';
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);

    return Scaffold(
      key: globalKey,
      appBar: MainAppBar(),
      drawer: AppDrawer(),
      body: Form(
        key: _formKey,
        autovalidate: autoValidate,
        child: FormUI()
      )
    );
  }

  Widget FormUI() {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    return Builder(
      builder: (context) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(15.0),
                child: CardUI(
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.all(20),
                                      child: Container(
                                          child: ListTile(
                                            leading: GestureDetector(
                                                onTap: () {
                                                  //Navigator.of(context).pop();
                                                  popUpDialog();
                                                },
                                                child: Icon(
                                                  Icons.arrow_back,
                                                )),
                                            title: Text('Unload VCF (${equipment.equipmentInitial+" "+ equipment.equipmentNumber})',
                                                style: TextStyle(
                                                    fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.normal,
                                                    fontFamily: 'Roboto',
                                                    color: Theme.of(context).textTheme.bodyText1.color)),
                                          ))),
                                ],
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.all(20),
                                        child: Container(
                                            child: GestureDetector(
                                              onTap: () {
                                                popUpDialog();
                                                //  Navigator.of(context).pop();
                                              },
                                              child: RichText(
                                                text: TextSpan(style: Theme.of(context).textTheme.bodyText1, children: <InlineSpan>[
                                                  TextSpan(
                                                      text: 'CANCEL',
                                                      style: TextStyle(
                                                          fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                                          fontStyle: FontStyle.normal,
                                                          fontWeight: FontWeight.w500,
                                                          letterSpacing: ScreenUtil().setWidth(1.25),
                                                          fontFamily: 'Roboto',
                                                          color: Color(0xFF3e8aeb)))
                                                ]),
                                              ),
                                            ))),
                                    Padding(
                                        padding: EdgeInsets.only(left: 0, top: 14, right: 24, bottom: 14),
                                        child: Container(
                                          width: ScreenUtil().setWidth(180),
                                          height: ScreenUtil().setHeight(48),
                                          child: FlatButton(
                                              onPressed: _saveUnloadVcf,
                                              child: Text(
                                                'SAVE',
                                                style: TextStyle(
                                                    fontFamily: 'Roboto',
                                                    fontSize: ScreenUtil().setHeight(14),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: ScreenUtil().setWidth(1.25),
                                                    color: Colors.white),
                                              ),
                                              color: Color(0xFF3e8aeb),
                                              textColor: Colors.white,
                                              disabledColor: Color(0xFF3e8aeb),
                                              disabledTextColor: Colors.grey,
                                              splashColor: Color(0xFF3e8aeb)),
                                        )),
                                  ],
                                )
                              ],
                            ),
                          ],
                        ),
                        Divider(
                          thickness: 5.0,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: 20.0,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  // crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('Gallon Capacity',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(this.gallonsCapacityUi,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500))
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 40.0,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  // crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('Tare Weight (lb)',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(this.tareWeightUomCnvrtdUi,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500))
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 40.0,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  // crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('Load Limit (lb)',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(this.loadLimitUi,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500))
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 40.0,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  // crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('DOT Number',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(this.assetMasterModel?.shipping_contain_spec != null ? this.assetMasterModel?.shipping_contain_spec : "",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500))
                                  ],
                                ),
                              ),
                            ]),
                        SizedBox(
                          height: 10.0,
                        ),
                        Divider(
                          thickness: 5.0,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: 20.0,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('LOAD DETAILS',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(20, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.bold)),
                                  ],
                                ),
                              ),
                            ]),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: 20.0,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('Product',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text((!equipment.compartmentList.isEmpty && null != equipment.compartmentList[0].productName) ? equipment.compartmentList[0].productName : "",
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500))
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 80.0,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  // crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('Amount in Gallons',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(this.currentAmountUomCnvrtdUi,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500))
                                  ],
                                ),
                              ),
                            ]),
                        SizedBox(
                          height: 10.0,
                        ),
                        Divider(
                          thickness: 5.0,
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: 20.0,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('Product',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                            letterSpacing: ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    CustomDropdown(
                                      dropdownWidth: 350,
                                      error: (this.autoValidate && this.selectedProductName == null)
                                          ? true : false,
                                      hintText: "Product",
                                      itemList: this.productAttributeList.map((e) => e.product_name).toList(),
                                      SelectedValue: this.selectedProductName,
                                      Ontap: this.canProductDropDownSelect ? this.onProductChanged : null,
                                    )
                                  ],
                                ),
                              ),

                            ]),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: 20.0,
                              ),
                              Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text('Opening Outage (in.)',
                                                    style: TextStyle(
                                                        fontFamily: 'Roboto',
                                                        fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                                        letterSpacing: ScreenUtil().setWidth(0.5),
                                                        fontStyle: FontStyle.normal,
                                                        fontWeight: FontWeight.normal)),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Container(
                                                  width: 213,
                                                  alignment: Alignment.center,
                                                  child: TextFormField(
                                                    controller: _openingOutageController,
                                                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                                                    inputFormatters: [
                                                      FilteringTextInputFormatter.allow(RegExp("^[0-9]*([.][0-9]*){0,1}")),
                                                      LengthLimitingTextInputFormatter(8),
                                                    ],
                                                    onChanged: (str) {
                                                      this.openingOutage = str;
                                                      this.isCalculationSuccess = false;
                                                    },
                                                    validator: (String arg) {
                                                      return isNullOrEmpty(this.openingOutage)? 'This field is mandatory' : null;
                                                    },
                                                    style: TextStyle(
                                                        color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Colors.black : null),
                                                    decoration: InputDecoration(
                                                      contentPadding: EdgeInsets.all(1.0),
                                                      enabledBorder: new OutlineInputBorder(
                                                        borderSide: new BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
                                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                                      ),
                                                      focusedBorder: new OutlineInputBorder(
                                                        borderSide: new BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
                                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                                      ),
                                                      border: new OutlineInputBorder(
                                                        borderSide: new BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
                                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                                      ),
                                                      filled: true,
                                                      fillColor:
                                                      _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                                      hintStyle: TextStyle(
                                                        fontFamily: 'Roboto',
                                                        fontSize: ScreenUtil().setHeight(14),
                                                        fontStyle: FontStyle.normal,
                                                        fontWeight: FontWeight.normal,
                                                        letterSpacing: ScreenUtil().setWidth(0.25),
                                                        color: Colors.grey,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ]),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text('Observed Temp (F)',
                                                    style: TextStyle(
                                                        fontFamily: 'Roboto',
                                                        fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                                        letterSpacing: ScreenUtil().setWidth(0.5),
                                                        fontStyle: FontStyle.normal,
                                                        fontWeight: FontWeight.normal)),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Container(
                                                  width: 213,
                                                  alignment: Alignment.center,
                                                  child: TextFormField(
                                                    controller: _observedTempController,
                                                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                                                    inputFormatters: [
                                                      FilteringTextInputFormatter.digitsOnly,
                                                      LengthLimitingTextInputFormatter(4),
                                                    ],
                                                    onChanged: (str) {
                                                      this.observedTemp = str;
                                                      this.isCalculationSuccess = false;
                                                    },
                                                    validator: (String arg) {
                                                      return isNullOrEmpty(this.observedTemp) ? 'This field is mandatory' : null;
                                                    },
                                                    style: TextStyle(
                                                        color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Colors.black : null),
                                                    decoration: InputDecoration(
                                                      contentPadding: EdgeInsets.all(1.0),
                                                      enabledBorder: new OutlineInputBorder(
                                                        borderSide: new BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
                                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                                      ),
                                                      focusedBorder: new OutlineInputBorder(
                                                        borderSide: new BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
                                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                                      ),
                                                      border: new OutlineInputBorder(
                                                        borderSide: new BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
                                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                                      ),
                                                      filled: true,
                                                      fillColor:
                                                      _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                                      hintStyle: TextStyle(
                                                        fontFamily: 'Roboto',
                                                        fontSize: ScreenUtil().setHeight(14),
                                                        fontStyle: FontStyle.normal,
                                                        fontWeight: FontWeight.normal,
                                                        letterSpacing: ScreenUtil().setWidth(0.25),
                                                        color: Colors.grey,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text("API" == this.gravityType?'API Gravity': 'Specific Gravity',
                                                    style: TextStyle(
                                                        fontFamily: 'Roboto',
                                                        fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                                        letterSpacing: ScreenUtil().setWidth(0.5),
                                                        fontStyle: FontStyle.normal,
                                                        fontWeight: FontWeight.normal)),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Container(
                                                  width: 213,
                                                  alignment: Alignment.center,
                                                  child: TextFormField(
                                                    controller: _gravityController,
                                                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                                                    inputFormatters: [
                                                      FilteringTextInputFormatter.allow(RegExp("^[0-9]*([.][0-9]*){0,1}")),
                                                      LengthLimitingTextInputFormatter(8),
                                                    ],
                                                    onChanged: (str) {
                                                      this.gravity = str;
                                                      this.isCalculationSuccess = false;
                                                    },
                                                    validator: (String arg) {
                                                      return isNullOrEmpty(this.gravity) ? 'This field is mandatory' : null;
                                                    },
                                                    style: TextStyle(
                                                        color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Colors.black : null),
                                                    decoration: InputDecoration(
                                                      contentPadding: EdgeInsets.all(1.0),
                                                      enabledBorder: new OutlineInputBorder(
                                                        borderSide: new BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
                                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                                      ),
                                                      focusedBorder: new OutlineInputBorder(
                                                        borderSide: new BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
                                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                                      ),
                                                      border: new OutlineInputBorder(
                                                        borderSide: new BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
                                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                                      ),
                                                      filled: true,
                                                      fillColor:
                                                      _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                                      hintStyle: TextStyle(
                                                        fontFamily: 'Roboto',
                                                        fontSize: ScreenUtil().setHeight(14),
                                                        fontStyle: FontStyle.normal,
                                                        fontWeight: FontWeight.normal,
                                                        letterSpacing: ScreenUtil().setWidth(0.25),
                                                        color: Colors.grey,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ]),

                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text('Gross Gallons',
                                              style: TextStyle(
                                                  fontFamily: 'Roboto',
                                                  fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.bold)),
                                        ]),
                                    SizedBox(
                                      height: 70.0,
                                    ),
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text('Net Gallons',
                                              style: TextStyle(
                                                  fontFamily: 'Roboto',
                                                  fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.bold)),
                                        ]),

                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(this.grossGallonsUi,
                                              style: TextStyle(
                                                  fontFamily: 'Roboto',
                                                  fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.bold)),
                                        ]),
                                    SizedBox(
                                      height: 70.0,
                                    ),
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(this.netGallonsUi,
                                              style: TextStyle(
                                                  fontFamily: 'Roboto',
                                                  fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.bold)),
                                        ]),

                                  ],
                                ),
                              ),
                            ]),
                        SizedBox(
                          height: 20.0,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: 40.0,
                              ),
                              Container(
                                width: ScreenUtil().setWidth(200),
                                height: ScreenUtil().setHeight(40),
                                child: FlatButton(
                                    onPressed: _calculate,
                                    child: Text(
                                      'Calculate',
                                      style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: ScreenUtil().setWidth(1.25),
                                          color: Color(0xFF3e8aeb)),
                                    ),
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(side: BorderSide(
                                        color: Colors.black38,
                                        width: 1,
                                        style: BorderStyle.solid
                                    ), borderRadius: BorderRadius.circular(1)),
                                    //textColor: Colors.white,
                                    disabledColor: Colors.grey,
                                    disabledTextColor: Colors.black,
                                    splashColor: Color(0xFF3e8aeb)),
                              ),
                            ]),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: ScreenUtil().setWidth(400),
                                height: ScreenUtil().setHeight(48),
                                child: FlatButton(
                                    onPressed: _saveUnloadVcfAndMoveToUnload,
                                    child: Text(
                                      'SAVE & UNLOAD RAILCAR',
                                      style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: ScreenUtil().setWidth(1.25),
                                          color: Colors.white),
                                    ),
                                    color: Color(0xFF3e8aeb),
                                    textColor: Colors.white,
                                    disabledColor: Color(0xFF3e8aeb),
                                    disabledTextColor: Colors.grey,
                                    splashColor: Color(0xFF3e8aeb)),
                              ),
                            ]),

                      ],
                    )),
              ),
            )
          ],
        );
      },
    );
  }
}
