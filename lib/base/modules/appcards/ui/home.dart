import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/user_profile_model.dart';
import 'package:mbase/base/core/components/screen_factory.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/aeiscan/ui/aei_scan_screen.dart';
import 'package:mbase/base/modules/inspection/ui/inspection_landing.dart';
import 'package:mbase/base/modules/listview/listview/ui/listview_screen.dart';
import 'package:mbase/base/modules/listview/yardview/ui/yardview_screen.dart';
import 'package:mbase/base/modules/load_vcf/ui/load_vcf_railcar_selection.dart';
import 'package:mbase/base/modules/seal/seal/ui/seal.dart';
import 'package:mbase/base/modules/switchlist/switchlist/ui/switchlists.dart';
import 'package:mbase/base/modules/unloadvcf/ui/unloadvcf_railcar_select.dart';
import 'package:mbase/env.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {

  @override
  void initState() {
    super.initState();
    setState(() {

    });
  }

  bool checkCardWithUserProfile(UserProfile profile, String screen_id) {
    return profile.screenIdList[profile.facilityId].contains(screen_id);
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    ScreenUtil.init(context, width: 1024, height: 768);

    return env.userProfile != null
        ? Scaffold(
            body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    checkCardWithUserProfile(env.userProfile, "Screen:ListView")
                        ? ConstrainedBox(
                            constraints: BoxConstraints(
                                minWidth: ScreenUtil().setWidth(270),
                                minHeight: ScreenUtil().setHeight(144)),
                            child: GestureDetector(
                              onTap: checkCardWithUserProfile(
                                      env.userProfile, "Screen:ListView")
                                  ? () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(ListViewScreen())));
                                    }
                                  : null,
                              child: Container(
                                constraints: BoxConstraints(
                                    minWidth: ScreenUtil().setWidth(270),
                                    minHeight: ScreenUtil().setHeight(144)),
                                margin: EdgeInsets.only(left: 24, right: 24),
                                decoration: BoxDecoration(
                                  color: Theme.of(context).canvasColor,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image(
                                        width: ScreenUtil().setWidth(59),
                                        fit: BoxFit.cover,
                                        image: _themeChanger
                                                    .getTheme()
                                                    .primaryColor ==
                                                Color(0xff182e42)
                                            ? AssetImage(
                                                'assets/images/list-view-dark.png')
                                            : _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xfff5f5f5)
                                                ? AssetImage(
                                                    'assets/images/list-view-light.png')
                                                : null),
                                    SizedBox(
                                      height: 15.0,
                                    ),
                                    Text("LIST VIEW",
                                        style: TextStyle(
                                            color: Theme.of(context)
                                                .textTheme
                                                // ignore: deprecated_member_use
                                                .headline
                                                .color,
                                            fontSize: ScreenUtil().setSp(16,
                                                allowFontScalingSelf: true),
                                            fontWeight: FontWeight.normal,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing:
                                                ScreenUtil().setWidth(0.5))),
                                  ],
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    checkCardWithUserProfile(env.userProfile, "Screen:ListView")
                        ? ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: ScreenUtil().setWidth(270),
                          minHeight: ScreenUtil().setHeight(144)),
                      child: GestureDetector(
                        onTap: checkCardWithUserProfile(
                            env.userProfile, "Screen:ListView")
                            ? () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(YardViewScreen())));
                        }
                            : null,
                        child: Container(
                          constraints: BoxConstraints(
                              minWidth: ScreenUtil().setWidth(270),
                              minHeight: ScreenUtil().setHeight(144)),
                          margin: EdgeInsets.only(left: 24, right: 24),
                          decoration: BoxDecoration(
                            color: Theme.of(context).canvasColor,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image(
                                  width: ScreenUtil().setWidth(59),
                                  fit: BoxFit.cover,
                                  image: _themeChanger
                                      .getTheme()
                                      .primaryColor ==
                                      Color(0xff182e42)
                                      ? AssetImage(
                                      'assets/images/list-view-dark.png')
                                      : _themeChanger
                                      .getTheme()
                                      .primaryColor ==
                                      Color(0xfff5f5f5)
                                      ? AssetImage(
                                      'assets/images/list-view-light.png')
                                      : null),
                              SizedBox(
                                height: 15.0,
                              ),
                              Text("YARD VIEW",
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                      // ignore: deprecated_member_use
                                          .headline
                                          .color,
                                      fontSize: ScreenUtil().setSp(16,
                                          allowFontScalingSelf: true),
                                      fontWeight: FontWeight.normal,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing:
                                      ScreenUtil().setWidth(0.5))),
                            ],
                          ),
                        ),
                      ),
                    )
                        : Container(),
                    checkCardWithUserProfile(env.userProfile, "Screen:SwitchList")
                        ? ConstrainedBox(
                            constraints: BoxConstraints(
                                minWidth: ScreenUtil().setWidth(270),
                                minHeight: ScreenUtil().setHeight(144)),
                            child: GestureDetector(
                              onTap: checkCardWithUserProfile(
                                      env.userProfile, "Screen:SwitchList")
                                  ? () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ScreenFactory(SwitchListsBlocProvider())));
                                    }
                                  : null,
                              child: Container(
                                constraints: BoxConstraints(
                                  minWidth: ScreenUtil().setWidth(270),
                                  minHeight: ScreenUtil().setHeight(144),
                                ),
                                margin: EdgeInsets.only(left: 24, right: 24),
                                decoration: BoxDecoration(
                                  color: Theme.of(context).canvasColor,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      constraints: BoxConstraints(
                                        minHeight: ScreenUtil().setHeight(37.5),
                                        minWidth: ScreenUtil().setWidth(46),
                                      ),
                                      child: Image(
                                          width: ScreenUtil().setWidth(46),
                                          fit: BoxFit.cover,
                                          image: _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xff182e42)
                                              ? AssetImage(
                                                  'assets/images/switch-list-dark.png')
                                              : _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xfff5f5f5)
                                                  ? AssetImage(
                                                      'assets/images/switch-list-light.png')
                                                  : null),
                                    ),
                                    SizedBox(
                                      height: 17.5,
                                    ),
                                    Text("SWITCH LIST",
                                        style: TextStyle(
                                            color: Theme.of(context)
                                                .textTheme
                                                // ignore: deprecated_member_use
                                                .headline
                                                .color,
                                            fontSize: ScreenUtil().setSp(16,
                                                allowFontScalingSelf: true),
                                            fontWeight: FontWeight.normal,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing:
                                                ScreenUtil().setWidth(0.5))),
                                  ],
                                ),
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(24),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    checkCardWithUserProfile(env.userProfile, "Screen:LoadVCF")
                        ? ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: ScreenUtil().setWidth(270),
                          minHeight: ScreenUtil().setHeight(144)),
                      child: GestureDetector(
                        onTap: true
                            ? () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(AddLoadVCF())));
                        }
                            : null,
                        child: Container(
                          constraints: BoxConstraints(
                              minWidth: ScreenUtil().setWidth(270),
                              minHeight: ScreenUtil().setHeight(144)),
                          margin: EdgeInsets.only(left: 24, right: 24),
                          decoration: BoxDecoration(
                            color: Theme.of(context).canvasColor,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image(
                                  width: ScreenUtil().setWidth(59),
                                  fit: BoxFit.cover,
                                  image: _themeChanger
                                      .getTheme()
                                      .primaryColor ==
                                      Color(0xff182e42)
                                      ? AssetImage(
                                      'assets/images/LoadVCF-dark.png')
                                      : _themeChanger
                                      .getTheme()
                                      .primaryColor ==
                                      Color(0xfff5f5f5)
                                      ? AssetImage(
                                      'assets/images/LoadVCF-light.png')
                                      : null),
                              SizedBox(
                                height: 15.0,
                              ),
                              Text("LOAD VCF",
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                      // ignore: deprecated_member_use
                                          .headline
                                          .color,
                                      fontSize: ScreenUtil().setSp(16,
                                          allowFontScalingSelf: true),
                                      fontWeight: FontWeight.normal,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing:
                                      ScreenUtil().setWidth(0.5))),
                            ],
                          ),
                        ),
                      ),
                    )
                        : Container(),

                    checkCardWithUserProfile(env.userProfile, "Screen:UnloadVCF")
                        ? ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: ScreenUtil().setWidth(270),
                          minHeight: ScreenUtil().setHeight(144)),
                      child: GestureDetector(
                        onTap: checkCardWithUserProfile(
                            env.userProfile, "Screen:UnloadVCF")
                            ? () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ScreenFactory(UnloadVcfSelectRailcar())));
                        }
                            : null,
                        child: Container(
                          constraints: BoxConstraints(
                            minWidth: ScreenUtil().setWidth(270),
                            minHeight: ScreenUtil().setHeight(144),
                          ),
                          margin: EdgeInsets.only(left: 24, right: 24),
                          decoration: BoxDecoration(
                            color: Theme.of(context).canvasColor,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                constraints: BoxConstraints(
                                  minHeight: ScreenUtil().setHeight(37.5),
                                  minWidth: ScreenUtil().setWidth(46),
                                ),
                                child: Image(
                                    width: ScreenUtil().setWidth(46),
                                    fit: BoxFit.cover,
                                    image: _themeChanger
                                        .getTheme()
                                        .primaryColor ==
                                        Color(0xff182e42)
                                        ? AssetImage(
                                        'assets/images/UnloadVCF-dark.png')
                                        : _themeChanger
                                        .getTheme()
                                        .primaryColor ==
                                        Color(0xfff5f5f5)
                                        ? AssetImage(
                                        'assets/images/UnloadVCF-light.png')
                                        : null),
                              ),
                              SizedBox(
                                height: 17.5,
                              ),
                              Text("UNLOAD VCF",
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                      // ignore: deprecated_member_use
                                          .headline
                                          .color,
                                      fontSize: ScreenUtil().setSp(16,
                                          allowFontScalingSelf: true),
                                      fontWeight: FontWeight.normal,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing:
                                      ScreenUtil().setWidth(0.5))),
                            ],
                          ),
                        ),
                      ),
                    )
                        : Container(),

                    checkCardWithUserProfile(env.userProfile, "Screen:Inspection")
                        ? ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: ScreenUtil().setWidth(270),
                          minHeight: ScreenUtil().setHeight(144)),
                      child: GestureDetector(
                        onTap: checkCardWithUserProfile(
                            env.userProfile, "Screen:Inspection")
                            ? () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ScreenFactory( InspectionLandingBlocProvider())));
                        }
                            : null,
                        child: Container(
                          constraints: BoxConstraints(
                              minWidth: ScreenUtil().setWidth(270),
                              minHeight: ScreenUtil().setHeight(144)),
                          margin:
                          EdgeInsets.only(left: 24.0, right: 24.0),
                          decoration: BoxDecoration(
                            color: Theme.of(context).canvasColor,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image(
                                  width: ScreenUtil().setWidth(40),
                                  fit: BoxFit.cover,
                                  image: _themeChanger
                                      .getTheme()
                                      .primaryColor ==
                                      Color(0xff182e42)
                                      ? AssetImage(
                                      'assets/images/inspection-dark.png')
                                      : _themeChanger
                                      .getTheme()
                                      .primaryColor ==
                                      Color(0xfff5f5f5)
                                      ? AssetImage(
                                      'assets/images/inspection-light.png')
                                      : null),
                              SizedBox(
                                height: 20.0,
                              ),
                              Text(
                                "INSPECTION",
                                style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                    // ignore: deprecated_member_use
                                        .headline
                                        .color,
                                    fontSize: ScreenUtil().setSp(16,
                                        allowFontScalingSelf: true),
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing:
                                    ScreenUtil().setWidth(0.5)),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                        : Container(),

                  ],
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(24),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    checkCardWithUserProfile(env.userProfile, "Screen:Seal")
                        ? ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: ScreenUtil().setWidth(270),
                          minHeight: ScreenUtil().setHeight(144)),
                      child: GestureDetector(
                        onTap: checkCardWithUserProfile(
                            env.userProfile, "Screen:Seal")
                            ? () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ScreenFactory(AddSeal())
                              ));
                        }
                            : null,
                        child: Container(
                          constraints: BoxConstraints(
                              minWidth: ScreenUtil().setWidth(270),
                              minHeight: ScreenUtil().setHeight(144)),
                          margin:
                          EdgeInsets.only(left: 24.0, right: 24.0),
                          decoration: BoxDecoration(
                            color: Theme.of(context).canvasColor,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image(
                                  width: ScreenUtil().setWidth(40),
                                  fit: BoxFit.cover,
                                  image: _themeChanger
                                      .getTheme()
                                      .primaryColor ==
                                      Color(0xff182e42)
                                      ? AssetImage(
                                      'assets/images/seal-dark.png')
                                      : _themeChanger
                                      .getTheme()
                                      .primaryColor ==
                                      Color(0xfff5f5f5)
                                      ? AssetImage(
                                      'assets/images/seal-light.png')
                                      : null),
                              SizedBox(
                                height: 20.0,
                              ),
                              Text(
                                "SEAL",
                                style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                    // ignore: deprecated_member_use
                                        .headline
                                        .color,
                                    fontSize: ScreenUtil().setSp(16,
                                        allowFontScalingSelf: true),
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing:
                                    ScreenUtil().setWidth(0.5)),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                        : Container(),

                    checkCardWithUserProfile(env.userProfile, "Screen:AEIScan")
                        ? ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: ScreenUtil().setWidth(270),
                          minHeight: ScreenUtil().setHeight(144)),
                      child: GestureDetector(
                        onTap: checkCardWithUserProfile(
                            env.userProfile, "Screen:AEIScan")
                            ? () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ScreenFactory(AEIScanScreen())
                              ));
                        }
                            : null,
                        child: Container(
                          constraints: BoxConstraints(
                              minWidth: ScreenUtil().setWidth(270),
                              minHeight: ScreenUtil().setHeight(144)),
                          margin:
                          EdgeInsets.only(left: 24.0, right: 24.0),
                          decoration: BoxDecoration(
                            color: Theme.of(context).canvasColor,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image(
                                  width: ScreenUtil().setWidth(40),
                                  fit: BoxFit.cover,
                                  image: _themeChanger
                                      .getTheme()
                                      .primaryColor ==
                                      Color(0xff182e42)
                                      ? AssetImage(
                                      'assets/images/aei-dark.png') //:TODO
                                      : _themeChanger
                                      .getTheme()
                                      .primaryColor ==
                                      Color(0xfff5f5f5)
                                      ? AssetImage(
                                      'assets/images/aei-light.png')  //:TODO
                                      : null),
                              SizedBox(
                                height: 20.0,
                              ),
                              Text(
                                "AEI SCAN",
                                style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                        // ignore: deprecated_member_use
                                        .headline
                                        .color,
                                    fontSize: ScreenUtil().setSp(16,
                                        allowFontScalingSelf: true),
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing:
                                    ScreenUtil().setWidth(0.5)),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                        : Container(),
                  ],
                )
              ],
            ),
          )
        : Text("Loading");
  }
}
