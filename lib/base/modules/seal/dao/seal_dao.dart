import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/repositories/offline_data_repository.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/seal/model/seal_model.dart';
import 'package:mbase/env.dart';
import 'package:uuid/uuid.dart';

class SealDAO extends BaseDao {
  SealDAO() : super("seal");

  Future<void> add({SealModel sealModel}) async {
    sealModel.id = Uuid().v1();

    return OfflineDataRepository.persistToDataStore(sealModel, collection);
  }

  Future<QuerySnapshot> checkExistingSeal(SealModel sealModel) async {
    try {
      QuerySnapshot qShot3 = await BaseQuery().where("equipment_id", isEqualTo: sealModel.equipment_id).where("permission_id", whereIn: [env.userProfile.id, APP_CONST.PUBLIC]).get();
      return qShot3;
    } on Exception {
      throw Exception('Error in checking Equipment Number');
    }
  }

  Future<QuerySnapshot> getSeal({String assetmasterId}) async {
    print("Fetching Railcar Seal By Railcar assetMasterId ......");

    try {
      QuerySnapshot qShot3 = await BaseQuery().where("asset_master_id", isEqualTo: assetmasterId).where("permission_id", whereIn: [env.userProfile.id, APP_CONST.PUBLIC]).get();
      return qShot3;
    } on Exception {
      throw Exception('Error in fetching seals');
    }
  }
}
