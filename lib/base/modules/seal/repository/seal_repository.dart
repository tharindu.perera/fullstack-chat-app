import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/base/modules/seal/dao/seal_dao.dart';
import 'package:mbase/base/modules/seal/model/seal_model.dart';
import 'package:mbase/env.dart';

class SealRepository {
  SealDAO sealDAO = new SealDAO();
  EquipmentDAO equipmentDAO = new EquipmentDAO();
  NotificationRepository notificationRepository = NotificationRepository();

  Future<List<SealModel>> fetchSeals({String assetmasterId}) async {
    QuerySnapshot querySnapshot = await sealDAO.getSeal(assetmasterId: assetmasterId);
    List removableIdlist = [];
    List<SealModel> tempList = querySnapshot?.docs?.map((e) => SealModel.fromMap(e.data())).toList();

    tempList.forEach((element) {
      removableIdlist.add(element.parent_id);
      removableIdlist.add(element.immediateParentId);
    });

    tempList.removeWhere((element) {
      return removableIdlist.contains(element.id);
    });

    return tempList;
  }

  Future<List<Equipment>> fetchRailcars() async {
    List<Equipment> equipment = await equipmentDAO.getRecords();
    return equipment;
  }

  Future<List<SealModel>> checkRailCar({List<String> SealList, Equipment selectedRailCar, String formattedDate}) async {
    SealModel sealModel = new SealModel();
    sealModel.equipment_id = selectedRailCar.activeAssetId;
    sealModel.asset_master_id = selectedRailCar.assetMasterId;
    sealModel.seal_text = SealList;
    sealModel.created_dt = DateTime.now();
    sealModel.updated_dt = DateTime.now();
    sealModel.source = APP_CONST.MOBILITY;
    sealModel.parent_id = null;
    sealModel.permission_id = env.userProfile.id;
    sealModel.facility_id = env.userProfile.facilityId;
    sealModel.operation_type = APP_CONST.ADD_SEAL;
    sealModel.user = env.userProfile.userName;
    sealModel.equipmentInitAndNum = selectedRailCar.equipmentInitial + " " + selectedRailCar.equipmentNumber;
    sealModel.transaction_status = APP_CONST.TRANSACTION_STATUS_PENDING;

    try {
      QuerySnapshot value = await sealDAO.checkExistingSeal(sealModel);
      if (value.docs.isNotEmpty) {
        print("parent seals exist");
        List removableIdlist = [];
        List<SealModel> tempList = value.docs.map((e) {
          return SealModel.fromMap(e.data());
        }).toList();
        tempList.forEach((element) {
          removableIdlist.add(element.parent_id);
          removableIdlist.add(element.immediateParentId);
        });
        tempList.removeWhere((element) {
          return removableIdlist.contains(element.id);
        });
        sealModel.parent_id = tempList[0].parent_id ?? tempList[0].id;
        sealModel.immediateParentId = tempList[0].id;
         sealDAO.add(sealModel: sealModel);
         notificationRepository.addNotification(sealModel, APP_CONST.ADD_SEAL);
      }
      else if (value.docs.isEmpty) {
        print("doc_id doesn't exist");
         sealDAO.add(sealModel: sealModel);
         notificationRepository.addNotification(sealModel, APP_CONST.ADD_SEAL);
      }
    } catch (error) {
      print("Error: ${error}");
    }
  }
}
