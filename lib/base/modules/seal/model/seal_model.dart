import 'dart:convert';

import 'package:mbase/base/core/util/date_time_util.dart';

SealModel sealModelFromMap(String str) => SealModel.fromMap(json.decode(str));

String sealModelToMap(SealModel data) => json.encode(data.toMap());

class SealModel {
  SealModel(
      {this.equipment_id, this.seal_text, this.source, this.id, this.created_dt, this.updated_dt, this.parent_id, this.permission_id, this.asset_master_id, this.facility_id, this.transaction_status});

  String equipment_id;
  List<dynamic> seal_text;
  String source;
  DateTime created_dt;
  DateTime updated_dt;
  String id;
  String asset_master_id;
  String permission_id;
  String parent_id;
  String immediateParentId;
  String facility_id;
  String transaction_status;
  String operation_type;
  String user;
  String equipInit;
  String equipNum;
  String equipmentInitAndNum;

  SealModel.fromMap(Map<String, dynamic> map)
      : equipment_id = map['equipment_id'],
        seal_text = map['seal_text'],
        operation_type = map['operation_type'],
        source = map['source'],
        id = map['id'],
        asset_master_id = map['asset_master_id'],
        user = map['user'],
        facility_id = map['facility_id'],
        created_dt = map['created_dt'] == null ? null : getLocalDateTimeFromUTCStr(map['created_dt']),
        updated_dt = map['updated_dt'] == null ? null : getLocalDateTimeFromUTCStr(map['updated_dt']),
        parent_id = map['parent_id'],
        immediateParentId = map['immediateParentId'],
        permission_id = map['permission_id'],
        transaction_status = map['transaction_status'];

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "asset_master_id": asset_master_id == null ? null : asset_master_id,
        "equipment_id": equipment_id == null ? null : equipment_id,
        "operation_type": operation_type == null ? null : operation_type,
        "user": user == null ? null : user,
        "seal_text": seal_text == null ? null : seal_text,
        "source": source == null ? null : source,
        "transaction_status": transaction_status == null ? null : transaction_status,
        "facility_id": facility_id == null ? null : facility_id,
        "permission_id": permission_id == null ? null : permission_id,
        "parent_id": parent_id == null ? null : parent_id,
        "immediateParentId": immediateParentId == null ? null : immediateParentId,
        "created_dt": created_dt == null ? null : getUTCTimeFromLocalDateTime(created_dt).toString(),
        "updated_dt": updated_dt == null ? null : getUTCTimeFromLocalDateTime(updated_dt).toString(),
      };

  @override
  String toString() {
    return 'SealModel{id: $id, parent_id: $parent_id, immediateParentId: $immediateParentId}';
  }
}
