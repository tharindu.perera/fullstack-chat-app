import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/seal/model/seal_model.dart';
import 'package:mbase/base/modules/seal/repository/seal_repository.dart';
import 'package:provider/provider.dart';

import '../../../../core/components/custom_dialog/custom_dialog.dart';

// ignore: must_be_immutable
class UpdateSeal extends StatefulWidget {
  Equipment equipment;

  UpdateSeal(Equipment equipment) {
    print(equipment == "");
    this.equipment = equipment;
  }

  @override
  _UpdateSealState createState() => _UpdateSealState(equipment);
}

class _UpdateSealState extends State<UpdateSeal> {
  Equipment equipment;
  int selectedRadio;
  var populatedArr = [];
  List<String> selectedSealList = new List<String>();
  final globalKey = GlobalKey<ScaffoldState>();
  final invalidFormatMes =
      SnackBar(content: Text('Can not auto populate. Invalid number format.'));
  final noSealEntered = SnackBar(content: Text('Seal value is not entered.'));
  final sealUpdateSuccess =
      SnackBar(content: Text('Seals updated successfully.'));

  final myController0 = TextEditingController();
  final myController1 = TextEditingController();
  final myController2 = TextEditingController();
  final myController3 = TextEditingController();
  final myController4 = TextEditingController();
  final myController5 = TextEditingController();
  final myController6 = TextEditingController();
  final myController7 = TextEditingController();
  final myController8 = TextEditingController();
  final myController9 = TextEditingController();

  List<TextEditingController> myList = new List<TextEditingController>();
  int changedController = null;
  int counter = 0;
  FToast fToast;
  List<String> initialFieldValues = List<String>();
  List<String> checkValues = List<String>();
  Function alteredListChecker = const ListEquality().equals;
  SealRepository updateSealRepository = SealRepository();

  _UpdateSealState(Equipment equipment) {
    this.equipment = equipment;
  }

  popUpDialog() async {
    checkValues.add(myController0.text ?? "");
    checkValues.add(myController1.text ?? "");
    checkValues.add(myController2.text ?? "");
    checkValues.add(myController3.text ?? "");
    checkValues.add(myController4.text ?? "");
    checkValues.add(myController5.text ?? "");
    checkValues.add(myController6.text ?? "");
    checkValues.add(myController7.text ?? "");
    checkValues.add(myController8.text ?? "");
    checkValues.add(myController9.text ?? "");

    var result = alteredListChecker(initialFieldValues, checkValues);

    if (result) {
      Navigator.of(context).pop();
    } else {
      final action = await CustomDialog.cstmDialog(
          context, "add_railcar", "Unsaved Changes", "");
      if (action[0] == DialogAction.yes) {
        Navigator.pop(context);
      }
    }
  }

  _showToast({String toastMessage, Color toastColor, IconData icon}) {
    fToast.showToast(
      child: CustomToast(
        toastColor: toastColor,
        toastMessage: toastMessage,
        icon: icon,
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);

    myList.insert(0, myController0);
    myList.insert(1, myController1);
    myList.insert(2, myController2);
    myList.insert(3, myController3);
    myList.insert(4, myController4);
    myList.insert(5, myController5);
    myList.insert(6, myController6);
    myList.insert(7, myController7);
    myList.insert(8, myController8);
    myList.insert(9, myController9);

    _getSeals();
  }

  _getSeals() async {
    List<SealModel> sealListFuture = equipment.assetMasterId != null
        ? await updateSealRepository.fetchSeals(
            assetmasterId: equipment.assetMasterId)
        : [];
    if (sealListFuture.length > 0) {
      print("sealListFuture : $sealListFuture");

      myController0.text = sealListFuture[0].seal_text.length >= 1
          ? sealListFuture[0].seal_text[0]
          : "";
      myController1.text = sealListFuture[0].seal_text.length >= 2
          ? sealListFuture[0].seal_text[1]
          : "";
      myController2.text = sealListFuture[0].seal_text.length >= 3
          ? sealListFuture[0].seal_text[2]
          : "";
      myController3.text = sealListFuture[0].seal_text.length >= 4
          ? sealListFuture[0].seal_text[3]
          : "";
      myController4.text = sealListFuture[0].seal_text.length >= 5
          ? sealListFuture[0].seal_text[4]
          : "";
      myController5.text = sealListFuture[0].seal_text.length >= 6
          ? sealListFuture[0].seal_text[5]
          : "";
      myController6.text = sealListFuture[0].seal_text.length >= 7
          ? sealListFuture[0].seal_text[6]
          : "";
      myController7.text = sealListFuture[0].seal_text.length >= 8
          ? sealListFuture[0].seal_text[7]
          : "";
      myController8.text = sealListFuture[0].seal_text.length >= 9
          ? sealListFuture[0].seal_text[8]
          : "";
      myController9.text = sealListFuture[0].seal_text.length >= 10
          ? sealListFuture[0].seal_text[9]
          : "";
    } else {
      initialFieldValues.add(myController0.text ?? "");
      initialFieldValues.add(myController1.text ?? "");
      initialFieldValues.add(myController2.text ?? "");
      initialFieldValues.add(myController3.text ?? "");
      initialFieldValues.add(myController4.text ?? "");
      initialFieldValues.add(myController5.text ?? "");
      initialFieldValues.add(myController6.text ?? "");
      initialFieldValues.add(myController7.text ?? "");
      initialFieldValues.add(myController8.text ?? "");
      initialFieldValues.add(myController9.text ?? "");
    }
  }

  _autoPopulate() {
    final firstNumberExtractionRegex = RegExp(r'\d+');
    final removeWhiteSpaces = RegExp(r"\s+");

    if (changedController == null) {
      _showToast(
          toastColor: Color(0xfff34336),
          toastMessage: "Please enter a seal number to proceed",
          icon: Icons.new_releases);
      return;
    }

    TextEditingController selectedController = myList[changedController];

    if (selectedController.text != null && selectedController.text.isNotEmpty) {
      // skipping if it's the last / 9th seal box

      if (changedController < 9) {
        //int number = int.parse(selectedController.text);
        final trimmedString =
            selectedController.text.replaceAll(new RegExp(r"\s+"), "");
        final extractAllNumbers = firstNumberExtractionRegex
            .allMatches(trimmedString)
            .map((m) => m.group(0));
        final splitStringAfterFirstMatchedNumber =
            trimmedString.split(extractAllNumbers.first.toString());
        final incrementFirstNumber =
            int.parse(extractAllNumbers.first.toString()) + 1;
        final concatAfterIncrement = splitStringAfterFirstMatchedNumber[0] +
            incrementFirstNumber.toString() +
            splitStringAfterFirstMatchedNumber[1];
        //for (int i = (changedController+1); i<=8 ; i++ ) {
        //number++;

        if (concatAfterIncrement.length <= 15) {
          myList[changedController + 1].text =
              (concatAfterIncrement).toString();
          changedController++;
        } else {
          _showToast(
              toastColor: Color(0xfff34336),
              toastMessage: "Character length has exceeded",
              icon: Icons.restore_from_trash);
        }

        //}

      }
    }
  }

  _saveSeal() {
    Loader.show(context);
    int counter = 0;
    if (myController0.text.isNotEmpty ||
        myController1.text.isNotEmpty ||
        myController2.text.isNotEmpty ||
        myController3.text.isNotEmpty ||
        myController4.text.isNotEmpty ||
        myController5.text.isNotEmpty ||
        myController6.text.isNotEmpty ||
        myController7.text.isNotEmpty ||
        myController8.text.isNotEmpty ||
        myController9.text.isNotEmpty) {
      for (TextEditingController textEditingController in myList) {
        if (textEditingController.text.isNotEmpty) {
          selectedSealList.insert(
              counter, textEditingController.text.toString());
        } else if (textEditingController.text.isEmpty) {
          selectedSealList.insert(counter, null);
        }
        counter++;
      }

      final DateFormat formatter = DateFormat('yyyyMMdd HHmmss');
      final String formattedDate = formatter.format(DateTime.now());
      try {
        updateSealRepository
            .checkRailCar(
                selectedRailCar: equipment,
                SealList: selectedSealList,
                formattedDate: formattedDate)
            .then((value) {
          Loader.hide();
          Navigator.pop(context);
          return _showToast(
              toastColor: Color(0xff7fae1b),
              toastMessage: "Seal Added Successfully",
              icon: Icons.check);
        });
      } catch (error) {
        Loader.hide();
        print("error: $error");
      }
    } else {
      Loader.hide();
      _showToast(
          toastColor: Color(0xfff34336),
          toastMessage: "Please enter a seal number to proceed",
          icon: Icons.new_releases);
    }
  }

// Platform messages are asynchronous, so we initialize in an async method.
  Future<void> scanBarcode(TextEditingController selectedEditor) async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.BARCODE);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      print("barcodeScanRes: $barcodeScanRes");
      if (barcodeScanRes != '-1') {
        selectedEditor.text = barcodeScanRes;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    return Scaffold(
      key: globalKey,
      appBar: MainAppBar(),
      drawer: AppDrawer(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: CardUI(
                  content: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Container(
                                    child: ListTile(
                                  leading: GestureDetector(
                                      onTap: () {
                                        //Navigator.of(context).pop();
                                        popUpDialog();
                                      },
                                      child: Icon(
                                        Icons.arrow_back,
                                      )),
                                  title: Text(
                                      'Update Seal (${equipment.equipmentInitial + " " + equipment.equipmentNumber})',
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(24,
                                              allowFontScalingSelf: true),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: 'Roboto',
                                          color: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .color)),
                                ))),
                          ],
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(20),
                                  child: Container(
                                      child: GestureDetector(
                                    onTap: () {
                                      popUpDialog();
                                      //  Navigator.of(context).pop();
                                    },
                                    child: RichText(
                                      text: TextSpan(
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                          children: <InlineSpan>[
                                            TextSpan(
                                                text: 'CANCEL',
                                                style: TextStyle(
                                                    fontSize: ScreenUtil().setSp(
                                                        14,
                                                        allowFontScalingSelf:
                                                            true),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(1.25),
                                                    fontFamily: 'Roboto',
                                                    color: Color(0xFF3e8aeb)))
                                          ]),
                                    ),
                                  ))),
                              Padding(
                                  padding: EdgeInsets.only(
                                      left: 0, top: 14, right: 24, bottom: 14),
                                  child: Container(
                                    width: ScreenUtil().setWidth(180),
                                    height: ScreenUtil().setHeight(48),
                                    child: FlatButton(
                                        onPressed: _saveSeal,
                                        child: Text(
                                          'UPDATE SEAL',
                                          style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize:
                                                  ScreenUtil().setHeight(14),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing:
                                                  ScreenUtil().setWidth(1.25),
                                              color: Colors.white),
                                        ),
                                        color: Color(0xFF3e8aeb),
                                        textColor: Colors.white,
                                        disabledColor: Color(0xFF3e8aeb),
                                        disabledTextColor: Colors.grey,
                                        splashColor: Color(0xFF3e8aeb)),
                                  )),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                  Divider(
                    thickness: 5.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Wrap(
                                direction: Axis.horizontal,
                                spacing: 10,
                                runSpacing: 5,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(214),
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: TextField(
                                            maxLength: 15,
                                            controller: myController0,
                                            onTap: () {
                                              changedController = 0;
                                            },
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .bodyText1
                                                    .color),
                                            decoration: InputDecoration(
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(1.0),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.black38),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.black38,
                                                    width: 1),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintText: 'XXXX',
                                              hintStyle: TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          trailing: GestureDetector(
                                            onTap: () {
                                              scanBarcode(myController0);
                                            },
                                            child: Image.asset(
                                                "assets/images/barcode.png",
                                                width:
                                                    ScreenUtil().setWidth(40),
                                                height:
                                                    ScreenUtil().setHeight(40)),
                                          ),
                                        )),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(214),
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: TextField(
                                            maxLength: 15,
                                            controller: myController1,
                                            onTap: () {
                                              changedController = 1;
                                            },
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .bodyText1
                                                    .color),
                                            decoration: InputDecoration(
                                              //prefixIcon: Icon(Icons.search),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(1.0),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.black38),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.black38,
                                                    width: 1),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintText: 'XXXX',
                                              hintStyle: TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          trailing: GestureDetector(
                                            onTap: () {
                                              scanBarcode(myController1);
                                            },
                                            child: Image.asset(
                                                "assets/images/barcode.png",
                                                width:
                                                    ScreenUtil().setWidth(40),
                                                height:
                                                    ScreenUtil().setHeight(40)),
                                          ),
                                        )),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(214),
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: TextField(
                                            maxLength: 15,
                                            controller: myController2,
                                            onTap: () {
                                              changedController = 2;
                                            },
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color),
                                            decoration: InputDecoration(
                                              //prefixIcon: Icon(Icons.search),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(1.0),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.black38),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.black38,
                                                    width: 1),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintText: 'XXXX',
                                              hintStyle: TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          trailing: GestureDetector(
                                            onTap: () {
                                              scanBarcode(myController2);
                                            },
                                            child: Image.asset(
                                                "assets/images/barcode.png",
                                                width:
                                                    ScreenUtil().setWidth(40),
                                                height:
                                                    ScreenUtil().setHeight(40)),
                                          ),
                                        )),
                                  ),
                                ])),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Wrap(
                                direction: Axis.horizontal,
                                spacing: 10,
                                runSpacing: 5,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(214),
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: TextField(
                                            maxLength: 15,
                                            controller: myController3,
                                            onTap: () {
                                              changedController = 3;
                                            },
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color),
                                            decoration: InputDecoration(
                                              //prefixIcon: Icon(Icons.search),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(1.0),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.black38),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.black38,
                                                    width: 1),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintText: 'XXXX',
                                              hintStyle: TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          trailing: GestureDetector(
                                            onTap: () {
                                              scanBarcode(myController3);
                                            },
                                            child: Image.asset(
                                                "assets/images/barcode.png",
                                                width:
                                                    ScreenUtil().setWidth(40),
                                                height:
                                                    ScreenUtil().setHeight(40)),
                                          ),
                                        )),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(214),
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: TextField(
                                            maxLength: 15,
                                            controller: myController4,
                                            onTap: () {
                                              changedController = 4;
                                            },
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color),
                                            decoration: InputDecoration(
                                              //prefixIcon: Icon(Icons.search),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(1.0),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.black38),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.black38,
                                                    width: 1),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintText: 'XXXX',
                                              hintStyle: TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          trailing: GestureDetector(
                                            onTap: () {
                                              scanBarcode(myController4);
                                            },
                                            child: Image.asset(
                                                "assets/images/barcode.png",
                                                width:
                                                    ScreenUtil().setWidth(40),
                                                height:
                                                    ScreenUtil().setHeight(40)),
                                          ),
                                        )),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(214),
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: TextField(
                                            maxLength: 15,
                                            controller: myController5,
                                            onTap: () {
                                              changedController = 5;
                                            },
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color),
                                            decoration: InputDecoration(
                                              //prefixIcon: Icon(Icons.search),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(1.0),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.black38),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.black38,
                                                    width: 1),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintText: 'XXXX',
                                              hintStyle: TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          trailing: GestureDetector(
                                            onTap: () {
                                              scanBarcode(myController5);
                                            },
                                            child: Image.asset(
                                                "assets/images/barcode.png",
                                                width:
                                                    ScreenUtil().setWidth(40),
                                                height:
                                                    ScreenUtil().setHeight(40)),
                                          ),
                                        )),
                                  ),
                                ]))
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Wrap(
                                direction: Axis.horizontal,
                                spacing: 10,
                                runSpacing: 5,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(214),
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: TextField(
                                            maxLength: 15,
                                            controller: myController6,
                                            onTap: () {
                                              changedController = 6;
                                            },
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color),
                                            decoration: InputDecoration(
                                              //prefixIcon: Icon(Icons.search),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(1.0),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.black38),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.black38,
                                                    width: 1),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintText: 'XXXX',
                                              hintStyle: TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          trailing: GestureDetector(
                                            onTap: () {
                                              scanBarcode(myController6);
                                            },
                                            child: Image.asset(
                                                "assets/images/barcode.png",
                                                width:
                                                    ScreenUtil().setWidth(40),
                                                height:
                                                    ScreenUtil().setHeight(40)),
                                          ),
                                        )),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(214),
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: TextField(
                                            maxLength: 15,
                                            controller: myController7,
                                            onTap: () {
                                              changedController = 7;
                                            },
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .body1
                                                    .color),
                                            decoration: InputDecoration(
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(1.0),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.black38),
                                              ),
                                              //prefixIcon: Icon(Icons.search),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.black38,
                                                    width: 1),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintText: 'XXXX',
                                              hintStyle: TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          trailing: GestureDetector(
                                            onTap: () {
                                              scanBarcode(myController7);
                                            },
                                            child: Image.asset(
                                                "assets/images/barcode.png",
                                                width:
                                                    ScreenUtil().setWidth(40),
                                                height:
                                                    ScreenUtil().setHeight(40)),
                                          ),
                                        )),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(214),
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: TextField(
                                            maxLength: 15,
                                            controller: myController8,
                                            onTap: () {
                                              changedController = 8;
                                            },
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color),
                                            decoration: InputDecoration(
                                              //prefixIcon: Icon(Icons.search),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(1.0),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.black38),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.black38,
                                                    width: 1),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintText: 'XXXX',
                                              hintStyle: TextStyle(
                                                fontSize: 18.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          trailing: GestureDetector(
                                            onTap: () {
                                              scanBarcode(myController8);
                                            },
                                            child: Image.asset(
                                                "assets/images/barcode.png",
                                                width:
                                                    ScreenUtil().setWidth(40),
                                                height:
                                                    ScreenUtil().setHeight(40)),
                                          ),
                                        )),
                                  ),
                                ]))
                      ]),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: Container(
                            width: ScreenUtil().setWidth(214),
                            padding: EdgeInsets.symmetric(vertical: 4.0),
                            alignment: Alignment.center,
                            child: ListTile(
                              title: TextField(
                                maxLength: 15,
                                controller: myController9,
                                onTap: () {
                                  changedController = 9;
                                },
                                style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                        // ignore: deprecated_member_use
                                        .body1
                                        .color),
                                decoration: InputDecoration(
                                  //prefixIcon: Icon(Icons.search),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(1.0),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.black38),
                                  ),
                                  enabledBorder: const OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.black38, width: 1),
                                  ),
                                  filled: true,
                                  fillColor: _themeChanger
                                              .getTheme()
                                              .primaryColor ==
                                          Color(0xff182e42)
                                      ? Color(0xff172636)
                                      : _themeChanger.getTheme().primaryColor ==
                                              Color(0xfff5f5f5)
                                          ? Color(0xfff5f5f5)
                                          : null,
                                  hintText: 'XXXX',
                                  hintStyle: TextStyle(
                                    fontSize: 18.0,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                              trailing: GestureDetector(
                                onTap: () {
                                  scanBarcode(myController9);
                                },
                                child: Image.asset("assets/images/barcode.png",
                                    width: ScreenUtil().setWidth(40),
                                    height: ScreenUtil().setHeight(40)),
                              ),
                            )),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 50),
                        child: Container(
                          width: ScreenUtil().setWidth(293),
                          //color:Colors.white,

                          child: FlatButton(
                              onPressed: _autoPopulate,
                              child: Text(
                                'AUTO POPULATE',
                                style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: ScreenUtil().setHeight(14),
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500,
                                    letterSpacing: ScreenUtil().setWidth(1.25),
                                    color: Color(0xFF3e8aeb)),
                              ),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: Colors.black38,
                                      width: 1,
                                      style: BorderStyle.solid),
                                  borderRadius: BorderRadius.circular(1)),
                              //textColor: Colors.white,
                              disabledColor: Colors.grey,
                              disabledTextColor: Colors.black,
                              splashColor: Color(0xFF3e8aeb)),
                        ),
                      ),
                    ],
                  ),
                ],
              )),
            ),
          )
        ],
      ),
    );
  }
}
