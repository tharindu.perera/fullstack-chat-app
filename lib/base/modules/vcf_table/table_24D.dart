import 'dart:math';

import 'package:mbase/base/core/util/util_functions.dart';

class Table24D {
  double densityOfWater = 999.016;
  double temperatureShiftValue = 0.013749795;
  double k0 = 0;
  double k1 = 0.34878;
  double k2 = 0;


  double getVCF(double t, double gravity) {

    double density = Util.truncateToHundreths( gravity * densityOfWater, 4) + 0.0001;

    double A = temperatureShiftValue/2*(((k0/density)+k1)*(1/density)+k2);
    double B = (2*k0 + k1*density) / (k0 + (k1 + k2*density)*density);

    double baseDensityShift = density*(1+(exp(A*(1+0.8*A))-1)/(1+A*(1+1.6*A)*B));

    double temperatureShiftITS90 = (t - 32) / 1.8;

    double ct = temperatureShiftITS90/630;

    double scaledTemperature = (-0.148759+( -0.267408 + ( 1.08076 + ( 1.269056 + ( -4.089591 + (-1.871251 + (7.438081 + -3.536296*ct)*ct )*ct )*ct )*ct )*ct )*ct )*ct;

    double temperatureIPTS68 = temperatureShiftITS90 - scaledTemperature;

    double alternativeTemp = 1.8* temperatureIPTS68 + 32;

    double tempCTL = alternativeTemp - 60.0068749;

    double thermalExpansionFactor = ((k0/baseDensityShift)+k1)*(1/baseDensityShift)+k2;

    double linearCTL = -thermalExpansionFactor*tempCTL*( 1 + 0.8*thermalExpansionFactor*( tempCTL + temperatureShiftValue));

    double ctl = exp(linearCTL);

    return Util.decimalPoints(ctl, 4);
  }
}
