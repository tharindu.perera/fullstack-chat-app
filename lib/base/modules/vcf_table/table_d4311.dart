import 'dart:math';

import 'package:mbase/base/core/util/util_functions.dart';

class TableD4311 {
  List<double> tableA = [1.02113262, 3.54898812, 4.49881];
  List<double> tableB = [1.02413769, 4.0641418, 6.79176];

  double getVCF(double t, double p) {
    try {
      double ctl = 0;
      if (p >= 0.967) {
       ctl = tableA.elementAt(0) -   tableA.elementAt(1)*t*pow(10, -4) + tableA.elementAt(2)*pow(10, -8)*pow(t, 2);
      } else {
        ctl = tableB.elementAt(0) -   tableB.elementAt(1)*t*pow(10, -4) + tableB.elementAt(2)*pow(10, -8)*pow(t, 2);
      }
      return Util.decimalPoints(ctl, 4);
    } catch (error) {
      return null;
    }
  }
}
