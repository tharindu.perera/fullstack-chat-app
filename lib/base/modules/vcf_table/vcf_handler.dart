import 'package:mbase/base/core/common_base/model/vcf_header_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/util/nullCheckUtil.dart';
import 'package:mbase/base/modules/vcf_table/table_24D.dart';
import 'package:mbase/base/modules/vcf_table/table_24E.dart';
import 'package:mbase/base/modules/vcf_table/table_d1555.dart';
import 'package:mbase/base/modules/vcf_table/table_d4311.dart';
import 'package:mbase/base/modules/vcf_table/table_sulfur.dart';

class VCFHandler {
  TableD1555 tableD1555 = new TableD1555();
  TableD4311 tableD4311 = new TableD4311();
  Table24D table24d = new Table24D();
  Table24E table24e = new Table24E();
  TableSulfur sulfurTable = new TableSulfur();

  getVCF(VCFHeaderModel vcfHeader, String gravity, String observed_temp) async{
    if ( isNullOrEmpty(vcfHeader?.description) ||isNullOrEmpty(gravity) || isNullOrEmpty(observed_temp) ){
      return null;
    }

    try {
      switch (vcfHeader.description) {
        case APP_CONST.TABLE_24E:
          return await table24e.getVCF(vcfHeader.id, gravity, observed_temp);
        case APP_CONST.Benzene_D1555:
          return tableD1555.getBenzeneVCF(double.parse(observed_temp));
        case APP_CONST.Cumene_D1555:
          return tableD1555.getCumeneVCF(double.parse(observed_temp));
        case APP_CONST.Cyclohexane_D1555:
          return tableD1555.getCyclohexaneVCF(double.parse(observed_temp));
        case APP_CONST.Ethylbenzene_D1555:
          return tableD1555.getEthylbenzeneVCF(double.parse(observed_temp));
        case APP_CONST.Styrene_D1555:
          return tableD1555.getStyreneVCF(double.parse(observed_temp));
        case APP_CONST.Toluene_D1555:
          return tableD1555.getTolueneVCF(double.parse(observed_temp));
        case APP_CONST.m_XyleneA_D1555:
          return tableD1555.getmXyleneAVCF(double.parse(observed_temp));
        case APP_CONST.o_Xylene_D1555:
          return tableD1555.getoXyleneAVCF(double.parse(observed_temp));
        case APP_CONST.p_Xylene_D1555:
          return tableD1555.getpXyleneAVCF(double.parse(observed_temp));
        case APP_CONST.Hyd300350_D1555:
          return tableD1555.getHyd300350VCF(double.parse(observed_temp));
        case APP_CONST.Hyd350400_D1555:
          return tableD1555.getHyd350400VCF(double.parse(observed_temp));
        case APP_CONST.TABLE_SULFUR:
          return sulfurTable.getVCF(double.parse(observed_temp));
        case APP_CONST.TABLE_D4311:
          return tableD4311.getVCF(
              double.parse(observed_temp), double.parse(gravity));
        case APP_CONST.TABLE_24D:
          return table24d.getVCF(
              double.parse(observed_temp), double.parse(gravity));
      }
    } catch (error) {
      return null;
    }
  }
}
