import 'dart:math';

import 'package:mbase/base/core/util/util_functions.dart';

class TableSulfur {
  double temperatureShift = 0.01374979547;
  double baseTemperature = 60.0068749;
  double alphaT = 0.000565490836480641;
  double betaC = 0.000237324937065082;
  double thetaC = 271.160236018699;
  double gammaC = 1.02926220353589e-06;

  double getVCF(double t) {
    try {
      double diff = t - baseTemperature;
      double diffAndShift = diff + temperatureShift;
      double tempMinusThetaC = t - thetaC;
      double linearCTL = -alphaT*diff*(1+0.8*alphaT*diffAndShift) + betaC*tempMinusThetaC + gammaC * pow(tempMinusThetaC, 2);
      double ctl = exp(linearCTL);
      return Util.decimalPoints(ctl, 4);
    } catch (e) {
      return null;
    }
  }
}