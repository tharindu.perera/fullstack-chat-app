import 'package:mbase/base/core/common_base/dao/vcf_detail_dao.dart';
import 'package:mbase/base/core/common_base/model/vcf_header_model.dart';
import 'package:mbase/base/core/util/common_vcf.dart';
import 'package:mbase/base/core/util/util_functions.dart';

class Table24E {
  VCFDetailDao vcfDetailDao = new VCFDetailDao();

  Future<double> getVCF(String headerId, String gravity, String observedTemp) async {
    try {
      List<VCFDetailModel> vcfDetailList = await vcfDetailDao.fetchAllByTemperatureLimit(observedTemp);
      List<VCFDetailModel> temp = vcfDetailList.where((element) => element.vcf_header_id == headerId).toList();
      VCFDetailModel vcfDetailsModel = CommonFunctions().getTheVcfDetails(temp, gravity, observedTemp);
      return Util.decimalPointsStr(vcfDetailsModel.vcf, 4);
    } catch (error) {
      return null;
    }
  }
}
