import 'dart:math';

import 'package:mbase/base/core/util/util_functions.dart';

class TableD1555 {

  getBenzeneVCF(double t) {
    List<double> constants = [
      1.038382492,
      -0.00062307,
      -2.8505e-07,
      1.2692e-10,
      0
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  getCumeneVCF(double t) {
    List<double> constants = [
      1.032401114,
      -0.00053445,
      -9.5067e-08,
      3.6272e-11,
      0
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  getCyclohexaneVCF(double t) {
    List<double> constants = [
      1.039337296,
      -0.00064728,
      -1.4582e-07,
      1.03538e-10,
      0
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  getEthylbenzeneVCF(double t) {
    List<double> constants = [
      1.033346632,
      -0.00055243,
      8.37035e-10,
      -1.2692e-09,
      5.55061e-12
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  getStyreneVCF(double t) {
    List<double> constants = [
      1.032227515,
      -0.00053444,
      -4.4323e-08,
      0,
      0
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  getTolueneVCF(double t) {
    List<double> constants = [
      1.035323647,
      -0.00058887,
      2.46508e-09,
      -7.2802e-12,
      0
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  getmXyleneAVCF(double t) {
    List<double> constants = [
      1.031887514,
      -0.00052326,
      -1.3253e-07,
      -7.3596e-11,
      0
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  getoXyleneAVCF(double t) {
    List<double> constants = [
      1.031436449,
      -0.00052302,
      -2.5217e-09,
      -2.1384e-10,
      0
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  getpXyleneAVCF(double t) {
    List<double> constants = [
      1.032307,
      -0.00052815,
      -1.8416e-07,
      1.89256e-10,
      0
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  getHyd300350VCF(double t) {
    List<double> constants = [
      1.031118,
      -0.00051827,
      -3.5109e-09,
      -1.9836e-11,
      0
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  getHyd350400VCF(double t) {
    List<double> constants = [
      1.029099,
      -0.00048287,
      -3.7692e-08,
      3.78575e-11,
      0
    ];
    double vcf = _getVCF(constants, t);
    return Util.decimalPoints(vcf, 4);
  }

  double _getVCF(List<double> constants, double t) {
    double top = (constants.elementAt(0) +
        constants.elementAt(1) * t +
        constants.elementAt(2) * pow(t, 2) +
        constants.elementAt(3) * pow(t, 3) +
        constants.elementAt(4) * pow(t, 4));
    double bottom = (constants.elementAt(0) +
        constants.elementAt(1) * 60 +
        constants.elementAt(2) * pow(60, 2) +
        constants.elementAt(3) * pow(60, 3) +
        constants.elementAt(4) * pow(60, 4));
    return top / bottom;
  }
}
