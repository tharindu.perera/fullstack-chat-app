import 'package:flutter/material.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/yard_view/yard_view_ds.dart';

@immutable
abstract class YardViewEvent {
  YardViewEvent();
}

class YardViewInitialDataLoad extends YardViewEvent {
  YardViewInitialDataLoad() : super();
}

// ignore: must_be_immutable
class YardViewFetchDataWithCriteria extends YardViewEvent {
  Map<String, dynamic> criteria;
  YardViewFetchDataWithCriteria(this.criteria) : super();
}

class YardViewFetchDataWithLastSetCriteria extends YardViewEvent {
  YardViewFetchDataWithLastSetCriteria() : super();
}

class YardViewRefreshData extends YardViewEvent {
  YardViewDataSource ds;
  YardViewRefreshData(this.ds) : super();
}
