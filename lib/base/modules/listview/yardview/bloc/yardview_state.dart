import 'package:mbase/base/modules/listview/yardview/ui/components/yard_view/yard_view_ds.dart';

abstract class YardViewState {
  YardViewDataSource dataSource = YardViewDataSource([]);
  YardViewState(this.dataSource);

  @override
  String toString() {
    return this.runtimeType.toString();
  }
}

class YardViewInitState extends YardViewState {
  YardViewInitState({YardViewDataSource dataSource})
      : super(dataSource ?? YardViewDataSource([]));
}

class YardViewInProgress extends YardViewState {
  YardViewInProgress({YardViewDataSource dataSource})
      : super(dataSource ?? YardViewDataSource([]));
}

class YardViewDataLoaded extends YardViewState {
  YardViewDataLoaded({YardViewDataSource dataSource})
      : super(dataSource ?? YardViewDataSource([]));
}
