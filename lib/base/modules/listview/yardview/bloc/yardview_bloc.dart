import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_state.dart';
import 'package:mbase/base/modules/listview/yardview/repository/yardview_repository.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/yard_view/yard_view_ds.dart';

class YardViewBloc extends Bloc<YardViewEvent, YardViewState> {
  YardViewRepository _repository;
  Map<String, dynamic> _lastSearchCriteria;
  StreamSubscription streamSubscription;
  bool ascending = true;
  int sortColumnIndex = 0;

  YardViewBloc(this._repository) : super(YardViewInitState()) {}

  @override
  Stream<YardViewState> mapEventToState(YardViewEvent event) async* {
    if (event is YardViewInitialDataLoad) {
      yield* _mapYardViewFetchDataWithCriteria(null);
    }

    if (event is YardViewFetchDataWithCriteria) {
      this._lastSearchCriteria = event.criteria;
      yield* _mapYardViewFetchDataWithCriteria(event.criteria);
    }

    if (event is YardViewFetchDataWithLastSetCriteria) {
      yield* _mapYardViewFetchDataWithCriteria(this._lastSearchCriteria);
    }

    if (event is YardViewRefreshData) {
      yield YardViewDataLoaded(dataSource: event.ds);
    }
  }

  Stream<YardViewState> _mapYardViewFetchDataWithCriteria(criteria,
      {ascending: true, sortColumnIndex: 0}) async* {
    this.ascending = ascending;
    this.sortColumnIndex = sortColumnIndex;
    yield YardViewInProgress();
    streamSubscription?.cancel();
    streamSubscription = _repository
        .getRecordByCriteria(criteria, ascending, sortColumnIndex)
        .listen((event) async {
      add(YardViewRefreshData(YardViewDataSource(event)));
    });
  }
}
