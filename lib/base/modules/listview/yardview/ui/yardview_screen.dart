import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/screen_factory.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/add_railcar/ui/add_railcar_screen.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_state.dart';
import 'package:mbase/base/modules/listview/yardview/repository/yardview_repository.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/action_button.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/color_coding_section/color_coding_view.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/search_section/search_view.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/yard_view/yard_view.dart';

class YardViewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<YardViewBloc>(
      create: (context) =>
          YardViewBloc(YardViewRepository())..add(YardViewInitialDataLoad()),
      child:
          BlocBuilder<YardViewBloc, YardViewState>(builder: (context, state) {
        return Scaffold(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(24),
                        vertical: ScreenUtil().setHeight(24)),
                    child: CardUI(
                      content: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: ScreenUtil().setWidth(24)),
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: ScreenUtil().setHeight(24)),
                                Row(children: [
                                  ColorCodingSection(),
                                  SizedBox(width: ScreenUtil().setWidth(23.5)),
                                  ActionButtn()
                                ]),
                                SizedBox(
                                  width: ScreenUtil().setWidth(928),
                                  child: Divider(
                                    thickness: ScreenUtil().setWidth(1.5),
                                    color: Colors.black,
                                  ),
                                ),
                                Row(children: [
                                  SearchSection(),
                                  SizedBox(width: ScreenUtil().setWidth(23.5))
                                ]),
                                (state is YardViewInProgress)
                                    ? CircularProgressIndicator()
                                    : YardViewBlocBuilder(
                                        BlocProvider.of<YardViewBloc>(context)
                                            .state
                                            .dataSource
                                            .inputList)
                              ],
                            ),
                          ),
                        ],
                      ),
                      // ),
                    ),
                  ),
                ),
              ],
            ),
            floatingActionButton: FloatingActionButton(
              backgroundColor: Color(0xFF508be4),
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () {
                return Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => BlocProvider.value(
                        value: BlocProvider.of<YardViewBloc>(context),
                        child: ScreenFactory(AddCar(APP_CONST.YARD_VIEW)),
                      ),
                    ));
              },
            ));
      }),
    );
  }
}
