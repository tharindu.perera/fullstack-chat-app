import 'dart:collection';

import "package:collection/collection.dart";
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/railcar_box_with_spot.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/railcar_box_without_spot.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/yard_view/yard_view_ds.dart';
import 'package:provider/provider.dart';

class YardViewBlocBuilder extends StatefulWidget {
  YardViewBlocBuilder(List<Equipment> equipments) {
    this.equipments = equipments;
  }
  List<Equipment> equipments = List<Equipment>();

  @override
  _YardViewState createState() => _YardViewState();
}

class _YardViewState extends State<YardViewBlocBuilder> {
  int tracks_count = 0;
  int yard_count = 0;
  int cars_count = 0;
  List<TrackModel> trackList = [];
  LinkedHashMap spotMap = new LinkedHashMap<String, List<SpotModel>>();
  List<YardModel> yardList = [];
  LinkedHashMap hashMap = new LinkedHashMap<String, Equipment>();

  @override
  void initState() {
    super.initState();
    filterRailcars(widget.equipments);
  }

  filterRailcars(List<Equipment> cars) {
    hashMap = groupBy(cars, (Equipment e) {
      return e.trackId;
    }) as LinkedHashMap;

    this.trackList =
        BlocProvider.of<YardViewBloc>(context).state.dataSource.trackList;
    this.yardList =
        BlocProvider.of<YardViewBloc>(context).state.dataSource.yardList;

    List<TrackModel> sortedTrackList = new List();
    for(YardModel yard in this.yardList) {
      List<TrackModel> temp = this.trackList.where((element) => element.yardId == yard.id).toList();
      sortedTrackList.addAll(temp);
    }
    this.trackList = sortedTrackList;

    this.trackList.forEach((element) {
      if (element.spotList != null && element.spotList.isNotEmpty) {
        element.spotList.sort((a, b) => double.parse(a.sequence).compareTo(double.parse(b.sequence)));
        spotMap[element.id] = element.spotList;
      }
    });
    yard_count = this.yardList != null ? this.yardList.length : 0;
    tracks_count = this.trackList != null ? this.trackList.length : 0;
    cars_count = cars.length;

    // tracks_count = hashMap.length;
    // trackList = hashMap.keys.toList();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    YardViewBloc yardViewBloc = BlocProvider.of<YardViewBloc>(context);
    YardViewDataSource yardViewDataSource = yardViewBloc.state.dataSource;
    yardViewDataSource.context = context;
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    return Container(
        child: SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _buildRows(tracks_count, context),
      ),
    ));
  }

  List<Widget> _buildRows(int count, BuildContext context) {
    return List.generate(
      count,
      (index) => _buildSingleTrack(index, context),
    );
  }

  Widget _buildSingleTrack(int index, BuildContext context) {
    TrackModel track = this.trackList?.elementAt(index);
    YardModel yard = this.yardList.firstWhere(
        (element) => element?.id == track?.yardId,
        orElse: () => null);

    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildHeaderCell(yard, track, context),
            _buildTailRow(index, trackList.elementAt(index).id, context)
          ]),
    );
  }

  Widget _buildHeaderCell(
      YardModel yard, TrackModel track, BuildContext context) {
    return DefaultTextStyle(
      style: TextStyle(color: Colors.white, fontSize: 18),
      child: Container(
        height: 190,
        width: 200,
        color: Color(0xff274060),
        child: Column(
          children: [
            SizedBox(height: ScreenUtil().setHeight(50)),
            Text(yard?.yardName != null ? yard.yardName : 'Yard'),
            SizedBox(height: ScreenUtil().setHeight(5)),
            Text(track?.trackName),
          ],
        ),
      ),
    );
  }

  Widget _buildTailRow(int index, String trackId, BuildContext context) {
    List<SpotModel> spots = spotMap[trackId];

    if (spots == null || spots.isEmpty) {
      return _buildRailCars(index, trackId, context);
    } else {
      return _buildRailCarsWithSpots(index, trackId, context);
    }
  }

  Widget _buildRailCars(int index, String trackId, BuildContext context) {
    List<Equipment> railcars = hashMap[trackId];

    //sort by sequence no
    if (railcars?.length > 0) {
      railcars.sort((a, b) {
        int result;
        if (null == a.sequenceRailcar) {
          result = 1;
        } else if (null == b.sequenceRailcar) {
          result = -1;
        } else {
          result = int.parse(a.sequenceRailcar).compareTo(int.parse(b.sequenceRailcar));
        }
        return result;
      });
    }

    return Flexible(
      child: SizedBox(
        height: 150,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: hashMap[trackId].length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) =>
              RailCarBoxWithoutSpot(equipment: railcars.elementAt(index)),
        ),
      ),
    );
  }

  Widget _buildRailCarsWithSpots(
      int index, String trackId, BuildContext context) {
    List<SpotModel> spots = spotMap[trackId];
    List<Equipment> railcars = hashMap[trackId];
    LinkedHashMap<SpotModel, Equipment> carMap = new LinkedHashMap();

    if (spots?.length > 0) {
      spots.forEach((spot) {
        carMap.putIfAbsent(
            spot,
            () => railcars.firstWhere((element) => element.spotId == spot.id,
                orElse: () => null));
      });
    }

    return Flexible(
      child: SizedBox(
        height: 190,
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: spots.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => RailCarBoxWithSpot(
                equipment: carMap[spots.elementAt(index)],
                spot: spots.elementAt(index))),
      ),
    );
  }
}
