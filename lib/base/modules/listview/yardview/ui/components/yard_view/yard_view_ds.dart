import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';

class YardViewDataSource extends ChangeNotifier {
  final formatter = new DateFormat('MM/dd/yyyy HH:mm');
  BuildContext context;
  final List<Equipment> inputList;
  List<TrackModel> trackList = [];
  List<YardModel> yardList = [];
  List<Equipment> selectedRows = [];
  int _selectedCount = 0;
  int _emptyCount = 0;
  int _loadCount = 0;

  YardViewDataSource(this.inputList) {
    this.trackList =
        this.inputList.map((e) => e.toTrack).whereType<TrackModel>().toList();
    this.trackList = this.trackList.toSet().toList();
    this.trackList.sort((a, b) => a.trackName.toLowerCase().compareTo(b.trackName.toLowerCase()));

    this.yardList =
        this.inputList.map((e) => e.toYard).whereType<YardModel>().toList();
    this.yardList = this.yardList.toSet().toList();
    this.yardList.sort((a, b) => a.yardName.toLowerCase().compareTo(b.yardName.toLowerCase()));

    _emptyCount = this.inputList.where((element) => (element.compartmentList == null
        || element.compartmentList.isEmpty
        || (element.compartmentList.isNotEmpty &&
            double.parse(element.compartmentList[0].currentAmount ?? "0") <= 0))).toList().length;
    _loadCount = this.inputList.length - _emptyCount;
  }

  onSelected(bool selected, Equipment equipment) async {
    print("Selected equipment in YardView screen ${equipment}");
    if (selectedRows.contains(equipment)) {
      selectedRows.remove(equipment);
    } else {
      selectedRows.add(equipment);
    }
    notifyListeners();
  }

  bool get isRowCountApproximate => false;

  int get rowCount => inputList?.length ?? 0;

  int get trackCount => trackList?.length ?? 0;

  int get yardCount => yardList?.length ?? 0;

  int get selectedRowCount => _selectedCount ?? 0;

  int get emptyCarsCount => _emptyCount ?? 0;

  int get loadedCarCount => _loadCount ?? 0;
}
