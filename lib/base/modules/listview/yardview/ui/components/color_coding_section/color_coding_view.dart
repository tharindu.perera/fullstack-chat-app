import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/repositories/track_repository.dart';
import 'package:mbase/base/core/common_base/repositories/yard_repository.dart';
import 'package:mbase/base/core/components/flyover/flyover.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/comment_section/comment.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/search_section/search_bloc.dart';
import 'package:provider/provider.dart';

class ColorCodingSection extends StatefulWidget {
  @override
  _ColorCodingState createState() => _ColorCodingState();
}

class _ColorCodingState extends State<ColorCodingSection> {
  ThemeChanger _themeChanger;

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);
    BlocProvider.of<YardViewBloc>(context).state.dataSource.addListener(() {
      setState(() => {});
    });
    return BlocProvider(
      create: (context) => AllFieldsFormBloc(
          BlocProvider.of<YardViewBloc>(context),
          TrackRepository(),
          YardRepository()),
      child: Builder(
        builder: (context) {
          final formBloc = BlocProvider.of<AllFieldsFormBloc>(context);
          formBloc.yardDropDown.state.items
              .sort((a, b) => a.yardName.compareTo(b.yardName));
          formBloc.trackDropDown.state.items
              .sort((a, b) => a.trackName.compareTo(b.trackName));

          return Row(
            children: [
              Container(
                width: ScreenUtil().setWidth(214),
                child: Text('Yard View',
                    style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize:
                            ScreenUtil().setSp(24, allowFontScalingSelf: true),
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.normal,
                        color: Theme.of(context)
                            .textTheme
                            // ignore: deprecated_member_use
                            .headline
                            .color)),
              ),
              SizedBox(width: ScreenUtil().setWidth(200)),
              //HIDE FOR NOW
              // Container(
              //   decoration: BoxDecoration(
              //     borderRadius: BorderRadius.circular(5.0),
              //     border: Border.all(color: Theme.of(context).textTheme.bodyText1.color, style: BorderStyle.solid),
              //   ),
              //   width: ScreenUtil().setWidth(214),
              //   child: DropdownFieldBlocBuilder<TrackModel>(
              //     selectFieldBloc: formBloc.trackDropDown,
              //     decoration: InputDecoration(
              //         contentPadding: EdgeInsets.only(left: 10),
              //         enabledBorder: InputBorder.none,
              //         labelText: 'Color Coding',
              //         labelStyle:
              //             TextStyle(color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Colors.black : null)
              //         //    prefixIcon: Icon(Icons.train),
              //         ),
              //     itemBuilder: (context, value) => value.trackName,
              //   ),
              // ),
              SizedBox(width: ScreenUtil().setWidth(180)),
              Container(
                width: ScreenUtil().setWidth(40),
                height: ScreenUtil().setHeight(40),
                child: FlatButton(
                  onPressed: BlocProvider.of<YardViewBloc>(context)
                          .state
                          .dataSource
                          .selectedRows
                          .isNotEmpty
                      ? () {
                          showDialog(
                              context: context,
                              builder: (_) {
                                return CommentFlyOver(
                                    BlocProvider.of<YardViewBloc>(context)
                                        .state
                                        .dataSource
                                        .selectedRows);
                              });
                        }
                      : null,
                  padding: EdgeInsets.all(0),
                  child: Icon(
                    Icons.comment,
                    size: 30,
                    color: Colors.white,
                  ),
                  color: Color(0xFF508be4),
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black,
                  splashColor: Color(0xFF508be4),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(20)),
              Container(
                width: ScreenUtil().setWidth(40),
                height: ScreenUtil().setHeight(40),
                child: FlatButton(
                  onPressed: BlocProvider.of<YardViewBloc>(context)
                          .state
                          .dataSource
                          .selectedRows
                          .isNotEmpty
                      ? () {
                          showDialog(
                              context: context,
                              builder: (_) {
                                return FlyOver(
                                    BlocProvider.of<YardViewBloc>(context)
                                        .state
                                        .dataSource
                                        .selectedRows);
                              });
                        }
                      : null,
                  padding: EdgeInsets.all(0),
                  child: Icon(
                    Icons.remove_red_eye,
                    size: 30,
                    color: Colors.white,
                  ),
                  color: Color(0xFF508be4),
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black,
                  splashColor: Color(0xFF508be4),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(5)),
            ],
          );
        },
      ),
    );
  }
}
