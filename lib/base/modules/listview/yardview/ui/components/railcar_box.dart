import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/yard_view/yard_view_ds.dart';

class RailCarBox extends StatefulWidget {
  RailCarBox(
      {this.equipment,
      this.title,
      this.ontap,
      this.icon,
      this.imagePath,
      this.dataSource});

  final String imagePath;
  final IconData icon;
  final Function ontap;
  final String title;
  final Equipment equipment;
  final YardViewDataSource dataSource;

  @override
  _RailCarBoxState createState() =>
      _RailCarBoxState(this.equipment, this.dataSource);
}

class _RailCarBoxState extends State<RailCarBox> {
  YardViewDataSource dataSource;
  Equipment equipment;

  _RailCarBoxState(Equipment equipment, YardViewDataSource dataSource) {
    this.equipment = equipment;
    this.dataSource = dataSource;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: Colors.red,
      focusColor: Colors.red,
      borderRadius: BorderRadius.circular(100.0),
      onTap: () {
        print("Container clicked");
        BlocProvider.of<YardViewBloc>(context)
            .state
            .dataSource
            .onSelected(true, equipment);
        setState(() {});
      },
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Container(
          margin: EdgeInsets.only(left: 30, top: 10, bottom: 10),
          decoration: BoxDecoration(
            border: Border.all(width: 2.0, color: Colors.black),
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            color: isSelected(equipment)
                ? Colors.blue.withOpacity(0.3)
                : Colors.transparent,
          ),
          height: 100,
          width: 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10),
                child: Image(
                    width: ScreenUtil().setWidth(50),
                    fit: BoxFit.cover,
                    image: this.isEmpty(equipment)
                        ? AssetImage('assets/images/boxcar-empty.png')
                        : AssetImage('assets/images/boxcar-loaded.png')),
              ),
              Text(equipment.equipmentInitial),
              Text(equipment.equipmentNumber),
              // Align(
              //   alignment: Alignment.bottomLeft,
              //   child: new Container(
              //     width: 15.0,
              //     height: 15.0,
              //     decoration: BoxDecoration(
              //         color: Colors.blue,
              //         shape: BoxShape.circle
              //     ),
              //     alignment: Alignment.center
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  bool isEmpty(Equipment equipment) {
    return (null == equipment.compartmentList ||
        equipment.compartmentList.isEmpty ||
        (equipment.compartmentList.isNotEmpty &&
            double.parse(equipment.compartmentList[0].currentAmount ?? "0") <=
                0));
  }

  bool isSelected(Equipment equipment) {
    return BlocProvider.of<YardViewBloc>(context)
        .state
        .dataSource
        .selectedRows
        .contains(equipment);
  }
}
