import 'package:flutter/material.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/railcar_box.dart';

class RailCarBoxWithoutSpot extends StatefulWidget {
  RailCarBoxWithoutSpot({this.equipment});

  final Equipment equipment;

  @override
  _RailCarBoxWithoutSpotState createState() =>
      _RailCarBoxWithoutSpotState(this.equipment);
}

class _RailCarBoxWithoutSpotState extends State<RailCarBoxWithoutSpot> {
  Equipment equipment;

  _RailCarBoxWithoutSpotState(Equipment equipment) {
    this.equipment = equipment;
  }

  @override
  Widget build(BuildContext context) {
    return RailCarBox(equipment: equipment);
  }
}
