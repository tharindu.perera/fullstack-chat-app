import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/common_base/repositories/track_repository.dart';
import 'package:mbase/base/core/common_base/repositories/yard_repository.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';

class AllFieldsFormBloc extends FormBloc {
  TrackRepository _trackRepository;
  YardRepository _yardRepository;
  YardViewBloc _yardViewBloc;

  final yardDropDown = SelectFieldBloc<YardModel, String>(
    name: "yard_id",
    toJson: (value) => value.id,
  );

  final trackDropDown = SelectFieldBloc<TrackModel, dynamic>(
    name: "track_id",
    toJson: (value) => value.id,
  );

  final textSearch = TextFieldBloc(
    name: "textSearch",
  );

  AllFieldsFormBloc(
      this._yardViewBloc, this._trackRepository, this._yardRepository) {
    _loadDropDownDataAndListen();
    addFieldBlocs(fieldBlocs: [yardDropDown, trackDropDown, textSearch]);
  }

  _loadDropDownDataAndListen() async {
    (await _yardRepository.fetchAllYards())
        .forEach((element) => yardDropDown.addItem(element));
    (await _trackRepository.fetchAllTracks())
        .forEach((element) => trackDropDown.addItem(element));

    yardDropDown.listen((covariant) async {
      if (!covariant.isInitial && covariant.value == null) {
        trackDropDown.updateValue(null);
        trackDropDown.updateItems((await _trackRepository.fetchAllTracks()));
      } else {
        !covariant.isInitial
            ? trackDropDown.updateItems((await _trackRepository
                .fetchTracksByYardId(yardDropDown?.value?.id)))
            : null;
      }
    });

    trackDropDown.listen((covariant) async {
      !covariant.isInitial && covariant.value != null
          ? yardDropDown
              .updateValue(MemoryData.inMemoryYardMap[covariant.value.yardId])
          : null;
    });

    this.listen((covariant) async {
      _yardViewBloc.add(YardViewFetchDataWithCriteria(state.toJson()));
    }); //listen to both dropdown and fetch data accordingly
  }

  @override
  void onSubmitting() async {
    try {
      await Future<void>.delayed(Duration(milliseconds: 500));
      emitSuccess(canSubmitAgain: true);
    } catch (e) {
      emitFailure();
    }
  }
}
