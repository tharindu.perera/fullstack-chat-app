import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/common_base/repositories/track_repository.dart';
import 'package:mbase/base/core/common_base/repositories/yard_repository.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/search_section/search_bloc.dart';
import 'package:provider/provider.dart';

class SearchSection extends StatelessWidget {
  ThemeChanger _themeChanger;

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);

    return BlocProvider(
      create: (context) => AllFieldsFormBloc(
          BlocProvider.of<YardViewBloc>(context),
          TrackRepository(),
          YardRepository()),
      child: Builder(
        builder: (context) {
          final formBloc = BlocProvider.of<AllFieldsFormBloc>(context);
          formBloc.yardDropDown.state.items
              .sort((a, b) => a.yardName.toLowerCase().compareTo(b.yardName.toLowerCase()));
          formBloc.trackDropDown.state.items
              .sort((a, b) => a.trackName.toLowerCase().compareTo(b.trackName.toLowerCase()));

          return Row(
            children: [
              Container(
                width: ScreenUtil().setWidth(214),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        BlocProvider.of<YardViewBloc>(context)
                                .state
                                .dataSource
                                .rowCount
                                .toString() +
                            ' Results',
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: ScreenUtil()
                                .setSp(18, allowFontScalingSelf: true),
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            color: Theme.of(context)
                                .textTheme
                                // ignore: deprecated_member_use
                                .headline
                                .color)),
                    Text(
                        BlocProvider.of<YardViewBloc>(context)
                                .state
                                .dataSource
                                .trackCount
                                .toString() +
                            ' Tracks',
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: ScreenUtil()
                                .setSp(10, allowFontScalingSelf: true),
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            color: Theme.of(context)
                                .textTheme
                                // ignore: deprecated_member_use
                                .headline
                                .color)),
                    Row(
                      children: [
                        Text(
                            'Loaded:  ' +
                                BlocProvider.of<YardViewBloc>(context)
                                    .state
                                    .dataSource
                                    .loadedCarCount
                                    .toString(),
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil()
                                    .setSp(10, allowFontScalingSelf: true),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .headline
                                    .color)),
                        Text(
                            ' Empty:  ' +
                                BlocProvider.of<YardViewBloc>(context)
                                    .state
                                    .dataSource
                                    .emptyCarsCount
                                    .toString(),
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil()
                                    .setSp(10, allowFontScalingSelf: true),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .headline
                                    .color)),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(20)),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(
                      color: Theme.of(context).textTheme.bodyText1.color,
                      style: BorderStyle.solid),
                ),
                // decoration: ShapeDecoration(
                //   shape: RoundedRectangleBorder(
                //     side: BorderSide(width: 1.0, style: BorderStyle.solid),
                //     borderRadius: BorderRadius.all(Radius.circular(5.0)),
                //   ),
                // ),
                width: ScreenUtil().setWidth(214),
                child: DropdownFieldBlocBuilder<YardModel>(
                  selectFieldBloc: formBloc.yardDropDown,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.only(left: 10, bottom: 0, top: 0),
                      enabledBorder: InputBorder.none,
                      labelText: 'Select Yard',
                      labelStyle: TextStyle(
                          color: _themeChanger.getTheme().primaryColor ==
                                  Color(0xff182e42)
                              ? Colors.white
                              : _themeChanger.getTheme().primaryColor ==
                                      Color(0xfff5f5f5)
                                  ? Colors.black
                                  : null)
                      //    prefixIcon: Icon(Icons.train),
                      ),
                  itemBuilder: (context, value) => value.yardName,
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(10)),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(
                      color: Theme.of(context).textTheme.bodyText1.color,
                      style: BorderStyle.solid),
                ),
                width: ScreenUtil().setWidth(214),
                child: DropdownFieldBlocBuilder<TrackModel>(
                  selectFieldBloc: formBloc.trackDropDown,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 10),
                      enabledBorder: InputBorder.none,
                      labelText: 'Select Track',
                      labelStyle: TextStyle(
                          color: _themeChanger.getTheme().primaryColor ==
                                  Color(0xff182e42)
                              ? Colors.white
                              : _themeChanger.getTheme().primaryColor ==
                                      Color(0xfff5f5f5)
                                  ? Colors.black
                                  : null)
                      //    prefixIcon: Icon(Icons.train),
                      ),
                  itemBuilder: (context, value) => value.trackName,
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(10)),
            ],
          );
        },
      ),
    );
  }
}
