import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/listview/actions/comment/dao/comment_dao.dart';
import 'package:mbase/base/modules/listview/actions/comment/model/comment_model.dart';
import 'package:provider/provider.dart';

enum FlyOverAction { done, forward, backward }

class CommentFlyOver extends StatefulWidget {
  CommentFlyOver(List<Equipment> selectedEquipments) {
    this.selectedEquipments = selectedEquipments;
  }

  List<Equipment> selectedEquipments = List<Equipment>();

  @override
  _CommentFlyOverState createState() => _CommentFlyOverState();
}

class _CommentFlyOverState extends State<CommentFlyOver> {
  ThemeChanger _themeChanger;
  String railcar_number = "";
  int current_iteration = 0;
  List<Equipment> selectedRailCars = [];
  List<Comment> comments = [];
  CommentDAO commentDAO = CommentDAO();
  final dateTimeFormatter = new DateFormat('MM/dd/yyyy HH:mm');

  @override
  void initState() {
    super.initState();
    selectedRailcar();
  }

  changeRailCar({FlyOverAction action}) {
    Equipment temp = selectedRailCars.elementAt(current_iteration);
    switch (action) {
      case FlyOverAction.backward:
        current_iteration--;
        temp = selectedRailCars.elementAt(current_iteration);
        railcar_number = temp?.equipmentInitial + temp?.equipmentNumber;
        fetchFlyOverData(temp);
        break;
      case FlyOverAction.forward:
        current_iteration++;
        temp = selectedRailCars.elementAt(current_iteration);
        railcar_number = temp?.equipmentInitial + temp?.equipmentNumber;
        fetchFlyOverData(temp);
        break;
    }
    setState(() {});
  }

  selectedRailcar() {
    selectedRailCars = widget.selectedEquipments;
    setState(() {
      Equipment temp = selectedRailCars.elementAt(current_iteration);
      railcar_number = temp?.equipmentInitial + temp?.equipmentNumber;
      fetchFlyOverData(temp);
    });
  }

  fetchFlyOverData(Equipment equipment) async {
    Loader.show(context);
    this.comments = await commentDAO.getCommentsForEquipment(equipment);
    Loader.hide();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);
    print("---------rebuilding-------------");

    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: AlertDialog(
        backgroundColor: Theme.of(context).canvasColor,
        content: Container(
            width: ScreenUtil().setWidth(860),
            height: ScreenUtil().setHeight(560),
            child: Column(
              children: <Widget>[
                Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    current_iteration > 0
                        ? Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            width: ScreenUtil().setWidth(20),
                            height: ScreenUtil().setHeight(20),
                            color: Colors.grey,
                            child: Center(
                              child: GestureDetector(
                                  onTap: () {
                                    print('backword');
                                    changeRailCar(
                                        action: FlyOverAction.backward);
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: 15,
                                  )),
                            ),
                          )
                        : Container(),
                    Container(
                      child: Text(
                        railcar_number,
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontStyle: FontStyle.normal,
                            fontFamily: 'Roboto',
                            letterSpacing: ScreenUtil().setWidth(0.5),
                            color: Theme.of(context).textTheme.bodyText1.color,
                            fontSize: ScreenUtil()
                                .setSp(20, allowFontScalingSelf: true)),
                      ),
                    ),
                    current_iteration != widget.selectedEquipments.length - 1
                        ? Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            width: ScreenUtil().setWidth(20),
                            height: ScreenUtil().setHeight(20),
                            color: Colors.grey,
                            child: Center(
                              child: GestureDetector(
                                  onTap: () {
                                    print('forward');
                                    changeRailCar(
                                        action: FlyOverAction.forward);
                                  },
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.white,
                                    size: 15,
                                  )),
                            ),
                          )
                        : Container()
                  ],
                )),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Text(
                          "${current_iteration + 1} of ${widget.selectedEquipments.length} ",
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontStyle: FontStyle.normal,
                              fontFamily: 'Roboto',
                              letterSpacing: ScreenUtil().setWidth(0.5),
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                              fontSize: ScreenUtil()
                                  .setSp(12, allowFontScalingSelf: true)),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(20),
                ),
                Container(
                    width: ScreenUtil().setWidth(1061),
                    height: ScreenUtil().setHeight(486),
                    color: Theme.of(context).scaffoldBackgroundColor,
                    child: DottedBorder(
                      color: _themeChanger.getTheme().primaryColor ==
                              Color(0xff182e42)
                          ? Color(0xfff5f5f5)
                          : _themeChanger.getTheme().primaryColor ==
                                  Color(0xfff5f5f5)
                              ? Color(0xff172636)
                              : null,
                      dashPattern: [1],
                      strokeWidth: 1,
                      child: Container(
                        child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Wrap(
                              children: comments.map((value) {
                                return Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14),
                                        vertical: ScreenUtil().setHeight(14)),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                      color: _themeChanger
                                                  .getTheme()
                                                  .primaryColor ==
                                              Color(0xff182e42)
                                          ? Colors.white
                                          : _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xfff5f5f5)
                                              ? Color.fromRGBO(0, 0, 0, 0.6)
                                              : null,
                                    )),
                                    child: ListTile(
                                      leading: Icon(
                                        Icons.account_circle,
                                        size: 50,
                                        color: Color(0xFF508be4),
                                      ),
                                      title: Text(
                                        value.user ?? "",
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(16,
                                              allowFontScalingSelf: true),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: 'Roboto',
                                          color: _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xff182e42)
                                              ? Colors.white
                                              : _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xfff5f5f5)
                                                  ? Color(0xFF508be4)
                                                  : null,
                                        ),
                                      ),
                                      trailing: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Container(
                                              width: ScreenUtil().setWidth(180),
                                              child: Text(
                                                value.commentType ?? "",
                                                style: TextStyle(
                                                  fontSize: ScreenUtil().setSp(
                                                      14,
                                                      allowFontScalingSelf:
                                                          true),
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.normal,
                                                  fontFamily: 'Roboto',
                                                  color: _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xff182e42)
                                                      ? Colors.white
                                                      : _themeChanger
                                                                  .getTheme()
                                                                  .primaryColor ==
                                                              Color(0xfff5f5f5)
                                                          ? Colors.black
                                                          : null,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              width: ScreenUtil().setWidth(220),
                                              child: RichText(
                                                text: TextSpan(
                                                  text: 'Last Updated: ',
                                                  style: TextStyle(
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: ScreenUtil().setSp(
                                                        14,
                                                        allowFontScalingSelf:
                                                            true),
                                                    fontFamily: 'Roboto',
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    color: _themeChanger
                                                                .getTheme()
                                                                .primaryColor ==
                                                            Color(0xff182e42)
                                                        ? Color(0xFFffffff)
                                                        : _themeChanger
                                                                    .getTheme()
                                                                    .primaryColor ==
                                                                Color(
                                                                    0xfff5f5f5)
                                                            ? Color.fromRGBO(
                                                                0, 0, 0, 0.6)
                                                            : null,
                                                  ),
                                                  children: <InlineSpan>[
                                                    TextSpan(
                                                        text: dateTimeFormatter
                                                            .format(value
                                                                ?.createdDt),
                                                        // value?.createdDt?.toString(),
                                                        style: TextStyle(
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontSize: ScreenUtil()
                                                              .setSp(14,
                                                                  allowFontScalingSelf:
                                                                      true),
                                                          fontFamily: 'Roboto',
                                                          fontWeight:
                                                              FontWeight.normal,
                                                          color: _themeChanger
                                                                      .getTheme()
                                                                      .primaryColor ==
                                                                  Color(
                                                                      0xff182e42)
                                                              ? Colors.white
                                                              : _themeChanger
                                                                          .getTheme()
                                                                          .primaryColor ==
                                                                      Color(
                                                                          0xfff5f5f5)
                                                                  ? Colors.black
                                                                  : null,
                                                        )),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ]),
                                      subtitle: Text(
                                        value.comment ?? "",
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(14,
                                              allowFontScalingSelf: true),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: 'Roboto',
                                        ),
                                      ),
                                      isThreeLine: true,
                                      dense: true,
                                    ));
                              }).toList(),
                            )),
                      ),
                    ))
              ],
            )),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(12),
                  vertical: ScreenUtil().setHeight(24)),
              child: Container(
                width: ScreenUtil().setWidth(180),
                height: ScreenUtil().setHeight(48),
                child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(FlyOverAction.done);
                    },
                    child: Text(
                      'DONE',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.25),
                          color: Colors.white),
                    ),
                    color: Color(0xFF508be4),
                    textColor: Colors.white,
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black,
                    splashColor: Color(0xFF3e8aeb)),
              )),
        ],
      ),
    );
  }
}
