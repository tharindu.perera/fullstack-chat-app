import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/railcar_box.dart';
import 'package:mbase/base/modules/listview/yardview/ui/components/yard_view/yard_view_ds.dart';

class RailCarBoxWithSpot extends StatefulWidget {
    RailCarBoxWithSpot({this.equipment, this.spot, this.dataSource});

  final SpotModel spot;
  final Equipment equipment;
  final YardViewDataSource dataSource;

  @override
  _RailCarBoxWithSpotState createState() =>
      _RailCarBoxWithSpotState(this.equipment, this.spot, this.dataSource);
}

class _RailCarBoxWithSpotState extends State<RailCarBoxWithSpot> {
  YardViewDataSource dataSource;
  Equipment equipment;
  SpotModel spot;

  _RailCarBoxWithSpotState(
      Equipment equipment, SpotModel spot, YardViewDataSource dataSource) {
    this.equipment = equipment;
    this.spot = spot;
    this.dataSource = dataSource;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          equipment != null
              ? RailCarBox(equipment: equipment)
              : Container(
                  height: 40,
                  width: 50,
                  margin: EdgeInsets.only(left: 30, top: 50, bottom: 50),
                  decoration: BoxDecoration(
                    border: Border.all(width: 2.0, color: Colors.black),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
          Container(
            height: 30,
            width: 70,
            margin: EdgeInsets.only(left: 60),
            child: spot != null
                ? Text(
                    spot.spotName,
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  )
                : null,
          ),
        ],
      ),
    );
  }
}
