import 'dart:async';

import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/dao/spot_dao.dart';
import 'package:mbase/base/core/common_base/dao/track_dao.dart';
import 'package:mbase/base/core/common_base/dao/yard_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/util/nullCheckUtil.dart';
import 'package:mbase/base/modules/listview/actions/comment/dao/comment_dao.dart';
import 'package:mbase/env.dart';

class YardViewRepository {
  EquipmentDAO equipmentDAO = EquipmentDAO();
  CommentDAO commentDAO = CommentDAO();
  YardDao yardDao = YardDao();
  TrackDAO trackDAO = TrackDAO();
  SpotDao spotDao = SpotDao();

  Stream<List<Equipment>> getRecordByCriteria(Map<String, dynamic> criteriaFromListGrid, ascending, sortColumnIndex) {
    Map<String, dynamic> criteria = Map();
    criteria.putIfAbsent("permission_id", () => [env.userProfile.id, APP_CONST.PUBLIC]);
    criteriaFromListGrid != null ? criteria.addAll(criteriaFromListGrid) : null;

    var text = (criteria != null && criteria.containsKey("textSearch") && criteria["textSearch"].toString().trim() != "") ? criteria["textSearch"] : null;
    criteria?.remove("textSearch");
    if (text != null) {
      StreamController<List<Equipment>> controller = StreamController<List<Equipment>>();

      var mapStream = (equipmentDAO.getRecordByCriteria(criteria)).map((list) {
        List<Equipment> temp = [];
        List<String> removableIdlist = List();
        list.forEach((equipment) async {
          if (equipment.equipmentInitial.startsWith(text)) {
            temp.add(equipment);
          } else if (equipment.equipmentInitial.contains(text)) {
            temp.add(equipment);
          } else if ((equipment.equipmentInitial + " " + equipment.equipmentNumber).contains(text)) {
            temp.add(equipment);
          } else if ((equipment.equipmentInitial + equipment.equipmentNumber).contains(text)) {
            temp.add(equipment);
          } else if (equipment.equipmentNumber.contains(text)) {
            temp.add(equipment);
          } else if (MemoryData.inMemoryTrackMap[equipment.trackId] != null && MemoryData.inMemoryTrackMap[equipment.trackId].trackName.toUpperCase().startsWith(text)) {
            temp.add(equipment);
          } else if (MemoryData.inMemorySpotMap[equipment.spotId] != null && MemoryData.inMemorySpotMap[equipment.spotId].spotName.toUpperCase().startsWith(text)) {
            temp.add(equipment);
          }

        });
        criteria["textSearch"] = text;
        temp.forEach((element) {
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.OUTBOUND_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.id)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.LOAD_EQUIPMENT_MASTER_REC_UPDATE)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.UNLOAD_EQUIPMENT_MASTER_REC_UPDATE)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.LOAD_CAR_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.UNLOAD_CAR_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.MOVE_RAILCAR)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;

          (element.isOutbounded) ? removableIdlist.add(element.id) : null;
        });
        temp.removeWhere((element) {
          return removableIdlist.contains(element.id);
        });

        commentDAO.getComments().then((comments) {
          temp.forEach((equipment) {
            comments.forEach((cmnt) {
              if (cmnt.assetMasterId != null && equipment.assetMasterId != null && cmnt.assetMasterId == equipment.assetMasterId) {
                if (cmnt.commentType == "Permanent") {
                  equipment.comments.add(cmnt);
                } else if (cmnt.commentType != "Permanent" && cmnt.facilityVisitId == equipment.facilityVisitId) {
                  equipment.comments.add(cmnt);
                }
              }
            });
          });
          controller.add(temp);
        });

        return temp;
      });
      mapStream.listen((event) {
        controller.add(event);
      });
      return controller.stream;
    } else {
      StreamController<List<Equipment>> controller = StreamController<List<Equipment>>();

      var mapStream = equipmentDAO.getRecordByCriteria(criteria).map((event) async {
        List<Equipment> temp = [];
        List<String> removableIdlist = List();
        temp.addAll(event);
        temp.forEach((element) {
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.OUTBOUND_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.id)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.LOAD_EQUIPMENT_MASTER_REC_UPDATE)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.UNLOAD_EQUIPMENT_MASTER_REC_UPDATE)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.LOAD_CAR_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.UNLOAD_CAR_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.MOVE_RAILCAR)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;

          (element.isOutbounded) ? removableIdlist.add(element.id) : null;

        });
        temp.removeWhere((element) {
          return removableIdlist.contains(element.id);
        });

        await trackDAO.fetchAllTracks().then((tracks) {
          temp.forEach((equipment) {
            tracks.forEach((track) {
              if (track.id != null && equipment.trackId != null && track.id == equipment.trackId) {
                equipment.toTrack = track;
              }
            });
          });
        });

        await spotDao.fetchAllSpots().then((spots) {
          temp.forEach((equipment) {
            TrackModel track = equipment.toTrack;
            List<SpotModel> tempSpots = spots.where((element) => element.trackId == equipment.trackId).toList();
            if (tempSpots != null && tempSpots.isNotEmpty) {
              equipment.toTrack.spotList = tempSpots;
            }
          });
        });

        await yardDao.fetchAllYards().then((yards) {
          temp.forEach((equipment) {
            yards.forEach((yard) {
              if (yard.id != null && equipment.yardId != null && yard.id == equipment.yardId) {
                equipment.toYard = yard;
              }
            });
          });
        });

        controller.add(temp);

        return temp;
      });

      mapStream.listen((event) {
        event.then((value) {
          controller.add(value);
        });
      });

      return controller.stream;
    }
  }

  _sortData(List<Equipment> list, ascending, sortColumnIndex) {
    if (sortColumnIndex == 0) {
      ascending
          ? list.sort((a, b) => (a.equipmentInitial + a.equipmentNumber).compareTo(b.equipmentInitial + b.equipmentNumber))
          : list.sort((a, b) => ((b.equipmentInitial + b.equipmentNumber).compareTo(a.equipmentInitial + a.equipmentNumber)));
    } else if (sortColumnIndex == 1) {
      ascending
          ? list.sort((a, b) => (MemoryData.inMemoryTrackMap[a.trackId].trackName + "/" + setNullToEmptyString(MemoryData.inMemorySpotMap[a.spotId]?.spotName))
              .compareTo(MemoryData.inMemoryTrackMap[b.trackId].trackName + "/" + setNullToEmptyString(MemoryData.inMemorySpotMap[b.spotId]?.spotName)))
          : list.sort((a, b) => (MemoryData.inMemoryTrackMap[b.trackId].trackName + "/" + setNullToEmptyString(MemoryData.inMemorySpotMap[b.spotId]?.spotName))
              .compareTo(MemoryData.inMemoryTrackMap[a.trackId].trackName + "/" + setNullToEmptyString(MemoryData.inMemorySpotMap[a.spotId]?.spotName)));
    } else if (sortColumnIndex == 2) {
      ascending ? list.sort((a, b) => a.placedDate.compareTo(b.placedDate)) : list.sort((a, b) => b.placedDate.compareTo(a.placedDate));
    } else if (sortColumnIndex == 3) {
      ascending
          ? list.sort((a, b) => setNullToEmptyString(a.equipmentType).compareTo(setNullToEmptyString(b.equipmentType)))
          : list.sort((a, b) => setNullToEmptyString(b.equipmentType).compareTo(setNullToEmptyString(a.equipmentType)));
    } else if (sortColumnIndex == 4) {
      ascending
          ? list.sort((a, b) => setNullToEmptyString(a.compartmentList.length > 0 ? a.compartmentList[0].productName : null)
              .compareTo(setNullToEmptyString(b.compartmentList.length > 0 ? b.compartmentList[0].productName : null)))
          : list.sort((a, b) => setNullToEmptyString(b.compartmentList.length > 0 ? b.compartmentList[0].productName : null)
              .compareTo(setNullToEmptyString(a.compartmentList.length > 0 ? a.compartmentList[0].productName : null)));
    } else if (sortColumnIndex == 5) {
      ascending
          ? list.sort((a, b) => setNullToEmptyString(a.compartmentList.length > 0 ? a.compartmentList[0].currentAmount : null)
              .compareTo(setNullToEmptyString(b.compartmentList.length > 0 ? b.compartmentList[0].currentAmount : null)))
          : list.sort((a, b) => setNullToEmptyString(b.compartmentList.length > 0 ? b.compartmentList[0].currentAmount : null)
              .compareTo(setNullToEmptyString(a.compartmentList.length > 0 ? a.compartmentList[0].currentAmount : null)));
    }
  }
}
