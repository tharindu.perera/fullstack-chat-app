import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';

class AddRailCarRepo {
  EquipmentDAO equipmentDAO = EquipmentDAO();
  NotificationRepository notificationRepository = NotificationRepository();

  Future<bool> checkEquipmentNumber(
      String equipmentInitial, String equipmentNumber) async {
    return equipmentDAO.checkEquipmentNumber(equipmentInitial, equipmentNumber);
  }

  Future<bool> addRailcar(
      equipInit, equipNum, yard, track, spot,dateTime, sequenceOfCar) async {
    String equipNumTemp = equipNum;
    if (equipNumTemp.length < 6) {
      equipNumTemp = equipNumTemp.padLeft(6, '0');
    }
    bool equipmentExisting =
        await checkEquipmentNumber(equipInit, equipNumTemp);
    if (equipmentExisting) {
      throw Exception("Equipment already exists.");
    } else {
      Equipment addRailCarModel = new Equipment();
      addRailCarModel.equipmentInitial = equipInit;
      addRailCarModel.equipmentNumber = equipNumTemp;
      addRailCarModel.yardId = yard; // yard name, not id
      addRailCarModel.trackId = track; // track name, not id
      addRailCarModel.spotId = spot; // track name, not id
      addRailCarModel.placedDate = dateTime;
      addRailCarModel.sequenceType = sequenceOfCar;
      addRailCarModel.equipmentType = "Railcar";
      addRailCarModel.permission_id = env.userProfile.id;
      addRailCarModel.facilityId =
          env.userProfile.facilityId; //userProfile.facilityID
      addRailCarModel.transactionStatus = APP_CONST.TRANSACTION_STATUS_PENDING;
      addRailCarModel.operationType = APP_CONST.ADD_CAR_DB_OPERATION;
      addRailCarModel.source = APP_CONST.MOBILITY;
      addRailCarModel.user = env.userProfile.userName;
      addRailCarModel.createdDt = DateTime.now();
      equipmentDAO.add(addRailCarModel);
      notificationRepository.addNotification(
          addRailCarModel, APP_CONST.ADD_CAR_DB_OPERATION);
    }
  }

  Future<Equipment> getYardTrack(
      String equipmentInitial, String equipmentNumber) async {
    return equipmentDAO.getYardTrack(equipmentInitial, equipmentNumber);
  }
}
