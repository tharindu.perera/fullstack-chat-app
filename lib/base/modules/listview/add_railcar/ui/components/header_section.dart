import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/modules/listview/add_railcar/ui/components/form_section/add_car_form_bloc.dart';
import 'package:mbase/base/modules/listview/add_railcar/ui/components/show_pop_func.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';

class AddScreenHeader extends StatelessWidget {
  String source;
  AddScreenHeader(String source) {
    this.source = source;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Container(
                      child: ListTile(
                    leading: GestureDetector(
                        onTap: () {
                          if (BlocProvider.of<AddCarFormBloc>(context).isStatusChanged) {
                            popUpDialog(context, this.source);
                          } else {
                            Navigator.pop(context);
                            BlocProvider.of<ListViewBloc>(context).add(ListViewFetchDataWithLastSetCriteria());
                          }
                        },
                        child: Icon(
                          Icons.arrow_back,
                        )),
                    title: Text('Add Railcar',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Roboto',
                            color: Theme.of(context)
                                .textTheme
                                .headline
                                .color)),
                  ))),
            ),
            Row(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(20),
                    child: Container(
                        child: GestureDetector(
                      onTap: () {
                        if (BlocProvider.of<AddCarFormBloc>(context).isStatusChanged) {
                          popUpDialog(context, this.source);
                        } else {
                          Navigator.pop(context);
                          BlocProvider.of<ListViewBloc>(context).add(ListViewFetchDataWithLastSetCriteria());
                        }
                      },
                      child: RichText(
                        text: TextSpan(style: Theme.of(context).textTheme.body1, children: <InlineSpan>[
                          TextSpan(
                              text: 'CANCEL',
                              style: TextStyle(
                                  fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  fontFamily: 'Roboto',
                                  color: Color(0xFF3e8aeb)))
                        ]),
                      ),
                    ))),
                Padding(
                    padding: EdgeInsets.all(20),
                    child: Container(
                      // width: 150,
                      width: ScreenUtil().setWidth(210),
                      height: ScreenUtil().setHeight(48),
                      child: FlatButton(
                          onPressed: () {
                            context.bloc<AddCarFormBloc>().submit();
                          },
                          child: Text('SAVE & ADD RAILCAR',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  color: Colors.white)),
                          color: Color(0xFF3e8aeb),
                          textColor: Colors.white,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          splashColor: Color(0xFF3e8aeb)),
                    ))
              ],
            ),
          ],
        ),
        SizedBox(height: ScreenUtil().setHeight(24)),
        Divider(thickness: 2.0),
        SizedBox(height: ScreenUtil().setHeight(24)),
      ],
    );
  }
}
