import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/util/formatter.dart';
import 'package:mbase/base/modules/listview/add_railcar/repository/addrailcar_repo.dart';
import 'package:mbase/base/modules/listview/add_railcar/ui/components/form_section/add_car_form_bloc.dart';
import 'package:mbase/base/modules/listview/add_railcar/ui/components/header_section.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';
import 'package:provider/provider.dart';

class AddCarForm extends StatelessWidget {
  //final dateFormat = new DateFormat('yyyy-MM-dd hh:mm');
  final dateFormat = new DateFormat('MM/dd/yyyy HH:mm');

  String source;

  AddCarForm(String source) {
    this.source = source;
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    return BlocProvider(
      create: (context) => AddCarFormBloc(AddRailCarRepo()),
      child: Builder(
        builder: (context) {
          print(">>>>>>>>>>");
          final formBloc = context.bloc<AddCarFormBloc>();
          return FormBlocListener<AddCarFormBloc, String, String>(
            onSuccess: (context, state) {
              FToast(context).showToast(
                child: CustomToast(
                  icon: Icons.check,
                  toastColor: Color(0xff7fae1b),
                  toastMessage: state.successResponse,
                ),
                gravity: ToastGravity.TOP,
                toastDuration: Duration(seconds: 2),
              );
              Navigator.pop(context);
              if (APP_CONST.YARD_VIEW == this.source) {
                BlocProvider.of<YardViewBloc>(context).add(YardViewFetchDataWithLastSetCriteria());
              } else {
                BlocProvider.of<ListViewBloc>(context).add(ListViewFetchDataWithLastSetCriteria());
              }
              Loader?.hide();
            },
            onFailure: (context, state) {
              
              FToast(context).showToast(
                child: CustomToast(
                  icon: Icons.new_releases,
                  toastColor: Colors.redAccent,
                  toastMessage: "Railcar already Exist",
                ),
                gravity: ToastGravity.TOP,
                toastDuration: Duration(seconds: 2),
              );
              Loader?.hide();
            },
             onSubmitting: (context, state) {
               Loader?.show(context);
             },


            child: Column(
              children: <Widget>[
                AddScreenHeader(this.source),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(width: ScreenUtil().setWidth(24)),
                    Container(
                      width: ScreenUtil().setWidth(214),
                      child: TextFieldBlocBuilder(
                        textFieldBloc: formBloc.equipInit,
                        keyboardType: TextInputType.name,
                        inputFormatters: [
                          UpperCaseFormatter(),
                          WhitelistingTextInputFormatter(RegExp("[A-Z]")),
                          LengthLimitingTextInputFormatter(4),
                        ],
                        style: TextStyle(
                          color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xfff5f5f5) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                        ),
                        decoration: InputDecoration(
                          // prefixIcon: Icon(
                          //   Icons.train,
                          //   color: Colors.black,
                          // ),
                          errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                          contentPadding: EdgeInsets.only(left: 10),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(1.0),
                            borderSide: BorderSide(width: 1, color: Colors.black38),
                          ),
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.black38, width: 1),
                          ),
                          filled: true,
                          fillColor:
                              _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                          hintText: 'Equipment Initial',
                          hintStyle: TextStyle(
                            fontSize: ScreenUtil().setHeight(14),
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.normal,
                            fontStyle: FontStyle.normal,
                            letterSpacing: ScreenUtil().setWidth(0.25),
                            color:
                                _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xfff5f5f5) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(24)),
                    Container(
                      width: ScreenUtil().setWidth(214),
                      child: TextFieldBlocBuilder(
                        textFieldBloc: formBloc.equipNum,
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          LengthLimitingTextInputFormatter(6),
                        ],
                        style: TextStyle(
                          color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xfff5f5f5) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                        ),
                        decoration: InputDecoration(
                          // prefixIcon: Icon(
                          //   Icons.train,
                          //   color: Colors.black,
                          // ),
                          contentPadding: EdgeInsets.only(left: 10),
                          errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(1.0),
                            borderSide: BorderSide(width: 1, color: Colors.black38),
                          ),
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.black38, width: 1),
                          ),
                          filled: true,
                          fillColor:
                              _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                          hintText: 'Equipment Number',
                          hintStyle: TextStyle(
                            fontSize: ScreenUtil().setHeight(14),
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.normal,
                            fontStyle: FontStyle.normal,
                            letterSpacing: ScreenUtil().setWidth(0.25),
                            color:
                                _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xfff5f5f5) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(24)),
                    Container(
                      width: 225,
                      child: DateTimeFieldBlocBuilder(
                        style: TextStyle(
                          color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xfff5f5f5) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                        ),
                        dateTimeFieldBloc: formBloc.dateTime,
                        format: dateFormat,
                        canSelectTime: true,
                        initialDate: DateTime.now(),
//                        initialTime:  TimeOfDay.now(),
                        firstDate: DateTime(1900),
                        lastDate: DateTime(2100),
                        decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.timer,
                            color:
                                _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xfff5f5f5) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                          ),
                          contentPadding: EdgeInsets.symmetric(vertical: 15),
                          errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(1.0),
                            borderSide: BorderSide(width: 1, color: Colors.black38),
                          ),
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.black38, width: 1),
                          ),
                          filled: true,
                          fillColor:
                              _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                          hintText: 'Select Date Time',
                          hintStyle: TextStyle(
                            fontSize: ScreenUtil().setHeight(14),
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.normal,
                            fontStyle: FontStyle.normal,
                            letterSpacing: ScreenUtil().setWidth(0.25),
                            color:
                                _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xfff5f5f5) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: ScreenUtil().setHeight(24)),
                Row(
                  children: [
                    SizedBox(width: ScreenUtil().setWidth(24)),
                    Theme(
                      data: Theme.of(context).copyWith(inputDecorationTheme: InputDecorationTheme(border: OutlineInputBorder(borderRadius: BorderRadius.circular(1)))),
                      child: Container(
                        width: ScreenUtil().setWidth(214),
                        // decoration: BoxDecoration(
                        //   borderRadius: BorderRadius.circular(5.0),
                        //   border: Border.all(color: Theme.of(context).textTheme.bodyText1.color, style: BorderStyle.solid),
                        // ),
                        child: DropdownFieldBlocBuilder<YardModel>(
                          selectFieldBloc: formBloc.yardDropDown,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor:
                                _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                            //  enabledBorder: InputBorder.none,
                            // contentPadding: EdgeInsets.only(left: 10),
                            labelText: 'Select Yard',
                            //     prefixIcon: Icon(Icons.train),
                          ),
                          itemBuilder: (context, value) => value.yardName,
                        ),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(24)),
                    Theme(
                      data: Theme.of(context).copyWith(inputDecorationTheme: InputDecorationTheme(border: OutlineInputBorder(borderRadius: BorderRadius.circular(1)))),
                      child: Container(
                        width: ScreenUtil().setWidth(214),
                        // decoration: BoxDecoration(
                        //   borderRadius: BorderRadius.circular(5.0),
                        //   border: Border.all(color: Theme.of(context).textTheme.bodyText1.color, style: BorderStyle.solid),
                        // ),
                        child: DropdownFieldBlocBuilder<TrackModel>(
                          selectFieldBloc: formBloc.trackDropDown,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor:
                                _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                            // enabledBorder: InputBorder.none,
                            // contentPadding: EdgeInsets.only(left: 10),
                            labelText: 'Select Track',
                            // hintText: "Select Track"
                            //   prefixIcon: Icon(Icons.train),
                          ),
                          itemBuilder: (context, value) => value.trackName,
                        ),
                      ),
                    ),    SizedBox(width: ScreenUtil().setWidth(24)),

                     Theme(
                       data: Theme.of(context).copyWith(
                           inputDecorationTheme: InputDecorationTheme(
                               border: OutlineInputBorder(
                                   borderRadius: BorderRadius.circular(1)))),
                       child: Container(
                         width: ScreenUtil().setWidth(214),
                         // decoration: BoxDecoration(
                         //   borderRadius: BorderRadius.circular(5.0),
                         //   border: Border.all(color: Theme.of(context).textTheme.bodyText1.color, style: BorderStyle.solid),
                         // ),
                         child: DropdownFieldBlocBuilder<SpotModel>(
                           selectFieldBloc: formBloc.spotDropDown,
                           decoration: InputDecoration(
                             filled: true,
                             fillColor:
                             _themeChanger
                                 .getTheme()
                                 .primaryColor == Color(0xff182e42) ? Color(
                                 0xff172636) : _themeChanger
                                 .getTheme()
                                 .primaryColor == Color(0xfff5f5f5) ? Color(
                                 0xfff5f5f5) : null,
                             // enabledBorder: InputBorder.none,
                             // contentPadding: EdgeInsets.only(left: 10),
                             labelText: 'Select Spot',
                             // hintText: "Select Track"
                             //   prefixIcon: Icon(Icons.train),
                           ),
                           itemBuilder: (context, value) => value.spotName,
                         ),
                       ),
                     ) ,

                  ],
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(10),
                ),
                RadioButtonGroupFieldBlocBuilder<String>(
                  selectFieldBloc: formBloc.sequenceOfCar,
                  itemBuilder: (context, value) => value,
                  labelStyle: TextStyle(
                    color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Colors.black : null,
                  ),
                  decoration: InputDecoration(
                    // fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xff172636):
                    // _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)? Color(0xfff5f5f5):null,
                    labelStyle: TextStyle(
                        color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Colors.black : null,
                        fontSize: ScreenUtil().setSp(20, allowFontScalingSelf: true),
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Roboto',
                        height: 0,
                        decorationThickness: 0),
                    errorBorder: InputBorder.none,
                    border: InputBorder.none,
                    labelText: 'Sequence of Railcar',
                    contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 30),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
