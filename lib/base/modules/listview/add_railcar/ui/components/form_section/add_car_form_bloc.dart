import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/modules/listview/add_railcar/repository/addrailcar_repo.dart';

class AddCarFormBloc extends FormBloc<String, String> {
  final AddRailCarRepo _addRailCarRepository;
  bool isStatusChanged = false;
  List<SpotModel> spotList=[];

  final yardDropDown = SelectFieldBloc<YardModel, dynamic>(
    name: "yard_id",
    toJson: (value) => value.toMap(),
    validators: [FieldBlocValidators.required],
    initialValue: MemoryData.inMemoryYardMap[MemoryData?.facilityConfiguration?.defaultInboundYardId],
  );

  final trackDropDown = SelectFieldBloc<TrackModel, dynamic>(
    name: "track_id",
    toJson: (value) => value.toMap(),
    validators: [FieldBlocValidators.required],
    initialValue: MemoryData.inMemoryTrackMap[MemoryData?.facilityConfiguration?.defaultInboundTrackId],
  );

  final spotDropDown = SelectFieldBloc<SpotModel, dynamic>(
    name: "spot_id",
    toJson: (value) => value,
    validators: [ ],
    initialValue: null,
  );

  final equipInit = TextFieldBloc(name: 'equipInit', validators: [FieldBlocValidators.required]);
  final equipNum = TextFieldBloc(name: 'equipNum', validators: [FieldBlocValidators.required]);

  final sequenceOfCar = SelectFieldBloc(
    name: 'Sequence',
    items: ['Front', 'Back'],
    validators: [FieldBlocValidators.required],
  );

  final dateTime = InputFieldBloc<DateTime, Object>(name: 'inboundDateTime', toJson: (value) => value.toUtc(), validators: [FieldBlocValidators.required],initialValue: DateTime.now());

  AddCarFormBloc(this._addRailCarRepository) {
    _loadDropDownDataAndListen();
    addFieldBlocs(
      fieldBlocs: [
        yardDropDown,
        trackDropDown,
        spotDropDown,
        equipInit,
        equipNum,
        sequenceOfCar,
        dateTime,
      ],
    );
  }

  _loadDropDownDataAndListen() async {
    List<YardModel> yardList = MemoryData.inMemoryYardMap.values.toList();
    yardList.sort((a,b )=> a.yardName.compareTo(b.yardName));
    List<TrackModel> tracks = MemoryData.inMemoryTrackMap.values.toList();
    tracks.sort((a,b )=> a.trackName.compareTo(b.trackName));
    List<SpotModel> spots = MemoryData.inMemorySpotMap.values.toList();
    spots.sort((a,b )=> a.spotName.compareTo(b.spotName));

    yardList.forEach((element) => yardDropDown.addItem(element));
    var filteredTracks = tracks.where((element) => element.yardId == MemoryData.facilityConfiguration.defaultInboundYardId).toList();

    filteredTracks.forEach((element) => trackDropDown.addItem(element));
    yardDropDown.listen((covariant) async {
      //trackDropDown.clear();
      var filteredTracks2 = tracks.where((element) => element.yardId == yardDropDown?.value?.id).toList();
      print("filteredTracks2 length:${filteredTracks2.length}, covariant.isInitial:${covariant.isInitial}");
      !covariant.isInitial ? trackDropDown.updateItems(filteredTracks2) : null;
//      !covariant.isInitial ? trackDropDown.updateValue(null) : null;
    });

    trackDropDown.listen((covariant) async {
      //trackDropDown.clear();
      print("spots length:${spots.length},  ");
      var filteredSpots2 = spots.where((element) => element.trackId == trackDropDown?.value?.id).toList();
      spotList=filteredSpots2;
      print("filteredSpots2 length:${filteredSpots2.length}, covariant.isInitial:${covariant.isInitial}");
      !covariant.isInitial ? spotDropDown.updateItems(filteredSpots2) : null;
//      !covariant.isInitial ? trackDropDown.updateValue(null) : null;
    });

    this.listen((covariant) async {
      if (yardDropDown.value != null || trackDropDown.value != null || !equipInit.value.trim().isEmpty || !equipNum.value.trim().isEmpty || sequenceOfCar.value != null || dateTime.value != null) {
        isStatusChanged = true;
      } else {
        isStatusChanged = false;
      }
    });
  }

  @override
  void onSubmitting() async {
    try {
      await _addRailCarRepository.addRailcar(equipInit.value, equipNum.value, yardDropDown.value.id, trackDropDown.value.id, spotDropDown.value?.id,dateTime.value, sequenceOfCar.value);
      emitSuccess(successResponse: "Railcar Added Successfully");
    } catch (exception) {
      print("Add car saving error >>>${exception.toString()}");
      emitFailure(failureResponse: exception.toString());
    }
  }
}
