import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';

popUpDialog(context, source) async {
  final action = await CustomDialog.cstmDialog(context, "add_railcar", "Unsaved Changes", "");
  if (action[0] == DialogAction.yes) {
    Navigator.pop(context);
    if (APP_CONST.YARD_VIEW == source)  {
      BlocProvider.of<YardViewBloc>(context).add(YardViewFetchDataWithLastSetCriteria());
    } else {
      BlocProvider.of<ListViewBloc>(context).add(ListViewFetchDataWithLastSetCriteria());
    }
  }
}
