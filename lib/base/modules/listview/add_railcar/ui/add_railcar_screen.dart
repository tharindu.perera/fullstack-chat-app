import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/modules/listview/add_railcar/ui/components/form_section/add_car_form_section.dart';

class AddCar extends StatelessWidget {
  String source;

  AddCar(String source) {
    this.source = source;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(24),
          vertical: ScreenUtil().setHeight(24)),
      child: CardUI(
          content: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: AddCarForm(this.source),
      )),
    );
  }
}
