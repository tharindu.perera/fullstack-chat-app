import 'dart:async';

import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/util/nullCheckUtil.dart';
import 'package:mbase/base/modules/listview/actions/comment/dao/comment_dao.dart';
import 'package:mbase/env.dart';

class ListViewRepository {
  EquipmentDAO equipmentDAO = EquipmentDAO();
  CommentDAO commentDAO = CommentDAO();

  Stream<List<Equipment>> getRecordByCriteria(Map<String, dynamic> criteriaFromListGrid, ascending, sortColumnIndex) {
    print("criteriaFromListGrid>> $criteriaFromListGrid");
    print("ascending>> $ascending");
    print("sortColumnIndex>> $sortColumnIndex");
    Map<String, dynamic> criteria = Map();
    criteria.putIfAbsent("permission_id", () => [env.userProfile.id, APP_CONST.PUBLIC]);
    criteriaFromListGrid != null ? criteria.addAll(criteriaFromListGrid) : null;

    var text = (criteria != null && criteria.containsKey("textSearch") && criteria["textSearch"].toString().trim() != "") ? criteria["textSearch"] : null;
    criteria?.remove("textSearch");
    if (text != null) {
      StreamController<List<Equipment>> controller = StreamController<List<Equipment>>();

      var mapStream = (equipmentDAO.getRecordByCriteria(criteria)).map((list) {
        List<Equipment> temp = [];
        List<String> removableIdlist = List();
        list.forEach((equipment) async {
          if (equipment.equipmentInitial.startsWith(text)) {
            temp.add(equipment);
          } else if (equipment.equipmentInitial.contains(text)) {
            temp.add(equipment);
          } else if ((equipment.equipmentInitial + " " + equipment.equipmentNumber).contains(text)) {
            temp.add(equipment);
          } else if ((equipment.equipmentInitial + equipment.equipmentNumber).contains(text)) {
            temp.add(equipment);
          } else if (equipment.equipmentNumber.contains(text)) {
            temp.add(equipment);
          } else if (MemoryData.inMemoryTrackMap[equipment.trackId] != null && MemoryData.inMemoryTrackMap[equipment.trackId].trackName.toUpperCase().startsWith(text)) {
            temp.add(equipment);
          } else if (MemoryData.inMemorySpotMap[equipment.spotId] != null && MemoryData.inMemorySpotMap[equipment.spotId].spotName.toUpperCase().startsWith(text)) {
            temp.add(equipment);
          }
          //adding comments to listview view comment
          //commentDAO.getCommentsForEquipment(equipment);
          // List<Comment> commentsList = await commentDAO.getCommentsForEquipment(equipment);
          // equipment.comments.addAll(commentsList);
        });
        criteria["textSearch"] = text;
        temp.forEach((element) {
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.OUTBOUND_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.id)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.LOAD_EQUIPMENT_MASTER_REC_UPDATE)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.UNLOAD_EQUIPMENT_MASTER_REC_UPDATE)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.LOAD_CAR_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.UNLOAD_CAR_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.MOVE_RAILCAR)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;

          (element.isOutbounded) ? removableIdlist.add(element.id) : null;
        });
        temp.removeWhere((element) {
          return removableIdlist.contains(element.id);
        });
        _sortData(temp, ascending, sortColumnIndex);

        //adding comments to listview view comment
        commentDAO.getComments().then((comments) {
          temp.forEach((equipment) {
            comments.forEach((cmnt) {
              if (cmnt.assetMasterId != null && equipment.assetMasterId != null && cmnt.assetMasterId == equipment.assetMasterId) {
                if (cmnt.commentType == "Permanent") {
                  equipment.comments.add(cmnt);
                } else if (cmnt.commentType != "Permanent" && cmnt.facilityVisitId == equipment.facilityVisitId) {
                  equipment.comments.add(cmnt);
                }
              }
            });
          });
          controller.add(temp);
        });

        return temp;
      });
      mapStream.listen((event) {
        controller.add(event);
      });
      return controller.stream;
    }
    else {
      print("else ...${criteria}");
      StreamController<List<Equipment>> controller = StreamController<List<Equipment>>();
      var mapStream;
        mapStream = equipmentDAO.getRecordByCriteria(criteria).map((event) {
        List<Equipment> temp = [];
        List<String> removableIdlist = List();
        temp.addAll(event);
        temp.forEach((element) {
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.OUTBOUND_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.id)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.LOAD_EQUIPMENT_MASTER_REC_UPDATE)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.UNLOAD_EQUIPMENT_MASTER_REC_UPDATE)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.LOAD_CAR_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.UNLOAD_CAR_OPERATION)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;
          (element.transactionStatus == APP_CONST.TRANSACTION_STATUS_PENDING && element.operationType == APP_CONST.MOVE_RAILCAR)
              ? {removableIdlist.add(element.parent_id), removableIdlist.add(element.immediateParentId)}
              : null;

          (element.isOutbounded) ? removableIdlist.add(element.id) : null;

          //adding comments to listview view comment

          // List<Comment> commentsList = await commentDAO.getCommentsForEquipment(element);
          //   element.comments.addAll(commentsList);
        });
        temp.removeWhere((element) {
          return removableIdlist.contains(element.id);
        });
        _sortData(temp, ascending, sortColumnIndex);

        commentDAO.getComments().then((comments) {
          temp.forEach((equipment) {
            comments.forEach((cmnt) {
              if (cmnt.assetMasterId != null && equipment.assetMasterId != null && cmnt.assetMasterId == equipment.assetMasterId) {
                if (cmnt.commentType == "Permanent") {
                  equipment.comments.add(cmnt);
                } else if (cmnt.commentType != "Permanent" && cmnt.facilityVisitId == equipment.facilityVisitId) {
                  equipment.comments.add(cmnt);
                 }
              }
            });
          });
          controller.add(temp);
        });

        return temp;
      });

      //adding comments to listview view comment
      mapStream.listen((event) {
        controller.add(event);
      });
      return controller.stream;
    }
  }

  _sortData(List<Equipment> list, ascending, sortColumnIndex) {
    if (sortColumnIndex == 0) {
      ascending
          ? list.sort((a, b) => (a.equipmentInitial + a.equipmentNumber).compareTo(b.equipmentInitial + b.equipmentNumber))
          : list.sort((a, b) => ((b.equipmentInitial + b.equipmentNumber).compareTo(a.equipmentInitial + a.equipmentNumber)));
    } else if (sortColumnIndex == 1) {
      ascending
          ? list.sort((a, b) => (MemoryData.inMemoryTrackMap[a.trackId].trackName + "/" + setNullToEmptyString(MemoryData.inMemorySpotMap[a.spotId]?.spotName))
              .compareTo(MemoryData.inMemoryTrackMap[b.trackId].trackName + "/" + setNullToEmptyString(MemoryData.inMemorySpotMap[b.spotId]?.spotName)))
          : list.sort((a, b) => (MemoryData.inMemoryTrackMap[b.trackId].trackName + "/" + setNullToEmptyString(MemoryData.inMemorySpotMap[b.spotId]?.spotName))
              .compareTo(MemoryData.inMemoryTrackMap[a.trackId].trackName + "/" + setNullToEmptyString(MemoryData.inMemorySpotMap[a.spotId]?.spotName)));
    } else if (sortColumnIndex == 2) {
      ascending
          ? list.sort((a, b) => convertStringToInt(a.sequenceRailcar).compareTo(convertStringToInt(b.sequenceRailcar)))
          : list.sort((a, b) => convertStringToInt(b.sequenceRailcar).compareTo(convertStringToInt(a.sequenceRailcar)));
    } else if (sortColumnIndex == 3) {
      ascending ? list.sort((a, b) => a.placedDate.compareTo(b.placedDate)) : list.sort((a, b) => b.placedDate.compareTo(a.placedDate));
    } else if (sortColumnIndex == 4) {
      ascending
          ? list.sort((a, b) => setNullToEmptyString(a.equipmentType).compareTo(setNullToEmptyString(b.equipmentType)))
          : list.sort((a, b) => setNullToEmptyString(b.equipmentType).compareTo(setNullToEmptyString(a.equipmentType)));
    } else if (sortColumnIndex == 5) {
      ascending
          ? list.sort((a, b) => setNullToEmptyString(a.compartmentList.length > 0 ? a.compartmentList[0].productName : null)
              .compareTo(setNullToEmptyString(b.compartmentList.length > 0 ? b.compartmentList[0].productName : null)))
          : list.sort((a, b) => setNullToEmptyString(b.compartmentList.length > 0 ? b.compartmentList[0].productName : null)
              .compareTo(setNullToEmptyString(a.compartmentList.length > 0 ? a.compartmentList[0].productName : null)));
    } else if (sortColumnIndex == 6) {
      ascending
          ? list.sort((a, b) => setNullToEmptyString(a.compartmentList.length > 0 ? a.compartmentList[0].currentAmount : null)
              .compareTo(setNullToEmptyString(b.compartmentList.length > 0 ? b.compartmentList[0].currentAmount : null)))
          : list.sort((a, b) => setNullToEmptyString(b.compartmentList.length > 0 ? b.compartmentList[0].currentAmount : null)
              .compareTo(setNullToEmptyString(a.compartmentList.length > 0 ? a.compartmentList[0].currentAmount : null)));
    } else if (sortColumnIndex == -1) {
      //default sorting by alphabetical Yard Name -> Track name -> Car's sequence number
      try {
        List<Equipment> sortedList = new List();

        //filter out yard list
        Set<String> yardIdList= list.map((e) => e.yardId).toSet();

        //try to avoid exceptions on missing yard ids in captured inMemoryYard Map
        Set<YardModel> yardSet = new Set();
        for (String yardId in yardIdList ) {
          if ( MemoryData.inMemoryYardMap.containsKey(yardId)) {
            yardSet.add(MemoryData.inMemoryYardMap[yardId]);
          }
        }

        List<YardModel> yardList = yardSet.toList();
        yardList.sort((a,b) => a.yardName.toLowerCase().compareTo(b.yardName.toLowerCase()));

        //sort by Yard
        for (YardModel yard in yardList) {
            List<Equipment> yardEquipments = list.where((a) => a.yardId == yard.id).toList();
            Set<String> trackIdList= yardEquipments.map((e) => e.trackId).toSet();

            //capture track list
            Set<TrackModel> trackSet = new Set();
            for (String trackId in trackIdList ) {
              if ( MemoryData.inMemoryTrackMap.containsKey(trackId)) {
                trackSet.add(MemoryData.inMemoryTrackMap[trackId]);
              }
            }

            List<TrackModel> trackList = trackSet.toList();
            trackList.sort((a,b) => a.trackName.toLowerCase().compareTo(b.trackName.toLowerCase()));

            //sort by Track
            for (TrackModel track in trackList) {
              List<Equipment> trackEquipments = list.where((a) => a.trackId == track.id).toList();
              //sort by Track's sequence number
              trackEquipments.sort((a,b) => convertStringToInt(a.sequenceRailcar).compareTo(convertStringToInt(b.sequenceRailcar)));
              sortedList.addAll(trackEquipments);
            }
        }
        //swap existing list with sorted one
        list.clear();
        list.addAll(sortedList);
        print('Successful default sorted in listview');
      } catch (error) {
        print('Error in default sort of listview');
      }
    }
  }
}
