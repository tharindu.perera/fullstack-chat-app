class ListViewTableModel {
  int id;
  String railcar;
  String track_spot;
  String spot;
  String placed;
  String carType;
  String product;
  String LE;
  String sequence;

  ListViewTableModel(
      {this.id,
      this.railcar,
      this.track_spot,
      this.spot,
      this.product,
      this.placed,
      this.carType,
      this.LE,
      this.sequence});

  factory ListViewTableModel.fromJson(Map<String, dynamic> value) {
    return ListViewTableModel(
        id: value["id"] as int,
        railcar: value["railcar"] as String,
        track_spot: value["Track/Spot"] as String,
        spot: value["Spot"] as String,
        placed: value["placed"] as String,
        carType: value["carType"] as String,
        product: value["product"] as String,
        LE: value['L/E'] as String,
        sequence: value["sequence"] as String);
  }
}
