class DropDownModel {
  String id;
  String value;

  DropDownModel({this.id, this.value});

  factory DropDownModel.fromJSON(Map<String, dynamic> json) {
    return DropDownModel(id: json['id'], value: json['value']);
  }
}
