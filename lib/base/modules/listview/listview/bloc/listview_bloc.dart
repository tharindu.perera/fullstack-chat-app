import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_state.dart';
import 'package:mbase/base/modules/listview/listview/repository/listview_repository.dart';
import 'package:mbase/base/modules/listview/listview/ui/components/list_grid/list_grid_ds.dart';

class ListViewBloc extends Bloc<ListViewEvent, ListViewState> {
  ListViewRepository _repository;
  Map<String, dynamic> _lastSearchCriteria;
  StreamSubscription streamSubscription;
  bool ascending = true;
  int sortColumnIndex = 0;
  List<Map<String,dynamic>> filterConfiguration = [];

  ListViewBloc(this._repository) : super(ListViewInitState()) {}

  @override
  Stream<ListViewState> mapEventToState(ListViewEvent event) async* {
    if (event is ListViewInitialDataLoad) {
      print("ListViewFetchData INITIAL DATA LOAD-------------------------------------");
      yield* _mapListViewFetchDataWithCriteria(null);
    }

    if (event is ListViewFetchDataWithCriteria) {
      this._lastSearchCriteria = event.criteria;
      filterConfiguration = [];
      print("filter event criteria ----> ${event.criteria}");
      filterConfiguration.add(event.criteria);
      print("ListViewFetchDataWithCriteria--------------------------------------");
      yield* _mapListViewFetchDataWithCriteria(event.criteria);
    }

    if (event is ListViewFetchDataWithLastSetCriteria) {
      print("ListViewFetchDataWithLastSetCriteria--------------------------------------");
      yield* _mapListViewFetchDataWithCriteria(this._lastSearchCriteria);
    }

    if (event is ListViewGridSortedClicked) {
      print("ListviewSortedClicked--------------------------------------");
      yield* _mapListViewFetchDataWithCriteria(this._lastSearchCriteria,
          ascending: event.ascending, sortColumnIndex: event.columnIndex);
    }

    if (event is ListViewRefreshData) {
      yield ListViewInProgress();
      print("ListViewRefresh,,,--------------------------------------${event.ds.inputList.length}");
      yield ListViewDataLoaded(dataSource: event.ds);
    }
  }

  Stream<ListViewState> _mapListViewFetchDataWithCriteria(criteria,
      {ascending: true, sortColumnIndex: -1}) async* {
    this.ascending = ascending;
    this.sortColumnIndex = sortColumnIndex;
    yield ListViewInProgress();
    streamSubscription?.cancel();
    streamSubscription = _repository
        .getRecordByCriteria(criteria, ascending, sortColumnIndex)
        .listen((event) async {
      add(ListViewRefreshData(ListGridDataSource(event)));
//      streamSubscription?.cancel();
    });
  }
}
