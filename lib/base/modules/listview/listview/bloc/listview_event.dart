import 'package:flutter/material.dart';
import 'package:mbase/base/modules/listview/listview/ui/components/list_grid/list_grid_ds.dart';

@immutable
abstract class ListViewEvent {
  List<String> filterConfiguration = [];
  ListViewEvent();
  ListViewEvent.filter(filterConfiguration){
    this.filterConfiguration = filterConfiguration;
  }
}

class ListViewInitialDataLoad extends ListViewEvent {
  ListViewInitialDataLoad() : super();
}

// ignore: must_be_immutable
class ListViewFetchDataWithCriteria extends ListViewEvent {
  Map<String, dynamic> criteria;
  ListViewFetchDataWithCriteria(this.criteria, {filterConfiguration}) : super.filter(filterConfiguration);

}
class ListViewFetchDataWithLastSetCriteria extends ListViewEvent {
  ListViewFetchDataWithLastSetCriteria() : super();
}

class ListViewGridSortedClicked extends ListViewEvent {
  var ascending;
  var columnIndex;

  ListViewGridSortedClicked(this.columnIndex, this.ascending) : super();
}

class ListViewRefreshData extends ListViewEvent {
  ListGridDataSource ds;
  ListViewRefreshData(this.ds) : super();
}




