import 'package:mbase/base/modules/listview/listview/ui/components/list_grid/list_grid_ds.dart';

abstract class ListViewState {
  ListGridDataSource dataSource = ListGridDataSource([]);
  ListViewState(this.dataSource);

  @override
  String toString() {
    return this.runtimeType.toString();
  }
}

class ListViewInitState extends ListViewState {
  ListViewInitState({ListGridDataSource dataSource})
      : super(dataSource ?? ListGridDataSource([]));
}

class ListViewInProgress extends ListViewState {
  ListViewInProgress({ListGridDataSource dataSource})
      : super(dataSource ?? ListGridDataSource([]));
}

class ListViewDataLoaded extends ListViewState {
  ListViewDataLoaded({ListGridDataSource dataSource})
      : super(dataSource ?? ListGridDataSource([]));
}
