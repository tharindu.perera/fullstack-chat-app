 import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/screen_factory.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/add_railcar/ui/add_railcar_screen.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_state.dart';
import 'package:mbase/base/modules/listview/listview/repository/listview_repository.dart';
import 'package:mbase/base/modules/listview/listview/ui/components/list_grid/list_grid.dart';
import 'package:mbase/base/modules/listview/listview/ui/components/screen_header.dart';
import 'package:mbase/base/modules/listview/listview/ui/components/search_section/search_view.dart';

class ListViewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ListViewBloc>(
      create: (context) => ListViewBloc(ListViewRepository())..add(ListViewInitialDataLoad()),
      child: BlocBuilder<ListViewBloc, ListViewState>(builder: (context, state) {

        return Scaffold(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24), vertical: ScreenUtil().setHeight(24)),
                    child: CardUI(
                      content: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                              child: Column(
                                children: <Widget>[
                                  ScreenHeader(),
                                  SizedBox(height: ScreenUtil().setHeight(24)),
                                  SizedBox(
                                    width: ScreenUtil().setWidth(928),
                                    child: Divider(thickness: ScreenUtil().setWidth(1)),
                                  ),
                                  SizedBox(height: ScreenUtil().setHeight(24)),
                                  SearchSection(),
                                  // Row(
                                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  //     children: [SearchSection()]),
                                  SizedBox(height: 10.0),
                                  (state is ListViewInProgress) ? CircularProgressIndicator() : ListGrid()
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            floatingActionButton: FloatingActionButton(
              backgroundColor: Color(0xFF508be4),
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () {
                return Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => BlocProvider.value(
                        value: BlocProvider.of<ListViewBloc>(context),
                        child: ScreenFactory(AddCar(APP_CONST.LIST_VIEW)),
                      ),
                    ));
              },
            ));
      }),
    );
  }
}
