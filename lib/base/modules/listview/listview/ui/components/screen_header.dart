import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/components/flyover/flyover.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';

import 'action_button.dart';

class ScreenHeader extends StatefulWidget {
  const ScreenHeader({
    Key key,
  }) : super(key: key);

  @override
  _ScreenHeaderState createState() => _ScreenHeaderState();
}

class _ScreenHeaderState extends State<ScreenHeader> {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ListViewBloc>(context).state.dataSource.addListener(() {
      setState(() => {});
    });

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(24)),
          child: Container(
              child: Text('List View',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.normal,
                      color: Theme.of(context)
                          .textTheme
                          // ignore: deprecated_member_use
                          .headline
                          .color))),
        ),
        SizedBox(width:ScreenUtil().setWidth(500),),
        Container(
          width: ScreenUtil().setWidth(40),
          height: ScreenUtil().setHeight(40),
          child: FlatButton(
            onPressed: BlocProvider.of<ListViewBloc>(context)
                .state
                .dataSource
                .selectedRows
                .isNotEmpty
                ? () {
              showDialog(
                  context: context,
                  builder: (_) {
                    return FlyOver(
                        BlocProvider.of<ListViewBloc>(context)
                            .state
                            .dataSource
                            .selectedRows);
                  });
            }
                : null,
            padding: EdgeInsets.all(0),
            child: Icon(
              Icons.remove_red_eye,
              size: 30,
              color: Colors.white,
            ),
            color: Color(0xFF508be4),
            textColor: Colors.white,
            disabledColor: Colors.grey,
            disabledTextColor: Colors.black,
            splashColor: Color(0xFF508be4),
          ),
        ),
        ActionButtn(),
      ],
    );
  }
}
