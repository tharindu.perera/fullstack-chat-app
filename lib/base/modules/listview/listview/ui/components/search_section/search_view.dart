import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/repositories/track_repository.dart';
import 'package:mbase/base/core/common_base/repositories/yard_repository.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/util/formatter.dart';
import 'package:mbase/base/modules/listview/filters/ui/search_filters.dart';
import 'package:mbase/base/modules/listview/filters/ui/tracks_filter.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/ui/components/search_section/search_bloc.dart';
import 'package:provider/provider.dart';

class SearchSection extends StatefulWidget {
  @override
  _SearchSectionState createState() => _SearchSectionState();
}

class _SearchSectionState extends State<SearchSection> {
  ThemeChanger _themeChanger;

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);
    BlocProvider.of<ListViewBloc>(context).state.dataSource.addListener(() {
      setState(() => {});
    });
    return BlocProvider(
      create: (context) => AllFieldsFormBloc(
          BlocProvider.of<ListViewBloc>(context),
          TrackRepository(),
          YardRepository()),
      child: Builder(
        builder: (context) {
          final formBloc = BlocProvider.of<AllFieldsFormBloc>(context);
          formBloc.yardDropDown.state.items
              .sort((a, b) => a.yardName.compareTo(b.yardName));
          formBloc.trackDropDown.state.items
              .sort((a, b) => a.trackName.compareTo(b.trackName));
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // Container(
              //   child: Text('3 Results',style: TextStyle(
              //     fontSize: 24,
              //     color: Theme.of(context).textTheme.bodyText1.color,
              //     fontStyle: FontStyle.normal,
              //     fontFamily: 'Roboto'
              //   ),),
              // ),
                  SizedBox(width: ScreenUtil().setWidth(250)),
              Column(
                children: [
                  Row(
                    children: [
                      Container(
                        width: ScreenUtil().setWidth(100),
                        height: ScreenUtil().setHeight(40),
                        child: FlatButton(
                          child: Text('Tracks',style: TextStyle(
                            fontFamily: 'Roboto',
                            fontStyle: FontStyle.normal,
                            fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
                            color:Colors.white,
                          ), ),
                          shape: RoundedRectangleBorder(side: BorderSide(color: Theme.of(context).textTheme.bodyText1.color, width: 1, style: BorderStyle.solid), borderRadius: BorderRadius.circular(1)),
                          onPressed:  () {
                            showDialog(
                                context: context,
                                builder: (_) {
                                  return ListviewTracksFilters(
                                    BlocProvider.of<ListViewBloc>(context)
                                        .state
                                        .dataSource
                                        .selectedRows, BlocProvider.of<ListViewBloc>(context),);
                                });
                          },
                          padding: EdgeInsets.all(0),
                          color:Color(0xFF508be4),
                          textColor: Theme.of(context).textTheme.bodyText1.color,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          splashColor: Color(0xFF508be4),
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      Container(
                        width: ScreenUtil().setWidth(100),
                        height: ScreenUtil().setHeight(40),
                        child: FlatButton(
                          child: Text('Filters',style: TextStyle(
                            fontFamily: 'Roboto',
                            fontStyle: FontStyle.normal,
                            fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
                            color: Colors.white,
                          ), ),
                          shape: RoundedRectangleBorder(side: BorderSide(color: Theme.of(context).textTheme.bodyText1.color, width: 1, style: BorderStyle.solid), borderRadius: BorderRadius.circular(1)),
                          onPressed:  () {
                            showDialog(
                                context: context,
                                builder: (_) {
                                  return ListviewSearchFilters(
                                    BlocProvider.of<ListViewBloc>(context)
                                        .state
                                        .dataSource
                                        .selectedRows, BlocProvider.of<ListViewBloc>(context),);
                                });
                          },
                          padding: EdgeInsets.all(0),
                          color:Color(0xFF508be4),
                          textColor: Theme.of(context).textTheme.bodyText1.color,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          splashColor: Color(0xFF508be4),
                        ),
                      ),
                    ],
                  )
              ],),
              // Container(
              //   decoration: BoxDecoration(
              //     borderRadius: BorderRadius.circular(5.0),
              //     border: Border.all(
              //         color: Theme.of(context).textTheme.bodyText1.color,
              //         style: BorderStyle.solid),
              //   ),
              //   width: ScreenUtil().setWidth(214),
              //   child: DropdownFieldBlocBuilder<YardModel>(
              //     selectFieldBloc: formBloc.yardDropDown,
              //     decoration: InputDecoration(
              //         contentPadding:
              //             EdgeInsets.only(left: 10, bottom: 0, top: 0),
              //         enabledBorder: InputBorder.none,
              //         labelText: 'Select Yard',
              //         labelStyle: TextStyle(
              //             color: _themeChanger.getTheme().primaryColor ==
              //                     Color(0xff182e42)
              //                 ? Colors.white
              //                 : _themeChanger.getTheme().primaryColor ==
              //                         Color(0xfff5f5f5)
              //                     ? Colors.black
              //                     : null)
              //         //    prefixIcon: Icon(Icons.train),
              //         ),
              //     itemBuilder: (context, value) => value.yardName,
              //   ),
              // ),
             // SizedBox(width: ScreenUtil().setWidth(5)),
         //     SizedBox(width: ScreenUtil().setWidth(5)),
              Container(
                width: ScreenUtil().setWidth(250),
                alignment: Alignment.center,
                child: TextFieldBlocBuilder(
                  debounceSuggestionDuration: Duration(milliseconds: 500),
                  textAlign: TextAlign.start,
                  textAlignVertical: TextAlignVertical.center,
                  inputFormatters: [
                    UpperCaseFormatter(),
                    LengthLimitingTextInputFormatter(10),
                    FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z,0-9]'))
                  ],
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: ScreenUtil().setHeight(14),
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.normal,
                    letterSpacing: ScreenUtil().setWidth(0.25),
                    color: Theme.of(context).textTheme.bodyText1.color,
                  ),
                  textFieldBloc: formBloc.textSearch,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(1.0),
                    suffixIcon: Icon(
                      Icons.search,
                      color: _themeChanger.getTheme().primaryColor ==
                              Color(0xff182e42)
                          ? Color(0xfff5f5f5)
                          : _themeChanger.getTheme().primaryColor ==
                                  Color(0xfff5f5f5)
                              ? Color(0xff172636)
                              : null,
                    ),
                    errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(1)),
                        borderSide: BorderSide(width: 1, color: Colors.red)),
                    focusedErrorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(1)),
                        borderSide: BorderSide(width: 1, color: Colors.red)),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(1.0),
                      borderSide: BorderSide(
                        width: 1,
                        color: _themeChanger.getTheme().primaryColor ==
                                Color(0xff182e42)
                            ? Color(0xfff5f5f5)
                            : _themeChanger.getTheme().primaryColor ==
                                    Color(0xfff5f5f5)
                                ? Color(0xff172636)
                                : null,
                      ),
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.black38, width: 1),
                    ),
                    filled: true,
                    fillColor: _themeChanger.getTheme().primaryColor ==
                            Color(0xff182e42)
                        ? Color(0xff172636)
                        : _themeChanger.getTheme().primaryColor ==
                                Color(0xfff5f5f5)
                            ? Color(0xfff5f5f5)
                            : null,
                    hintText: 'Search by Railcar, Track, Spot',
                    hintStyle: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: ScreenUtil().setHeight(14),
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.normal,
                      letterSpacing: ScreenUtil().setWidth(0.25),
                      color: Theme.of(context).textTheme.bodyText1.color,
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
