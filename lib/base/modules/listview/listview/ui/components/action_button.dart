import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/dao/outage_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/outage_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/ui/assign_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/ui/assign_defect.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/ui/assign_switchlist.dart';
import 'package:mbase/base/modules/listview/actions/comment/ui/comment.dart';
import 'package:mbase/base/modules/listview/actions/inspect_railcar/ui/inspect_railcar.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/ui/loadcar.dart';
import 'package:mbase/base/modules/listview/actions/move_railcar/ui/move_railcar.dart';
import 'package:mbase/base/modules/listview/actions/outbound_railcar/ui/outbound_railcar.dart';
import 'package:mbase/base/modules/listview/actions/test/ui/test.dart';
import 'package:mbase/base/modules/listview/actions/unloadcar/ui/unloadcar.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/ui/components/bottomsheet_item.dart';
import 'package:mbase/base/modules/load_vcf/repository/load_vcf_repository.dart';
import 'package:mbase/base/modules/load_vcf/ui/load_vcf_edit.dart';
import 'package:mbase/base/modules/seal/update_seal/ui/update_seal.dart';
import 'package:mbase/base/modules/unloadvcf/repository/unloadvcf_repository.dart';
import 'package:mbase/base/modules/unloadvcf/ui/unloadvcf_edit.dart';
import 'package:mbase/env.dart';
import 'package:provider/provider.dart';

class ActionButtn extends StatefulWidget {
  @override
  _ActionButtnState createState() => _ActionButtnState();
}

class _ActionButtnState extends State<ActionButtn> {
  ThemeChanger _themeChanger;

  String selectedRailcar = null;

  FToast fToast;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ListViewBloc>(context).state.dataSource.addListener(() {
      setState(() => {});
    });
    fToast = FToast(context);
    _themeChanger = Provider.of<ThemeChanger>(context);
    //  ListViewBloc listViewBloc = BlocProvider.of<ListViewBloc>(context);
    return Container(
      width: ScreenUtil().setWidth(133),
      height: ScreenUtil().setHeight(50),
      child: FlatButton(
          onPressed: BlocProvider.of<ListViewBloc>(context).state.dataSource.selectedRows.isNotEmpty ? () => showBottomDrawer(context) : null,
          child: Text(
            'ACTIONS',
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.25),
                color: Colors.white),
          ),
          color: Color(0xFF508be4),
          textColor: Colors.white,
          disabledColor: Colors.grey,
          disabledTextColor: Colors.black,
          splashColor: Color(0xFF508be4)),
    );
  }

  void showBottomDrawer(context123) {
    showModalBottomSheet(
        context: context123,
        builder: (context) {
          return Container(
            alignment: Alignment.center,
            width: ScreenUtil().setWidth(1024),
            height: ScreenUtil().setHeight(505),
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  checkBottomItemWithUserProfile("List_View:Move_Railcar")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/move-railcar-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/move-railcar-light.png" : null,
                          title: 'MOVE RAILCAR(S)',
                          ontap: () {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 0) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => MoveRailcar(BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows, APP_CONST.LIST_VIEW),
                                  ));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Inspect_Railcar")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/inspection-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/inspection-light.png" : null,
                          title: 'INSPECT RAILCAR',
                          ontap: () {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 1) {
                              _showToastError([]..add("Please select only one car"), context123);
                            } else if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length == 1) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => InspectRailCar(BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows),
                                  ));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Load_Railcar")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/outbound-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/outbound-light.png" : null,
                          title: 'LOAD RAILCAR(S)',
                          ontap: () {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 0) {
                              var equipments = BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows;
                              List<String> errorList = [];
                              int counter = 0;
                              for (Equipment equipment in equipments) {
                                if (counter++ > 5) {
                                  break;
                                }
                                if (equipment.compartmentList.isNotEmpty && double.parse(equipment.compartmentList[0].currentAmount ?? "0") > 0) {
                                  errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber}");
                                }
                              }
                              if (errorList.isNotEmpty) {
                                _showToastError(
                                    []..add("One or more railcar(s) are loaded. Cannot load already loaded railcar(s)"),
//                                      ..add("Following Railcars Are Already Empty")
//                                      ..addAll(errorList),
                                    context123);
                              } else {
                                Navigator.of(context).pop();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => LoadRailCar(BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows, APP_CONST.LIST_VIEW, context, null, null, null),
                                    ));
                              }
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Unload_Railcar")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/unload-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/unload-light.png" : null,
                          title: 'UNLOAD RAILCAR(S)',
                          ontap: () {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 0) {
                              var equipments = BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows;
                              List<String> errorList = [];
                              int counter = 0;
                              for (Equipment equipment in equipments) {
                                if (counter++ > 5) {
                                  break;
                                }
                                if (equipment.compartmentList.isEmpty || (equipment.compartmentList.isNotEmpty && double.parse(equipment.compartmentList[0].currentAmount ?? "0") <= 0)) {
                                  errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber}");
                                }
                              }
                              if (errorList.isNotEmpty) {
                                _showToastError(
                                    []..add("One or more railcar(s) are empty. Cannot unload empty railcar(s)"),
//                                      ..add("Following Railcars Are Already Empty")
//                                      ..addAll(errorList),
                                    context123);
                              } else {
                                Navigator.of(context).pop();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => UnLoadRailCar(BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows),
                                    ));
                              }
                            }else{
                              print(">>>> U have selected multiple equipents");
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Assign_To_Switchlist")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/switch-list-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/switch-list-light.png" : null,
                          title: 'ASSIGN TO SWITCH LIST',
                          ontap: () {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 0) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<ListViewBloc>(context123),
                                            child: AssignSwitchListPage(BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Assign_Defect")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/assign-defect-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/assign-defect-light.png" : null,
                          title: 'ASSIGN DEFECT',
                          ontap: () {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 1) {
                              _showToastError([]..add("Please select only one car"), context123);
                            } else if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length == 1) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<ListViewBloc>(context123),
                                            child: AssignDefect(BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows, APP_CONST.LIST_VIEW),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Assign_Block")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/assign-block-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/assign-block-light.png" : null,
                          title: 'ASSIGN BLOCK',
                          ontap: () {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 1) {
                              _showToastError([]..add("Please select only one car"), context123);
                            } else if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length == 1) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<ListViewBloc>(context123),
                                            child: AssignBlock(BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows, APP_CONST.LIST_VIEW),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Leave_Comment")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/leave-comment-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/leave-comment-light.png" : null,
                          title: 'LEAVE A COMMENT',
                          ontap: () {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 1) {
                              _showToastError([]..add("Please select only one car"), context123);
                            } else if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length == 1) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<ListViewBloc>(context123),
                                            child: LeaveComment(BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows, APP_CONST.LIST_VIEW),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Seal")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/seal-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/seal-light.png" : null,
                          title: 'ADD SEAL',
                          ontap: () {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 1) {
                              _showToastError([]..add("Please select only one car"), context123);
                            } else if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length == 1) {
                              selectedRailcar = BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows[0].equipmentInitial +
                                  " " +
                                  BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows[0].equipmentNumber;
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<ListViewBloc>(context123),
                                            child: UpdateSeal(BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows[0]),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Load_VCF")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/LoadVCF-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/LoadVCF-light.png" : null,
                          title: 'LOAD VCF',
                          ontap: () async {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 1) {
                              _showToastError([]..add("Please select only one car"), context123);
                            } else if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length == 1) {
                              var equip = BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows[0];
                              ProductModel productModel = ProductModel();
                              bool validProduct = true;
                              if (equip.compartmentList.isNotEmpty && equip.compartmentList[0]?.productId != null) {
                                 productModel = await CommonDao().fetchProductById(equip.compartmentList[0].productId);
                             //    if (productModel != null && productModel.dryOrLiquid != APP_CONST.LIQUID) {
                             //      validProduct = false;
                             //    }

                               }
                             // if (!validProduct) {
                             //   //_showToastError([]..add("Railcar does not have liquid product"), context123);
                             //   return;
                             // }

                              AssetMasterDataModel assetMasterModel = await LoadVCFRepository().fetchAssetMasterData(equip.equipmentInitial, equip.equipmentNumber);
                              OutageModel  outtageModel = await OutageDao().fetchOutageById(equip.assetMasterId);

                              if (null != assetMasterModel && null != assetMasterModel.gallon_capacity && null != assetMasterModel.load_limit && null != outtageModel) {
                                selectedRailcar = equip.equipmentInitial + " " + equip.equipmentNumber;
                                Navigator.of(context).pop();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => BlocProvider.value(
                                          value: BlocProvider.of<ListViewBloc>(context123),
                                          child: ProcessLoadVCF(equip, assetMasterModel,productModel),
                                        )));
                              } else {
                                if (null == assetMasterModel) {
                                  _showToastError([]..add("Railcar does not have Asset Information"), context123);
                                } else if (assetMasterModel.gallon_capacity == null) {
                                  _showToastError([]..add("Railcar does not have Gallon Capacity"), context123);
                                } else if (assetMasterModel.load_limit == null) {
                                  _showToastError([]..add("Railcar does not have Load Limit"), context123);
                                }
                                else if(null == outtageModel){
                                  _showToastError([]..add("Railcar does not have Outage Details"), context123);
                                }
                              }
                            }
                          })
                        : Container(),
                  checkBottomItemWithUserProfile("List_View:Unload_VCF")
                      ? BottomSheetItem(
                      imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                          ? "assets/images/UnloadVCF-dark.png"
                          : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/UnloadVCF-light.png" : null,
                      title: 'UNLOAD VCF',
                      ontap: () async {
                        if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 1) {
                          _showToastError([]..add("Please select only one car"), context123);
                        } else if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length == 1) {
                          var equip = BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows[0];
                          if (equip.compartmentList.isNotEmpty
                              && double.parse(equip.compartmentList[0].currentAmount ?? "0") > 0) {
                            ProductModel productModel = await CommonDao().fetchProductById(equip.compartmentList[0].productId);
                            if (productModel != null && productModel.dryOrLiquid == APP_CONST.LIQUID) {
                              AssetMasterDataModel assetMasterModel = await UnloadvcfRepository().fetchAssetMasterModel(equip.assetMasterId);
                              if (null != assetMasterModel && null != assetMasterModel.gallon_capacity) {
                                selectedRailcar = equip.equipmentInitial + " " + equip.equipmentNumber;
                                Navigator.of(context).pop();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => BlocProvider.value(
                                          value: BlocProvider.of<ListViewBloc>(context123),
                                          child: EditUnloadVcf(equip, assetMasterModel),
                                        )));
                              } else {
                                _showToastError([]..add("Railcar does not have Gallon Capacity"), context123);
                              }
                            } else {
                              _showToastError([]..add("Railcar does not have liquid product"), context123);
                            }

                          } else {
                            _showToastError([]..add("Please select only a loaded car"), context123);
                          }
                        }
                      })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Outbound_Railcar")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/outbound-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/outbound-light.png" : null,
                          title: 'OUTBOUND RAILCAR(S)',
                          ontap: () {
                            if (BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows.length > 0) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<ListViewBloc>(context123),
                                            child: OutboundRailCar(BlocProvider.of<ListViewBloc>(context123).state.dataSource.selectedRows, APP_CONST.LIST_VIEW),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Test")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/outbound-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/outbound-light.png" : null,
                          title: 'TEST',
                          ontap: () {
                            Navigator.of(context).pop();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BlocProvider.value(
                                          value: BlocProvider.of<ListViewBloc>(context123),
                                          child: Test(),
                                        )));
                          })
                      : Container(),
                ],
              ),
            ),
          );
        });
  }

  bool checkBottomItemWithUserProfile(String screenActionID) {
    return env.userProfile.screenActionIdList[env.userProfile.facilityId].contains(screenActionID);
  }

  void _showToastError(List<String> errorList, context) {
    fToast = FToast(context);
    Widget toast = Container(
        width: ScreenUtil().setWidth(552),
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
        decoration: BoxDecoration(
          color: Colors.redAccent,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: errorList.map((e) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.warning),
                SizedBox(
                  width: 12.0,
                ),
                Expanded(
                  child: Text(
                    e,
                    style: TextStyle(
                      // ignore: deprecated_member_use
                      color: Theme.of(context).textTheme.body1.color,
                    ),
                  ),
                ),
              ],
            );
          }).toList(),
        ));

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }
}
