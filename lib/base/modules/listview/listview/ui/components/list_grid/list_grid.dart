import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_paginated_datatable.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:mbase/base/modules/listview/listview/ui/components/list_grid/list_grid_ds.dart';
import 'package:provider/provider.dart';

class ListGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ListViewBloc listViewBloc = BlocProvider.of<ListViewBloc>(context);
    ListGridDataSource listGridDataSource = listViewBloc.state.dataSource;
    listGridDataSource.context=context;
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    return CustomPaginatedDataTable(
      sortArrowColor: Colors.white,
      cardBackgroundColor: Theme.of(context).canvasColor,
      footerTextColor: Theme.of(context)
          .textTheme
          // ignore: deprecated_member_use
          .body1
          .color,
      rowColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff1d2e40) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xffffffff) : null,

      columnColor: Color(0xff274060),
      checkboxBorderColor:
          _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color.fromRGBO(0, 0, 0, 0.54) : null,
      dataRowHeight: ScreenUtil().setHeight(50),
      // headingRowHeight: 60,
      horizontalMargin: ScreenUtil().setWidth(19),
      columnSpacing: ScreenUtil().setWidth(30),
      header: SizedBox(
        width: 1,
        height: 1,
      ),
      columns: [
        CustomDataColumn(
          onSort: (int columnIndex, bool ascending) {
            listViewBloc.add(ListViewGridSortedClicked(columnIndex, ascending));
          },
          label: Row(
            children: [
              // Container(
              //   child: GestureDetector(
              //       onTap: () {
              //         // Navigator.push(
              //         //     context,
              //         //     MaterialPageRoute(
              //         //         builder:
              //         //             (context) =>
              //         //                 ViewComment()));
              //         // ignore: deprecated_member_use
              //       },
              //       child: Icon(
              //         Icons.message,
              //         // ignore: deprecated_member_use
              //         color: Color.fromRGBO(255, 255, 255, 0.54),
              //         size: 18,
              //       )),
              // ),
              SizedBox(
                width: ScreenUtil().setWidth(26),
              ),
              Container(
                  child: Text(
                "RAILCAR",
                style: TextStyle(
                  fontFamily: 'Roboto',
                  // ignore: deprecated_member_use
                  color: Colors.white,
                  fontSize: ScreenUtil().setHeight(15),
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.normal,
                  letterSpacing: ScreenUtil().setWidth(1.75),
                ),
              ))
            ],
          ),
        ),
        CustomDataColumn(
          label: Text("TRACK / SPOT",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )
          ),
          numeric: false,
          onSort: (int columnIndex, bool ascending) {
            listViewBloc.add(ListViewGridSortedClicked(columnIndex, ascending));
          },
        ),
        CustomDataColumn(
          label: Text("SEQ",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )
          ),
          numeric: false,
          onSort: (int columnIndex, bool ascending) {
            listViewBloc.add(ListViewGridSortedClicked(columnIndex, ascending));
          },
        ),
        CustomDataColumn(
          label: Text("PLACED",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
          onSort: (int columnIndex, bool ascending) {
            listViewBloc.add(ListViewGridSortedClicked(columnIndex, ascending));
          },
        ),
        CustomDataColumn(
          label: Text("CAR TYPE",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
          onSort: (int columnIndex, bool ascending) {
            listViewBloc.add(ListViewGridSortedClicked(columnIndex, ascending));
          },
        ),
        CustomDataColumn(
          label: Text("PRODUCT",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
          onSort: (int columnIndex, bool ascending) {
            listViewBloc.add(ListViewGridSortedClicked(columnIndex, ascending));
          },
        ),
        CustomDataColumn(
          label: Text("L / E",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
          onSort: (int columnIndex, bool ascending) {
            listViewBloc.add(ListViewGridSortedClicked(columnIndex, ascending));
          },
        ),
      ],
      source: listGridDataSource ?? [],
      sortAscending: listViewBloc.ascending,
      sortColumnIndex: listViewBloc.sortColumnIndex,
      rowsPerPage: 5,
    );
  }
}
