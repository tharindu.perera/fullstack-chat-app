import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_data_table_souce.dart';
import 'package:mbase/base/modules/listview/actions/view_comments/ui/view_comment.dart';
class ListGridDataSource extends CustomDataTableSource {
  final formatter = new DateFormat('MM/dd/yyyy HH:mm');
  BuildContext context;
  final List<Equipment> inputList;
  List<Equipment> selectedRows = [];
  int _selectedCount = 0;
//  ThemeChanger _themeChanger = Provider.of<ThemeChanger>(navigatorKey.currentContext,listen: false);


  ListGridDataSource(this.inputList,);

  onSelectedRow(bool selected, Equipment equipment) async {
    print("Selected equipment in Listview screen ${equipment}");
    if (selectedRows.contains(equipment)) {
      notifyListeners();
      selectedRows.remove(equipment);
    } else {
      selectedRows.add(equipment);
    }
    notifyListeners();
  }

  @override
  CustomDataRow getRow(int index) {
    final cellVal = inputList[index];
    return CustomDataRow.byIndex(
        index: index,
        selected: cellVal.isSelected,
        onSelectChanged: (bool value) {
          if (cellVal.isSelected != value) {
            cellVal.isSelected = value;
            notifyListeners();
          }
          onSelectedRow(value, cellVal);
        },
        cells: [
          CustomDataCell(
              Row(
            children: [
              Container(
                child: GestureDetector(
                    onTap: () {
                      cellVal.comments.isNotEmpty?
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ViewComment(selectedRow: cellVal,))):null;
                    },
                    child: Icon(
                      Icons.message,
                      size: 18,
                      color:cellVal.comments.isNotEmpty ? Color(0xFF508be4):Colors.grey,
                    )),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(26),
              ),
              Container(
                  child: Text(
                '${cellVal.equipmentInitial ?? ''} ${cellVal.equipmentNumber ?? ''}',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  // ignore: deprecated_member_use
                  //color: Theme.of(context).textTheme.body1.color,
                  fontSize: ScreenUtil().setHeight(15),
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.normal,
                  letterSpacing: ScreenUtil().setWidth(1.75),
                ),
              ))
            ],
          )),
          CustomDataCell(
              Text(
            '${MemoryData.inMemoryTrackMap[cellVal.trackId]?.trackName ?? cellVal.trackId ?? '-'} '
            '/ ${MemoryData.inMemorySpotMap[cellVal.spotId]?.spotName ?? cellVal.spotId ?? '-'}',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: ScreenUtil().setHeight(15),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal,
              letterSpacing: ScreenUtil().setWidth(1.75),
            ),
          )),
          CustomDataCell(Container(
            width: 110,
            child: Text(
              '${cellVal.sequenceRailcar ?? '-'}',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(15),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.75),
              ),
            ),
          )),
          CustomDataCell(Text(
            '${formatter.format(DateTime.parse(cellVal.placedDate.toString())) != null ? formatter.format(DateTime.parse(cellVal.placedDate.toString())) : '-'}',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: ScreenUtil().setHeight(15),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal,
              letterSpacing: ScreenUtil().setWidth(1.75),
            ),
          )),
          CustomDataCell(Container(
            width: 110,
            child: Text(
              '${cellVal.equipmentType ?? '-'}',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(15),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.75),
              ),
            ),
          )),
          CustomDataCell(Container(
            width: 80,
            child: Text(
              cellVal.compartmentList.isNotEmpty && double.parse(cellVal.compartmentList[0].currentAmount ?? "0") > 0 ? cellVal.compartmentList[0].productName : '-',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(15),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.75),
              ),
            ),
          )),
          CustomDataCell(Row(
            children: [
              Container(
                child: Icon(
                  Icons.lens,
                  color: Color(0xff57657a),
                  size: 10,
                ),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(8),
              ),
              Container(
                child: Text(
                  cellVal.compartmentList.isNotEmpty && double.parse(cellVal.compartmentList[0].currentAmount ?? "0") > 0 ? "L" : 'E',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: ScreenUtil().setHeight(15),
                    // ignore: deprecated_member_use
                    //color: Theme.of(context).textTheme.body1.color,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                    letterSpacing: ScreenUtil().setWidth(1.75),
                    //color:Theme.of(context).textTheme.body1.color
                  ),
                ),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(40),
              ),
//              Container(
//                child: GestureDetector(
//                  onTap: () {},
//                  child: Icon(
//                    Icons.keyboard_arrow_right,
//                    //   color:Theme.of(context).textTheme.body1.color,
//                  ),
//                ),
//              )
            ],
          ))
        ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => inputList.length ?? 0;

  @override
  int get selectedRowCount => _selectedCount ?? 0;
}
