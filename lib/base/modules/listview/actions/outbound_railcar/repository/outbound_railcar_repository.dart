import 'package:flutter/material.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/facility_configuration_model.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/model/assigned_defect.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/repository/assign_defect_repository.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';

class OutboundRailCarRepository {
  EquipmentDAO equipmentDAO = EquipmentDAO();
  NotificationRepository notificationRepository = NotificationRepository();

  Future<Map<String, List<String>>> checkBeforeOutbound(
      List<Equipment> selectedEquipments) async {
    print(
        "start checkBeforeOutbound() method selectedEquipments length:${selectedEquipments.length}");

    Map<String, List<String>> map = {};
    List<String> errorList = [];
    List<String> warningList = [];
    FacilityConfigurationModel fcModel = MemoryData.facilityConfiguration;

    print("fc config outboundRailCarDefect:${fcModel.outboundRailCarDefect}");
    for (Equipment equipment in selectedEquipments) {
      if ("Error" == fcModel.outboundRailCarDefect ||
          "Warning" == fcModel.outboundRailCarDefect) {
        AssignDefectRepository defRepo = new AssignDefectRepository();
        List<AssignedDefect> defectList =
            await defRepo.fetchRailcarDefectsByRailcar(equipment.assetMasterId);
        print("defectList ${defectList.length}");
        if (!defectList.isEmpty) {
          "Error" == fcModel.outboundRailCarDefect
              ? errorList.add(
                  "${equipment.equipmentInitial + " " + equipment.equipmentNumber}")
              : warningList.add(
                  "${equipment.equipmentInitial + " " + equipment.equipmentNumber} ");
        }
      }
    }
    print("Error list:${errorList.length}, Warning list:${warningList.length}");
    map["error"] = errorList;
    map["warning"] = warningList;
    return map;
  }

  Future<void> outboundRailcar(List<Equipment> selectedCarList,
      DateTime outboundedDate, TimeOfDay outboundedTime) async {
    outboundedDate = outboundedDate.add(
        Duration(hours: outboundedTime.hour, minutes: outboundedTime.minute));

    selectedCarList.forEach((equipmentModel) async {
      equipmentModel.outboundedDateTime = outboundedDate;
      equipmentModel.transactionStatus = APP_CONST.TRANSACTION_STATUS_PENDING;
      equipmentModel.source = APP_CONST.MOBILITY;
      equipmentModel.parent_id = equipmentModel.id;
      equipmentModel.operationType = APP_CONST.OUTBOUND_OPERATION;
      equipmentModel.permission_id = env.userProfile.id;
      equipmentModel.updatedDt = DateTime.now();
      equipmentModel.facilityId = env.userProfile.facilityId;
      equipmentModel.user = env.userProfile.userName;
      await equipmentDAO.add(equipmentModel);
      notificationRepository.addNotification(
          equipmentModel, APP_CONST.OUTBOUND_OPERATION);
    });
  }
}
