import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/facility_configuration_model.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/listview/actions/outbound_railcar/ui/outbound_form_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class OutboundRailCar extends StatefulWidget {
  List<Equipment> selectedEquipments = List<Equipment>();
  String source;

  OutboundRailCar(List<Equipment> selectedEquipments, String source) {
    this.selectedEquipments = selectedEquipments;
    this.source = source;
  }

  @override
  _OutboundRailCarState createState() => _OutboundRailCarState(selectedEquipments, source);
}

class _OutboundRailCarState extends State<OutboundRailCar> {

  List<Equipment> selectedEquipments = List<Equipment>();
  FacilityConfigurationModel fcModel = MemoryData.facilityConfiguration;
  final dateFormat = new DateFormat('MM/dd/yyyy');
  TimeOfDay _time = TimeOfDay.now();
  TimeOfDay selectedTime;
  List<bool> isSelected;
  FToast fToast;
  final globalKey = GlobalKey<ScaffoldState>();
  ThemeChanger _themeChanger;
  String source;

  _OutboundRailCarState(List<Equipment> selectedEquipments, String source) {
    this.selectedEquipments = selectedEquipments;
    this.source = source;
  }

  Future<Null> selectTime(BuildContext context) async {
    selectedTime = await showTimePicker(context: context, initialTime: _time);

    setState(() {
      _time = selectedTime;
      print("time selected is ${_time}");
    });
  }

  void _showToastError(String errorText) {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Colors.redAccent, // color: Color(0xff7fae1b),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.error),
          SizedBox(
            width: 12.0,
          ),
          Text(
            "Railcar has a defect : ${errorText.replaceAll("\n", ", ")}",
            style: TextStyle(
              // ignore: deprecated_member_use
              color: Theme.of(context).textTheme.body1.color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  confirmationMessage(String warnText) async {
    bool returnValue = false;
    List<Object> returnList = await CustomDialog.cstmDialog(context, "confirm_to_continue", "There is at least one railcar with a warning",
        "$warnText \nDo you wish to continue?");

    if (returnList[0] == DialogAction.yes) {
      returnValue = true;
    }
    return returnValue;
  }

  void showToastSaveSuccess() {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Color(0xff7fae1b),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text(
            "Railcar Outbounded Successfully",
            style: TextStyle(
              // ignore: deprecated_member_use
              color: Theme.of(context).textTheme.body1.color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  @override
  void initState() {
    super.initState();
    isSelected = [false, false];
    selectedTime = _time;
    fToast = FToast(context);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);
    _themeChanger = Provider.of<ThemeChanger>(context);

    return Scaffold(
        key: globalKey,
        appBar: MainAppBar(),
        drawer: AppDrawer(),
        body: Form(
          autovalidate: true,
          child: FormUI(),
        ));
  }

  Widget FormUI() {
    final MaterialLocalizations localizations = MaterialLocalizations.of(context);

    return BlocProvider(
        create: (context) => OutboundFormBloc(this.selectedEquipments),
        //value:OutboundFormBloc(this.selectedEquipments) ,

    child:
        Builder(
          builder: (context) {

            final formBloc = BlocProvider.of<OutboundFormBloc>(context);


            return
              FormBlocListener<OutboundFormBloc, String, String>(

            onSubmitting: (context, state) {
                print("submitting");
                // LoadingDialog.show(context);
              },
              onSuccess: (context, state) {
                print("block onSuccess");
                showToastSaveSuccess();
                Navigator.pop(context);
                print("source:$source");
                if (APP_CONST.AEI_SCAN == source) {
                  BlocProvider.of<AEIScanBloc>(context).add(AEIScanFetchDataWithLastSetCriteria());
                } else if (APP_CONST.YARD_VIEW == source) {
                  BlocProvider.of<YardViewBloc>(context)
                      .add(YardViewFetchDataWithLastSetCriteria());
                } else {
                  BlocProvider.of<ListViewBloc>(context).add(ListViewFetchDataWithLastSetCriteria());
                }
              },
              onFailure: (context, state) async {
                print("block onFailure, fcModel.outboundRailCarDefect:${fcModel.outboundRailCarDefect}");
                if ("Error" == fcModel.outboundRailCarDefect) {
                  print("testing");

                  _showToastError(state.failureResponse);
                } else if ("Warning" == fcModel.outboundRailCarDefect) {
                  bool isOk = await confirmationMessage(state.failureResponse);
                  print("isOk:$isOk");
                  if (isOk) {
                    formBloc.isWarningConfirmOk = true;
                    formBloc.submit();
                  }
                }
               // LoadingDialog.hide(context);
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: CardUI(
                          content: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.all(20),
                                        child: Container(
                                            child: ListTile(
                                          leading: GestureDetector(
                                              onTap: () {
                                                //   popUpDialog();
                                                Navigator.of(context).pop();
                                              },
                                              child: Icon(
                                                Icons.arrow_back,
                                              )),
                                          title: Text('Outbound Railcars (${selectedEquipments.length})',
                                              style: TextStyle(
                                                  fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.normal,
                                                  fontFamily: 'Roboto',
                                                  color: Theme.of(context)
                                                      .textTheme
                                                      // ignore: deprecated_member_use
                                                      .headline
                                                      .color)),
                                        ))),
                                  ],
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Padding(
                                          padding: EdgeInsets.all(20),
                                          child: Container(
                                              child: GestureDetector(
                                            onTap: () {
                                              // popUpDialog();
                                              Navigator.of(context).pop();
                                            },
                                            child: RichText(
                                              text: TextSpan(
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      // ignore: deprecated_member_use
                                                      .body1,
                                                  children: <InlineSpan>[
                                                    TextSpan(
                                                        text: 'CANCEL',
                                                        style: TextStyle(
                                                            fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                                            fontStyle: FontStyle.normal,
                                                            fontWeight: FontWeight.w500,
                                                            letterSpacing: ScreenUtil().setWidth(1.25),
                                                            fontFamily: 'Roboto',
                                                            color: Color(0xFF3e8aeb)))
                                                  ]),
                                            ),
                                          ))),
                                      Padding(
                                          padding: EdgeInsets.all(20),
                                          child: Container(
                                              // width: 150,
                                              width: ScreenUtil().setWidth(200),
                                              height: ScreenUtil().setHeight(48),
                                              child: FlatButton(
                                                  onPressed: formBloc.onSubmitting,
                                                  child: Text('OUTBOUND RAILCAR',
                                                      style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: ScreenUtil().setHeight(14),
                                                          fontStyle: FontStyle.normal,
                                                          fontWeight: FontWeight.w500,
                                                          letterSpacing: ScreenUtil().setWidth(1.25),
                                                          color: Colors.white)),
                                                  color: Color(0xFF3e8aeb),
                                                  textColor: Colors.white,
                                                  disabledColor: Colors.grey,
                                                  disabledTextColor: Colors.black,
                                                  splashColor: Color(0xFF3e8aeb)))),
                                    ],
                                  )
                                ],
                              ),
                            ],
                          ),
                          Divider(
                            thickness: 5.0,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                  children: selectedEquipments.map((railcar) {
                                var railcar_number = Container(
                                    width: ScreenUtil().setWidth(144),
                                    margin: EdgeInsets.symmetric(vertical: 4.0),
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      title: Text(
                                        'Railcar',
                                        // ignore: deprecated_member_use
                                        style: Theme.of(context).textTheme.body2,
                                      ),
                                      subtitle: Text(
                                        '${railcar.equipmentInitial} ${railcar.equipmentNumber}',
                                        // ignore: deprecated_member_use
                                        style: Theme.of(context).textTheme.body2,
                                      ),
                                    ));
                                return railcar_number;
                              }).toList()),
                            ),
                          ),
                          Divider(
                            thickness: 1.0,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                            children: <Widget>[
                              SizedBox(
                                width: ScreenUtil().setWidth(25),
                              ),
                              Container(
                                width: ScreenUtil().setWidth(214),
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(vertical: 4.0),
                                child: DateTimeFieldBlocBuilder(
                                  style: TextStyle( color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                                  _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null, fontSize: 20),
                                  dateTimeFieldBloc: formBloc.input_date,
                                  format: dateFormat,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime.now().subtract(Duration(days: 1)),
                                  lastDate: DateTime(2100),
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),

                                    labelStyle: TextStyle( color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                                    _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null,),
                                    //errorStyle: TextStyle(height:2),
                                    suffixIcon: Icon(Icons.date_range, color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                                    _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null,),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide: const BorderSide(color: Colors.black38, width: 1),
                                    ),
                                    filled: true,
                                    fillColor:
                                    _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
//                                    hintText: _dateTime == null ? 'Depart Date' : dateFormat.format(_dateTime).toString(),
                                    hintStyle: TextStyle(
                                      fontSize: 20.0,
                                      color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                                      _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: ScreenUtil().setWidth(25),
                              ),
                              Container(
                                width: ScreenUtil().setWidth(214),
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(vertical: 4.0),
                                child: TimeFieldBlocBuilder(
                                    style: TextStyle( color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                                    _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null, fontSize: 20),
                                    timeFieldBloc: formBloc.input_time,
                                    format: new DateFormat('HH:mm'),
                                    initialTime: TimeOfDay.now(),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                                      // errorStyle: TextStyle(height:2),
                                      suffixIcon: Icon(Icons.timer,  color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                                      _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null,),
                                      enabledBorder: const OutlineInputBorder(
                                        borderSide: const BorderSide(color: Colors.black38, width: 1),
                                      ),
                                      filled: true,
                                      fillColor:
                                      _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                      hintText: selectedTime == null ? 'No Time Is Set' : localizations.formatTimeOfDay(selectedTime),
                                      hintStyle: TextStyle(
                                        fontSize: 20.0,
                                         color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                                      _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null,
                                      ),
                                    )),
                              ),
                            ],
                          ),
                        ],
                      )),
                    ),
                  )
                ],
              ),
            );
          },
        )
    );
  }
}
