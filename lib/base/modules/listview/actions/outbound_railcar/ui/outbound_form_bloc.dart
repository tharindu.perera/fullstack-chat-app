import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/modules/listview/actions/outbound_railcar/repository/outbound_railcar_repository.dart';

class OutboundFormBloc extends FormBloc<String, String> {
  List<Equipment> selectedCarList = List<Equipment>();
  OutboundRailCarRepository repo = new OutboundRailCarRepository();

  // ignore: close_sinks
  final input_date = InputFieldBloc<DateTime, Object>(
    initialValue: DateTime.now(),
    validators: [
      FieldBlocValidators.required,
    ],
  );
  // ignore: close_sinks
  final input_time = InputFieldBloc<TimeOfDay, Object>(
    initialValue: TimeOfDay.now() ,
    validators: [
      FieldBlocValidators.required,
    ],
  );

  bool isWarningConfirmOk = false;

  OutboundFormBloc(List<Equipment> selectedCarList) {
    this.selectedCarList = selectedCarList;

    addFieldBlocs(fieldBlocs: [
      input_time,
      input_date,
    ]);
  }

  @override
  void onSubmitting() async {
    print("start outbound form block onSubmitting() method");
    try {
      print("isWarningConfirmOk:$isWarningConfirmOk");
      if (!isWarningConfirmOk) {
        OutboundRailCarRepository outRepo = new OutboundRailCarRepository();
        Map<String, List<String>> map = await outRepo.checkBeforeOutbound(selectedCarList);

        List<String> errorList = map["error"];
        List<String> warningList = map["warning"];

        if (errorList.isNotEmpty) {
          String errorText = "";
          for (String error in errorList) {
            errorText += "$error\n";
          }
          emitFailure(failureResponse: errorText);
        }
        else if (warningList.isNotEmpty) {
          String errorText = "";
          for (String error in warningList) {
            errorText += "$error\n";
          }
          emitFailure(failureResponse: errorText);
        }
        else {
          saveOutbound();
        }
      }
      else {
        saveOutbound();
      }
    }
    catch (e) {
      emitFailure(failureResponse: 'Application error!');
    }
  }

  saveOutbound() {
    print("start outbound form block saveOutbound() method");

    repo.outboundRailcar(selectedCarList, input_date.state.value, input_time.state.value).then((value) {
      // for AEI Scan
      List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"] != null ?
      MemoryData.dataMap["scannedEquipList"] : [];
      print("selectedCarList length:${selectedCarList.length}");
      print("scannedEquipList 1 length:${scannedEquipList.length}");

      List<Equipment> removingEquipList = [];
      selectedCarList.forEach((selectedEquipment) {
        scannedEquipList.forEach((scannedEquipment) {
          if (selectedEquipment.equipmentInitial == scannedEquipment.equipmentInitial &&
              selectedEquipment.equipmentNumber == scannedEquipment.equipmentNumber) {
            print("removing outbounded '${selectedEquipment.equipmentInitial} ${selectedEquipment.equipmentNumber}' from the list");
            removingEquipList.add(scannedEquipment);
          }
        });
      });

      print("removingEquipList length:${removingEquipList.length}");

      removingEquipList.forEach((element) {
        scannedEquipList.remove(element);
      });
      print("scannedEquipList 2 length:${scannedEquipList.length}");

      // correct the sequence of remaining railcars.
      if (scannedEquipList.length > 0) {
        scannedEquipList.sort((a, b) => (a.sequence).compareTo(b.sequence));

        int seq = 1;
        scannedEquipList.forEach((element) {
          print("seq:$seq");
          element.sequence = seq++;
        });
      }

      MemoryData.dataMap.remove("scannedEquipList");
      MemoryData.dataMap.putIfAbsent("scannedEquipList", () => scannedEquipList);
      //------------

      emitSuccess();
    });
  }
}
