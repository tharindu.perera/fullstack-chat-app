import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:provider/provider.dart';


class ViewComment extends StatefulWidget {
  Equipment selectedEquipments ;

  ViewComment({Equipment selectedRow}) {
    this.selectedEquipments = selectedRow;
  }

  @override
  _ViewCommentState createState() => _ViewCommentState(selectedEquipments);
}

class _ViewCommentState extends State<ViewComment> {
  final globalKey = GlobalKey<ScaffoldState>();
  Equipment selectedEquipments ;
  final dateTimeFormatter = new DateFormat('MM/dd/yyyy HH:mm');

  onRequesSuccess() async {
    List<Object> returnList = await CustomDialog.cstmDialog(context, "success",
        "Request Successful", "Changes have been added to the system");
    print("returnList--> ${returnList.toString()}");
    returnList[0] == DialogAction.yes ? Navigator.pop(context) : null;
  }

  _ViewCommentState(Equipment selectedEquipments, ) {
    this.selectedEquipments = selectedEquipments;
  }

    @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    ScreenUtil.init(context, width: 1024, height: 768);

    return Scaffold(
      appBar: MainAppBar(),
      drawer: AppDrawer(),
      key: globalKey,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: CardUI(
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(

                                        child: ListTile(
                                          leading: GestureDetector(
                                              onTap: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: Icon(
                                                Icons.arrow_back,
                                              )),
                                          title: Text('View Comment',
                                              style: TextStyle(
                                                  fontSize: ScreenUtil().setSp(24,
                                                      allowFontScalingSelf: true),
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.normal,
                                                  fontFamily: 'Roboto',
                                                  color: Theme.of(context)
                                                      .textTheme
                                                      .headline
                                                      .color)),
                                          //subtitle: Text('TMOX 004251',style:Theme.of(context).textTheme.headline ),
                                        ))),
                              ],
                            ),
                          ),
                        ],
                      ),

                      SizedBox(
                        height: ScreenUtil().setHeight(14),
                      ),
                      Divider(
                        thickness: 2.0,
                        //color: Color.fromRGBO(0, 0, 0, 1),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(24),
                      ),

                      Wrap(
                        children: selectedEquipments.comments.map((value) {
                          return Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(14),
                                  vertical: ScreenUtil().setHeight(14)),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                    color: _themeChanger.getTheme().primaryColor ==
                                        Color(0xff182e42)
                                        ? Colors.white
                                        : _themeChanger.getTheme().primaryColor ==
                                        Color(0xfff5f5f5)
                                        ? Color.fromRGBO(0, 0, 0, 0.6)
                                        : null,
                                  )),
                              child: ListTile(
                                leading: Icon(
                                  Icons.account_circle,
                                  size: 50,
                                  color: Color(0xFF508be4),
                                ),
                                title: Text(
                                  value.user??"",
                                  style: TextStyle(
                                    fontSize: ScreenUtil()
                                        .setSp(16, allowFontScalingSelf: true),
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                    fontFamily: 'Roboto',
                                    color: _themeChanger.getTheme().primaryColor ==
                                        Color(0xff182e42)
                                        ? Colors.white
                                        : _themeChanger.getTheme().primaryColor ==
                                        Color(0xfff5f5f5)
                                        ? Color(0xFF508be4)
                                        : null,
                                  ),
                                ),
                                trailing: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Container(
                                        width: ScreenUtil().setWidth(180),
                                        child: Text(
                                          value.commentType??"",
                                          style: TextStyle(
                                            fontSize: ScreenUtil()
                                                .setSp(14, allowFontScalingSelf: true),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'Roboto',
                                            color:
                                            _themeChanger.getTheme().primaryColor ==
                                                Color(0xff182e42)
                                                ? Colors.white
                                                : _themeChanger
                                                .getTheme()
                                                .primaryColor ==
                                                Color(0xfff5f5f5)
                                                ? Colors.black
                                                : null,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: ScreenUtil().setWidth(220),
                                        child: RichText(
                                          text: TextSpan(
                                            text: 'Last Updated: ',
                                            style: TextStyle(
                                              fontStyle: FontStyle.normal,
                                              fontSize: ScreenUtil()
                                                  .setSp(14, allowFontScalingSelf: true),
                                              fontFamily: 'Roboto',
                                              fontWeight: FontWeight.normal,
                                              color: _themeChanger.getTheme().primaryColor ==
                                                  Color(0xff182e42)
                                                  ? Color(0xFFffffff)
                                                  : _themeChanger.getTheme().primaryColor ==
                                                  Color(0xfff5f5f5)
                                                  ? Color.fromRGBO(0, 0, 0, 0.6)
                                                  : null,
                                            ),
                                            children: <InlineSpan>[
                                              TextSpan(
                                                  text:dateTimeFormatter.format(value?.createdDt),
                                                  // value?.createdDt?.toString(),
                                                  style: TextStyle(
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: ScreenUtil()
                                                        .setSp(14, allowFontScalingSelf: true),
                                                    fontFamily: 'Roboto',
                                                    fontWeight: FontWeight.normal,
                                                    color:
                                                    _themeChanger.getTheme().primaryColor ==
                                                        Color(0xff182e42)
                                                        ? Colors.white
                                                        : _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                        Color(0xfff5f5f5)
                                                        ? Colors.black
                                                        : null,
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ]),
                                subtitle: Text(
                                  value.comment??"",
                                  style: TextStyle(
                                    fontSize: ScreenUtil()
                                        .setSp(14, allowFontScalingSelf: true),
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                    fontFamily: 'Roboto',
                                  ),
                                ),
                                isThreeLine: true,
                                dense: true,
                              ));

                        }).toList(),
                      )

                    ],
                  )),
            ),
          )
        ],
      ),
    );
  }
}
