import 'package:mbase/base/modules/inspection/model/inspection_template_model.dart';
import 'package:mbase/base/modules/listview/actions/inspect_railcar/dao/inspect_railcar_dao.dart';

class InspectRailcarRepository {
  InspectRailcarDAO inspectRailcarDAO = new InspectRailcarDAO();

  // Future<void> saveDefect(List<Equipment> selectedCarList, String selectedBlockName) async {
  //   AssignedDefect assignedDefect = new AssignedDefect();
  //   assignedDefect.equipmentId = selectedCarList[0].id;
  //   assignedDefect.railcarDefectId = selectedBlockName;
  //   assignedDefect.facilityId = userProfile.facilityId;
  //   assignedDefect.transactionStatus = APP_CONST.TRANSACTION_STATUS_PENDING;
  //   assignedDefect.source = APP_CONST.MOBILITY;
  //   assignedDefect.operationType = APP_CONST.ASSIGN_DEFECT_OPERATION;
  //   assignedDefect.permission_id = userProfile.id;
  //   assignedDefect.createdDt = DateTime.now();
  //   await assignDefectDAO.add(assignedDefect);
  // }

  Future<List<InspectionTemplate>> fetchTemplates() async {
    return await inspectRailcarDAO.fetchInspectionTemplate();
  }
}