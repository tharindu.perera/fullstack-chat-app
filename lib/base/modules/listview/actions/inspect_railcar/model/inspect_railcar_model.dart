// import 'package:mbase/base/core/common_base/model/equipment.dart';
//
// class InspectRailCarModel {
//   InspectRailCarModel({
//     this.createdDt,
//     this.updatedDt,
//     this.source,
//     this.transactionStatus,
//     this.id,
//     this.facility_id,
//     this.inspection_template_id,
//     this.permission_id,
//     this.template_description,
//     this.template_id,
//     this.template_name,
//     this.item_list
//   });
//
//   String createdDt;
//   String updatedDt;
//   String source;
//   String transactionStatus;
//   String facility_id;
//   String id;
//   String inspection_template_id;
//   String permission_id;
//   List<String> item_list;
//   String template_description;
//   String template_id;
//   String template_name;
//
//   factory InspectRailCarModel.fromMap(Map<String, dynamic> json) =>
//       InspectRailCarModel(
//         createdDt: json["createdDt"] == null
//             ? null
//             : json["createdDt"],
//         updatedDt: json["updatedDt"] == null ? null : json["updatedDt"],
//         source: json["source"] == null ? null : json["source"],
//         transactionStatus: json["transactionStatus"] == null ? null : json["transactionStatus"],
//         id: json["id"] == null ? null : json["id"],
//         inspection_template_id: json["inspection_template_id"] == null ? null : json["inspection_template_id"],
//         permission_id: json["permission_id"] == null ? null : json["permission_id"],
//         facility_id: json["facility_id"] == null ? null : json["facility_id"],
//         template_description: json["template_description"] == null ? null : json["template_description"],
//         template_id: json["template_id"] == null ? null : json["template_id"],
//         template_name: json["template_name"] == null ? null : json["template_name"],
//           item_list: json["item_list"] == null ? null : json["item_list"]
//       );
//
//
//   Map<String, dynamic> toMap() => {
//     "id":id = null?null:id,
//     "transaction_status": transactionStatus == null ? null : transactionStatus,
//     "created_dt": createdDt == null ? null : createdDt,
//     "updated_dt": updatedDt == null ? null : updatedDt,
//     "source": source == null ? null : source,
//     "inspection_template_id": inspection_template_id == null ? null : inspection_template_id,
//     "permission_id": permission_id == null ? null : permission_id,
//     "facility_id": facility_id == null ? null : facility_id,
//     "template_description": template_description == null ? null : template_description,
//     "template_id": template_id == null ? null : template_id,
//     "template_name": template_name == null ? null : template_name,
//     "item_list": item_list == null ? null : item_list,
//
//   };
//
//   @override
//   String toString() {
//     return 'InspectRailCarModel{id: $id, facility_id: $facility_id, transaction_status: $transactionStatus},'
//         'inspection_template_id:$inspection_template_id,permission_id:$permission_id,'
//         'facility_id:$facility_id,template_name:$template_name,template_id:$template_id'
//         'template_description:$template_description,created_dt:$createdDt,item_list:$item_list';
//   }
// }
