import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/inspection/model/inspection_template_model.dart';
import 'package:mbase/base/modules/inspection/repository/inspection_details_repository.dart';
import 'package:mbase/base/modules/inspection/ui/inspection_details.dart';
import 'package:provider/provider.dart';

class InspectRailCar extends StatefulWidget {
  List<Equipment> selectedEquipments = List<Equipment>();

  InspectRailCar(List<Equipment> selectedEquipments) {
    this.selectedEquipments = selectedEquipments;
  }

  @override
  _InspectRailCarState createState() => _InspectRailCarState(selectedEquipments);
}

class _InspectRailCarState extends State<InspectRailCar> {
  final globalKey = GlobalKey<ScaffoldState>();
  FToast fToast;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final selectBlock = SnackBar(content: Text('RailCar Block cannot be empty, Please choose a defect to continue.'));
  final successMessage = SnackBar(content: Text('Request Successful.'));
  List<bool> isSelected;
  InspectionTemplate selectedRadio;
  bool selectedTemp = false;
  List<Equipment> selectedEquipments = List<Equipment>();
  List<Map<String, dynamic>> send = [];

  InspectionRailcarModel inspectionRailcarModel = InspectionRailcarModel();

  InspectionDetailsRepo inspectRailcarRepository = InspectionDetailsRepo();
  List<InspectionTemplate> inspectRailCarModelList = [];

  _InspectRailCarState(List<Equipment> selectedEquipments) {
    this.selectedEquipments = selectedEquipments;
  }

  setSelectedRadio(int value) {
    setState(() {
      selectedRadio = inspectRailCarModelList[value];
      selectedTemp=true;
    });
    print("selectedTemplate = " + selectedRadio.templateName.toString());
  }

  _beginInspection() {
    if (selectedRadio == null) {
      globalKey.currentState.showSnackBar(selectBlock);
    } else {
      inspectionRailcarModel.templateName = selectedRadio.templateName;
      inspectionRailcarModel.inspectionTemplateId = selectedRadio.id;
      inspectionRailcarModel.equipment = selectedEquipments.first.equipmentInitial + " " + selectedEquipments.first.equipmentNumber;
      inspectionRailcarModel.equipmentId = selectedEquipments.first.id;
      inspectionRailcarModel.source = selectedRadio.source;
      inspectionRailcarModel.type = selectedRadio.type;
      inspectionRailcarModel.inspectionStatus = "Pending";
      inspectionRailcarModel.inspectionTemplate = selectedRadio;
      inspectionRailcarModel.equipmentType = selectedEquipments.first.equipmentType;

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => InspectionDetails(
                    inspectionRailcarModel: inspectionRailcarModel,
                  )));
    }
  }

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);
    inspectRailcarRepository.fetchInspectionTemplate().then((value) {
      value.forEach((element) {
        print("inspection template---> ${element}");

        inspectRailCarModelList.add(element);
      });
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    ScreenUtil.init(context, width: 1024, height: 768);

    return Scaffold(
      appBar: MainAppBar(),
      drawer: AppDrawer(),
      key: globalKey,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: CardUI(
                  content: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Container(
                                    child: ListTile(
                                  leading: GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Icon(
                                        Icons.arrow_back,
                                      )),
                                  title: Text('Inspect Railcar',
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: 'Roboto',
                                          color: Theme.of(context).textTheme.headline.color)),
                                ))),
                          ],
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(20),
                                  child: Container(
                                      child: GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: RichText(
                                      text: TextSpan(style: Theme.of(context).textTheme.body1, children: <InlineSpan>[
                                        TextSpan(
                                            text: 'CANCEL',
                                            style: TextStyle(
                                                fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                                letterSpacing: ScreenUtil().setWidth(1.25),
                                                fontFamily: 'Roboto',
                                                color: Color(0xFF3e8aeb)))
                                      ]),
                                    ),
                                  ))),
                              Padding(
                                  padding: EdgeInsets.only(left: 0, top: 14, right: 24, bottom: 14),
                                  child: Container(
                                    width: ScreenUtil().setWidth(180),
                                    height: ScreenUtil().setHeight(48),
                                    child: FlatButton(
                                        onPressed: selectedTemp ? _beginInspection : null,
                                        child: Text(
                                          'BEGIN INSPECTION',
                                          style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize: ScreenUtil().setHeight(14),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing: ScreenUtil().setWidth(1.25),
                                              color: Colors.white),
                                        ),
                                        color: Color(0xFF508be4),
                                        textColor: Colors.white,
                                        disabledColor: Colors.grey,
                                        disabledTextColor: Colors.black,
                                        splashColor: Color(0xFF508be4)),
                                  )),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(14),
                  ),
                  Divider(
                    thickness: 2.0,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(24),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                    child: Wrap(
                        direction: Axis.horizontal,
                        spacing: ScreenUtil().setWidth(42),
                        runSpacing: ScreenUtil().setHeight(10),
                        children: selectedEquipments.map((railcar) {
                          Widget railcarWidget = Container(
                              width: ScreenUtil().setWidth(160),
                              child: ListTile(
                                title: Text(
                                  'Railcar',
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: 'Roboto',
                                      letterSpacing: ScreenUtil().setWidth(0.5),
                                      color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xFFffffff) : Color.fromRGBO(0, 0, 0, 0.6)),
                                ),
                                subtitle: Text('${railcar.equipmentInitial} ${railcar.equipmentNumber}',
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(20, allowFontScalingSelf: true),
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: 'Roboto',
                                        letterSpacing: ScreenUtil().setWidth(0.25),
                                        color: Theme.of(context).textTheme.headline.color)),
                              ));
                          return railcarWidget;
                        }).toList()),
                  ),
                  Divider(
                    thickness: 2.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24), vertical: ScreenUtil().setHeight(8)),
                            alignment: Alignment.topLeft,
                            child: Text(
                              'Inspection Template',
                              style: Theme.of(context).textTheme.body2,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Wrap(
                    direction: Axis.horizontal,
                    spacing: ScreenUtil().setWidth(10),
                    runSpacing: ScreenUtil().setHeight(10),
                    children: inspectRailCarModelList.map((e) {
                      return Container(
                        width: ScreenUtil().setWidth(200),
                        child: ListTile(
                          leading: Radio(
                            value: inspectRailCarModelList.indexOf(e),
                            groupValue: selectedRadio == null ? null : inspectRailCarModelList.indexOf(selectedRadio),
                            activeColor: Color(0xFF3e8aeb),
                            onChanged: (val) {
                              print("Radio onChanged " + val.toString());
                              setSelectedRadio(val);
                            },
                          ),
                          title: Text(
                            inspectRailCarModelList[inspectRailCarModelList.indexOf(e)].templateName,
                            style: Theme.of(context).textTheme.bodyText2,
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                ],
              )),
            ),
          )
        ],
      ),
    );
  }
}
