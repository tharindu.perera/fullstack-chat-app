import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/modules/inspection/model/inspection_template_model.dart';
import 'package:mbase/env.dart';

import '../../../../../../main.dart';

class InspectRailcarDAO extends BaseDao {
  InspectRailcarDAO() : super("Inspection_Template");

  // Future<void> add(AssignedDefect assignedDefect) async {
  //   assignedDefect.id = Uuid().v1();
  //   await collection.doc(assignedDefect.id).set(assignedDefect.toMap());
  //
  // }

  Future<List<InspectionTemplate>> fetchInspectionTemplate() async {

    FirebaseFirestore firebaseFirestore = DatabaseProvider.firestore;
    CollectionReference inspectionRailcarCollection = firebaseFirestore.collection("inspection_template");

    return (await inspectionRailcarCollection.where("permission_id", isEqualTo: APP_CONST.PUBLIC)
    .where("facility_id",isEqualTo: env.userProfile.facilityId)
        .get()).docs.map((e) => InspectionTemplate.fromMap(e.data())).toList();

  }
}