import 'dart:async';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/repository/assign_switchlist_repository.dart';
import 'package:meta/meta.dart';

import '../../../../../core/common_base/bloc/scy_bloc.dart';

part 'assign_switchlist_event.dart';

part 'assign_switchlist_state.dart';

class AssignSwitchListBloc
    extends SCYBloC
    <AssignSwitchListEvent, AssignSwitchListState> {
  final AssignSwitchListRepo _assignSwitchListRepository;

  AssignSwitchListBloc({AssignSwitchListRepo assignSwitchListRepo})
      : _assignSwitchListRepository = assignSwitchListRepo, super(AssignSwitchListState.initial());
  //
  // @override
  // AssignSwitchListState get initialState => AssignSwitchListState.initial();

  @override
  Stream<AssignSwitchListState> mapEventToState(
    AssignSwitchListEvent event,
  ) async* {
    if (event is AssignSwitchListLoaded) {
      yield* _mapAssignSwitchListLoadedToState();
    }
    else if (event is AssignSwitchListYardChanged) {
     yield* _mapAssignSwitchListYardChangedToState(event);
    }
    else if (event is AssignSwitchListTrackChanged) {
     // yield* _mapAssignSwitchListTracksChangedToState(event);
    } else if (event is AssignSwitchListSpotChanged) {
     // yield* _mapAssignSwitchListSpotChangedToState(event);
    }
  }

  Stream<AssignSwitchListState> _mapAssignSwitchListLoadedToState() async* {
    yield AssignSwitchListState.yardsLoadInProgress();
    final yards = await _assignSwitchListRepository.fetchYards();
    print("bloc yards -----> ${yards}");
    //final tracks = await _assignSwitchListRepository.fetchTracks();
    yield AssignSwitchListState.yardsLoadSuccess(yards: yards, tracks: []);
  }

  Stream<AssignSwitchListState> _mapAssignSwitchListYardChangedToState(
      AssignSwitchListYardChanged event) async* {
    final currentState = state;
    yield AssignSwitchListState.tracksLoadInProgress(
      yards: currentState.yards,
      yard: event.yard,
    );
    final tracks = await _assignSwitchListRepository.fetchTracksByYardName(
        yardName: event.yard);
    yield AssignSwitchListState.tracksLoadSuccess(
      yards: currentState.yards,
      yard: event.yard,
      tracks: tracks,
    );
  }

  // @override
  // Stream<AssignSwitchListState> mapEventToState(AssignSwitchListEvent event) {
  //   // TODO: implement mapEventToState
  //   throw UnimplementedError();
  // }
}
