part of 'assign_switchlist_bloc.dart';

class AssignSwitchListState {
  final List<String> yards;
  final String yard;

  final List<String> tracks;
  final String track;

  final List<String> spots;
  final String spot;

  const AssignSwitchListState({
    @required this.yards,
    @required this.yard,
    @required this.tracks,
    @required this.track,
    @required this.spots,
    @required this.spot,
  });

  bool get isComplete => yard != null && track != null && spot != null;

  factory AssignSwitchListState.initial() => AssignSwitchListState(
        yards: <String>[],
        yard: null,
        tracks: <String>[],
        track: null,
        spots: <String>[],
        spot: null,
      );

  factory AssignSwitchListState.yardsLoadInProgress() => AssignSwitchListState(
        yards: <String>[],
        yard: null,
        tracks: <String>[],
        track: null,
        spots: <String>[],
        spot: null,
      );

  factory AssignSwitchListState.yardsLoadSuccess({
    @required List<String> yards,
    @required List<String> tracks,
  }) =>
      AssignSwitchListState(
        yards: yards,
        yard: null,
        tracks: tracks,
        track: null,
        spots: <String>[],
        spot: null,
      );

  factory AssignSwitchListState.tracksLoadInProgress({
    @required List<String> yards,
    @required String yard,
  }) =>
      AssignSwitchListState(
        yards: yards,
        yard: yard,
        tracks: <String>[],
        track: null,
        spots: <String>[],
        spot: null,
      );

  factory AssignSwitchListState.tracksLoadSuccess({
    @required List<String> yards,
    @required String yard,
    @required List<String> tracks,
  }) =>
      AssignSwitchListState(
        yards: yards,
        yard: yard,
        tracks: tracks,
        track: null,
        spots: <String>[],
        spot: null,
      );

  factory AssignSwitchListState.spotsLoadInProgress({
    @required List<String> yards,
    @required String yard,
    @required List<String> tracks,
    @required String track,
  }) =>
      AssignSwitchListState(
        yards: yards,
        yard: yard,
        tracks: tracks,
        track: track,
        spots: <String>[],
        spot: null,
      );

  factory AssignSwitchListState.spotsLoadSuccess({
    @required List<String> yards,
    @required String yard,
    @required List<String> tracks,
    @required String track,
    @required List<String> spots,
  }) =>
      AssignSwitchListState(
        yards: yards,
        yard: yard,
        tracks: tracks,
        track: track,
        spots: spots,
        spot: null,
      );

  AssignSwitchListState copyWith({
    List<String> yards,
    String yard,
    List<String> tracks,
    String track,
    List<String> spots,
    String spot,
  }) {
    return AssignSwitchListState(
      yards: yards ?? this.yards,
      yard: yard ?? this.yard,
      tracks: tracks ?? this.tracks,
      track: track ?? this.track,
      spots: spots ?? this.spots,
      spot: spot ?? this.spot,
    );
  }
}
