part of 'assign_switchlist_bloc.dart';

@immutable
abstract class AssignSwitchListEvent {
  const AssignSwitchListEvent();
}

class AssignSwitchListLoaded extends AssignSwitchListEvent {}

class AssignSwitchListYardChanged extends AssignSwitchListEvent {
  final String yard;

  const AssignSwitchListYardChanged({this.yard});
}

class AssignSwitchListTrackChanged extends AssignSwitchListEvent {
  final String track;

  const AssignSwitchListTrackChanged({this.track});
}

class AssignSwitchListSpotChanged extends AssignSwitchListEvent {
  final String spot;

  const AssignSwitchListSpotChanged({this.spot});
}
