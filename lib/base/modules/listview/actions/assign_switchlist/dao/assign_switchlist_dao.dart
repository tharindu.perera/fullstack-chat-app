import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/dao/yard_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/model/assigned_switchlist_model.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';
import 'package:uuid/uuid.dart';

class AssignSwitchlistDao extends BaseDao {
  AssignSwitchlistDao() : super("switchlist");
  FirebaseFirestore _firestore = DatabaseProvider.firestore;
  NotificationRepository notificationRepository = NotificationRepository();

  Future<List<NewSwitchList>> getRecords() async {
    return (await BaseQuery()
            //.where("transaction_status", isEqualTo: APP_CONST.TRANSACTION_STATUS_ACCEPTED)
            // .where("operation_type",isEqualTo:APP_CONST.SWITCHLIST_HEADER )

            //.where("permission_id",whereIn: [APP_CONST.PUBLIC])

            .where("switch_list_status", whereIn: [
      APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_NEW,
      APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS
    ]).get())
        .docs
        .map((snapshot) {
      return NewSwitchList.fromJson(snapshot.data());
    }).toList();
  }

  Future<List<YardModel>> getYards() async {
    YardDao yardDao = YardDao();
    List<YardModel> yards = await yardDao.fetchAllYards();
    return yards;
  }

  Future<List<TrackModel>> getTracks() async {
    YardDao yardDao = YardDao();
    List<TrackModel> tracks;
    //= await yardDao.fetchAllYards();
    return tracks;
  }

  Future<List<YardModel>> fetchTracksByYardName() async {
    YardDao yardDao = YardDao();
    List<YardModel> yards = await yardDao.fetchAllYards();
    return yards;
  }

  Future<bool> verify(SwitchListEquipmentModel assignedSwitchListModel) async {
    assignedSwitchListModel.id = Uuid().v1();

    QuerySnapshot qShot3 = await _firestore
        .collection("switchlist_equipment")
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .where("switch_list_id",
            isEqualTo: assignedSwitchListModel.switch_list_id)
        .where("to_track_id", isEqualTo: assignedSwitchListModel.to_track_id)
        .where("to_yard_id", isEqualTo: assignedSwitchListModel.to_yard_id)
        .where("equipment_initial",
            isEqualTo: assignedSwitchListModel.equipment_initial)
        .where("equipment_number",
            isEqualTo: assignedSwitchListModel.equipment_number)
        .get();
    return qShot3.docs.length > 0;
  }

  Future<bool> add(NewSwitchList assignedSwitchListModel) async {
    assignedSwitchListModel.id = Uuid().v1();

    print(assignedSwitchListModel);
    await _firestore
        .collection("switchlist")
        .doc(assignedSwitchListModel.id)
        .set(assignedSwitchListModel.toMap());
    await notificationRepository?.addNotification(
        assignedSwitchListModel, APP_CONST.SWITCHLIST_OPERATION);

    return true;
  }
}
