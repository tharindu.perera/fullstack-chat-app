import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/repository/assign_switchlist_repository.dart';

class CustomGroupDropdown extends StatefulWidget {
  CustomGroupDropdown({
    this.assingedSwitchlistModel,
    @required this.railcar,
    this.selectedEquipments,
    this.refreshScreen
  });

  final Equipment railcar;
  NewSwitchListEquipment assingedSwitchlistModel;
  List<Equipment> selectedEquipments;
  Function() refreshScreen;



  AssignSwitchListRepo assignSwitchListRepo = AssignSwitchListRepo();
  @override
  _CustomGroupDropdownState createState() => _CustomGroupDropdownState(assingedSwitchlistModel);
}

class _CustomGroupDropdownState extends State<CustomGroupDropdown> {

  List<YardModel> yardList =[];
  List<TrackModel> trackList = [];
  List<TrackModel> filteredTracksList =[];
  List<SpotModel> spotList =[];
   final NewSwitchListEquipment switchListEquipment;
  _CustomGroupDropdownState(this.switchListEquipment);

  onYardChange(String value) {
      switchListEquipment.to_track_id = null;
      switchListEquipment.to_spot_id = null;
      widget.railcar.toTrack = null;
      widget.railcar.tospot = null;

    widget.railcar.toYard=yardList.singleWhere((element) => element.yardName == value);
    switchListEquipment.to_yard_id=widget.railcar.toYard.id;

    var matchedYardObject = yardList.where((element) => element.yardName == value).map((e) => e.id).toList();

    var  filteredTracks = trackList.where((element) => element.yardId==matchedYardObject[0]).toList();
      filteredTracksList = [];

      filteredTracks.sort((a, b) => a.trackName.compareTo(b.trackName));

      setState(() {
      filteredTracksList = filteredTracks;

    });

  }

  onTrackChange(String value) {
    switchListEquipment.to_spot_id = null;
    widget.railcar.toTrack=trackList.singleWhere((element) => element.trackName == value);
    switchListEquipment.to_track_id=widget.railcar.toTrack.id;
    var matchedTrackObject = trackList.where((element) => element.trackName == value).map((e) => e.id).toList();
    spotList = MemoryData.inMemorySpotMap.values.toList();
    spotList = spotList.where((element) => element.trackId==matchedTrackObject[0]).toList();
    spotList.sort((a, b) => a.spotName.compareTo(b.spotName));

    setState(() {
    });
  }

  onSpotChange(String value) {
    widget.railcar.tospot=spotList.singleWhere((element) => element.spotName == value);
    switchListEquipment.to_spot_id=widget.railcar.tospot.id;
    setState(() {

     });
  }


  @override
  void initState() {
    // TODO: implement initState
      yardList = MemoryData.inMemoryYardMap.values.toList();
      trackList = MemoryData.inMemoryTrackMap.values.toList();
      yardList.sort((a, b) => a.yardName.compareTo(b.yardName));
      trackList.sort((a, b) => a.trackName.compareTo(b.trackName));
     setState(() {
       filteredTracksList = trackList;
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 200,
          alignment: Alignment.center,
          child: ListTile(
            title: Text(widget.railcar.equipmentInitial+" "+widget.railcar.equipmentNumber,
                style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: ScreenUtil().setHeight(20),
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                    letterSpacing: ScreenUtil().setWidth(0.25),
                    color: Theme.of(context).textTheme.body1.color)),
          ),
        ),
        Container(
          width: ScreenUtil().setWidth(214),
          child: CustomDropdown(
              hintText: "Yard",
              itemList: yardList.isNotEmpty == true
                  ? yardList.map((e) => e.yardName).toList()
                  : const [],
               SelectedValue:switchListEquipment.to_yard_id == null ? null : MemoryData.inMemoryYardMap[switchListEquipment.to_yard_id].yardName  ,
              Ontap: (str) => onYardChange(str)),
        ),
        SizedBox(
          width: ScreenUtil().setWidth(23),
        ),
        Container(
          width: ScreenUtil().setWidth(214),
          child: CustomDropdown(
              hintText: "Track",
              itemList: filteredTracksList.isNotEmpty == true
                  ? filteredTracksList.map((e) => e.trackName).toList()
                  : const [],
              SelectedValue:switchListEquipment.to_track_id == null ? null : MemoryData.inMemoryTrackMap[switchListEquipment.to_track_id].trackName  ,

              Ontap: (value) => onTrackChange(value)),
        ),
        SizedBox(
          width: ScreenUtil().setWidth(23),
        ),
        spotList.isNotEmpty?

        Container(
          width: ScreenUtil().setWidth(214),
          child: CustomDropdown(
              hintText: "Spot",
              itemList: spotList.isNotEmpty == true
                  ? spotList.map((e) => e.spotName).toList()
                  : const [],
              SelectedValue:switchListEquipment.to_spot_id == null ? null : MemoryData.inMemorySpotMap[switchListEquipment.to_spot_id].spotName  ,
              Ontap: (value) => onSpotChange(value)),
        )
            :Container(
          width: ScreenUtil().setWidth(213),
          height: ScreenUtil().setHeight(48),
        ),

       widget.selectedEquipments.length > 1? Container(
          child: IconButton(
            icon: Icon(Icons.delete),
            iconSize: ScreenUtil().setWidth(35),
            color: Theme.of(context).textTheme.bodyText1.color,
            highlightColor: Colors.red,
            hoverColor: Colors.green,
            disabledColor: Colors.amber,
            onPressed: () {
              widget.selectedEquipments.removeWhere((item) => item.id == widget.railcar.id );
              widget.refreshScreen();
            },
          ),
        ) :Container(
       )

      ],
    );

    ;
  }
}
