import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown-generic.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/components/screen_factory.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/components/custom_group_dropdown/custom_group_dropdown.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/model/assigned_switchlist_model.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/repository/assign_switchlist_repository.dart';
import 'package:mbase/base/modules/listview/listview/ui/listview_screen.dart';
import 'package:mbase/env.dart';

class AssignSwitchListPage extends StatelessWidget {
  final List<Equipment> selectedEquipments;

  AssignSwitchListPage(this.selectedEquipments);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar(),
      drawer: AppDrawer(),
      body: AssignSwitchlist(selectedEquipments),
    );
  }
}

class AssignSwitchlist extends StatefulWidget {
  final List<Equipment> selectedEquipments;

  AssignSwitchlist(this.selectedEquipments);

  @override
  _AssignSwitchlistState createState() =>
      _AssignSwitchlistState(selectedEquipments);
}

class _AssignSwitchlistState extends State<AssignSwitchlist> {
  List<SwitchListEquipmentModel> assignedSwitchList = [];

  List<Equipment> selectedEquipments;

  _AssignSwitchlistState(this.selectedEquipments);

  final globalKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  FToast fToast;
  AssignSwitchListRepo assignSwitchListRepo = new AssignSwitchListRepo();

  bool yardSelected = false;
  String selectedYardName;
  bool trackSelected = false;
  String selectedTrackName;
  bool spotSelected = false;
  String selectedSpotName;
  String switchListName = null;
  String targetStart = null;
  String targetComplete = null;
  String switchListID = null;
  String swichlistStatus = null;

  String switchCrew = null;
  String switchCrewName = null;

  List<NewSwitchListEquipment> railcarsInSelectedSwitchlist =
      List<NewSwitchListEquipment>();

  EquipmentDAO equipment_dao = EquipmentDAO();

  bool verifyAssignSwitchListRailCar = false;
  final DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm');

  List<NewSwitchListEquipment> assignedRailCarList =
      List<NewSwitchListEquipment>();

  List<String> spotList2 = [];

  List<bool> isSelected;
  List<NewSwitchListEquipment> tempCarList = List<NewSwitchListEquipment>();

  List<NewSwitchList> switchLists = [];
  NewSwitchList selectedSwitchlist;

  NewSwitchList switchlistHeader = NewSwitchList();

  Map<String, List<String>> verifiedAssignToSwitchlistCars = {};

  onSwitchListChange(NewSwitchList value) {
    setState(() {
      this.selectedSwitchlist = value;
      switchListID = this.selectedSwitchlist.id;
      switchListName = this.selectedSwitchlist.switch_list_name;
      switchCrewName = this.selectedSwitchlist.switch_crew_name;
      swichlistStatus = this.selectedSwitchlist.switch_list_status;
      //     targetStart = formatter.format(DateTime.parse(e.target_start_date.toString()));
      targetStart = this.selectedSwitchlist.target_start_date;
      targetComplete = this.selectedSwitchlist.target_complete_date;
      //   targetComplete = formatter.format(DateTime.parse(e.target_complete_date.toString()));
      switchCrew = this.selectedSwitchlist.switch_crew;
      railcarsInSelectedSwitchlist = this.selectedSwitchlist.railcars;
    });
  }

  @override
  void initState() {
    super.initState();

    fToast = FToast(context);

    isSelected = [false, false];

    assignSwitchListRepo.fetchSwitchList().then((returnValue) {
      returnValue
          .sort((a, b) => a.switch_list_name.compareTo(b.switch_list_name));
      setState(() {
        switchLists = returnValue;
      });
    });
  }

  checkBeforeExit() async {
    if (selectedSwitchlist != null) {
      final action = await CustomDialog.cstmDialog(
          context, "add_railcar", "Unsaved Changes", "");
      if (action[0] == DialogAction.yes) {
        // Navigator.pop(context);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => ScreenFactory(ListViewScreen()),
          ),
        );
      }
    } else {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => ScreenFactory(ListViewScreen()),
        ),
      );
      //  Navigator.of(context).pop();
    }
  }

  _showToast({String message, Color color, IconData icon}) {
    fToast.showToast(
      child: CustomToast(
        toastColor: color,
        toastMessage: message,
        icon: icon,
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  void _showToastError(List<String> errorList) {
    Widget toast = Container(
        width: ScreenUtil().setWidth(552),
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
        decoration: BoxDecoration(
          color: Colors.redAccent,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: errorList.map((e) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.warning),
                SizedBox(
                  width: 12.0,
                ),
                Expanded(
                  child: Text(
                    e,
                    style: TextStyle(
                      // ignore: deprecated_member_use
                      color: Theme.of(context).textTheme.body1.color,
                    ),
                  ),
                ),
              ],
            );
          }).toList(),
        ));

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  refreshScreen() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);

    // checkBeforeAssignToSwitchList() async {
    //   Map<String, List<String>> verifiedAssignToSwitchlistCars = {};
    //   List<String> alreadyAvailableRailcarList = [];
    //   for (NewSwitchListEquipment newswitchlisteq in selectedSwitchlist.railcars) {
    //     for (Equipment equipment in selectedEquipments) {
    //       if (newswitchlisteq.equipment_initial == equipment.equipmentInitial && newswitchlisteq.equipment_number == equipment.equipmentNumber) {
    //
    //         alreadyAvailableRailcarList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has been already assigned ");
    //         verifiedAssignToSwitchlistCars["railcars"] = alreadyAvailableRailcarList;
    //       }
    //     }
    //   }
    //   return verifiedAssignToSwitchlistCars;
    // }

    checkIfSelectedRailcarsExistInSwitchList(
        {NewSwitchListEquipment selectedRailcar,
        List<NewSwitchListEquipment> assignedRailCarList}) {
      List<String> alreadyAvailableRailcarList = [];

      var contain = assignedRailCarList?.where((element) =>
          element.equipment_initial == selectedRailcar.equipment_initial &&
          element.equipment_number == selectedRailcar.equipment_number);
      if (contain?.isEmpty == true) {
        tempCarList.add(selectedRailcar);
        switchlistHeader.railcars = tempCarList;
      } else if (contain?.isEmpty == false) {
        alreadyAvailableRailcarList.add(
            "${selectedRailcar.equipment_initial + " " + selectedRailcar.equipment_number} has been already assigned ");
        verifiedAssignToSwitchlistCars["railcars"] =
            alreadyAvailableRailcarList;
      }
    }

    clearScannedEquipList() {
      List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"];
      if (scannedEquipList != null) {
        MemoryData.dataMap
            .removeWhere((key, value) => key == "scannedEquipList");
      }
    }

    _assignSwitchList() async {
      Map<String, List<String>> alreadyAssignedRailCar = {};
      tempCarList = [];

      if (this.selectedSwitchlist == null) {
        _showToast(
            message: "Select a switch list to proceed.",
            color: Colors.red,
            icon: Icons.new_releases);
      } else {
        int count = 0;

        //   railcarsInSelectedSwitchlist.forEach((e) => assignedRailCarList.add(e));

        selectedEquipments?.forEach((equipment) {
          NewSwitchListEquipment switchListEquipment = NewSwitchListEquipment();

          //SwitchList Details
          switchlistHeader.target_start_date = targetStart;
          switchlistHeader.action_type = APP_CONST.ASSIGN_RAILCAR;
          switchlistHeader.operation_type = APP_CONST.SWITCHLIST_OPERATION;
          switchlistHeader.scy_pk = switchListID;
          switchlistHeader.parent_id = equipment?.parent_id ?? switchListID;
          switchlistHeader.immediateParentId = switchListID;
          switchlistHeader.facility_id = env.userProfile.facilityId;
          //switchlistHeader.createdDt = DateTime.now().toString();

          switchlistHeader.switch_crew = null;
          switchlistHeader.switch_crew_name = switchCrewName;

          ///switchCrew.toString();
          switchlistHeader.source = APP_CONST.MOBILITY;
          switchlistHeader.user = env.userProfile.userName;
          switchlistHeader.permission_id = env.userProfile.id;
          switchlistHeader.transaction_status =
              APP_CONST.TRANSACTION_STATUS_PENDING;
          switchlistHeader.switch_list_status = swichlistStatus;
          switchlistHeader.switch_list_name = switchListName;

          //SwitchList Assigned Railcar Details
          switchListEquipment.equipment_number = equipment.equipmentNumber;
          switchListEquipment.equipment_initial = equipment.equipmentInitial;
          switchListEquipment.scy_pk = null;
          switchListEquipment.railcar_action_type =
              APP_CONST.SWITCHLIST_ASSIGNED_RAILCAR;
          switchListEquipment.from_yard_id = equipment?.yardId;
          switchListEquipment.from_track_id = equipment?.trackId;
          switchListEquipment.from_spot_id = equipment?.spotId;
          switchListEquipment.switch_list_equipment_status =
              APP_CONST.SWITCHLIST_EQUIPMENT_STATUS_NOT_STARTED;
          switchListEquipment.to_yard_id = equipment.toYard?.id;
          switchListEquipment.to_track_id = equipment.toTrack?.id;
          switchListEquipment.to_spot_id = equipment.tospot?.id;

          switchListEquipment.parent_id = null;
          switchListEquipment.permission_id = env.userProfile.id;
          switchListEquipment.asset_master_id = equipment?.assetMasterId;
          switchListEquipment.transaction_status =
              APP_CONST.TRANSACTION_STATUS_PENDING;

          switchListEquipment.placed_date = equipment.placedDate.toString();
          // assignedRailCarList.add(switchListEquipment);

          checkIfSelectedRailcarsExistInSwitchList(
              selectedRailcar: switchListEquipment,
              assignedRailCarList: railcarsInSelectedSwitchlist);

          count++;
        });

        // alreadyAssignedRailCar = await checkBeforeAssignToSwitchList();

        List<String> assigned_rail_cars =
            verifiedAssignToSwitchlistCars["railcars"] ?? [];
        // assignSwitchListRepo.addSwitchList(switchlistHeader).then((value) {
        //   return _showToast(message: "Railcar added to switch list successfully.", color: Color(0xff7fae1b),icon: Icons.check);
        // }).then((value) => Navigator.pop(context));
        if (assigned_rail_cars.isEmpty) {
          switchlistHeader?.railcars?.addAll(railcarsInSelectedSwitchlist);
          print("switchlist header-----> ${switchlistHeader.railcars}");
          assignSwitchListRepo.addSwitchList(switchlistHeader).then((value) {
//            clearScannedEquipList();
            return _showToast(
                message: "Railcar added to switch list successfully.",
                color: Color(0xff7fae1b),
                icon: Icons.check);
          }).then((value) => Navigator.pop(context));
        } else {
          _showToastError(assigned_rail_cars.toSet().toList());
          verifiedAssignToSwitchlistCars["railcars"] = [];
        }
      }
    }

    Widget FormUI() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: CardUI(
                  content: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Container(
                                    child: ListTile(
                                  leading: GestureDetector(
                                      onTap: () {
                                        checkBeforeExit();
                                      },
                                      child: Icon(
                                        Icons.arrow_back,
                                      )),
                                  title: Text(
                                      'Assign To Switch List (${selectedEquipments.length})',
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(24,
                                              allowFontScalingSelf: true),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: 'Roboto',
                                          color: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .color)),
                                ))),
                          ],
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(20),
                                  child: Container(
                                      child: GestureDetector(
                                    onTap: () {
                                      checkBeforeExit();
                                    },
                                    child: RichText(
                                      text: TextSpan(
                                          style:
                                              Theme.of(context).textTheme.body1,
                                          children: <InlineSpan>[
                                            TextSpan(
                                                text: 'CANCEL',
                                                style: TextStyle(
                                                    fontSize: ScreenUtil().setSp(
                                                        14,
                                                        allowFontScalingSelf:
                                                            true),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(1.25),
                                                    fontFamily: 'Roboto',
                                                    color: Color(0xFF3e8aeb)))
                                          ]),
                                    ),
                                  ))),
                              Padding(
                                  padding: EdgeInsets.all(20),
                                  child: Container(
                                    height: 50,
                                    child: FlatButton(
                                        onPressed: _assignSwitchList,
                                        child: Text('ASSIGN TO SWITCH LIST',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(14),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(1.25),
                                                color: Colors.white)),
                                        color: Color(0xFF3e8aeb),
                                        textColor: Colors.white,
                                        disabledColor: Color(0xFF3e8aeb),
                                        disabledTextColor: Colors.black,
                                        splashColor: Color(0xFF3e8aeb)),
                                  )),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(24),
                  ),
                  Divider(
                    thickness: 2.0,
                    color: Color.fromRGBO(0, 0, 0, 1),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(24),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Wrap(
                      direction: Axis.horizontal,
                      spacing: ScreenUtil().setWidth(25),
                      runSpacing: ScreenUtil().setHeight(5),
                      children: <Widget>[
                        Container(
                          width: ScreenUtil().setWidth(213),
                          child: CustomDropdownGeneric<NewSwitchList>(
                              error: selectedSwitchlist == null ? true : false,
                              hintText: "Select a Switch List",
                              itemList: switchLists.isNotEmpty == true
                                  ? switchLists
                                  : const [],
                              SelectedValue: selectedSwitchlist,
                              Ontap: (NewSwitchList selectedValue) {
                                onSwitchListChange(selectedValue);
                              }),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(135),
                          alignment: Alignment.center,
                          child: Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Column(
                                children: [
                                  Text(
                                    'Target Start',
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                  Padding(
                                      padding:
                                          EdgeInsets.only(top: 10, left: 10),
                                      child: Text(
                                        targetStart != null
                                            ? formatter
                                                    .format(DateTime.parse(
                                                        targetStart ?? ""))
                                                    .toString() ??
                                                ''
                                            : "",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                      )),
                                ],
                              )),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(145),
                          alignment: Alignment.center,
                          child: Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Column(
                                children: [
                                  Text(
                                    'Target Complete',
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                  Padding(
                                      padding:
                                          EdgeInsets.only(top: 10, left: 30),
                                      child: Text(
                                        targetComplete == null
                                            ? " "
                                            : targetComplete,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                      )),
                                ],
                              )),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(115),
                          alignment: Alignment.center,
                          child: Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Column(
                                children: [
                                  Text(
                                    'Switch Crew',
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                  Container(
                                    width: ScreenUtil().setWidth(120),
                                    padding: EdgeInsets.only(top: 10, left: 10),
                                    child: Text(
                                      switchCrewName ?? '', // TODO:
                                      maxLines: 1,
                                      softWrap: false,
                                      overflow: TextOverflow.visible,
                                      style:
                                          Theme.of(context).textTheme.bodyText2,
                                    ),
                                  ),
                                ],
                              )),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(31.5),
                  ),
                  Divider(
                    thickness: 1.0,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(31.5),
                  ),
                  Container(
                      width: ScreenUtil().setWidth(115),
                      alignment: Alignment.center,
                      child: ListTile(
                        title: Text(
                          'Railcar',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      )),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Wrap(
                          direction: Axis.horizontal,
                          spacing: 10,
                          runSpacing: 5,
                          children: selectedEquipments == null ||
                                  selectedEquipments.isEmpty
                              ? Container
                              : selectedEquipments.map((railcar) {
                                  //var swEquipme/nt=  NewSwitchListEquipment();
                                  // selec/tedS/witchlist?.railcars?.add(swEquipment);
                                  return CustomGroupDropdown(
                                    assingedSwitchlistModel:
                                        NewSwitchListEquipment(),
                                    railcar: railcar,
                                    selectedEquipments: selectedEquipments,
                                    refreshScreen: refreshScreen,
                                  );
                                }).toList()))
                ],
              )),
            ),
          )
        ],
      );
    }

    return Scaffold(
        key: globalKey,
        body: Form(
          key: _formKey,
          autovalidate: true,
          child: FormUI(),
        ));
  }
}
