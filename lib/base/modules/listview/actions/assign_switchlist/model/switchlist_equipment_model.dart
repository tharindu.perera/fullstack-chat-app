import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';


SwitchlistEquipmentModel sealModelFromMap(String str) => SwitchlistEquipmentModel.fromMap(json.decode(str));

String sealModelToMap(SwitchlistEquipmentModel data) => json.encode(data.toMap());

class SwitchlistEquipmentModel {
  SwitchlistEquipmentModel(
      {
        this.source,
        this.id,
        this.updatedDt,
        this.createdDt,
        this.reference,
        this.switch_crew,
        this.switch_list_name,
        this.switch_list_type,
        this.switch_list_status,
        this.target_start_date,
        this.facility_id,
        this.target_complete_date,
        this.transaction_status
      });
 
  String id;
  String source;
  DateTime createdDt;
  DateTime updatedDt;
  String switch_crew;
  String switch_list_name;
  String switch_list_type;
  String switch_list_status;
  DateTime target_start_date;
  DateTime target_complete_date;
  String facility_id;
  String transaction_status;
  DocumentReference reference;


  SwitchlistEquipmentModel.fromMap(Map<String, dynamic> map, {this.reference})
      : id = map['id'],
        source = map['source'],
        createdDt = map['created_dt'] != null ? DateTime.parse(map['created_dt']) : null,
        updatedDt = map['updated_dt'] != null ? DateTime.parse(map['updated_dt']) : null,
        switch_crew = map['switch_crew'],
        switch_list_name = map['switch_list_name'],
        switch_list_type = map['switch_list_type'],
        switch_list_status = map['switch_list_status'],
        target_start_date = map['target_start_date'] != null ? DateTime.parse(map['target_start_date']) : null,
        target_complete_date = map['target_complete_date'] != null ? DateTime.parse(map['target_complete_date']) : null,
        facility_id = map['facility_id'],
        transaction_status = map['transaction_status'];

        SwitchlistEquipmentModel.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data(), reference: snapshot.reference);

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "source": source == null ? null : source,
    "created_dt": createdDt == null ? null : createdDt,
    "updated_dt": updatedDt == null ? null : updatedDt,
    "switch_crew": switch_crew == null ? null : switch_crew,
    "switch_list_name": switch_list_name == null ? null : switch_list_name,
    "switch_list_type": switch_list_type == null ? null : switch_list_type,
    "switch_list_status": switch_list_status == null ? null : switch_list_status,
    "target_start_date": target_start_date == null ? null : target_start_date,
    "target_complete_date": target_complete_date == null ? null : target_complete_date,
    "facility_id": facility_id == null ? null : facility_id,
    "transaction_status": transaction_status == null ? null : transaction_status,
  };

  @override
  String toString() {
    return 'SwitchlistEquipmentModel{id: $id,source:${source} ,created_dt: $createdDt,updated_dt: $updatedDt ,'
        'switch_crew :$switch_crew , switch_list_name: $switch_list_name,  switch_list_type: $switch_list_type, switch_list_status: $switch_list_status, '
        'target_start_date: $target_start_date},target_complete_date: $target_complete_date,facility_id: $facility_id,transaction_status: $transaction_status,';
  }
}