    class AssignSwitchlistDTO {
  AssignSwitchlistDTO(
      {this.selectedSwitchlist,
      this.selectedYardName,
      this.selectedTrackName,
      this.selectedSpotName,
      this.selectedEquipmentList});

  String selectedSwitchlist;
  String selectedYardName;
  String selectedTrackName;
  String selectedSpotName;
  List<String> selectedEquipmentList;
}
