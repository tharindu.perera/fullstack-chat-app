import 'package:cloud_firestore/cloud_firestore.dart';


// AssignedSwitchListModel sealModelFromMap(String str) => AssignedSwitchListModel.fromMap(json.decode(str));
//
// String sealModelToMap(AssignedSwitchListModel data) => json.encode(data.toMap());

class SwitchListEquipmentModel {
  SwitchListEquipmentModel(
      {
        this.source,
        this.id,
        this.updatedDt,
        this.createdDt,
        this.reference,
        this.switch_list_id,
        this.from_yard_id,
        this.from_track_id,
        this.from_spot_id,
        this.to_yard_id,
        this.to_track_id,
        this.to_spot_id,
        this.equipment_id,
        this.equipment_initial,
        this.equipment_number,
        this.switch_list_equipment_status,
        this.switch_list_comment,
        this.operation_type,
        this.comment,
        this.facility_id,
        this.parent_id,
        this.transaction_status,
        this.permission_id
      });

  String id;
  String source;
  String switch_list_id;
  String from_yard_id;
  String from_track_id;
  String from_spot_id;
  String to_yard_id;
  String to_track_id;
  String to_spot_id;
  String equipment_id;
  String equipment_initial;
  String equipment_number;
  String switch_list_equipment_status;
  String switch_list_comment;
  String comment;
  DateTime createdDt;
  DateTime updatedDt;
  String transaction_status;
  String operation_type;
  String facility_id;
  String parent_id;
  String permission_id;
  DocumentReference reference;


  SwitchListEquipmentModel.fromMap(Map<String, dynamic> map, {this.reference})
      : id = map['id'],
        source = map['source'],
        createdDt = map['created_dt'] != null ? DateTime.parse(map['created_dt']) : null,
        updatedDt = map['updated_dt'] != null ? DateTime.parse(map['updated_dt']) : null,
        transaction_status = map['transaction_status'],
        switch_list_id = map['switch_list_id'],
        from_yard_id = map['from_yard_id'],
        from_track_id = map['from_track_id'],
        from_spot_id = map['from_spot_id'],
        to_yard_id = map['to_yard_id'],
        to_track_id = map['to_track_id'],
        to_spot_id = map['to_spot_id'],
        equipment_id = map['equipment_id'],
        equipment_initial = map['equipment_initial'],
        equipment_number = map['equipment_number'],
        switch_list_equipment_status = map['switch_list_equipment_status'],
        switch_list_comment = map['switch_list_comment'],
        comment = map['comment'],
        operation_type = map['operation_type'],
        facility_id = map['facility_id'],
        parent_id = map['parent_id'],
        permission_id = map['[permission_id]'];

  SwitchListEquipmentModel.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data(), reference: snapshot.reference);

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "source": source == null ? null : source,
    "created_dt": createdDt == null ? null : createdDt.toString(),
    "updated_dt": updatedDt == null ? null : updatedDt.toString(),
    "transaction_status": transaction_status == null ? null : transaction_status,
    "switch_list_id": switch_list_id == null ? null : switch_list_id,
    "from_yard_id": from_yard_id == null ? null : from_yard_id,
    "from_track_id": from_track_id == null ? null : from_track_id,
    "from_spot_id": from_spot_id == null ? null : from_spot_id,
    "to_yard_id": to_yard_id == null ? null : to_yard_id,
    "to_track_id": to_track_id == null ? null : to_track_id,
    "to_spot_id": to_spot_id == null ? null : to_spot_id,
    "equipment_id": equipment_id == null ? null : equipment_id,
    "equipment_initial": equipment_initial == null ? null : equipment_initial,
    "equipment_number": equipment_number == null ? null : equipment_number,
    "switch_list_equipment_status": switch_list_equipment_status == null ? null : switch_list_equipment_status,
    "switch_list_comment": switch_list_comment == null ? null : switch_list_comment,
    "comment": comment == null ? null : comment,
    "operation_type" :operation_type == null? null :operation_type,
    "facility_id" :facility_id == null? null :facility_id,
    "parent_id" :parent_id == null? null :parent_id,
    "permission_id":permission_id == null ? null :permission_id
  };

  @override
  String toString() {
    return 'AssignedSwitchListModel{id: $id,source:${source} ,created_dt: $createdDt,updated_dt: $updatedDt,transaction_status: $transaction_status'
        'switch_list_id:$switch_list_id,from_yard_id:$from_yard_id,from_track_id:$from_track_id,'
        'from_spot_id:$from_spot_id,to_yard_id:$to_yard_id,to_track_id:$to_track_id,to_spot_id:$to_spot_id'
        'equipment_id:$equipment_id,equipment_initial:$equipment_initial,equipment_number:$equipment_number'
        'switch_list_equipment_status:$switch_list_equipment_status,switch_list_comment:$switch_list_comment, operation_type:$operation_type,facility_id:$facility_id'
        'comment:$comment,parent_id:$parent_id,permission_id:$permission_id';
  }
}


