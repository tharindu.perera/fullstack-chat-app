import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/dao/assign_switchlist_dao.dart';

class AssignSwitchListRepo {
  List<NewSwitchList> SwitchlistList = List<NewSwitchList>();

  AssignSwitchlistDao assignSwitchlistDao = AssignSwitchlistDao();
  EquipmentDAO equipmentDAO = EquipmentDAO();

  List<NewSwitchList> _list = List<NewSwitchList>();

  Future<List<NewSwitchList>> fetchSwitchList() async {
    return await assignSwitchlistDao.getRecords();
  }

  Future<List<NewSwitchList>> fetchSwitchListBySelection(String switchlist_name) async {
    List<NewSwitchList> selected_switchlist = SwitchlistList.where((element) => element.switch_list_name == switchlist_name).toList();

    return selected_switchlist;
  }

  Future<bool> addSwitchList(NewSwitchList assignedSwitchListModel) async {
    return assignSwitchlistDao.add(assignedSwitchListModel);
  }

  Future<List<SpotModel>> getEmptySpots({String selectedYardId,String selectedTrackId, List<SpotModel> spotsAccordingToYardTrack}) async{
    print("spot count for selection --- > ${spotsAccordingToYardTrack.length}");

    List<SpotModel> availableSpots = [];

    for(var spot in spotsAccordingToYardTrack) {
      await equipmentDAO.checkForSpotAvailability(Yard: selectedYardId,Track: selectedTrackId,Spot: spot.id).then((value) {
        if(spot.id != value?.spotId){
          availableSpots.add(spot);
        }

      });
    }

      print("empty spots----> ${availableSpots.length}");

    return availableSpots;



    }

  Future<bool> checkWithSwitchList({NewSwitchList newSwitchList} // SwitchListEquipmentModel assignedSwitchListModel
      ) async {
    bool returnValue = false;

    _list.forEach((listElement) {
      if (listElement.switch_list_name == newSwitchList.switch_list_name) {
        if (newSwitchList.railcars != null) {
          listElement.railcars.forEach((element) {
            newSwitchList.railcars.forEach((value) {
              if (element.from_yard_id == value.from_yard_id && element.from_track_id == value.from_track_id && value.from_spot_id == value.from_spot_id) {
                returnValue = true;
              }
            });
          });
        }
      }
    });

    return returnValue;
  }

  Future<List<String>> fetchYards() async {
    List<String> yards = List<String>();
    List<YardModel> fetchedYards = await assignSwitchlistDao.getYards();
    fetchedYards.forEach((e) => yards.add(e.yardName));
    return yards;
  }

  Future<List<String>> fetchTracksByYardName({String yardName}) async {
    List<String> tracks = List<String>();
    List<TrackModel> fetchedTracks = await assignSwitchlistDao.getTracks();

    return [];
  }
}
