import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:mbase/base/core/util/date_time_util.dart';

 // ignore: must_be_immutable
 class Comment extends Equatable {
  Comment(
      {this.facilityId,
      this.assetMasterId,
      this.facilityVisitId,
      this.id,
      this.permissionID,
      this.createdDt,
      this.updatedDt,
      this.source,
      this.scyPK,
      this.comment,
      this.commentType,
      this.equipmentID,
      this.user,
      this.operationType,
      this.comment2,
      this.transactionStatus,
      this.last_modified_date});

  String facilityId;
  String assetMasterId;
  String facilityVisitId;
  String id;
  DateTime createdDt;
  DateTime updatedDt;
  String permissionID;
  String scyPK;
  String equipmentID;
  String comment;
  String commentType;
  String user;
  String updated_user;
  String operationType;
  String source;
  String comment2;
  String transactionStatus;
  DateTime last_modified_date;

  @override
  List<Object> get props => [id];

  Comment.fromMap(Map<String, dynamic> map)
      : scyPK = map["scy_pk"].toString(),
        equipmentID = map["equipment_id"],
        comment = map["comment"],
        commentType = map["comment_type"],
        facilityId = map['facility_id'],
        assetMasterId = map['asset_master_id'],
        facilityVisitId = map['facility_visit_id'],
        user = map['user'],
        id = map['id'],
        permissionID = map["permission_id"],
        createdDt = map['created_dt'] != null ? getLocalDateTimeFromUTCStr(map['created_dt']) : null,
        updatedDt = map['updated_dt'] != null ?getLocalDateTimeFromUTCStr(map['updated_dt']) : null,
        last_modified_date = map['last_modified_date'] != null ? getLocalDateTimeFromUTCStr(map['last_modified_date']) : null,
        source = map['source'],
        transactionStatus = map['transaction_status'],
        comment2 = map['comment2'],
        operationType = map['operation_type'];

  Comment.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data());

  Map<String, dynamic> toMap() => {
        "equipment_id": equipmentID == null ? null : equipmentID,
        "comment": comment == null ? null : comment,
        "comment_type": commentType == null ? null : commentType,
        "scy_pk": scyPK == null ? null : scyPK,
        "facility_id": facilityId == null ? null : facilityId,
        "asset_master_id": assetMasterId == null ? null : assetMasterId,
        "facility_visit_id": facilityVisitId == null ? null : facilityVisitId,
        "id": id == null ? null : id,
        "permission_id": permissionID == null ? null : permissionID,
        "user": user == null ? null : user,
        "created_dt": createdDt == null ? null : getUTCTimeFromLocalDateTime(createdDt).toString(),
        "updated_dt": updatedDt == null ? null : getUTCTimeFromLocalDateTime(createdDt).toString(),
        "source": source == null ? null : source,
        "transaction_status": transactionStatus == null ? null : transactionStatus,
        "operation_type": operationType == null ? null : operationType,
        "last_modified_date": last_modified_date == null ? null : getUTCTimeFromLocalDateTime(last_modified_date).toString(),
        "comment2": comment2 == null ? null : comment2,
      };

  @override
  String toString() {
    return 'Comment{facilityId: $facilityId, assetMasterId: $assetMasterId, facilityVisitId: $facilityVisitId, id: $id, createdDt: $createdDt, updatedDt: $updatedDt, permissionID: $permissionID, scyPK: $scyPK, equipmentID: $equipmentID, comment: $comment, commentType: $commentType, user: $user, updated_user: $updated_user, operationType: $operationType, source: $source, comment2: $comment2, transactionStatus: $transactionStatus}';
  }


 }
