import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/modules/listview/actions/comment/model/comment_model.dart';
import 'package:uuid/uuid.dart';

class CommentDAO extends BaseDao {
  CommentDAO() : super("listview_comment");

  Future<Comment> add(Comment comment) async {
    comment.id = Uuid().v1();
    await collection.doc(comment.id).set(comment.toMap());
    return comment;
  }

  Future<Comment> get(Comment comment) async {
    comment.id = Uuid().v1();
    await collection.doc(comment.id).set(comment.toMap());
    return comment;
  }


  Future<List<Comment>> getCommentsForEquipment(Equipment equipment) async {
    print("Fetching Comments For Equipment...... facility_visit_id =${equipment.facilityVisitId} asset_master_id =${equipment.assetMasterId}");

    return BaseQuery()
        .where("facility_visit_id", isEqualTo: equipment.facilityVisitId)
        .where("asset_master_id", isEqualTo: equipment.assetMasterId)
        .get()
        .then((value) =>
            value.docs.map((e) => Comment.fromMap(e.data())).toList());
  }

  Future<List<Comment>> getCommentsForFlyOver(Equipment equipment) async {
    print("Fetching Comments For Equipment Flyover...... facility_visit_id =${equipment.facilityVisitId} asset_master_id =${equipment.assetMasterId}");

    return collection
//        .where("facility_visit_id", isEqualTo: equipment.facilityVisitId)
        .where("asset_master_id", isEqualTo: equipment.assetMasterId)
        .orderBy("last_modified_date")
        .get()
        .then((value) =>
        value.docs.map((e) => Comment.fromMap(e.data())).toList());
  }


  Future<List<Comment>> getComments () async {
     return await collection
         .where("asset_master_id", isNotEqualTo: null)
         .get().then((value) => value.docs.map((element) => Comment.fromMap(element.data())).toList());
  }


}
