import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/listview/actions/comment/repository/comment_repository.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class LeaveComment extends StatefulWidget {
  List<Equipment> selectedEquipments = List<Equipment>();
  String source;

  LeaveComment(List<Equipment> selectedEquipments, String source) {
    this.selectedEquipments = selectedEquipments;
    this.source = source;
  }

  @override
  _LeaveCommentState createState() =>
      _LeaveCommentState(selectedEquipments, source);
}

class _LeaveCommentState extends State<LeaveComment> {
  List<Equipment> selectedEquipments = List<Equipment>();

  _LeaveCommentState(List<Equipment> selectedEquipments, String source) {
    this.selectedEquipments = selectedEquipments;
    this.source = source;
  }

  bool checkBoxValue = false;
  final globalKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _commentController = TextEditingController();
  CommentRepository _commentRepository = CommentRepository();
  FToast fToast;
  String source;

  final emptyComment =
      SnackBar(content: Text('Comment field cannot be empty.'));
  final selectComment = SnackBar(
      content: Text(
          'Comment Type cannot be empty, Please select the comment type to continue.'));
  final successMessage = SnackBar(content: Text('Request Successful.'));

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);
  }

  clearScannedEquipList() {
    List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"];
    if (scannedEquipList != null) {
//      MemoryData.dataMap.removeWhere((key, value) => key == "scannedEquipList");
    }
  }

  _addComment() async {
    String commentInput = _commentController.text;

    if (commentInput.isNotEmpty || _formKey.currentState.validate()) {
      await _commentRepository
          .addComment(
              selectedEquipments, _commentController.text, checkBoxValue)
          .then((value) {
        // scannedEquipList needs to be cleared inorder to clear the AEI grid after a operation is performed (Only applicable when navigating from AEI actions).
//        clearScannedEquipList();
        _showToast(
            toastColor: Color(0xff7fae1b),
            message: "Comment Added Successfully.",
            icon: Icons.check);
        Navigator.pop(context);
        print("source:$source");
        if (APP_CONST.AEI_SCAN == source) {
          BlocProvider.of<AEIScanBloc>(context)
              .add(AEIScanFetchDataWithLastSetCriteria());
        } else if (APP_CONST.YARD_VIEW == source) {
          BlocProvider.of<YardViewBloc>(context)
              .add(YardViewFetchDataWithLastSetCriteria());
        } else {
          BlocProvider.of<ListViewBloc>(context)
              .add(ListViewFetchDataWithLastSetCriteria());
        }
      });
    } else {
      _showToast(
          toastColor: Colors.red,
          message: "Please Add Your Comment To Proceed",
          icon: Icons.warning);
    }
  }

  _showToast({Color toastColor, String message, IconData icon}) {
    fToast.showToast(
      child: CustomToast(
        toastColor: toastColor,
        toastMessage: message,
        icon: icon,
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  onRequesSuccess() async {
    List<Object> returnList = await CustomDialog.cstmDialog(context, "success",
        "Request Successful", "Changes have been added to the system");
    print("returnList--> ${returnList.toString()}");
    returnList[0] == DialogAction.yes ? Navigator.pop(context) : null;
  }

  popUpDialog() async {
    if (_commentController.text.isNotEmpty) {
      final action = await CustomDialog.cstmDialog(
          context, "add_railcar", "Unsaved Changes", "");
      if (action[0] == DialogAction.yes) {
        Navigator.pop(context);
      }
    } else {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    Widget FormUI() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: CardUI(
                  content: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(20),
                                  child: Container(
                                      child: ListTile(
                                    leading: GestureDetector(
                                        onTap: () {
                                          popUpDialog();
                                        },
                                        child: Icon(
                                          Icons.arrow_back,
                                        )),
                                    title: Text(
                                        'Leave Comment (${selectedEquipments.length})',
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(24,
                                                allowFontScalingSelf: true),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'Roboto',
                                            color: Theme.of(context)
                                                .textTheme
                                                // ignore: deprecated_member_use
                                                .headline
                                                .color)),
                                  ))),
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        child: GestureDetector(
                                      onTap: () {
                                        popUpDialog();
                                      },
                                      child: RichText(
                                        text: TextSpan(
                                            style: Theme.of(context)
                                                .textTheme
                                                // ignore: deprecated_member_use
                                                .body1,
                                            children: <InlineSpan>[
                                              TextSpan(
                                                  text: 'CANCEL',
                                                  style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(
                                                          14,
                                                          allowFontScalingSelf:
                                                              true),
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      letterSpacing:
                                                          ScreenUtil()
                                                              .setWidth(1.25),
                                                      fontFamily: 'Roboto',
                                                      color: Color(0xFF3e8aeb)))
                                            ]),
                                      ),
                                    ))),
                                Padding(
                                    padding: EdgeInsets.only(
                                        left: 0,
                                        top: 14,
                                        right: 24,
                                        bottom: 14),
                                    child: Container(
                                      width: ScreenUtil().setWidth(180),
                                      height: ScreenUtil().setHeight(48),
                                      child: FlatButton(
                                          onPressed: _addComment,
                                          // .then((value) {
                                          //        _showToast();
                                          //        Navigator.pop(context);
                                          //        BlocProvider.of<ListViewBloc>(context).add(
                                          //            ListViewFetchDataWithLastSetCriteria());
                                          //    }),
                                          //        .whenComplete(() {
                                          //      _showToast();
                                          //      Navigator.pop(context);
                                          //      BlocProvider.of<ListViewBloc>(context).add(
                                          //          ListViewFetchDataWithLastSetCriteria());
                                          //    }),

                                          child: Text(
                                            'ADD COMMENT',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(14),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(1.25),
                                                color: Colors.white),
                                          ),
                                          color: Color(0xFF3e8aeb),
                                          textColor: Colors.white,
                                          disabledColor: Color(0xFF3e8aeb),
                                          disabledTextColor: Colors.grey,
                                          splashColor: Color(0xFF3e8aeb)),
                                    )),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                    Divider(
                      thickness: 1.0,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(24)),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                            children: selectedEquipments.map((railcar) {
                          var railcar_number = Container(
                              width: ScreenUtil().setWidth(144),
                              margin: EdgeInsets.symmetric(vertical: 4.0),
                              alignment: Alignment.center,
                              child: ListTile(
                                title: Text(
                                  'Railcar',
                                  // ignore: deprecated_member_use
                                  style: Theme.of(context).textTheme.body2,
                                ),
                                subtitle: Text(
                                  '${railcar.equipmentInitial} ${railcar.equipmentNumber}',
                                  // ignore: deprecated_member_use
                                  style: Theme.of(context).textTheme.body2,
                                ),
                              ));
                          return railcar_number;
                        }).toList()),
                      ),
                    ),
                    Divider(
                      thickness: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.all(20),
                            child: Container(
                                width: 300,
                                padding: EdgeInsets.symmetric(vertical: 4.0),
                                alignment: Alignment.center,
                                child: ListTile(
                                  title: Text('Comments',
                                      style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(16),
                                          fontWeight: FontWeight.normal,
                                          fontStyle: FontStyle.normal,
                                          letterSpacing:
                                              ScreenUtil().setWidth(0.5),
                                          color: _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xff182e42)
                                              ? Color(0xFFffffff)
                                              : Color.fromRGBO(0, 0, 0, 0.6))),
                                ))),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: ScreenUtil().setWidth(928),
                          height: ScreenUtil().setHeight(230),
                          child: TextFormField(
                            validator: (String arg) {
                              if (arg.isEmpty)
                                return 'This field is mandatory';
                              else
                                return null;
                            },
                            decoration: InputDecoration(
                                // fillColor: Theme.of(context).scaffoldBackgroundColor,
                                filled: true,
                                fillColor: _themeChanger
                                            .getTheme()
                                            .primaryColor ==
                                        Color(0xff182e42)
                                    ? Color(0xff172636)
                                    : _themeChanger.getTheme().primaryColor ==
                                            Color(0xfff5f5f5)
                                        ? Color(0xfff5f5f5)
                                        : null,
                                focusedBorder: new OutlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          .color),
                                ),
                                border: new OutlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          .color),
                                ),
                                hintText: '',
                                hintStyle: TextStyle(
                                    fontSize: ScreenUtil()
                                        .setSp(14, allowFontScalingSelf: true),
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.italic,
                                    letterSpacing:
                                        ScreenUtil().setWidth(0.25))),
                            controller: _commentController,
                            //inputFormatters: [UpperCaseFormatter()],
                            style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .color),
                            maxLines: 10,
                            keyboardType: TextInputType.multiline,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(32),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(24)),
                          width: ScreenUtil().setWidth(130),
                          height: ScreenUtil().setHeight(28),
                          child: Text('Comment Type',
                              style: TextStyle(
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal,
                                  fontSize: ScreenUtil().setHeight(16),
                                  fontFamily: 'Roboto',
                                  letterSpacing: ScreenUtil().setWidth(0.5))),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: ListTile(
                            leading: Container(
                              child: Checkbox(
                                value: checkBoxValue,
                                onChanged: (value) {
                                  setState(() {
                                    checkBoxValue = value;
                                  });
                                },
                              ),
                            ),
                            title: Text(
                              'Permanent',
                              style: TextStyle(
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal,
                                  fontSize: ScreenUtil().setHeight(16),
                                  fontFamily: 'Roboto',
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  color:
                                      // ignore: deprecated_member_use
                                      Theme.of(context).textTheme.body1.color),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )),
            ),
          )
        ],
      );
    }

    return Scaffold(
        appBar: MainAppBar(),
        drawer: AppDrawer(),
        key: globalKey,
        body: Form(
          key: _formKey,
          autovalidate: true,
          child: FormUI(),
        ));
  }
}
