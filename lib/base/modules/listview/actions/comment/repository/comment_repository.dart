import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/actions/comment/dao/comment_dao.dart';
import 'package:mbase/base/modules/listview/actions/comment/model/comment_model.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';

class CommentRepository {
  CommentDAO commentDAO = new CommentDAO();
  NotificationRepository notificationRepository=NotificationRepository();

  Future<bool> addComment(List<Equipment> selectedEquipments, String comment, bool permanent) async {

    selectedEquipments.forEach((railcar) async{
      Comment commentModel = new Comment();
      commentModel.equipmentID = railcar.equipmentInitial +" "+ railcar.equipmentNumber;
      commentModel.comment = comment;
      commentModel.commentType = permanent ? "Permanent" : "FacilityVisit";
      commentModel.assetMasterId = railcar.assetMasterId;
      commentModel.facilityVisitId = permanent ? null : railcar.facilityVisitId;
      commentModel.facilityId = env.userProfile.facilityId;
      commentModel.createdDt = DateTime.now().toUtc();
      commentModel.updatedDt = DateTime.now().toUtc();
      commentModel.user = env.userProfile.userName;
      commentModel.scyPK = null;
      commentModel.source = APP_CONST.MOBILITY;
      commentModel.operationType = APP_CONST.ADD_COMMENT_OPERATION;
      commentModel.transactionStatus = APP_CONST.TRANSACTION_STATUS_PENDING;
      commentModel.permissionID = env.userProfile.id;
      commentDAO.add(commentModel);
      await notificationRepository.addNotification(commentModel, APP_CONST.ADD_COMMENT_OPERATION);
    });
  }

}
