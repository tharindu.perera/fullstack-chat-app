import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

RailcarBlock railcarBlockFromMap(String str) =>
    RailcarBlock.fromMap(json.decode(str));

String railcarBlockMap(RailcarBlock data) => json.encode(data.toMap());

class RailcarBlock {
  RailcarBlock(
      {this.clientId,
      this.railcarBlockToCodeName,
      this.createdDt,
      this.updatedDt,
      this.id});

  String clientId;
  String railcarBlockToCodeName;
  String createdDt;
  String updatedDt;
  String id;

  DocumentReference reference;

  RailcarBlock.fromMap(Map<String, dynamic> map, {this.reference})
      : clientId = map['client_id'],
        railcarBlockToCodeName = map['railcar_block_to_code_name'],
        id = map['id'];

  RailcarBlock.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data(), reference: snapshot.reference);

  Map<String, dynamic> toMap() => {
        "client_id": clientId == null ? null : clientId,
        "railcar_block_to_code_name":
            railcarBlockToCodeName == null ? null : railcarBlockToCodeName,
        "created_dt": createdDt == null ? null : createdDt,
        "updated_dt": updatedDt == null ? null : updatedDt,
        "id": id == null ? null : id,
      };

  @override
  String toString() {
    return 'RailcarBlock{clientId: $clientId, railcarBlockToCodeName: $railcarBlockToCodeName, createdDt: $createdDt, updatedDt: $updatedDt, id: $id, reference: $reference}';
  }


}
