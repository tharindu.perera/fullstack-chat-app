import 'dart:convert';

import 'package:mbase/base/core/util/date_time_util.dart';

AssignedBlock assignedBlockFromMap(String str) => AssignedBlock.fromMap(json.decode(str));

String assignedBlockToMap(AssignedBlock data) => json.encode(data.toMap());

class AssignedBlock {
  AssignedBlock({this.equipmentId,this.asset_master_id,this.blockName, this.railcarBlockId, this.facilityId, this.createdDt, this.source, this.transactionStatus, this.operationType, this.permission_id, this.id, this.user});

  String user;
  String equipmentId;
  String railcarBlockId;
  String facilityId;
  DateTime createdDt;
  String source;
  String transactionStatus;
  String operationType;
  String permission_id;
  String id;
  String blockName;
  String equipmentInitAndNum;
  String asset_master_id;

  factory AssignedBlock.fromMap(Map<String, dynamic> json) => AssignedBlock(
        id: json["id"] == null ? "id" : json["id"],
        asset_master_id: json["asset_master_id"] == null ? null : json["asset_master_id"],
        equipmentId: json["equipment_id"] == null ? "equipment_id" : json["equipment_id"],
        railcarBlockId: json["block_to_id"] == null ? "railcar_block_id" : json["block_to_id"],
        blockName: json["block_to_name"] == null ? "block_to_name" : json["block_to_name"],
        facilityId: json["facility_id"] == null ? "facility_id" : json["facility_id"],
        createdDt: json["created_dt"] == null ? "created_dt" : getLocalDateTimeFromUTCStr(json["created_dt"]),
        source: json["source"] == null ? "source" : json["source"],
        transactionStatus: json["transaction_status"] == null ? "transaction_status" : json["transaction_status"],
        operationType: json["operation_type"] == null ? "operation_type" : json["operation_type"],
        user: json["user"] == null ? "user" : json["user"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "asset_master_id": asset_master_id == null ? null : asset_master_id,
        "equipment_id": equipmentId == null ? null : equipmentId,
        "railcar_block_id": railcarBlockId == null ? null : railcarBlockId,
        "facility_id": facilityId == null ? null : facilityId,
        "created_dt": createdDt == null ? null : getUTCTimeFromLocalDateTime(createdDt).toString(),
        "source": source == null ? null : source,
        "transaction_status": transactionStatus == null ? null : transactionStatus,
        "operation_type": operationType == null ? null : operationType,
        "user": user == null ? null : user,
      };

  @override
  String toString() {
    return 'AssignedBlock{user: $user, equipmentId: $equipmentId, railcarBlockId: $railcarBlockId, facilityId: $facilityId, createdDt: $createdDt, source: $source, transactionStatus: $transactionStatus, operationType: $operationType, permission_id: $permission_id, id: $id, blockName: $blockName, equipmentInitAndNum: $equipmentInitAndNum, asset_master_id: $asset_master_id}';
  }


}
