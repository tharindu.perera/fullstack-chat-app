import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/dao/assign_block_dao.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/assigned_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/railcar_block.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';

class AssignBlockRepository {
  AssignBlockDAO assignBlockDAO = new AssignBlockDAO();
  NotificationRepository notificationRepository = new NotificationRepository();

  Future<void> saveBlock(List<Equipment> selectedCarList, String selectedBlockId,String blockName) async {
    AssignedBlock assignedBlock = new AssignedBlock();
    assignedBlock.equipmentId = selectedCarList[0].activeAssetId;
    assignedBlock.asset_master_id = selectedCarList[0].assetMasterId;
    assignedBlock.railcarBlockId = selectedBlockId;
    assignedBlock.facilityId = env.userProfile.facilityId;
    assignedBlock.transactionStatus = APP_CONST.TRANSACTION_STATUS_PENDING;
    assignedBlock.source = APP_CONST.MOBILITY;
    assignedBlock.operationType = APP_CONST.ASSIGN_BLOCK_OPERATION;
    assignedBlock.permission_id = env.userProfile.id;
    assignedBlock.user = env.userProfile.userName;
    assignedBlock.createdDt = DateTime.now().toUtc();
    assignedBlock.blockName = blockName;
    assignedBlock.equipmentInitAndNum = selectedCarList[0].equipmentInitial+" "+selectedCarList[0].equipmentNumber;
     assignBlockDAO.add(assignedBlock);
     notificationRepository.addNotification(assignedBlock, APP_CONST.ASSIGN_BLOCK_OPERATION);

  }

  Future<List<RailcarBlock>> fetchRailcarBlocks() async {
    return await assignBlockDAO.fetchRailcarBlocks();
  }
}
