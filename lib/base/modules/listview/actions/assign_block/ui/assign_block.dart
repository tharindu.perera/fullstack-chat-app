import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/railcar_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/repository/assign_block_repository.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class AssignBlock extends StatefulWidget {
  List<Equipment> selectedEquipments = List<Equipment>();
  String source;

  AssignBlock(List<Equipment> selectedRow, String source) {
    this.selectedEquipments = selectedRow;
    this.source = source;
  }

  @override
  _AssignBlockState createState() =>
      _AssignBlockState(selectedEquipments, source);
}

class _AssignBlockState extends State<AssignBlock> {
  AssignBlockRepository assignBlockRepository = new AssignBlockRepository();
  List<RailcarBlock> railcarBlockList = [];
  RailcarBlock selectedRadio;

  FToast fToast;
  String selectedBlockToCode;
  bool isBlockSelected = false;
  final globalKey = GlobalKey<ScaffoldState>();
  List<RailcarBlock> railcarBlockToCodeList = new List<RailcarBlock>();
  List<Widget> widgetList = new List<Widget>();
  List<Equipment> selectedEquipments = List<Equipment>();
  String selectedEquipment = "";
  bool selectedTemp = false;
  String source;

  _AssignBlockState(List<Equipment> selectedEquipments, String source) {
    this.selectedEquipments = selectedEquipments;
    this.source = source;
    selectedEquipment = selectedEquipments[0].equipmentInitial +
        " " +
        selectedEquipments[0].equipmentNumber;
  }

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);
    fetchRailcarBlocks();
  }

  fetchRailcarBlocks() async {
    assignBlockRepository.fetchRailcarBlocks().then((value) {
      value.forEach((element) {
        railcarBlockList.add(element);
      });
      setState(() {});
    });
  }

  setSelectedRadio(int value) {
    setState(() {
      selectedRadio = railcarBlockList[value];
      selectedTemp = true;
    });
  }

  _showToast() {
    fToast.showToast(
      child: CustomToast(
        toastColor: Color(0xff7fae1b),
        toastMessage: "Block to code assigned to railcar successfully",
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  clearScannedEquipList() {
    List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"];
    if (scannedEquipList != null) {
//      MemoryData.dataMap.removeWhere((key, value) => key == "scannedEquipList");
    }
  }

  assignBlock() async {
    try {
      String selectedBlockId;
      String blockName;

      if (selectedRadio.railcarBlockToCodeName != null) {
        railcarBlockList.forEach((element) {
          if (element.railcarBlockToCodeName ==
              selectedRadio.railcarBlockToCodeName) {
            selectedBlockId = element.id;
            blockName = element.railcarBlockToCodeName;
          }
        });
      }
      assignBlockRepository
          .saveBlock(selectedEquipments, selectedBlockId, blockName)
          .then((value) {
        // scannedEquipList needs to be cleared inorder to clear the AEI grid after a operation is performed (Only applicable when navigating from AEI actions).
        clearScannedEquipList();
        _showToast();
        Navigator.pop(context);
        if (APP_CONST.AEI_SCAN == source) {
          BlocProvider.of<AEIScanBloc>(context)
              .add(AEIScanFetchDataWithLastSetCriteria());
        } else if (APP_CONST.YARD_VIEW == source) {
          BlocProvider.of<YardViewBloc>(context)
              .add(YardViewFetchDataWithLastSetCriteria());
        } else {
          BlocProvider.of<ListViewBloc>(context)
              .add(ListViewFetchDataWithLastSetCriteria());
        }
      });
    } catch (e) {
      print('Exception : ${e}');
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);

    return Scaffold(
        key: globalKey,
        appBar: MainAppBar(),
        drawer: AppDrawer(),
        body: Form(
          autovalidate: true,
          child: FormUI(),
        ));
  }

  Widget FormUI() {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    return Builder(
      builder: (context) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(15.0),
                child: CardUI(
                    content: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(20),
                                  child: Container(
                                      child: ListTile(
                                    leading: GestureDetector(
                                        onTap: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Icon(
                                          Icons.arrow_back,
                                        )),
                                    title: Text('Assign Block',
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(24,
                                                allowFontScalingSelf: true),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'Roboto',
                                            color: Theme.of(context)
                                                .textTheme
                                                // ignore: deprecated_member_use
                                                .headline
                                                .color)),
                                  ))),
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        child: GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: RichText(
                                        text: TextSpan(
                                            style: Theme.of(context)
                                                .textTheme
                                                // ignore: deprecated_member_use
                                                .body1,
                                            children: <InlineSpan>[
                                              TextSpan(
                                                  text: 'CANCEL',
                                                  style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(
                                                          14,
                                                          allowFontScalingSelf:
                                                              true),
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      letterSpacing:
                                                          ScreenUtil()
                                                              .setWidth(1.25),
                                                      fontFamily: 'Roboto',
                                                      color: Color(0xFF3e8aeb)))
                                            ]),
                                      ),
                                    ))),
                                Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(180),
                                        height: ScreenUtil().setHeight(48),
                                        child: FlatButton(
                                            onPressed: selectedTemp
                                                ? assignBlock
                                                : null,
                                            child: Text('ASSIGN BLOCK',
                                                style: TextStyle(
                                                    fontFamily: 'Roboto',
                                                    fontSize: ScreenUtil()
                                                        .setHeight(14),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(1.25),
                                                    color: Colors.white)),
                                            color: Color(0xFF3e8aeb),
                                            textColor: Colors.white,
                                            disabledColor: Colors.grey,
                                            disabledTextColor: Colors.black,
                                            splashColor: Color(0xFF3e8aeb)))),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                    Divider(
                      thickness: 5.0,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(20),
                              child: Container(
                                  width: 300,
                                  padding: EdgeInsets.symmetric(vertical: 4.0),
                                  alignment: Alignment.center,
                                  child: ListTile(
                                    title: Text(
                                      'Railcar',
                                      style: Theme.of(context)
                                          .textTheme
                                          // ignore: deprecated_member_use
                                          .body2,
                                    ),
                                    subtitle: Text(
                                      selectedEquipment,
                                      style:
                                          Theme.of(context).textTheme.headline5,
                                    ),
                                  )),
                            ),
                          ],
                        ),
                      ],
                    ),
                    // Divider(
                    //   thickness: 2.0,
                    // ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                        width: 300,
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: Text(
                                            'Block To Code',
                                            style: Theme.of(context)
                                                .textTheme
                                                // ignore: deprecated_member_use
                                                .body2,
                                          ),
                                        )),

                                    // Row(
                                    //   children: <Widget>[getRadioWidgets()],
                                    // ),
                                  ],
                                )),
                          ],
                        ),
                      ],
                    ),

                    Wrap(
                      direction: Axis.horizontal,
                      spacing: ScreenUtil().setWidth(10),
                      runSpacing: ScreenUtil().setHeight(10),
                      children: railcarBlockList.map((e) {
                        return Container(
                          width: ScreenUtil().setWidth(200),
                          child: ListTile(
                            leading: Radio(
                              value: railcarBlockList.indexOf(e),
                              groupValue: selectedRadio == null
                                  ? null
                                  : railcarBlockList.indexOf(selectedRadio),
                              activeColor: Color(0xFF3e8aeb),
                              onChanged: (val) {
                                print("Radio onChanged " + val.toString());
                                setSelectedRadio(val);
                              },
                            ),
                            title: Text(
                              e.railcarBlockToCodeName,
                              style: Theme.of(context).textTheme.bodyText2,
                            ),
                          ),
                        );
                      }).toList(),
                    ),

                    // Padding(
                    //   padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                    //   child: RadioButtonGroupFieldBlocBuilder<String>(
                    //     selectFieldBloc: formBloc.railcarBlockField,
                    //     itemBuilder: (context, value) => value,
                    //     labelStyle: TextStyle(
                    //       color:
                    //       _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Colors.white:
                    //       _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)? Colors.black:null,
                    //     ),
                    //     decoration: InputDecoration(
                    //       border: InputBorder.none,
                    //       prefixIcon: SizedBox(),
                    //     ),
                    //   ),
                    // ),
                  ],
                )),
              ),
            )
          ],
        );
      },
    );
  }
}
