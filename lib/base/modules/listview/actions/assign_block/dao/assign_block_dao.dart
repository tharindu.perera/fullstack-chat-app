import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/assigned_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/railcar_block.dart';
import 'package:mbase/env.dart';
import 'package:uuid/uuid.dart';

class AssignBlockDAO extends BaseDao {
  AssignBlockDAO() : super("assigned_block");

  Future<void> add(AssignedBlock assignedBlock) async {
    assignedBlock.id = Uuid().v1();
     collection.doc(assignedBlock.id).set(assignedBlock.toMap());

  }

  Future<List<RailcarBlock>> fetchRailcarBlocks() async {
    FirebaseFirestore firebaseFirestore = DatabaseProvider.firestore;
    CollectionReference railcarBlockCollection = firebaseFirestore.collection("railcar_block");
    railcarBlockCollection.orderBy("railcar_block_to_code_name", descending: true);

    return (await railcarBlockCollection.where("client_id", isEqualTo: env.userProfile.clientId).where("facility_id",isEqualTo:env. userProfile.facilityId).get()).docs.map((snapshot) {
      return RailcarBlock.fromSnapshot(snapshot);
    }).toList();
  }

  Future<List<AssignedBlock>> fetchRailcarBlocksByAssetMaterID(String id) async {
    print("Fetching Railcar Blocks By Railcar assetMasterId ......$id     ${env. userProfile.facilityId}");

    FirebaseFirestore firebaseFirestore = DatabaseProvider.firestore;
    CollectionReference railcarBlockCollection = firebaseFirestore.collection("assigned_block");

    return (await railcarBlockCollection
//        .where("client_id", isEqualTo: env.userProfile.clientId)
        .where("facility_id",isEqualTo:env. userProfile.facilityId)
        .where("asset_master_id",isEqualTo:id)
         .orderBy("created_dt",descending: true)
        .get()).docs.map((snapshot) {
      return AssignedBlock.fromMap(snapshot.data());
    }).toList();
  }
}
