import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class MoveRailCarDateTimeBloc extends FormBloc<String, String> {


  final dateTime = InputFieldBloc<DateTime, Object>(name: 'inboundDateTime', toJson: (value) => value.toUtc(), validators: [FieldBlocValidators.required],initialValue: DateTime.now());

  final sequenceOfCar = SelectFieldBloc(name: 'Sequence', items: ['Front', 'Back'], validators: [FieldBlocValidators.required]);

  MoveRailCarDateTimeBloc():super(isLoading:false) {
    addFieldBlocs(
      fieldBlocs: [
        dateTime,
        sequenceOfCar,
      ],
    );
  }

  var _throwException = true;

  @override
  void onLoading() async {
    try {
      await Future<void>.delayed(Duration(milliseconds: 1500));

      if (_throwException) {
        // Simulate network error
        throw Exception('Network request failed. Please try again later.');
      }


      emitLoaded();
    } catch (e) {
      _throwException = false;

      emitLoadFailed();
    }
  }

  @override
  void onSubmitting() async {
    try {
      await Future<void>.delayed(Duration(milliseconds: 500));

      emitSuccess(canSubmitAgain: true);
    } catch (e) {
      emitFailure();
    }
  }
}