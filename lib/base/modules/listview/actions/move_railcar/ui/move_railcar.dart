import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/repository/assign_switchlist_repository.dart';
import 'package:mbase/base/modules/listview/actions/move_railcar/ui/move_railcar_datetime_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';
import 'package:provider/provider.dart';

class MoveRailcar extends StatefulWidget {
    MoveRailcar(List<Equipment> selectedEquipments, String source,{context}) {
    this.selectedEquipments = selectedEquipments;
    this.source = source;
    this.aeiBloc = context;
    }
  List<Equipment> selectedEquipments = List<Equipment>();
  String source;
    AEIScanBloc aeiBloc;
    @override
  _MoveRailcarState createState() => _MoveRailcarState(selectedEquipments, source);
}

class _MoveRailcarState extends State<MoveRailcar> {
  List<Equipment> selectedEquipments = List<Equipment>();
  List<YardModel> yardList = [];
  List<TrackModel> trackList = [];
  List<TrackModel> filteredTracksList = [];
  String selectedyardName = null;
  String selectedYardID;
  String yardHintText = "";
  String source;

  String selectedTrackID;
  String selectedTrackName = null;
  bool trackChanged = false;
  String trackHintText = "";
  bool autoValidate = false;

  String selectedSpot;

  String selectedSpotId;

  List<SpotModel> spotList = [];
  List<SpotModel> filteredSpotsList = [];

  String placed_date = null;

  FToast fToast;

  int _sortColumnIndex = 0;
  bool _sortAscending = true;

  final dateFormat = new DateFormat('MM/dd/yyyy HH:mm');
  var formBloc;
  Map<String, List<String>> verifiedMoveRailCarList;

  AssignSwitchListRepo assignSwitchListRepo = AssignSwitchListRepo();


  _MoveRailcarState(List<Equipment> selectedEquipmentsTemp, String source) {
    this.source = source;
    this.placed_date = null;
    this.selectedEquipments = [];
    selectedEquipmentsTemp.forEach((element) {
      placed_date = element.placedDate.toString();
      selectedEquipments.add(Equipment.fromMap(element.toMap())..move.to_spot_id=null);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    yardList = MemoryData.inMemoryYardMap.values.toList();
    trackList = MemoryData.inMemoryTrackMap.values.toList();
    spotList = MemoryData.inMemorySpotMap.values.toList();
    fToast = FToast(context);
  }

  _showToast({String toastMessage, Color toastColor, IconData icon}) {
    fToast.showToast(
      child: CustomToast(
        toastColor: toastColor,
        toastMessage: toastMessage,
        icon: icon,
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  onYardChange(String value) {
    selectedTrackID = null;
    filteredTracksList = [];
    selectedSpotId = null;
    filteredSpotsList = [];

    // Equipment railcar;
    // railcar?.move?.to_track_id = null;
    // railcar?.move?.to_spot_id = null;
    // filteredTracksList = [];
    // filteredSpotsList = [];

    selectedYardID =
        yardList.singleWhere((element) => element.yardName == value).id;

    var filteredTracks =
        trackList.where((element) => element.yardId == selectedYardID).toList();
    filteredTracksList = [];
    setState(() {
      selectedyardName = value;
      filteredTracksList = filteredTracks;
      selectedTrackName = null;
    });
  }

  onTrackChange(String value) async {
    selectedSpotId = null;
    filteredSpotsList = [];

    Loader.show(context);
    selectedTrackID =
        trackList.singleWhere((element) => element.trackName == value).id;

    var filteredSpots = spotList
        .where((element) => element.trackId == selectedTrackID)
        .toList();

    List <SpotModel> availableSpots = filteredSpots.length > 0 ? await checkForAvailableSpots(selectedYardId:selectedYardID
        ,selectedTrackId: selectedTrackID,spotsAccordingToYardTrack:filteredSpots ):[];

    setState(() {
      selectedTrackName = value;
      filteredSpotsList =  availableSpots;
      ;
    });
    Loader.hide();

  }

  onSpotChange(String value, Equipment railcar) {

    //railcar.move.to_spot_id = spotList.singleWhere((element) => element.spotName == value && selectedTrackID == element.trackId).id;
    selectedSpotId =   spotList.singleWhere((element) => element.spotName == value && selectedTrackID == element.trackId).id;
    railcar.move.to_yard_id = selectedYardID;
    railcar.move.to_track_id = selectedTrackID;
    railcar.move.to_spot_id = selectedSpotId;
    setState(() {

    });
  }

  checkForAvailableSpots(
      {String selectedYardId,
        String selectedTrackId,
        List<SpotModel> spotsAccordingToYardTrack}) async{
    return  await assignSwitchListRepo.getEmptySpots(selectedYardId: selectedYardId, selectedTrackId:selectedTrackId,
        spotsAccordingToYardTrack: spotsAccordingToYardTrack
    );

  }

  _validateFields(var moveCarList, var move_direction) {
    bool valid = true;
    if (selectedyardName == null) {
      valid = false;
    }
    if (selectedTrackName == null) {
      valid = false;
    }

    moveCarList.forEach((element) {
      if (element.move.to_spot_id == null && move_direction.value == null) {
        valid = false;
      }
    });

    if (filteredSpotsList.isEmpty && move_direction.value == null) {
      formBloc.submit();
      valid = false;
    }
    return valid;
  }

  _confirmMove() async {
   // CustomSpinner.show(context);
    Loader.show(context);
    EquipmentDAO equipmentDAO = EquipmentDAO();


    InputFieldBloc<DateTime, Object> railcar_move_date = formBloc.dateTime;
    SelectFieldBloc<String, dynamic> move_direction = formBloc.sequenceOfCar;
    List <String> railcarsWithIncorrectMoveDate = [];

    var moveCarList = selectedEquipments.map((equipment) {
      equipment.transactionStatus = APP_CONST.TRANSACTION_STATUS_PENDING;
      equipment.permission_id = env.userProfile.id;
      equipment.facilityId = env.userProfile.facilityId;
      equipment.source = APP_CONST.MOBILITY;
      equipment.operationType = APP_CONST.MOVE_RAILCAR;
      equipment.user = env.userProfile.userName;
      equipment.updatedDt = DateTime.now();
      equipment.parent_id = equipment?.parent_id ?? equipment.id;
      equipment.immediateParentId = equipment.id;
      equipment.move.move_direction = move_direction.value;
      equipment.move.railcar_move_date = railcar_move_date.value?.toIso8601String();

      equipment.move.from_yard_id = equipment.yardId;
      equipment.move.from_track_id = equipment.trackId;
      equipment.move.from_spot_id = equipment.spotId;

      equipment.move.to_yard_id = selectedYardID;
      equipment.move.to_track_id = selectedTrackID;
      // equipment.move.to_spot_id = selectedSpotId ;

      equipment.yardId =  selectedYardID;
      equipment.trackId = selectedTrackID;
      equipment.spotId = equipment.move.to_spot_id ;
      // equipment.yardId = equipment.move.to_yard_id;
      // equipment.trackId = equipment.move.to_track_id;
      // equipment.spotId = equipment.move.to_spot_id;
      return equipment;
    }).toList();

    if (!_validateFields(moveCarList, move_direction)) {
      setState(() {
        autoValidate = true;
      });
      Loader.hide();
      return;
    }
    print('move car list -----> ${moveCarList}');

    if (selectedyardName == null) {
      Loader.hide();
      _showToast(
        toastColor: Colors.red,
        icon: Icons.new_releases,
        toastMessage: 'Yard selection cannot be empty',
      );
    } else if (selectedTrackName == null) {
      Loader.hide();

      _showToast(
        toastColor: Colors.red,
        icon: Icons.new_releases,
        toastMessage: 'Track selection cannot be empty',
      );
    } else if (railcar_move_date.value == null) {
      Loader.hide();

      _showToast(
        toastColor: Colors.red,
        icon: Icons.new_releases,
        toastMessage: 'Date & Time cannot be empty',
      );
    } else if (filteredSpotsList.isEmpty && move_direction.value == null) {
      Loader.hide();
      _showToast(
        toastColor: Colors.red,
        icon: Icons.new_releases,
        toastMessage: 'Please select an option',
      );
    } else if (railcar_move_date.value != null) {
      bool isAllRailcarsMoveDateValidated = true;

      moveCarList.forEach((railcar) {
        if (DateTime.parse(railcar.move.railcar_move_date)
            .isBefore(DateTime.parse(railcar.placedDate.toString()))) {
          isAllRailcarsMoveDateValidated = false;
          String railcarName = railcar.equipmentInitial + railcar.equipmentNumber;
          railcarsWithIncorrectMoveDate.add(railcarName);
          Loader.hide();

        }
      });
      if(!isAllRailcarsMoveDateValidated){
        _showToast(
          toastColor: Colors.red,
          icon: Icons.new_releases,
          toastMessage:
          'Move date should be greater than the inbound date of railcar :${railcarsWithIncorrectMoveDate} ',
        );
      }

      if (isAllRailcarsMoveDateValidated) {
        bool isAllOk = true;
        moveCarList.forEach((element) {
          if (element.move.to_spot_id == null && move_direction.value == null) {
            isAllOk = false;
            Loader.hide();

            _showToast(
                toastColor: Colors.red,
                toastMessage: 'Please select a spot to move railcar(s)',
                icon: Icons.new_releases);
          }
        });
        if (isAllOk) {
          List<String> selectedSpotId = [];
          List<Equipment> usedSpots = null;
          moveCarList.forEach((element) {
            if (element.move.to_spot_id != null) {
              String spot_id =
                  MemoryData.inMemorySpotMap[element.move.to_spot_id].id;
              selectedSpotId.add(spot_id);
            }
          });


          if (selectedSpotId.isNotEmpty) {
             if(selectedSpotId.toSet().toSet().length != selectedSpotId.length){
               Loader.hide();
               _showToast(
                   toastColor: Colors.red,
                   icon: Icons.new_releases,
                   toastMessage: "Same spot cannot be assigned to multiple railcars ");
             }

             else {
               usedSpots = await equipmentDAO.validateSpotSelection(
                   yardId: selectedYardID,
                   trackId: selectedTrackID,
                   spots: selectedSpotId);
             }
          }

          if (selectedSpotId.toSet().toSet().length != selectedSpotId.length) {

          } else if (usedSpots != null && usedSpots.isNotEmpty && selectedSpotId.isNotEmpty) {
            Loader.hide();
            String toastMsg = "";
            String usedSpot = "";
            usedSpots.forEach((element) {
              usedSpot = usedSpot + MemoryData.inMemorySpotMap[element.spotId].spotName + ", ";
            });
            usedSpot = usedSpot.substring(0,usedSpot.length-2);
            toastMsg = "Spots: ${usedSpot} has already been assigned to railcars";
            _showToast(
                toastColor: Colors.red,
                icon: Icons.new_releases,
                toastMessage: toastMsg);
          } else {
            await moveCarList.forEach((element) async {
               equipmentDAO.add(element);
               NotificationRepository()
                  .addNotification(element, APP_CONST.MOVE_RAILCAR);
            });

            print(">>>>>374");
            if(source==APP_CONST.AEI_SCAN){
         List<Equipment> scanndelIst=    MemoryData.dataMap["scannedEquipList"];
         moveCarList.forEach((move) {
           var firstWhere = scanndelIst.firstWhere((element) => element.equipmentInitial==move.equipmentInitial && move.equipmentNumber==element.equipmentNumber);
           scanndelIst.remove( firstWhere);
           move.sequence=firstWhere.sequence;
           move.isSelected=true;
           move.isDataVerified=true;
           scanndelIst.add(move);
         });
               MemoryData.dataMap["selectedEquipList"]=moveCarList;
                 this.widget.aeiBloc.add(AEIScanFetchDataWithLastSetCriteria());
            }
            Loader.hide();

            // scannedEquipList needs to be cleared inorder to clear the AEI grid after a operation is performed (Only applicable when navigating from AEI actions).

            _showToast(
                toastMessage: "Move Railcar operation is successful",
                icon: Icons.check,
                toastColor: Color(0xff7fae1b));
            redirectBack();
          }
        }
      }
    } else {
      await moveCarList.forEach((element) async {
         equipmentDAO.add(element);
         NotificationRepository()
            .addNotification(element, APP_CONST.MOVE_RAILCAR);
      });
      Loader.hide();
       // scannedEquipList needs to be cleared inorder to clear the AEI grid after a operation is performed (Only applicable when navigating from AEI actions).
      if(source==APP_CONST.AEI_SCAN){
        List<Equipment> scanndelIst=    MemoryData.dataMap["scannedEquipList"];
        moveCarList.forEach((move) {
          var firstWhere = scanndelIst.firstWhere((element) => element.equipmentInitial==move.equipmentInitial && move.equipmentNumber==element.equipmentNumber);
          scanndelIst.remove( firstWhere);
          move.sequence=firstWhere.sequence;
          move.isSelected=true;
          scanndelIst.add(move);
        });
        MemoryData.dataMap["selectedEquipList"]=moveCarList;
        this.widget.aeiBloc.add(AEIScanFetchDataWithLastSetCriteria());
      }
      _showToast(
          toastMessage: "Move has been completed successfully",
          icon: Icons.check,
          toastColor: Color(0xff7fae1b));

      redirectBack();

    }
  }

  popUpDialog() async {
    InputFieldBloc<DateTime, Object> railcarMovdDate = formBloc.dateTime;
    SelectFieldBloc<String, dynamic> selectedSequence = formBloc.sequenceOfCar;

    if (selectedYardID != null ||
        selectedTrackID != null ||
        railcarMovdDate.value != null ||
        selectedSequence.value != null) {
      final action = await CustomDialog.cstmDialog(
          context, "add_railcar", "Unsaved Changes", "");
      if (action[0] == DialogAction.yes) {
        redirectBack();
      }
    } else {
      redirectBack();
    }
  }

  onSortColumn(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        selectedEquipments.sort((a, b) => (a.equipmentInitial + a.equipmentNumber).compareTo(b.equipmentInitial + b.equipmentNumber));
        setState(() {});
      } else {
        selectedEquipments.sort((a, b) => (b.equipmentInitial + b.equipmentNumber).compareTo(a.equipmentInitial + a.equipmentNumber));
        setState(() {});
      }
    }

    if (columnIndex == 1) {
      if (ascending) {
        selectedEquipments.sort((a, b) => a.compartmentList.first.currentAmount.compareTo(b.compartmentList.first.currentAmount));
        setState(() {
        });
      } else {
        selectedEquipments.sort((a, b) => b.compartmentList.first.currentAmount.compareTo(a.compartmentList.first.currentAmount));
        setState(() {
        });
      }
    }

    if (columnIndex == 2) {
      if (ascending) {
        selectedEquipments.sort((a, b) => a.compartmentList.first.productName.compareTo(b.compartmentList.first.productName));
        setState(() {
        });
      } else {
        selectedEquipments.sort((a, b) => b.compartmentList.first.productName.compareTo(a.compartmentList.first.productName));
        setState(() {
        });
      }
    }

    if (columnIndex == 3) {
      if (ascending) {
        selectedEquipments.sort((a, b) => a.yardId.compareTo(b.yardId));
        setState(() {
        });
      } else {
        selectedEquipments.sort((a, b) => b.yardId.compareTo(a.yardId));
        setState(() {
        });
      }
    }

    if (columnIndex == 4) {
      if (ascending) {
        selectedEquipments.sort((a, b) => a.trackId.compareTo(b.trackId));
        setState(() {
        });
      } else {
        selectedEquipments.sort((a, b) => b.trackId.compareTo(a.trackId));
        setState(() {
        });
      }
    }

    if (columnIndex == 5) {
      if (ascending) {
        selectedEquipments.sort((a, b) => a.sequenceType.compareTo(b.sequenceType));
        setState(() {
        });
      } else {
        selectedEquipments.sort((a, b) => b.sequenceType.compareTo(a.sequenceType));
        setState(() {
        });
      }
    }

  }

  redirectBack() {
    Navigator.pop(context);
    if (APP_CONST.YARD_VIEW == source) {
      BlocProvider.of<YardViewBloc>(context)
          .add(YardViewFetchDataWithLastSetCriteria());
    }
  }


  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    ScreenUtil.init(context, width: 1024, height: 768);

    return Scaffold(
        appBar: MainAppBar(),
        drawer: AppDrawer(),
        body: BlocProvider(
          create: (context) => MoveRailCarDateTimeBloc(),
          child: Builder(
            builder: (context) {
              formBloc = context.bloc<MoveRailCarDateTimeBloc>();
              return FormBlocListener<MoveRailCarDateTimeBloc, String, String>(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: CardUI(
                        content: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(20),
                                            child: Container(
                                                child: ListTile(
                                              leading: GestureDetector(
                                                  onTap: popUpDialog,
                                                  child: Icon(
                                                    Icons.arrow_back,
                                                  )),
                                              title: Text(
                                                  'Move Railcars (${selectedEquipments.length})',
                                                  style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(
                                                          24,
                                                          allowFontScalingSelf:
                                                              true),
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      fontFamily: 'Roboto',
                                                      color: Theme.of(context).textTheme.bodyText1.color)),
                                            ))),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Padding(
                                              padding: EdgeInsets.all(20),
                                              child: Container(
                                                  child: GestureDetector(
                                                onTap: popUpDialog,
                                                child: RichText(
                                                  text: TextSpan(
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1,
                                                      children: <InlineSpan>[
                                                        TextSpan(
                                                            text: 'CANCEL',
                                                            style: TextStyle(
                                                                fontSize: ScreenUtil()
                                                                    .setSp(14,
                                                                        allowFontScalingSelf:
                                                                            true),
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                letterSpacing:
                                                                    ScreenUtil()
                                                                        .setWidth(
                                                                            1.25),
                                                                fontFamily:
                                                                    'Roboto',
                                                                color: Color(
                                                                    0xFF3e8aeb)))
                                                      ]),
                                                ),
                                              ))),
                                          Padding(
                                              padding: EdgeInsets.only(
                                                  left: 0,
                                                  top: 14,
                                                  right: 24,
                                                  bottom: 14),
                                              child: Container(
                                                width:
                                                    ScreenUtil().setWidth(180),
                                                height:
                                                    ScreenUtil().setHeight(48),
                                                child: FlatButton(
                                                    onPressed: _confirmMove,
                                                    child: Text(
                                                      'CONFIRM MOVE',
                                                      style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: ScreenUtil()
                                                              .setHeight(14),
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          letterSpacing:
                                                              ScreenUtil()
                                                                  .setWidth(
                                                                      1.25),
                                                          color: Colors.white),
                                                    ),
                                                    color: Color(0xFF508be4),
                                                    textColor: Colors.white,
                                                    disabledColor: Colors.grey,
                                                    disabledTextColor:
                                                        Colors.black,
                                                    splashColor:
                                                        Color(0xFF508be4)),
                                              )),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: ScreenUtil().setHeight(14)),
                                child: Divider(
                                  thickness: 2.0,
                                  // color: Color.fromRGBO(0, 0, 0, 1),
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      CustomDropdown(
                                          hintText: "Yard",
                                          error: (this.autoValidate && this.selectedyardName == null)
                                              ? true : false,
                                          itemList: yardList.isNotEmpty == true
                                              ? yardList
                                                  .map((e) => e.yardName)
                                                  .toList()
                                              : const [],
                                          SelectedValue:
                                              selectedyardName ?? null,
                                          Ontap: (str) => onYardChange(str)),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      CustomDropdown(
                                          hintText: "Track",
                                          error: (this.autoValidate && this.selectedTrackName == null)
                                              ? true : false,
                                          itemList:
                                              filteredTracksList.isNotEmpty ==
                                                      true
                                                  ? filteredTracksList
                                                      .map((e) => e.trackName)
                                                      .toList()
                                                  : const [],
                                          SelectedValue:
                                              selectedTrackName ?? null,
                                          Ontap: (str) => onTrackChange(str)),
                                    ],
                                  ),
                                  Container(
                                    width: 230,
                                    child: DateTimeFieldBlocBuilder(
                                      style: TextStyle(
                                        color: _themeChanger
                                                    .getTheme()
                                                    .primaryColor ==
                                                Color(0xff182e42)
                                            ? Color(0xfff5f5f5)
                                            : _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xfff5f5f5)
                                                ? Color(0xff172636)
                                                : null,
                                      ),
                                      format: dateFormat,
                                      dateTimeFieldBloc: formBloc.dateTime,
                                      canSelectTime: true,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(1900),
                                      lastDate: DateTime(2100),
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(
                                          Icons.timer,
                                          color: _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xff182e42)
                                              ? Color(0xfff5f5f5)
                                              : _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xfff5f5f5)
                                                  ? Color(0xff172636)
                                                  : null,
                                        ),
                                        contentPadding:
                                            EdgeInsets.symmetric(vertical: 15),
                                        errorBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(1)),
                                            borderSide: BorderSide(
                                                width: 1, color: Colors.red)),
                                        focusedErrorBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(1)),
                                            borderSide: BorderSide(
                                                width: 1, color: Colors.red)),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(1.0),
                                          borderSide: BorderSide(
                                              width: 1, color: Colors.black38),
                                        ),
                                        enabledBorder: const OutlineInputBorder(
                                          borderSide: const BorderSide(
                                              color: Colors.black38, width: 1),
                                        ),
                                        filled: true,
                                        fillColor: _themeChanger
                                                    .getTheme()
                                                    .primaryColor ==
                                                Color(0xff182e42)
                                            ? Color(0xff172636)
                                            : _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xfff5f5f5)
                                                ? Color(0xfff5f5f5)
                                                : null,
                                        hintText: 'Select Date Time',
                                        hintStyle: TextStyle(
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.normal,
                                          fontStyle: FontStyle.normal,
                                          letterSpacing:
                                              ScreenUtil().setWidth(0.25),
                                          color: _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xff182e42)
                                              ? Color(0xfff5f5f5)
                                              : _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xfff5f5f5)
                                                  ? Color(0xff172636)
                                                  : null,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: ScreenUtil().setHeight(10),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                // mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  filteredSpotsList.isEmpty
                                      ? Padding(
                                          padding: const EdgeInsets.only(
                                              left: 100,
                                              right: 0,
                                              top: 0,
                                              bottom: 0),
                                          child: Container(
                                            child:
                                                RadioButtonGroupFieldBlocBuilder<
                                                    String>(
                                              selectFieldBloc:
                                                  formBloc.sequenceOfCar,
                                              itemBuilder: (context, value) =>
                                                  value,
                                              labelStyle: TextStyle(
                                                color: _themeChanger
                                                            .getTheme()
                                                            .primaryColor ==
                                                        Color(0xff182e42)
                                                    ? Colors.white
                                                    : _themeChanger
                                                                .getTheme()
                                                                .primaryColor ==
                                                            Color(0xfff5f5f5)
                                                        ? Colors.black
                                                        : null,
                                              ),
                                              decoration: InputDecoration(
                                                labelStyle: TextStyle(
                                                  color: _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xff182e42)
                                                      ? Colors.white
                                                      : _themeChanger
                                                                  .getTheme()
                                                                  .primaryColor ==
                                                              Color(0xfff5f5f5)
                                                          ? Colors.black
                                                          : null,
                                                    fontSize: ScreenUtil().setSp(20, allowFontScalingSelf: true),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.normal,
                                                    fontFamily: 'Roboto',
                                                    height: 0,
                                                    decorationThickness: 0),

                                                errorBorder: InputBorder.none,
                                                border: InputBorder.none,                                                labelText:
                                                    'Sequence of Railcar',
                                                contentPadding:
                                                    EdgeInsets.symmetric(
                                                        vertical: 15,
                                                        horizontal: 30),
                                                prefixIcon: SizedBox(),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Container(),
                                ],
                              ),
                              SizedBox(
                                height: ScreenUtil().setHeight(10),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 15, right: 15),
                                child: CustomDataTable(
                                  dataRowHeight: ScreenUtil().setHeight(85.0),
                                  rowColor: _themeChanger
                                              .getTheme()
                                              .primaryColor ==
                                          Color(0xff182e42)
                                      ? Color(0xff1d2e40)
                                      : _themeChanger.getTheme().primaryColor ==
                                              Color(0xfff5f5f5)
                                          ? Color(0xffffffff)
                                          : null,
                                  columnColor: Color(0xff274060),
                                  checkboxBorderColor: Colors.white,
                                  sortArrowColor: Colors.white,
                                  sortColumnIndex: _sortColumnIndex,
                                  sortAscending: _sortAscending,
                                  columns: [
                                    CustomDataColumn(
                                        label: Text('RAILCAR',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'Roboto',
                                              fontSize:
                                                  ScreenUtil().setHeight(14),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing:
                                                  ScreenUtil().setWidth(1.43),
                                            )),
                                        onSort: (columnIndex, ascending) {
                                          setState(() {
                                            _sortColumnIndex = columnIndex;
                                            _sortAscending = !_sortAscending;
                                          });
                                          onSortColumn(columnIndex, ascending);
                                          //  listData = [];
                                        },
                                        numeric: false,
                                        tooltip: 'RAILCAR'),
                                    CustomDataColumn(
                                        label: Text('L/E',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'Roboto',
                                              fontSize:
                                                  ScreenUtil().setHeight(14),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing:
                                                  ScreenUtil().setWidth(1.43),
                                            )),
                                        onSort: (columnIndex, ascending) {
                                          setState(() {
                                            _sortColumnIndex = columnIndex;
                                            _sortAscending = !_sortAscending;
                                          });
                                          onSortColumn(columnIndex, ascending);
                                          //  listData = [];
                                        },
                                        numeric: false,
                                        tooltip: 'L/E'),
                                    CustomDataColumn(
                                        label: Text('PRODUCT',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'Roboto',
                                              fontSize:
                                                  ScreenUtil().setHeight(14),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing:
                                                  ScreenUtil().setWidth(1.43),
                                            )),
                                        onSort: (columnIndex, ascending) {
                                          setState(() {
                                            _sortColumnIndex = columnIndex;
                                            _sortAscending = !_sortAscending;
                                          });
                                          onSortColumn(columnIndex, ascending);
                                          //  listData = [];
                                        },
                                        numeric: false,
                                        tooltip: 'PRODUCT'),
                                    CustomDataColumn(
                                        label: Text(
                                          'FROM YARD',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: 'Roboto',
                                            fontSize:
                                                ScreenUtil().setHeight(13),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing:
                                                ScreenUtil().setWidth(1.43),
                                          ),
                                        ),
                                        onSort: (columnIndex, ascending) {
                                          setState(() {
                                            _sortColumnIndex = columnIndex;
                                            _sortAscending = !_sortAscending;
                                          });
                                          onSortColumn(columnIndex, ascending);
                                          //  listData = [];
                                        },
                                        numeric: false,
                                        tooltip: 'FROM YARD'),
                                    CustomDataColumn(
                                        label: Text('FROM TRACK',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'Roboto',
                                              fontSize:
                                                  ScreenUtil().setHeight(14),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing:
                                                  ScreenUtil().setWidth(1.43),
                                            )),
                                        onSort: (columnIndex, ascending) {
                                          setState(() {
                                            _sortColumnIndex = columnIndex;
                                            _sortAscending = !_sortAscending;
                                          });
                                          onSortColumn(columnIndex, ascending);
                                          //  listData = [];
                                        },
                                        numeric: false,
                                        tooltip: 'FROM TRACK'),
                                    CustomDataColumn(
                                        label: Text('SEQ',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'Roboto',
                                              fontSize:
                                                  ScreenUtil().setHeight(14),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing:
                                                  ScreenUtil().setWidth(1.43),
                                            )),
                                        onSort: (columnIndex, ascending) {
                                          setState(() {
                                            _sortColumnIndex = columnIndex;
                                            _sortAscending = !_sortAscending;
                                          });
                                          onSortColumn(columnIndex, ascending);
                                          //  listData = [];
                                        },
                                        numeric: false,
                                        tooltip: 'SEQ'),
                                    CustomDataColumn(
                                        label: Text('SPOT',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'Roboto',
                                              fontSize:
                                                  ScreenUtil().setHeight(14),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing:
                                                  ScreenUtil().setWidth(1.43),
                                            )),
                                        numeric: false,
                                        tooltip: 'SPOT'),
                                  ],
                                  rows: selectedEquipments?.map((railcar) {
                                        return CustomDataRow(cells: [
                                          CustomDataCell(Text(
                                              railcar.equipmentInitial +
                                                  " " +
                                                  railcar.equipmentNumber,
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color,
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(16),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                              ))),
                                          CustomDataCell(Text(
                                              railcar.compartmentList
                                                          .isNotEmpty &&
                                                      double.parse(railcar
                                                                  .compartmentList[
                                                                      0]
                                                                  .currentAmount ??
                                                              "0") >
                                                          0
                                                  ? "L"
                                                  : 'E',
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color,
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(16),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                              ))),
                                          CustomDataCell(Text(
                                              // railcar.productName ?? "-",
                                              railcar.compartmentList == null ||
                                                      railcar.compartmentList
                                                              .length ==
                                                          0
                                                  ? "-"
                                                  : railcar.compartmentList[0]
                                                          .productName ??
                                                      "-",
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color,
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(16),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                              ))),
                                          CustomDataCell(
                                            GestureDetector(
                                              onTap: () {},
                                              child: Text(
                                                  //railcar.yardId ??"- ",
                                                  railcar.yardId == null
                                                      ? " - "
                                                      : MemoryData
                                                          .inMemoryYardMap[
                                                              railcar.yardId]
                                                          .yardName,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .textTheme
                                                        // ignore: deprecated_member_use
                                                        .body1
                                                        .color,
                                                    fontFamily: 'Roboto',
                                                    fontSize: ScreenUtil()
                                                        .setHeight(16),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(0.5),
                                                  )),
                                            ),
                                            //showEditIcon: true,
                                            placeholder: true,
                                          ),
                                          CustomDataCell(Text(
                                            railcar.trackId == null
                                                ? "-"
                                                : MemoryData
                                                    .inMemoryTrackMap[
                                                        railcar.trackId]
                                                    .trackName,
                                            style: TextStyle(
                                              color:
                                                  // ignore: deprecated_member_use
                                                  Theme.of(context)
                                                      .textTheme
                                                      .body1
                                                      .color,
                                              fontFamily: 'Roboto',
                                              fontSize:
                                                  ScreenUtil().setHeight(16),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.normal,
                                              letterSpacing:
                                                  ScreenUtil().setWidth(0.5),
                                            ),
                                          )),
                                          CustomDataCell(Text(
                                              railcar.sequenceType ?? "-",
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color,
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(16),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                              ))),
                                          CustomDataCell(filteredSpotsList
                                                  .isNotEmpty
                                              ? Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: CustomDropdown(
                                                      hintText: "Spot",
                                                      error: (this.autoValidate && railcar
                                                          .move
                                                          .to_spot_id ==
                                                          null)
                                                          ? true : false,
                                                      itemList: filteredSpotsList
                                                                  .isNotEmpty ==
                                                              true
                                                          ? filteredSpotsList
                                                              .map((e) =>
                                                                  e.spotName)
                                                              .toList()
                                                          : const [],
                                                      SelectedValue: railcar
                                                                  .move
                                                                  .to_spot_id ==
                                                              null
                                                          ? null
                                                          : filteredSpotsList
                                                              .singleWhere(
                                                                  (element) =>
                                                                      element.id ==
                                                                      railcar
                                                                          .move
                                                                          .to_spot_id,
                                                                  orElse: () =>
                                                                      null)
                                                              ?.spotName,
                                                      Ontap: (str) =>
                                                          onSpotChange(str, railcar)),
                                                )
                                              : Text("-",
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .textTheme
                                                        // ignore: deprecated_member_use
                                                        .body1
                                                        .color,
                                                    fontFamily: 'Roboto',
                                                    fontSize: ScreenUtil()
                                                        .setHeight(16),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(0.5),
                                                  )))
                                        ]);
                                      })?.toList() ??
                                      [],
                                ),
                              )
                            ]),
                      ),
                    ),
                  ),
                ],
              ));
            },
          ),
        ));
  }
}
