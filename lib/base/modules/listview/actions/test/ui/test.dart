import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/modules/listview/actions/outbound_railcar/ui/outbound_form_bloc.dart';

class Test extends StatefulWidget {

  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  List<Equipment> selectedEquipments = List<Equipment>();

  final globalKey = GlobalKey<ScaffoldState>();

  _TestState() {
    selectedEquipments.add(new Equipment(equipmentInitial: 'CHAN', equipmentNumber: '123456'));
  }

  @override
  void initState() {
    super.initState();
  }

  Widget FormUI() {

    return BlocProvider(
        create: (context) => OutboundFormBloc(this.selectedEquipments),
        child: Builder(
          builder: (context) {
            return FormBlocListener<OutboundFormBloc, String, String>(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: CardUI(
                          content: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.all(20),
                                        child: Container(
                                            child: ListTile(
                                          leading: GestureDetector(
                                              onTap: () {
                                                //   popUpDialog();
                                                Navigator.of(context).pop();
                                              },
                                              child: Icon(
                                                Icons.arrow_back,
                                              )),
                                          title: Text('Test',
                                              style: TextStyle(
                                                  fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.normal,
                                                  fontFamily: 'Roboto',
                                                  color: Theme.of(context)
                                                      .textTheme
                                                      // ignore: deprecated_member_use
                                                      .headline
                                                      .color)),
                                        ))),
                                  ],
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Padding(
                                          padding: EdgeInsets.all(20),
                                          child: Container(
                                              child: GestureDetector(
                                            onTap: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: RichText(
                                              text: TextSpan(
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      // ignore: deprecated_member_use
                                                      .body1,
                                                  children: <InlineSpan>[
                                                    TextSpan(
                                                        text: 'CANCEL',
                                                        style: TextStyle(
                                                            fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                                            fontStyle: FontStyle.normal,
                                                            fontWeight: FontWeight.w500,
                                                            letterSpacing: ScreenUtil().setWidth(1.25),
                                                            fontFamily: 'Roboto',
                                                            color: Color(0xFF3e8aeb)))
                                                  ]),
                                            ),
                                          ))),
                                      Padding(
                                          padding: EdgeInsets.all(20),
                                          child: Container(
                                              width: ScreenUtil().setWidth(180),
                                              height: ScreenUtil().setHeight(48),
                                              child: FlatButton(
                                                  onPressed: testCrash,
                                                  child: Text('CRASH',
                                                      style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: ScreenUtil().setHeight(14),
                                                          fontStyle: FontStyle.normal,
                                                          fontWeight: FontWeight.w500,
                                                          letterSpacing: ScreenUtil().setWidth(1.25),
                                                          color: Colors.white)),
                                                  color: Color(0xFF3e8aeb),
                                                  textColor: Colors.white,
                                                  disabledColor: Colors.grey,
                                                  disabledTextColor: Colors.black,
                                                  splashColor: Color(0xFF3e8aeb)))),
                                    ],
                                  )
                                ],
                              ),
                            ],
                          ),
                        ],
                      )),
                    ),
                  )
                ],
              ),
            );
          },
        ));
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);

    return Scaffold(
        key: globalKey,
        appBar: MainAppBar(),
        drawer: AppDrawer(),
        body: Form(

          child: FormUI(),
        ));
  }

  testCrash() {
    print("crashing...");
    FirebaseCrashlytics.instance.crash();
  }
}
