import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/location_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/common_base/model/purchase_order_model.dart';
import 'package:mbase/base/core/common_base/model/shipment_type_model.dart';
import 'package:mbase/base/core/common_base/model/uom.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/railcar_block.dart';

class LoadRailCarDTO {
  List<Equipment> selectedEquipmentList=[];
  List<Equipment> selectedEquipmentListSecondGrid=[];
  ProductModel selectedProduct;
  UOM selectedUom;
  PurchaseOrderModel selectedPurchaseOrder;
  ShipmentTypeModel selectedShipmentTypeValue;
  RailcarBlock blockTo;
  LocationModel loadFrom;
  String loadAmount;
  String shipmentText;
  DateTime loadedDateTime;
  Map additionalField = {};
}
