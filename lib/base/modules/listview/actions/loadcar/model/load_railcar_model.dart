import 'package:mbase/base/core/common_base/model/equipment.dart';

class LoadRailCarModel {
  LoadRailCarModel(
      {this.product,
      this.equipment,
      this.uomValue,
      this.uomId,
      this.purchaseOrder,
      this.shipmentType,
      this.shipmentName,
      this.loadDate,
      this.loadFrom,
      this.blockTo,
      this.loadAmount,
      this.currentAmount,
      this.edited,
      this.railCarVisibleTextValue,
      this.equipmentId,
      this.additionalFields,
      this.facilityId,
      this.createdDt,
      this.updatedDt,
      this.source,
      this.transactionStatus,
      this.permission_id,
      this.operationType,
      this.asset_master_id,
      this.id});

  String product;
  String uomValue;
  String uomId;
  String purchaseOrder;
  String shipmentType;
  String shipmentName;
  DateTime loadDate;
  String loadFrom;
  String blockTo;
  String loadAmount;
  String currentAmount;
  bool edited;
  String railCarVisibleTextValue;
  String equipmentId;
  List<AdditionFiled> additionalFields;
  String facilityId;
  String asset_master_id;
  DateTime createdDt;
  String updatedDt;
  String source;
  String transactionStatus;
  String permission_id;
  String operationType;
  String id;
  String user;
  Equipment equipment;

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "equipment_id": equipmentId == null ? null : equipmentId,
        "equipment_doc": equipment == null ? null : equipment.toMap(),
        "uom": uomId == null ? null : uomId,
        "purchase_order": purchaseOrder == null ? null : purchaseOrder,
        "load_from_id": loadFrom == null ? null : loadFrom,
        "block_to_id": blockTo == null ? null : blockTo,
        "product_id": product == null ? null : product,
        "load_amount": loadAmount == null ? null : loadAmount,
        "current_amount": currentAmount == null ? null : currentAmount,
        "load_date_time": loadDate == null ? null : loadDate.toString(),
        "shipment_type_id": shipmentType == null ? null : shipmentType,
        "shipment_name": shipmentName == null ? null : shipmentName,
        "additional_fields": additionalFields == null ? null : additionalFields.map((i) => i.toMap()).toList(),
        "facility_id": facilityId == null ? null : facilityId,
        "transaction_status": transactionStatus == null ? null : transactionStatus,
        "permission_id": permission_id == null ? null : permission_id,
        "created_dt": createdDt == null ? null : createdDt.toString(),
        "updated_dt": updatedDt == null ? null : updatedDt.toString(),
        "source": source == null ? null : source,
        "operation_type": operationType == null ? null : operationType,
        "asset_master_id": asset_master_id == null ? null : asset_master_id,
        "user": user == null ? null : user,
      };

  @override
  String toString() {
    return 'LoadRailCarModel{loadAmount: $loadAmount, currentAmount: $currentAmount, equipmentId: $railCarVisibleTextValue}';
  }
}

class AdditionFiled {
  String additionalFieldName;
  String additionalFieldValue;

  AdditionFiled(this.additionalFieldName, this.additionalFieldValue);

  Map<String, String> toMap() {
    return {
      "additionalFieldName": additionalFieldName == null ? null : additionalFieldName,
      "additionalFieldValue": additionalFieldValue == null ? null : additionalFieldValue,
    };
  }
}
