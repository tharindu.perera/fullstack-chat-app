import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/assigned_entry_field.dart';
import 'package:mbase/base/core/common_base/model/customer_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/facility_configuration_model.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/location_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/common_base/model/purchase_order_model.dart';
import 'package:mbase/base/core/common_base/model/shipment_type_model.dart';
import 'package:mbase/base/core/common_base/model/uom.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/inspection/repository/inspection_details_repository.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/railcar_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/repository/assign_block_repository.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/model/load_railcar_dto.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/base/modules/seal/model/seal_model.dart';
import 'package:mbase/base/modules/seal/repository/seal_repository.dart';
import 'package:mbase/env.dart';

class LoadCarRepo {
  var commonDao = CommonDao();
  var equipmentDao = EquipmentDAO();
  var assignBlockRepository = AssignBlockRepository();
  var sealRepo = SealRepository();
  var inspectionDetailsRepo = InspectionDetailsRepo();
  var notificationRepository = NotificationRepository();

  Future<List<RailcarBlock>> fetchRailcarBlocks() {
    return assignBlockRepository.fetchRailcarBlocks().then((list) {
      list.sort((a, b) => a.railcarBlockToCodeName.toLowerCase().compareTo(b.railcarBlockToCodeName.toLowerCase()));
      list.insert(0, new RailcarBlock(id: "0", railcarBlockToCodeName: "--Please Select--"));
      return list;
    });
  }

  Future<List<AssignedEntryField>> fetchAdditionalFields() async {
    return commonDao.fetchAdditionalFields(env.userProfile.facilityId, "LOAD");
  }

  Future<List<UOM>> fetchUOMList() async {
    return commonDao.fetchUOMList().then((list) {
      list.sort((a, b) => a.unit.toLowerCase().compareTo(b.unit.toLowerCase()));
      list.insert(0, new UOM(id: "0", unit: "--Please Select--"));
      return list;
    });
  }

  // Queries for railcarBlockId from assigned_block and return block data from railcar_block
  Future <Map<String,String>> fetchBlockToCodesForSelectedRailcars({String facility_visit_id, String facilityId}) async {
    Map<String, String> blockDataMap = {}; //Consists Block Name & Block Id data

  return commonDao.fetchAssignedBlocks(facility_visit_id:facility_visit_id, facilityId:facilityId).then((value) {
    if (value == null) {
      return null;
    }
      return commonDao.fetchRailcarBlockData(railcar_block_id: value.railcarBlockId,facilityId: value.facilityId)
            .then((value) {
          blockDataMap.putIfAbsent("blockToId", () => value.id);
          blockDataMap.putIfAbsent("blockToName", () => value.railcarBlockToCodeName);
          print("blockDataMap --------->>>>>>>>>>>>>>>>> ${blockDataMap}");
          return blockDataMap;
        });
    });
  }

  Future<List<PurchaseOrderModel>> fetchPurchaseOrders() async {
    return commonDao.fetchPurchaseOrderList(env.userProfile.facilityId);
  }

  Future<List<PurchaseOrderModel>> fetchOpenPurchaseOrders() async {
    return commonDao.fetchOpenPurchaseOrders(env.userProfile.facilityId).then((list) {
      list.retainWhere((element) => element.order_status=="Open" && element.inbound_outbound_ind=="O");
      list.sort((a, b) => a.purchaseOrderNumber.toLowerCase().compareTo(b.purchaseOrderNumber.toLowerCase()));
      list.insert(0, new PurchaseOrderModel(id: "0", purchaseOrderNumber: "--Please Select--", productId: "0"));
      return list;
    });
  }

  Future<List<ShipmentTypeModel>> fetchShipmentTypes() async {
    return commonDao.fetchShipmentTypeList().then((list) {
      list.sort((a, b) => a.shipmentTypeDescription.toLowerCase().compareTo(b.shipmentTypeDescription.toLowerCase()));
      list.insert(0, new ShipmentTypeModel(id: "0", shipmentTypeDescription: "--Please Select--"));
      return list;
    });
  }

  Future<List<ProductModel>> fetchProducts() async {
    return commonDao.fetchProductsList().then((list) {
      list.sort((a, b) => a.productName.toLowerCase().compareTo(b.productName.toLowerCase()));
      list.insert(0, new ProductModel(productId: "0", productName: "--Please Select--", uom_id: "0", dryOrLiquid: ""));
      return list;
    });
  }

  Future<List<LocationModel>> fetchLocations() async {
    List<LocationModel> list = [];
    return commonDao.fetchLocationList().then((locations) {
      locations.forEach((location) {
        Map map = {};
        LocationModel tempLoc;
        location.inventory.forEach((inventory) {
          if (map[inventory.product_id + (inventory.customer_id ?? "")] == null) {
            tempLoc = LocationModel();
            tempLoc.id = location.id;
            tempLoc.facility_id = location.facility_id;
            tempLoc.areaId = location.areaId;
            tempLoc.name = location.name;
            tempLoc.createdDt = location.createdDt;
            tempLoc.updatedDt = location.updatedDt;
            tempLoc.inventory = location.inventory;
            tempLoc.measure_id = inventory.measure_id;
            tempLoc.measure_name = inventory.measure_name;
            tempLoc.product_id = inventory.product_id;
            tempLoc.product_name = inventory.product_name;
            tempLoc.customer_id = inventory.customer_id;
            tempLoc.customer_name = inventory.customer_name;
            tempLoc.quantity = inventory.quantity;
            tempLoc.dropDownDisplayName = tempLoc.name + " / " + (tempLoc.product_name ?? "* NO PRODUCT *") + " / " + (tempLoc.customer_name ?? "* NO CUSTOMER *");
            list.add(tempLoc);
            map[inventory.product_id + (inventory.customer_id ?? "")] = tempLoc;
          } else {
            tempLoc = map[inventory.product_id + (inventory.customer_id ?? "")];
            if (inventory.measure_id != tempLoc.measure_id) {
              var uomConversion = MemoryData.uomConversion[inventory.measure_id + tempLoc.measure_id];
              if (uomConversion == null) {
                print(">>>>>>UOM Conversion not found for ${inventory.measure_name}(id:${inventory.measure_id})  and ${tempLoc.measure_name} (id:${tempLoc.measure_id})");
//                throw "UOM Conversion not found for ${inventory.measure_name}(id:${inventory.measure_id})  and ${tempLoc.measure_name} (id:${tempLoc.measure_id})";
              } else {
                tempLoc.quantity = (double.parse(tempLoc.quantity) + (double.parse(uomConversion.conversion_factor) * double.parse(inventory.quantity))).toString();
              }
            } else {
              tempLoc.quantity = (double.parse(tempLoc.quantity) + double.parse(inventory.quantity)).toString();
            }
          }
        });
      });
      list.sort((a, b) => a.dropDownDisplayName.toLowerCase().compareTo(b.dropDownDisplayName.toLowerCase()));
      LocationModel locationModel = new LocationModel();
      locationModel.id = "0";
      locationModel.dropDownDisplayName = "--Please Select--";
      locationModel.product_name = "";
      locationModel.product_id = "0";
      list.insert(0, locationModel);
      return list;
    });
  }

  Future<Map<String, List<String>>> checkBeforeSavingLoad(LoadRailCarDTO dto, FacilityConfigurationModel fcModel) async {
    Map<String, List<String>> map = {};
    List<String> errorList = [];
    List<String> warningList = [];
    List<Equipment> equipmentList = dto.selectedEquipmentList;

    for (Equipment equipment in equipmentList) {
      LocationModel locationModel = equipment.compartmentList[0].loadFrom;

      double existingLocationAmount;
      print("locationModel:$locationModel");
      if(locationModel != null) {
        if (locationModel.measure_id != equipment.compartmentList[0].uomId) {
          var conversion = MemoryData.uomConversion[locationModel.measure_id +
              equipment.compartmentList[0].uomId]; //from to to
          if (conversion == null) {
            throw "UOM conversion not found for ${locationModel
                .measure_name}  and ${equipment.compartmentList[0].uomValue}";
          }
          var factor = double.parse(conversion.conversion_factor);
          existingLocationAmount =
              factor * double.parse(locationModel.quantity);
        } else {
          existingLocationAmount = double.parse(locationModel.quantity);
        }

        if (double.parse(equipment.compartmentList[0].loadAmount) >
            existingLocationAmount) {
//        errorList.add("Trying to load more than existing amount in the location.");
        }
      }

      print("fcModel.whenLoadingRailcarWithoutSeal:${fcModel.whenLoadingRailcarWithoutSeal}");
      if ("Error" == fcModel.whenLoadingRailcarWithoutSeal || "Warning" == fcModel.whenLoadingRailcarWithoutSeal) {
        List<SealModel> fetchSeals = await sealRepo.fetchSeals(assetmasterId: equipment.assetMasterId);
        print("fetchSeals:${fetchSeals.length}");
        if (fetchSeals.isEmpty) {
          "Error" == fcModel.whenLoadingRailcarWithoutSeal
              ? errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has no seals")
              : warningList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has no seals");
        }
      }

      print("fcModel.whenLoadingRailcarWithoutInspection:${fcModel.whenLoadingRailcarWithoutInspection}");
      if ("Error" == fcModel.whenLoadingRailcarWithoutInspection || "Warning" == fcModel.whenLoadingRailcarWithoutInspection) {
        List<InspectionRailcarModel> inspectionList = await inspectionDetailsRepo.fetchInspectionRailcarsForLoadAndUnload(equipment.equipmentInitial + " " + equipment.equipmentNumber);
        print("inspectionList:${inspectionList.length}");
        if (inspectionList.isEmpty) {
          "Error" == fcModel.whenLoadingRailcarWithoutInspection
              ? errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has no inspection")
              : warningList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has no inspection");
        }
      }

      print("fcModel.whenLoadingRailcarWithFailedInspection:${fcModel.whenLoadingRailcarWithFailedInspection}");
      if ("Error" == fcModel.whenLoadingRailcarWithFailedInspection || "Warning" == fcModel.whenLoadingRailcarWithFailedInspection) {
        List<InspectionRailcarModel> inspectionList = await inspectionDetailsRepo.fetchInspectionRailcarsForLoadAndUnload(equipment.equipmentInitial + " " + equipment.equipmentNumber);
        print("inspectionList:${inspectionList.length}");
        inspectionList.forEach((element) {
          print("inspectionList loop  ${element.id + "  " + element.inspectionStatus}");
          if (element.inspectionStatus == "FAILED") {
            "Error" == fcModel.whenLoadingRailcarWithFailedInspection
                ? errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has failed inspection")
                : warningList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has failed inspection");
          }
        });
      }

      print("fcModel.requirePurchaseOrderWhenLoadingAnAsset:${fcModel.requirePurchaseOrderWhenLoadingAnAsset}");
      if ("True" == fcModel.requirePurchaseOrderWhenLoadingAnAsset) {
        if (dto.selectedPurchaseOrder == null) {
          "True" == fcModel.requirePurchaseOrderWhenLoadingAnAsset
              ? errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has no purchase order")
              : null;
        }
      }


      print("fcModel.requireBlockToWhenLoadingRailcar:${fcModel.requireBlockToWhenLoadingRailcar}");
      if ("True" == fcModel.requireBlockToWhenLoadingRailcar) {
        // if (dto.blockTo == null || dto.blockTo.id == "0") {
        //   "True" == fcModel.requireBlockToWhenLoadingRailcar
        //       ? errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has no block to selection")
        //       :null;
        // }
        if (equipment.blockToName == null || equipment.blockToId == "0") {
            "True" == fcModel.requireBlockToWhenLoadingRailcar
                ? errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has no block to selection")
                :null;
          }
      }

      print("fcModel.requireLocationWhenLoadingAsset:${fcModel.requireLocationWhenLoadingAsset}");
      if ("True" == fcModel.requireLocationWhenLoadingAsset) {
        if(dto.loadFrom == null){
          "True" == fcModel.requireLocationWhenLoadingAsset
              ? errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has no load from selection")
              :null;
        }
      }

      print("fcModel.overLoadLimit:${fcModel.overLoadLimit}");

      if ("Error" == fcModel.overLoadLimit &&  equipment.weight_limit !=null || "Warning" == fcModel.overLoadLimit &&  equipment.weight_limit !=null ) {
        if ((double.parse(dto.loadAmount??"0") + double.parse(equipment.compartmentList[0].currentAmount??"0"))
            > double.parse(equipment.weight_limit)) {
          "Error" == fcModel.overLoadLimit
              ? errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has exceeded load limit")
              : warningList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has exceeded load limit");
        }
      }

      if ("False" == fcModel.overrideUmlerLoadLimitWhenLoadingRailcar && equipment.weight_limit !=null) {
        if ((double.parse(dto.loadAmount??"0") + double.parse(equipment.compartmentList[0].currentAmount??"0"))
            > double.parse(equipment.weight_limit)) {
          "Error" == fcModel.overLoadLimit
              ? errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber} has exceeded load limit")
              : null;
        }
      }

      print("fcModel.productLoadingRestrictions:${fcModel.productLoadingRestrictions}");
      if ("Error" == fcModel.productLoadingRestrictions || "Warning" == fcModel.productLoadingRestrictions) {
         print("equipment.lastContainedProductId:${equipment.lastContainedProductId}");
         if (equipment.lastContainedProductId != null) {
           bool isRestricted = await new CommonDao().checkLoadingProductRestricted(equipment.lastContainedProductId,
               dto.selectedProduct.productId);
           if (isRestricted) {
             "Error" == fcModel.productLoadingRestrictions
                 ? errorList.add("${dto.selectedProduct.productName} product is restricted for ${equipment.equipmentInitial} ${equipment.equipmentNumber} railcar")
                 : warningList.add("${dto.selectedProduct.productName} product is restricted for ${equipment.equipmentInitial} ${equipment.equipmentNumber} railcar");
           }
         }
      }

      print("fcModel.customerRestrictions:${fcModel.customerRestrictions}");
      if ("Error" == fcModel.customerRestrictions || "Warning" == fcModel.customerRestrictions) {
        CustomernModel customerModel = await commonDao.fetchCustomer(dto.selectedPurchaseOrder?.customer_id);
        print("customerRestrictions customerModel:${customerModel?.id}");

        if (customerModel != null && customerModel.is_restricted == "Y") {
          "Error" == fcModel.customerRestrictions
              ? errorList.add("Customer(${customerModel.name}) of Purchase order is restricted")
              : warningList.add("Customer(${customerModel.name}) of Purchase order is restricted");
        }
      }
      print(">>>>Error list:${errorList.length} Warning list:${warningList.length}  ");
    }

    map["error"] = errorList;
    map["warning"] = warningList;
    return map;
  }

  Future<void> saveLoad(LoadRailCarDTO dto) async {
    var finalLoadCarList = dto.selectedEquipmentList.map((equipment) {
      equipment.transactionStatus = APP_CONST.TRANSACTION_STATUS_PENDING;
      equipment.permission_id = env.userProfile.id;
      equipment.facilityId = env.userProfile.facilityId;
      equipment.source = APP_CONST.MOBILITY;
      equipment.operationType = APP_CONST.LOAD_CAR_OPERATION;
      equipment.user = env.userProfile.userName;
      equipment.compartmentList[0].currentAmount = (double.parse(equipment.compartmentList[0].currentAmount ?? "0") + double.parse(equipment.compartmentList[0].loadAmount)).toString();
      equipment.createdDt = DateTime.now();
      equipment.updatedDt = DateTime.now();
      equipment.parent_id = equipment?.parent_id ?? equipment.id;
      equipment.immediateParentId = equipment.id;
      return equipment;
    }).toList();

     await finalLoadCarList.forEach((element) async {
      await equipmentDao.add(element);
      await notificationRepository.addNotification(element, APP_CONST.LOAD_CAR_OPERATION);
    });

     return null;
  }
}
