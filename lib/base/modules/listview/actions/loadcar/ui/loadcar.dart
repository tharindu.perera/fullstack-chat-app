import 'dart:async';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/model/additional_field.dart';
import 'package:mbase/base/core/common_base/model/assigned_entry_field.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/facility_configuration_model.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/location_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/common_base/model/purchase_order_model.dart';
import 'package:mbase/base/core/common_base/model/shipment_type_model.dart';
import 'package:mbase/base/core/common_base/model/uom.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown-generic.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/railcar_block.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/model/load_railcar_dto.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/repository/loadcar_repository.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/ui/load_railcar_table.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class LoadRailCar extends StatefulWidget {
  List<Equipment> selectedEquipments = List<Equipment>();
  String source;
  var parentContext;
  var product;
  double loadAmount;
  var uom;

  LoadRailCar(List<Equipment> selectedEquipments, String source, parentContext,
      var product, double loadAmount, var uom) {
    this.selectedEquipments = selectedEquipments;
    this.source = source;
    this.parentContext = parentContext;
    this.product = product;
    this.loadAmount = loadAmount;
    this.uom = uom;
  }

  @override
  _LoadRailCarState createState() => _LoadRailCarState(
      selectedEquipments, product, loadAmount, uom, source, parentContext);
}

class _LoadRailCarState extends State<LoadRailCar> {
  LoadRailCarDTO dto = LoadRailCarDTO();
  var loadCarRepo = LoadCarRepo();
  int selectedCarCount = 0;
  final globalKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

//  final _loadDateController = TextEditingController();
  FToast fToast;
  FacilityConfigurationModel fcModel = MemoryData.facilityConfiguration;
  bool isApplyButtonClicked = false;
  bool formIsValid = false;
  bool canProductDropDownSelect = true;
  String productDropDownHintText =
      "Product"; //Product is disabled according to purchase orders and location selection

  List<PurchaseOrderModel> purchaseOrderList = [];
  List<ShipmentTypeModel> shipmentTypeList = [];
  List<LocationModel> loadFromList = [];
  List<LocationModel> originalUnmodifiedLoadFromList = [];
  List<UOM> originalUnmodifiedUnitList = [];
  List<ProductModel> productList = [];
  List<UOM> uomUnitList = [];
  List<RailcarBlock> blockToList = [];
  List<AssignedEntryField> assignedEntryFields = [];
  ThemeChanger _themeChanger;
  final dateFormat = new DateFormat('MM/dd/yyyy HH:mm');

  String source;
  var parentContext;

  _LoadRailCarState(List<Equipment> selectedEquipments, var product,
      double loadAmount, var uom, String source, parentContext) {
    this.source = source;
    this.parentContext = parentContext;
    dto.selectedProduct = product;
    dto.loadAmount = loadAmount != null ? loadAmount.toString() : '';
    dto.selectedUom = uom;
    for (Equipment equipment in selectedEquipments) {
      this.dto.selectedEquipmentList.add(Equipment.fromMap(equipment.toMap()));
    }
  }

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);
    Future(() async {
      Loader.show(context,
          progressIndicator: CircularProgressIndicator(),
          overlayColor: Colors.transparent);
      await fetchAssignedBlocksForRailcar();
      selectedCarCount = await dto.selectedEquipmentList.length;
      purchaseOrderList = await loadCarRepo.fetchOpenPurchaseOrders();
      uomUnitList = await loadCarRepo.fetchUOMList();
      shipmentTypeList = await loadCarRepo.fetchShipmentTypes();
      loadFromList = await loadCarRepo.fetchLocations().catchError((err) {
        print("Error >>>> ${err}");
        _showToastError([]..add(err.toString()));
        return Future.value(List<LocationModel>());
      });
      originalUnmodifiedLoadFromList.addAll(loadFromList);
      originalUnmodifiedUnitList.addAll(uomUnitList);
      productList = await loadCarRepo.fetchProducts();
      // await loadCarRepo.fetchBlockToCodesForSelectedRailcars(assert_master_id: "4412",facilityId: "1");
      blockToList = await loadCarRepo.fetchRailcarBlocks();
      assignedEntryFields = await loadCarRepo.fetchAdditionalFields();

      try {
        if (dto.selectedEquipmentList.length == 1 &&
            dto.selectedEquipmentList[0].purchaseOrderName != null) {
          dto.selectedPurchaseOrder = purchaseOrderList.singleWhere((element) =>
              element.purchaseOrderNumber ==
              dto.selectedEquipmentList[0].purchaseOrderName);
          onPurchaseOrderChanged(
              dto.selectedEquipmentList[0].purchaseOrderName);
        }
      } catch (error) {
        print(error);
      }

      Loader.hide();
      dto.loadedDateTime = DateTime.now();
//      _loadDateController.text = dateFormat.format(DateTime.now())?.toString();
      setState(() {});
    });
  }

  fetchAssignedBlocksForRailcar() async {
    Map<String, String> resultMap = {};
    this.dto.selectedEquipmentList.forEach((element) async {
      resultMap = await loadCarRepo.fetchBlockToCodesForSelectedRailcars(
          facility_visit_id: element.facilityVisitId,
          facilityId: element.facilityId);
      print("block to resultMap --------- ${resultMap}");
      if (resultMap != null) {
        element?.blockToId = resultMap["blockToId"];
        element?.blockToName = resultMap["blockToName"];
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);
    _themeChanger = Provider.of<ThemeChanger>(context);

    return Scaffold(
        key: globalKey,
        appBar: MainAppBar(),
        drawer: AppDrawer(),
        body: Form(
          key: _formKey,
          autovalidate: false,
          child: CardUI(
              content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.all(20),
                            child: Container(
                                child: ListTile(
                              leading: GestureDetector(
                                  onTap: () {
                                    cancelPopUpDialog();
                                  },
                                  child: Icon(
                                    Icons.arrow_back,
                                  )),
                              title: Text('Load Railcars ($selectedCarCount)',
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(24,
                                          allowFontScalingSelf: true),
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: 'Roboto',
                                      color: Theme.of(context)
                                          .textTheme
                                          // ignore: deprecated_member_use
                                          .headline
                                          .color)),
                            ))),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.all(20),
                              child: Container(
                                  child: GestureDetector(
                                onTap: () {
                                  cancelPopUpDialog();
                                },
                                child: RichText(
                                  text: TextSpan(
                                      style: Theme.of(context)
                                          .textTheme
                                          // ignore: deprecated_member_use
                                          .body1,
                                      children: <InlineSpan>[
                                        TextSpan(
                                            text: 'CANCEL',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(14),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(1.25),
                                                color: Color(0xFF3e8aeb)))
                                      ]),
                                ),
                              ))),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 0, top: 14, right: 24, bottom: 14),
                              child: Container(
                                width: ScreenUtil().setWidth(180),
                                height: ScreenUtil().setHeight(48),
                                child: FlatButton(
                                    onPressed: isApplyButtonClicked
                                        ? () async => await save()
                                        : null,
                                    child: Text(
                                      'LOAD RAILCAR',
                                      style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing:
                                              ScreenUtil().setWidth(1.25),
                                          color: Colors.white),
                                    ),
                                    color: Color(0xFF508be4),
                                    textColor: Colors.white,
                                    disabledColor: Colors.grey,
//                                    disabledTextColor: Colors.black,
                                    splashColor: Color(0xFF3e8aeb)),
                              )),
                        ],
                      )
                    ],
                  ),
                ],
              ),
              Padding(
                padding:
                    EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(14)),
                child: Divider(
                  thickness: 2.0,
                  // color: Color.fromRGBO(0, 0, 0, 1),
                ),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(1),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    CustomDropdown(
                      dropdownWidth: 230,
                      error: "Yes" ==
                                  fcModel
                                      .requirePurchaseOrderWhenLoadingAnAsset &&
                              isApplyButtonClicked &&
                              dto.selectedPurchaseOrder == null
                          ? true
                          : false,
                      hintText: "Purchase Order",
                      itemList: purchaseOrderList
                          .map((e) => e.purchaseOrderNumber)
                          .toList(),
                      SelectedValue:
                          dto.selectedPurchaseOrder?.purchaseOrderNumber,
                      Ontap: (String selectedValue) {
                        onPurchaseOrderChanged(selectedValue);
                      },
                    ),
                    CustomDropdown(
//                      error: isApplyButtonClicked && dto.selectedShipmentTypeValue == null ? true : false,
                      hintText: "Shipment Type",
                      itemList: shipmentTypeList
                          .map((e) => e?.shipmentTypeDescription)
                          .toList(),
                      SelectedValue: dto
                          .selectedShipmentTypeValue?.shipmentTypeDescription,
                      Ontap: (String selectedValue) {
                        onShipmentTypeChange(selectedValue);
                      },
                    ),
                    Container(
                      width: 213,
                      alignment: Alignment.center,
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly,
                          LengthLimitingTextInputFormatter(50),
                        ],
                        onChanged: (str) {
                          dto.shipmentText = str;
                          if (isApplyButtonClicked) {
                            _formKey.currentState.validate();
                          }
                        },
                        validator: (String arg) {
                          if (isApplyButtonClicked &&
                              (dto.selectedShipmentTypeValue != null &&
                                  arg.isEmpty)) {
                            return 'This field is mandatory';
                          } else
                            return null;
                        },
                        style: TextStyle(
                            color: _themeChanger.getTheme().primaryColor ==
                                    Color(0xff182e42)
                                ? Colors.white
                                : _themeChanger.getTheme().primaryColor ==
                                        Color(0xfff5f5f5)
                                    ? Colors.black
                                    : null),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(1.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _themeChanger.getTheme().primaryColor ==
                                        Color(0xff182e42)
                                    ? Color(0xffffffff)
                                    : _themeChanger.getTheme().primaryColor ==
                                            Color(0xfff5f5f5)
                                        ? Colors.black26
                                        : null),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _themeChanger.getTheme().primaryColor ==
                                        Color(0xff182e42)
                                    ? Color(0xffffffff)
                                    : _themeChanger.getTheme().primaryColor ==
                                            Color(0xfff5f5f5)
                                        ? Color(0xff182e42)
                                        : null),
                          ),
                          filled: true,
                          fillColor: _themeChanger.getTheme().primaryColor ==
                                  Color(0xff182e42)
                              ? Color(0xff172636)
                              : _themeChanger.getTheme().primaryColor ==
                                      Color(0xfff5f5f5)
                                  ? Color(0xfff5f5f5)
                                  : null,
                          hintText: 'Shipment Name',
                          hintStyle: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: ScreenUtil().setHeight(14),
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            letterSpacing: ScreenUtil().setWidth(0.25),
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          width: 230,
                          padding: EdgeInsets.symmetric(vertical: 4.0),
                          alignment: Alignment.center,
                          child: DateTimeField(
                            initialValue: DateTime.now(),
                            onShowPicker: (context, currentValue) async {
                              final date = await showDatePicker(
                                  context: context,
                                  firstDate: DateTime(1900),
                                  initialDate: currentValue ?? DateTime.now(),
                                  lastDate: DateTime(2100));
                              if (date != null) {
                                final time = await showTimePicker(
                                  context: context,
                                  initialTime: TimeOfDay.fromDateTime(
                                      currentValue ?? DateTime.now()),
                                );
                                dto.loadedDateTime =
                                    DateTimeField.combine(date, time);
                                return DateTimeField.combine(date, time);
                              } else {
                                dto.loadedDateTime = currentValue;
                                return currentValue;
                              }
                            },
                            format: dateFormat,
                            style: TextStyle(
                                color: _themeChanger.getTheme().primaryColor ==
                                        Color(0xff182e42)
                                    ? Color(0xffffffff)
                                    : _themeChanger.getTheme().primaryColor ==
                                            Color(0xfff5f5f5)
                                        ? Color(0xff182e42)
                                        : null),
//                        controller: _loadDateController,
                            readOnly: true,
                            decoration: InputDecoration(
                              prefixIcon: Icon(Icons.date_range,
                                  color: _themeChanger
                                              .getTheme()
                                              .primaryColor ==
                                          Color(0xff182e42)
                                      ? Color(0xffffffff)
                                      : _themeChanger.getTheme().primaryColor ==
                                              Color(0xfff5f5f5)
                                          ? Color(0xff182e42)
                                          : null),
                              labelText: "Load Date",
                              labelStyle: TextStyle(
                                  color: _themeChanger
                                              .getTheme()
                                              .primaryColor ==
                                          Color(0xff182e42)
                                      ? Color(0xffffffff)
                                      : _themeChanger.getTheme().primaryColor ==
                                              Color(0xfff5f5f5)
                                          ? Color(0xff182e42)
                                          : null),
                              contentPadding: EdgeInsets.all(1.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color:
                                        _themeChanger.getTheme().primaryColor ==
                                                Color(0xff182e42)
                                            ? Color(0xffffffff)
                                            : _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xfff5f5f5)
                                                ? Colors.black26
                                                : null),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color:
                                        _themeChanger.getTheme().primaryColor ==
                                                Color(0xff182e42)
                                            ? Color(0xffffffff)
                                            : _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xfff5f5f5)
                                                ? Colors.black26
                                                : null),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color:
                                        _themeChanger.getTheme().primaryColor ==
                                                Color(0xff182e42)
                                            ? Color(0xffffffff)
                                            : _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xfff5f5f5)
                                                ? Colors.black26
                                                : null),
                              ),
                              filled: true,
                              fillColor:
                                  _themeChanger.getTheme().primaryColor ==
                                          Color(0xff182e42)
                                      ? Color(0xff172636)
                                      : _themeChanger.getTheme().primaryColor ==
                                              Color(0xfff5f5f5)
                                          ? Color(0xfff5f5f5)
                                          : null,
                            ),
                          ),
                        ),
                        CustomDropdownGeneric<LocationModel>(
                            error: "True" ==
                                        fcModel
                                            .requireLocationWhenLoadingAsset &&
                                    isApplyButtonClicked &&
                                    dto.loadFrom == null
                                ? true
                                : false,
                            hintText: "Load From",
                            itemList: loadFromList,
                            SelectedValue: dto.loadFrom,
                            Ontap: onLoadFromChanged),
                        CustomDropdown(
                            // error: "True" == fcModel.requireBlockToWhenLoadingRailcar && isApplyButtonClicked && dto.blockTo == null ? true : false,
                            hintText: "Block To",
                            itemList: blockToList
                                .map((e) => e.railcarBlockToCodeName)
                                .toList(),
                            SelectedValue: dto.blockTo?.railcarBlockToCodeName,
                            Ontap: onBlockToChanged),
                      ])),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      CustomDropdown(
                          dropdownWidth: 230,
                          error: isApplyButtonClicked &&
                                  dto.selectedProduct == null
                              ? true
                              : false,
                          hintText: productDropDownHintText,
                          itemList:
                              productList.map((e) => e.productName).toList(),
                          SelectedValue: dto.selectedProduct?.productName,
                          Ontap: canProductDropDownSelect
                              ? onProductChanged
                              : null),
                      Container(
                        width: 213,
                        alignment: Alignment.center,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                            LengthLimitingTextInputFormatter(6),
                          ],
                          initialValue: dto.loadAmount,
                          onChanged: (str) {
                            dto.loadAmount = str;
                            if (isApplyButtonClicked) {
                              _formKey.currentState.validate();
                            }
                          },
                          validator: (String arg) {
                            if (isApplyButtonClicked &&
                                (arg == null || arg.isEmpty)) {
                              return 'This field is mandatory';
                            } else
                              return null;
                          },
                          style: TextStyle(
                              color: _themeChanger.getTheme().primaryColor ==
                                      Color(0xff182e42)
                                  ? Colors.white
                                  : _themeChanger.getTheme().primaryColor ==
                                          Color(0xfff5f5f5)
                                      ? Colors.black
                                      : null),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(1.0),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: _themeChanger
                                              .getTheme()
                                              .primaryColor ==
                                          Color(0xff182e42)
                                      ? Color(0xffffffff)
                                      : _themeChanger.getTheme().primaryColor ==
                                              Color(0xfff5f5f5)
                                          ? Colors.black26
                                          : null),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: _themeChanger
                                              .getTheme()
                                              .primaryColor ==
                                          Color(0xff182e42)
                                      ? Color(0xffffffff)
                                      : _themeChanger.getTheme().primaryColor ==
                                              Color(0xfff5f5f5)
                                          ? Color(0xff182e42)
                                          : null),
                            ),
                            filled: true,
                            fillColor: _themeChanger.getTheme().primaryColor ==
                                    Color(0xff182e42)
                                ? Color(0xff172636)
                                : _themeChanger.getTheme().primaryColor ==
                                        Color(0xfff5f5f5)
                                    ? Color(0xfff5f5f5)
                                    : null,
                            hintText: 'Load Amount',
                            hintStyle: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: ScreenUtil().setHeight(14),
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal,
                              letterSpacing: ScreenUtil().setWidth(0.25),
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                      CustomDropdown(
                          error: isApplyButtonClicked && dto.selectedUom == null
                              ? true
                              : false,
                          hintText: "Unit",
                          itemList: uomUnitList.map((e) => e.unit).toList(),
                          SelectedValue: dto.selectedUom?.unit,
                          Ontap: onUOMChanged),
                    ],
                  )),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              assignedEntryFields.length > 0
                  ? Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: DottedBorder(
                        color: _themeChanger.getTheme().primaryColor ==
                                Color(0xff182e42)
                            ? Color(0xfff5f5f5)
                            : _themeChanger.getTheme().primaryColor ==
                                    Color(0xfff5f5f5)
                                ? Color(0xff172636)
                                : null,
                        dashPattern: [4],
                        strokeWidth: 1,
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Wrap(
                            alignment: WrapAlignment.spaceEvenly,
                            spacing: 10,
                            direction: Axis.horizontal,
                            children: assignedEntryFields
                                .map(
                                  (e) => Container(
                                    width: ScreenUtil().setWidth(213),
                                    padding:
                                        EdgeInsets.symmetric(vertical: 4.0),
                                    alignment: Alignment.center,
                                    child: TextFormField(
                                      style: TextStyle(
                                          color: _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xff182e42)
                                              ? Colors.white
                                              : _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xfff5f5f5)
                                                  ? Colors.black
                                                  : null),
                                      validator: (String arg) {
                                        return (arg == null || arg.isEmpty) &&
                                                e.required == "1" &&
                                                (dto.additionalField[e.id] ==
                                                        null ||
                                                    dto.additionalField[e.id]
                                                        .toString()
                                                        .isEmpty)
                                            ? 'This field is mandatory'
                                            : null;
                                      },
                                      readOnly: false,
                                      onChanged: (str) {
                                        onAdditionalFieldChanged(str, e.label);
                                      },
                                      decoration: InputDecoration(
                                        labelText: e.label,
                                        labelStyle:
                                            TextStyle(color: Colors.blueGrey),
                                        contentPadding: EdgeInsets.all(1.0),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xffffffff)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Colors.black26
                                                      : null),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xffffffff)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xff182e42)
                                                      : null),
                                        ),
                                        filled: true,
                                        fillColor: _themeChanger
                                                    .getTheme()
                                                    .primaryColor ==
                                                Color(0xff182e42)
                                            ? Color(0xff172636)
                                            : _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xfff5f5f5)
                                                ? Color(0xfff5f5f5)
                                                : null,
                                      ),
                                    ),
                                  ),
                                )
                                .toList(),
                          ),
                        ),
                      ),
                    )
                  : Container(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      height: ScreenUtil().setHeight(48),
                      width: ScreenUtil().setWidth(288),
                      decoration: ShapeDecoration(
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                              width: 1.0,
                              style: BorderStyle.solid,
                              color: Color.fromRGBO(0, 0, 0, 0.12)),
                          // borderRadius: BorderRadius.all(Radius.circular(4.0)),
                        ),
                      ),
                      child: FlatButton(
                          child: Text(
                            'APPLY TO SELECTION',
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(14),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w500,
                                letterSpacing: ScreenUtil().setWidth(1.25),
                                color: Colors.white),
                          ),
                          onPressed:
                              dto.selectedEquipmentListSecondGrid.isNotEmpty
                                  ? () {
                                      applyToSelection();
                                    }
                                  : null,
                          color: Color(0xFF508be4),
                          textColor: Colors.white,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          splashColor: Color(0xFF3e8aeb)),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 50),
                child: Center(child: LoadRailCarTable(dto, refreshScreen)),
              )
            ],
          )),
        ));
  }

  onShipmentTypeChange(String value) {
    if (value != "--Please Select--") {
      dto.selectedShipmentTypeValue = shipmentTypeList
          .singleWhere((element) => element.shipmentTypeDescription == value);
    } else {
      dto.selectedShipmentTypeValue = null;
    }
    setState(() {});
  }

  onPurchaseOrderChanged(String purchaseOrder) {
    if (purchaseOrder != "--Please Select--") {
      dto.selectedPurchaseOrder = purchaseOrderList.singleWhere(
          (element) => element.purchaseOrderNumber == purchaseOrder);
      dto.loadFrom = null;

      print(
          "dto.selectedPurchaseOrder.productId:${dto.selectedPurchaseOrder.productId}");
      dto.selectedProduct = productList.singleWhere((element) =>
          element.productId == dto.selectedPurchaseOrder.productId);
      canProductDropDownSelect =
          purchaseOrder != "--Please Select--" ? false : true;
      productDropDownHintText = dto.selectedProduct.productName;
      _filterUOMList();
      _filterLocationList();
    } else {
      dto.selectedPurchaseOrder = null;
      if (dto.loadFrom == null ||
          dto.loadFrom.dropDownDisplayName == "--Please Select--") {
        dto.selectedProduct = null;
        canProductDropDownSelect = true;
      }
      _resetUOMList();
      _resetLocationList();
    }
    setState(() => {});
  }

  onLoadFromChanged(LocationModel loadFrom) {
    if (loadFrom.dropDownDisplayName != "--Please Select--") {
      try {
        dto.loadFrom = loadFrom;
        dto.selectedProduct = productList.lastWhere(
            (element) => element.productId == dto.loadFrom.product_id);
        canProductDropDownSelect = loadFrom.id != "0" ? false : true;
        productDropDownHintText = dto.selectedProduct.productName;
      } catch (error) {
        _showToastError([]..add(
            "No product match for this location ${dto.loadFrom.product_name}"));
      }
      _filterUOMList();
    } else {
      dto.loadFrom = null;
      canProductDropDownSelect =
          dto.selectedPurchaseOrder == null ? true : false;
    }
    setState(() {});
  }

  onProductChanged(String product) {
    if (product != "--Please Select--") {
      dto.selectedProduct =
          productList.singleWhere((element) => element.productName == product);
      _filterUOMList();
      _filterLocationList();
    } else {
      dto.selectedProduct = null;
      _resetUOMList();
      _resetLocationList();
    }
    setState(() {});
  }

  void _filterUOMList() {
    uomUnitList = [];
    uomUnitList.addAll(originalUnmodifiedUnitList);
    dto.selectedUom = null;
    uomUnitList.removeWhere((element) {
      if ((dto.selectedProduct.dryOrLiquid == "DRY") &&
          (["Metric Tons", "Pounds", "Net Tons", "Kilograms", "Bundles", "Each"]
              .contains(element.unit))) {
        return false;
      } else if ((dto.selectedProduct.dryOrLiquid == "LIQUID") &&
          (![
            "Metric Tons",
            "Pounds",
            "Net Tons",
            "Kilograms",
            "Bundles",
            "Each"
          ].contains(element.unit))) {
        return false;
      } else if (element.id == "0") {
        return false;
      } else {
        return true;
      }
    });
    dto.selectedUom = uomUnitList
        .singleWhere((element) => element.id == dto.selectedProduct?.uom_id);
  }

  void _resetUOMList() {
    dto.selectedUom = null;
    uomUnitList = [];
    uomUnitList.addAll(originalUnmodifiedUnitList);
  }

  void _filterLocationList() {
    loadFromList = [];
    loadFromList.addAll(originalUnmodifiedLoadFromList);
    loadFromList.removeWhere((location) {
      return (dto.selectedProduct?.productId != null &&
              location.product_id != null &&
              location.product_id != dto.selectedProduct?.productId) ||
          (dto.selectedPurchaseOrder?.customer_id != null &&
              location.customer_id != null &&
              location.customer_id != dto.selectedPurchaseOrder?.customer_id);
    });
    LocationModel locationModel = new LocationModel();
    locationModel.id = "0";
    locationModel.dropDownDisplayName = "--Please Select--";
    locationModel.product_name = "";
    locationModel.product_id = "0";
    loadFromList.insert(0, locationModel); // Add empty record
  }

  _resetLocationList() {
    loadFromList = [];
    loadFromList.addAll(originalUnmodifiedLoadFromList);
  }

  onUOMChanged(String uom) {
    if (uom != "--Please Select--") {
      dto.selectedUom =
          uomUnitList.singleWhere((element) => element.unit == uom);
    } else {
      dto.selectedUom = null;
    }
    setState(() {});
  }

  onBlockToChanged(String block) {
    if (block != null && block != "--Please Select--") {
      dto.blockTo = blockToList
          .singleWhere((element) => element.railcarBlockToCodeName == block);
    } else {
      dto.blockTo = null;
    }
    setState(() {});
  }

  onAdditionalFieldChanged(String value, String id) {
    dto.additionalField.update(id, (e) => value, ifAbsent: () => value);
  }

  cancelPopUpDialog() async {
    final action = await CustomDialog.cstmDialog(
        context, "add_railcar", "Unsaved Changes", "");
    if (action[0] == DialogAction.yes) {
      Navigator.pop(context);
    }
  }

  refreshScreen() {
    setState(() {});
  }

  checkForProductMatchWithAssignedPO() async {
    CommonDao commonDao = new CommonDao();

    ProductModel productModel = await commonDao.fetchProductDetailsByProductId(
        product_id: dto.selectedProduct.productId);
    ProductModel assignedPOProductDetails =
        await commonDao.fetchProductDetailsByProductId(
            product_id: dto.selectedPurchaseOrder.productId);

    String selectedProductName = productModel.productName;
    String selectedProductState = productModel.dryOrLiquid;

    String assignedPOProductState = assignedPOProductDetails.dryOrLiquid;
    String assignedPOProductName = assignedPOProductDetails.productName;

    if (assignedPOProductState == APP_CONST.LIQUID &&
        selectedProductState == APP_CONST.LIQUID &&
        selectedProductName == assignedPOProductName) {
      return true;
    } else {
      return false;
    }
  }

  applyToSelection() {
    isApplyButtonClicked = true;
    final FormState form = _formKey.currentState;
    if (form.validate()) {
      if (dto.selectedProduct == null || dto.selectedProduct.productId == "0") {
        print(">>>>> form is invalid. (applyToSelection method- load screen )");
        formIsValid = false;
        _showToastError([]..add("Product cannot be empty."));
      }
      // else if ((dto.selectedPurchaseOrder.purchaseOrderNumber.isNotEmpty ||
      //         dto.selectedPurchaseOrder.purchaseOrderNumber != null) &&
      //     checkForProductMatchWithAssignedPO() == false) {
      //   _showToastError([]..add(
      //       "Assigned PO product details does not match with selected product"));
      // }
      else if (dto.selectedUom == null || dto.selectedUom.id == "0") {
        print(">>>>> form is invalid. (applyToSelection method- load screen )");
        formIsValid = false;
        _showToastError([]..add("Unit cannot be empty."));
      } else {
        dto.selectedEquipmentListSecondGrid.map((equipment) {
          Compartment compartment;
          if (equipment.compartmentList.isEmpty) {
            compartment = Compartment();
            equipment.compartmentList.add(compartment);
          } else {
            compartment = equipment.compartmentList[0];
          }
          compartment.productId = dto.selectedProduct.productId;
          compartment.productName = dto.selectedProduct.productName;
          compartment.purchaseOrderId = dto.selectedPurchaseOrder?.id;
          compartment.purchaseOrderName =
              dto.selectedPurchaseOrder?.purchaseOrderNumber;
          compartment.shipmentTypeId = dto.selectedShipmentTypeValue?.id;
          compartment.shipmentName = dto.shipmentText;
          compartment.loadFromId = dto.loadFrom?.id;
          compartment.loadFromName = dto.loadFrom?.name;
          compartment.loadFrom = dto.loadFrom;
          compartment.customer_id = dto.loadFrom
              ?.customer_id; //?? (dto.selectedPurchaseOrder?.customer_id); // check first purchase order , then location
          equipment.blockToName =
              dto.blockTo?.railcarBlockToCodeName ?? equipment.blockToName;
          equipment.blockToId = dto.blockTo?.id ?? equipment.blockToId;
          compartment.loadAmount = dto.loadAmount;
          compartment.loadDate = dto.loadedDateTime;
          compartment.uomId = dto.selectedUom.id;
          compartment.uomValue = dto.selectedUom.unit;
          compartment.additionalFields =
              dto.additionalField.entries.map((entry) {
            return AdditionFiled(entry.key, entry.value);
          }).toList();
        }).toList();

        formIsValid = true;
      }
    } else {
      print(">>>>> form is invalid. (applyToSelection method- load screen )");
      formIsValid = false;
    }
    setState(() {});
  }

  Future<void> save() async {
    if (formIsValid && isApplyButtonClicked) {
      Map<String, List<String>> map;
      try {
        map = await loadCarRepo.checkBeforeSavingLoad(dto, fcModel);
      } catch (err) {
        _showToastError([]..add("${err}"));
        return;
      }
      List<String> errorList = map["error"];
      List<String> warningList = map["warning"];

      if (errorList.isNotEmpty) {
        isApplyButtonClicked = false;
        _showToastError(errorList);
      } else if (warningList.isNotEmpty) {
        bool isOk = await confirmationMessage(warningList);
        if (isOk) {
          await loadCarRepo.saveLoad(dto);
//          showToastRecordsSavedSuccessfully();
          // scannedEquipList needs to be cleared inorder to clear the AEI grid after a operation is performed (Only applicable when navigating from AEI actions).
//          clearScannedEquipList();

          if (APP_CONST.AEI_SCAN == source) {
            Navigator.pop(context);
            BlocProvider.of<AEIScanBloc>(parentContext)
                .add(AEIScanFetchDataWithLastSetCriteria());
          } else if (APP_CONST.YARD_VIEW == source) {
            Navigator.pop(context);
            BlocProvider.of<YardViewBloc>(parentContext)
                .add(YardViewFetchDataWithLastSetCriteria());
          }  else if (APP_CONST.LOAD_VCF == source) {
            int count = 0;
            Navigator.of(context).popUntil((_) => count++ >= 2);
          } else {
//            Navigator.push(
//                context,
//                MaterialPageRoute(
//                    builder: (context) => ScreenFactory(ListViewScreen())));
            Navigator.pop(context);
            showToastRecordsSavedSuccessfully();
          }
//          showToastRecordsSavedSuccessfully();
        }
      } else {
        await loadCarRepo.saveLoad(dto);
//        showToastRecordsSavedSuccessfully();
        // scannedEquipList needs to be cleared inorder to clear the AEI grid after a operation is performed (Only applicable when navigating from AEI actions).
        clearScannedEquipList();
        if (APP_CONST.AEI_SCAN == source) {
          Navigator.pop(context);
          BlocProvider.of<AEIScanBloc>(parentContext)
              .add(AEIScanFetchDataWithLastSetCriteria());
        } else if (APP_CONST.YARD_VIEW == source) {
          Navigator.pop(context);
          BlocProvider.of<YardViewBloc>(parentContext)
              .add(YardViewFetchDataWithLastSetCriteria());
        } else if (APP_CONST.LOAD_VCF == source) {
          int count = 0;
          Navigator.of(context).popUntil((_) => count++ >= 2);
        } else {
//          Navigator.push(
//              context,
//              MaterialPageRoute(
//                  builder: (context) => ScreenFactory(ListViewScreen())));
          Navigator.pop(context);
        }
        showToastRecordsSavedSuccessfully();

      }
    } else {
      showToastApplyButtonNotClicked();
    }
  }

  clearScannedEquipList() {
    List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"];
    if (scannedEquipList != null) {
//      MemoryData.dataMap.removeWhere((key, value) => key == "scannedEquipList");
    }
  }

  void _showToastError(List<String> errorList) {
    Widget toast = Container(
        width: ScreenUtil().setWidth(552),
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
        decoration: BoxDecoration(
          color: Colors.redAccent,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: errorList.map((e) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.warning),
                SizedBox(
                  width: 12.0,
                ),
                Expanded(
                  child: Text(
                    e,
                    style: TextStyle(
                      // ignore: deprecated_member_use
                      color: Theme.of(context).textTheme.body1.color,
                    ),
                  ),
                ),
              ],
            );
          }).toList(),
        ));

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  void showToastApplyButtonNotClicked() {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Colors.red,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.warning),
          SizedBox(
            width: 12.0,
          ),
          Text(
            "Please apply selection to railcar(s) to proceed",
            style: TextStyle(
              // ignore: deprecated_member_use
              color: Theme.of(context).textTheme.body1.color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  void showToastRecordsSavedSuccessfully() {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Color(0xff7fae1b),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text(
            "Railcar Loaded Successfully",
            style: TextStyle(
              // ignore: deprecated_member_use
              color: Theme.of(context).textTheme.body1.color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

 Future<bool> confirmationMessage(List<String> warningList) async {
    String sealErrorText = "";
    for (String error in warningList) {
      sealErrorText += "$error\n";
    }
    bool returnValue = false;
    List<Object> returnList = await CustomDialog.cstmDialog(
        context,
        "confirm_to_continue",
        "There is at least one railcar with a warning",
        "$sealErrorText \nDo you wish to continue?");

    if (returnList[0] == DialogAction.yes) {
      returnValue = true;
    }
    return returnValue;
  }
}
