import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/model/load_railcar_dto.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class LoadRailCarTable extends StatefulWidget {
  LoadRailCarDTO dto;
  Function() refreshScreen;

  LoadRailCarTable(LoadRailCarDTO loadRailCarDTO, Function() refreshScreen) {
    this.dto = loadRailCarDTO;
    this.refreshScreen = refreshScreen;
  }

  @override
  _LoadRailCarTableState createState() => _LoadRailCarTableState();
}

class _LoadRailCarTableState extends State<LoadRailCarTable> {
  bool sortAsc = true;


  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: CustomDataTable(
              rowColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff1d2e40) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xffffffff) : null,
              columnColor: Color(0xff274060),
              checkboxBorderColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xffffffff) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Colors.blueGrey : null,
              sortColumnIndex: 0,
              sortArrowColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xffffffff) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xffffffff) : null,
              sortAscending: sortAsc,
              onSelectAll: (c) {
                if (c) {
                  widget.dto.selectedEquipmentListSecondGrid.addAll(widget.dto.selectedEquipmentList);
                } else {
                  widget.dto.selectedEquipmentListSecondGrid.removeRange(0, widget.dto.selectedEquipmentListSecondGrid.length);
                }
                widget.refreshScreen();
              },
              columns: [
                CustomDataColumn(
                    onSort: (int columnIndex, bool ascending) {
                      widget.dto.selectedEquipmentList.sort((a, b) {
                        return ascending
                            ? (a.equipmentInitial + a.equipmentNumber).compareTo(b.equipmentInitial + b.equipmentNumber)
                            : (a.equipmentInitial + a.equipmentNumber).compareTo(b.equipmentInitial + b.equipmentNumber) * -1;
                      });
                      sortAsc = ascending;
                      widget.refreshScreen();
                    },
                    label: Text(
                      'RAILCAR',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Roboto',
                        fontSize: ScreenUtil().setHeight(14),
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        letterSpacing: ScreenUtil().setWidth(1.43),
                      ),
                    ),
                    numeric: false,
                    tooltip: 'Product'),
                CustomDataColumn(
                    label: Text('PRODUCT',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    tooltip: 'Product'),
                CustomDataColumn(
                    label: Text('PURCHASE ORDER',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    tooltip: 'Purchase Order'),
                CustomDataColumn(
                    label: Text(
                      'LOAD FROM',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Roboto',
                        fontSize: ScreenUtil().setHeight(14),
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        letterSpacing: ScreenUtil().setWidth(1.43),
                      ),
                    ),
                    numeric: false,
                    tooltip: 'Load From'),
                CustomDataColumn(
                    label: Text('LOAD AMT',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    tooltip: 'Load Amount'),
                CustomDataColumn(
                    label: Text('CURRENT AMT',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    tooltip: 'Current Amount'),
                CustomDataColumn(
                    label: Text('BLOCK TO CODE',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    tooltip: 'UOM'),
                CustomDataColumn(
                    label: Text('UOM',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    numeric: false,
                    tooltip: 'UOM'),
              ],
              rows: widget.dto.selectedEquipmentList?.map((railcar) {
                    Compartment compartment;
                    if (railcar.compartmentList.isEmpty) {
                      compartment = Compartment();
                      railcar.compartmentList.add(compartment);
                    } else {
                      compartment = railcar.compartmentList[0];
                    }

                    return CustomDataRow(
                        selected: widget.dto.selectedEquipmentListSecondGrid.contains(railcar),
                        onSelectChanged: (c) {
                          if (c) {
                            widget.dto.selectedEquipmentListSecondGrid.add(railcar);
                          } else {
                            widget.dto.selectedEquipmentListSecondGrid.remove(railcar);
                          }
                          widget.refreshScreen();
                        },
                        cells: [
                          CustomDataCell(Text(railcar.equipmentInitial + " " + railcar.equipmentNumber,
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(16),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                letterSpacing: ScreenUtil().setWidth(0.5),
                              ))),
                          CustomDataCell(Text(compartment.productName ?? "-",
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(16),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                letterSpacing: ScreenUtil().setWidth(0.5),
                              ))),
                          CustomDataCell(Text(compartment.purchaseOrderName ?? "-",
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(16),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                letterSpacing: ScreenUtil().setWidth(0.5),
                              ))),
                          CustomDataCell(Text(compartment.loadFromName ?? "-",
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(16),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                letterSpacing: ScreenUtil().setWidth(0.5),
                              ))),
                          CustomDataCell(
                            GestureDetector(
                              onTap: () {},
                              child: Text(double.parse(compartment.loadAmount ?? "0").toStringAsFixed(2).toString(),
                                  style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                        // ignore: deprecated_member_use
                                        .body1
                                        .color,
                                    fontFamily: 'Roboto',
                                    fontSize: ScreenUtil().setHeight(16),
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                    letterSpacing: ScreenUtil().setWidth(0.5),
                                  )),
                            ),
                            //showEditIcon: true,
                            placeholder: true,
                          ),
                          CustomDataCell(Text(
                            double.parse(compartment.currentAmount ?? "0").toStringAsFixed(2).toString(),
                            style: TextStyle(
                              color:
                                  // ignore: deprecated_member_use
                                  Theme.of(context).textTheme.body1.color,
                              fontFamily: 'Roboto',
                              fontSize: ScreenUtil().setHeight(16),
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal,
                              letterSpacing: ScreenUtil().setWidth(0.5),
                            ),
                          )),
                          CustomDataCell(Text(railcar.blockToName ?? "-",
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(16),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                letterSpacing: ScreenUtil().setWidth(0.5),
                              ))),
                          CustomDataCell(Text(compartment.uomValue ?? "-",
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(16),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                letterSpacing: ScreenUtil().setWidth(0.5),
                              )))
                        ]);
                  })?.toList() ??
                  [],
            )));
  }
}
