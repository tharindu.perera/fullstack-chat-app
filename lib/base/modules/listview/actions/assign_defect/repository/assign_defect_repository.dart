import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/dao/assign_defect_dao.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/model/assigned_defect.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/model/railcar_defect.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';

class AssignDefectRepository {
  AssignDefectDAO assignDefectDAO = new AssignDefectDAO();
  NotificationRepository notificationRepository = new NotificationRepository();

  Future<void> saveDefect(List<Equipment> selectedCarList, String selectedDefectedId,String defectName) async {
    AssignedDefect assignedDefect = new AssignedDefect();
    assignedDefect.equipmentId = selectedCarList[0].activeAssetId;
    assignedDefect.asset_master_id = selectedCarList[0].assetMasterId;
    assignedDefect.railcarDefectId = selectedDefectedId;
    assignedDefect.facilityId = env.userProfile.facilityId;
    assignedDefect.transactionStatus = APP_CONST.TRANSACTION_STATUS_PENDING;
    assignedDefect.source = APP_CONST.MOBILITY;
    assignedDefect.operationType = APP_CONST.ASSIGN_DEFECT_OPERATION;
    assignedDefect.permission_id = env.userProfile.id;
    assignedDefect.createdDt = new DateFormat("yyyy-MM-dd hh:mm:ss").format(DateTime.now());
    assignedDefect.user = env.userProfile.userName;
    assignedDefect.equipmentInitAndNumber = selectedCarList[0].equipmentInitial+" "+selectedCarList[0].equipmentNumber;
    assignedDefect.defect_name = defectName;

    assignDefectDAO.add(assignedDefect);
    notificationRepository.addNotification(assignedDefect, APP_CONST.ASSIGN_DEFECT_OPERATION);
  }

  Future<List<RailcarDefect>> fetchRailcarDefects() async {
    return await assignDefectDAO.fetchRailcarDefects();
  }

  Future<List<AssignedDefect>> fetchRailcarDefectsByRailcar(String assetMasterId) async {
    return await assignDefectDAO.fetchRailcarDefectsByRailcar(assetMasterId);
  }
}
