import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/util/date_time_util.dart';

AssignedDefect assignedBlockFromMap(String str) => AssignedDefect.fromMap(json.decode(str));

String assignedBlockToMap(AssignedDefect data) => json.encode(data.toMap());

class AssignedDefect {
  AssignedDefect(
      {this.equipmentId, this.asset_master_id,this.defect_name, this.railcarDefectId, this.facilityId, this.createdDt, this.source, this.transactionStatus, this.permission_id, this.id, this.operationType, this.user});

  String equipmentId;
  String railcarDefectId;
  String facilityId;
  String createdDt;
  String source;
  String transactionStatus;
  String permission_id;
  String id;
  String operationType;
  String user;
  String defect_name;
  String equipmentInitAndNumber;
  String asset_master_id;

  DocumentReference reference;

  factory AssignedDefect.fromMap(Map<String, dynamic> json) => AssignedDefect(
        id: json["id"] == null ? null : json["id"],
        asset_master_id: json["asset_master_id"] == null ? null : json["asset_master_id"],
        defect_name: json["defect_name"] == null ? null : json["defect_name"],
        equipmentId: json["equipment_id"] == null ? null : json["equipment_id"],
        railcarDefectId: json["railcar_defect_id"] == null ? null : json["railcar_defect_id"],
        facilityId: json["facility_id"] == null ? "facility_id" : json["facility_id"],
        createdDt: json["created_dt"] == null ? null : getLocalDateTimeFromUTCStr(json["created_dt"]).toString(),
        source: json["source"] == null ? null : json["source"],
        transactionStatus: json["transaction_status"] == null ? null : json["transaction_status"],
        user: json["user"] == null ? null : json["user"],
        operationType: json["operation_type"] == null ? null : json["operation_type"],
      );




  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "asset_master_id": asset_master_id == null ? null : asset_master_id,
        "defect_name": defect_name == null ? null : defect_name,
        "equipment_id": equipmentId == null ? null : equipmentId,
        "railcar_defect_id": railcarDefectId == null ? null : railcarDefectId,
        "facility_id": facilityId == null ? null : facilityId,
        "created_dt": createdDt == null ? null : getUTCTimeFromLocalDateTimeStr(createdDt).toString(),
        "source": source == null ? null : source,
        "transaction_status": transactionStatus == null ? null : transactionStatus,
        "operation_type": operationType == null ? null : operationType,
        "user": user == null ? null : user,
      };

  @override
  String toString() {
    return 'AssignedDefect{equipmentId: $equipmentId, railcarDefectId: $railcarDefectId, facilityId: $facilityId, createdDt: $createdDt, source: $source, transactionStatus: $transactionStatus, permission_id: $permission_id, id: $id, operationType: $operationType, user: $user, defect_name: $defect_name, equipmentInitAndNumber: $equipmentInitAndNumber, asset_master_id: $asset_master_id, reference: $reference}';
  }


}
