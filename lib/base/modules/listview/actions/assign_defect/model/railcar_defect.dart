import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

RailcarDefect railcarBlockFromMap(String str) =>
    RailcarDefect.fromMap(json.decode(str));

String railcarBlockMap(RailcarDefect data) => json.encode(data.toMap());

class RailcarDefect {
  RailcarDefect(
      {this.clientId,
      this.railcarDefectName,
      this.createdDt,
      this.updatedDt,
      this.id});

  String clientId;
  String railcarDefectName;
  String createdDt;
  String updatedDt;
  String id;

  DocumentReference reference;

  RailcarDefect.fromMap(Map<String, dynamic> map, {this.reference})
      : clientId = map['client_id'],
        railcarDefectName = map['railcar_defect_name'],
        id = map['id'];

  RailcarDefect.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data(), reference: snapshot.reference);

  Map<String, dynamic> toMap() => {
        "client_id": clientId == null ? null : clientId,
        "railcar_defect_name":
            railcarDefectName == null ? null : railcarDefectName,
        "created_dt": createdDt == null ? null : createdDt,
        "updated_dt": updatedDt == null ? null : updatedDt,
        "id": id == null ? null : id,
      };
}
