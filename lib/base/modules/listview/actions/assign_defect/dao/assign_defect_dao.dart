import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/model/assigned_defect.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/model/railcar_defect.dart';
import 'package:mbase/env.dart';
import 'package:uuid/uuid.dart';

class AssignDefectDAO extends BaseDao {
  AssignDefectDAO() : super("assigned_defect");

  Future<void> add(AssignedDefect assignedDefect) async {
    assignedDefect.id = Uuid().v1();
     collection.doc(assignedDefect.id).set(assignedDefect.toMap());
  }

  Future<List<RailcarDefect>> fetchRailcarDefects() async {
    print("start fetchRailcarDefect() in AssignedDefectDAO");
    FirebaseFirestore firebaseFirestore = DatabaseProvider.firestore;
    CollectionReference railcarDefectCollection = firebaseFirestore.collection("railcar_defect");
    railcarDefectCollection.orderBy("railcar_defect_name", descending: true);

    return (await railcarDefectCollection
        .where("client_id", isEqualTo: env.userProfile.clientId)
        .where("facility_id",isEqualTo: env.userProfile.facilityId)
        
        .get()).docs.map((snapshot) {
      return RailcarDefect.fromSnapshot(snapshot);
    }).toList();
  }

  Future<List<AssignedDefect>> fetchRailcarDefectsByRailcar(String assetMasterId) async {
    print("Fetching Railcar Defects By Railcar assetMasterId ......");

    FirebaseFirestore firebaseFirestore = DatabaseProvider.firestore;
    CollectionReference railcarDefectCollection = firebaseFirestore.collection("assigned_defect");
    railcarDefectCollection.orderBy("asset_master_id");

    return (await railcarDefectCollection
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .where("permission_id", whereIn: [env.userProfile.id, APP_CONST.PUBLIC])
        .where("asset_master_id", isEqualTo: assetMasterId)
        .get()).docs.map((snapshot) {
      return AssignedDefect.fromMap(snapshot.data());
    }).toList();
  }
  Future<List<AssignedDefect>> fetchRailcarDefectsByRailcarForFlyover(String assetMasterId) async {
    print("Fetching Railcar Defects By Railcar assetMasterId ......");

    FirebaseFirestore firebaseFirestore = DatabaseProvider.firestore;
    CollectionReference railcarDefectCollection = firebaseFirestore.collection("assigned_defect");

    return (await railcarDefectCollection
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .where("permission_id", whereIn: [env.userProfile.id, APP_CONST.PUBLIC])
        .where("asset_master_id", isEqualTo: assetMasterId).orderBy("created_dt",descending: true)
        .get()).docs.map((snapshot) {
      return AssignedDefect.fromMap(snapshot.data());
    }).toList();
  }
}
