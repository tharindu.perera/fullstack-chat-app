import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/model/railcar_defect.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/repository/assign_defect_repository.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_bloc.dart';
import 'package:mbase/base/modules/listview/yardview/bloc/yardview_event.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class AssignDefect extends StatefulWidget {
  List<Equipment> selectedEquipments = List<Equipment>();
  String source;

  AssignDefect(List<Equipment> selectedRow, String source) {
    this.selectedEquipments = selectedRow;
    this.source = source;
  }

  @override
  _AssignDefectState createState() =>
      _AssignDefectState(selectedEquipments, source);
}

class _AssignDefectState extends State<AssignDefect> {
  AssignDefectRepository defectRepository = new AssignDefectRepository();

  FToast fToast;
  String selectedDefects;
  bool isBlockSelected = false;
  final globalKey = GlobalKey<ScaffoldState>();
  List<RailcarDefect> railcarDefectList = [];
  RailcarDefect selectedRadio;
  List<Equipment> selectedEquipments = List<Equipment>();
  String selectedEquipment = "";
  bool selectedTemp = false;
  String source;

  _AssignDefectState(List<Equipment> selectedEquipments, String source) {
    this.selectedEquipments = selectedEquipments;
    this.source = source;
    selectedEquipment = selectedEquipments[0].equipmentInitial +
        " " +
        selectedEquipments[0].equipmentNumber;
  }

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);
    fetchRailcarDefects();
  }

  fetchRailcarDefects() async {
    defectRepository.fetchRailcarDefects().then((value) {
      value.forEach((element) {
        railcarDefectList.add(element);
      });
      railcarDefectList.sort((a,b){
        return a.railcarDefectName.toLowerCase().compareTo(b.railcarDefectName.toLowerCase());
      });
      setState(() {});
    });
  }

  setSelectedRadio(int value) {
    setState(() {
      selectedRadio = railcarDefectList[value];
      selectedTemp = true;
    });
  }

  clearScannedEquipList() {
    List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"];
    if (scannedEquipList != null) {
//      MemoryData.dataMap.removeWhere((key, value) => key == "scannedEquipList");
    }
  }

  assignDefect() async {
    try {
      String selectedDefectId;
      String defectName;

      if (selectedRadio.railcarDefectName != null) {
        railcarDefectList.forEach((element) {
          if (element.railcarDefectName == selectedRadio.railcarDefectName) {
            selectedDefectId = element.id;
            defectName = element.railcarDefectName;
          }
        });
      }
      defectRepository
          .saveDefect(selectedEquipments, selectedDefectId, defectName)
          .then((value) {
        // scannedEquipList needs to be cleared inorder to clear the AEI grid after a operation is performed (Only applicable when navigating from AEI actions).
        clearScannedEquipList();
        _showToast();
        Navigator.pop(context);
        if (APP_CONST.AEI_SCAN == source) {
          BlocProvider.of<AEIScanBloc>(context)
              .add(AEIScanFetchDataWithLastSetCriteria());
        } else if (APP_CONST.YARD_VIEW == source) {
          BlocProvider.of<YardViewBloc>(context)
              .add(YardViewFetchDataWithLastSetCriteria());
        } else {
          BlocProvider.of<ListViewBloc>(context)
              .add(ListViewFetchDataWithLastSetCriteria());
        }
      });
    } catch (e) {
      print('Exception : ${e}');
    }
  }

  _showToast() {
    fToast.showToast(
      child: CustomToast(
        toastColor: Color(0xff7fae1b),
        toastMessage: "Defect assigned to railcar successfully",
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);

    return Scaffold(
        key: globalKey,
        appBar: MainAppBar(),
        drawer: AppDrawer(),
        body: Form(
          autovalidate: true,
          child: FormUI(),
        ));
  }

  Widget FormUI() {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    return Builder(
      builder: (context) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(15.0),
                child: CardUI(
                    content: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(20),
                                  child: Container(
                                      child: ListTile(
                                    leading: GestureDetector(
                                        onTap: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Icon(
                                          Icons.arrow_back,
                                        )),
                                    title: Text('Assign Defect',
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(24,
                                                allowFontScalingSelf: true),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'Roboto',
                                            color: Theme.of(context)
                                                .textTheme
                                                // ignore: deprecated_member_use
                                                .headline
                                                .color)),
                                  ))),
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        child: GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: RichText(
                                        text: TextSpan(
                                            style: Theme.of(context)
                                                .textTheme
                                                // ignore: deprecated_member_use
                                                .body1,
                                            children: <InlineSpan>[
                                              TextSpan(
                                                  text: 'CANCEL',
                                                  style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(
                                                          14,
                                                          allowFontScalingSelf:
                                                              true),
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      letterSpacing:
                                                          ScreenUtil()
                                                              .setWidth(1.25),
                                                      fontFamily: 'Roboto',
                                                      color: Color(0xFF3e8aeb)))
                                            ]),
                                      ),
                                    ))),
                                Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Container(
                                        width: ScreenUtil().setWidth(180),
                                        height: ScreenUtil().setHeight(48),
                                        child: FlatButton(
                                            onPressed: selectedTemp
                                                ? assignDefect
                                                : null,
                                            child: Text('ASSIGN DEFECT',
                                                style: TextStyle(
                                                    fontFamily: 'Roboto',
                                                    fontSize: ScreenUtil()
                                                        .setHeight(14),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(1.25),
                                                    color: Colors.white)),
                                            color: Color(0xFF3e8aeb),
                                            textColor: Colors.white,
                                            disabledColor: Colors.grey,
                                            disabledTextColor: Colors.black,
                                            splashColor: Color(0xFF3e8aeb)))),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                    Divider(
                      thickness: 5.0,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(20),
                              child: Container(
                                  width: 300,
                                  padding: EdgeInsets.symmetric(vertical: 4.0),
                                  alignment: Alignment.center,
                                  child: ListTile(
                                    title: Text(
                                      'Railcar',
                                      style: Theme.of(context)
                                          .textTheme
                                          // ignore: deprecated_member_use
                                          .body2,
                                    ),
                                    subtitle: Text(
                                      selectedEquipment,
                                      style:
                                          Theme.of(context).textTheme.headline5,
                                    ),
                                  )),
                            ),
                          ],
                        ),
                      ],
                    ),
                    // Divider(
                    //   thickness: 2.0,
                    // ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                        width: 300,
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.0),
                                        alignment: Alignment.center,
                                        child: ListTile(
                                          title: Text(
                                            'Defect',
                                            style: Theme.of(context)
                                                .textTheme
                                                // ignore: deprecated_member_use
                                                .body2,
                                          ),
                                        )),

                                    // Row(
                                    //   children: <Widget>[getRadioWidgets()],
                                    // ),
                                  ],
                                )),
                          ],
                        ),
                      ],
                    ),
                    Wrap(
                      direction: Axis.horizontal,
                      spacing: ScreenUtil().setWidth(10),
                      runSpacing: ScreenUtil().setHeight(10),
                      children: railcarDefectList.map((e) {
                        return Container(
                          width: ScreenUtil().setWidth(200),
                          child: ListTile(
                            leading: Radio(
                              value: railcarDefectList.indexOf(e),
                              groupValue: selectedRadio == null
                                  ? null
                                  : railcarDefectList.indexOf(selectedRadio),
                              activeColor: Color(0xFF3e8aeb),
                              onChanged: (val) {
                                print("Radio onChanged " + val.toString());
                                setSelectedRadio(val);
                              },
                            ),
                            title: Text(
                              e.railcarDefectName,
                              style: Theme.of(context).textTheme.bodyText2,
                            ),
                          ),
                        );
                      }).toList(),
                    ),
                  ],
                )),
              ),
            )
          ],
        );
      },
    );
  }
}
