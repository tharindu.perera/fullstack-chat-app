import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/listview/actions/unloadcar/model/unload_railcar_dto.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class UnLoadRailCarTable extends StatefulWidget {
  Function() refreshScreen;
  UnLoadRailCarDTO dto;

  UnLoadRailCarTable(UnLoadRailCarDTO loadRailCarDTO, Function() refreshScreen) {
    this.dto = loadRailCarDTO;
    this.refreshScreen = refreshScreen;
  }

  @override
  _UnLoadRailCarTableState createState() => _UnLoadRailCarTableState();
}

class _UnLoadRailCarTableState extends State<UnLoadRailCarTable> {
  int _sortColumnIndex = 0;

  bool _sortAscending = true;

  onSortColumn(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        widget.dto.selectedEquipmentList.sort((a, b) => a.equipmentNumber.compareTo(b.equipmentNumber));
        setState(() {});
      } else {
        widget.dto.selectedEquipmentList.sort((a, b) => b.equipmentNumber.compareTo(a.equipmentNumber));
        setState(() {});
      }
    }

    if (columnIndex == 1) {
      if (ascending) {
        widget.dto.selectedEquipmentList.sort((a, b) => a.compartmentList.first.productName.compareTo(b.compartmentList.first.productName));
        setState(() {});
      } else {
        widget.dto.selectedEquipmentList.sort((a, b) => b.compartmentList.first.productName.compareTo(a.compartmentList.first.productName));
        setState(() {});
      }
    }

    if (columnIndex == 2) {
      if (ascending) {
        widget.dto.selectedEquipmentList.sort((a, b) => a.compartmentList.first.unload_to_name.compareTo(b.compartmentList.first.unload_to_name));
        setState(() {});
      } else {
        widget.dto.selectedEquipmentList.sort((a, b) => b.compartmentList.first.unload_to_name.compareTo(a.compartmentList.first.unload_to_name));
        setState(() {});
      }
    }

    if (columnIndex == 3) {
      if (ascending) {
        widget.dto.selectedEquipmentList.sort((a, b) => a.blockToName.compareTo(b.blockToName));
        setState(() {});
      } else {
        widget.dto.selectedEquipmentList.sort((a, b) => b.blockToName.compareTo(a.blockToName));
        setState(() {});
      }
    }

    if (columnIndex == 4) {
      if (ascending) {
        widget.dto.selectedEquipmentList.sort((a, b) => a.compartmentList.first.unload_to_name.compareTo(b.compartmentList.first.unload_to_name));
        setState(() {});
      } else {
        widget.dto.selectedEquipmentList.sort((a, b) => b.compartmentList.first.unload_to_name.compareTo(a.compartmentList.first.unload_to_name));
        setState(() {});
      }
    }

    if (columnIndex == 5) {
      if (ascending) {
        widget.dto.selectedEquipmentList.sort((a, b) => a.compartmentList.first.currentAmount.compareTo(b.compartmentList.first.currentAmount));
        setState(() {});
      } else {
        widget.dto.selectedEquipmentList.sort((a, b) => b.compartmentList.first.currentAmount.compareTo(a.compartmentList.first.currentAmount));
        setState(() {});
      }
    }

    if (columnIndex == 6) {
      if (ascending) {
        widget.dto.selectedEquipmentList.sort((a, b) => a.compartmentList.first.uomValue.compareTo(b.compartmentList.first.uomValue));
        setState(() {});
      } else {
        widget.dto.selectedEquipmentList.sort((a, b) => b.compartmentList.first.uomValue.compareTo(a.compartmentList.first.uomValue));
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: CustomDataTable(
              sortArrowColor: Colors.white,
              checkboxBorderColor:
                  _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color.fromRGBO(0, 0, 0, 0.54) : null,
              rowColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff1d2e40) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xffffffff) : null,
              columnColor: Color(0xff274060),
              // _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color.fromRGBO(0, 0, 0, 0.54) : null,
              sortAscending: _sortAscending,
              sortColumnIndex: _sortColumnIndex,
              onSelectAll: (c) {
                if (c) {
                  widget.dto.selectedEquipmentListSecondGrid.addAll(widget.dto.selectedEquipmentList);
                } else {
                  widget.dto.selectedEquipmentListSecondGrid.removeRange(0, widget.dto.selectedEquipmentListSecondGrid.length);
                }
                widget.refreshScreen();
              },
              columns: [
                CustomDataColumn(
                    label: Text('RAILCAR',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'RAIL CAR'),
                CustomDataColumn(
                    label: Text('PRODUCT',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'Product'),
                CustomDataColumn(
                    label: Text(
                      'UNLOAD TO',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Roboto',
                        fontSize: ScreenUtil().setHeight(14),
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        letterSpacing: ScreenUtil().setWidth(1.43),
                      ),
                    ),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'UNLOAD TO'),
                CustomDataColumn(
                    label: Text('BLOCK TO',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'BLOCK TO'),
                CustomDataColumn(
                    label: Text('UNLOAD AMT',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'UNLOAD Amount'),
                CustomDataColumn(
                    label: Text('CURRENT AMT',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'Current Amount'),
                CustomDataColumn(
                    label: Text('UOM',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'UOM'),
              ],

              rows: widget.dto.selectedEquipmentList?.map((loadRailcarModel) {
                    Compartment compartment;
                    if (loadRailcarModel.compartmentList.isEmpty) {
                      compartment = Compartment();
                      loadRailcarModel.compartmentList.add(compartment);
                    } else {
                      compartment = loadRailcarModel.compartmentList[0];
                    }
                    return CustomDataRow(
                        selected: widget.dto.selectedEquipmentListSecondGrid.contains(loadRailcarModel),
                        onSelectChanged: (c) {
                          if (c) {
                            widget.dto.selectedEquipmentListSecondGrid.add(loadRailcarModel);
                          } else {
                            widget.dto.selectedEquipmentListSecondGrid.remove(loadRailcarModel);
                          }
                          widget.refreshScreen();
                        },
                        cells: [
                          CustomDataCell(Text(loadRailcarModel.equipmentInitial + " " + loadRailcarModel.equipmentNumber,
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(16),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                letterSpacing: ScreenUtil().setWidth(0.5),
                              ))),
                          CustomDataCell(Text(compartment.productName ?? "-",
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(16),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                letterSpacing: ScreenUtil().setWidth(0.5),
                              ))),
                          CustomDataCell(Text(compartment.unload_to_name ?? "-",
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(16),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                letterSpacing: ScreenUtil().setWidth(0.5),
                              ))),
                          CustomDataCell(
                            GestureDetector(
                              onTap: () {},
                              child: Text(loadRailcarModel.blockToName ?? "-",
                                  style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                        // ignore: deprecated_member_use
                                        .body1
                                        .color,
                                    fontFamily: 'Roboto',
                                    fontSize: ScreenUtil().setHeight(16),
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                    letterSpacing: ScreenUtil().setWidth(0.5),
                                  )),
                            ),
                            //showEditIcon: true,
                            placeholder: true,
                          ),
                          CustomDataCell(
                            GestureDetector(
                              onTap: () {
//                                editLoad(loadRailcarModel);
                              },
                              child: Text(double.parse(compartment.unload_amount ?? "0").toStringAsFixed(2).toString(),
                                  style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                        // ignore: deprecated_member_use
                                        .body1
                                        .color,
                                    fontFamily: 'Roboto',
                                    fontSize: ScreenUtil().setHeight(16),
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                    letterSpacing: ScreenUtil().setWidth(0.5),
                                  )),
                            ),
                            //showEditIcon: true,
                            placeholder: true,
                          ),
                          CustomDataCell(Text(
                            double.parse(compartment.currentAmount ?? "0").toStringAsFixed(2).toString(),
                            style: TextStyle(
                              color:
                                  // ignore: deprecated_member_use
                                  Theme.of(context).textTheme.body1.color,
                              fontFamily: 'Roboto',
                              fontSize: ScreenUtil().setHeight(16),
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal,
                              letterSpacing: ScreenUtil().setWidth(0.5),
                            ),
                          )),
                          CustomDataCell(Text(MemoryData.uomMap[compartment.uomId].unit ?? "-",
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setHeight(16),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                letterSpacing: ScreenUtil().setWidth(0.5),
                              )))
                        ]);
                  })?.toList() ??
                  [],
            )));
  }
}
