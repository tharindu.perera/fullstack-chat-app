import 'dart:async';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/ReasonCode.dart';
import 'package:mbase/base/core/common_base/model/additional_field.dart';
import 'package:mbase/base/core/common_base/model/assigned_entry_field.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/facility_configuration_model.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/location_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown-generic.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/railcar_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/repository/assign_block_repository.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/repository/loadcar_repository.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/ui/load_railcar_table.dart';
import 'package:mbase/base/modules/listview/actions/unloadcar/model/unload_railcar_dto.dart';
import 'package:mbase/base/modules/listview/actions/unloadcar/repository/unloadcar_repository.dart';
import 'package:mbase/base/modules/listview/actions/unloadcar/ui/unload_railcar_table.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class UnLoadRailCar extends StatefulWidget {
  List<Equipment> selectedEquipments = List<Equipment>();
  bool populateVCF;
  String unloadAmount;

  UnLoadRailCar(List<Equipment> selectedEquipments,
      [bool populateVCF, String unloadAmount]) {
    this.selectedEquipments = selectedEquipments;
    this.populateVCF = (null != populateVCF) ? populateVCF : false;
    this.unloadAmount = unloadAmount;
  }

  @override
  _UnLoadRailCarState createState() =>
      _UnLoadRailCarState(selectedEquipments, populateVCF, unloadAmount);
}

class _UnLoadRailCarState extends State<UnLoadRailCar> {
  UnLoadRailCarDTO dto = UnLoadRailCarDTO();
  var unloadCarRepo = UnLoadCarRepo();
  var assignBlockRepository = AssignBlockRepository();
  int selectedCarCount = 0;
  final globalKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

//  final _unloadDateController = TextEditingController();
  final dateFormat = new DateFormat('MM/dd/yyyy HH:mm');

  // var formatter = new DateFormat('yyyy-MM-dd');
  LoadRailCarTable loadRailCarTable;
  FToast fToast;
  FacilityConfigurationModel fcModel = MemoryData.facilityConfiguration;
  bool isApplyButtonClicked = false;
  bool formIsValid = false;
  bool populateVCF = false;
  String unloadAmount = null;
  List<LocationModel> unloadFromList = [];
  List <ReasonCode> reasonCodeList =[];
  List<RailcarBlock> blockToList = [];
  List<AssignedEntryField> assignedEntryFields = [];
  ThemeChanger _themeChanger;
  bool markAsEmptyValue = false;
  final TextEditingController _unloadAmountController =
      new TextEditingController();

  _UnLoadRailCarState(List<Equipment> selectedEquipments, bool populateVCF,
      String unloadAmount) {
    for (Equipment equipment in selectedEquipments) {
      this.dto.selectedEquipmentList.add(Equipment.fromMap(equipment.toMap()));
    }
    this.populateVCF = populateVCF;
    this.unloadAmount = unloadAmount;
  }

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);
    Loader.show(context);

    Future(() async {
      selectedCarCount = await dto.selectedEquipmentList.length;
      unloadFromList = await unloadCarRepo.fetchLocations().catchError((err) {
        print("Error >>>> ${err}");
        _showToastError([]..add(err.toString()));
        return Future.value(List<LocationModel>());
      });

      await fetchAssignedBlocksForRailcar();
   reasonCodeList = await unloadCarRepo.fetchReasonCodes();

      List<LocationModel> refinedLocList = [];

      unloadFromList.forEach((loc) {
        bool locationFoundForCompartmentProduct = false;
        dto.selectedEquipmentList.forEach((eq) {
          if (loc.product_id == eq.compartmentList[0].productId &&
              eq.compartmentList[0].customer_id == loc.customer_id) {
            locationFoundForCompartmentProduct = true;
          } else if (loc.product_id == null &&
              eq.compartmentList[0].customer_id == loc.customer_id) {
            locationFoundForCompartmentProduct = true;
          } else if (loc.customer_id == null &&
              eq.compartmentList[0].productId == loc.product_id) {
            locationFoundForCompartmentProduct = true;
          } else if (loc.customer_id == null &&
              loc.product_id == null &&
              MemoryData.uomMap[loc.measure_id].dryOrLiquid ==
                  MemoryData.uomMap[eq.compartmentList[0].uomId].dryOrLiquid) {
            locationFoundForCompartmentProduct = true;
          }
        });
        locationFoundForCompartmentProduct == true
            ? refinedLocList.add(loc)
            : null;
      });
      refinedLocList.sort((a, b) => a.dropDownDisplayName
          .toLowerCase()
          .compareTo(b.dropDownDisplayName.toLowerCase()));

      unloadFromList = refinedLocList;

      blockToList = await assignBlockRepository.fetchRailcarBlocks();
      blockToList.sort((a, b) => a.railcarBlockToCodeName
          .toLowerCase()
          .compareTo(b.railcarBlockToCodeName.toLowerCase()));

      assignedEntryFields = await unloadCarRepo.fetchAdditionalFields();
      dto.unloadDateTime = DateTime.now();
      if (this.populateVCF) {
        autoPopulateVCFData();
      }
      Loader.hide();
      setState(() {});
    });
  }

  fetchAssignedBlocksForRailcar() async {
    Map<String, String> resultMap = {};
    this.dto.selectedEquipmentList.forEach((element) async {
      resultMap = await new LoadCarRepo().fetchBlockToCodesForSelectedRailcars(
          facility_visit_id: element.facilityVisitId,
          facilityId: element.facilityId);
      print("block to resultMap --------- ${resultMap}");
      if (resultMap != null) {
        element?.blockToId = resultMap["blockToId"];
        element?.blockToName = resultMap["blockToName"];
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);
    _themeChanger = Provider.of<ThemeChanger>(context);
    bool showMarkAsEmpty =
        "True" == fcModel.showMarkAsEmptyDuringUnloadingRailcar;

    Widget formUI() {
      return CardUI(
          content: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Container(
                        child: ListTile(
                      leading: GestureDetector(
                          onTap: () {
                            cancelPopUpDialog();
                          },
                          child: Icon(
                            Icons.arrow_back,
                          )),
                      title: Text('Unload Railcars ($selectedCarCount)',
                          style: TextStyle(
                              fontSize: ScreenUtil()
                                  .setSp(24, allowFontScalingSelf: true),
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Roboto',
                              color: Theme.of(context)
                                  .textTheme
                                  // ignore: deprecated_member_use
                                  .headline
                                  .color)),
                    ))),
              ),
              Row(
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.all(20),
                      child: Container(
                          child: GestureDetector(
                        onTap: () {
                          cancelPopUpDialog();
                        },
                        child: RichText(
                          text: TextSpan(
                              style: Theme.of(context)
                                  .textTheme
                                  // ignore: deprecated_member_use
                                  .body1,
                              children: <InlineSpan>[
                                TextSpan(
                                    text: 'CANCEL',
                                    style: TextStyle(
                                        fontFamily: 'Roboto',
                                        fontSize: ScreenUtil().setHeight(14),
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing:
                                            ScreenUtil().setWidth(1.25),
                                        color: Color(0xFF3e8aeb)))
                              ]),
                        ),
                      ))),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 0, top: 14, right: 24, bottom: 14),
                      child: Container(
                        width: ScreenUtil().setWidth(180),
                        height: ScreenUtil().setHeight(48),
                        child: FlatButton(
                            onPressed: () {
                              unloadRailCar()
                                  .then((value) => null)
                                  .catchError((error) {
                                _showToasties(
                                    message: "Application Error: ${error}",
                                    color: Colors.red);
                              });
                            }
                            // isApplyButtonClicked
                            //     ? () {
                            //         unloadRailCar().then((value) => null).catchError((error) {
                            //           _showToasties(message: "Application Error: ${error}", color: Colors.red);
                            //         });
                            //       }
                            //     : null
                            ,
                            child: Text(
                              'UNLOAD RAILCAR',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  color: Colors.white),
                            ),
                            color: Color(0xFF508be4),
                            textColor: Colors.white,
                            disabledColor: Colors.grey,
//                                disabledTextColor: Colors.black,
                            splashColor: Color(0xFF3e8aeb)),
                      )),
                ],
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(14)),
            child: Divider(
              thickness: 2.0,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CustomDropdownGeneric(
                    error:
                        "Error" == fcModel.requireLocationWhenUnloadingAsset &&
                                isApplyButtonClicked &&
                                dto.selectedUnloadTo == null
                            ? true
                            : false,
                    hintText: "Unload To",
                    itemList: unloadFromList,
                    SelectedValue: dto.selectedUnloadTo,
                    Ontap: onUnloadToChanged),
                CustomDropdown(
                    error:
                        isApplyButtonClicked && dto.selectedBlockValue == null
                            ? false
                            : false,
                    hintText: "Block To",
                    itemList: blockToList
                        .map((e) => e.railcarBlockToCodeName)
                        .toList(),
                    SelectedValue:
                        dto.selectedBlockValue?.railcarBlockToCodeName,
                    Ontap: onBlockToChanged),
                Container(
                  width: 213,
                  alignment: Alignment.center,
                  child: TextFormField(
                    controller: _unloadAmountController,
                    keyboardType: TextInputType.number,
                    enabled: !this.populateVCF,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                      LengthLimitingTextInputFormatter(6),
                    ],
                    onChanged: (str) {
                      dto.unloadAmount = str;
                      if (isApplyButtonClicked) {
                        _formKey.currentState.validate();
                      }
                    },
                    validator: (String arg) {
                      if (isApplyButtonClicked &&
                          (arg == null || arg.isEmpty)) {
                        return 'This field is mandatory';
                      } else
                        return null;
                    },
                    style: TextStyle(
                        color: _themeChanger.getTheme().primaryColor ==
                                Color(0xff182e42)
                            ? Colors.white
                            : _themeChanger.getTheme().primaryColor ==
                                    Color(0xfff5f5f5)
                                ? Colors.black
                                : null),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(1.0),
                      errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(1)),
                          borderSide: BorderSide(width: 1, color: Colors.red)),
                      focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(1)),
                          borderSide: BorderSide(width: 1, color: Colors.red)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: _themeChanger.getTheme().primaryColor ==
                                    Color(0xff182e42)
                                ? Color(0xffffffff)
                                : _themeChanger.getTheme().primaryColor ==
                                        Color(0xfff5f5f5)
                                    ? Color(0xff182e42)
                                    : null),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: _themeChanger.getTheme().primaryColor ==
                                    Color(0xff182e42)
                                ? Color(0xffffffff)
                                : _themeChanger.getTheme().primaryColor ==
                                        Color(0xfff5f5f5)
                                    ? Colors.black26
                                    : null),
                      ),
                      filled: true,
                      fillColor: _themeChanger.getTheme().primaryColor ==
                              Color(0xff182e42)
                          ? Color(0xff172636)
                          : _themeChanger.getTheme().primaryColor ==
                                  Color(0xfff5f5f5)
                              ? Color(0xfff5f5f5)
                              : null,
                      hintText: 'Unload Amount',
                      hintStyle: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: ScreenUtil().setHeight(14),
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.normal,
                        letterSpacing: ScreenUtil().setWidth(0.25),
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 230,
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                  alignment: Alignment.center,
                  child: DateTimeField(
                    initialValue: DateTime.now(),
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                          context: context,
                          firstDate: DateTime(1900),
                          initialDate: currentValue ?? DateTime.now(),
                          lastDate: DateTime(2100));
                      if (date != null) {
                        final time = await showTimePicker(
                          context: context,
                          initialTime: TimeOfDay.fromDateTime(
                              currentValue ?? DateTime.now()),
                        );
                        dto.unloadDateTime = DateTimeField.combine(date, time);
                        return DateTimeField.combine(date, time);
                      } else {
                        dto.unloadDateTime = currentValue;
                        return currentValue;
                      }
                    },
                    format: dateFormat,
                    style: TextStyle(
                        color: _themeChanger.getTheme().primaryColor ==
                                Color(0xff182e42)
                            ? Colors.white
                            : _themeChanger.getTheme().primaryColor ==
                                    Color(0xfff5f5f5)
                                ? Colors.black
                                : null),
//                    controller: _unloadDateController,
                    readOnly: true,
                    decoration: InputDecoration(
                      labelText: "Unload Date",
                      labelStyle: TextStyle(color: Colors.blueGrey),
                      contentPadding: EdgeInsets.all(1.0),
                      prefixIcon: Icon(
                        Icons.date_range,
                        color: _themeChanger.getTheme().primaryColor ==
                                Color(0xff182e42)
                            ? Color(0xfff5f5f5)
                            : _themeChanger.getTheme().primaryColor ==
                                    Color(0xfff5f5f5)
                                ? Color(0xff172636)
                                : null,
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color(0xffc0c5cc),
                            style: BorderStyle.solid,
                            width: 1.0),
                      ),
                      filled: true,
                      fillColor: _themeChanger.getTheme().primaryColor ==
                              Color(0xff182e42)
                          ? Color(0xff172636)
                          : _themeChanger.getTheme().primaryColor ==
                                  Color(0xfff5f5f5)
                              ? Color(0xfff5f5f5)
                              : null,
                    ),
                  ),
                ),
              ],
            ),
          ),
          showMarkAsEmpty
              ? Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(width: 70),
                      Checkbox(
                        checkColor: Colors.greenAccent,
                        activeColor: Color(0xFF3e8aeb),
                        value: markAsEmptyValue,
                        onChanged: (bool value) {
                          setState(() {
                            markAsEmptyValue = value;
                          });
                        },
                      ),
                      Text('Mark As Empty'),
                      SizedBox(
                        width: ScreenUtil().setWidth(112),
                      ),
                      markAsEmptyValue ?
                      Container(
                        margin: EdgeInsets.all(5.0),
                        child: CustomDropdown(
                            error:
                            isApplyButtonClicked && dto.reason_code_id == null
                                ? false
                                : false,
                            hintText: "Reason Code",
                            itemList: reasonCodeList
                                .map((e) => e.code)
                                .toList(),
                            SelectedValue: dto.reason_code ?? null,
                            Ontap: onReasonCodeChanged),
                      ):Container()
                    ],
                  ),
                )
              : new Container(),
          SizedBox(
            height: ScreenUtil().setHeight(20),
          ),


          assignedEntryFields.length > 0
              ? Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: DottedBorder(
                      color: _themeChanger.getTheme().primaryColor ==
                              Color(0xff182e42)
                          ? Color(0xfff5f5f5)
                          : _themeChanger.getTheme().primaryColor ==
                                  Color(0xfff5f5f5)
                              ? Color(0xff172636)
                              : null,
                      dashPattern: [4],
                      strokeWidth: 1,
                      child: Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(16),
                            vertical: ScreenUtil().setHeight(20)),
                        child: Wrap(
                          alignment: WrapAlignment.spaceEvenly,
                          spacing: 10,
                          direction: Axis.horizontal,
                          children: assignedEntryFields
                              .map(
                                (e) => Container(
                                  width: ScreenUtil().setWidth(213),
                                  padding: EdgeInsets.symmetric(vertical: 4.0),
                                  alignment: Alignment.center,
                                  child: TextFormField(
                                    style: TextStyle(
                                        color: _themeChanger
                                                    .getTheme()
                                                    .primaryColor ==
                                                Color(0xff182e42)
                                            ? Colors.white
                                            : _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xfff5f5f5)
                                                ? Colors.black
                                                : null),
                                    validator: (String arg) {
                                      return (arg == null || arg.isEmpty) &&
                                              e.required == "1" &&
                                              (dto.additionalField[e.id] ==
                                                      null ||
                                                  dto.additionalField[e.id]
                                                      .toString()
                                                      .isEmpty)
                                          ? 'This field is mandatory'
                                          : null;
                                    },
                                    readOnly: false,
                                    onChanged: (str) {
                                      onAdditionalFieldChanged(str, e.label);
                                    },
                                    decoration: InputDecoration(
                                      labelText: e.label,
                                      labelStyle:
                                          TextStyle(color: Colors.blueGrey),
                                      contentPadding: EdgeInsets.all(1.0),
                                      errorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(1)),
                                          borderSide: BorderSide(
                                              width: 1, color: Colors.red)),
                                      focusedErrorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(1)),
                                          borderSide: BorderSide(
                                              width: 1, color: Colors.red)),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xff182e42)
                                                ? Color(0xffffffff)
                                                : _themeChanger
                                                            .getTheme()
                                                            .primaryColor ==
                                                        Color(0xfff5f5f5)
                                                    ? Color(0xff182e42)
                                                    : null),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xff182e42)
                                                ? Color(0xffffffff)
                                                : _themeChanger
                                                            .getTheme()
                                                            .primaryColor ==
                                                        Color(0xfff5f5f5)
                                                    ? Colors.black26
                                                    : null),
                                      ),
                                      filled: true,
                                      fillColor: _themeChanger
                                                  .getTheme()
                                                  .primaryColor ==
                                              Color(0xff182e42)
                                          ? Color(0xff172636)
                                          : _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xfff5f5f5)
                                              ? Color(0xfff5f5f5)
                                              : null,
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                )
              : Container(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  height: ScreenUtil().setHeight(48),
                  width: ScreenUtil().setWidth(288),
                  decoration: ShapeDecoration(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                          width: 1.0,
                          style: BorderStyle.solid,
                          color: Color.fromRGBO(0, 0, 0, 0.12)),
                    ),
                  ),
                  child: FlatButton(
                      child: Text(
                        'APPLY TO SELECTION',
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: ScreenUtil().setHeight(14),
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                            letterSpacing: ScreenUtil().setWidth(1.25),
                            color: Colors.white),
                      ),
                      onPressed: dto.selectedEquipmentListSecondGrid.isNotEmpty
                          ? () {
                              applyToSelection();
                            }
                          : null,
                      color: Color(0xFF508be4),
                      textColor: Colors.white,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      splashColor: Color(0xFF3e8aeb)),
                ),
              ),
            ],
          ),
          SizedBox(
            height: ScreenUtil().setHeight(1),
          ),
          Center(child: UnLoadRailCarTable(dto, refreshScreen))
        ],
      ));
    }

    return Scaffold(
        key: globalKey,
        appBar: MainAppBar(),
        drawer: AppDrawer(),
        body: Form(
          key: _formKey,
          autovalidate: false,
          child: formUI(),
        ));
  }

  refreshScreen() {
    setState(() {});
  }

  onUnloadToChanged(LocationModel unloadTo) {
    dto.selectedUnloadTo =
        unloadFromList.singleWhere((element) => element == unloadTo);
    isApplyButtonClicked = false;
    setState(() {});
  }

  onBlockToChanged(String block) {
    dto.selectedBlockValue = blockToList
        .singleWhere((element) => element.railcarBlockToCodeName == block);
    isApplyButtonClicked = false;
    setState(() {});
  }

  onReasonCodeChanged(String reason_code){
    dto.reason_code = reason_code;
    dto.reason_code_id = reasonCodeList.singleWhere((element) => element.code== reason_code).reason_code_id;
    isApplyButtonClicked = false;
    setState(() {});
  }


  onAdditionalFieldChanged(String value, String id) {
    dto.additionalField.update(id, (e) => value, ifAbsent: () => value);
    isApplyButtonClicked = false;
  }

  cancelPopUpDialog() async {
    final action = await CustomDialog.cstmDialog(
        context, "add_railcar", "Unsaved Changes", "");
    if (action[0] == DialogAction.yes) {
      Navigator.pop(context);
    }
  }

  applyToSelection() {
    isApplyButtonClicked = true;
    formIsValid = false;
    final FormState form = _formKey.currentState;
    if ("Error" == fcModel.requireLocationWhenUnloadingAsset &&
        dto.selectedUnloadTo == null) {
      form.validate();
      formIsValid = false;
    } else if (form.validate()) {
      var measure_id = dto.selectedUnloadTo?.measure_id;
      var product_id = dto.selectedUnloadTo?.product_id;
      var customer_id = dto.selectedUnloadTo?.customer_id;

      dto.selectedEquipmentListSecondGrid.map((equipment) {
        Compartment compartment;
        if (equipment.compartmentList.isEmpty) {
          compartment = Compartment();
          equipment.compartmentList.add(compartment);
        } else {
          compartment = equipment.compartmentList[0];
        }

        if (measure_id != null &&
            MemoryData.uomMap[compartment.uomId].dryOrLiquid !=
                MemoryData.uomMap[measure_id].dryOrLiquid) {
          _showToastError([]..add(
              "${equipment.equipmentInitial + ' ' + equipment.equipmentNumber} and Location contain different UOMs."));
          isApplyButtonClicked = false;
          return;
        }

        if (product_id != null && compartment.productId != product_id) {
          _showToastError([]..add(
              "${equipment.equipmentInitial + ' ' + equipment.equipmentNumber} has a different product than the selected Location "));
          isApplyButtonClicked = false;
          return;
        }

        if (customer_id != null && compartment.customer_id != customer_id) {
          _showToastError([]..add(
              "${equipment.equipmentInitial + ' ' + equipment.equipmentNumber} has a different customer than the selected Location "));
          isApplyButtonClicked = false;
          return;
        }

        if (customer_id != null && compartment.customer_id != customer_id) {
          _showToastError([]..add(
              "${equipment.equipmentInitial + ' ' + equipment.equipmentNumber} has a different customer than the selected Location "));
          isApplyButtonClicked = false;
          return;
        }

        compartment.unload_to_id = dto.selectedUnloadTo?.id;
        compartment.unload_to_name = dto.selectedUnloadTo?.name;
//        compartment.customer_id = dto.selectedUnloadTo.customer_id;// this was commented since we must send same customer which coming from load car
        equipment.blockToId = dto.selectedBlockValue?.id;
        equipment.assignToSelection = true;
        equipment.blockToName = dto.selectedBlockValue?.railcarBlockToCodeName;
        compartment.unload_amount = dto.unloadAmount ?? "0";
        compartment.unloadDate = dto.unloadDateTime ?? DateTime.now();
        compartment.markAsEmpty = markAsEmptyValue;
        compartment.reason_code = dto.reason_code;
        compartment.reason_code_id =dto.reason_code_id;
        compartment.additionalFields = dto.additionalField.entries.map((entry) {
          return AdditionFiled(entry.key, entry.value);
        }).toList();
      }).toList();

      //  isApplyButtonClicked = true;
      formIsValid = true;
    }
    setState(() {});
  }

  clearScannedEquipList() {
    List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"];
    if (scannedEquipList != null) {
//      MemoryData.dataMap.removeWhere((key, value) => key == "scannedEquipList");
    }
  }

  Future<void> unloadRailCar() async {
    bool validateAssignSelection = false;

    dto.selectedEquipmentListSecondGrid.forEach((e) {
      if (e.assignToSelection == true) {
        validateAssignSelection = true;
      }
    });

    if (formIsValid &&
        validateAssignSelection &&
        valuesEnteredForAllEquipment()) {
      Map<String, List<String>> map =
          await unloadCarRepo.checkBeforeSavingUnLoad(dto, fcModel);

      List<String> errorList = map["error"];
      List<String> warningList = map["warnning"];

      if (errorList.isNotEmpty) {
        _showToastError(errorList);
      } else if (warningList.isNotEmpty) {
        bool isOk = await confirmationMessage(warningList);
        if (isOk) {
          print("dto----> ${dto}");
          await unloadCarRepo.saveUnLoad(dto);
          showToastRecordsSavedSuccess();
//          clearScannedEquipList();
          navigateBack();
        }
      } else {
        print("dto----> ${dto}");
         await unloadCarRepo.saveUnLoad(dto);
        showToastRecordsSavedSuccess();
//        clearScannedEquipList();
        navigateBack();
      }
    } else {
      showToastApplyButtonNotClicked();
    }
  }

  navigateBack() {
    if (this.populateVCF) {
      int count = 0;
      Navigator.of(context).popUntil((_) => count++ >= 2);
    } else {
      Navigator.pop(context);
    }
  }

  bool valuesEnteredForAllEquipment() {
    print("start valuesEnteredForAllEquipment()");
    bool returnValue = true;
    List<Equipment> equipmentList = dto.selectedEquipmentList;
    for (Equipment equipment in equipmentList) {
      Compartment compartment = equipment.compartmentList[0];
      print("compartment.unload_amount:${compartment.unload_amount}");
      if (compartment.unload_amount == null ||
          double.parse(compartment.unload_amount) == 0) {
        returnValue = false;
        _showToastError([]..add(
            "${equipment.equipmentInitial + ' ' + equipment.equipmentNumber} - unload amount is not entered."));
        break;
      }
    }
    print("returnValue:$returnValue");
    return returnValue;
  }

  _showToasties({String message, Color color}) {
    fToast.showToast(
      child: CustomToast(
        toastColor: color,
        toastMessage: message,
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  void _showToastError(List<String> errorList) {
    Widget toast = Container(
        width: ScreenUtil().setWidth(552),
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
        decoration: BoxDecoration(
          color: Colors.redAccent,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: errorList.map((e) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.warning),
                SizedBox(
                  width: 12.0,
                ),
                Expanded(
                  child: Text(
                    e,
                    style: TextStyle(
                      // ignore: deprecated_member_use
                      color: Theme.of(context).textTheme.body1.color,
                    ),
                  ),
                ),
              ],
            );
          }).toList(),
        ));

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  void showToastApplyButtonNotClicked() {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Colors.red,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.warning),
          SizedBox(
            width: 12.0,
          ),
          Text(
            "Please apply selection to railcar(s) to proceed",
            style: TextStyle(
              // ignore: deprecated_member_use
              color: Theme.of(context).textTheme.body1.color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  void showToastRecordsSavedSuccess() {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Color(0xff7fae1b),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text(
            "Railcar Unloaded Successfully",
            style: TextStyle(
              // ignore: deprecated_member_use
              color: Theme.of(context).textTheme.body1.color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  autoPopulateVCFData() {
    _unloadAmountController.text = this.unloadAmount;
    dto.unloadAmount = this.unloadAmount;
    dto.populateVCF = this.populateVCF;
  }

  confirmationMessage(List<String> warningList) async {
    String sealErrorText = "";
    for (String error in warningList) {
      sealErrorText += "$error\n";
    }
    bool returnValue = false;
    List<Object> returnList = await CustomDialog.cstmDialog(
        context,
        "confirm_to_continue",
        "There is at least one railcar with a warning",
        "$sealErrorText \nDo you wish to continue?");

    if (returnList[0] == DialogAction.yes) {
      returnValue = true;
    }
    return returnValue;
  }
}
