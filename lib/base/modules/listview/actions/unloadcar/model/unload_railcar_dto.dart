import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/location_model.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/railcar_block.dart';

class UnLoadRailCarDTO {
  List<Equipment> selectedEquipmentList = [];
  List<Equipment> selectedEquipmentListSecondGrid = [];
  RailcarBlock selectedBlockValue;
  LocationModel selectedUnloadTo;
  String unloadAmount;
  String currentAmount;
  String UOM;
  String waybill_amount;
  String reason_code;
  String reason_code_id;
  DateTime unloadDateTime;
  Map additionalField = {};
  bool populateVCF = false;
}
