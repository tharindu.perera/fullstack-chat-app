import 'dart:convert';

import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/model/load_railcar_model.dart';

String unloadRailCarModelToMap(UnLoadRailCarModel data) => json.encode(data.toMap());

class UnLoadRailCarModel {
  UnLoadRailCarModel(
      {this.unload_railcar_id,
      this.permission_id,
      this.equipment_id,
      this.unload_to_id,
      this.block_to_id,
      this.unload_amount,
      this.unload_date_time,
      this.facility_id,
      this.operation_type,
      this.source,
      this.created_dt,
      this.updated_dt,
      this.id,
      this.transaction_status,
      this.user,
      this.equipment,
      this.additionalFields,
      this.railCarVisibleTextValue,
      this.current_amount,
      this.uom,
      this.asset_master_id,
      this.productID});

  String unload_railcar_id;
  String equipment_id;
  String unload_to_id;
  String unload_to_name;
  String block_to_name;
  String block_to_id;
  String unload_amount;
  DateTime unload_date_time;
  String facility_id;
  DateTime created_dt;
  String updated_dt;
  String id;
  String transaction_status;
  String source;
  String permission_id;
  String operation_type;
  String user;
  String asset_master_id;
  Equipment equipment;
  List<AdditionFiled> additionalFields;

  // Additional fields
  String railCarVisibleTextValue;
  String uom;
  String uomValue;
  String current_amount;
  String productID;
  String productName;

  Map<String, dynamic> toMap() => {
        "equipment_doc": equipment == null ? null : equipment.toMap(),
        "unload_railcar_id": unload_railcar_id == null ? null : unload_railcar_id,
        "equipment_id": equipment_id == null ? null : equipment_id,
        "unload_to_id": unload_to_id == null ? null : unload_to_id,
        "block_to_id": block_to_id == null ? null : block_to_id,
        "product_id": productID == null ? null : productID,
        "unload_amount": unload_amount == null ? null : unload_amount,
        "unload_date_time": unload_date_time == null ? null : unload_date_time.toString(),
        "additional_fields": additionalFields == null ? null : additionalFields.map((i) => i.toMap()).toList(),
        "facility_id": facility_id == null ? null : facility_id,
        "id": id == null ? null : id,
        "user": user == null ? null : user,
        "transaction_status": transaction_status == null ? null : transaction_status,
        "created_dt": created_dt == null ? null : created_dt.toString(),
        "updated_dt": updated_dt == null ? null : updated_dt.toString(),
        "source": source == null ? null : source,
        "operation_type": operation_type == null ? null : operation_type,
        "railCarVisibleTextValue": railCarVisibleTextValue == null ? null : railCarVisibleTextValue,
        "uom": uom == null ? null : uom,
        "current_amount": current_amount == null ? null : current_amount,
        "asset_master_id": asset_master_id == null ? null : asset_master_id,
      };
}
