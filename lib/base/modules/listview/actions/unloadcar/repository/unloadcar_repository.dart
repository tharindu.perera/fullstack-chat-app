import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/ReasonCode.dart';
import 'package:mbase/base/core/common_base/model/assigned_entry_field.dart';
import 'package:mbase/base/core/common_base/model/client_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/facility_configuration_model.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/location_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/common_base/model/purchase_order_model.dart';
import 'package:mbase/base/core/common_base/model/shipment_type_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/actions/unloadcar/model/unload_railcar_dto.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';

class UnLoadCarRepo {
  var commonDao = CommonDao();
  var equipmentDao = EquipmentDAO();
  var notificationRepository = NotificationRepository();

  Future<List<AssignedEntryField>> fetchAdditionalFields() async {
    return commonDao.fetchAdditionalFields(
        env.userProfile.facilityId, "UNLOAD");
  }

  Future<List<PurchaseOrderModel>> fetchPurchaseOrders() async {
    return commonDao.fetchPurchaseOrderList(env.userProfile.facilityId);
  }

  Future<List<Equipment>> fetchRailcarDetails(String id) async {
    return commonDao.fetchRailCarList(env.userProfile.facilityId);
  }

  Future<List<ShipmentTypeModel>> fetchShipmentTypes() async {
    return commonDao.fetchShipmentTypeList();
  }

  Future<List<ProductModel>> fetchProducts() async {
    return commonDao.fetchProductsList();
  }

  Future<List<ReasonCode>> fetchReasonCodes() async {
    List<ClientModel> clientData = await commonDao.fetchReasonCodeList();
    List<ReasonCode> reasonCodeData = [];
    clientData.forEach((element) {
      reasonCodeData = element.reason_codes;
    });
    return reasonCodeData;
  }

  Future<List<LocationModel>> fetchLocations() async {
    List<LocationModel> list = [];
    return commonDao.fetchLocationList().then((locations) {
      locations.forEach((location) {
        Map map = {};
        LocationModel tempLoc;
        if (location.inventory == null || location.inventory.isEmpty) {
          location.measure_id = location.measure_id;
          location.measure_name = MemoryData.uomMap[location.measure_id].unit;
          location.product_id = null;
          location.product_name = null;
          location.customer_id = null;
          location.customer_name = null;
          location.quantity = null;
          location.dropDownDisplayName = location.name +
              " / " +
              (location.product_name ?? "* NO PRODUCT *") +
              " / " +
              (location.customer_name ?? "* NO CUSTOMER *");
          list.add(location);
        } else {
          location.inventory.forEach((inventory) {
            if (map[inventory.product_id + (inventory.customer_id ?? "")] ==
                null) {
              tempLoc = LocationModel();
              tempLoc.id = location.id;
              tempLoc.facility_id = location.facility_id;
              tempLoc.areaId = location.areaId;
              tempLoc.name = location.name;
              tempLoc.createdDt = location.createdDt;
              tempLoc.updatedDt = location.updatedDt;
              tempLoc.inventory = location.inventory;
              tempLoc.measure_id = inventory.measure_id;
              tempLoc.measure_name = inventory.measure_name;
              tempLoc.product_id = inventory.product_id;
              tempLoc.product_name = inventory.product_name;
              tempLoc.customer_id = inventory.customer_id;
              tempLoc.customer_name = inventory.customer_name;
              tempLoc.quantity = inventory.quantity;
              tempLoc.dropDownDisplayName = tempLoc.name +
                  " / " +
                  (tempLoc.product_name ?? "* NO PRODUCT *") +
                  " / " +
                  (tempLoc.customer_name ?? "* NO CUSTOMER *");
              list.add(tempLoc);
              map[inventory.product_id + (inventory.customer_id ?? "")] =
                  tempLoc;
            } else {
              tempLoc =
                  map[inventory.product_id + (inventory.customer_id ?? "")];
              if (inventory.measure_id != tempLoc.measure_id) {
                var uomConversion = MemoryData
                    .uomConversion[inventory.measure_id + tempLoc.measure_id];
                if (uomConversion == null) {
                  print(
                      ">>>>>>UOM Conversion not found for ${inventory.measure_name}(id:${inventory.measure_id})  and ${tempLoc.measure_name} (id:${tempLoc.measure_id})");
//                throw "UOM Conversion not found for ${inventory.measure_name}(id:${inventory.measure_id})  and ${tempLoc.measure_name} (id:${tempLoc.measure_id})";
                } else {
                  tempLoc.quantity = (double.parse(tempLoc.quantity) +
                          (double.parse(uomConversion.conversion_factor) *
                              double.parse(inventory.quantity)))
                      .toString();
                }
              } else {
                tempLoc.quantity = (double.parse(tempLoc.quantity) +
                        double.parse(inventory.quantity))
                    .toString();
              }
            }
          });
        }
      });
      return list;
    });
  }

  Future<Map<String, List<String>>> checkBeforeSavingUnLoad(
      UnLoadRailCarDTO dto, FacilityConfigurationModel fcModel) async {
    Map<String, List<String>> map = {};
    List<String> errorList = [];
    List<String> warningList = [];
    List<Equipment> equipmentList = dto.selectedEquipmentList;

    for (Equipment equipment in equipmentList) {
      Compartment compartment = equipment.compartmentList[0];
      if (compartment.productId == null) {
        errorList.add(
            "${equipment.equipmentInitial + " " + equipment.equipmentNumber} : Cannot unload without a product.");
      }

      if ("Error" ==
              fcModel.whenRailcarUnloadAmountIsGreaterThanWaybillAmount ||
          "Warning" ==
              fcModel.whenRailcarUnloadAmountIsGreaterThanWaybillAmount) {
        if (double.parse(compartment.unload_amount) >
            double.parse(compartment.currentAmount)) {
          "Error" == fcModel.whenRailcarUnloadAmountIsGreaterThanWaybillAmount
              ? errorList.add(
                  "${equipment.equipmentInitial + " " + equipment.equipmentNumber} unload amount is greater than waybill amount")
              : warningList.add(
                  "${equipment.equipmentInitial + " " + equipment.equipmentNumber} unload amount is greater than waybill amount");
        }
      }

      if (dto.selectedUnloadTo == null &&
          ("Error" == fcModel.requireLocationWhenUnloadingAsset ||
              "Warning" == fcModel.requireLocationWhenUnloadingAsset)) {
        "Error" == fcModel.whenRailcarUnloadAmountIsGreaterThanWaybillAmount
            ? errorList.add(
                "${equipment.equipmentInitial + " " + equipment.equipmentNumber} require location for unloading")
            : warningList.add(
                "${equipment.equipmentInitial + " " + equipment.equipmentNumber} require location for unloading");
      }
    }
    print(
        ">>>>Error list :${errorList.length} Warning list:${warningList.length}  ");
    map["error"] = errorList;
    map["warnning"] = warningList;
    return map;
  }

  void saveUnLoad(UnLoadRailCarDTO dto) async {
    var finalLoadCarList = dto.selectedEquipmentList.map((equipment) {
      equipment.transactionStatus = APP_CONST.TRANSACTION_STATUS_PENDING;
      equipment.permission_id = env.userProfile.id;
      equipment.facilityId = env.userProfile.facilityId;
      equipment.source = APP_CONST.MOBILITY;
      equipment.operationType = APP_CONST.UNLOAD_CAR_OPERATION;
      equipment.user = env.userProfile.userName;

      if (dto.populateVCF) {
        equipment.compartmentList[0].currentAmount = "0";
      } else {
        equipment.compartmentList[0].currentAmount =
            (double.parse(equipment.compartmentList[0].currentAmount ?? "0") -
                    double.parse(equipment.compartmentList[0].unload_amount))
                .toString();
      }
      equipment.createdDt = DateTime.now();
      equipment.updatedDt = DateTime.now();
      equipment.parent_id = equipment?.parent_id ?? equipment.id;
      equipment.immediateParentId = equipment.id;
      return equipment;
    }).toList();

    finalLoadCarList.forEach((element) {
      equipmentDao.add(element);
      notificationRepository.addNotification(
          element, APP_CONST.UNLOAD_CAR_OPERATION);
    });
  }
}
