import 'package:equatable/equatable.dart';

class ExpansionDataModel extends Equatable {
  ExpansionDataModel(
      {this.id, this.yardName, this.isExpanded,this.filteredTracks});
  String id;
  String yardName;
  bool isSelected = false;
  bool isExpanded = false;
  List <TrackDetails> filteredTracks =[];

  @override
  // TODO: implement props
  List<Object> get props => [id];

  ExpansionDataModel.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        yardName = map['yard_name'];

  @override
  String toString() {
    // TODO: implement toString
    return 'id: ${id}, yardName: ${yardName}';
  }
}

class TrackDetails extends Equatable {
  TrackDetails({this.trackName, this.isTrackSelected,this.id,this.yardId});
  String trackName ;
  String id;
  String yardId;
  bool isTrackSelected = false;



  @override
  // TODO: implement props
  List<Object> get props => [id];

}


