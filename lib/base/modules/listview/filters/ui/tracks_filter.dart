import 'package:dartz/dartz_unsafe.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/common_base/repositories/track_repository.dart';
import 'package:mbase/base/core/common_base/repositories/yard_repository.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/filters/dto/search_filter_dto.dart';
import 'package:mbase/base/modules/listview/filters/expansion_data_model/expansion_model.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:provider/provider.dart';

enum ListviewTracksFiltersOptions { apply, cancel }

class ListviewTracksFilters extends StatefulWidget {
  ListviewTracksFilters(
      List<Equipment> selectedEquipments, ListViewBloc _listViewBloc) {
    this.selectedEquipments = selectedEquipments;
    this._listViewBloc = _listViewBloc;
  }
  List<Equipment> selectedEquipments = List<Equipment>();
  ListViewBloc _listViewBloc;

  @override
  _ListviewTracksFiltersState createState() => _ListviewTracksFiltersState();
}

class _ListviewTracksFiltersState extends State<ListviewTracksFilters> {
  ThemeChanger _themeChanger;
  FToast fToast;
  SearchFilterDto _searchFilterDto = SearchFilterDto();
  bool _isChecked = true;
  Map<String, dynamic> _selectedChoices = {};
  TrackRepository _trackRepository = TrackRepository();
  YardRepository _yardRepository = YardRepository();
  List<TrackModel> trackData;
  List<ExpansionDataModel> _items = [];
  Map<int, dynamic> tracks_dto = {};
  List<YardModel> yardResultList = [];
  List<TrackModel> trackResultList = [];
  List<String> selectedYardNames = [];
  List<String> selectedTrackNames = [];
  List<String> selectedChoicesList = [];
  List<String> previousFilterCriteriaForYards = [];
  List<String> previousFilterCreteriaForTracks = [];

  bool selectedYard = false;

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);
    fetchYards();
  }
  checkConfiguration(){
     if(widget._listViewBloc?.filterConfiguration.isNotEmpty){
       print('Filter configuration------> ${widget._listViewBloc.filterConfiguration[0]}');
       previousFilterCriteriaForYards = widget._listViewBloc.filterConfiguration[0]['Filter_By_Yard'];
       previousFilterCreteriaForTracks = widget._listViewBloc.filterConfiguration[0]['Filter_By_Track'];
       widget._listViewBloc?.filterConfiguration[0]?.forEach((key, value) {
        if(key == APP_CONST.Filter_By_Yard){
          List <String> yardIdList = value;
          yardIdList.forEach((yard_id) {
            selectedChoicesList.add( MemoryData.inMemoryYardMap[yard_id].yardName);
            selectedYardNames.add(yard_id);
          });
          setPreviousYardSelection();
          setState(() {
          });
        }
        else if( key == APP_CONST.Filter_By_Track){
          List <String> trackIdList = value;
          trackIdList.forEach((track_id) {
            selectedChoicesList.add( MemoryData.inMemoryTrackMap[track_id].trackName);
            selectedTrackNames.add(track_id);

          });
          setPreviousTrackSelection();
          setState(() {
          });
        }
       });
    }

  }

  fetchYards() async {
    yardResultList = await _yardRepository.fetchAllYards();
    trackResultList = await _trackRepository.fetchAllTracks();
    if (yardResultList.length > 0) {
      yardResultList.forEach((yard_data) {
        ExpansionDataModel _expansionDataModel = ExpansionDataModel();
        _expansionDataModel.id = yard_data.id;
        _expansionDataModel.yardName = yard_data.yardName;
        _expansionDataModel.isExpanded = false;
        _expansionDataModel.filteredTracks = filterTracksByYardId(yardID: yard_data.id);
        _expansionDataModel.filteredTracks.sort((a,b)=>a.trackName.compareTo(b.trackName));
        _items.add(_expansionDataModel);
        _items.sort((a,b)=>a.yardName.compareTo(b.yardName));
      });
      checkConfiguration();
      setState(() {});
    }
  }

  List<TrackDetails> filterTracksByYardId({String yardID}) {
    List<TrackDetails> resultTrack = [];
    trackResultList.forEach((track) {
      if (track.yardId == yardID) {
        resultTrack.add((TrackDetails(
            trackName: track.trackName,
            isTrackSelected: false,
            id: track.id,
            yardId: track.yardId)));
      }
    });
    return resultTrack;
  }

  _showToast({String toastMessage, Color toastColor, IconData icon}) {
    fToast.showToast(
      child: CustomToast(
        toastColor: toastColor,
        toastMessage: toastMessage,
        icon: icon,
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  onCancel(){
    if(_searchFilterDto.dto.length>0){
      //Navigator.of(context).pop(ListviewTracksFiltersOptions.apply);
      Navigator.pop(context);
    }
    else{
      widget._listViewBloc
          .add(ListViewInitialDataLoad());
      Navigator.of(context).pop(ListviewTracksFiltersOptions.apply);
    }
  }

  applyFilters() {
    _searchFilterDto.dto[APP_CONST.Filter_By_YardAndTrack] = APP_CONST.Filter_By_YardAndTrack;
    print("selected apply filters ---->  ${_searchFilterDto.dto}");
    widget._listViewBloc.add(ListViewFetchDataWithCriteria(_searchFilterDto.dto));
    Navigator.of(context).pop(ListviewTracksFiltersOptions.apply);
  }

  void setChoice({String type, String choice}) {
    selectedChoicesList.add(choice);
    print("selectedChoices ----> ${selectedChoicesList}");
    _selectedChoices[type] = choice;
  }

  void validateFilters() {
    if (_searchFilterDto.dto.length==0) {
        // widget._listViewBloc
        //     .add(ListViewInitialDataLoad());
      widget._listViewBloc.add(ListViewInitialDataLoad());
      Navigator.pop(context);

    }
    else{
      applyFilters();
    }
  }

  resetAll() {
    _selectedChoices.clear();
    _searchFilterDto.dto.clear();
    selectedYardNames =[];
    selectedTrackNames=[];
    selectedChoicesList = [];
    widget._listViewBloc.filterConfiguration.clear();

    _items.forEach((element) {
      element.isExpanded = false;
      element.isSelected = false;
      element.filteredTracks.forEach((value) {
        value.isTrackSelected = false;
      });
    });

    setState(() {

    });
  }

  Widget userSelection({String choice}) {
    return FilterChip(
      onSelected: null,
      backgroundColor: Colors.red,
      label: Text(
        choice,
        style: TextStyle(
            color: Theme.of(context).textTheme.bodyText1.color,
            fontStyle: FontStyle.normal,
            fontFamily: 'Roboto',
            fontSize: 12),
      ),
    );
  }

updateSelectedYards({bool value, ExpansionDataModel data }){
    if(value){
      setState(() {
        selectedYardNames.add( yardResultList.singleWhere((element) => element.yardName == data.yardName).id);
      });
      _searchFilterDto.dto[APP_CONST.Filter_By_Yard] = selectedYardNames;

    }
    else{
      setState(() {
        selectedYardNames.removeWhere((element) => element==data.id);
      });
      checkIfTracksSelectedUnderRemovedYard(removedYardId:data.id);
      _searchFilterDto.dto[APP_CONST.Filter_By_Yard] = selectedYardNames;

    }

}

updateSelectedTracks({bool value, ExpansionDataModel data,int index}){
    if(value){
      String track_id = trackResultList.singleWhere((element) => element.trackName == data.filteredTracks[index].trackName).id;
      String trackAssignedToYardId = trackResultList.singleWhere((element) => element.trackName == data.filteredTracks[index].trackName).yardId;
      String yardName = yardResultList.singleWhere((element) => element.id == trackAssignedToYardId).yardName;
      selectedTrackNames.add(track_id);
      checkIfYardIsNotSelected(trackAssignedToYardId: trackAssignedToYardId,yardName: yardName);
      _searchFilterDto.dto[APP_CONST.Filter_By_Track] = selectedTrackNames;

    }
    else{
      selectedTrackNames.removeWhere((element) => element==data.id);
      _searchFilterDto.dto[APP_CONST.Filter_By_Track] = selectedTrackNames;

    }
}


checkIfYardIsNotSelected({String trackAssignedToYardId,String yardName}){
    print("checkIfYardIsNotSelected called ${trackAssignedToYardId}");
  yardResultList.forEach((yard_id){
    _items.forEach((element) {
      if(element.id == trackAssignedToYardId && !element.isSelected){
        print("checkIfYardIsNotSelected found ");
        element.isSelected = true;
        String yard_id =  yardResultList.singleWhere((element) => element.id == trackAssignedToYardId).id;
        selectedYardNames.add(yard_id);
       // _searchFilterDto.dto[APP_CONST.Filter_By_Yard] = selectedYardNames;
        setChoice(type: yardName,choice: yardName);
      }
    });
  });
  setState(() {
  });
}

checkIfTracksSelectedUnderRemovedYard({String removedYardId}){
      _items.forEach((yard_data) {
        yard_data.filteredTracks.forEach((track_data) {
          if(track_data.yardId == removedYardId){
                track_data.isTrackSelected = false;
               selectedTrackNames.removeWhere((track_id) => track_id==  track_data.id);
               _searchFilterDto.dto[APP_CONST.Filter_By_Track] = selectedTrackNames;
               selectedChoicesList.removeWhere((choice) => choice == track_data.trackName);
          }
        });

      });
    setState(() {
    });
}

  onYardSetChoice({ExpansionDataModel data}){
    if(data.isSelected){
      setChoice(
          type: data.yardName,
          choice: data.yardName);
    }
    else{
      selectedChoicesList.removeWhere((element) => element == data.yardName);
      print("removed selectedChoiceList---> $selectedChoicesList");
    }

  }

  onTrackSetChoice({ExpansionDataModel data,int index}){
    if( data.filteredTracks[index].isTrackSelected){
      setChoice(
          type: data.filteredTracks[index].trackName,
          choice: data.filteredTracks[index].trackName);
    }
    else{
      selectedChoicesList.removeWhere((element) => element == data.filteredTracks[index].trackName);
      print("removed selectedChoiceList---> $selectedChoicesList");
    }
}

  setPreviousYardSelection(){
    widget._listViewBloc.filterConfiguration[0]['Filter_By_Yard'].forEach((yard_id){
      _items.forEach((element) {
        if(element.id == yard_id){
                element.isSelected = true;
             //   element.isExpanded = true;
              }
            });
        });
    _searchFilterDto.dto[APP_CONST.Filter_By_Yard] = previousFilterCriteriaForYards;
    setState(() {
    });
  }

  setPreviousTrackSelection(){
    int count = 0;
    previousFilterCreteriaForTracks.forEach((track_id) {
      _items.forEach((element) {
        print("elementtt---> ${element.filteredTracks}");
        if(element.filteredTracks !=null && element.filteredTracks.isNotEmpty ){
        TrackDetails result = element?.filteredTracks?.singleWhere((value) =>  value?.id == track_id,orElse:()=> null);
          if(result != null){
            result.isTrackSelected = true;
            setState(() {

            });
          }
        }
      });
    });

    _searchFilterDto.dto[APP_CONST.Filter_By_Track] = previousFilterCreteriaForTracks;
  }


  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);
    print("---------rebuilding-------------");
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: AlertDialog(
        backgroundColor: Theme.of(context).canvasColor,
        content: Container(
            width: ScreenUtil().setWidth(860),
            height: ScreenUtil().setHeight(570),
            child: Column(
              children: <Widget>[
                Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              child: Text(
                                "Tracks Filters",
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'Roboto',
                                    letterSpacing: ScreenUtil().setWidth(0.5),
                                    color: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .color,
                                    fontSize: ScreenUtil()
                                        .setSp(20, allowFontScalingSelf: true)),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              child: Text(
                                "|",
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'Roboto',
                                    letterSpacing: ScreenUtil().setWidth(0.5),
                                    color: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .color,
                                    fontSize: ScreenUtil()
                                        .setSp(20, allowFontScalingSelf: true)),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              child: Icon(Icons.refresh,
                                  color: Colors.blueAccent, size: 20),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            GestureDetector(
                              onTap: resetAll,
                              child: Container(
                                child: Text(
                                  "RESET ALL",
                                  style: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontStyle: FontStyle.normal,
                                      fontFamily: 'Roboto',
                                      letterSpacing: ScreenUtil().setWidth(0.5),
                                      color: Colors.blueAccent,
                                      fontSize: ScreenUtil().setSp(14,
                                          allowFontScalingSelf: true)),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          width: ScreenUtil().setWidth(577),
                          height: ScreenUtil().setHeight(100),
                          color: Theme.of(context).scaffoldBackgroundColor,
                          child: DottedBorder(
                            color: _themeChanger.getTheme().primaryColor ==
                                    Color(0xff182e42)
                                ? Color(0xfff5f5f5)
                                : _themeChanger.getTheme().primaryColor ==
                                        Color(0xfff5f5f5)
                                    ? Color(0xff172636)
                                    : null,
                            dashPattern: [1],
                            strokeWidth: 1,
                            child: Container(
                                child: Wrap(
                                    spacing: 8,
                                    children: List.generate(
                                        selectedChoicesList?.length,
                                        (index) {
                                      return userSelection(
                                          choice: selectedChoicesList[index]);
                                    }))

                            ),
                          ),
                        )
                      ],
                    )
                  ],
                )),
                SizedBox(
                  height: ScreenUtil().setHeight(10),
                ),
                Container(
                    width: ScreenUtil().setWidth(1061),
                    height: ScreenUtil().setHeight(450),
                    color: Theme.of(context).scaffoldBackgroundColor,
                    child: DottedBorder(
                      color: _themeChanger.getTheme().primaryColor ==
                              Color(0xff182e42)
                          ? Color(0xfff5f5f5)
                          : _themeChanger.getTheme().primaryColor ==
                                  Color(0xfff5f5f5)
                              ? Color(0xff172636)
                              : null,
                      dashPattern: [1],
                      strokeWidth: 1,
                      child: ListView(
                        children: <Widget>[
                          Theme(
                            data: Theme.of(context).copyWith(
                                cardColor:
                                    Theme.of(context).scaffoldBackgroundColor),
                            child: ExpansionPanelList(
                              expansionCallback: (int index, bool isExpanded) {
                                setState(() {
                                  _items[index].isExpanded =!_items[index].isExpanded;
                                });
                              },
                              children: _items.map((ExpansionDataModel data) {
                                return ExpansionPanel(
                                    isExpanded:  data.isExpanded ,
                                    body: SizedBox(
                                      height: 100.0,
                                      child: Scrollbar(
                                        child: ListView.builder(
                                          scrollDirection: Axis.vertical,
                                          physics: ScrollPhysics(),
                                          itemCount: data.filteredTracks.length,
                                          itemBuilder: (BuildContext context, int index) {
                                            return Container(
                                                color: Theme.of(context)
                                                    .scaffoldBackgroundColor,
                                                child:Column(
                                                  children: [
                                                    Container(
                                                      margin: const EdgeInsets.only(left: 60.0),
                                                      child: ListTile(
                                                        leading: Checkbox(
                                                          checkColor:
                                                          Colors.greenAccent,
                                                          activeColor:
                                                          Color(0xFF3e8aeb),
                                                          value: data
                                                              .filteredTracks[index]
                                                              .isTrackSelected,
                                                          onChanged: (bool value) {
                                                            updateSelectedTracks(value:value,data:data,index:index);
                                                            setState(() {
                                                              data.filteredTracks[index].isTrackSelected = value;
                                                            });
                                                            onTrackSetChoice(data: data,index: index);
                                                          },
                                                        ),
                                                        title: Text(
                                                          data.filteredTracks[index]
                                                              .trackName,
                                                          style: TextStyle(
                                                              fontFamily: 'Roboto',
                                                              color: Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1
                                                                  .color),
                                                        ),
                                                      ),
                                                    )

                                                  ],
                                                )
                                                );
                                          },
                                        ),
                                      ),
                                    ),
                                    headerBuilder: (BuildContext context,
                                        bool isExpanded) {
                                      return Container(
                                        color: Theme.of(context).scaffoldBackgroundColor,
                                        child: ListTile(
                                          leading: Checkbox(
                                            checkColor: Colors.greenAccent,
                                            activeColor: Color(0xFF3e8aeb),
                                            value: data.isSelected,
                                            onChanged: (bool value) {
                                              updateSelectedYards(value: value,data: data);
                                              setState(() {
                                                data.isSelected = value;
                                              });
                                              onYardSetChoice(data:data);
                                            },
                                          ),
                                          title: Text(
                                            data.yardName,
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color),
                                          ),
                                        ),
                                      );
                                    });
                              }).toList(),
                            ),
                          )
                        ],
                      ),
                    ))
              ],
            )),
        actions: <Widget>[
          Container(
            width: ScreenUtil().setWidth(97),
            height: ScreenUtil().setHeight(48),
            child: FlatButton(
                shape: RoundedRectangleBorder(side: BorderSide(color: Theme.of(context).textTheme.bodyText1.color, width: 1, style: BorderStyle.solid), borderRadius: BorderRadius.circular(1)),
                onPressed:onCancel,
                child: Text(
                  'CANCEL',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: ScreenUtil().setHeight(14),
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w500,
                      letterSpacing: ScreenUtil().setWidth(1.25),
                      color:
                      _themeChanger.getTheme().primaryColor ==
                          Color(0xFF4468BE)
                          ? Color(0xfff5f5f5)
                          : _themeChanger.getTheme().primaryColor ==
                          Color(0xfff5f5f5)
                          ? Color(0xff172636)
                          : null
                      //Color(0xFF4468BE)
                ),
                ),
                color: Colors.transparent,
                //textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                splashColor: Color(0xFF3e8aeb)),
          ),
          SizedBox(
            width: 24,
          ),
          Container(
            width: ScreenUtil().setWidth(160),
            height: ScreenUtil().setHeight(48),
            child: FlatButton(
                onPressed: validateFilters,
                child: Text(
                  'APPLY',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: ScreenUtil().setHeight(14),
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w500,
                      letterSpacing: ScreenUtil().setWidth(1.25),
                      color: Colors.white),
                ),
                color: Color(0xFF508be4),
                textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                splashColor: Color(0xFF3e8aeb)),
          ),
        ],
      ),
    );
  }
}
