import 'dart:math';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/model/customer_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/facility_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/components/flyover/dao/flyover_dao.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/railcar_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/repository/assign_block_repository.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/repository/loadcar_repository.dart';
import 'package:mbase/base/modules/listview/filters/dto/search_filter_dto.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_bloc.dart';
import 'package:mbase/base/modules/listview/listview/bloc/listview_event.dart';
import 'package:provider/provider.dart';

enum ListviewSearchFiltersOptions { apply, cancel }

class ListviewSearchFilters extends StatefulWidget {
  ListviewSearchFilters(
      List<Equipment> selectedEquipments, ListViewBloc _listViewBloc) {
    this.selectedEquipments = selectedEquipments;
    this._listViewBloc = _listViewBloc;
  }
  List<Equipment> selectedEquipments = List<Equipment>();
  ListViewBloc _listViewBloc;

  @override
  _ListviewSearchFiltersState createState() => _ListviewSearchFiltersState();
}

class _ListviewSearchFiltersState extends State<ListviewSearchFilters> {
  ThemeChanger _themeChanger;
  var loadCarRepo = LoadCarRepo();
  List<String> inboundShipperList = [];
  String _selectedInboundShipper = null;
  List<String> inboundConsigneeList = [];
  String _selectedInboundConsignee;
  List<String> outboundConsigneeList = [];
  List<String> filteredOutboundConsigneeList = [];
  String _selectedOutboundConsignee;
  List<String> outboundShipperList = [];
  String _selectedOutboundShipper;
  String _selectedEquipmentGroup;
  String _selectedProduct;
  String _selectedBlockTo;
  String _selectedInspection;
  bool _isDefective = false;
  bool _hasBOL = false;
  bool _hasComments = false;
  bool _isHazardous = false;
  bool _isOnOrder = false;
  bool _isVCFCalculated = false;
  int defaultChoiceIndex;
  FToast fToast;
  SearchFilterDto _searchFilterDto = SearchFilterDto();
  Map<String, dynamic> _selectedChoices = {};
  List<String> _choicesList = ['All', 'Loaded', 'Empty'];
  List<String> _inspectionStatusList = [
    APP_CONST.INSPECTION_STATUS_SAVED,
    APP_CONST.INSPECTION_STATUS_FAILED,
    APP_CONST.INSPECTION_STATUS_COMPLETED
  ];
  List<ProductModel> productList = [];
  List<RailcarBlock> blockToList = [];
  List<String> equipmentGroupList = [];
  AssignBlockRepository assignBlockRepository = new AssignBlockRepository();
  FlyOverDao _flyOverDao = FlyOverDao();
  CommonDao _commonDao = CommonDao();
  List<CustomernModel> customerDataList = [];
  @override
  void initState() {
    super.initState();
    defaultChoiceIndex = 0;
    fToast = FToast(context);
      fetchRailcarBlocks();
      fetchProducts();
      fetchEquipmentGroup();
      fetchShipperAndConsigneeDetails();
      checkConfiguration();

  }

  checkConfiguration() {
    if (widget._listViewBloc?.filterConfiguration.isNotEmpty) {
      widget._listViewBloc?.filterConfiguration[0]?.forEach((key, value) {
        print('key --> $key || value ---> $value || value type ---> ${value.runtimeType}');
        if (key != APP_CONST.ListView_Filter) {
          _selectedChoices[key] = value.runtimeType == String
              ? fetchNameByKeyForStringTypes(key: key, value: value)
              : fetchNameByKeyForBoolTypes(key: key);
          _searchFilterDto.dto[key] = value.runtimeType == String
              ? fetchNameByKeyForStringTypes(key: key, value: value)
              : fetchNameByKeyForBoolTypes(key: key);
        }
      });
    }
  }

  String fetchNameByKeyForStringTypes({String key, String value}) {
    String tagName = "";
    switch (key) {
      case APP_CONST.Filter_By_InboundShipper:
        {
          _selectedInboundShipper = value;
          tagName = value;
        }
        break;

      case APP_CONST.Filter_By_InboundConsignee:
        {
          _selectedInboundConsignee = value;
          tagName = value;
        }
        break;

      case APP_CONST.Filter_By_OutboundShipper:
        {
          _selectedOutboundShipper = value;
          tagName = value;
        }
        break;

      case APP_CONST.Filter_By_OutboundConsignee:
        {
          _selectedOutboundConsignee = value;
          tagName = value;
        }
        break;

      case APP_CONST.Filter_By_EquipmentGroup:
        {
          _selectedEquipmentGroup = value;
          tagName = value;
        }
        break;

      case APP_CONST.Filter_By_Inspection:
        {
          _selectedInspection = value;
          tagName = value;
        }
        break;

      // case APP_CONST.Filter_By_Block_To:
      //   {
      //     _selectedBlockTo =   blockToList?.singleWhere((element) => element.id == value)?.railcarBlockToCodeName ;
      //     tagName = blockToList?.singleWhere((element) => element.id == value)?.railcarBlockToCodeName;
      //   }
      //   break;
      case APP_CONST.Filter_By_Product:
        {
          _selectedProduct = value;
          tagName = value;
        }
        break;

      case APP_CONST.Filter_By_Loaded:
        {
          defaultChoiceIndex = 1;
          tagName = "Loaded";
        }
        break;

      case APP_CONST.Filter_By_LoadEmpty:
        {
          defaultChoiceIndex = 2;
          tagName = "Empty";
        }
        break;

      case APP_CONST.Filter_By_LoadAll:
        {
          defaultChoiceIndex = 0;
          tagName = "All";
        }
        break;

      default:
        {
          print("Invalid key");
        }
        break;
    }
    setState(() {});
    return tagName;
  }

  String fetchNameByKeyForBoolTypes({String key}) {
    String tagName = "";
    switch (key) {
      case APP_CONST.Filter_By_Defective:
        {
          _isDefective = true;
          tagName = "Defective";
        }
        break;

      case APP_CONST.Filter_By_HasBOL:
        {
          _hasBOL = true;
          tagName = "Has BOL";
        }
        break;

      case APP_CONST.Filter_By_HasComments:
        {
          _hasComments = true;
          tagName = "Has Comments";
        }
        break;

      case APP_CONST.Filter_By_Hazardous:
        {
          _isHazardous = true;
          tagName = "Hazardous";
        }
        break;

      case APP_CONST.Filter_By_OnOrder:
        {
          _isOnOrder = true;
          tagName = "On Order";
        }
        break;

      case APP_CONST.Filter_By_VCFCalculated:
        {
          _isVCFCalculated = true;
          tagName = "VCF Calculated";
        }
        break;

      case APP_CONST.Filter_By_Loaded:
        {
          defaultChoiceIndex = 1;
          tagName = "Loaded";
        }
        break;

      case APP_CONST.Filter_By_LoadEmpty:
        {
          defaultChoiceIndex = 2;
          tagName = "Empty";
        }
        break;

      case APP_CONST.Filter_By_LoadAll:
        {
          defaultChoiceIndex = 0;
          tagName = "All";
        }
        break;

      default:
        {
          print("Invalid key");
        }
        break;
    }
    setState(() {});
    return tagName;
  }

  fetchRailcarBlocks() async {
    assignBlockRepository.fetchRailcarBlocks().then((value) {
      value.forEach((element) {
        blockToList.add(element);
      });
      setState(() {});
    });
  }

  _showToast({String toastMessage, Color toastColor, IconData icon}) {
    fToast.showToast(
      child: CustomToast(
        toastColor: toastColor,
        toastMessage: toastMessage,
        icon: icon,
      ),
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  fetchProducts() async {
    productList = await loadCarRepo.fetchProducts();
    productList
        .removeWhere((element) => element.productName == "--Please Select--");
    setState(() {});
  }

  fetchEquipmentGroup() async {
    FacilityModel facilityModel = await _flyOverDao.fetchFacilityData();
    if (null != facilityModel?.equipment_group_list) {
      equipmentGroupList = facilityModel.equipment_group_list.toList();
    }
    setState(() {});
  }

  fetchShipperAndConsigneeDetails() async {
    customerDataList = await _commonDao.fetchAllCustomers();
    if (customerDataList?.length > 0) {
      customerDataList?.forEach((element) {
        inboundShipperList.add(element.name);
        inboundConsigneeList.add(element.name);
        outboundShipperList.add(element.name);
        element?.customer_location_list?.forEach((outbnd_consignee) {
          outboundConsigneeList.add(outbnd_consignee);
        });
      });
    }
  }

  applyFilters() {
    _searchFilterDto.dto[APP_CONST.ListView_Filter] = APP_CONST.ListView_Filter;
    widget._listViewBloc
        .add(ListViewFetchDataWithCriteria(_searchFilterDto.dto));
    Navigator.pop(context);
  }

  void setChoice({String type, String choice}) {
    _selectedChoices[type] = choice;
  }

  void validateFilters() {
    print("passing dto length----> ${_searchFilterDto.dto.length}");
    if (_searchFilterDto.dto.length == 0) {
      widget._listViewBloc.add(ListViewInitialDataLoad());
      resetAll();
      Navigator.pop(context);
      // _showToast(
      //     toastMessage: "No filters applied, Please apply a filter to proceed",
      //     icon: Icons.warning,
      //     toastColor: Colors.red);
    } else {
      applyFilters();
    }
  }

  void _filterOutboundConsignee({String selectedOutboundShipper}) {
    _selectedOutboundConsignee = null;
    filteredOutboundConsigneeList = [];

    CustomernModel customernModel = customerDataList
        .singleWhere((element) => element.name == selectedOutboundShipper);

    if (customernModel != null &&
        customernModel.customer_location_list.length > 0) {
      customernModel?.customer_location_list?.forEach((element) {
        filteredOutboundConsigneeList.add(element);
      });
      setState(() {});
    }
  }

  resetAll() {
    setState(() {
      _selectedProduct = null;
      _selectedOutboundConsignee = null;
      _selectedOutboundShipper = null;
      _selectedInboundConsignee = null;
      _selectedInboundShipper = null;
      _selectedEquipmentGroup = null;
      _selectedInspection = null;
      _selectedBlockTo = null;
      defaultChoiceIndex = 0;
      _isDefective = false;
      _hasBOL = false;
      _hasComments = false;
      _isHazardous = false;
      _isOnOrder = false;
      _isVCFCalculated = false;
      _selectedChoices.clear();
      _searchFilterDto.dto.clear();
      widget._listViewBloc?.filterConfiguration = [];
    });
  }

  Widget userSelection({String choice}) {
    return FilterChip(
      onSelected: null,
      label: Text(
        choice,
        style: TextStyle(
            color: Theme.of(context).textTheme.bodyText1.color,
            fontStyle: FontStyle.normal,
            fontFamily: 'Roboto',
            fontSize: 12),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);
    print("---------rebuilding-------------");
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: AlertDialog(
        backgroundColor: Theme.of(context).canvasColor,
        content: Container(
            width: ScreenUtil().setWidth(860),
            height: ScreenUtil().setHeight(570),
            child: Column(
              children: <Widget>[
                Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              child: Text(
                                "Search Filters",
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'Roboto',
                                    letterSpacing: ScreenUtil().setWidth(0.5),
                                    color: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .color,
                                    fontSize: ScreenUtil()
                                        .setSp(20, allowFontScalingSelf: true)),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              child: Text(
                                "|",
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'Roboto',
                                    letterSpacing: ScreenUtil().setWidth(0.5),
                                    color: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .color,
                                    fontSize: ScreenUtil()
                                        .setSp(20, allowFontScalingSelf: true)),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              child: Icon(Icons.refresh,
                                  color: Colors.blueAccent, size: 20),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            GestureDetector(
                              onTap: resetAll,
                              child: Container(
                                child: Text(
                                  "RESET ALL",
                                  style: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontStyle: FontStyle.normal,
                                      fontFamily: 'Roboto',
                                      letterSpacing: ScreenUtil().setWidth(0.5),
                                      color: Colors.blueAccent,
                                      fontSize: ScreenUtil().setSp(14,
                                          allowFontScalingSelf: true)),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          width: ScreenUtil().setWidth(577),
                          height: ScreenUtil().setHeight(100),
                          color: Theme.of(context).scaffoldBackgroundColor,
                          child: DottedBorder(
                            color: _themeChanger.getTheme().primaryColor ==
                                    Color(0xff182e42)
                                ? Color(0xfff5f5f5)
                                : _themeChanger.getTheme().primaryColor ==
                                        Color(0xfff5f5f5)
                                    ? Color(0xff172636)
                                    : null,
                            dashPattern: [1],
                            strokeWidth: 1,
                            child: Scrollbar(
                              child: SingleChildScrollView(
                                physics: ScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                child: Container(
                                    child: Wrap(
                                        spacing: 8,
                                        children: List.generate(
                                            _selectedChoices?.entries?.length,
                                            (index) {
                                          return userSelection(
                                              choice: _selectedChoices?.entries
                                                  ?.elementAt(index)
                                                  ?.value);
                                        }))),
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                )),
                SizedBox(
                  height: ScreenUtil().setHeight(10),
                ),
                Container(
                    width: ScreenUtil().setWidth(1061),
                    height: ScreenUtil().setHeight(450),
                    color: Theme.of(context).scaffoldBackgroundColor,
                    child: DottedBorder(
                      color: _themeChanger.getTheme().primaryColor ==
                              Color(0xff182e42)
                          ? Color(0xfff5f5f5)
                          : _themeChanger.getTheme().primaryColor ==
                                  Color(0xfff5f5f5)
                              ? Color(0xff172636)
                              : null,
                      dashPattern: [1],
                      strokeWidth: 1,
                      child: Scrollbar(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Container(
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Text(
                                            'Inbound Shipper',
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'Roboto',
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color,
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf:
                                                        true)),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        CustomDropdown(
                                          dropdownWidth: 230,
                                          hintText: "Inbound Shipper",
                                          itemList: inboundShipperList,
                                          SelectedValue:
                                              _selectedInboundShipper,
                                          Ontap: (String selectedValue) {
                                            setChoice(
                                                type: APP_CONST
                                                    .Filter_By_InboundShipper,
                                                choice: selectedValue);
                                            _searchFilterDto.dto[APP_CONST
                                                    .Filter_By_InboundShipper] =
                                                selectedValue;
                                            setState(() {
                                              _selectedInboundShipper =
                                                  selectedValue;
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Text(
                                            'Inbound Consignee',
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'Roboto',
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color,
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf:
                                                        true)),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        CustomDropdown(
                                          dropdownWidth: 230,
                                          hintText: "Inbound Consignee",
                                          itemList: inboundConsigneeList,
                                          SelectedValue:
                                              _selectedInboundConsignee,
                                          Ontap: (String selectedValue) {
                                            setChoice(
                                                type: APP_CONST
                                                    .Filter_By_InboundConsignee,
                                                choice: selectedValue);
                                            _searchFilterDto.dto[APP_CONST
                                                    .Filter_By_InboundConsignee] =
                                                selectedValue;
                                            setState(() {
                                              _selectedInboundConsignee =
                                                  selectedValue;
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Text(
                                            'Products',
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'Roboto',
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color,
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf:
                                                        true)),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        CustomDropdown(
                                          dropdownWidth: 230,
                                          hintText: "Products",
                                          itemList: productList
                                              .map((e) => e.productName)
                                              .toList(),
                                          SelectedValue: _selectedProduct,
                                          Ontap: (String selectedValue) {
                                            setChoice(
                                                type:
                                                    APP_CONST.Filter_By_Product,
                                                choice: selectedValue);
                                            _searchFilterDto.dto[APP_CONST
                                                    .Filter_By_Product] =
                                                selectedValue;
                                            setState(() {
                                              _selectedProduct = selectedValue;
                                            });
                                          },
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Text(
                                            'Outbound Shipper',
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'Roboto',
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color,
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf:
                                                        true)),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        CustomDropdown(
                                          dropdownWidth: 230,
                                          hintText: "Outbound Shipper",
                                          itemList: outboundShipperList,
                                          SelectedValue:
                                              _selectedOutboundShipper,
                                          Ontap: (String selectedValue) {
                                            _filterOutboundConsignee(
                                                selectedOutboundShipper:
                                                    selectedValue);
                                            setChoice(
                                                type: APP_CONST
                                                    .Filter_By_OutboundShipper,
                                                choice: selectedValue);
                                            _searchFilterDto.dto[APP_CONST
                                                    .Filter_By_OutboundShipper] =
                                                selectedValue;
                                            setState(() {
                                              _selectedOutboundShipper =
                                                  selectedValue;
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Text(
                                            'Outbound Consignee',
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'Roboto',
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color,
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf:
                                                        true)),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        CustomDropdown(
                                          dropdownWidth: 230,
                                          hintText: "Outbound Consignee",
                                          itemList:
                                              filteredOutboundConsigneeList ??
                                                  [],
                                          SelectedValue:
                                              _selectedOutboundConsignee ??
                                                  null,
                                          Ontap: (String selectedValue) {
                                            setChoice(
                                                type: APP_CONST
                                                    .Filter_By_OutboundConsignee,
                                                choice: selectedValue);
                                            _searchFilterDto.dto[APP_CONST
                                                    .Filter_By_OutboundConsignee] =
                                                selectedValue;
                                            setState(() {
                                              _selectedOutboundConsignee =
                                                  selectedValue;
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Text(
                                            'Equipment Groups',
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'Roboto',
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color,
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf:
                                                        true)),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        CustomDropdown(
                                          dropdownWidth: 230,
                                          hintText: "Equipment Groups",
                                          itemList: equipmentGroupList,
                                          SelectedValue:
                                              _selectedEquipmentGroup,
                                          Ontap: (String selectedValue) {
                                            print("$_selectedEquipmentGroup");
                                            setChoice(
                                                type: APP_CONST
                                                    .Filter_By_EquipmentGroup,
                                                choice: selectedValue);
                                            _searchFilterDto.dto[APP_CONST
                                                    .Filter_By_EquipmentGroup] =
                                                selectedValue;
                                            setState(() {
                                              _selectedEquipmentGroup =
                                                  selectedValue;
                                            });
                                          },
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Column(
                                      children: [
                                        Row(
                                          children: [
                                            Column(
                                              children: [
                                                Container(
                                                  width: ScreenUtil()
                                                      .setWidth(104),
                                                  child: FilterChip(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        side: BorderSide(
                                                            width: 1),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                      selected: _isDefective,
                                                      label: Text('Defective'),
                                                      onSelected:
                                                          (bool selected) {
                                                        setState(() {
                                                          _isDefective =
                                                              !_isDefective;
                                                        });
                                                        _isDefective
                                                            ? _searchFilterDto
                                                                        .dto[
                                                                    APP_CONST
                                                                        .Filter_By_Defective] =
                                                                _isDefective
                                                            : _searchFilterDto
                                                                .dto
                                                                .remove(APP_CONST
                                                                    .Filter_By_Defective);
                                                        _isDefective
                                                            ? setChoice(
                                                                type: APP_CONST
                                                                    .Filter_By_Defective,
                                                                choice:
                                                                    'Defective')
                                                            : _selectedChoices
                                                                .remove(APP_CONST
                                                                    .Filter_By_Defective);
                                                      }),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: [
                                                Container(
                                                  width: ScreenUtil()
                                                      .setWidth(104),
                                                  child: FilterChip(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        side: BorderSide(
                                                            width: 1),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                      selected: _hasBOL,
                                                      label: Text('Has BOL'),
                                                      onSelected:
                                                          (bool selected) {
                                                        setState(() {
                                                          _hasBOL = !_hasBOL;
                                                        });
                                                        _hasBOL
                                                            ? _searchFilterDto
                                                                        .dto[
                                                                    APP_CONST
                                                                        .Filter_By_HasBOL] =
                                                                _hasBOL
                                                            : _searchFilterDto
                                                                .dto
                                                                .remove(APP_CONST
                                                                    .Filter_By_HasBOL);
                                                        _hasBOL
                                                            ? setChoice(
                                                                type: APP_CONST
                                                                    .Filter_By_HasBOL,
                                                                choice:
                                                                    'Has BOL')
                                                            : _selectedChoices
                                                                .remove(APP_CONST
                                                                    .Filter_By_HasBOL);
                                                      }),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Column(
                                              children: [
                                                Container(
                                                  width: ScreenUtil()
                                                      .setWidth(104),
                                                  child: FilterChip(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        side: BorderSide(
                                                            width: 1),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                      selected: _hasComments,
                                                      label:
                                                          Text('Has Comments'),
                                                      onSelected:
                                                          (bool selected) {
                                                        setState(() {
                                                          _hasComments =
                                                              !_hasComments;
                                                        });

                                                        _hasComments
                                                            ? _searchFilterDto
                                                                        .dto[
                                                                    APP_CONST
                                                                        .Filter_By_HasComments] =
                                                                _hasComments
                                                            : _searchFilterDto
                                                                .dto
                                                                .remove(APP_CONST
                                                                    .Filter_By_HasComments);

                                                        _hasComments
                                                            ? setChoice(
                                                                type: APP_CONST
                                                                    .Filter_By_HasComments,
                                                                choice:
                                                                    'Has Comments')
                                                            : _selectedChoices
                                                                .remove(APP_CONST
                                                                    .Filter_By_HasComments);
                                                      }),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: [
                                                Container(
                                                  width: ScreenUtil()
                                                      .setWidth(104),
                                                  child: FilterChip(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        side: BorderSide(
                                                            width: 1),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                      selected: _isHazardous,
                                                      label: Text('Hazardous'),
                                                      onSelected:
                                                          (bool selected) {
                                                        setState(() {
                                                          _isHazardous =
                                                              !_isHazardous;
                                                        });
                                                        _isHazardous
                                                            ? _searchFilterDto
                                                                        .dto[
                                                                    APP_CONST
                                                                        .Filter_By_Hazardous] =
                                                                _isHazardous
                                                            : _searchFilterDto
                                                                .dto
                                                                .remove(APP_CONST
                                                                    .Filter_By_Hazardous);

                                                        _isHazardous
                                                            ? setChoice(
                                                                type: APP_CONST
                                                                    .Filter_By_Hazardous,
                                                                choice:
                                                                    'Hazardous')
                                                            : _selectedChoices
                                                                .remove(APP_CONST
                                                                    .Filter_By_Hazardous);
                                                      }),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Column(
                                              children: [
                                                Container(
                                                  width: ScreenUtil()
                                                      .setWidth(104),
                                                  child: FilterChip(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        side: BorderSide(
                                                            width: 1),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                      selected: _isOnOrder,
                                                      label: Text('On Order'),
                                                      onSelected:
                                                          (bool selected) {
                                                        setChoice(
                                                            type: APP_CONST
                                                                .Filter_By_OnOrder,
                                                            choice: 'On Order');
                                                        setState(() {
                                                          _isOnOrder =
                                                              !_isOnOrder;
                                                        });

                                                        _isOnOrder
                                                            ? _searchFilterDto
                                                                        .dto[
                                                                    APP_CONST
                                                                        .Filter_By_OnOrder] =
                                                                _isOnOrder
                                                            : _searchFilterDto
                                                                .dto
                                                                .remove(APP_CONST
                                                                    .Filter_By_OnOrder);

                                                        _isOnOrder
                                                            ? setChoice(
                                                                type: APP_CONST
                                                                    .Filter_By_OnOrder,
                                                                choice:
                                                                    'On Order')
                                                            : _selectedChoices
                                                                .remove(APP_CONST
                                                                    .Filter_By_OnOrder);
                                                      }),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: [
                                                Container(
                                                  width: ScreenUtil()
                                                      .setWidth(104),
                                                  child: FilterChip(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        side: BorderSide(
                                                            width: 1),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                      selected:
                                                          _isVCFCalculated,
                                                      label: Text(
                                                          'VCF Calculated'),
                                                      onSelected:
                                                          (bool selected) {
                                                        setChoice(
                                                            type: APP_CONST
                                                                .Filter_By_VCFCalculated,
                                                            choice:
                                                                "VCF Calculated");
                                                        setState(() {
                                                          _isVCFCalculated =
                                                              !_isVCFCalculated;
                                                        });

                                                        _isVCFCalculated
                                                            ? _searchFilterDto
                                                                        .dto[
                                                                    APP_CONST
                                                                        .Filter_By_VCFCalculated] =
                                                                _isVCFCalculated
                                                            : _searchFilterDto
                                                                .dto
                                                                .remove(APP_CONST
                                                                    .Filter_By_VCFCalculated);

                                                        _isVCFCalculated
                                                            ? setChoice(
                                                                type: APP_CONST
                                                                    .Filter_By_VCFCalculated,
                                                                choice:
                                                                    'VCF Calculated')
                                                            : _selectedChoices
                                                                .remove(APP_CONST
                                                                    .Filter_By_VCFCalculated);
                                                      }),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Text(
                                            'Inspection Status',
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'Roboto',
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color,
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf:
                                                        true)),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        CustomDropdown(
                                          dropdownWidth: 230,
                                          hintText: "Inspection Status",
                                          itemList: _inspectionStatusList,
                                          SelectedValue: _selectedInspection,
                                          Ontap: (String selectedValue) {
                                            print("$selectedValue");
                                            setChoice(
                                                type: APP_CONST
                                                    .Filter_By_Inspection,
                                                choice: selectedValue);
                                            _searchFilterDto.dto[APP_CONST
                                                    .Filter_By_Inspection] =
                                                selectedValue;
                                            setState(() {
                                              _selectedInspection =
                                                  selectedValue;
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Text(
                                            'Block To',
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'Roboto',
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color,
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf:
                                                        true)),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        CustomDropdown(
                                          dropdownWidth: 230,
                                          hintText: "Block To",
                                          itemList: blockToList
                                              .map((e) =>
                                                  e.railcarBlockToCodeName)
                                              .toList(),
                                          SelectedValue:
                                              _selectedBlockTo ?? null,
                                          Ontap: (String selectedValue) {
                                            setChoice(
                                                type: APP_CONST
                                                    .Filter_By_Block_To,
                                                choice: selectedValue);
                                            _searchFilterDto.dto[APP_CONST
                                                    .Filter_By_Block_To] =
                                                blockToList
                                                    .singleWhere((element) =>
                                                        element
                                                            .railcarBlockToCodeName ==
                                                        selectedValue)
                                                    .id;
                                            setState(() {
                                              _selectedBlockTo = selectedValue;
                                            });
                                            //onPurchaseOrderChanged(selectedValue);
                                          },
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        Container(
                                          child: Text(
                                            'Load Status',
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'Roboto',
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color,
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf:
                                                        true)),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          child: Wrap(
                                            spacing: 8,
                                            children: List.generate(
                                                _choicesList.length, (index) {
                                              return ChoiceChip(
                                                labelPadding:
                                                    EdgeInsets.all(2.0),
                                                label: Text(
                                                  _choicesList[index],
                                                  style: TextStyle(
                                                      fontFamily: 'Roboto',
                                                      fontSize: 14,
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      color: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1
                                                          .color),
                                                ),
                                                selected:
                                                    defaultChoiceIndex == index,
                                                selectedColor:
                                                    Colors.blueAccent,
                                                onSelected: (value) {
                                                  setChoice(
                                                      type: APP_CONST
                                                          .Filter_By_LoadAll,
                                                      choice:
                                                          _choicesList[index]);

                                                  setState(() {
                                                    defaultChoiceIndex = value
                                                        ? index
                                                        : defaultChoiceIndex;
                                                    _searchFilterDto.dto[
                                                        APP_CONST
                                                            .Filter_By_Loaded];
                                                  });
                                                  if (_choicesList[index]
                                                      .contains("All")) {
                                                    _searchFilterDto.dto[APP_CONST
                                                            .Filter_By_LoadAll] =
                                                        APP_CONST
                                                            .Filter_By_LoadAll;
                                                  } else if (_choicesList[index]
                                                      .contains("Loaded")) {
                                                    _searchFilterDto.dto[APP_CONST
                                                            .Filter_By_Loaded] =
                                                        APP_CONST
                                                            .Filter_By_Loaded;
                                                  } else if (_choicesList[index]
                                                      .contains("Empty")) {
                                                    _searchFilterDto.dto[APP_CONST
                                                            .Filter_By_LoadEmpty] =
                                                        APP_CONST
                                                            .Filter_By_LoadEmpty;
                                                  }

                                                  // _choicesList[index].contains("All")?
                                                  // _searchFilterDto.dto[
                                                  // APP_CONST
                                                  //     .Filter_By_LoadAll]:
                                                  // _choicesList[index].contains("Loaded") ?
                                                  // _searchFilterDto.dto[
                                                  // APP_CONST
                                                  //     .Filter_By_Loaded]:
                                                  // _searchFilterDto.dto[
                                                  // APP_CONST
                                                  //     .Filter_By_LoadEmpty];
                                                },
                                                // backgroundColor: color,
                                                elevation: 1,
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 10),
                                              );
                                            }),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ))
              ],
            )),
        actions: <Widget>[
          Container(
            width: ScreenUtil().setWidth(97),
            height: ScreenUtil().setHeight(48),
            child: FlatButton(
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                        color: Theme.of(context).textTheme.bodyText1.color,
                        width: 1,
                        style: BorderStyle.solid),
                    borderRadius: BorderRadius.circular(1)),
                onPressed: () {
                  widget._listViewBloc.add(ListViewInitialDataLoad());
                  Navigator.of(context).pop(ListviewSearchFiltersOptions.apply);
                },
                child: Text(
                  'CANCEL',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: ScreenUtil().setHeight(14),
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w500,
                      letterSpacing: ScreenUtil().setWidth(1.25),
                      color: _themeChanger.getTheme().primaryColor ==
                              Color(0xFF4468BE)
                          ? Color(0xfff5f5f5)
                          : _themeChanger.getTheme().primaryColor ==
                                  Color(0xfff5f5f5)
                              ? Color(0xff172636)
                              : null),
                ),
                color: Colors.transparent,
                //textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                splashColor: Color(0xFF3e8aeb)),
          ),
          SizedBox(
            width: 24,
          ),
          Container(
            width: ScreenUtil().setWidth(160),
            height: ScreenUtil().setHeight(48),
            child: FlatButton(
                onPressed: validateFilters,
                child: Text(
                  'APPLY',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: ScreenUtil().setHeight(14),
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w500,
                      letterSpacing: ScreenUtil().setWidth(1.25),
                      color: Colors.white),
                ),
                color: Color(0xFF508be4),
                textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                splashColor: Color(0xFF3e8aeb)),
          ),
        ],
      ),
    );
  }
}
