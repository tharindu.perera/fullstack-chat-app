import 'dart:convert';

LoadVCFModel loadVCFModelFromMap(String str) =>
    LoadVCFModel.fromMap(json.decode(str));

String loadVCFModelToMap(LoadVCFModel data) => json.encode(data.toMap());

class LoadVCFModel {
  LoadVCFModel(
      {this.equipment_id,
      this.asset_master_id,
      this.product_id,
      this.product_name,
      this.gravity,
      this.gravity_type_id,
      this.observed_temp,
      this.heel,
      this.heel_gallons,
      this.heel_uom,
      this.actual_outage_inches,
      this.actual_outage_gallons,
      this.id,
      this.scy_pk,
      this.parent_id,
      this.vcf,
      this.vcf_max_temp,
      this.max_temp,
      this.density,
      this.adj_gallon_per_vcf,
      this.adj_gallon_protect_weight,
      this.min_outage_inches,
      this.min_outage_gallons,
      this.max_gross_gallons,
      this.load_to_gallons,
      this.gross_gallons,
      this.net_gallons,
      this.variance,
      this.min_dot_outage,
      this.safety_outage,
      this.vcf_header_description,
      this.product_weight,
      this.total_freight_weight,
      this.created_dt,
      this.updated_dt,
      this.source,
      this.facility_id,
      this.max_temperature_rating,
      this.facility_visit_id,
      this.transaction_status,
      this.observed_temperature_scale});

  String equipment_id;
  String asset_master_id;
  String product_id;
  String product_name;
  String gravity;
  String gravity_type_id;
  String observed_temp;
  String heel;
  String heel_gallons;
  String heel_uom;
  String actual_outage_inches;
  String actual_outage_gallons;
  DateTime created_dt;
  DateTime updated_dt;
  String id;
  String scy_pk;
  String parent_id;
  String vcf;
  String vcf_max_temp;
  String max_temp;
  String density;
  String adj_gallon_per_vcf;
  String adj_gallon_protect_weight;
  String min_outage_inches;
  String min_outage_gallons;
  String max_gross_gallons;
  String load_to_gallons;
  String gross_gallons;
  String net_gallons;
  String variance;
  String min_dot_outage;
  String safety_outage;
  String vcf_header_description;
  String product_weight;
  String total_freight_weight;
  String source;
  String permission_id;
  String facility_id;
  String facility_visit_id;
  String operation_type;
  String user;
  String equipInit;
  String equipNum;
  String transaction_status;
  String observed_temperature_scale;
  String max_temperature_rating;

  LoadVCFModel.fromMap(Map<String, dynamic> map)
      : equipment_id = map['equipment_id'],
        product_id = map['product_id'],
        product_name = map['product_name'],
        gravity = map['gravity'],
        gravity_type_id = map['gravity_type_id'],
        observed_temp = map['observed_temp'],
        heel = map['heel'],
        heel_gallons = map['heel_gallons'],
        heel_uom = map['heel_uom'],
        actual_outage_inches = map['actual_outage_post_load_in'],
        actual_outage_gallons = map['actual_outage_post_load_gallon'],
        operation_type = map['operation_type'],
        source = map['source'],
        id = map['id'],
        scy_pk = map['scy_pk'],
        parent_id = map['parent_id'],
        vcf = map['vcf_observed_temperature'],
        vcf_max_temp = map['vcf_max_temp'],
        max_temp = map['max_temp'],
        density = map['density'],
        adj_gallon_per_vcf = map['adj_gallon_per_vcf'],
        adj_gallon_protect_weight = map['adj_gallon_protect_weight'],
        min_outage_inches = map['min_outage_pre_load_in'],
        min_outage_gallons = map['min_outage_pre_load_gallon'],
        max_gross_gallons = map['max_gross_gallons'],
        load_to_gallons = map['load_to_gallons'],
        gross_gallons = map['gross_gallons'],
        net_gallons = map['net_gallons'],
        variance = map['variance'],
        min_dot_outage = map['min_dot_outage'],
        safety_outage = map['safety_outage'],
        vcf_header_description = map['vcf_header_description'],
        product_weight = map['product_weight'],
        total_freight_weight = map['total_freight_weight'],
        asset_master_id = map['asset_master_id'],
        user = map['user'],
        facility_id = map['facility_id'],
        facility_visit_id = map['facility_visit_id'],
        created_dt = map['created_dt'] == null
            ? null
            : DateTime.parse(map['created_dt']),
        updated_dt = map['updated_dt'] == null
            ? null
            : DateTime.parse(map['updated_dt']),
        permission_id = map['permission_id'],
        transaction_status = map['transaction_status'],
        max_temperature_rating = map['max_temperature_rating'],
        observed_temperature_scale = map['observed_temperature_scale'];

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "scy_pk": scy_pk == null ? null : scy_pk,
        "parent_id": parent_id == null ? null : parent_id,
        "asset_master_id": asset_master_id == null ? null : asset_master_id,
        "equipment_id": equipment_id == null ? null : equipment_id,
        "operation_type": operation_type == null ? null : operation_type,
        "source": source == null ? null : source,
        "user": user == null ? null : user,
        "product_id": product_id == null ? null : product_id,
        "product_name": product_name == null ? null : product_name,
        "gravity": gravity == null ? null : gravity,
        "gravity_type_id": gravity_type_id == null ? null : gravity_type_id,
        "observed_temp": observed_temp == null ? null : observed_temp,
        "heel": heel == null ? null : heel,
        "heel_gallons": heel_gallons == null ? null : heel_gallons,
        "heel_uom": heel_uom == null ? null : heel_uom,
        "actual_outage_post_load_in": actual_outage_inches == null ? null : actual_outage_inches,
        "actual_outage_post_load_gallon": actual_outage_gallons == null ? null : actual_outage_gallons,
        "vcf_observed_temperature": vcf == null ? null : vcf,
        "vcf_max_temp": vcf_max_temp == null ? null : vcf_max_temp,
        "max_temp": max_temp == null ? null : max_temp,
        "density": density == null ? null : density,
        "adj_gallon_per_vcf": adj_gallon_per_vcf == null ? null : adj_gallon_per_vcf,
        "adj_gallon_protect_weight": adj_gallon_protect_weight == null ? null : adj_gallon_protect_weight,
        "min_outage_pre_load_in": min_outage_inches == null ? null : min_outage_inches,
        "min_outage_pre_load_gallon": min_outage_gallons == null ? null : min_outage_gallons,
        "max_gross_gallons": max_gross_gallons == null ? null : max_gross_gallons,
        "load_to_gallons": load_to_gallons == null ? null : load_to_gallons,
        "gross_gallons": gross_gallons == null ? null : gross_gallons,
        "net_gallons": net_gallons == null ? null : net_gallons,
        "variance": variance == null ? null : variance,
        "min_dot_outage": min_dot_outage == null ? null : min_dot_outage,
        "safety_outage": safety_outage == null ? null : safety_outage,
        "vcf_header_description": vcf_header_description == null ? null : vcf_header_description,
        "product_weight": product_weight == null ? null : product_weight,
        "total_freight_weight": total_freight_weight == null ? null : total_freight_weight,
        "facility_id": facility_id == null ? null : facility_id,
        "facility_visit_id": facility_visit_id == null ? null : facility_visit_id,
        "permission_id": permission_id == null ? null : permission_id,
        "created_dt": created_dt == null ? null : created_dt.toString(),
        "updated_dt": updated_dt == null ? null : updated_dt.toString(),
        "transaction_status": transaction_status == null ? null : transaction_status.toString(),
        "max_temperature_rating": max_temperature_rating == null ? null : max_temperature_rating  .toString(),
        "observed_temperature_scale": observed_temperature_scale == null ? null : observed_temperature_scale.toString(),
      };

  @override
  String toString() {
    return 'LoadVCFModel{id: $id, equipment_id: $equipment_id}';
  }
}
