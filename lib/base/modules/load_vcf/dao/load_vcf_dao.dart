import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/repositories/offline_data_repository.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/load_vcf/model/load_vcf_model.dart';
import 'package:mbase/env.dart';
import 'package:uuid/uuid.dart';

class LoadVCFDAO extends BaseDao {
  LoadVCFDAO() : super("load_vcf");

  Future<void> save({LoadVCFModel loadVCFModel}) async {
    if (loadVCFModel.id == null) {
      loadVCFModel.id = Uuid().v1();
    }

    return OfflineDataRepository.persistToDataStore(loadVCFModel, collection);
  }

  Future<QuerySnapshot> checkExistingLoadVCF(LoadVCFModel loadVCFModel) async {
    try {
      QuerySnapshot qShot3 = await BaseQuery()
          .where("asset_master_id", isEqualTo: loadVCFModel.asset_master_id).get();
      return qShot3;
    } on Exception {
      throw Exception('Error in checking Equipment Number');
    }
  }

  Future<LoadVCFModel> getLoadVCF(LoadVCFModel loadVCFModel) async {
    print("fetching Load vcf data ....");
    try {
      QuerySnapshot qShotUser = await BaseQuery()
          .where("facility_visit_id", isEqualTo: loadVCFModel.facility_visit_id).where("asset_master_id", isEqualTo: loadVCFModel.asset_master_id)
          .where("permission_id", isEqualTo: env.userProfile.id).get();
      if (qShotUser.docs.isNotEmpty) {
        return LoadVCFModel.fromMap(qShotUser.docs[0].data());
      } else {
        QuerySnapshot qShotPublic = await BaseQuery()
            .where("facility_visit_id", isEqualTo: loadVCFModel.facility_visit_id).where("asset_master_id", isEqualTo: loadVCFModel.asset_master_id)
            .where("permission_id", isEqualTo: APP_CONST.PUBLIC).get();
        if (qShotPublic.docs.isNotEmpty) {
          LoadVCFModel toReturn = LoadVCFModel.fromMap(qShotPublic.docs[0].data());
          toReturn.parent_id = toReturn.id;
          toReturn.id = null;
          return toReturn;
        } else {
          return null;
        }
      }
    } on Exception {
      throw Exception('Error in fetching Load VCF for Asset master Id: '+ loadVCFModel.asset_master_id);
    }
  }
}
