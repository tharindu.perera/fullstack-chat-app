import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/outage_model.dart';
import 'package:mbase/base/core/common_base/model/product_attributes_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/common_base/model/uom.dart';
import 'package:mbase/base/core/common_base/model/vcf_header_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/util/nullCheckUtil.dart';
import 'package:mbase/base/core/util/util_functions.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/ui/loadcar.dart';
import 'package:mbase/base/modules/load_vcf/model/load_vcf_model.dart';
import 'package:mbase/base/modules/load_vcf/repository/load_vcf_repository.dart';
import 'package:mbase/base/modules/vcf_table/vcf_handler.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:provider/provider.dart';
import 'dart:async';
import 'package:mbase/base/core/util/common_vcf.dart';

enum UomTypes { In, Gal }

enum OutageConversion {
  MinOutageInches,
  HeelGallons,
  HeelInches,
  ActualOutageGallons
}

// ignore: must_be_immutable
class ProcessLoadVCF extends StatefulWidget {
  Equipment equipment;
  AssetMasterDataModel assetMasterModel;
  ProductModel currentProductDetails;

  ProcessLoadVCF(Equipment equipment, AssetMasterDataModel assetMasterModel,
      ProductModel currentProductDetails) {
    print(equipment);
    this.equipment = equipment;
    this.assetMasterModel = assetMasterModel;
    this.currentProductDetails = currentProductDetails;
  }

  @override
  _ProcessLoadVCFState createState() =>
      _ProcessLoadVCFState(equipment, assetMasterModel);
}

class _ProcessLoadVCFState extends State<ProcessLoadVCF> {
  // final formatter = new NumberFormat('#,###,###');
  final formatter = new NumberFormat('###,000');

  LoadVCFModel loadVCFModel = new LoadVCFModel();
  Equipment equipment;
  ProductAttributesModel selectedProduct;
  AssetMasterDataModel selectedAssetMasterData;
  ProductModel railCarProduct;
  UOM gallonsUOM;

  LoadVCFRepository repo = new LoadVCFRepository();

  CommonDao commonDao = new CommonDao();

  VCFHandler vcfHandler = new VCFHandler();

  List<Equipment> selectedEquipmentList;
  List<ProductAttributesModel> productList = [];

  Timer _debounce;
  FToast _fToast;

  final _preloadFormKey = GlobalKey<FormState>();
  final _postloadFormKey = GlobalKey<FormState>();

  bool _preLoadAutoValidate = false;
  bool _postLoadAutoValidate = false;

  bool _validateGravityField = false;
  bool _validateTemperatureField = false;
  bool _validateActualOutageField = false;
  bool _validateGrossGallons = false;
  bool _validateProductWeight = false;
  bool _validateNetGallons = false;
  bool _isPostLoadDisabled = true;

  bool isPreCalculationSuccess = false;
  bool isPostCalculationSuccess = false;

  bool isPreCalculateButtonClicked = false;
  bool isPostCalculateButtonClicked = false;
  List<String> warningList = new List();

  //asset master data fields
  double gallon_capacity;
  String get gallon_capacity_ui => this.gallon_capacity != null
      ? formatter.format(this.gallon_capacity)
      : '';
  double tare_weight;
  String get tare_weight_ui =>
      this.tare_weight != null ? formatter.format(this.tare_weight) : '';
  double load_limit;
  String get load_limit_ui =>
      this.load_limit != null ? formatter.format(this.load_limit) : '';
  String dot_number;

  //user inputs
  final _gravityController = TextEditingController(); //specific gravity
  final _observedTempController =
      TextEditingController(); //observed temperature
  final _heelController = TextEditingController(); //heel
  final _outageController = TextEditingController(); //actual outage
  UomTypes _heel_uom = UomTypes.In; //UOM, default is Inches

  //pulled values
  double vcf_max; //Pull from ASTM Table based on Max Temp, Specific Gravity
  double vcf; //Pull from ASTM Table based on Observed Temp, Specific Gravity
  int max_temp_ref; //Pull from Product Attributes Table, winter/summer
  String
      max_reference_temp_scale; //Pull from Product Attributes Table, winter/summer
  double dot_outage_percentage; //Pull from Product Attributes
  double safety_outage_percentage; //Pull from Product Attributes
  String vcf_header_description; //Pull from Product Attributes
  String gravity_type; //Pull from Product Attributes

  //calculated values
  String get specific_gravity => 'API' == this.gravity_type
      ? Util.convertApiToSpecificGravity(this._gravityController.text)
      : this._gravityController.text;

  double get density => Util.doubleSafetyParse(specific_gravity) != null
      ? Util.decimalPoints(
          Util.doubleSafetyParse(specific_gravity) * 8.32828, 4)
      : null;

  int max_gross_gallons;
  String get max_gross_gallons_ui => this.max_gross_gallons != null
      ? formatter.format(this.max_gross_gallons)
      : '';

  int min_outage_gallons;

  double min_outage_inches;
  String get min_outage_inches_ui =>
      this.min_outage_inches != null ? this.min_outage_inches.toString() : '';

  double max_net_gallons;

  int load_to_gallons;
  String get load_to_gallons_ui => this.load_to_gallons != null
      ? formatter.format(this.load_to_gallons)
      : '';

  int adj_gallons_per_vcf; //gallon_capacity * vcf
  int get adj_gallons_protect_weight => density != null
      ? Util.nearestRound(load_limit / density)
      : null; // load_limit / density

  double heel_gallons;
  double heel_inches;

  double actual_outage_inches;
  double actual_outage_gallons;

  int gross_gallons;
  String get gross_gallons_ui =>
      this.gross_gallons != null ? formatter.format(this.gross_gallons) : '';

  int product_weight;
  String get product_weight_ui =>
      this.product_weight != null ? formatter.format(this.product_weight) : '';

  int net_gallons;
  String get net_gallons_ui =>
      this.net_gallons != null ? formatter.format(this.net_gallons) : '';

  int total_freight_weight;
  String get total_freight_weight_ui => this.total_freight_weight != null
      ? formatter.format(this.total_freight_weight)
      : '';

  int variance; //Max gross gallons - gross gallons

  _ProcessLoadVCFState(
      Equipment equipment, AssetMasterDataModel assetMasterModel) {
    this.equipment = equipment;
    this.selectedAssetMasterData = assetMasterModel;
    //for Load-RailCar passing info
    this.selectedEquipmentList = List();
    selectedEquipmentList.add(equipment);
  }

  @override
  void initState() {
    super.initState();
    _fToast = FToast(context);
    _fetchInitialData();
  }

  @override
  void dispose() {
    _debounce?.cancel();
    Loader.hide();
    super.dispose();
  }

  _fetchInitialData() async {
    Loader.show(context,
        progressIndicator: CircularProgressIndicator(),
        overlayColor: Color(0x2FFFFFFF));
    await Future.wait<void>([
      _fetchLoadVCFData(), //fetch existing Load VCF record
      _fetchAssetMasterData(), //fetch Asset Master data information
      _fetchProductList(), //fetch product dropdown list
    ]).whenComplete(() => {_validatePostLoadForm(), Loader.hide()});
  }

  _fetchLoadVCFData() async {
    return repo
        .fetchLoadVCF(
            this.equipment?.assetMasterId, this.equipment?.facilityVisitId)
        .then((returnValue) {
      //if existing load vcf record found
      if (returnValue != null) {
        setState(() {
          this.loadVCFModel = returnValue;
          //load values from existing model
          _fromModel();
          //select product, if product list exists at that time
          if (this.productList.isNotEmpty) {
            this.selectedProduct = productList.firstWhere((element) =>
                element.product_id == this.loadVCFModel?.product_id);
            _applyProductAttributes();
          }
        });
      }
    });
  }

  _fetchAssetMasterData() async {
    if (selectedAssetMasterData == null ||
        selectedAssetMasterData.gallon_capacity == null ||
        selectedAssetMasterData.load_limit == null) {
      Loader.hide();
      Navigator.pop(context);
      if (selectedAssetMasterData.gallon_capacity == null) {
        _showToast(
            toastColor: Color(0xfff34336),
            toastMessage: 'Gallon Capacity does not exist for selected Railcar',
            icon: Icons.new_releases,
            duration: 4);
      } else if (selectedAssetMasterData.load_limit == null) {
        _showToast(
            toastColor: Color(0xfff34336),
            toastMessage: 'Load Limit does not exist for selected Railcar',
            icon: Icons.new_releases,
            duration: 4);
      }
      return;
    }

    setState(() {
      this.gallon_capacity = Util.decimalPointsStr(
          this.selectedAssetMasterData?.gallon_capacity, 3);
      double tareWeight =
          Util.doubleSafetyParse(this.selectedAssetMasterData?.tare_weight);
      this.tare_weight = tareWeight != null ? tareWeight : 0;
      double loadLimit =
          Util.doubleSafetyParse(this.selectedAssetMasterData?.load_limit);
      this.load_limit = loadLimit != null ? loadLimit : 0;
      this.dot_number = this.selectedAssetMasterData?.shipping_contain_spec;
    });
  }

  _fetchProductList() async {
    return repo.fetchProducts().then((returnValue) {
      setState(() {
        this.productList = returnValue;
        //select product, if existing selection exists
        if (this.loadVCFModel.product_id != null) {
          this.selectedProduct = this.productList.firstWhere(
              (element) => element.product_id == this.loadVCFModel.product_id);
          _applyProductAttributes();
        }
      });
    });
  }

  _onProductChange(ProductAttributesModel value) {
    setState(() {
      print('product:$value');
      this.selectedProduct = value;
      if (this.selectedProduct?.gravity != null &&
          "True" ==
              MemoryData.facilityConfiguration
                  .allowToSetGravityFromProductAttributeInLoadVCF) {
        this._gravityController.text = this.selectedProduct?.gravity;
        this.gravity_type = this.selectedProduct?.gravity_type;
      } else {
        this._gravityController.text = '';
        _resetPreLoadValues();
      }
      _applyProductAttributes();
    });
  }

  _applyProductAttributes() async {
    this.gravity_type = this.selectedProduct?.gravity_type;
    bool isSummer = this.selectedProduct?.winter_temp_flag == '0';

    //cross check current date with winter months
    if (!isSummer && !Util.isWinterMonth()) {
      isSummer = true;
    }

    String maxTempValue = isSummer
        ? this.selectedProduct?.max_reference_temp
        : this.selectedProduct?.winter_max_reference_temp;

    String maxTempScale = isSummer
        ? this.selectedProduct?.max_reference_temp_scale
        : this.selectedProduct?.winter_max_reference_temp_scale;

    if (APP_CONST.DOT_SPEC == maxTempValue) {
      if (isNullOrEmpty(this.dot_number)) {
        _showToast(
            toastMessage: 'DOT Spec value is not available',
            toastColor: Colors.red,
            icon: Icons.new_releases,
            duration: 5);
      } else {
        maxTempValue = await repo.fetchTankCarSpecMaxTempByDotNumber(this.dot_number.trim(), isSummer);
        if (maxTempValue.isEmpty) {
          _showToast(
              toastMessage: 'DOT Spec value is not available',
              toastColor: Colors.red,
              icon: Icons.new_releases,
              duration: 5);
        }
      }

    } else {
      if ("C" == maxTempScale) {
        maxTempValue = Util.convertCelciusToFarenhite(maxTempValue);
      }
    }

    if (maxTempValue.isNotEmpty) {
      double maxTemp = Util.doubleSafetyParse(maxTempValue);
      this.max_temp_ref = Util.doubleToInt(maxTemp);
    } else {
      print('Error on capturing Max Temp in loadvcf');
      this.max_temp_ref = null;
    }

    this.max_reference_temp_scale = isSummer
        ? this.selectedProduct?.max_reference_temp_scale
        : this.selectedProduct?.winter_max_reference_temp_scale;
    this.dot_outage_percentage = Util.decimalPoints(
        double.parse(this.selectedProduct?.min_outage) / 100, 2);
    this.safety_outage_percentage =
        double.parse(this.selectedProduct?.safety_outage) / 100;
    // _fetchMaxVCF();
    // _fetchVCF();
  }

  _enterObservedTemperature() {
    setState(() {
      // _fetchVCF();
    });
  }

  _enterGravity() {
    setState(() {
      // _fetchMaxVCF();
      // _fetchVCF();
    });
  }

  _calculatePreLoad() async {
    final FormState form = _preloadFormKey.currentState;

    if (form.validate() && _validatePreLoadFields()) {
      this.isPreCalculateButtonClicked = true;

      Future(() async {
        Loader.show(context,
            progressIndicator: CircularProgressIndicator(),
            overlayColor: Color(0x2FFFFFFF));
        try {
          bool isVCFAvailable = await _fetchVCF();
          bool isMaxVCFAvailable = await _fetchMaxVCF();

          if (isVCFAvailable && isMaxVCFAvailable) {
            setState(() {
              this.adj_gallons_per_vcf =
                  Util.nearestRound(this.vcf_max * this.gallon_capacity);
              int smallestAdjGallons =
                  this.adj_gallons_per_vcf > this.adj_gallons_protect_weight
                      ? this.adj_gallons_protect_weight
                      : this.adj_gallons_per_vcf;
              this.max_gross_gallons = Util.nearestRound(
                  (smallestAdjGallons / this.vcf) *
                      (1 -
                          (this.dot_outage_percentage +
                              this.safety_outage_percentage)));
              this.min_outage_gallons = Util.doubleToInt(
                  this.gallon_capacity - this.max_gross_gallons);
            });

            bool isOutageOk = await _calculateOutage(
                UomTypes.Gal,
                this.min_outage_gallons.toDouble(),
                OutageConversion.MinOutageInches);

            bool isHeelOk = await _convertHeel();
            this.isPreCalculationSuccess = isOutageOk && isHeelOk;
          }
        } catch (err) {
          print('Error on calculating Pre-Load: $err');
          _showToast(
              toastMessage: 'Error on calculation, please verify your inputs',
              toastColor: Colors.red,
              icon: Icons.new_releases,
              duration: 4);
        }
        Loader.hide();
        this.isPreCalculateButtonClicked = false;
      });
    }
  }

  _calculatePostLoad() async {
    final FormState form = _postloadFormKey.currentState;
    if (form.validate() && _validatePostLoadFields()) {
      this.isPostCalculateButtonClicked = true;

      Future(() async {
        Loader.show(context,
            progressIndicator: CircularProgressIndicator(),
            overlayColor: Color(0x2FFFFFFF));
        try {
          setState(() {
            this.actual_outage_inches =
                Util.doubleSafetyParse(this._outageController.text);
          });
          this.isPostCalculationSuccess = await _calculateOutage(UomTypes.In,
              this.actual_outage_inches, OutageConversion.ActualOutageGallons);
          this.isPostCalculateButtonClicked = false;
        } catch (err) {
          print('Error on calculating Post-Load: $err');
          _showToast(
              toastMessage: 'Error on calculation, please verify your inputs',
              toastColor: Colors.red,
              icon: Icons.new_releases,
              duration: 4);
        }
        Loader.hide();
      });
    }
  }

  Future<bool> _calculateOutage(
      UomTypes fromUOM, double fromValue, OutageConversion field) async {
    OutageModel outageModel =
        await repo.fetchOutageByAssetId(this.equipment?.assetMasterId);
    if (null == outageModel ||
        outageModel.details == null ||
        outageModel.details.length == 0) {
      _showToast(
          toastMessage: 'No outage details found for this Railcar',
          toastColor: Colors.red,
          icon: Icons.new_releases,
          duration: 4);
      _resetPreLoadValues();
      return false;
    }

    OutageDetails outageDetailsModel = null;

    if (fromUOM == UomTypes.Gal) {
      outageDetailsModel = CommonFunctions()
          .getMeasurementAmount(outageModel, fromValue?.toString());
    } else if (fromUOM == UomTypes.In) {
      outageDetailsModel = CommonFunctions()
          .getTheOutageAmount(outageModel, fromValue?.toString());
    }

    if (null == outageDetailsModel) {
      String msg = 'Outage Amount does not exist';
      switch (field) {
        case OutageConversion.MinOutageInches:
          msg = 'Measurement does not exist calculated Min Outage (Gallons)';
          _resetPreLoadValues();
          break;
        case OutageConversion.ActualOutageGallons:
          msg = 'Outage does not exist for given Actual Outage (In.)';
          _resetPostLoadValues();
          break;
        case OutageConversion.HeelGallons:
          msg = 'Outage does not exist for given Heel (In.)';
          _resetPreLoadValues();
          break;
        case OutageConversion.HeelInches:
          msg = 'Measurement does not exist for given Heel (Gallons)';
          _resetPreLoadValues();
          break;
      }
      _showToast(
          toastMessage: msg,
          toastColor: Colors.red,
          icon: Icons.new_releases,
          duration: 4);
      return false;
    }

    double toValue = 0;
    if (fromUOM == UomTypes.Gal) {
      toValue = Util.decimalPointsStr(outageDetailsModel?.measurement, 3);
    } else if (fromUOM == UomTypes.In) {
      toValue = Util.decimalPointsStr(outageDetailsModel?.outage_amount, 3);
    }

    _onOutageChange(field, toValue);

    return true;
  }

  _onOutageChange(OutageConversion field, double value) {
    setState(() {
      switch (field) {
        case OutageConversion.MinOutageInches:
          this.min_outage_inches = value;
          break;
        case OutageConversion.ActualOutageGallons:
          this.actual_outage_gallons = value;
          this.gross_gallons = Util.doubleToInt(
              this.gallon_capacity - this.actual_outage_gallons);
          this.net_gallons = Util.nearestRound(this.gross_gallons * this.vcf);
          this.product_weight =
              Util.nearestRound(this.net_gallons * this.density);
          this.variance = this.max_gross_gallons - this.gross_gallons;
          this.total_freight_weight =
              Util.nearestRound(this.product_weight + this.tare_weight);
          _validatePostLoadForm();
          break;
        case OutageConversion.HeelGallons:
          this.heel_gallons = value;
          _calculateLoadsToGallons();
          break;
        case OutageConversion.HeelInches:
          this.heel_inches = value;
          _calculateLoadsToGallons();
          break;
      }
    });
  }

  Future<bool> _convertHeel() async {
    //heel is an optional field
    if (this._heelController.text.isEmpty ||
        this._heelController.text == null) {
      this.heel_gallons = null;
      this.heel_inches = null;
      _calculateLoadsToGallons();
      return true;
    }

    switch (this._heel_uom) {
      case UomTypes.In:
        this.heel_inches = double.parse(_heelController.text);
        return await _calculateOutage(
            UomTypes.In, this.heel_inches, OutageConversion.HeelGallons);
        break;
      case UomTypes.Gal:
        this.heel_gallons = double.parse(_heelController.text);
        this.heel_inches = null;
        _calculateLoadsToGallons();
        return true;
        // return await _calculateOutage(UomTypes.Gal, this.heel_gallons, OutageConversion.HeelInches);
        break;
    }

    return false;
  }

  _calculateLoadsToGallons() {
    if (this.heel_gallons != null) {
      this.load_to_gallons =
          Util.doubleToInt(this.max_gross_gallons - this.heel_gallons);
    } else {
      this.load_to_gallons = this.max_gross_gallons;
    }
  }

  Future<bool> _fetchMaxVCF() async {
    String gravity = 'API' == this.gravity_type
        ? Util.convertApiToSpecificGravity(this._gravityController.text)
        : this._gravityController.text;
    String max_temp =
        this.max_temp_ref != null ? this.max_temp_ref.toString() : '';
    String vcf_header_id = this.selectedProduct?.vcf_header_id;

    if (gravity == null ||
        max_temp == null ||
        vcf_header_id == null ||
        this.max_temp_ref == null ||
        gravity.isEmpty ||
        max_temp.isEmpty ||
        vcf_header_id.isEmpty) {
      _showToast(
          toastMessage: 'Please fill all the mandatory fields',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      return false;
    }

    VCFHeaderModel vcfHeaderModel =
        await repo.fetchVCFHeaderById(vcf_header_id);
    if (null == vcfHeaderModel) {
      _showToast(
          toastMessage: 'No VCF details found for this product',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      this.vcf_max = null;
      _resetPreLoadValues();
      return false;
    }

    double vcfValue =
        await this.vcfHandler.getVCF(vcfHeaderModel, gravity, max_temp);

    if (null == vcfValue || 0 == vcfValue) {
      this.vcf_max = null;
      _showToast(
          toastMessage: 'Gravity does not exist for product\'s max temperature',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      _resetPreLoadValues();
      return false;
    } else {
      this.vcf_max = vcfValue;
    }

    return true;
  }

  Future<bool> _fetchVCF() async {
    String gravity = 'API' == this.gravity_type
        ? Util.convertApiToSpecificGravity(this._gravityController.text)
        : this._gravityController.text;
    String observed_temp = this._observedTempController.text;
    String vcf_header_id = this.selectedProduct?.vcf_header_id;

    if (gravity == null ||
        observed_temp == null ||
        vcf_header_id == null ||
        gravity.isEmpty ||
        observed_temp.isEmpty ||
        vcf_header_id.isEmpty) {
      _showToast(
          toastMessage: 'Please fill all the mandatory fields',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      return false;
    }

    VCFHeaderModel vcfHeaderModel =
        await repo.fetchVCFHeaderById(vcf_header_id);
    if (null == vcfHeaderModel) {
      _showToast(
          toastMessage: 'No VCF details found for this product',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      this.vcf = null;
      _resetPreLoadValues();
      return false;
    }

    this.vcf_header_description = vcfHeaderModel.description;

    double vcfValue =
        await this.vcfHandler.getVCF(vcfHeaderModel, gravity, observed_temp);

    if (null == vcfValue || 0 == vcfValue) {
      _showToast(
          toastMessage: 'Gravity does not exist for the Temperature',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      this.vcf = null;
      _resetPreLoadValues();
      return false;
    } else {
      this.vcf = vcfValue;
    }

    return true;
  }

  _validatePostLoadForm() {
    if (this.gross_gallons == null ||
        this._outageController.text.isEmpty ||
        this.max_gross_gallons == null ||
        this.product_weight == null ||
        this.total_freight_weight == null ||
        this.load_limit == null) {
      this._isPostLoadDisabled = true;
    } else {
      bool validActualOutage = false;

      bool isWarning = false;
      warningList.clear();

      if (double.parse(this._outageController?.text) < this.min_outage_inches) {
        if ('Error' ==
            MemoryData.facilityConfiguration
                .whenActualOutageInchesLessThanMinOutageInches) {
          validActualOutage = true;
        } else if ('Warning' ==
            MemoryData.facilityConfiguration
                .whenActualOutageInchesLessThanMinOutageInches) {
          isWarning = true;
          warningList.add('Actual Outage Inches less that Min Outage Inches');
        }
      }

      this._validateProductWeight = this.gross_gallons < 0;
      if (this.product_weight >= this.load_limit) {
        if ('Error' ==
            MemoryData
                .facilityConfiguration.whenProductWeightGreaterThanLoadLimit) {
          this._validateProductWeight = true;
        } else if ('Warning' ==
            MemoryData
                .facilityConfiguration.whenProductWeightGreaterThanLoadLimit) {
          isWarning = true;
          warningList.add('Product Weight greater than Load Limit');
        }
      }

      if (isWarning) {
        _showToastWarning(warningList);
      }

      this._validateGrossGallons =
          this.gross_gallons >= this.max_gross_gallons ||
              this.gross_gallons < 0;
      this._validateNetGallons = this.net_gallons < 0;
      this._isPostLoadDisabled = validActualOutage ||
          this._validateProductWeight ||
          this._validateGrossGallons ||
          this._validateNetGallons;
    }
  }

  _saveLoadVCF() async {
    if (this.selectedProduct == null) {
      _showToast(
          toastColor: Colors.red,
          toastMessage: 'Product is not selected ',
          icon: Icons.new_releases);
      return;
    }
    //extract screen values to the model
    _toModel();

    try {
      return repo.addLoadVCF(this.loadVCFModel).then((value) {
        return NotificationRepository()
            .addNotification(this.loadVCFModel, APP_CONST.LOAD_VCF);
      }).whenComplete(() => {
            Navigator.of(context).pop(),
            _showToast(
                toastColor: Color(0xff7fae1b),
                toastMessage: 'LoadVCF Added Successfully',
                icon: Icons.check)
          });
    } catch (error) {
      print('error: $error');
    }
  }

  _saveAndLoadRailcar() async {
    //first, save loadVCF record
    _toModel();

    await repo.addLoadVCF(this.loadVCFModel).then((value) {
      return NotificationRepository()
          .addNotification(this.loadVCFModel, APP_CONST.LOAD_VCF);
    }).whenComplete(() => {
          _showToast(
              toastColor: Color(0xff7fae1b),
              toastMessage: 'LoadVCF Added Successfully',
              icon: Icons.check)
        });

    //fetch passing objects to load-railcar screen
    await Future.wait<void>([
      repo
          .fetchProductById(this.selectedProduct.product_id)
          .then((result) => this.railCarProduct = result),
      repo.fetchUOMByUnit('Gallons').then((result) => this.gallonsUOM = result),
    ]);

    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => LoadRailCar(
              selectedEquipmentList,
              APP_CONST.LOAD_VCF,
              context,
              this.railCarProduct,
              this.net_gallons.toDouble(),
              this.gallonsUOM),
        ));
  }

  _pushToLoadRailCar() async {
    bool isOk = await confirmationMessage(this.warningList);
    bool isLiquid = await checkForLiquidProduct();
    bool isProductMatch = await checkForProductMatch();
    print("compartment list ---> ${widget.equipment.compartmentList.isEmpty}");

    if (isOk && widget.equipment.compartmentList.isEmpty) {
      _saveAndLoadRailcar();
    }

    if (isOk && widget.equipment.compartmentList.isNotEmpty) {
      if (isProductMatch) {
        _saveAndLoadRailcar();
      } else {
        _showToast(
            toastColor: Colors.red,
            toastMessage: 'Compartment Cannot Contain Mixed Product',
            icon: Icons.info);
      }
    }

    // else{
    //   _showToast(toastColor: Colors.red, toastMessage: 'Compartment Cannot Contain Mixed Product', icon: Icons.info);
    // }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);

    return Scaffold(appBar: MainAppBar(), drawer: AppDrawer(), body: FormUI());
  }

  Widget FormUI() {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    return Builder(
      builder: (context) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(15.0),
                child: CardUI(
                    content: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Form(
                        //Preload form
                        key: _preloadFormKey,
                        autovalidate: _preLoadAutoValidate,
                        child: SafeArea(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(20),
                                            child: Container(
                                                child: ListTile(
                                              leading: GestureDetector(
                                                  onTap: popUpDialog,
                                                  child: Icon(
                                                    Icons.arrow_back,
                                                  )),
                                              title: Text(
                                                  'LOAD VCF (${equipment.equipmentInitial + ' ' + equipment.equipmentNumber})',
                                                  style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(
                                                          24,
                                                          allowFontScalingSelf:
                                                              true),
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      fontFamily: 'Roboto',
                                                      color: Theme.of(context)
                                                          .textTheme
                                                          // ignore: deprecated_member_use
                                                          .headline
                                                          .color)),
                                            ))),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Padding(
                                              padding: EdgeInsets.all(20),
                                              child: Container(
                                                  child: GestureDetector(
                                                onTap: popUpDialog,
                                                child: RichText(
                                                  text: TextSpan(
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          // ignore: deprecated_member_use
                                                          .body1,
                                                      children: <InlineSpan>[
                                                        TextSpan(
                                                            text: 'CANCEL',
                                                            style: TextStyle(
                                                                fontSize: ScreenUtil()
                                                                    .setSp(14,
                                                                        allowFontScalingSelf:
                                                                            true),
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                letterSpacing:
                                                                    ScreenUtil()
                                                                        .setWidth(
                                                                            1.25),
                                                                fontFamily:
                                                                    'Roboto',
                                                                color: Color(
                                                                    0xFF3e8aeb)))
                                                      ]),
                                                ),
                                              ))),
                                          Padding(
                                              padding: EdgeInsets.only(
                                                  left: 0,
                                                  top: 14,
                                                  right: 24,
                                                  bottom: 14),
                                              child: Container(
                                                width:
                                                    ScreenUtil().setWidth(180),
                                                height:
                                                    ScreenUtil().setHeight(48),
                                                child: FlatButton(
                                                  child: Text(
                                                    'SAVE',
                                                    style: TextStyle(
                                                        fontFamily: 'Roboto',
                                                        fontSize: ScreenUtil()
                                                            .setHeight(14),
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        letterSpacing:
                                                            ScreenUtil()
                                                                .setWidth(1.25),
                                                        color: Colors.white),
                                                  ),
                                                  color: Color(0xFF3e8aeb),
                                                  textColor: Colors.white,
                                                  disabledColor: Colors.grey,
                                                  disabledTextColor:
                                                      Colors.black,
                                                  splashColor:
                                                      Color(0xFF3e8aeb),
                                                  onPressed: _saveLoadVCF,
                                                ),
                                              )),
                                        ],
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              Divider(
                                thickness: 2.0,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Gallon Capacity',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(16,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text('$gallon_capacity_ui',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500))
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Tare Weight (lb)',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(16,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text('$tare_weight_ui',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500))
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Load Limit (lb)',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(16,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text('$load_limit_ui',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500))
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('DOT Number',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(16,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                            dot_number == null ||
                                                    dot_number.isEmpty
                                                ? "-"
                                                : dot_number,
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(20,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500))
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: ScreenUtil().setHeight(10)),
                              Divider(
                                thickness: 2.0,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.all(5),
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 25.0),
                                        alignment: Alignment.center,
                                        child: Text('PRE-LOAD',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(16,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(1),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.bold)),
                                      )),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  SizedBox(width: ScreenUtil().setWidth(20)),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Product',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(14,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          width: ScreenUtil().setWidth(200),
                                          child: DropdownButtonFormField<
                                              ProductAttributesModel>(
                                            value: selectedProduct,
                                            hint: Text(
                                              'Select a Product',
                                            ),
                                            onChanged: (value) => setState(
                                                () => _onProductChange(value)),
                                            validator: (value) => value == null
                                                ? 'Product required'
                                                : null,
                                            items: productList.map<
                                                    DropdownMenuItem<
                                                        ProductAttributesModel>>(
                                                (ProductAttributesModel value) {
                                              return DropdownMenuItem<
                                                  ProductAttributesModel>(
                                                value: value,
                                                child: Text(
                                                  value.product_name,
                                                  style: TextStyle(
                                                      color: _themeChanger
                                                                  .getTheme()
                                                                  .primaryColor ==
                                                              Color(0xff182e42)
                                                          ? Colors.white
                                                          : _themeChanger
                                                                      .getTheme()
                                                                      .primaryColor ==
                                                                  Color(
                                                                      0xfff5f5f5)
                                                              ? Colors.black
                                                              : null),
                                                ),
                                              );
                                            }).toList(),
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.all(5.0),
                                              errorBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Colors.red),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              focusedErrorBorder:
                                                  OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintStyle: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(14),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.25),
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                            "API" == this.gravity_type
                                                ? 'API Gravity'
                                                : 'Specific Gravity',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(14,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          width: ScreenUtil().setWidth(180),
                                          padding: EdgeInsets.symmetric(
                                              vertical: 4.0),
                                          alignment: Alignment.centerLeft,
                                          child: TextFormField(
                                            autovalidate:
                                                _preLoadAutoValidate ||
                                                    _validateGravityField,
                                            keyboardType:
                                                TextInputType.numberWithOptions(
                                                    decimal: true),
                                            inputFormatters: [
                                              FilteringTextInputFormatter.allow(
                                                  RegExp(
                                                      '^\$|^(0|([1-9][0-9]{0,}))(\\.[0-9]{0,})?\$')),
                                            ],
                                            controller: _gravityController,
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Please enter gravity';
                                              }
                                              return null;
                                            },
                                            onTap: () {
                                              setState(() {
                                                _validateGravityField = true;
                                              });
                                            },
                                            onChanged: (string) {
                                              if (_debounce?.isActive ?? false)
                                                _debounce.cancel();
                                              _debounce = Timer(
                                                  const Duration(
                                                      milliseconds: 1500), () {
                                                _enterGravity();
                                              });
                                            },
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .color),
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.all(5.0),
                                              errorBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Colors.red),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              focusedErrorBorder:
                                                  OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintStyle: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(14),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.25),
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Observed Temp (F)',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(14,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          width: ScreenUtil().setWidth(180),
                                          padding: EdgeInsets.symmetric(
                                              vertical: 4.0),
                                          alignment: Alignment.centerLeft,
                                          child: TextFormField(
                                            autovalidate:
                                                _preLoadAutoValidate ||
                                                    _validateTemperatureField,
                                            keyboardType:
                                                TextInputType.numberWithOptions(
                                                    decimal: true),
                                            inputFormatters: [
                                              FilteringTextInputFormatter.allow(
                                                  RegExp(
                                                      '^\$|^(0|([1-9][0-9]{0,}))(\\.[0-9]{0,})?\$')),
                                            ],
                                            controller: _observedTempController,
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Please enter temperature';
                                              }
                                              return null;
                                            },
                                            onTap: () {
                                              setState(() {
                                                _validateTemperatureField =
                                                    true;
                                              });
                                            },
                                            onChanged: (string) {
                                              if (_debounce?.isActive ?? false)
                                                _debounce.cancel();
                                              _debounce = Timer(
                                                  const Duration(
                                                      milliseconds: 1500), () {
                                                _enterObservedTemperature();
                                              });
                                            },
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  .color,
                                            ),
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.all(5.0),
                                              errorBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Colors.red),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              focusedErrorBorder:
                                                  OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintStyle: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(14),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.25),
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Heel',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(14,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          width: ScreenUtil().setWidth(180),
                                          padding: EdgeInsets.symmetric(
                                              vertical: 4.0),
                                          alignment: Alignment.centerLeft,
                                          child: TextFormField(
                                            keyboardType:
                                                TextInputType.numberWithOptions(
                                                    decimal: true),
                                            inputFormatters: [
                                              FilteringTextInputFormatter.allow(
                                                  RegExp('[0-9]+')),
                                            ],
                                            controller: _heelController,
                                            onChanged: (string) {
                                              setState(() {});
                                            },
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  .color,
                                            ),
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.all(5.0),
                                              errorBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Colors.red),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              focusedErrorBorder:
                                                  OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    width: 1.0,
                                                    style: BorderStyle.solid,
                                                    color: Color(0xffc0c5cc)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(4.0)),
                                              ),
                                              filled: true,
                                              fillColor: _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xff182e42)
                                                  ? Color(0xff172636)
                                                  : _themeChanger
                                                              .getTheme()
                                                              .primaryColor ==
                                                          Color(0xfff5f5f5)
                                                      ? Color(0xfff5f5f5)
                                                      : null,
                                              hintStyle: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize:
                                                    ScreenUtil().setHeight(14),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal,
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.25),
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(1)),
                                      child: Column(
                                        children: <Widget>[
                                          ListTile(
                                            title: Text(
                                              'In.',
                                              style: TextStyle(
                                                  color: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1
                                                      .color),
                                            ),
                                            leading: Radio<UomTypes>(
                                              value: UomTypes.In,
                                              groupValue: _heel_uom,
                                              onChanged: (UomTypes value) {
                                                setState(() {
                                                  _heel_uom = value;
                                                });
                                              },
                                            ),
                                          ),
                                          ListTile(
                                            title: Text(
                                              'Gal.',
                                              style: TextStyle(
                                                  color: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1
                                                      .color),
                                            ),
                                            leading: Radio<UomTypes>(
                                              value: UomTypes.Gal,
                                              groupValue: _heel_uom,
                                              onChanged: (UomTypes value) {
                                                setState(() {
                                                  _heel_uom = value;
                                                });
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: ScreenUtil().setHeight(13.5)),
                              Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  SizedBox(width: ScreenUtil().setWidth(20)),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: SizedBox(
                                      width: 250,
                                      height: 50,
                                      child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(1.0),
                                        ),
                                        onPressed: () {
                                          if (_preloadFormKey.currentState
                                                  .validate() &&
                                              !this
                                                  .isPreCalculateButtonClicked) {
                                            _calculatePreLoad();
                                          } else {
                                            setState(() {
                                              _preLoadAutoValidate = true;
                                            });
                                          }
                                        },
                                        child: FittedBox(
                                            child: Text(
                                          'Calculate',
                                          style: TextStyle(
                                              color: Colors.blue,
                                              fontFamily: 'Roboto',
                                              fontSize: ScreenUtil().setSp(14,
                                                  allowFontScalingSelf: true)),
                                        )),
                                        //icon: Icon(Icons.train, color: Colors.white),
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(50)),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Min Outage Inches: ',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(16,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        Text(' $min_outage_inches_ui',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(18,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.bold)),
                                        SizedBox(
                                          height: 5,
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(80)),
                                  SizedBox(width: ScreenUtil().setWidth(80)),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(14)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          children: [
                                            Text('Max Gross Gallons: ',
                                                style: TextStyle(
                                                    fontFamily: 'Roboto',
                                                    fontSize: ScreenUtil().setSp(
                                                        16,
                                                        allowFontScalingSelf:
                                                            true),
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(0.5),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                            Text(' $max_gross_gallons_ui',
                                                style: TextStyle(
                                                    fontFamily: 'Roboto',
                                                    fontSize: ScreenUtil().setSp(
                                                        18,
                                                        allowFontScalingSelf:
                                                            true),
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(0.5),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Text('Load to Gallons: ',
                                                style: TextStyle(
                                                    fontFamily: 'Roboto',
                                                    fontSize: ScreenUtil().setSp(
                                                        16,
                                                        allowFontScalingSelf:
                                                            true),
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(0.5),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                            Text(' $load_to_gallons_ui',
                                                style: TextStyle(
                                                    fontFamily: 'Roboto',
                                                    fontSize: ScreenUtil().setSp(
                                                        18,
                                                        allowFontScalingSelf:
                                                            true),
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(0.5),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: ScreenUtil().setWidth(23.5)),
                            ],
                          ),
                        )),
                    SizedBox(
                      height: 5,
                    ),
                    Divider(
                      thickness: 2.0,
                    ),
                    Form(
                      //Preload form
                      key: _postloadFormKey,
                      autovalidate: _postLoadAutoValidate,
                      child: SafeArea(
                        child: Column(children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 15.0),
                                    alignment: Alignment.center,
                                    child: Text('POST-LOAD',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(16,
                                                allowFontScalingSelf: true),
                                            letterSpacing:
                                                ScreenUtil().setWidth(1),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.bold)),
                                  )),
                            ],
                          ),
                          Row(
                            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              SizedBox(width: ScreenUtil().setWidth(20)),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('Actual Outage (in.)',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(14,
                                                allowFontScalingSelf: true),
                                            letterSpacing:
                                                ScreenUtil().setWidth(0.5),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      width: ScreenUtil().setWidth(200),
                                      padding:
                                          EdgeInsets.symmetric(vertical: 4.0),
                                      alignment: Alignment.centerLeft,
                                      child: TextFormField(
                                        autovalidate: _postLoadAutoValidate ||
                                            _validateActualOutageField,
                                        keyboardType:
                                            TextInputType.numberWithOptions(
                                                decimal: true),
                                        inputFormatters: [
                                          FilteringTextInputFormatter.allow(RegExp(
                                              '^\$|^(0|([1-9][0-9]{0,}))(\\.[0-9]{0,})?\$')),
                                        ],
                                        controller: _outageController,
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return 'Please enter actual outage';
                                          } else if (value.isNotEmpty &&
                                              "Error" ==
                                                  MemoryData
                                                      .facilityConfiguration
                                                      .whenActualOutageInchesLessThanMinOutageInches &&
                                              this.min_outage_inches != null &&
                                              !this.min_outage_inches.isNaN &&
                                              double.parse(value) <
                                                  this.min_outage_inches) {
                                            return 'Greater than Min outage (in)';
                                          }
                                          return null;
                                        },
                                        onTap: () {
                                          setState(() {
                                            _validateActualOutageField = true;
                                            _isPostLoadDisabled = true;
                                          });
                                        },
                                        onChanged: (string) {
                                          // if (_debounce?.isActive ?? false) _debounce.cancel();
                                          // _debounce = Timer(const Duration(milliseconds: 1500), () {
                                          //   setState(() {
                                          //     _validatePostLoadForm();
                                          //   });
                                          // });
                                        },
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .color,
                                        ),
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.all(5.0),
                                          errorBorder: const OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                width: 1.0,
                                                style: BorderStyle.solid,
                                                color: Colors.red),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4.0)),
                                          ),
                                          focusedErrorBorder:
                                              OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                width: 1.0,
                                                style: BorderStyle.solid,
                                                color: Color(0xffc0c5cc)),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4.0)),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                width: 1.0,
                                                style: BorderStyle.solid,
                                                color: Color(0xffc0c5cc)),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4.0)),
                                          ),
                                          enabledBorder:
                                              const OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                width: 1.0,
                                                style: BorderStyle.solid,
                                                color: Color(0xffc0c5cc)),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4.0)),
                                          ),
                                          filled: true,
                                          fillColor: _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xff182e42)
                                              ? Color(0xff172636)
                                              : _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xfff5f5f5)
                                                  ? Color(0xfff5f5f5)
                                                  : null,
                                          hintStyle: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize:
                                                ScreenUtil().setHeight(14),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            letterSpacing:
                                                ScreenUtil().setWidth(0.25),
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(50)),
                              Container(
                                constraints: BoxConstraints(
                                    minWidth: 300, maxWidth: 300),
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: [
                                        Text('Gross Gallons: ',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(16,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        Text(' $gross_gallons_ui',
                                            style: TextStyle(
                                                color: _validateGrossGallons
                                                    ? Colors.red
                                                    : Colors.green,
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(18,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(60)),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: [
                                        Text('Net Gallons: ',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(16,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        Text(' $net_gallons_ui',
                                            style: TextStyle(
                                                color: _validateNetGallons
                                                    ? Colors.red
                                                    : Colors.green,
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(18,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: ScreenUtil().setHeight(20)),
                          Row(
                            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              SizedBox(width: ScreenUtil().setWidth(20)),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(14)),
                                child: SizedBox(
                                  width: 250,
                                  height: 50,
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(1.0),
                                    ),
                                    onPressed: () {
                                      if (_postloadFormKey.currentState
                                              .validate() &&
                                          !this.isPostCalculateButtonClicked) {
                                        _calculatePostLoad();
                                      } else {
                                        setState(() {
                                          _postLoadAutoValidate = true;
                                        });
                                      }
                                    },
                                    child: FittedBox(
                                        child: Text(
                                      'Calculate',
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setSp(14,
                                              allowFontScalingSelf: true)),
                                    )),
                                    //icon: Icon(Icons.train, color: Colors.white),
                                    color: Colors.white,
                                    disabledColor: Colors.white,
                                  ),
                                ),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(50)),
                              Container(
                                constraints: BoxConstraints(
                                    minWidth: 300, maxWidth: 300),
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: [
                                        Text('Product Weight: ',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(16,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        Text(' $product_weight_ui',
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: _validateProductWeight
                                                    ? Colors.red
                                                    : Colors.green,
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(18,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(60)),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(14)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: [
                                        Text('Total Freight Weight: ',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(16,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal)),
                                        Text(' $total_freight_weight_ui',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: ScreenUtil().setSp(18,
                                                    allowFontScalingSelf: true),
                                                letterSpacing:
                                                    ScreenUtil().setWidth(0.5),
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: ScreenUtil().setHeight(23.5)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(14)),
                                child: SizedBox(
                                  width: 400,
                                  height: 50,
                                  child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(1.0),
                                      ),
                                      onPressed: this._isPostLoadDisabled
                                          ? null
                                          : () => _pushToLoadRailCar(),
                                      child: FittedBox(
                                          child: Text(
                                        'SAVE & LOAD RAILCAR',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setSp(14,
                                                allowFontScalingSelf: true)),
                                      )),
                                      color: Color(0xFF3e8aeb),
                                      textColor: Colors.white,
                                      disabledColor: Colors.grey,
                                      disabledTextColor: Colors.black,
                                      splashColor: Color(0xFF3e8aeb)),
                                ),
                              )
                            ],
                          ),
                        ]),
                      ),
                    ),
                  ],
                )),
              ),
            )
          ],
        );
      },
    );
  }

  void _fromModel() {
    if (this.loadVCFModel == null) {
      return;
    }

    //user input values
    this._gravityController.text = this.loadVCFModel?.gravity;
    if (!isNullOrEmpty(this.loadVCFModel.observed_temperature_scale) &&
        this.loadVCFModel.observed_temperature_scale == APP_CONST.C) {
      String value =
          Util.convertCelciusToFarenhite(this.loadVCFModel.observed_temp);
      this._observedTempController.text = value;
    } else {
      this._observedTempController.text = this.loadVCFModel.observed_temp;
    }
    this._outageController.text = this.loadVCFModel?.actual_outage_inches;
    this._heel_uom =
        this.loadVCFModel?.heel_uom == '2' ? UomTypes.Gal : UomTypes.In;
//    this. _heelController.text =this.loadVCFModel?.heel;
    this._heelController.text = this.loadVCFModel?.heel == null
        ? null
        : double.parse(this.loadVCFModel?.heel).toInt().toString();

    //calculated values
    this.vcf = Util.doubleSafetyParse(this.loadVCFModel?.vcf ?? '');
    this.vcf_max =
        Util.doubleSafetyParse(this.loadVCFModel?.vcf_max_temp ?? '');
    this.heel_gallons = Util.doubleSafetyParse(this.loadVCFModel?.heel_gallons);
    this.min_outage_inches =
        Util.doubleSafetyParse(this.loadVCFModel?.min_outage_inches ?? '');

    this.min_outage_gallons =
        Util.intSafetyParse(this.loadVCFModel?.min_outage_gallons ?? '');
    this.max_gross_gallons =
        Util.intSafetyParse(this.loadVCFModel?.max_gross_gallons ?? '');
    this.load_to_gallons =
        Util.intSafetyParse(this.loadVCFModel?.load_to_gallons ?? '');
    this.gross_gallons =
        Util.intSafetyParse(this.loadVCFModel?.gross_gallons ?? '');
    this.net_gallons =
        Util.intSafetyParse(this.loadVCFModel?.net_gallons ?? '');
    this.product_weight =
        Util.intSafetyParse(this.loadVCFModel?.product_weight ?? '');
    this.variance = Util.intSafetyParse(this.loadVCFModel?.variance ?? '');
    this.total_freight_weight =
        Util.intSafetyParse(this.loadVCFModel?.total_freight_weight ?? '');
  }

  void _toModel() {
    this.loadVCFModel.equipment_id =
        this.equipment.equipmentInitial + ' ' + this.equipment.equipmentNumber;
    this.loadVCFModel.asset_master_id = this.selectedAssetMasterData?.id;
    this.loadVCFModel.product_id = this.selectedProduct?.product_id;
    this.loadVCFModel.product_name = this.selectedProduct?.product_name;
    this.loadVCFModel.heel_uom = this._heel_uom == UomTypes.In ? '1' : '2';
    this.loadVCFModel.gravity = this._gravityController?.text;
    this.loadVCFModel.gravity_type_id = 'API' == this.gravity_type ? '2' : '1';
    this.loadVCFModel.observed_temp = this._observedTempController?.text;
    this.loadVCFModel.max_temp = this.max_temp_ref.toString();
    this.loadVCFModel.heel = this._heelController?.text;
    this.loadVCFModel.heel_gallons =
        Util.doubleToInt(this.heel_gallons)?.toString();
    this.loadVCFModel.actual_outage_inches = this._outageController?.text;
    this.loadVCFModel.actual_outage_gallons =
        Util.doubleToInt(this.actual_outage_gallons)?.toString();

    this.loadVCFModel.vcf = this.vcf?.toString();
    this.loadVCFModel.vcf_max_temp = this.vcf_max?.toString();
    this.loadVCFModel.density = this.density?.toString();
    this.loadVCFModel.adj_gallon_per_vcf = this.adj_gallons_per_vcf?.toString();
    this.loadVCFModel.adj_gallon_protect_weight =
        this.adj_gallons_protect_weight?.toString();
    this.loadVCFModel.min_outage_inches = this.min_outage_inches?.toString();
    this.loadVCFModel.min_outage_gallons = this.min_outage_gallons?.toString();
    this.loadVCFModel.max_gross_gallons = this.max_gross_gallons?.toString();
    this.loadVCFModel.load_to_gallons = this.load_to_gallons?.toString();

    this.loadVCFModel.gross_gallons = this.gross_gallons?.toString();
    this.loadVCFModel.net_gallons = this.net_gallons?.toString();
    this.loadVCFModel.product_weight = this.product_weight?.toString();
    this.loadVCFModel.variance = this.variance?.toString();
    this.loadVCFModel.total_freight_weight =
        this.total_freight_weight?.toString();
    this.loadVCFModel.min_dot_outage = this.selectedProduct?.min_outage;
    // this.loadVCFModel.min_dot_outage = Util.doubleToInt(this.dot_outage_percentage).toString();
    this.loadVCFModel.safety_outage = this.selectedProduct?.safety_outage;
    // this.loadVCFModel.safety_outage = Util.decimalPoints(this.safety_outage_percentage, 3)?.toString();
    this.loadVCFModel.vcf_header_description = this.vcf_header_description;

    this.loadVCFModel.facility_visit_id = this.equipment?.facilityVisitId;
    this.loadVCFModel.asset_master_id = this.equipment?.assetMasterId;
    this.loadVCFModel.source = APP_CONST.MOBILITY;
    this.loadVCFModel.transaction_status = APP_CONST.TRANSACTION_STATUS_PENDING;
    this.loadVCFModel.observed_temperature_scale = APP_CONST.F;
    this.loadVCFModel.operation_type = APP_CONST.LOAD_VCF;
    this.loadVCFModel.created_dt = this.loadVCFModel?.created_dt != null
        ? this.loadVCFModel?.created_dt
        : DateTime.now();
    this.loadVCFModel.updated_dt = DateTime.now();
  }

  bool _validatePreLoadFields() {
    if (this.gallon_capacity == null) {
      _showToast(
          toastMessage: 'Gallon Capacity not found for calculation',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      return false;
    }

    if (this.adj_gallons_protect_weight == null) {
      _showToast(
          toastMessage: 'Adj Gallon Capacity not found for calculation',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      return false;
    }

    if (this.dot_outage_percentage == null) {
      _showToast(
          toastMessage: 'Dot Outage % not found for calculation',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      return false;
    }

    if (this.safety_outage_percentage == null) {
      _showToast(
          toastMessage: 'Safety Outage % not found calculation',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      return false;
    }
    return true;
  }

  bool _validatePostLoadFields() {
    if (this.gallon_capacity == null) {
      _showToast(
          toastMessage: 'Gallon Capacity not found for calculation',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      return false;
    }

    if (this.max_gross_gallons == null) {
      _showToast(
          toastMessage: 'Max Gross Gallons not found for calculation',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      return false;
    }

    if (this.density == null) {
      _showToast(
          toastMessage: 'Density not found for calculation',
          toastColor: Colors.red,
          icon: Icons.new_releases);
      return false;
    }

    return true;
  }

  _resetPreLoadValues() {
    setState(() {
      this.adj_gallons_per_vcf = null;
      this.max_gross_gallons = null;
      this.min_outage_gallons = null;
      this.min_outage_inches = null;
      this.load_to_gallons = null;
      this._isPostLoadDisabled = true;
    });
  }

  _resetPostLoadValues() {
    setState(() {
      this.actual_outage_gallons = null;
      this.gross_gallons = null;
      this.net_gallons = null;
      this.product_weight = null;
      this.total_freight_weight = null;
      this._isPostLoadDisabled = true;
    });
  }

  _showToast(
      {String toastMessage,
      Color toastColor,
      IconData icon,
      int duration = 2}) {
    try {
      _fToast = FToast(context);
      _fToast.showToast(
        child: CustomToast(
          toastColor: toastColor,
          toastMessage: toastMessage,
          icon: icon,
        ),
        gravity: ToastGravity.TOP,
        toastDuration: Duration(seconds: duration),
      );
    } catch (error) {
      print('error: $error');
      Loader.hide();
    }
  }

  void _showToastWarning(List<String> warningList) {
    Widget toast = Container(
        width: ScreenUtil().setWidth(552),
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
        decoration: BoxDecoration(
          color: Colors.orangeAccent,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: warningList.map((e) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.warning),
                SizedBox(
                  width: 12.0,
                ),
                Text(
                  e,
                  style: TextStyle(
                    // ignore: deprecated_member_use
                    color: Theme.of(context).textTheme.body1.color,
                  ),
                ),
              ],
            );
          }).toList(),
        ));
    _fToast = FToast(context);
    _fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 4),
    );
  }

  checkForLiquidProduct() async {
    bool returnValue = false;

    print("current Product ----> ${widget.currentProductDetails.dryOrLiquid}");
    print("selected Product ----> ${selectedProduct}");

    ProductModel productModel = await commonDao.fetchProductDetailsByProductId(
        product_id: selectedProduct.product_id);

    if (widget.currentProductDetails.dryOrLiquid == APP_CONST.LIQUID &&
        productModel.dryOrLiquid == APP_CONST.LIQUID) {
      returnValue = true;
      return returnValue;
    }

    return returnValue;
  }

  checkForProductMatch() async {
    ProductModel productModel = await commonDao.fetchProductDetailsByProductId(
        product_id: selectedProduct.product_id);
    String currentProductState = widget.currentProductDetails.dryOrLiquid;
    String currentProductName = widget.currentProductDetails.productName;
    String selectedProductName = productModel.productName;
    String selectedProductState = productModel.dryOrLiquid;

    if (currentProductName != null && currentProductState != null) {
      if (currentProductState == APP_CONST.LIQUID &&
          selectedProductState == APP_CONST.LIQUID &&
          selectedProductName == currentProductName) {
        return true;
      }
      return false;
    } else {
      return true;
    }
  }

  confirmationMessage(List<String> warningList) async {
    if (warningList.isEmpty) {
      return true;
    }

    String sealErrorText = "";
    for (String error in warningList) {
      sealErrorText += "$error\n";
    }
    bool returnValue = false;
    List<Object> returnList = await CustomDialog.cstmDialog(
        context,
        "confirm_to_continue",
        "Warning",
        "$sealErrorText \nDo you wish to continue?");

    if (returnList[0] == DialogAction.yes) {
      returnValue = true;
    }
    return returnValue;
  }

  popUpDialog() async {
    if (this.selectedProduct == null) {
      Navigator.of(context).pop();
    } else {
      final action = await CustomDialog.cstmDialog(
          context, "add_railcar", "Unsaved Changes", "");
      if (action[0] == DialogAction.yes) {
        Navigator.pop(context);
      }
    }
  }
}
