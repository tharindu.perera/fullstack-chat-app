import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/dao/outage_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/outage_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/util/formatter.dart';
import 'package:mbase/base/modules/load_vcf/ui/load_vcf_edit.dart';
import 'package:mbase/base/modules/load_vcf/repository/load_vcf_repository.dart';
import 'package:provider/provider.dart';

import 'load_vcf_auto_complete_text.dart';

class AddLoadVCF extends StatefulWidget {
  @override
  _AddLoadVCFState createState() => _AddLoadVCFState();
}

class _AddLoadVCFState extends State<AddLoadVCF> {
  FToast fToast;
  List<String> suggestions = new List<String>();
  LoadVCFRepository repo = new LoadVCFRepository();
  String currentText = "";
  bool equipmentSelected = false;
  final myController = TextEditingController();
  final railcarNotSelected = SnackBar(
      content: Text('Enter a valid railcar number & select it from the list'));
  final globalKey = GlobalKey<ScaffoldState>();
  final autoKey = GlobalKey<AutoCompleteTextFieldState<String>>();

  ThemeChanger _themeChanger;

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);
    Loader.show(context, progressIndicator: CircularProgressIndicator(), overlayColor: Color(0x2FFFFFFF));
    LoadVCFRepository repo = new LoadVCFRepository();
    Future<List<Equipment>> railCarModelList = repo.fetchRailcars();
    railCarModelList
        .then((event) => event.forEach((element) {
          print("initial ${element.equipmentInitial}, number: ${element.equipmentNumber}");
          suggestions.add(element.equipmentInitial + " " + element.equipmentNumber);
        })).whenComplete(() => Loader.hide());
  }

  _setSelectedText() {
    myController.text = currentText;
  }

  popUpDialog() async {
    if (myController.text.isEmpty) {
      Navigator.of(context).pop();
    } else {
      final action = await CustomDialog.cstmDialog(
          context, "add_railcar", "Unsaved Changes", "");
      if (action[0] == DialogAction.yes) {
        Navigator.pop(context);
      }
    }
  }

  _errorToast() {
    Widget errorToast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Colors.redAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.error),
          SizedBox(
            width: 12.0,
          ),
          Text(
            "Please select a railcar to proceed ",
            style: TextStyle(
              // ignore: deprecated_member_use
              color: Theme.of(context).textTheme.body1.color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: errorToast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  changeColor(Color color) {
    setState(() {
      return color;
    });
  }

  _addLoadVCF() async {
    print("equipmentSelected:$equipmentSelected, currentText:$currentText, suggestions contains currentText:${suggestions.contains(currentText)}");
    if (equipmentSelected && suggestions.contains(currentText)) {
      var equip = await EquipmentDAO().getEquipmentByInitAndNumber(currentText.split(" ")[0], currentText.split(" ")[1]);
      Loader.show(context);
      ProductModel productModel = ProductModel();
      bool validProduct = await _verifyProductModel(equip);
      AssetMasterDataModel validAsset = await _verifyAssetMasterData(currentText.split(" ")[0], currentText.split(" ")[1]);
      OutageModel  outtageModel = await OutageDao().fetchOutageById(equip.assetMasterId);
      if(equip.compartmentList?.length>0 && outtageModel != null ){
         productModel = await CommonDao().fetchProductById(equip.compartmentList[0]?.productId);
      }
      if(outtageModel == null){
        _showToast(toastColor: Colors.red,icon: Icons.error_outline,toastMessage: 'Railcar does not have Outage Details');
      }
      Loader.hide();
      if (validProduct && null != validAsset && outtageModel != null) {
        Navigator.push(context, MaterialPageRoute(builder: (context) => ProcessLoadVCF(equip, validAsset,productModel)));
      }
    } else {
      _errorToast();
    }
  }

 Future<AssetMasterDataModel> _verifyAssetMasterData(String initial, String number) async {
    Loader.show(context);
    return repo.fetchAssetMasterData(initial, number)
        .then((returnValue) {
      if (returnValue == null) {
        _showToast(toastColor: Color(0xfff34336), toastMessage: 'Asset Information does not exist for selected Railcar', icon: Icons.new_releases, duration: 4);
        return null;
      } else if (returnValue.gallon_capacity == null) {
          _showToast(toastColor: Color(0xfff34336), toastMessage: 'Gallon Capacity does not exist for selected Railcar', icon: Icons.new_releases, duration: 4);
          return null;
      } else if (returnValue.load_limit == null) {
          _showToast(toastColor: Color(0xfff34336), toastMessage: 'Load Limit does not exist for selected Railcar', icon: Icons.new_releases, duration: 4);
          return null;
      } else {
        return returnValue;
      }
    }).catchError((e) {
      print("Error on asset master data validation");
      return null;
    });
  }

  Future<bool> _verifyProductModel(Equipment equipment) async {
    ProductModel productModel = null;
    if (equipment.compartmentList.isNotEmpty && equipment.compartmentList[0]?.productId != null) {
      productModel = await CommonDao().fetchProductById(equipment.compartmentList[0].productId);
      // if (productModel != null && productModel.dryOrLiquid != APP_CONST.LIQUID) {
      //   _showToast(toastMessage: 'Railcar does not have liquid product', toastColor: Colors.red, icon: Icons.warning);
      //   return false;
      // }
    }
    return true;
  }

  _showToast({String toastMessage, Color toastColor, IconData icon, int duration=2}) {
    try {
      fToast = FToast(context);
      fToast.showToast(
        child: CustomToast(
          toastColor: toastColor,
          toastMessage: toastMessage,
          icon: icon,
        ),
        gravity: ToastGravity.TOP,
        toastDuration: Duration(seconds: duration),
      );
    } catch (error) {
      print('error: $error');
      Loader.hide();
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);
    _themeChanger = Provider.of<ThemeChanger>(context);

    Widget formUI() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: CardUI(
                  content: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Container(
                                    child: ListTile(
                                  leading: GestureDetector(
                                      onTap: popUpDialog,
                                      child: Icon(
                                        Icons.arrow_back,
                                      )),
                                  title: Text('Load VCF',
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(24,
                                              allowFontScalingSelf: true),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: 'Roboto',
                                          color: Theme.of(context)
                                              .textTheme
                                              // ignore: deprecated_member_use
                                              .headline
                                              .color)),
                                  //subtitle: Text('TMOX 004251',style:Theme.of(context).textTheme.headline ),
                                ))),
                          ],
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(20),
                                  child: Container(
                                      child: GestureDetector(
                                    onTap: popUpDialog,
                                    child: RichText(
                                      text: TextSpan(
                                          style:
                                              // ignore: deprecated_member_use
                                              Theme.of(context).textTheme.body1,
                                          children: <InlineSpan>[
                                            TextSpan(
                                                text: 'CANCEL',
                                                style: TextStyle(
                                                    fontSize: ScreenUtil().setSp(
                                                        14,
                                                        allowFontScalingSelf:
                                                            true),
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: ScreenUtil()
                                                        .setWidth(1.25),
                                                    fontFamily: 'Roboto',
                                                    color: Color(0xFF3e8aeb)))
                                          ]),
                                    ),
                                  ))),
                              Padding(
                                  padding: EdgeInsets.only(
                                      left: 0, top: 14, right: 24, bottom: 14),
                                  child: Container(
                                    width: ScreenUtil().setWidth(180),
                                    height: ScreenUtil().setHeight(48),
                                    child: FlatButton(
                                        onPressed: equipmentSelected &&
                                                suggestions
                                                    .contains(currentText)
                                            ? _addLoadVCF
                                            : null,
                                        child: Text(
                                          'NEXT',
                                          style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize:
                                                  ScreenUtil().setHeight(14),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing:
                                                  ScreenUtil().setWidth(1.25),
                                              color: Colors.white),
                                        ),
                                        color: Color(0xFF3e8aeb),
                                        textColor: Colors.white,
                                        disabledColor: Colors.grey,
                                        disabledTextColor: Colors.black,
                                        splashColor: Color(0xFF3e8aeb)),
                                  )),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(14),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(976),
                    child: Divider(thickness: 1),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(24),
                  ),
                  Row(children: <Widget>[
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(20),
                            child: Container(
                                width: ScreenUtil().setWidth(610),
                                padding: EdgeInsets.symmetric(vertical: 4.0),
                                alignment: Alignment.center,
                                child: ListTile(
                                    title: Text(
                                      'Select a Railcar',
                                      // ignore: deprecated_member_use
                                      style: Theme.of(context).textTheme.body2,
                                    ),
                                    subtitle: Container(
                                      //width: 300,
                                      padding:
                                          EdgeInsets.symmetric(vertical: 4.0),
                                      alignment: Alignment.center,
                                      child: LoadVCFAutoCompleteTextField(
                                        key: autoKey,
                                        controller: myController,
                                        inputFormatters: [
                                          UpperCaseFormatter(),
                                          LengthLimitingTextInputFormatter(11),
                                          FilteringTextInputFormatter.allow(
                                              RegExp(r'[a-zA-Z,0-9,.\s]'))
                                        ],
                                        textCapitalization:
                                            TextCapitalization.characters,
                                        suggestions: suggestions,
                                        suggestionsAmount: 20,
                                        textChanged: (text) =>
                                            currentText = text,
                                        clearOnSubmit: true,
                                        textInputAction: _setSelectedText(),
                                        textSubmitted: (text) => setState(() {
                                          equipmentSelected = false;
                                          if (text != "") {
                                            print(
                                                "myController text:${myController.text}");
                                            equipmentSelected = true;
                                            currentText = text;
                                          }
                                        }),
                                        style: TextStyle(
                                            color: Theme.of(context)
                                                .textTheme
                                                .bodyText1
                                                .color),
                                        decoration: InputDecoration(
                                          suffixIcon: Icon(
                                            Icons.search,
                                            color: _themeChanger
                                                        .getTheme()
                                                        .primaryColor ==
                                                    Color(0xff182e42)
                                                ? changeColor(Color(0xfff5f5f5))
                                                : _themeChanger
                                                            .getTheme()
                                                            .primaryColor ==
                                                        Color(0xfff5f5f5)
                                                    ? changeColor(
                                                        Color(0xff172636))
                                                    : null,
                                          ),
                                          errorBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(1)),
                                              borderSide: BorderSide(
                                                  width: 1, color: Colors.red)),
                                          focusedErrorBorder:
                                              OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(1)),
                                                  borderSide: BorderSide(
                                                      width: 1,
                                                      color: Colors.red)),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(1.0),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: _themeChanger
                                                            .getTheme()
                                                            .primaryColor ==
                                                        Color(0xff182e42)
                                                    ? Color(0xfff5f5f5)
                                                    : _themeChanger
                                                                .getTheme()
                                                                .primaryColor ==
                                                            Color(0xfff5f5f5)
                                                        ? Color(0xff172636)
                                                        : null),
                                          ),
                                          enabledBorder:
                                              const OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                color: Colors.black38,
                                                width: 1),
                                          ),
                                          filled: true,
                                          fillColor: _themeChanger
                                                      .getTheme()
                                                      .primaryColor ==
                                                  Color(0xff182e42)
                                              ? Color(0xff172636)
                                              : _themeChanger
                                                          .getTheme()
                                                          .primaryColor ==
                                                      Color(0xfff5f5f5)
                                                  ? Color(0xfff5f5f5)
                                                  : null,
                                          hintText: 'Search Railcar Number',
                                          hintStyle: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize:
                                                ScreenUtil().setHeight(14),
                                            fontStyle: FontStyle.italic,
                                            fontWeight: FontWeight.normal,
                                            letterSpacing:
                                                ScreenUtil().setWidth(0.25),
                                            color: changeColor(Theme.of(context)
                                                .textTheme
                                                .bodyText1
                                                .color),
                                          ),
                                        ),
                                      ),
                                    ))),
                          ),
                        ]),
                  ]),
                ],
              )),
            ),
          )
        ],
      );
    }

    return Scaffold(
        key: globalKey,
        // appBar: MainAppBar(),
        // drawer: AppDrawer(),
        body: Form(
          child: formUI(),
        ));
  }
}
