import 'package:mbase/base/core/common_base/dao/asset_master_data_dao.dart';
import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/dao/outage_dao.dart';
import 'package:mbase/base/core/common_base/dao/product_attributes_dao.dart';
import 'package:mbase/base/core/common_base/dao/tank_car_specification_dao.dart';
import 'package:mbase/base/core/common_base/dao/vcf_detail_dao.dart';
import 'package:mbase/base/core/common_base/dao/vcf_header_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/outage_model.dart';
import 'package:mbase/base/core/common_base/model/product_attributes_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/common_base/model/tank_specification_model.dart';
import 'package:mbase/base/core/common_base/model/uom.dart';
import 'package:mbase/base/core/common_base/model/vcf_header_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/util/nullCheckUtil.dart';
import 'package:mbase/base/core/util/util_functions.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/base/modules/load_vcf/dao/load_vcf_dao.dart';
import 'package:mbase/base/modules/load_vcf/model/load_vcf_model.dart';
import 'package:mbase/env.dart';

class LoadVCFRepository {
  CommonDao commonDao = CommonDao();
  LoadVCFDAO loadVCFDAO = new LoadVCFDAO();
  EquipmentDAO equipmentDAO = new EquipmentDAO();
  ProductAttributesDao productAttributesDao = new ProductAttributesDao();
  AssetMasterDataDao assetMasterDataDao = new AssetMasterDataDao();
  VCFHeaderDao vcfHeaderDao = new VCFHeaderDao();
  VCFDetailDao vcfDetailDao = new VCFDetailDao();
  OutageDao outageDao = new OutageDao();
  TankCarSpecDao tankCarSpecDao = new TankCarSpecDao();
  NotificationRepository notificationRepository = NotificationRepository();

  Future<LoadVCFModel> fetchLoadVCF(String assetId, String facilityVisitId) async {
    LoadVCFModel loadVCFModel = new LoadVCFModel();
    loadVCFModel.asset_master_id = assetId;
    loadVCFModel.facility_visit_id = facilityVisitId;
    return await loadVCFDAO.getLoadVCF(loadVCFModel);
  }


  Future<List<Equipment>> fetchRailcars() async {
    Map<String, dynamic> criteria = Map();
    criteria.putIfAbsent("permission_id", () => [env.userProfile.id, APP_CONST.PUBLIC]);
    List<Equipment> equipmentList = await equipmentDAO.getRecordsByCriteria(criteria);
    return equipmentList;
    //filter out only empty rail cars
    // return equipmentList.where((element) => element.compartmentList.isEmpty || double.parse(element.compartmentList[0].currentAmount ?? "0") <= 0).toList();
  }

  Future<bool> addLoadVCF(LoadVCFModel loadVCFModel) async {
    loadVCFModel.permission_id = env.userProfile.id;
    loadVCFModel.facility_id = env.userProfile.facilityId;
    loadVCFModel.user = env.userProfile.userName;
    try {
      await loadVCFDAO.save(loadVCFModel: loadVCFModel);
      return true;
    } catch (error) {
      print("Error: ${error}");
      return false;
    }
  }

  Future<bool> checkEquipmentNumber(
      String equipmentInitial, String equipmentNumber) async {
    return await equipmentDAO.checkEquipmentNumber(equipmentInitial, equipmentNumber);
  }

  Future<List<ProductAttributesModel>> fetchProducts() async {
    return await productAttributesDao.fetchAllProductAttributes(env.userProfile.facilityId);
  }

  Future<ProductModel> fetchProductById(String productId) async {
    return await commonDao.fetchProductById(productId);
  }

  Future<UOM> fetchUOMByUnit(String unit) async {
    return await commonDao.fetchUOMByUnit(unit);
  }

  Future<AssetMasterDataModel> fetchAssetMasterData(
      String equipmentInitial, String equipmentNumber) {
    return assetMasterDataDao.fetchAssetByEquipmentIntialAndNumber(
        equipmentInitial, equipmentNumber);
  }

  Future<VCFHeaderModel> fetchVCFHeaderById(String vcf_header_id) async {
    return await vcfHeaderDao.fetchVCFById(vcf_header_id);
  }

  Future<OutageDetails> fetchOutageByMeasurement(String assetId, String value) async {
    return await outageDao.fetchOutageDetailsByAssetIdAndMeasurement(assetId, value);
  }

  Future<OutageDetails> fetchMeasurementByOutage(String assetId, String value) async {
    return await outageDao.fetchOutageDetailsByAssetIdAndOutage(assetId, value);
  }

  Future<OutageModel> fetchOutageByAssetId(String assetMasterId) async {
    return await outageDao.fetchOutageById(assetMasterId);
  }

  Future<String> fetchTankCarSpecMaxTempByDotNumber(String dotNumber, bool isSummer ) async {
    if (isNullOrEmpty(dotNumber)) {
      return "";
    }
    TankCarSpecificationModel tankSpec = await tankCarSpecDao.fetchAssetByShippingContainSpec(dotNumber);
    if (tankSpec == null) {
      return "";
    }
    String scale = tankSpec?.temp_scale;
    String temp = isSummer?tankSpec.summer_temp:tankSpec.winter_temp;

    if ("C" == scale) {
      temp = Util.convertCelciusToFarenhite(temp);
    }

    return temp;
  }

}
