class SCYNotification {
  SCYNotification({this.id, this.operation_type, this.createdDateTime, this.ackDateTime, this.status, this.details, this.actionObjectId, this.facilityId});

  String id;
  String operation_type;
  DateTime createdDateTime;
  DateTime ackDateTime;
  String status;
  String details;
  String actionObjectId;
  String userId;
  String facilityId;
  String errorReason;
  var isSelected = false;

  SCYNotification.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        operation_type = map['operation_type'],
        createdDateTime = map['createdDateTime'] != null ? DateTime.parse(map['createdDateTime']) : null,
        ackDateTime = map['ackDateTime'] != null ? DateTime.parse(map['ackDateTime']) : null,
        status = map['status'],
        details = map['details'],
        userId = map['userId'],
        facilityId = map['facility_id'],
        errorReason = map['error_reason'],
        actionObjectId = map['actionObjectId'];

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "operation_type": operation_type == null ? null : operation_type,
        "createdDateTime": createdDateTime == null ? null : createdDateTime.toString(),
        "ackDateTime": ackDateTime == null ? null : ackDateTime.toString(),
        "status": status == null ? null : status,
        "details": details == null ? null : details,
        "userId": userId == null ? null : userId,
        "facility_id": facilityId == null ? null : facilityId,
        "error_reason": errorReason == null ? null : errorReason,
        "actionObjectId": actionObjectId == null ? null : actionObjectId
      };
}
