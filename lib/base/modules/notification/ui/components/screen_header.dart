import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ScreenHeader extends StatelessWidget {
  const ScreenHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(24)),
          child: Container(
              child: Text('Notifications',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.normal,
                      color: Theme.of(context)
                          .textTheme
                          // ignore: deprecated_member_use
                          .headline
                          .color))),
        ),
      ],
    );
  }
}
