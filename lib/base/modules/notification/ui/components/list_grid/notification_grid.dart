import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_paginated_datatable.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/notification/bloc/notification_bloc.dart';
import 'package:mbase/base/modules/notification/ui/components/list_grid/notification_grid_ds.dart';
import 'package:provider/provider.dart';

class NotificationGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NotificationBloc bloc = BlocProvider.of<NotificationBloc>(context);
    NotificationGridDataSource listGridDataSource = bloc.state.dataSource;
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    return CustomPaginatedDataTable(
      sortArrowColor: Colors.white,
      cardBackgroundColor: Theme.of(context).canvasColor,
      footerTextColor: Theme.of(context)
          .textTheme
          // ignore: deprecated_member_use
          .body1
          .color,
      rowColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff1d2e40) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xffffffff) : null,

      columnColor: Color(0xff274060),
      checkboxBorderColor:
          _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color.fromRGBO(0, 0, 0, 0.54) : null,
      dataRowHeight: ScreenUtil().setHeight(55),
      // headingRowHeight: 60,
      horizontalMargin: ScreenUtil().setWidth(19),
      columnSpacing: ScreenUtil().setWidth(30),
      header: SizedBox(
        width: 1,
        height: 1,
      ),
      columns: [
        CustomDataColumn(
          label: Text("OPERATION TYPE",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
        ),
        CustomDataColumn(
          label: Text("CREATED TIME",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
        ),
        CustomDataColumn(
          label: Text("ACKNOWLEDGED TIME",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
        ),
        CustomDataColumn(
          label: Text("STATUS",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
        ),
        CustomDataColumn(
          label: Text("REJECTED REASON",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
        ),
        CustomDataColumn(
          label: Text("DETAILS",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
        ),
      ],
      source: listGridDataSource ?? [],
      sortAscending: bloc.ascending,
      sortColumnIndex: 1,
      rowsPerPage: 5,
    );
  }
}
