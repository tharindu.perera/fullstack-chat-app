import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_data_table_souce.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/notification/model/notification.dart';

class NotificationGridDataSource extends CustomDataTableSource {

  final List<SCYNotification> _inputList;
  List<SCYNotification> selectedRow = [];
  int _selectedCount = 0;

  NotificationGridDataSource(this._inputList);

  onSelectedRow(bool selected, SCYNotification rowNumber) async {
    if (selectedRow.contains(rowNumber)) {
      notifyListeners();
      selectedRow.remove(rowNumber);
    } else {
      selectedRow.add(rowNumber);
    }
    notifyListeners();
  }

  @override
  CustomDataRow getRow(int index) {
    final cellVal = _inputList[index];
    return CustomDataRow.byIndex(
        index: index,
        selected: cellVal.isSelected,
        onSelectChanged: (bool value) {
          if (cellVal.isSelected != value) {
            cellVal.isSelected = value;
            notifyListeners();
          }
          onSelectedRow(value, cellVal);
        },
        cells: [
          CustomDataCell(Text(
            '${cellVal.operation_type ?? '-'}',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: ScreenUtil().setHeight(15),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal,
              letterSpacing: ScreenUtil().setWidth(1.75),
            ),
          )),
          CustomDataCell(Text(
            '${cellVal.createdDateTime != null ? DateFormat('MM/dd/yyyy HH:mm').format(cellVal.createdDateTime.toLocal()) : '-'}',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: ScreenUtil().setHeight(15),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal,
              letterSpacing: ScreenUtil().setWidth(1.75),
            ),
          )),
          CustomDataCell(Text(
            '${cellVal.ackDateTime != null ? DateFormat('MM/dd/yyyy HH:mm').format(cellVal.ackDateTime.toLocal()) : '-'}',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: ScreenUtil().setHeight(15),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal,
              letterSpacing: ScreenUtil().setWidth(1.75),
            ),
          )),
          CustomDataCell(
            Container(
              width: ScreenUtil().setWidth(110),
              height: ScreenUtil().setHeight(25),
              child: Text(
                cellVal.status ?? '-',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: ScreenUtil().setHeight(15),
                  // ignore: deprecated_member_use
                  //color: Theme.of(context).textTheme.body1.color,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                  letterSpacing: ScreenUtil().setWidth(1.75),
                  //color:Theme.of(context).textTheme.body1.color
                ),
              ),
              color: (cellVal.status != null && cellVal.status == APP_CONST.TRANSACTION_STATUS_PENDING) ?  Colors.amberAccent
                      : (cellVal.status != null && cellVal.status == APP_CONST.TRANSACTION_STATUS_ACCEPTED) ? Colors.lightGreen
                      : (cellVal.status != null && cellVal.status == APP_CONST.TRANSACTION_STATUS_REJECTED) ? Colors.red :
              (cellVal.status != null && cellVal.status == APP_CONST.TRANSACTION_STATUS_ERROR) ? Colors.red :
              Colors.blueGrey,
              alignment: Alignment.center,

            ),
          ),
          CustomDataCell(
            Container(
              width: ScreenUtil().setWidth(300),
              child: Text(
                cellVal.errorReason ?? '-',
                maxLines: 10,
                overflow: TextOverflow.visible,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: ScreenUtil().setHeight(15),
                  // ignore: deprecated_member_use
                  //color: Theme.of(context).textTheme.body1.color,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.normal,
                  letterSpacing: ScreenUtil().setWidth(1.75),
                  //color:Theme.of(context).textTheme.body1.color
                ),
              ),
            ),
          ),
          CustomDataCell(
            Text(
              cellVal.details ?? '-',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(15),
                // ignore: deprecated_member_use
                //color: Theme.of(context).textTheme.body1.color,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.75),
                //color:Theme.of(context).textTheme.body1.color
              ),
            ),
          )
        ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _inputList.length ?? 0;

  @override
  int get selectedRowCount => _selectedCount ?? 0;
}
