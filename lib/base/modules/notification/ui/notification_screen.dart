import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/modules/notification/bloc/notification_bloc.dart';
import 'package:mbase/base/modules/notification/bloc/notification_event.dart';
import 'package:mbase/base/modules/notification/bloc/notification_state.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/base/modules/notification/ui/components/list_grid/notification_grid.dart';
import 'package:mbase/base/modules/notification/ui/components/screen_header.dart';

class NotificationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     return BlocProvider<NotificationBloc>(
      create: (context) => NotificationBloc(NotificationRepository())..add(NotificationInitialDataLoad()),
      child: BlocBuilder<NotificationBloc, NotificationState>(builder: (context, state) {
        return Scaffold(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24), vertical: ScreenUtil().setHeight(24)),
                    child: CardUI(
                      content: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                              child: Column(
                                children: <Widget>[
                                  ScreenHeader(),
//                                  SizedBox(height: ScreenUtil().setHeight(24)),
                                  SizedBox(
                                    width: ScreenUtil().setWidth(928),
                                    child: Divider(thickness: ScreenUtil().setWidth(1)),
                                  ),
//                                  SizedBox(height: ScreenUtil().setHeight(24)),
//                                  Row(children: [
//                                    SearchSection(),
//                                    SizedBox(width: ScreenUtil().setWidth(23.5)),
//                                    ActionButtn()]),
//                                  SizedBox(height: 10.0),
                                  (state is NotificationInProgress) ? CircularProgressIndicator() : NotificationGrid()
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            );
      }),
    );
  }
}
