import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/modules/notification/model/notification.dart';
import 'package:mbase/env.dart';
import 'package:uuid/uuid.dart';

class NotificationDAO extends BaseDao {
  NotificationDAO() : super("notification");

  Stream<List<SCYNotification>> getRecords() {
    return BaseQuery().where("userId", isEqualTo: env.userProfile.id).snapshots().map((snapshot) {
      var list = snapshot.docs.map((doc) {
        return SCYNotification.fromMap(doc.data());
      }).toList();
      list.sort((a, b) => (a.createdDateTime).compareTo(b.createdDateTime) * -1);
      return list;
    });
  }

  Future<SCYNotification> getRecordByID({String document_id}) async{
  try{
    QuerySnapshot snapshot = await BaseQuery().where("actionObjectId",isEqualTo: document_id).get();
    if (snapshot.docs.isNotEmpty) {
      return SCYNotification.fromMap(snapshot.docs[0].data());
    } else {
      return null;
    }
  }
  on Exception {
    throw Exception('Error In fetching Notification Data');
  }
  }

  Future<SCYNotification> updateRecordOnError({String id}) async {
    try{
     // QuerySnapshot snapshot = await BaseQuery().where("actionObjectId",isEqualTo: document_id).get();
      FirebaseFirestore.instance.collection('notification').doc(id).update({'status': 'ERROR'});
    }
    on Exception {
      throw Exception('Error In fetching Notification Data');
    }

  }

   Future<SCYNotification> addNotification(SCYNotification notification) async {
    notification.id = Uuid().v1();
    collection.doc(notification.id).set(notification.toMap());
    return notification;
  }
}
