import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/assigned_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/model/assigned_defect.dart';
import 'package:mbase/base/modules/listview/actions/comment/model/comment_model.dart';
import 'package:mbase/base/modules/load_vcf/model/load_vcf_model.dart';
import 'package:mbase/base/modules/notification/dao/notifications_dao.dart';
import 'package:mbase/base/modules/notification/model/notification.dart';
import 'package:mbase/base/modules/seal/model/seal_model.dart';
import 'package:mbase/base/modules/unloadvcf/model/unloadvcf_model.dart';
import 'package:mbase/env.dart';

class NotificationRepository {
  NotificationDAO _notificationDAO = NotificationDAO();

  Stream<List<SCYNotification>> getRecordByCriteria(
      Map<String, dynamic> criteriaFromListGrid, ascending, sortColumnIndex) {
    return _notificationDAO.getRecords();
  }

  Future<SCYNotification> getNotificationsById({String document_id}) {
    return _notificationDAO.getRecordByID(document_id: document_id);
  }

  Future<SCYNotification> updateNotificationOnError({String document_id}) {
    return _notificationDAO.updateRecordOnError(id: document_id);
  }

  Future<SCYNotification> addNotification(dynamic obj, String operation) async {
    SCYNotification notification = SCYNotification();
    if (obj is Equipment && operation == APP_CONST.ADD_CAR_DB_OPERATION) {
      notification.details =
          "Equipment: ${obj.equipmentInitial + ' ' + obj.equipmentNumber} , Yard:${MemoryData.inMemoryYardMap[obj.yardId].yardName}, Track:${MemoryData.inMemoryTrackMap[obj.trackId].trackName} ";
    } else if (obj is Equipment && operation == APP_CONST.OUTBOUND_OPERATION) {
      notification.details =
          "Equipment: ${obj.equipmentInitial + ' ' + obj.equipmentNumber}, Outbound Time: ${DateFormat('yyyy-MM-dd HH:mm:ss').format(obj.outboundedDateTime.toLocal())}";
    } else if (obj is Comment) {
      notification.details =
          "Equipment: ${obj.equipmentID}, Comment: ${obj.comment}, Comment Type: ${obj.commentType}";
    } else if (obj is AssignedBlock) {
      notification.details =
          "Equipment: ${obj.equipmentInitAndNum}, Block To Code: ${obj.blockName}";
    } else if (obj is AssignedDefect) {
      notification.details =
          "Equipment: ${obj.equipmentInitAndNumber}, Defect: ${obj.defect_name}";
    } else if (obj is Equipment && operation == APP_CONST.LOAD_CAR_OPERATION) {
      notification.details =
          "Equipment: ${obj.equipmentInitial} ${obj.equipmentNumber}, Load Amount: ${obj.compartmentList[0].loadAmount}, Product: ${obj.compartmentList[0].productName}";
    } else if (obj is NewSwitchList &&
        operation == APP_CONST.SWITCHLIST_OPERATION) {
      notification.details =
          "Switch List: ${obj.switch_list_name}, Assigned Railcars: ${obj.railcars?.map((e) => e.equipment_initial + e.equipment_number)}";
    } else if (obj is NewSwitchList &&
        operation == APP_CONST.SWITCHLIST_START_OPERATION) {
      notification.details =
          "Start Switch List: ${obj.switch_list_name}, SwitchList Status: ${obj.switch_list_status}, Assigned Railcars: ${obj.railcars.map((e) => e.equipment_initial + e.equipment_number)}";
    } else if (obj is NewSwitchList &&
        operation == APP_CONST.SWITCHLIST_COMPLETE_RAILCAR) {
      notification.details =
          "Complete Switch List: ${obj.switch_list_name}, SwitchList Status: ${obj.switch_list_status}, Assigned Railcars: ${obj.railcars.map((e) => e.equipment_initial + e.equipment_number)}";
    } else if (obj is NewSwitchList &&
        operation == APP_CONST.SWITCHLIST_EDIT_RAILCAR) {
      notification.details =
          "Edit Switch List: ${obj.switch_list_name}, SwitchList Status: ${obj.switch_list_status}, Assigned Railcars: ${obj.railcars.map((e) => e.equipment_initial + e.equipment_number)}";
    } else if (obj is Equipment &&
        operation == APP_CONST.UNLOAD_CAR_OPERATION) {
      notification.details =
          "Equipment: ${obj.equipmentInitial} ${obj.equipmentNumber}, Amount: ${obj.compartmentList[0].unload_amount}";
    } else if (obj is LoadVCFModel && operation == APP_CONST.LOAD_VCF) {
      notification.details =
          "Load VCF Equipment: ${obj.equipment_id}, Net Gallons: ${obj.net_gallons ?? ''}, Gross Gallons: ${obj.gross_gallons ?? ''}";
    } else if (obj is UnloadVcfModel && operation == APP_CONST.UNLOAD_VCF) {
      notification.details =
          "Unload VCF Equipment: ${obj.equipment_id}, Net Gallons: ${obj.net_gallons ?? ''}, Gross Gallons: ${obj.gross_gallons ?? ''}";
    } else if (obj is Equipment && operation == APP_CONST.MOVE_RAILCAR) {
      notification.details =
          "Equipment: ${obj.equipmentInitial} ${obj.equipmentNumber}, "
          "From Yard: ${MemoryData.inMemoryYardMap[obj.move.from_yard_id]?.yardName}, "
          "From Track: ${MemoryData.inMemoryTrackMap[obj.move.from_track_id]?.trackName}, "
          "From Spot: ${MemoryData.inMemorySpotMap[obj.move.from_spot_id]?.spotName ?? '-'}, "
          "To Yard: ${MemoryData.inMemoryYardMap[obj.move.to_yard_id]?.yardName}, "
          "To Track: ${MemoryData.inMemoryTrackMap[obj.move.to_track_id]?.trackName}, To Spot: ${MemoryData.inMemorySpotMap[obj.move.to_spot_id]?.spotName ?? '-'}";
    } else if (obj is SealModel) {
      notification.details =
          "Equipment: ${obj.equipmentInitAndNum}, Seals: ${obj.seal_text}";
    } else if (obj is InspectionRailcarModel) {
      notification.details =
          "Equipment: ${obj.equipment}, Status: ${obj.inspectionStatus}, Template Name: ${obj.templateName}";
    }

    notification.actionObjectId = obj.id;
    notification.status = APP_CONST.TRANSACTION_STATUS_PENDING;
    notification.ackDateTime = null;
    notification.createdDateTime = DateTime.now().toLocal();
    notification.operation_type = operation;
    notification.userId = env.userProfile.id;
    notification.facilityId = env.userProfile.facilityId;
    return _notificationDAO.addNotification(notification);
  }
}
