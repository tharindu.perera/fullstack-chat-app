import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:mbase/base/modules/notification/bloc/notification_event.dart';
import 'package:mbase/base/modules/notification/bloc/notification_state.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/base/modules/notification/ui/components/list_grid/notification_grid_ds.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  NotificationRepository _repository;
  StreamSubscription streamSubscription;
  bool ascending = true;
  int sortColumnIndex = 4;

  NotificationBloc(this._repository) : super(NotificationInitState()) {}

  @override
  Stream<NotificationState> mapEventToState(NotificationEvent event) async* {
    if (event is NotificationInitialDataLoad) {
      yield* _mapListViewFetchDataWithCriteria(null);
    }
    if (event is NotificationRefreshData) {
      yield NotificationDataLoaded(dataSource: event.ds);
    }
  }

  Stream<NotificationState> _mapListViewFetchDataWithCriteria(criteria, {ascending: true, sortColumnIndex: 0}) async* {
    this.ascending = ascending;
    this.sortColumnIndex = sortColumnIndex;
    yield NotificationInProgress();
    streamSubscription?.cancel();
    streamSubscription = _repository.getRecordByCriteria(criteria, ascending, sortColumnIndex).listen((event) async {
      add(NotificationRefreshData(NotificationGridDataSource(event)));
    });
  }
}
