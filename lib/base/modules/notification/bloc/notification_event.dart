import 'package:flutter/material.dart';
import 'package:mbase/base/modules/notification/ui/components/list_grid/notification_grid_ds.dart';

@immutable
abstract class NotificationEvent {
  NotificationEvent();
}

class NotificationInitialDataLoad extends NotificationEvent {
  NotificationInitialDataLoad() : super();
}

class NotificationRefreshData extends NotificationEvent {
  NotificationGridDataSource ds;
  NotificationRefreshData(this.ds) : super();
}
