import 'package:mbase/base/modules/notification/ui/components/list_grid/notification_grid_ds.dart';

abstract class NotificationState {
  NotificationGridDataSource dataSource = NotificationGridDataSource([]);

  NotificationState(this.dataSource);

  @override
  String toString() {
    return this.runtimeType.toString();
  }
}

class NotificationInitState extends NotificationState {
  NotificationInitState({NotificationGridDataSource dataSource}) : super(dataSource ?? NotificationGridDataSource([]));
}

class NotificationInProgress extends NotificationState {
  NotificationInProgress({NotificationGridDataSource dataSource}) : super(dataSource ?? NotificationGridDataSource([]));
}

class NotificationDataLoaded extends NotificationState {
  NotificationDataLoaded({NotificationGridDataSource dataSource}) : super(dataSource ?? NotificationGridDataSource([]));
}
