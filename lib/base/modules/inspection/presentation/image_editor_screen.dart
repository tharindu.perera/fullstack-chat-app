import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class ImageEditorPage extends StatefulWidget {
  ImageEditorPage({Key key, this.imgPath}) : super(key: key);

  final String imgPath;

  @override
  _ImageEditorPageState createState() => _ImageEditorPageState();
}

class _ImageEditorPageState extends State<ImageEditorPage> {
  ui.Image image;
  bool isImageloaded = false;
  GlobalKey _myCanvasKey = new GlobalKey();
  File imgPath;

  Future<void> initDirectory() async {
    setState(() {});
  }

  void initState() {
    super.initState();
    init();
  }

  Future<Null> init() async {
    imgPath = new File(widget.imgPath);
    Uint8List bytes = imgPath.readAsBytesSync();
    image = await loadImage(Uint8List.view(bytes.buffer));
  }

  Future<ui.Image> loadImage(List<int> img) async {
    final Completer<ui.Image> completer = Completer();
    ui.decodeImageFromList(img, (ui.Image img) {
      setState(() {
        isImageloaded = true;
      });
      return completer.complete(img);
    });
    return completer.future;
  }

  @override
  Widget build(BuildContext context) {
    ImageEditor editor = ImageEditor(image: image);

    if (this.isImageloaded) {
      GestureDetector touch = new GestureDetector(
        onPanDown: (detailData) {
          editor.update(detailData.localPosition);
          _myCanvasKey.currentContext.findRenderObject().markNeedsPaint();
        },
        onPanUpdate: (detailData) {
          editor.update(detailData.localPosition);
          _myCanvasKey.currentContext.findRenderObject().markNeedsPaint();
        },
      );

      CustomPaint canvas = new CustomPaint(
        painter: editor,
        child: touch,
        key: _myCanvasKey,
      );

      return Container(
        padding: EdgeInsets.symmetric(horizontal: 40, vertical: 80),
        child: LayoutBuilder(
          // Inner yellow container
          builder: (_, constraints) => Container(
            width: constraints.widthConstraints().maxWidth,
            height: constraints.heightConstraints().maxHeight,
//            color: Colors.yellow,
            child: canvas,
          ),
        ),
        decoration: BoxDecoration(image: DecorationImage(image: FileImage(File(widget.imgPath)), fit: BoxFit.contain)),
      );
    } else {
      return Center(child: Text('loading'));
    }
  }
}

class ImageEditor extends CustomPainter {
  ImageEditor({
    this.image,
  });

  ui.Image image;

  List<Offset> points = List();

  final Paint painter = new Paint()
    ..color = Colors.blue[400]
    ..style = PaintingStyle.fill;

  void update(Offset offset) {
    points.add(offset);
  }

  @override
  void paint(Canvas canvas, Size size) {
//    canvas.drawImage(image, Offset(0.0, 0.0), Paint());
    for (Offset offset in points) {
      canvas.drawCircle(offset, 10, painter);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
