import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';

import 'draggable_text_sample.dart';
import 'image_editor_screen.dart';

class ScreenshotPage extends StatefulWidget {
  ScreenshotPage({Key key, this.inspectionAnswer, this.selectedIndex, this.file}) : super(key: key);
  InspectionAnswer inspectionAnswer;
  int selectedIndex;
  File file;

  @override
  _ScreenshotPageState createState() => new _ScreenshotPageState();
}

class _ScreenshotPageState extends State<ScreenshotPage> {
  static GlobalKey previewContainer = new GlobalKey();
  bool addComment = false;

  @override
  Widget build(BuildContext context) {
    var file = widget.file;
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Image"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.insert_comment),
            iconSize: 40,
            onPressed: () {
              setState(() {
                addComment = true;
              });
            },
          ),
          IconButton(
            icon: Icon(Icons.save_alt),
            iconSize: 40,
            onPressed: () async {
              var neImsg = await takeScreenShot(file.path);
              Navigator.pop(context, neImsg);
            },
          )
        ],
      ),
      body: RepaintBoundary(
        child: Stack(
          children: [
            ImageEditorPage(
              imgPath: file.path,
            ),
            addComment ? HomePage() : Container(),
          ],
        ),
        key: previewContainer,
      ),
    );
  }

  takeScreenShot(path) async {
    RenderRepaintBoundary boundary = previewContainer.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage();
//    ui.Image image = await boundary.toImage(pixelRatio: 3.0);
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
//    File imgPath = new File(path+"dd");
    PaintingBinding.instance.imageCache.clear();
//    await imgPath.delete();
    File imgPath = new File(path+"dd");
    imgPath.writeAsBytes(pngBytes);
    String img64 = base64Encode(pngBytes);
    widget.inspectionAnswer.imgList.removeAt(widget.selectedIndex);
    widget.inspectionAnswer.imgList.insert(widget.selectedIndex, img64);
    return imgPath;
  }
}
