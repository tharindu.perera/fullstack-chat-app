import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/inspection/presentation/screenshot_screen.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class DisplayPictureScreen extends StatefulWidget {
  final InspectionAnswer inspectionAnswer;

  const DisplayPictureScreen({Key key, @required this.inspectionAnswer}) : super(key: key);

  @override
  DisplayPictureScreenState createState() => DisplayPictureScreenState(inspectionAnswer);
}

class DisplayPictureScreenState extends State<DisplayPictureScreen> {
  InspectionAnswer inspectionAnswer;
  int selectedIndex = 0;
  List<File> fileList = [];

  DisplayPictureScreenState(InspectionAnswer inspectionAnswer) {
    this.inspectionAnswer = inspectionAnswer;
  }

  @override
  void initState() {
    super.initState();
    Future(() async {
      for (String str in inspectionAnswer.imgList) {
        final decodedBytes = base64Decode(str);
        final Directory extDir = await getApplicationDocumentsDirectory();
        final String dirPath = '${extDir.path}/${APP_CONST.InspectionImagePath}';
        await Directory(dirPath).create(recursive: true);
        var file = File(dirPath + "/${DateTime.now()}");
        file.writeAsBytesSync(decodedBytes, flush: true);
        fileList.add(file);
      }
      setState(() {});
    });
  }

  Future<void> deleteFile(selectedIndex) async {
    try {
      inspectionAnswer.imgList.removeAt(selectedIndex);
      Navigator.pop(context);
    } catch (e) {
      print("File Delete Error: " + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Theme.of(context).textTheme.bodyText1.color, //change your color here
        ),
        title: Text('Saved Images', style: Theme.of(context).textTheme.bodyText1,),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.all(5),
            child: RaisedButton(
              child: Text(
                "Highlight",
                style: TextStyle(fontSize: 20),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ScreenshotPage(
                            inspectionAnswer: inspectionAnswer,
                            selectedIndex: selectedIndex,
                            file: fileList[selectedIndex],
                          )),
                ).then((value) => setState(() {
                      value != null ? (fileList[selectedIndex] = value) : null;
                    }));
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5),
            child: RaisedButton(
              child: Text(
                "Delete",
                style: TextStyle(fontSize: 20),
              ),
              onPressed: () {
                print("delete image -  :$selectedIndex");
                deleteFile(selectedIndex);
                PaintingBinding.instance.imageCache.clear();
              },
            ),
          ),
        ],
      ),
      body: PhotoViewGallery.builder(
        itemCount: fileList.length,
        builder: (context, index) {
          return PhotoViewGalleryPageOptions(
            imageProvider: FileImage(fileList[index]),
            // Contained = the smallest possible size to fit one dimension of the screen
            minScale: PhotoViewComputedScale.contained * 0.8,
            // Covered = the smallest possible size to fit the whole screen
            maxScale: PhotoViewComputedScale.covered * 2,
          );
        },
        scrollPhysics: BouncingScrollPhysics(),
        // Set the background color to the "classic white"
        backgroundDecoration: BoxDecoration(
          color: Theme.of(context).canvasColor,
        ),
        onPageChanged: (int page) {
          setState(() {
            print("selectedIndex=$page");
            selectedIndex = page;
          });
        },
        loadingChild: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
