// import 'package:couchbase_lite/couchbase_lite.dart';
// import 'package:scy/scy/core/common_base/database.dart';
// import 'package:scy/scy/core/constants/global-constants.dart';
// import 'package:scy/scy/core/globals.dart';
// import 'package:scy/scy/modules/inspection/model/Inspection_railcar_model.dart';
// import 'package:scy/scy/modules/inspection/inspection_details/model/inspection_dto.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/modules/inspection/dto/inspection_dto.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/inspection/model/inspection_template_model.dart';
import 'package:mbase/base/modules/notification/repository/notification_repository.dart';
import 'package:mbase/env.dart';
import 'package:uuid/uuid.dart';

class InspectionDetailsRepo {
  FirebaseFirestore _firestore = DatabaseProvider.firestore;
  EquipmentDAO equipmentDAO = EquipmentDAO();
  NotificationRepository notificationRepository = NotificationRepository();
  final String fixedStringValue = "data:image/png;base64,";

  Future<List<InspectionRailcarModel>> fetchInspectionRailcar(
      InspectionRailcarModel inspectionRailcarModel) async {
    CollectionReference collection =
        _firestore.collection("inspection_railcar");
    List<InspectionRailcarModel> tempList = (await collection
            .where("facility_id", isEqualTo: env.userProfile.facilityId)
            .where("equipment", isEqualTo: inspectionRailcarModel.equipment)
            .where("inspection_template_id",
                isEqualTo: inspectionRailcarModel.inspectionTemplateId)
            // .where("inspection_status",isEqualTo: APP_CONST.INSPECTION_STATUS_COMPLETED)
            .get())
        .docs
        .map((e) => InspectionRailcarModel.fromJson(e.data()))
        .toList();

    List removableIdlist = [];

    tempList.forEach((element) {
      removableIdlist.add(element.parent_id);
      removableIdlist.add(element.immediateParentId);
    });

    tempList.removeWhere((element) {
      return removableIdlist.contains(element.id);
    });

    return tempList;
  }

  Future< InspectionRailcarModel> fetchInspectionByRailcar(Equipment equipment) async {
    print("Fetching Inspection By Railcar......");

    CollectionReference collection =
    _firestore.collection("inspection_railcar");
    List<InspectionRailcarModel> tempList = (await collection
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .where("equipment", isEqualTo: equipment.equipmentInitial+" "+equipment.equipmentNumber)
        .get())
        .docs
        .map((e) => InspectionRailcarModel.fromJson(e.data()))
        .toList();

    return tempList.isEmpty?null:tempList.first;
  }

  Future<List<InspectionRailcarModel>> fetchInspectionRailcarsForLoadAndUnload(
      String equipment) async {
    print("start fetchInspectionRailcar()");
    CollectionReference collection =
        _firestore.collection("inspection_railcar");
    return (await collection
            .where("facility_id", isEqualTo: env.userProfile.facilityId)
            .where("equipment", isEqualTo: equipment)
            .get())
        .docs
        .map((e) => InspectionRailcarModel.fromJson(e.data()))
        .toList();
  }

  Future<List<InspectionTemplate>> fetchInspectionTemplate() async {
    FirebaseFirestore firebaseFirestore = DatabaseProvider.firestore;
    CollectionReference inspectionRailcarCollection =
        firebaseFirestore.collection("inspection_template");

    return (await inspectionRailcarCollection
            .where("permission_id", isEqualTo: APP_CONST.PUBLIC)
            .where("facility_id", isEqualTo: env.userProfile.facilityId)
            .get())
        .docs
        .map((e) {
      print(">>${e.data()}");
      return InspectionTemplate.fromMap(e.data());
    }).toList();
  }

  Future<InspectionTemplate> fetchInspectionTemplateByID(id) async {
    FirebaseFirestore firebaseFirestore = DatabaseProvider.firestore;
    CollectionReference inspectionRailcarCollection =
        firebaseFirestore.collection("inspection_template");

    return (await inspectionRailcarCollection
            .where("permission_id", isEqualTo: APP_CONST.PUBLIC)
            .where("facility_id", isEqualTo: env.userProfile.facilityId)
            .where("id", isEqualTo: id)
            .get())
        .docs
        .map((e) => InspectionTemplate.fromMap(e.data()))
        .toList()
        .first;
  }

  Future<List<InspectionRailcarModel>> fetchInspectionRailcars(String status,
      int sortColumnIndex, bool sortAscending, String searchQuery) async {
    List<InspectionRailcarModel> inspectionList = [];
    List<ProductModel> productList = [];
    List<InspectionRailcarModel> tempList = [];
    String product_name = null;

    CollectionReference collection =
        _firestore.collection("inspection_railcar");
    CollectionReference productCollection = _firestore.collection("product");
    var x = status == "All"
        ? await collection
            .where("facility_id", isEqualTo: env.userProfile.facilityId)
            .get()
        : await collection
            .where("facility_id", isEqualTo: env.userProfile.facilityId)
            .where("inspection_status", isEqualTo: status)
            .get();

    var y = await productCollection
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .get();

    inspectionList =
        x.docs.map((e) => InspectionRailcarModel.fromJson(e.data())).toList();
    productList = y.docs.map((e) => ProductModel.fromMap(e.data())).toList();

    inspectionList.forEach((element) {
      productList.forEach((value) {
        if (element.product_id == value.productId) {
          print(
              "element product ${element.product_id} || value product ${value.productId}");
          product_name = value.productName;
        }
      });

      if (element.equipment
          .toUpperCase()
          .startsWith(searchQuery.toUpperCase())) {
        tempList.add(InspectionRailcarModel(
            type: element.type,
            equipment: element.equipment,
            inspectionUser: element.inspectionUser,
            equipmentId: element.equipmentId,
            inspectionTemplateId: element.inspectionTemplateId,
            inspectionDatetime: element.inspectionDatetime,
            inspectionStatus: element.inspectionStatus,
            id: element.id,
            answers: element.answers,
            equipmentType: element.equipmentType,
            createdDt: element.createdDt,
            transactionStatus: element.transactionStatus,
            source: element.source,
            updatedDt: element.updatedDt,
            product_id: product_name));
      }
      product_name = null;
    });

    return tempList;
  }

  Stream<List<InspectionRailcarModel>> fetchInspectionTableData(
      {int tabIndex, String status, String searchQuery}) {
    List<InspectionRailcarModel> inspectionRailcarList;
    tabIndex == 0
        ? status = APP_CONST.INSPECTION_STATUS_SAVED
        : tabIndex == 1
            ? status = APP_CONST.INSPECTION_STATUS_COMPLETED
            : tabIndex == 2
                ? status = APP_CONST.INSPECTION_STATUS_FAILED
                : null;
    CollectionReference inspectionListCollection =
        _firestore.collection("inspection_railcar");

    var inspectionListDataCollection = inspectionListCollection
        .where("permission_id", whereIn: [APP_CONST.PUBLIC, env.userProfile.id])
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .limit(300)
        .orderBy("inspection_datetime", descending: true);

    return inspectionListDataCollection.snapshots().map((snapshot) {
      inspectionRailcarList = snapshot.docs
          .map((doc) => InspectionRailcarModel.fromJson(doc.data()))
          .toList();
      List removableIdList = [];

      inspectionRailcarList.forEach((value) {
        removableIdList.add(value.parent_id);
        removableIdList.add(value.immediateParentId);
        if (value.inspectionStatus != status) {
          removableIdList.add(value.id);
        }
      });

      inspectionRailcarList.removeWhere((value) {
        return removableIdList.contains(value.id);
      });

      List<InspectionRailcarModel> returnList = [];

      if (searchQuery != null) {
        inspectionRailcarList.forEach((insRailcar) {
          if (insRailcar.equipment
              .toUpperCase()
              .startsWith(searchQuery.toUpperCase())) {
            returnList.add(insRailcar);
          } else if (insRailcar.equipment.split(" ")[1] ==
              searchQuery.padLeft(6, "0")) {
            returnList.add(insRailcar);
          }
        });
      } else {
        returnList = inspectionRailcarList;
      }
      return returnList;
    });
  }

  Future<bool> inspect(InspectionDTO inspectionDTO, String status) async {
    print(
        "${inspectionDTO.existingInspectionRailcarModel.answers.length.toString()}");
    print("${inspectionDTO.existingInspectionRailcarModel.answers[0].imgList}");
    var equipment = await equipmentDAO.getEquipmentByInitAndNumber(
        inspectionDTO.equipment.split(" ")[0],
        inspectionDTO.equipment.split(" ")[1]);

    InspectionRailcarModel inspectionRailcarModel = InspectionRailcarModel();
    inspectionRailcarModel.equipment = inspectionDTO.equipment;
//    inspectionRailcarModel.equipmentId = inspectionDTO.equipmentId;
    inspectionRailcarModel.source = APP_CONST.MOBILITY;
    inspectionRailcarModel.inspectionUser =
        env.userProfile.firstName + " " + env.userProfile.lastName;
    inspectionRailcarModel.user = env.userProfile.userName;
    inspectionRailcarModel.permission_id = env.userProfile.id;
    inspectionRailcarModel.inspectionStatus = status;
    inspectionRailcarModel.transactionStatus =
        APP_CONST.TRANSACTION_STATUS_PENDING;
    inspectionRailcarModel.facility_id = env.userProfile.facilityId;
    inspectionRailcarModel.type = "Railcar";
    inspectionRailcarModel.createdDt = DateTime.now();
    inspectionRailcarModel.operation_type = APP_CONST.INSPECTION;
    inspectionRailcarModel.asset_master_id = equipment.assetMasterId;
    inspectionRailcarModel.id = Uuid().v1();
    inspectionRailcarModel.inspectionDatetime = DateTime.now();
    inspectionRailcarModel.lastModifiedDatetime = DateTime.now();
    inspectionRailcarModel.inspection_id =
        inspectionDTO.existingInspectionRailcarModel?.scy_pk;
    inspectionRailcarModel.inspectionTemplateId =
        inspectionDTO.inspectionTemplateId;
    inspectionRailcarModel.templateName = inspectionDTO.templateName;
    inspectionRailcarModel.parent_id =
        inspectionDTO.existingInspectionRailcarModel?.parent_id ??
            inspectionDTO.existingInspectionRailcarModel?.id;
    inspectionRailcarModel.immediateParentId =
        inspectionDTO.existingInspectionRailcarModel?.id;
    inspectionRailcarModel.equipmentType = inspectionDTO.equipmentType;

    inspectionDTO.existingInspectionRailcarModel.answers.forEach((ans) => {
          if (ans.inspectionQuestion.category == "Signature" &&
              ans.answer != null)
            {ans.answer = fixedStringValue + ans.answer}
        });

    Map<String, dynamic> inspectionRailcarModelMap =
        inspectionRailcarModel.toMap();
    inspectionRailcarModelMap.putIfAbsent(
        "item_list",
        () => inspectionDTO.existingInspectionRailcarModel.answers
            .map((i) => i.toMap())
            .toList());
    notificationRepository.addNotification(
        inspectionRailcarModel, APP_CONST.INSPECTION);
    return _firestore
        .collection("inspection_railcar")
        .doc(inspectionRailcarModel.id)
        .set(inspectionRailcarModelMap)
        .then((value) => true)
        .catchError((err) {
      debugPrint('Error $err');
      notificationRepository
          .getNotificationsById(document_id: inspectionRailcarModel.id)
          .then((value) {
        if (value != null) {
          notificationRepository.updateNotificationOnError(
              document_id: value.id);
        } else {
          return debugPrint(
              'Could not find notification for the given inspection id');
        }
      });
    });
  }
}
