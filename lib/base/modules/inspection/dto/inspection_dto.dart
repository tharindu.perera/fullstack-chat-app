import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';

class InspectionDTO {
  String equipmentId;
  String equipment;
  String inspectionTemplateId;
  String templateName;
  String equipmentType;
  InspectionRailcarModel existingInspectionRailcarModel;


}
