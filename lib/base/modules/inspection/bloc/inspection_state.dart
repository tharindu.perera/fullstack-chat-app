import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:meta/meta.dart';

class InspectionStateLoadInProgress  extends InspectionState{
  List<InspectionRailcarModel> inspectionList = [];
  InspectionStateLoadInProgress();
}

class InspectionState {
  final List<InspectionRailcarModel> inspectionList;

  const InspectionState({
    @required this.inspectionList,
  });

  factory InspectionState.initial() => InspectionState(
        inspectionList: <InspectionRailcarModel>[],
      );

  factory InspectionState.inspectionLoadInProgress() => InspectionState(
        inspectionList: <InspectionRailcarModel>[],
      );

  factory InspectionState.inspectionLoadSuccess({
    List<InspectionRailcarModel> inspectionList,
  }) =>
      InspectionState(
        inspectionList: inspectionList,
      );

  InspectionState copyWith({
    List<InspectionRailcarModel> inspectionList,
  }) {
    return InspectionState(
      inspectionList: inspectionList ?? this.inspectionList,
    );
  }
}
