import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:mbase/base/modules/inspection/repository/inspection_details_repository.dart';

import 'inspection_event.dart';
import 'inspection_state.dart';

class InspectionBloc extends Bloc<InspectionEvent, InspectionState> {
  StreamSubscription streamSubscription;
  InspectionDetailsRepo inspectionRepository = new InspectionDetailsRepo();

  InspectionBloc(InspectionState initialState) : super(initialState);

  InspectionState get initialState => InspectionState.initial();

  @override
  Stream<InspectionState> mapEventToState(
    InspectionEvent event,
  ) async* {
    // if (event is InspectionLoaded) {
    //   yield* _mapInspectionListLoadedToState();
    // }
     if (event is FetchInspectionTableDataByStatusEvent) {
      yield*  _mapFetchInspectionTableDataByStatus(event);
    }
    else if (event is FetchInspectionTableDataByStatusLoadedEvent) {
      yield InspectionState.inspectionLoadSuccess(inspectionList: event.inspectionTableData);
    }

  }

  // Stream<InspectionState> _mapInspectionListLoadedToState() async* {
  //   yield InspectionState.inspectionLoadInProgress();
  //
  //   List inspectionList = <InspectionRailcarModel>[];
  //   yield InspectionState.inspectionLoadSuccess(inspectionList: inspectionList);
  // }


  Stream<InspectionState> _mapFetchInspectionTableDataByStatus(event) async* {
    streamSubscription?.cancel();
    yield InspectionStateLoadInProgress();
    streamSubscription = inspectionRepository.fetchInspectionTableData(tabIndex: event.tabIndex,searchQuery:event.searchQuery).listen((eve) async {
      add(FetchInspectionTableDataByStatusLoadedEvent(inspectionTableData: eve));
    });
  }

}
