
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:meta/meta.dart';

@immutable
abstract class InspectionEvent {
  const InspectionEvent();
}

class InspectionLoaded extends InspectionEvent {}

class FetchInspectionTableDataByStatusEvent extends InspectionEvent {
  int tabIndex;
  String searchQuery;

  FetchInspectionTableDataByStatusEvent({@required this.tabIndex,this.searchQuery});

  @override
  // TODO: implement props
  List<Object> get props => [tabIndex];

}

class FetchInspectionTableDataByStatusLoadedEvent extends InspectionEvent {
  List<InspectionRailcarModel>  inspectionTableData;

  FetchInspectionTableDataByStatusLoadedEvent({@required this.inspectionTableData});

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}



