import 'dart:convert';

InspectionTemplate inspectionTemplateModelFromMap(String str) => InspectionTemplate.fromMap(json.decode(str));

class InspectionTemplate {
  InspectionTemplate({
    this.type,
    this.templateName,
    this.templateDescription,
    this.facilityId,
    this.inspectionTemplateQuestionList,
    this.createdDt,
    this.updatedDt,
    this.source,
    this.channel,
    this.id,
    this.tempList
  });

  String type;
  String templateName;
  String templateDescription;
  String facilityId;
  List<InspectionTemplateQuestion> inspectionTemplateQuestionList;
  String createdDt;
  String updatedDt;
  String source;
  String channel;
  String id;
  List <InspectionTemplateQuestion>tempList =[];

  factory InspectionTemplate.fromMap(Map<String, dynamic> json) => InspectionTemplate(
        type: json["type"] == null ? null : json["type"],
        id: json["id"] == null ? null : json["id"],
        templateName: json["template_name"] == null ? null : json["template_name"],
        templateDescription: json["template_description"] == null ? null : json["template_description"],
        facilityId: json["facility_id"] == null ? null : json["facility_id"],

    inspectionTemplateQuestionList: json["item_list"] == null ? null : List<InspectionTemplateQuestion>
        .from(json["item_list"].map((x) => InspectionTemplateQuestion.fromMap(x))).toList(),
     
     // inspectionTemplateQuestionList: json["item_list"] == null ? null : List<InspectionTemplateQuestion>
     //     .from(json["item_list"].map((x) => InspectionTemplateQuestion.fromMap(x))).toList(),
        
        createdDt: json["created_dt"] == null ? null : json["created_dt"],
        updatedDt: json["updated_dt"] == null ? null : json["updated_dt"],
        source: json["source"] == null ? null : json["source"],
        channel: json["channel"] == null ? null : json["channel"],
      );
}

class InspectionTemplateQuestion {
  String id;
  String isRequired;
  String category;
  String name;
  List<String> optionList;

  InspectionTemplateQuestion({
    this.id,
    this.isRequired,
    this.category,
    this.name,
    this.optionList,
  });

  factory InspectionTemplateQuestion.fromMap(Map<String, dynamic> json) => InspectionTemplateQuestion(
        id: json["id"] == null ? null : json["id"],
        isRequired: json["is_required"] == null ? null : json["is_required"],
        category: json["category"] == null ? null : json["category"],
        name: json["name"] == null ? null : json["name"],
        optionList: json["option_list"] == null ? null : List<String>.from(json["option_list"]),
      );

  @override
  String toString() {
    return 'InspectionTemplateQuestion{id: $id, isRequired: $isRequired, questionType: $category, questionText: $name, optionList: $optionList}';
  }


}
