import 'package:mbase/base/core/util/date_time_util.dart';
import 'package:mbase/base/modules/inspection/model/inspection_template_model.dart';

class InspectionRailcarModel {
  String type;
  InspectionTemplate inspectionTemplate;
  String inspectionTemplateId;
  dynamic inspection_id;
  String equipmentId;
  String equipment;
  String templateName;
  String inspectionStatus;
  String equipmentType;
  String product_id;
  String product_name;
  List<InspectionAnswer> answers = [];
  DateTime inspectionDatetime;
  DateTime lastModifiedDatetime;
  String inspectionUser;
  String user;
  DateTime createdDt;
  DateTime updatedDt;
  String transactionStatus;
  String source;
  String id;
  String permission_id;
  String facility_id;
  String operation_type;
  String asset_master_id;
  dynamic scy_pk;
  String parent_id;
  String immediateParentId;

  InspectionRailcarModel(
      {this.type,
      this.inspectionTemplateId,
      this.equipmentId,
      this.equipment,
      this.templateName,
      this.inspectionStatus,
      this.equipmentType,
      this.product_id,
      this.product_name,
      this.answers,
      this.inspectionDatetime,
      this.lastModifiedDatetime,
      this.inspectionUser,
      this.createdDt,
      this.updatedDt,
      this.transactionStatus,
      this.source,
      this.permission_id,
      this.id,
      this.facility_id,
      this.asset_master_id,
      this.scy_pk,
      this.inspection_id,
      this.user,
      this.parent_id,
      this.immediateParentId,
      this.operation_type});

  factory InspectionRailcarModel.fromJson(Map<String, dynamic> json) => InspectionRailcarModel(
        type: json["type"] == null ? null : json["type"],
        inspectionTemplateId: json["inspection_template_id"] == null ? null : json["inspection_template_id"],
        equipmentId: json["equipment_id"] == null ? null : json["equipment_id"],
        equipment: json["equipment"] == null ? null : json["equipment"],
        templateName: json["inspection_template_name"] == null ? null : json["inspection_template_name"],
        inspectionStatus: json["inspection_status"] == null ? null : json["inspection_status"],
        equipmentType: json["equipment_type"] == null ? null : json["equipment_type"],
        product_id: json["product_id"] == null ? null : json["product_id"],
        product_name: json["product_name"] == null ? null : json["product_name"],
        answers: json["item_list"] == null ? null : List<InspectionAnswer>.from(json["item_list"].map((x) => InspectionAnswer().fromMap(x))),
        inspectionDatetime: json["inspection_datetime"] == null ? null : getLocalDateTimeFromUTCStr(json['inspection_datetime']),
        lastModifiedDatetime: json["last_modified_datetime"] == null ? null : getLocalDateTimeFromUTCStr(json['last_modified_datetime']),
        inspectionUser: json["inspection_user"] == null ? null : json["inspection_user"],
        createdDt: json['created_dt'] != null ? getLocalDateTimeFromUTCStr(json['created_dt']) : null,
        updatedDt: json["updated_dt"] != null ? getLocalDateTimeFromUTCStr(json['updated_dt']):null,

    //updatedDt: json["updated_dt"] == null ? null : json["updated_dt"],
        transactionStatus: json["transaction_status"] == null ? null : json["transaction_status"],
        source: json["source"] == null ? null : json["source"],
        id: json["id"] == null ? null : json["id"],
        permission_id: json["permission_id"] == null ? null : json["permission_id"],
        facility_id: json["facility_id"] == null ? null : json["facility_id"],
        operation_type: json["operation_type"] == null ? null : json["operation_type"],
        asset_master_id: json["asset_master_id"] == null ? null : json["asset_master_id"],
        inspection_id: json["inspection_id"] == null ? null : json["inspection_id"],
        immediateParentId: json["immediateParentId"] == null ? null : json["immediateParentId"],
        parent_id: json["parent_id"] == null ? null : json["parent_id"],
        scy_pk: json["scy_pk"] == null ? null : json["scy_pk"],
        user: json["user"] == null ? null : json["user"],
      );

  Map<String, dynamic> toMap() => {
        "type": type == null ? null : type,
        "inspection_template_id": inspectionTemplateId == null ? null : inspectionTemplateId,
        "equipment_id": equipmentId == null ? null : equipmentId,
        "equipment": equipment == null ? null : equipment,
        "inspection_template_name": templateName == null ? null : templateName,
        "inspection_status": inspectionStatus == null ? null : inspectionStatus,
        "equipment_type": equipmentType == null ? null : equipmentType,
        "product_id": product_id == null ? null : product_id,
        "product_name": product_name == null ? null : product_name,
        "inspection_datetime": inspectionDatetime == null ? null : getUTCTimeFromLocalDateTime(inspectionDatetime).toString(),
        "last_modified_datetime": lastModifiedDatetime == null ? null : getUTCTimeFromLocalDateTime(lastModifiedDatetime).toString(),
        "inspection_user": inspectionUser == null ? null : inspectionUser,
        "created_dt": createdDt == null ? null : getUTCTimeFromLocalDateTime(createdDt).toString(),
        "updated_dt": updatedDt == null ? null : getUTCTimeFromLocalDateTime(updatedDt).toString(),
        "transaction_status": transactionStatus == null ? null : transactionStatus,
        "source": source == null ? null : source,
        "id": id == null ? null : id,
        "permission_id": permission_id == null ? null : permission_id,
        "facility_id": facility_id == null ? null : facility_id,
        "operation_type": operation_type == null ? null : operation_type,
        "asset_master_id": asset_master_id == null ? null : asset_master_id,
        "inspection_id": inspection_id == null ? null : inspection_id,
        "scy_pk": scy_pk == null ? null : scy_pk,
        "parent_id": parent_id == null ? null : parent_id,
        "immediateParentId": immediateParentId == null ? null : immediateParentId,
        "user": user == null ? null : user,
      };

  @override
  String toString() {
    return 'InspectionRailcarModel{answers: $answers}';
  }
}

class InspectionAnswer {
  String category;
  String id;
  String name;
  dynamic answer;
  List<String> imgList=[];
  InspectionTemplateQuestion inspectionQuestion;

  InspectionAnswer({this.category, this.id, this.name, this.answer, this.imgList});

  InspectionAnswer fromMap(Map<String, dynamic> json) {
    return InspectionAnswer(
      id: json["id"] == null ? null : json["id"],
      category: json["category"] == null ? null : json["category"],
      name: json["name"] == null ? null : json["name"],
      answer: json["answer"] == null ? null : json["answer"],
      imgList: json["imgList"] == null ? [] : List<String>.from(json["imgList"].map((x) => x)),
    );
  }

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "category": category == null ? null : category,
        "name": name == null ? null : name,
        "answer": answer == null ? null : answer,
        "imgList": imgList == null ? [] : List<String>.from(imgList.map((x) => x)),
      };

  @override
  String toString() {
    return 'InspectionAnswer{category: $category, id: $id, name: $name, answer: $answer, imgList: $imgList, inspectionQuestion: $inspectionQuestion}';
  }


}
