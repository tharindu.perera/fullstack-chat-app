import 'dart:collection';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/util/formatter.dart';
import 'package:mbase/base/modules/inspection/bloc/inspection_bloc.dart';
import 'package:mbase/base/modules/inspection/bloc/inspection_state.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/inspection/model/inspection_template_model.dart';
import 'package:mbase/base/modules/inspection/repository/inspection_details_repository.dart';
import 'package:mbase/base/modules/inspection/ui/inspection_details.dart';
import 'package:mbase/base/modules/seal/seal/ui/auto_complete_text_scy.dart';
import 'package:provider/provider.dart';

class NewInspectionBlocProvider extends StatelessWidget {
  const NewInspectionBlocProvider({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar(),
      drawer: AppDrawer(),
      body: BlocProvider(
        create: (context) => InspectionBloc(InspectionState.initial()),
        child: NewInspection(),
      ),
    );
  }
}

class NewInspection extends StatefulWidget {
  @override
  _NewInspectionState createState() => _NewInspectionState();
}

class _NewInspectionState extends State<NewInspection> {
  InspectionTemplate selectedTemplate;
  List<Map<String, dynamic>> send = [];
  bool selectedTemp = false;

  setSelectedRadio(int value) {
    setState(() {
      selectedTemplate = templates[value];
      selectedTemp = true;
    });
    print("selectedTemplate = " + selectedTemplate.templateName.toString());
  }

  FToast fToast;
  List<String> suggestions = new List<String>();

  String currentText = "";
  bool equipmentSelected = false;
  final myController = TextEditingController();
  final railcarNotSelected = SnackBar(content: Text('Enter a valid railcar number & select it from the list'));
  final globalKey = GlobalKey<ScaffoldState>();
  final autoKey = GlobalKey<AutoCompleteTextFieldState<String>>();
  List<InspectionTemplate> templates = [];
  InspectionDetailsRepo inspectionDetailsRepo = new InspectionDetailsRepo();
  LinkedHashMap<String, Equipment> railcarIDMap = LinkedHashMap();
  List<Map<String, dynamic>> itemList = [];
  ThemeChanger _themeChanger;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    fToast = FToast(context);

    fetchRailcars();
    loadTemplates();
  }

  fetchRailcars() async {
    try {
      Loader.show(context, progressIndicator: CircularProgressIndicator(), overlayColor: Color(0x2FFFFFFF));
      List<Equipment> railCarModelList = MemoryData.equipmentList;
      railCarModelList.forEach((element) {
        suggestions.add(element.equipmentInitial + " " + element.equipmentNumber);
        railcarIDMap[element.equipmentInitial + " " + element.equipmentNumber] = element;
      });
    } catch (exception) {
      print("Exception occur in fetching Inspection Railcars >>>${exception.toString()}");
    }
    Loader.hide();

  }

  loadTemplates() async {
    templates=MemoryData.inspectionTemplateList;
    //Filtering in alphabetical order
        templates.sort((a,b){
      return a.templateName.toLowerCase().compareTo(b.templateName.toLowerCase());
    });
//    print("loading templates...");
//   inspectionDetailsRepo.fetchInspectionTemplate().then((inspectionTemps) {
//     print("inspectionTemps length:${inspectionTemps.length}");
//      setState(() {
//        templates = inspectionTemps;
//      });
//    });
  }

  _setSelectedText() {
    myController.text = currentText;
  }

  beginInspection() async {
    if (currentText != null && selectedTemplate != null) {
      InspectionRailcarModel inspectionRailcarModel = InspectionRailcarModel();
      inspectionRailcarModel.equipmentId = railcarIDMap[currentText].id;
      inspectionRailcarModel.equipment = railcarIDMap[currentText].equipmentInitial + " " + railcarIDMap[currentText].equipmentNumber;
      inspectionRailcarModel.templateName = selectedTemplate.templateName;
      inspectionRailcarModel.inspectionTemplateId = selectedTemplate.id;
      inspectionRailcarModel.inspectionStatus = "Pending";
      inspectionRailcarModel.source = selectedTemplate.source;
      inspectionRailcarModel.type = selectedTemplate.type;
      inspectionRailcarModel.inspectionTemplate = selectedTemplate;

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => InspectionDetails(
                    inspectionRailcarModel: inspectionRailcarModel,
                  )));
    } else {
      print("ID or Template not chosen");
    }
  }

  changeColor(Color color) {
    setState(() {
      return color;
    });
  }

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);

    ScreenUtil.init(context, width: 1024, height: 768);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: CardUI(
                content: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.all(20),
                              child: Container(
                                  child: ListTile(
                                leading: GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).pop();

                                      // Navigator.push(context, MaterialPageRoute(builder: (context)=> SwitchLists() ));
                                    },
                                    child: Icon(
                                      Icons.arrow_back,
                                    )),
                                title: Text('New Inspection',
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'Roboto',
                                        color: Theme.of(context).textTheme.headline.color)),
                              ))),
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Container(
                                    child: GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: RichText(
                                    text: TextSpan(style: Theme.of(context).textTheme.body1, children: <InlineSpan>[
                                      TextSpan(
                                          text: 'CANCEL',
                                          style: TextStyle(
                                              fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing: ScreenUtil().setWidth(1.25),
                                              fontFamily: 'Roboto',
                                              color: Color(0xFF3e8aeb)))
                                    ]),
                                  ),
                                ))),
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Container(
                                  width: ScreenUtil().setWidth(180),
                                  height: ScreenUtil().setHeight(48),
                                  child: FlatButton(
                                      onPressed: selectedTemp && currentText.isNotEmpty ? beginInspection : null,
                                      child: Text(
                                        'BEGIN INSPECTION',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setHeight(14),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing: ScreenUtil().setWidth(1.25),
                                            color: Colors.white),
                                      ),
                                      color: Color(0xFF3e8aeb),
                                      textColor: Colors.white,
                                      disabledColor: Colors.grey,
                                      disabledTextColor: Colors.black,
                                      splashColor: Color(0xFF3e8aeb)),
                                )),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(14),
                ),
                Divider(
                  thickness: 2.0,
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(25),
                ),
                Row(
                  children: <Widget>[
                    Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: Container(
                            width: ScreenUtil().setWidth(610),
                            padding: EdgeInsets.symmetric(vertical: 4.0),
                            alignment: Alignment.center,
                            child: ListTile(
                                title: Text(
                                  'Enter Railcar Number',
                                  // ignore: deprecated_member_use
                                  style: Theme.of(context).textTheme.body2,
                                ),
                                subtitle: Container(
                                  //width: 300,
                                  padding: EdgeInsets.symmetric(vertical: 4.0),
                                  alignment: Alignment.center,
                                  child: SimpleAutoCompleteTextFieldSCY(
                                    key: autoKey,
                                    controller: myController,
                                    inputFormatters: [UpperCaseFormatter(),
                                      LengthLimitingTextInputFormatter(11),
                                      FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z,0-9,.\s]'))],
                                    suggestions: suggestions,
                                    suggestionsAmount: 20,
                                    textChanged: (text) => currentText = text,
                                    clearOnSubmit: true,
                                    textInputAction: _setSelectedText(),
                                    textSubmitted: (text) => setState(() {
                                      if (text != "") {
                                        equipmentSelected = true;
                                        currentText = text;
                                      }
                                    }),
                                    style: TextStyle(color:Theme.of(context).textTheme.bodyText1.color),
                                    decoration:
                                    InputDecoration(
                                      suffixIcon: Icon(Icons.search,color:  _themeChanger.getTheme().primaryColor == Color(0xff182e42)?
                                      changeColor(Color(0xfff5f5f5)) : _themeChanger.getTheme().primaryColor ==
                                          Color(0xfff5f5f5)?  changeColor(Color(0xff172636)):null,),
                                      errorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(1)),
                                          borderSide: BorderSide(width: 1,color: Colors.red)
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(1)),
                                          borderSide: BorderSide(width: 1,color: Colors.red)
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(1.0),
                                        borderSide: BorderSide(width: 1, color:
                                        _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                                        _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null),
                                      ),
                                      enabledBorder: const OutlineInputBorder(
                                        borderSide: const BorderSide(color: Colors.black38, width: 1),
                                      ),
                                      filled: true,
                                      fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? changeColor(Color(0xff172636)):
                                      _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)? changeColor(Color(0xfff5f5f5)):null,
                                      hintText: '',
                                      hintStyle: TextStyle(
                                        fontFamily: 'Roboto',
                                        fontSize: ScreenUtil().setHeight(14),
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.normal,
                                        letterSpacing: ScreenUtil().setWidth(0.25),
                                        color: changeColor( Theme.of(context).textTheme.bodyText1.color),
                                      ),
                                    ),
                                  ),
                                ))),
                      ),
                    ]),
                  ],
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(31.5),
                ),
                Divider(
                  thickness: 1.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 300,
                      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 25),
                      alignment: Alignment.topLeft,
                      child: Text('Inspection Template',
                          style: TextStyle(
                              color: Theme.of(context).textTheme.body1.color,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.normal,
                              fontStyle: FontStyle.normal,
                              letterSpacing: ScreenUtil().setWidth(0.5),
                              fontSize: ScreenUtil().setHeight(16))),
                    ),
                    Wrap(
                      children: List<Widget>.generate(
                        templates.length,
                        (int i) => Container(
                          width: ScreenUtil().setWidth(200),
                          child: ListTile(
                            leading: Radio(
                              value: i,
                              groupValue: selectedTemplate == null ? null : templates.indexOf(selectedTemplate),
                              activeColor: Color(0xFF3e8aeb),
                              onChanged: (val) {
                                print("Radio onChanged " + val.toString());
                                setSelectedRadio(val);
                              },
                            ),
                            title: Text(
                              templates[i].templateName,
                              style: Theme.of(context).textTheme.body2,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            )),
          ),
        )
      ],
    );
//      ),
//    );
//  }
  }
}

class SearchFunction extends SearchDelegate<String> {
  final seals = ["UTLX12412", "UTLX21411", "UTLX57231", "UTLX77129", "UTLX89912", "UTLX65412"];
  final recentSearch = ["UTLX12412", "UTLX21411"];

  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return null;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    final suggestionList = query.isEmpty ? recentSearch : seals.where((searchString) => searchString.startsWith(query)).toList();
    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
        leading: Icon(Icons.local_offer),
        title: Text(suggestionList[index]),
      ),
      itemCount: suggestionList.length,
    );
  }
}
