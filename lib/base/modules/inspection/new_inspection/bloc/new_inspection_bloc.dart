import 'dart:async';

import 'package:meta/meta.dart';

import '../../../../core/common_base/bloc/scy_bloc.dart';

part 'new_inspection_event.dart';
part 'new_inspection_state.dart';

class NewInspectionBloc  extends SCYBloC<NewInspectionBlocEvent, NewInspectionBlocState> {
  NewInspectionBloc(NewInspectionBlocState initialState) : super(NewInspectionBlocState.initial());

  // @override
  // // TODO: implement initialState
  // NewInspectionBlocState get initialState => NewInspectionBlocState.initial();

  @override
  Stream<NewInspectionBlocState> mapEventToState(
      NewInspectionBlocEvent event) async* {
    // TODO: implement mapEventToState

    if (event is NewInspectionBlocLoaded) {
      yield* _mapTemplateBlocLoadedToState();
    }

    if (event is NewInspectionBlocEventChanged) {
      yield NewInspectionBlocState.configLoadInProgress(
          tempConfigS: event.tempConfig);
    }
  }

  Stream<NewInspectionBlocState> _mapTemplateBlocLoadedToState() async* {
    yield NewInspectionBlocState.initial();
  }
}
