part of 'new_inspection_bloc.dart';

class NewInspectionBlocState {
  final List<Map<String, dynamic>> tempConfig;

  const NewInspectionBlocState({@required this.tempConfig});

  factory NewInspectionBlocState.initial() =>
      NewInspectionBlocState(tempConfig: <Map<String, dynamic>>[]);

  factory NewInspectionBlocState.configLoadInProgress(
          {@required List<Map<String, dynamic>> tempConfigS}) =>
      NewInspectionBlocState(tempConfig: tempConfigS);

  NewInspectionBlocState copyWith({List<Map<String, dynamic>> tempConfig}) {
    return NewInspectionBlocState(
      tempConfig: tempConfig ?? this.tempConfig,
    );
  }
}
