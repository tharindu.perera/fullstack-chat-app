part of 'new_inspection_bloc.dart';

@immutable
abstract class NewInspectionBlocEvent {
  const NewInspectionBlocEvent();
}

class NewInspectionBlocLoaded extends NewInspectionBlocEvent {}

class NewInspectionBlocEventChanged extends NewInspectionBlocEvent {
  final List<Map<String, dynamic>> tempConfig;

  const NewInspectionBlocEventChanged({this.tempConfig});
}
