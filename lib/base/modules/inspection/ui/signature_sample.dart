import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_signature_pad/flutter_signature_pad.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';

class SignatureSample extends StatefulWidget {
  int index;
  InspectionAnswer inspectionAnswer;
  var k=UniqueKey();

  SignatureSample(int index, InspectionAnswer inspectionAnswer, {Key key}):super(key:UniqueKey()) {
    this.inspectionAnswer = inspectionAnswer;
    this.index = index;

  }

  @override
  SignatureSampleState createState() =>
      SignatureSampleState(index, inspectionAnswer,k);
}

class _WatermarkPaint extends CustomPainter {
  final String price;
  final String watermark;

  _WatermarkPaint(this.price, this.watermark);

  @override
  void paint(ui.Canvas canvas, ui.Size size) {}

  @override
  bool shouldRepaint(_WatermarkPaint oldDelegate) {
    return oldDelegate != this;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is _WatermarkPaint &&
          runtimeType == other.runtimeType &&
          price == other.price &&
          watermark == other.watermark;

  @override
  int get hashCode => price.hashCode ^ watermark.hashCode;
}

class SignatureSampleState extends State<SignatureSample> {
  static ByteData img = ByteData(0);

  // static String encodedImage;
  var color = Colors.red;
  var strokeWidth = 5.0;
  final _sign = GlobalKey<SignatureState>();
  int index;
  String savedEncodedImage;
  InspectionAnswer inspectionAnswer;
  var k;
  final String fixedStringValue = "data:image/png;base64,";
  SignatureSampleState(int index, InspectionAnswer inspectionAnswer,this. k) {
    this.index = index;
    this.inspectionAnswer = inspectionAnswer;
  }

  @override
  void initState() {
    super.initState();
    print("sign currentState--> ${_sign.currentState?.getData()}");
    _sign.currentState?.clear();
    setState(() {
      img = ByteData(0);
    });

     if (widget.inspectionAnswer.answer != null) {
       widget.inspectionAnswer.answer =  widget.inspectionAnswer.answer.toString().replaceAll(fixedStringValue, "");
      final decodedBytes = base64Decode(widget.inspectionAnswer.answer);
      img = ByteData.view(decodedBytes.buffer);
    }

  }

  @override
  Widget build(BuildContext context) {
    _sign.currentState?.clear();

    return Scaffold(
      body: Column(key: this.k,
        children: <Widget>[
          Container(
            width: 700,
            height: 200,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Signature(
                color: color,
                key: _sign,
                onSign: () {
                  final sign = _sign.currentState;
                  debugPrint('${sign.points.length} points in the signature');
                },
                backgroundPainter: _WatermarkPaint("2.0", "2.0"),
                strokeWidth: strokeWidth,
              ),
            ),
            color: Colors.black12,
          ),
          img.buffer.lengthInBytes == 0
              ? Container()
              : LimitedBox(
                  maxHeight: 100.0,
                  child: Image.memory(img.buffer.asUint8List())),
          Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MaterialButton(
                      color: Colors.green,
                      onPressed: () async {
                        final sign = _sign.currentState;
                        //retrieve image data, do whatever you want with it (send to server, save locally...)
                        final image = await sign.getData();

                        var data = await image.toByteData(
                            format: ui.ImageByteFormat.png);
                        sign.clear();
                      //  widget.inspectionAnswer.answer = fixedStringValue+base64.encode(data.buffer.asUint8List());
                          widget.inspectionAnswer.answer = base64.encode(data.buffer.asUint8List());;

                        debugPrint("currentSign ---> $sign");
                        setState(() {
                          img = data;
                        });
                        debugPrint("onPressed encodedImage:" +
                            widget.inspectionAnswer.answer);
                      },
                      child: Text("Save")),
                  MaterialButton(
                      color: Colors.grey,
                      onPressed: () {
                        final sign = _sign.currentState;
                        sign.clear();
                        setState(() {
                          img = ByteData(0);
                          widget.inspectionAnswer.answer = null;
                        });
                        debugPrint("cleared");
                      },
                      child: Text("Clear")),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  MaterialButton(
                      onPressed: () {
                        setState(() {
                          color =
                              color == Colors.green ? Colors.red : Colors.green;
                        });
                        debugPrint("change color");
                      },
                      child: Text("Change color")),
                  MaterialButton(
                      onPressed: () {
                        setState(() {
                          int min = 1;
                          int max = 10;
                          int selection = min + (Random().nextInt(max - min));
                          strokeWidth = selection.roundToDouble();
                          debugPrint("change stroke width to $selection");
                        });
                      },
                      child: Text("Change stroke width")),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
