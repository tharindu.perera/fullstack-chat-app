import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_data_table_souce.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_paginated_datatable.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/inspection/bloc/inspection_bloc.dart';
import 'package:mbase/base/modules/inspection/bloc/inspection_event.dart';
import 'package:mbase/base/modules/inspection/bloc/inspection_state.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/inspection/new_inspection/ui/new_inspection.dart';
import 'package:mbase/base/modules/inspection/repository/inspection_details_repository.dart';
import 'package:mbase/base/modules/inspection/ui/inspection_details.dart';
import 'package:mbase/base/modules/switchlist/switchlist_detail/ui/switchlist_details.dart';
import 'package:provider/provider.dart';

class InspectionLandingBlocProvider extends StatelessWidget {
  const InspectionLandingBlocProvider({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var inspectionListBloc = InspectionBloc(InspectionState.initial())..add(FetchInspectionTableDataByStatusEvent(tabIndex: 0));
    return BlocProvider(create: (context) => inspectionListBloc, child: InspectionLanding());
  }
}

class InspectionLanding extends StatefulWidget {
  @override
  InspectionLandingState createState() => InspectionLandingState();
}

class InspectionLandingState extends State<InspectionLanding> {
  GlobalKey<CustomTabBarState> customTabBarGlobalKey = new GlobalKey<CustomTabBarState>();

  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  final _debouncer = Debouncer(milliseconds: 500);
  String searchQuery = '';
  TextEditingController _searchController = TextEditingController();
  bool fetchComplete = false;
  int _sortColumnIndex = 0;
  bool _sortAscending = true;
  InspectionBloc switchlistbloc;
  InspectionLandingDataSource source;

  List<InspectionLandingDataSource> sortedSource;

  List<InspectionRailcarModel> savedInspectionListData = [];
  List<InspectionRailcarModel> failedInspectionListData = [];
  List<InspectionRailcarModel> completedInspectionListData = [];

  List<String> names = ["Incomplete", "Complete", "Failed"];

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 1000), () {
      setState(() {});
    });
  }

  loadDetailsPage(InspectionRailcarModel inspectionRailcarModel) async {
    inspectionRailcarModel.inspectionTemplate = await InspectionDetailsRepo().fetchInspectionTemplateByID(inspectionRailcarModel.inspectionTemplateId);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => InspectionDetails(
                  inspectionRailcarModel: inspectionRailcarModel,
                ))).then((value) => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    ScreenUtil.init(context, width: 1024, height: 768, allowFontScaling: true);
    List<Widget> tabbody = [
      Container(
        child: getPaginatedTable("Incomplete", context),
      ),
      Container(
        child: getPaginatedTable("Complete", context),
      ),
      Container(
        child: getPaginatedTable("Failed", context),
      ),
    ];

    customTabBarGlobalKey.currentState != null
        ? customTabBarGlobalKey.currentState.controller.addListener(() {
            print("index is changing------------------------------${customTabBarGlobalKey.currentState.controller.index}");

            if (customTabBarGlobalKey.currentState.controller.indexIsChanging) {
              print("index is changing------------------------------${customTabBarGlobalKey.currentState.controller.index}");

              fetchInspectionDataByStatus(tabIndex: customTabBarGlobalKey.currentState.controller.index, searchQuery: searchQuery);
            }
          })
        : print("customTabBarGlobalKey.currentState null");

    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24), vertical: ScreenUtil().setHeight(24)),
              child: CardUI(
                floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
                floatingWidget: FloatingActionButton(
                  backgroundColor: Color(0xFF3f8aeb),
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => NewInspectionBlocProvider()));
                  },
                ),
                content: Container(
                  height: ScreenUtil().setHeight(600),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(20),
                                  child: ConstrainedBox(
                                      constraints: BoxConstraints(minHeight: ScreenUtil().setHeight(28), minWidth: ScreenUtil().setWidth(104)),
                                      child: Text(
                                        'Inspection',
                                        style: TextStyle(fontSize: ScreenUtil().setHeight(24), color: Theme.of(context).textTheme.bodyText1.color),
                                      ))),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    height: ScreenUtil().setHeight(48),
                                    width: ScreenUtil().setWidth(452),
                                    padding: EdgeInsets.symmetric(
                                      vertical: 4.0,
                                    ),
                                    margin: EdgeInsets.symmetric(
                                      horizontal: 5.0,
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                    alignment: Alignment.center,
                                    child: TextField(
                                      style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Theme.of(context).textTheme.bodyText1.color,
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(1.0),
                                        prefixIcon: Icon(Icons.search),
                                        enabledBorder: const OutlineInputBorder(
                                          borderSide: const BorderSide(color: Color(0xff0c5cc), width: 1),
                                        ),
                                        filled: true,
                                        fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                            ? Color(0xff172636)
                                            : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                        hintText: 'Search by Railcar ID',
                                        hintStyle: TextStyle(
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.italic,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      controller: _searchController,
                                      onChanged: (string) {
                                        setState(() {
//                                                searchQuery = string;
                                          searchQuery = _searchController.text;
//                                                offsetValue = 0;
                                        });

                                        savedInspectionListData = [];
                                        failedInspectionListData = [];
                                        completedInspectionListData = [];
                                        fetchInspectionDataByStatus(tabIndex: customTabBarGlobalKey.currentState.controller.index, searchQuery: searchQuery);
                                      },
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                      Divider(
                        thickness: 5.0,
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Expanded(
                        child: CustomTabBarNav(key: customTabBarGlobalKey, tabitem: names, tabbody: tabbody),
//                              child: tabBar,
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  fetchInspectionDataByStatus({int tabIndex, String searchQuery}) async {
    print("tab index is ---> ${tabIndex}");

    switch (tabIndex) {
      case 0:
        BlocProvider.of<InspectionBloc>(context).add(FetchInspectionTableDataByStatusEvent(tabIndex: 0, searchQuery: searchQuery));
        setState(() {
          failedInspectionListData = [];
          completedInspectionListData = [];
          fetchComplete = true;
        });
        break;

      case 1:
        BlocProvider.of<InspectionBloc>(context).add(FetchInspectionTableDataByStatusEvent(tabIndex: 1, searchQuery: searchQuery));
        setState(() {
          savedInspectionListData = [];
          failedInspectionListData = [];
          fetchComplete = true;
        });
        break;

      case 2:
        BlocProvider.of<InspectionBloc>(context).add(FetchInspectionTableDataByStatusEvent(tabIndex: 2, searchQuery: searchQuery));
        setState(() {
          completedInspectionListData = [];
          savedInspectionListData = [];
          fetchComplete = true;
        });
        break;

      default:
        break;
    }
  }

  onSortColumn(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        BlocProvider.of<InspectionBloc>(context).state.inspectionList.sort((a, b) => a.equipment.compareTo(b.equipment));
        setState(() {});
      } else {
        BlocProvider.of<InspectionBloc>(context).state.inspectionList.sort((a, b) => b.equipment.compareTo(a.equipment));
        setState(() {});
      }
    }


    if (columnIndex == 1) {
      if (ascending) {
        BlocProvider.of<InspectionBloc>(context).state.inspectionList.sort((a, b) => a.equipmentType.compareTo(b.equipmentType));
        setState(() {});
      } else {
        BlocProvider.of<InspectionBloc>(context).state.inspectionList.sort((a, b) => b.equipmentType.compareTo(a.equipmentType));
        setState(() {});
      }
    }
    if (columnIndex == 2) {
      if (ascending) {
        BlocProvider.of<InspectionBloc>(context).state.inspectionList.sort((a, b) => a.product_name.compareTo(b.product_name));
        setState(() {});
      } else {
        BlocProvider.of<InspectionBloc>(context).state.inspectionList.sort((a, b) => b.product_name.compareTo(a.product_name));

        setState(() {});
      }
    }

    if (columnIndex == 3) {
      if (ascending) {
        BlocProvider.of<InspectionBloc>(context).state.inspectionList.sort((a, b) => a.inspectionDatetime.compareTo(b.inspectionDatetime));
        setState(() {});
      } else {
        BlocProvider.of<InspectionBloc>(context).state.inspectionList.sort((a, b) => b.inspectionDatetime.compareTo(a.inspectionDatetime));
        setState(() {});
      }
    }

    if (columnIndex == 4) {
      if (ascending) {
        BlocProvider.of<InspectionBloc>(context).state.inspectionList.sort((a, b) => b.inspectionUser.compareTo(a.inspectionUser));
        setState(() {});
      } else {
        BlocProvider.of<InspectionBloc>(context).state.inspectionList.sort((a, b) => a.inspectionUser.compareTo(b.inspectionUser));
        setState(() {});
      }
    }
  }

  SingleChildScrollView getPaginatedTable(String status, BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    switch (status) {
      case 'Incomplete':
        setState(() {
          source = new InspectionLandingDataSource(inspectionListData: savedInspectionListData, context: context);
        });
        break;
      case 'Complete':
        setState(() {
          source = new InspectionLandingDataSource(inspectionListData: completedInspectionListData, context: context);
        });
        break;
      case 'Failed':
        setState(() {
          source = new InspectionLandingDataSource(inspectionListData: failedInspectionListData, context: context);
        });
        break;
      default:
        break;
    }

    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: BlocBuilder<InspectionBloc, InspectionState>(
            // ignore: missing_return
            builder: (context, state) {
               if (state is InspectionStateLoadInProgress) {
                Loader.show(context, progressIndicator: CircularProgressIndicator(), overlayColor: Color(0x2FFFFFFF));
              }else{
                Loader.hide();
              }

          if (state.inspectionList.isNotEmpty) {
            return CustomPaginatedDataTable(
              sortArrowColor: Colors.white,
              cardBackgroundColor: Theme.of(context).canvasColor,
              footerTextColor: Theme.of(context)
                  .textTheme
                  // ignore: deprecated_member_use
                  .body1
                  .color,
              rowColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff1d2e40) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xffffffff) : null,
              columnColor: Color(0xff274060),
              checkboxBorderColor:
                  _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color.fromRGBO(0, 0, 0, 0.54) : null,
              dataRowHeight: ScreenUtil().setHeight(38),
              // headingRowHeight: 60,
              horizontalMargin: ScreenUtil().setWidth(50),
              columnSpacing: ScreenUtil().setWidth(30),
              header: Text(''),
              columns: [
                CustomDataColumn(
                    label: Text('RAILCAR',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'RailCar'),
                CustomDataColumn(
                    label: Text('CAR TYPE',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'Equipment Type'),
                CustomDataColumn(
                    label: Text('PRODUCT',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'Product'),
                CustomDataColumn(
                    label: Text('START DATE',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'Date'),
                CustomDataColumn(
                    label: Text('USER',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'User'),
              ],
              source: InspectionLandingDataSource(context: context, inspectionListData: state.inspectionList),
              sortAscending: _sortAscending,
              sortColumnIndex: _sortColumnIndex,
              rowsPerPage: 8,
            );
          } else if (state.inspectionList.isEmpty) {
            return Container();
          } else {
            return CircularProgressIndicator();
          }
        }));
  }
}

class InspectionLandingDataSource extends CustomDataTableSource {
  final DateTime now = DateTime.now();

  final DateFormat formatter = DateFormat('MM/dd/yyyy HH:mm');
  InspectionLandingState inspectionLandingState = InspectionLandingState();
  InspectionRailcarModel inspectionRailcarModel = InspectionRailcarModel();
  final List<InspectionRailcarModel> inspectionListData;
  final GlobalKey<InspectionLandingState> paginatedDataTableGlobalKey = new GlobalKey<InspectionLandingState>();
  int _selectedCount = 0;
  BuildContext context;

  InspectionLandingDataSource({this.inspectionListData, this.context});

  @override
  CustomDataRow getRow(int index) {
    InspectionRailcarModel cellVal = inspectionListData[index];
    return CustomDataRow.byIndex(index: index, cells: [
      CustomDataCell(Text(
        '${cellVal.equipment}',
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: ScreenUtil().setHeight(15),
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          letterSpacing: ScreenUtil().setWidth(1.75),
        ),
      )),
      CustomDataCell(Text(
        '${cellVal.equipmentType ?? "-"}',
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: ScreenUtil().setHeight(15),
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          letterSpacing: ScreenUtil().setWidth(1.75),
        ),
      )),
      CustomDataCell(Text(
        '${cellVal.product_name ?? "-"}',
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: ScreenUtil().setHeight(15),
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          letterSpacing: ScreenUtil().setWidth(1.75),
        ),
      )),
      CustomDataCell(Text(
        '${cellVal.inspectionDatetime == null ? "-" : formatter.format(DateTime.parse(cellVal.inspectionDatetime.toIso8601String()))}',
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: ScreenUtil().setHeight(15),
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          letterSpacing: ScreenUtil().setWidth(1.75),
        ),
      )),
      CustomDataCell(Row(
        children: [
          Container(
            child: Icon(
              Icons.lens,
              color: Color(0xff57657a),
              size: 10,
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(8),
          ),
          Container(
            child: Text(
              //cellVal.inspectionUser??"",
              cellVal.inspectionUser.replaceRange(5, cellVal.inspectionUser.length, '...') ?? '-',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(15),
                // ignore: deprecated_member_use
                //color: Theme.of(context).textTheme.body1.color,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.75),
                //color:Theme.of(context).textTheme.body1.color
              ),
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(40),
          ),
          Container(
            child: GestureDetector(
              onTap: () async {
                inspectionRailcarModel = cellVal;
                inspectionRailcarModel.inspectionTemplate = await InspectionDetailsRepo().fetchInspectionTemplateByID(cellVal.inspectionTemplateId);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InspectionDetails(
                              inspectionRailcarModel: inspectionRailcarModel,
                            )));
                //.then((value) =>  setState(() {}))
              },
              child: Icon(
                Icons.keyboard_arrow_right,
                //   color:Theme.of(context).textTheme.body1.color,
              ),
            ),
          )
        ],
      )
          // SwitchlistNavigatorButton(
          // status: cellVal.Status,
          // switchListModel: cellVal)
          )
    ]);
  }

  @override
  // TODO: implement isRowCountApproximate
  bool get isRowCountApproximate => false;

  @override
  // TODO: implement rowCount
  int get rowCount => (inspectionListData.length ?? 0).ceil();

  @override
  // TODO: implement selectedRowCount
  int get selectedRowCount => _selectedCount;
}

class CustomTabBarNav extends StatefulWidget {
  List<String> tabitem;
  List<Widget> tabbody;

  CustomTabBarNav({Key key, this.tabitem, this.tabbody}) : super(key: key);

  @override
  CustomTabBarState createState() => CustomTabBarState();
}

class CustomTabBarState extends State<CustomTabBarNav> with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(length: widget.tabitem.length, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768, allowFontScaling: true);

    return Scaffold(
      backgroundColor: Theme.of(context).canvasColor,
      body: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: TabBar(
                controller: controller,
                isScrollable: true,
                tabs: widget.tabitem.map((tabitem) {
                  return Tab(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(minWidth: ScreenUtil().setWidth(41), minHeight: ScreenUtil().setHeight(24)),
                      child: Text(
                        tabitem,
                        style: TextStyle(fontSize: ScreenUtil().setHeight(20), letterSpacing: ScreenUtil().setWidth(0.25), color: Theme.of(context).textTheme.bodyText1.color),
                      ),
                    ),
                  );
                }).toList()),
          ),
          Expanded(
            child: TabBarView(physics: NeverScrollableScrollPhysics(), controller: controller, children: widget.tabbody),
          )
        ],
      ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      // floatingActionButton:  FloatingActionButton(
      //
      //   backgroundColor: Color(0xFF3f8aeb),
      //   child: Icon(
      //     Icons.add,
      //     color: Colors.white,
      //   ),
      //   onPressed: () {
      //     Navigator.push(context, MaterialPageRoute(builder: (context) => NewInspectionBlocProvider()));
      //   },
      // ),
    );
  }
}
