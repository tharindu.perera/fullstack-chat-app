import 'dart:async';
import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/inspection/new_inspection/ui/new_inspection.dart';
import 'package:mbase/base/modules/inspection/repository/inspection_details_repository.dart';
import 'package:provider/provider.dart';

import '../../../core/components/inspection_table/inspection_table.dart';

class Inspection extends StatefulWidget {
  @override
  _InspectionState createState() => _InspectionState();
}

class _InspectionState extends State<Inspection> {
  GlobalKey<CustomTabBarState> customInspectionTabBarGlobalKey = new GlobalKey<CustomTabBarState>();

  final _debouncer = Debouncer(milliseconds: 500);
  String searchQuery = '';
  TextEditingController _searchController = TextEditingController();
  bool fetchComplete = false;
  int _sortColumnIndex = 0;
  bool _sortAscending = true;
  LinkedHashMap<String, String> yardMap = LinkedHashMap();
  bool yardsLoaded;

//  List<SwitchListData> listData;
  List<InspectionRailcarModel> incompleteInspectionListData = [];
  List<InspectionRailcarModel> completeInspectionListData = [];
  List<InspectionRailcarModel> failedInspectionListData = [];
  List<String> names = ["Incomplete", "Complete", "Failed"];

  @override
  void initState() {
    super.initState();
    fetchInspectionListData();
  }

  fetchInspectionListData() async {
    InspectionDetailsRepo inspectionRepository = new InspectionDetailsRepo();

    List<InspectionRailcarModel> failedInspectionModels = await inspectionRepository.fetchInspectionRailcars(APP_CONST.INSPECTION_STATUS_FAILED, _sortColumnIndex, _sortAscending, searchQuery);

    List<InspectionRailcarModel> incompleteInspectionModels = await inspectionRepository.fetchInspectionRailcars(APP_CONST.INSPECTION_STATUS_SAVED, _sortColumnIndex, _sortAscending, searchQuery);

    List<InspectionRailcarModel> completeInspectionModels = await inspectionRepository.fetchInspectionRailcars(APP_CONST.INSPECTION_STATUS_COMPLETED, _sortColumnIndex, _sortAscending, searchQuery);

    setState(() {
      failedInspectionListData = failedInspectionModels;
      incompleteInspectionListData = incompleteInspectionModels;
      completeInspectionListData = completeInspectionModels;
      fetchComplete = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768, allowFontScaling: true);
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    customInspectionTabBarGlobalKey.currentState != null
        ? customInspectionTabBarGlobalKey.currentState.controller.addListener(() {
            if (customInspectionTabBarGlobalKey.currentState.controller.indexIsChanging) {
              if (searchQuery != "") {
                setState(() {
                  _searchController.text = "";
                  searchQuery = "";
                  failedInspectionListData = [];
                  incompleteInspectionListData = [];
                  completeInspectionListData = [];
                  fetchInspectionListData();
                });
              }
            }
          })
        : print("customTabBarGlobalKey.currentState null");

    return Scaffold(
      // appBar: MainAppBar(),
      // drawer: AppDrawer(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: CardUI(
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Container(
                                    child: Text('Inspection',
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setHeight(24),
                                            fontFamily: 'Roboto',
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: Theme.of(context).textTheme.body1.color)))),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(20),
                                  child: Container(
                                    width: ScreenUtil().setWidth(293),
                                    padding: EdgeInsets.symmetric(vertical: 4.0),
                                    alignment: Alignment.center,
                                    child: TextField(
                                      onChanged: (string) {
                                        setState(() {
                                          searchQuery = _searchController.text;
                                        });
                                        _debouncer.run(() {
                                          failedInspectionListData = [];
                                          incompleteInspectionListData = [];
                                          completeInspectionListData = [];
                                          fetchInspectionListData();
                                        });
                                      },
                                      style: TextStyle(
                                        fontFamily: 'Roboto',
                                        color: Theme.of(context).textTheme.bodyText1.color,
                                        fontSize: ScreenUtil().setHeight(14),
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.normal,
                                        letterSpacing: ScreenUtil().setWidth(0.25),
                                        //color: Colors.black,
                                        //     fontSize: 15.0
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(1.0),
                                        suffixIcon: Icon(
                                          Icons.search,
                                          color: Colors.black,
                                        ),
                                        enabledBorder: const OutlineInputBorder(
                                          borderSide: const BorderSide(color: Color(0xff0c5cc), width: 1),
                                        ),
                                        filled: true,
                                        fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                            ? Color(0xff172636)
                                            : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                        hintText: 'Search by Railcar ID',
                                        hintStyle: TextStyle(
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.italic,
                                          color: Colors.grey,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),

                    Divider(
                      thickness: 1.0,
                    ),

                    SizedBox(
                      height: 10.0,
                    ),
//                      BlocBuilder<InspectionBloc, InspectionState>(
//                        builder: (context, state) {
//                          return
                    fetchComplete
                        ? CustomTabBarNav(
                            key: customInspectionTabBarGlobalKey,
                            tabitem: names,
                            tabbody: [
                              Container(
                                child: Center(
                                  child: InspectionTable(listData: incompleteInspectionListData),
                                ),
                              ),
                              Container(
                                child: Center(
                                  child: InspectionTable(listData: completeInspectionListData),
                                ),
                              ),
                              Container(
                                child: Center(
                                  child: InspectionTable(listData: failedInspectionListData),
                                ),
                              )
                            ],
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
          )
        ],
      ),

      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xFF3f8aeb),
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => NewInspectionBlocProvider()));
        },
      ),
    );
  }
}


class CustomTabBarNav extends StatefulWidget {
  List<String> tabitem;
  List<Widget> tabbody;

  CustomTabBarNav({Key key, this.tabitem, this.tabbody}) : super(key: key);

  @override
  CustomTabBarState createState() => CustomTabBarState();
}

class CustomTabBarState extends State<CustomTabBarNav> with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    controller = new TabController(length: widget.tabitem.length, vsync: this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768, allowFontScaling: true);

    return Container(
      height: 300,
      child: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: TabBar(
                controller: controller,
                isScrollable: true,
                tabs: widget.tabitem.map((tabitem) {
                  return Tab(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(minWidth: ScreenUtil().setWidth(41), minHeight: ScreenUtil().setHeight(24)),
                      child: Text(
                        tabitem,
                        style: TextStyle(fontSize: ScreenUtil().setHeight(20), letterSpacing: ScreenUtil().setWidth(0.25), color: Theme.of(context).textTheme.body1.color),
                      ),
                    ),
                  );
                }).toList()),
          ),
          Expanded(
            child: TabBarView(controller: controller, children: widget.tabbody),
          )
        ],
      ),
    );
  }
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}
