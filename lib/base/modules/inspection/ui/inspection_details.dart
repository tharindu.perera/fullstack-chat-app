import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:io' as Io;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:grouped_checkbox/grouped_checkbox.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/core/components/custom_dropdown/custom_dropdown.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/util/action_permission_checker.dart';
import 'package:mbase/base/core/util/hex_color.dart';
import 'package:mbase/base/modules/inspection/dto/inspection_dto.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/inspection/model/inspection_template_model.dart';
import 'package:mbase/base/modules/inspection/presentation/display_picture_screen.dart';
import 'package:mbase/base/modules/inspection/repository/inspection_details_repository.dart';
import 'package:mbase/base/modules/inspection/ui/signature_sample.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/ui/assign_defect.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class InspectionDetails extends StatefulWidget {
  final InspectionRailcarModel inspectionRailcarModel;

  InspectionDetails({Key key, this.inspectionRailcarModel}) : super(key: key);

  @override
  _InspectionDetailsState createState() => _InspectionDetailsState(inspectionRailcarModel);
}

class _InspectionDetailsState extends State<InspectionDetails> {
  InspectionRailcarModel inspectionRailcarModel;
  EquipmentDAO equipmentDAO = new EquipmentDAO();
  FToast fToast;

  _InspectionDetailsState(inspectionRailcarModel) {
    this.inspectionRailcarModel = inspectionRailcarModel;
  }

  Color appbarColour = HexColor("274076");
  Color buttonColour = HexColor("5c8cde");
  final dateTimeFormatter = new DateFormat('MM/dd/yyyy HH:mm');

  var strokeWidth = 5.0;
  InspectionDetailsRepo inspectionDetailsRepo = InspectionDetailsRepo();
  String railcarInspectionStatus = "";
  String railcarInspectionDate = "";
  String lastModifiedDate = "";
  String inspectionUser = "--";
  InspectionRailcarModel existingInspection = null;
  InspectionRailcarModel questionnaireInitialState = new InspectionRailcarModel();
  bool formChanged = false;

  Function alteredAnswerChecker = const ListEquality().equals;

  List<String> checkAnswers = List<String>();
  List<String> alteredAnswers = List<String>();

  @override
  void initState() {
    super.initState();
    fToast = FToast(context);
    Loader.show(context);
    inspectionDetailsRepo.fetchInspectionRailcar(inspectionRailcarModel).then((value) {
      if (value.length > 0) {
        existingInspection = value[0];
        List<InspectionAnswer> ans = existingInspection.answers;
        questionnaireInitialState = value[0];
        //initialInspection = InspectionRailcarModel.fromJson(value[0].toMap());
        // initialInspection.answers = ans.map((e) {
        //   return InspectionAnswer().fromMap(e.toMap());
        // }).toList();
        print("%%%%${questionnaireInitialState}");
        //  initialInspection = value[0];
        print("existingInspection =${existingInspection}");
        print("existingInspection id =${existingInspection.id}");
        setState(() {
          railcarInspectionStatus = existingInspection.inspectionStatus ?? "-";
          railcarInspectionDate = dateTimeFormatter.format(existingInspection.inspectionDatetime).toString();
          //      lastModifiedDate = dateTimeFormatter.format(existingInspection.lastModifiedDatetime).toString();
          lastModifiedDate = dateTimeFormatter.format(existingInspection.lastModifiedDatetime ?? DateTime.now()).toString();
          inspectionUser = existingInspection.inspectionUser;
        });
      } else {
        print("existingInspection is null");
        setState(() {
          railcarInspectionStatus = "--";
          railcarInspectionDate = "--";
          lastModifiedDate = "--";
        });
      }
      Loader.hide();
    });
  }



  checkFormAlterations() async {
    print("existing inspection ${existingInspection.answers}");

    print("initial inspection ${questionnaireInitialState.answers}");

    // var result = alteredAnswerChecker(existingInspection.answers, initialInspection.answers);
    // print("result---> ${result}");
    existingInspection.answers?.forEach((ex_inspection) {
      questionnaireInitialState.answers?.forEach((initial_questionnaire) {
        print("ex_inspection.answer ---> ${ex_inspection.answer.toString()}");

        print("init_inspection.answer ---> ${initial_questionnaire.answer.toString()}");

        if (!identical(initial_questionnaire.answer, ex_inspection.answer)) {
          setState(() {
            formChanged = true;
          });
        }
      });
    });

    if (formChanged) {
      final action = await CustomDialog.cstmDialog(context, "add_railcar", "Unsaved Changes", "");
      if (action[0] == DialogAction.yes) {
        Navigator.pop(context);
      }
    } else {
      Navigator.of(context).pop();
    }
  }

  _showSuccessToast(String message) {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Color(0xff7fae1b),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text(
            message,
            style: TextStyle(
              color: Theme.of(context)
                  .textTheme
                  // ignore: deprecated_member_use
                  .body1
                  .color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
    Timer(Duration(seconds: 1), () => Navigator.pop(context));
  }

  _showErrorToast(String error) {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Color(0xfff34336),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.error),
          SizedBox(
            width: 12.0,
          ),
          Text(
            error,
            style: TextStyle(
              color: Theme.of(context)
                  .textTheme
                  // ignore: deprecated_member_use
                  .body1
                  .color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  _showWarnToast(String error) {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Colors.yellow,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.error),
          SizedBox(
            width: 12.0,
          ),
          Text(
            error,
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }


  inspect(status) async {
    InspectionDTO inspectionDTO = new InspectionDTO();
    inspectionDTO.equipmentId = inspectionRailcarModel.equipmentId;
    inspectionDTO.equipment = inspectionRailcarModel.equipment;
    inspectionDTO.inspectionTemplateId = inspectionRailcarModel.inspectionTemplateId;
    inspectionDTO.templateName = inspectionRailcarModel.templateName;
    inspectionDTO.existingInspectionRailcarModel = existingInspection;
    inspectionDTO.equipmentType = inspectionRailcarModel.equipmentType;
    var err = [];

    existingInspection.answers.forEach((element) {
      if (element.inspectionQuestion.isRequired != null && element.inspectionQuestion.isRequired.trim() == "Yes" && (element.answer == null || element.answer.toString().trim() == "")) {
        err.add("Answer is required for ${element.inspectionQuestion.name}");
      }
    });

    if (err.isNotEmpty && status == APP_CONST.INSPECTION_STATUS_COMPLETED) {
      // String msg = "";
      // err.toList().forEach((element) {
      //   msg = msg + "**" + element.toString() + "\n";
      // });
      // _showErrorToast("${msg}");
      _showErrorToast("Please complete the mandatory fields to complete the inspection");
      return;
    }
    Loader.show(context);
   inspectionDetailsRepo.inspect(inspectionDTO, status);
    Loader.hide();
    _showSuccessToast("Inspection updated successfully.");

    // if (isSuccess) {
    //   _showSuccessToast("Inspection updated successfully.");
    // }
    // else {
    //   _showErrorToast("Something went wrong.");
    // }
  }

  Future<List<String>> getImageList(int index) async {
    List<String> imageList = [];
    final String inspectionId = inspectionRailcarModel.equipmentId;
    // final String inspectionId =
    // inspectionRailcarModel.inspectionRailcarId.split(":")[1];
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/${APP_CONST.InspectionImagePath}/$inspectionId/$index';
    print("dirPath:$dirPath");

    final myDir = new Directory(dirPath);
    bool myDirExists = await myDir.exists() ?? "";
    print("getImageList($index) myDirExists:$myDirExists");
    if (myDirExists) {
      List<FileSystemEntity> _images = myDir.listSync(recursive: true, followLinks: false);
      print("getImageList($index) _images.length:${_images.length}");
      String imgPath;

      for (FileSystemEntity fileSystemEntity in _images) {
        imgPath = fileSystemEntity.path;
        print("getImageList($index) imgPath:$imgPath");
        final bytes = Io.File(imgPath).readAsBytesSync();
        String img64 = base64Encode(bytes);
        print(img64.substring(0, 100));
        imageList.add(img64);
      }
    } else {
      imageList = null;
    }
    return imageList;
  }

  failInspection() async {
    try {
      Loader.show(context);
      InspectionDTO inspectionDTO = new InspectionDTO();
      inspectionDTO.equipmentId = inspectionRailcarModel.equipmentId;
      inspectionDTO.equipment = inspectionRailcarModel.equipment;
      inspectionDTO.inspectionTemplateId = inspectionRailcarModel.inspectionTemplateId;
      inspectionDTO.templateName = inspectionRailcarModel.templateName;
      inspectionDTO.existingInspectionRailcarModel = existingInspection;
      inspectionDTO.equipmentType = inspectionRailcarModel.equipmentType;

      // bool isSuccess = await inspectionDetailsRepo.inspect(inspectionDTO, APP_CONST.INSPECTION_STATUS_FAILED);
      inspectionDetailsRepo.inspect(inspectionDTO, APP_CONST.INSPECTION_STATUS_FAILED);

        Loader.hide();
        final action = await CustomDialog.cstmDialog(
            context, "confirm_to_continue", "Assign Defect to Railcar",
            "Inspection updated successfully.");
        if (action[0] == DialogAction.yes) {
          var equipment = await equipmentDAO.getEquipmentByInitAndNumber(
              inspectionRailcarModel.equipment.split(" ")[0],
              inspectionRailcarModel.equipment.split(" ")[1]);

          List<Equipment> equipmentList = List();
          equipmentList.add(equipment);

          Navigator.pop(context);
          Navigator.push(context, MaterialPageRoute(builder: (context) => AssignDefect(equipmentList, '')));
        } else {
          Navigator.pop(context);
        }

      // if (isSuccess) {
      //   Loader.hide();
      //   final action = await CustomDialog.cstmDialog(
      //       context, "confirm_to_continue", "Assign Defect to Railcar",
      //       "Inspection updated successfully.");
      //   if (action[0] == DialogAction.yes) {
      //     var equipment = await equipmentDAO.getEquipmentByInitAndNumber(
      //         inspectionRailcarModel.equipment.split(" ")[0],
      //         inspectionRailcarModel.equipment.split(" ")[1]);
      //
      //     List<Equipment> equipmentList = List();
      //     equipmentList.add(equipment);
      //
      //     Navigator.pop(context);
      //     Navigator.push(context, MaterialPageRoute(builder: (context) => AssignDefect(equipmentList, '')));
      //   } else {
      //     Navigator.pop(context);
      //   }
      // } else {
      //   Loader.hide();
      //   _showErrorToast("Inspection not updated, something went wrong.");
      // }
    } catch (exception) {
      Loader.hide();
      _showErrorToast("Something went wrong.");
      print("Exception occur in fail inspection action >>>${exception.toString()}");
    }
  }


  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768, allowFontScaling: true);
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Theme.of(context).canvasColor,
        appBar: MainAppBar(),
        drawer: AppDrawer(),
        body: Scrollbar(
          child: SingleChildScrollView(
            physics: ScrollPhysics(),
            scrollDirection: Axis.vertical,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.all(20),
                              child: Container(
                                  child: ListTile(
                                leading: GestureDetector(
                                    onTap: () {
                                      // Navigator.of(context).pop();
                                      checkFormAlterations();
                                    },
                                    child: Icon(
                                      Icons.arrow_back,
                                    )),
                                title: Text('Inspection Details',
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'Roboto',
                                        color: Theme.of(context)
                                            .textTheme
                                            // ignore: deprecated_member_use
                                            .headline
                                            .color)),
                              ))),
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Container(
                                    child: GestureDetector(
                                  onTap: () {
                                    checkFormAlterations();
                                    // Navigator.of(context).pop();
                                  },
                                  child: RichText(
                                    text: TextSpan(
                                        style: Theme.of(context)
                                            .textTheme
                                            // ignore: deprecated_member_use
                                            .body1,
                                        children: <InlineSpan>[
                                          TextSpan(
                                              text: 'CANCEL',
                                              style: TextStyle(
                                                  fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.w500,
                                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                                  fontFamily: 'Roboto',
                                                  color: Color(0xFF3e8aeb)))
                                        ]),
                                  ),
                                ))),
                            Padding(
                                padding: EdgeInsets.only(left: 0, top: 14, right: 24, bottom: 14),
                                child: Container(
                                  width: ScreenUtil().setWidth(180),
                                  height: ScreenUtil().setHeight(48),
                                  child: FlatButton(
                                    child: Text(
                                      'FAIL',
                                      style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: ScreenUtil().setWidth(1.25),
                                          color: Colors.white),
                                    ),
                                    color: Color(0xFF3e8aeb),
                                    textColor: Colors.white,
                                    disabledColor: Colors.grey,
                                    disabledTextColor: Colors.black,
                                    splashColor: Color(0xFF3e8aeb),
                                    onPressed:!checkUserPermission(["Inspection - Edit"]) ? null :
                                        (railcarInspectionStatus == "--" || railcarInspectionStatus == APP_CONST.INSPECTION_STATUS_SAVED) ? failInspection : null,
                                  ),
                                )),
                            Padding(
                                padding: EdgeInsets.only(left: 0, top: 14, right: 24, bottom: 14),
                                child: Container(
                                  width: ScreenUtil().setWidth(180),
                                  height: ScreenUtil().setHeight(48),
                                  child: FlatButton(
                                    child: Text(
                                      'SAVE',
                                      style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(14),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: ScreenUtil().setWidth(1.25),
                                          color: Colors.white),
                                    ),
                                    color: Color(0xFF3e8aeb),
                                    textColor: Colors.white,
                                    disabledColor: Colors.grey,
                                    disabledTextColor: Colors.black,
                                    splashColor: Color(0xFF3e8aeb),
                                    onPressed:!checkUserPermission(["Inspection - Edit"]) ? null :
                                        (railcarInspectionStatus == "--" || railcarInspectionStatus == APP_CONST.INSPECTION_STATUS_SAVED) ? () => inspect(APP_CONST.INSPECTION_STATUS_SAVED) : null,
                                  ),
                                )),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
                SizedBox(height: ScreenUtil().setHeight(24)),
                Divider(
                  thickness: 2.0,
                ),
                SizedBox(height: ScreenUtil().setHeight(21)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Template',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal)),
                          SizedBox(
                            height: 5,
                          ),
                          Text(inspectionRailcarModel.inspectionTemplate.templateName ?? "",
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(20, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500))
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Railcar',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal)),
                          SizedBox(
                            height: 5,
                          ),
                          Text(inspectionRailcarModel.equipment ?? "",
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(20, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500))
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Inspection Status',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal)),
                          SizedBox(
                            height: 5,
                          ),
                          Text(railcarInspectionStatus ?? "",
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(20, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500))
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Inspection Date',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal)),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                              // railcarInspectionDate== null? "-" : formatter.format(DateTime.parse(railcarInspectionDate)) ,

                              lastModifiedDate ?? "--",
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(20, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500))
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('User',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal)),
                          SizedBox(
                            height: 5,
                          ),
                          Text(inspectionUser ?? "--",
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(20, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500))
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: ScreenUtil().setHeight(31.5)),
                Divider(
                  thickness: 2.0,
                ),
                SizedBox(height: ScreenUtil().setHeight(23.5)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(20),
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 4.0),
                          alignment: Alignment.center,
                          child: Text('Inspection Criteria',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.normal)),
                        )),
                  ],
                ),
                ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: inspectionRailcarModel.inspectionTemplate.inspectionTemplateQuestionList.length,
                    itemBuilder: (context, index) {
                     int imgCountInQuestionare =0;
                      InspectionAnswer inspectionAnswer;
                      InspectionTemplateQuestion question = inspectionRailcarModel.inspectionTemplate.inspectionTemplateQuestionList[index];
                      if (existingInspection != null) {
                        var first = existingInspection.answers.where((e) => e.id == question.id).isNotEmpty ? existingInspection.answers.where((e) => e.id == question.id).first : null;
                        inspectionAnswer = first;
                        if (inspectionAnswer == null) {
                          inspectionAnswer = InspectionAnswer(category: question.category, name: question.name, id: question.id, answer: null, imgList: []);
                          existingInspection.answers?.add(inspectionAnswer);
                        }
                      } else {
                        if (inspectionAnswer == null) {
                          inspectionAnswer = InspectionAnswer(category: question.category, name: question.name, id: question.id, answer: null, imgList: []);
                        }
                        existingInspection = InspectionRailcarModel(answers: []);
                        existingInspection.answers?.add(inspectionAnswer);
                      }
                      inspectionAnswer.inspectionQuestion = question;

                      TextEditingController textBoxController;
                      TextEditingController textAreaController;
                      List<String> checkboxlist = [];
                      if (question.category == "DatePicker") {
                        inspectionAnswer.answer = inspectionAnswer.answer ?? DateFormat('MM/dd/yyyy').format(DateTime.now().toLocal());
                      } else if (question.category != "CheckBox") {
                        textBoxController = new TextEditingController(text: inspectionAnswer.answer);
                        textAreaController = new TextEditingController(text: inspectionAnswer.answer);
                      } else {
                        checkboxlist = inspectionAnswer.answer?.toString()?.replaceAll("[", "")?.replaceAll("]", "")?.replaceAll("|", ",")?.trim()?.split(",")?.map((e) => e.trim())?.toList();
                        checkboxlist?.removeWhere((element) => element?.isEmpty);
                        List<String> temp = [];
                        question.optionList.forEach((question) {
                          checkboxlist?.forEach((answr) {
                            if (answr.toString() == question.toString()) {
                              temp.add(question);
                            }
                          });
//                          checkboxlist.where((element) => element==question).isNotEmpty?temp.add(question):null;
                        });
                        checkboxlist = temp;
                        print("$checkboxlist");
                      }

                      void checkBoxSelectionChanged(List<String> list) {
                        if (list.length > 0) {
                          inspectionAnswer.answer = list.toString()?.replaceAll("[", "")?.replaceAll("]", "")?.replaceAll(",", "|");
                          checkboxlist = list;
                        } else {
                          inspectionAnswer.answer = null;
                          checkboxlist = [];
                        }
                      }

                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          SizedBox(
                            height: 60.0,
                          ),
                          Container(
                            width: 180,
                            margin: EdgeInsets.fromLTRB(20, 5, 20, 20),
                            child: Text(
                              question.isRequired == "Yes" ? "${question.name} *" : "${question.name}",
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                          question.category == "YesNo"
                              ? Container(
                                  padding: EdgeInsets.all(20),
                                  width: ScreenUtil().setWidth(452),
                                  height: 100,
                                  child: RadioGroup<String>.builder(
                                    groupValue: inspectionAnswer.answer,
                                    items: question.optionList ?? ["Yes", "No"],
                                    direction: Axis.horizontal,
                                    itemBuilder: (item) => RadioButtonBuilder(
                                      item,
                                    ),
                                    onChanged: (value) {
//                                                      yesNoSelectionChanged(value);
                                      setState(() {
                                        inspectionAnswer.answer = value;
//                                                        yesNoValueChanged = true;
                                      });
                                    },
                                  ),
                                )
                              : question.category == "Radio"
                                  ? Container(
                                      padding: EdgeInsets.all(20),
                                      width: ScreenUtil().setWidth(452),
                                      height: 100,
                                      child: RadioGroup<String>.builder(
                                        groupValue: inspectionAnswer.answer,
                                        items: question.optionList ?? ["Yes", "No"],
                                        direction: Axis.horizontal,
                                        itemBuilder: (item) => RadioButtonBuilder(
                                          item,
                                        ),
                                        onChanged: (value) {
//                                                          radioSelectionChanged(value);
                                          setState(() {
                                            inspectionAnswer.answer = value;
//                                                            radioValueChanged = true;
                                          });
                                        },
                                      ),
                                    )
                                  : question.category == "DDL"
                                      ? Container(
                                          width: ScreenUtil().setWidth(452),
                                          child: CustomDropdown(
                                            hintText: "",
                                            itemList: question.optionList.toSet().toList(),
                                            SelectedValue: ((inspectionAnswer?.answer?.toString()) ?? "").trim().isEmpty ? null : inspectionAnswer.answer,
                                            Ontap: (String selectedValue) {
                                              setState(() {
                                                inspectionAnswer.answer = selectedValue;
                                              });
                                            },
                                          ),
                                        )
                                      : question.category == "CheckBox"
                                          ? Container(
                                              width: ScreenUtil().setWidth(452),
                                              child: GroupedCheckbox(
                                                  itemList: question.optionList,
                                                  onChanged: (list) {
                                                    checkBoxSelectionChanged(list);
                                                    setState(() {});
//
                                                  },
                                                  orientation: null,
                                                  checkedItemList: checkboxlist),
                                            )
                                          : question.category == "TextBox"
                                              ? Container(
                                                  width: ScreenUtil().setWidth(452),
                                                  alignment: Alignment.center,
                                                  child: TextField(
                                                    controller: textBoxController,
                                                    onChanged: (newText) {
                                                      inspectionAnswer.answer = newText;
                                                    },
                                                    style: TextStyle(
                                                        color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                            ? Color(0xffffffff)
                                                            : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff182e42) : null),
                                                    decoration: InputDecoration(
                                                      contentPadding: EdgeInsets.all(1.0),
                                                      errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                                                      focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                                                      focusedBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(1.0),
                                                        borderSide: BorderSide(
                                                          width: 1,
                                                          color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                              ? Color(0xfff5f5f5)
                                                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                                                        ),
                                                      ),
                                                      enabledBorder: const OutlineInputBorder(
                                                        borderSide: const BorderSide(color: Colors.black38, width: 1),
                                                      ),
                                                      filled: true,
                                                      fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                          ? Color(0xff172636)
                                                          : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                                      hintText: 'Enter Text Here',
                                                      hintStyle: TextStyle(
                                                        fontSize: 15.0,
                                                        color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                            ? Color(0xffffffff)
                                                            : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff182e42) : null,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              : question.category == "TextArea"
                                                  ? Container(
                                                      width: ScreenUtil().setWidth(452),
                                                      alignment: Alignment.center,
                                                      child: TextField(
                                                        minLines: 3,
                                                        controller: textAreaController,
                                                        onChanged: (newText) {
                                                          inspectionAnswer.answer = newText;
                                                        },
                                                        style: TextStyle(
                                                            color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                                ? Color(0xffffffff)
                                                                : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff182e42) : null),
                                                        decoration: InputDecoration(
                                                          contentPadding: EdgeInsets.all(1.0),
                                                          errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                                                          focusedErrorBorder:
                                                              OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                                                          focusedBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(1.0),
                                                            borderSide: BorderSide(
                                                              width: 1,
                                                              color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                                  ? Color(0xfff5f5f5)
                                                                  : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                                                            ),
                                                          ),
                                                          enabledBorder: const OutlineInputBorder(
                                                            borderSide: const BorderSide(color: Colors.black38, width: 1),
                                                          ),
                                                          filled: true,
                                                          fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                              ? Color(0xff172636)
                                                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                                          hintText: 'Enter Text Here',
                                                          hintStyle: TextStyle(
                                                            fontSize: 15.0,
                                                            color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                                ? Color(0xffffffff)
                                                                : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff182e42) : null,
                                                          ),
                                                        ),
                                                        maxLines: 5,
                                                      ),
                                                    )
                                                  : question.category == "DatePicker"
                                                      ? Container(
                                                          width: ScreenUtil().setWidth(452),
                                                          alignment: Alignment.center,
                                                          child: TextField(
                                                            readOnly: true,
                                                            style: TextStyle(
                                                                color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                                    ? Color(0xffffffff)
                                                                    : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff182e42) : null),
                                                            decoration: InputDecoration(
                                                                contentPadding: EdgeInsets.all(1.0),
                                                                errorBorder:
                                                                    OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                                                                focusedErrorBorder:
                                                                    OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(1)), borderSide: BorderSide(width: 1, color: Colors.red)),
                                                                focusedBorder: OutlineInputBorder(
                                                                  borderRadius: BorderRadius.circular(1.0),
                                                                  borderSide: BorderSide(
                                                                    width: 1,
                                                                    color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                                        ? Color(0xfff5f5f5)
                                                                        : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                                                                  ),
                                                                ),
                                                                enabledBorder: const OutlineInputBorder(
                                                                  borderSide: const BorderSide(color: Colors.black38, width: 1),
                                                                ),
                                                                filled: true,
                                                                fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                                    ? Color(0xff172636)
                                                                    : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
                                                                hintText: inspectionAnswer.answer ?? "Enter Date",
                                                                hintStyle: TextStyle(
                                                                    fontSize: 15.0,
                                                                    color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                                                                        ? Color(0xffffffff)
                                                                        : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff182e42) : null),
                                                                prefixIcon: GestureDetector(
                                                                    onTap: () {
                                                                      showDatePicker(
                                                                              context: context,
                                                                              initialDate: DateTime.now(),
                                                                              firstDate: DateTime.now().subtract(Duration(days: 1)),
                                                                              lastDate: DateTime(2022))
                                                                          .then((date) {
//                                                                                        dateChanged(date);
                                                                        setState(() {
                                                                          inspectionAnswer.answer = DateFormat('MM/dd/yyyy').format(date);
//                                                                                          selectedDateTime = date;
                                                                        });
                                                                      }
                                                                              // else{
                                                                              //   setState(() {
                                                                              //     selectedDate = DateTime.now();
                                                                              //   });
                                                                              // }
//                                                                                       dateChanged(date);
                                                                              );
                                                                    },
                                                                    child: Icon(Icons.date_range, color: Theme.of(context).textTheme.bodyText1.color))),
                                                          ),
                                                        )
                                                      : question.category == "Signature"
                                                          ? Container(width: 700, height: 400, child: SignatureSample(index, inspectionAnswer))
                                                          : question.category == "Label"
                                                              ? Container(
                                                                  width: ScreenUtil().setWidth(555),
                                                                )
                                                              : Container(
                                                                  child: Text(
                                                                  'No Widget Selected',
                                                                  style: TextStyle(fontSize: 20),
                                                                )),
                          question.category != "Label" && question.category != "Signature"
                              ? Row(
                                  children: <Widget>[
                                    IconButton(
                                      icon: Icon(Icons.camera_alt),
                                      iconSize: 36,
                                      onPressed: !checkUserPermission(["Inspection - Edit"]) ? null : inspectionAnswer.imgList
                                          .length > 4
                                          ? null
                                          : () async {
                                        existingInspection.answers.forEach((
                                            element) {
                                          imgCountInQuestionare =
                                              imgCountInQuestionare +
                                                  element.imgList.length;
                                        });
                                        if (imgCountInQuestionare >= 10) {
                                          _showWarnToast(
                                              "Inspection already has 10 images");
                                          return;
                                        }

                                              final picker = ImagePicker();

                                              final pickedFile = await picker.getImage(source: ImageSource.camera);

                                              if (pickedFile != null) {
                                                // File _image = File(pickedFile.path);
                                                var result = await FlutterImageCompress.compressWithFile(
                                                    pickedFile.path,
                                                    minWidth: 500,
                                                    minHeight: 600,
                                                    quality: 80);
                                                // var image = imagePub.decodeImage(_image.readAsBytesSync());
                                                // // Resize the image to a 120x? thumbnail (maintaining the aspect ratio).
                                                // var thumbnail = imagePub.copyResize(image, width: 500);
                                                // // Save the thumbnail as a PNG.
                                                // var file = new Io.File(pickedFile.path)..writeAsBytesSync(imagePub.encodePng(thumbnail));
                                                // final bytess = file.readAsBytesSync();
                                                String img64 = base64Encode(result);
                                                inspectionAnswer.imgList.add(img64);
                                              } else {
                                                print('No image selected.');
                                              }
                                              setState(() => {});
                                            },
                                    ),
                                    IconButton(
                                      icon: Icon(Icons.photo),
                                      iconSize: 36,
                                      onPressed: () {
                                        inspectionAnswer.imgList.isEmpty  ? _showErrorToast("No images saved"):
                                        Timer(Duration(seconds: 1), () {
                                          print("waited for 1 second to allow decoding of images.");
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => DisplayPictureScreen(
                                                      inspectionAnswer: inspectionAnswer,
                                                    )),
                                          ).then((value) => setState(() {}));
                                        });
                                      },
                                    ),
                                    Text("(${inspectionAnswer.imgList.length})")
                                  ],
                                )
                              : Container()
                        ],
                      );
                    }),
                Divider(
                  thickness: 1.0,
                ),
                Center(
                  child: RaisedButton(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "COMPLETE INSPECTION",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                        fontFamily: 'Roboto',
                      ),
                    ),
                    color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? buttonColour : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? buttonColour : null,
                    textColor: Colors.white,
                    onPressed: !checkUserPermission(["Inspection - Edit"]) ? null : (railcarInspectionStatus == APP_CONST.INSPECTION_STATUS_COMPLETED) ? null : () => inspect(APP_CONST.INSPECTION_STATUS_COMPLETED),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
