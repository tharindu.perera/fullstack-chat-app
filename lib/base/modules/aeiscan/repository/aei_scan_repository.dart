//import 'dart:math';

import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/util/nullCheckUtil.dart';

class AEIScanRepository {
  EquipmentDAO equipmentDAO = EquipmentDAO();

  Stream<List<Equipment>> getRecordByCriteria(ascending, sortColumnIndex) {

    List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"];
    _sortData(scannedEquipList, ascending, sortColumnIndex);
    Stream<List<Equipment>> streamList = Stream.value(scannedEquipList);
    MemoryData.dataMap["scannedEquipList"]=scannedEquipList;
    return streamList;
  }

  _sortData(List<Equipment> list, ascending, sortColumnIndex) {
    if (sortColumnIndex == 0) {
      ascending
          ? list.sort((a, b) => (a.sequence).compareTo(b.sequence))
          : list.sort((a, b) => ((b.sequence).compareTo(a.sequence)));
    } else if (sortColumnIndex == 1) {
      ascending
          ? list.sort((a, b) => a.equipmentInitial.compareTo(b.equipmentInitial))
          : list.sort((a, b) => b.equipmentInitial.compareTo(a.equipmentInitial));
    } else if (sortColumnIndex == 2) {
      ascending
          ? list.sort((a, b) => a.equipmentNumber.compareTo(b.equipmentNumber))
          : list.sort((a, b) => b.equipmentNumber.compareTo(a.equipmentNumber));
    } else if (sortColumnIndex == 3) {
      ascending
          ? list.sort((a, b) => setNullToEmptyString(MemoryData.inMemoryYardMap[a.yardId]?.yardName)
            .compareTo(setNullToEmptyString(MemoryData.inMemoryYardMap[b.yardId]?.yardName)))
          : list.sort((a, b) => setNullToEmptyString(MemoryData.inMemoryYardMap[b.yardId]?.yardName)
            .compareTo(setNullToEmptyString(MemoryData.inMemoryYardMap[a.yardId]?.yardName)));
    } else if (sortColumnIndex == 4) {
      ascending
          ? list.sort((a, b) => setNullToEmptyString(MemoryData.inMemoryTrackMap[a.trackId]?.trackName)
          .compareTo(setNullToEmptyString(MemoryData.inMemoryTrackMap[b.trackId]?.trackName)))
          : list.sort((a, b) => setNullToEmptyString(MemoryData.inMemoryTrackMap[b.trackId]?.trackName)
          .compareTo(setNullToEmptyString(MemoryData.inMemoryTrackMap[a.trackId]?.trackName)));
    }
  }
}
