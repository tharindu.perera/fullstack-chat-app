import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/aeiscan/ui/inbound/inbound_car_form_bloc.dart';
import 'package:mbase/base/modules/aeiscan/ui/inbound/show_pop_func.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class AddScreenHeader extends StatelessWidget {
  List<Equipment> selectedEquipList;

  AddScreenHeader(List<Equipment> selectedEquipList) {
    this.selectedEquipList = selectedEquipList;
    print("selectedEquipList length:${this.selectedEquipList.length}");
    MemoryData.dataMap.remove("selectedEquipList");
    MemoryData.dataMap.putIfAbsent("selectedEquipList", () => this.selectedEquipList);
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Container(
                      child: ListTile(
                    leading: GestureDetector(
                        onTap: () {
                          if (BlocProvider.of<InboundCarFormBloc>(context).isStatusChanged) {
                            popUpDialog(context);
                          } else {
                            Navigator.pop(context);
                            BlocProvider.of<AEIScanBloc>(context).add(AEIScanFetchDataWithLastSetCriteria());
                          }
                        },
                        child: Icon(
                          Icons.arrow_back,
                        )),
                    title: Text('Inbound Railcar (${selectedEquipList.length})',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Roboto',
                            color: Theme.of(context)
                                .textTheme
                                // ignore: deprecated_member_use
                                .headline
                                .color)),
                  ))),
            ),
            Row(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(20),
                    child: Container(
                        child: GestureDetector(
                      onTap: () {
                        if (BlocProvider.of<InboundCarFormBloc>(context).isStatusChanged) {
                          popUpDialog(context);
                        } else {
                          Navigator.pop(context);
                          BlocProvider.of<AEIScanBloc>(context).add(AEIScanFetchDataWithLastSetCriteria());
                        }
                      },
                      child: RichText(
                        // ignore: deprecated_member_use
                        text: TextSpan(style: Theme.of(context).textTheme.body1, children: <InlineSpan>[
                          TextSpan(
                              text: 'CANCEL',
                              style: TextStyle(
                                  fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  fontFamily: 'Roboto',
                                  color: Color(0xFF3e8aeb)))
                        ]),
                      ),
                    ))),
                Padding(
                    padding: EdgeInsets.all(20),
                    child: Container(
                      // width: 150,
                      width: ScreenUtil().setWidth(180),
                      height: ScreenUtil().setHeight(48),
                      child: FlatButton(
                          onPressed: () {
                            context.bloc<InboundCarFormBloc>().submit();
                          },
                          child: Text('INBOUND RAILCAR(S)',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  color: Colors.white)),
                          color: Color(0xFF3e8aeb),
                          textColor: Colors.white,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          splashColor: Color(0xFF3e8aeb)),
                    ))
              ],
            ),
          ],
        ),
        Divider(
          thickness: 5.0,
        ),
        SizedBox(
          height: 10.0,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
          child: Wrap(
              direction: Axis.horizontal,
              spacing: ScreenUtil().setWidth(42),
              runSpacing: ScreenUtil().setHeight(10),
              children: selectedEquipList.map((railcar) {
                Widget railcarWidget = Container(
                    width: ScreenUtil().setWidth(160),
                    child: ListTile(
                      title: Text(
                        'Railcar',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Roboto',
                            letterSpacing: ScreenUtil().setWidth(0.5),
                            color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xFFffffff) : Color.fromRGBO(0, 0, 0, 0.6)),
                      ),
                      subtitle: Text('${railcar.equipmentInitial} ${railcar.equipmentNumber}',
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(20, allowFontScalingSelf: true),
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w500,
                              fontFamily: 'Roboto',
                              letterSpacing: ScreenUtil().setWidth(0.25),
                              // ignore: deprecated_member_use
                              color: Theme.of(context).textTheme.headline.color)),
                    ));
                return railcarWidget;
              }).toList()),
        ),
        SizedBox(height: ScreenUtil().setHeight(24)),
        Divider(thickness: 2.0),
        SizedBox(height: ScreenUtil().setHeight(24)),
      ],
    );
  }
}
