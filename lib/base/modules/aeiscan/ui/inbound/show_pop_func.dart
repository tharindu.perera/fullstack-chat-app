import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mbase/base/core/components/custom_dialog/custom_dialog.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';

popUpDialog(context) async {
  final action = await CustomDialog.cstmDialog(context, "add_railcar", "Unsaved Changes", "");
  if (action[0] == DialogAction.yes) {
    Navigator.pop(context);
    BlocProvider.of<AEIScanBloc>(context).add(AEIScanFetchDataWithLastSetCriteria());
  }
}
