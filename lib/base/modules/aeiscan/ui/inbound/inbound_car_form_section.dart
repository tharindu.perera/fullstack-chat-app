import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/components/custom_toast/custom_toast.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/aeiscan/ui/inbound/header_section.dart';
import 'package:mbase/base/modules/aeiscan/ui/inbound/inbound_car_form_bloc.dart';
import 'package:mbase/base/modules/listview/add_railcar/repository/addrailcar_repo.dart';
import 'package:provider/provider.dart';


// ignore: must_be_immutable
class InboundCarForm extends StatelessWidget {
  final dateTimeFormatter = new DateFormat('MM/dd/yyyy HH:mm');
  List<Equipment> selectedEquipList;

  InboundCarForm(List<Equipment> selectedRow) {
    this.selectedEquipList = selectedRow;
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    return BlocProvider(
      create: (context) => InboundCarFormBloc(
        AddRailCarRepo(),
      ),
      child: Builder(
        builder: (context) {
          final formBloc = context.bloc<InboundCarFormBloc>();
          return FormBlocListener<InboundCarFormBloc, String, String>(
            onSuccess: (context, state) {
              FToast(context).showToast(
                child: CustomToast(
                  icon: Icons.check,
                  toastColor: Color(0xff7fae1b),
                  toastMessage: state.successResponse,
                ),
                gravity: ToastGravity.TOP,
                toastDuration: Duration(seconds: 2),
              );
              Navigator.pop(context);
              BlocProvider.of<AEIScanBloc>(context).add(AEIScanFetchDataWithLastSetCriteria());
            },
            onFailure: (context, state) {
              FToast(context).showToast(
                child: CustomToast(
                  icon: Icons.new_releases,
                  toastColor: Colors.redAccent,
                  toastMessage: state.failureResponse,
                ),
                gravity: ToastGravity.TOP,
                toastDuration: Duration(seconds: 4),
              );
              Navigator.pop(context);
              BlocProvider.of<AEIScanBloc>(context).add(AEIScanFetchDataWithLastSetCriteria());
            },
            child: Column(
              children: <Widget>[
                AddScreenHeader(selectedEquipList),
                Row(
                  children: [
                    SizedBox(width: ScreenUtil().setWidth(24)),
                    Theme(
                      data: Theme.of(context)
                          .copyWith(inputDecorationTheme: InputDecorationTheme(
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(1)))),
                      child: Container(
                        width: ScreenUtil().setWidth(214),
                        child: DropdownFieldBlocBuilder<YardModel>(
                          selectFieldBloc: formBloc.yardDropDown,

                          decoration: InputDecoration(
                            filled: true,
                            fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xff172636):
                            _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)? Color(0xfff5f5f5):null,
                            labelText: 'Select Yard',
                          ),
                          itemBuilder: (context, value) => value.yardName,
                        ),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(24)),
                    Theme(
                      data: Theme.of(context)
                          .copyWith(inputDecorationTheme: InputDecorationTheme(border: OutlineInputBorder(borderRadius: BorderRadius.circular(1)))),
                      child: Container(
                        width: ScreenUtil().setWidth(214),
                        child: DropdownFieldBlocBuilder<TrackModel>(
                          selectFieldBloc: formBloc.trackDropDown,
                          decoration: InputDecoration(

                            filled: true,
                            fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xff172636):
                            _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)? Color(0xfff5f5f5):null,
                            labelText: 'Select Track',
                          ),
                          itemBuilder: (context, value) => value.trackName,
                        ),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(24)),
                    Container(
                      width: ScreenUtil().setWidth(240),
                      child: DateTimeFieldBlocBuilder(
                        style: TextStyle( color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                        _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null,),
                        dateTimeFieldBloc: formBloc.dateTime,
                        format: dateTimeFormatter,
                        canSelectTime: true,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1900),
                        lastDate: DateTime(2100),
                        decoration: InputDecoration(

                          prefixIcon: Icon(
                            Icons.timer,
                            color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                            _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null,
                          ),
                          contentPadding: EdgeInsets.symmetric(vertical: 15),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(1)),
                              borderSide: BorderSide(width: 1,color: Colors.red)
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(1)),
                              borderSide: BorderSide(width: 1,color: Colors.red)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(1.0),
                            borderSide: BorderSide(width: 1, color:Colors.black38 ),
                          ),
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.black38, width: 1),
                          ),
                          filled: true,
                          fillColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xff172636):
                          _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)? Color(0xfff5f5f5):null,
                          hintText: 'Select Date Time',
                          hintStyle: TextStyle(
                            fontSize: ScreenUtil().setHeight(14),
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.normal,
                            fontStyle: FontStyle.normal,
                            letterSpacing: ScreenUtil().setWidth(0.25),
                            color: _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Color(0xfff5f5f5):
                            _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)?  Color(0xff172636):null,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(10),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(1)),
                  child: RadioButtonGroupFieldBlocBuilder<String>(
                    selectFieldBloc: formBloc.sequenceOfCar,
                    itemBuilder: (context, value) => value,
                    labelStyle: TextStyle(
                      color:
                      _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Colors.white:
                      _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)? Colors.black:null,
                    ),
                    decoration: InputDecoration(
                      labelStyle: TextStyle(
                        color:   _themeChanger.getTheme().primaryColor == Color(0xff182e42)? Colors.white:
                        _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)? Colors.black:null,
                        fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                      ),
                      border: InputBorder.none,
                      labelText: 'Sequence of Railcar',
                      contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                      prefixIcon: SizedBox(),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
