import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/modules/aeiscan/ui/inbound/inbound_car_form_section.dart';

// ignore: must_be_immutable
class InboundCar extends StatelessWidget {
  List<Equipment> selectedEquipList;
  InboundCar(List<Equipment> selectedRow) {
    this.selectedEquipList = selectedRow;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(24),
          vertical: ScreenUtil().setHeight(24)),
      child: CardUI(
          content: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: InboundCarForm(this.selectedEquipList),
      )),
    );
  }
}
