import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/modules/listview/add_railcar/repository/addrailcar_repo.dart';

class InboundCarFormBloc extends FormBloc<String, String> {

  final AddRailCarRepo _addRailCarRepository;
  bool isStatusChanged = false;

  final yardDropDown = SelectFieldBloc<YardModel, dynamic>(
    name: "yard_id",
    toJson: (value) => value.toMap(),
    validators: [FieldBlocValidators.required],
    initialValue: MemoryData.inMemoryYardMap[MemoryData.facilityConfiguration.defaultInboundYardId],
  );

  final trackDropDown = SelectFieldBloc<TrackModel, dynamic>(
    name: "track_id",
    toJson: (value) => value.toMap(),
    validators: [FieldBlocValidators.required],
    initialValue: MemoryData.inMemoryTrackMap[MemoryData.facilityConfiguration.defaultInboundTrackId],
  );

  final sequenceOfCar = SelectFieldBloc(name: 'Sequence', items: ['Front', 'Back'], validators: [FieldBlocValidators.required]);

  final dateTime = InputFieldBloc<DateTime, Object>(name: 'inboundDateTime', toJson: (value) => value.toUtc(), validators: [FieldBlocValidators.required],initialValue: DateTime.now());

  InboundCarFormBloc(this._addRailCarRepository) {
    _loadDropDownDataAndListen();
    addFieldBlocs(
      fieldBlocs: [
        yardDropDown,
        trackDropDown,
        sequenceOfCar,
        dateTime,
      ],
    );
  }

  _loadDropDownDataAndListen() async {
    List<YardModel> yardList = MemoryData.inMemoryYardMap.values.toList();
    List<TrackModel> tracksWithoutSpotsList = MemoryData.inMemoryTracksWithoutSpotsMap.values.toList();

    yardList.forEach((element) => yardDropDown.addItem(element));
    var filteredTracks = tracksWithoutSpotsList.where((element) => element.yardId == MemoryData.facilityConfiguration.defaultInboundYardId).toList();

    filteredTracks.forEach((element) =>trackDropDown.addItem(element));
    yardDropDown.listen((covariant) async {
      trackDropDown.clear();
      var filteredTracks2 = tracksWithoutSpotsList.where((element) => element.yardId == yardDropDown?.value?.id).toList();
      !covariant.isInitial ? trackDropDown.updateItems(filteredTracks2) : null;
    });
    this.listen((covariant) async {
      if (yardDropDown.value != null || trackDropDown.value != null
          || sequenceOfCar.value != null || dateTime.value != null) {
        isStatusChanged = true;
      } else {
        isStatusChanged = false;
      }
    });
  }

  @override
  void onSubmitting() async {
    try {
        List<Equipment> selectedEquipList = MemoryData.dataMap["selectedEquipList"];
          selectedEquipList.forEach((equipment) {
           try {
             _addRailCarRepository.addRailcar(
                equipment.equipmentInitial, equipment.equipmentNumber, yardDropDown.value.id,
                trackDropDown.value.id,null, dateTime.value, sequenceOfCar.value);
             equipment.placedDate=dateTime.value;
             equipment.yardId=yardDropDown.value.id;
             equipment.trackId=trackDropDown.value.id;

          } catch (exception) {
            emitFailure(failureResponse: exception.toString());
          }
        });
        emitSuccess(successResponse: "Railcar(s) Inbounded Successfully");
    } catch (exception) {
      print("Inbound car saving error >>>${exception.toString()}");
      emitFailure(failureResponse: exception.toString());
    }
  }
}
