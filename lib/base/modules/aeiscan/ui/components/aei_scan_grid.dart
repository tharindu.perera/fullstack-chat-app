import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_paginated_datatable.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/aeiscan/ui/components/aei_scan_ds.dart';
import 'package:provider/provider.dart';

class AEIScanGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AEIScanBloc aeiScanBloc = BlocProvider.of<AEIScanBloc>(context);
    AEIScanDataSource listGridDataSource = aeiScanBloc.state.dataSource;
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    return CustomPaginatedDataTable(
      sortArrowColor: Colors.white,
      cardBackgroundColor: Theme.of(context).canvasColor,
      footerTextColor: Theme.of(context)
          .textTheme
          // ignore: deprecated_member_use
          .body1
          .color,
      rowColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff1d2e40)
          : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xffffffff) : null,

      columnColor: Color(0xff274060),
      checkboxBorderColor:
          _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white
              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color.fromRGBO(0, 0, 0, 0.54) : null,
      dataRowHeight: ScreenUtil().setHeight(47),
      horizontalMargin: ScreenUtil().setWidth(19),
      columnSpacing: ScreenUtil().setWidth(52),
      header: SizedBox(
        width: 1,
        height: 1,
      ),
      columns: [
        CustomDataColumn(
          onSort: (int columnIndex, bool ascending) {
            aeiScanBloc.add(AEIScanGridSortedClicked(columnIndex, ascending));
          },
          label: Row(
            children: [
              Container(
                  child: Text(
                "SEQUENCE",
                style: TextStyle(
                  fontFamily: 'Roboto',
                  // ignore: deprecated_member_use
                  color: Colors.white,
                  fontSize: ScreenUtil().setHeight(15),
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.normal,
                  letterSpacing: ScreenUtil().setWidth(1.75),
                ),
              ))
            ],
          ), numeric: true
        ),
        CustomDataColumn(
          label: Text("INITIAL",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )
          ),
          numeric: false,
          onSort: (int columnIndex, bool ascending) {
            aeiScanBloc.add(AEIScanGridSortedClicked(columnIndex, ascending));
          },
        ),
        CustomDataColumn(
          label: Text("NUMBER",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: true,
          onSort: (int columnIndex, bool ascending) {
            aeiScanBloc.add(AEIScanGridSortedClicked(columnIndex, ascending));
          },
        ),
        CustomDataColumn(
          label: Text("YARD",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
          onSort: (int columnIndex, bool ascending) {
            aeiScanBloc.add(AEIScanGridSortedClicked(columnIndex, ascending));
          },
        ),
        CustomDataColumn(
          label: Text("TRACK",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                letterSpacing: ScreenUtil().setWidth(1.43),
              )),
          numeric: false,
          onSort: (int columnIndex, bool ascending) {
            aeiScanBloc.add(AEIScanGridSortedClicked(columnIndex, ascending));
          },
        ),
      ],
      source: listGridDataSource ?? [],
      sortAscending: aeiScanBloc.ascending,
      sortColumnIndex: aeiScanBloc.sortColumnIndex,
      rowsPerPage: 5,
    );
  }
}
