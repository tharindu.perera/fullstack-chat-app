//import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/loader/LoadingDialog.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/listview/add_railcar/repository/addrailcar_repo.dart';
import 'package:uuid/uuid.dart';

class ScanButton extends StatefulWidget {
  @override
  _ScanButtonState createState() => _ScanButtonState();
}

class _ScanButtonState extends State<ScanButton> {
  String selectedRailcar = null;
  FToast fToast;
  static const platform = const MethodChannel('com.wabtec.flutter_scy/thingMagicTagData');
  //int scanClickCount = 1;

  @override
  Widget build(BuildContext context) {
    fToast = FToast(context);
    return Container(
      width: ScreenUtil().setWidth(133),
      height: ScreenUtil().setHeight(50),
      child: FlatButton(
          onPressed: () => _getThingMagicTagData(context),
          child: Text(
            'SCAN',
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.25),
                color: Colors.white),
          ),
          color: Color(0xFF508be4),
          textColor: Colors.white,
          disabledColor: Colors.grey,
          disabledTextColor: Colors.black,
          splashColor: Color(0xFF508be4)),
    );
  }

  Future<void> _getThingMagicTagData(BuildContext context) async {
    print("1>>>>> ${DateTime.now().toIso8601String()}");
    LoadingDialog.show(context);
    await Future.delayed(Duration(seconds: 1));
    print("2>>>>> ${DateTime.now().toIso8601String()}");
    String scanResult = "No Tag";
    try {
      scanResult = await platform.invokeMethod('getThingMagicTagData');
      print("scanResult:$scanResult >>>>> ${DateTime.now().toIso8601String()}");
      // result is "No Tag" for no bar code
//      scanResult = "9A0181C78900A913A2000000002E873D"; // ABCD 123456
//      scanResult = "9B4967061DA0B60B9000000000000331"; //RCRX 100200
//      print("Hard coded scanResult:$scanResult");
      if (scanResult == null || scanResult == "No Tag") {
        print("4>>>>> ${DateTime.now().toIso8601String()}");
        LoadingDialog?.hide(context);
        print("5>>>>> ${DateTime.now().toIso8601String()}");

        _showToastError("No tag scanned or an invalid tag is scanned.");
        print("6>>>>> ${DateTime.now().toIso8601String()}");

      } else {
        print("7>>>>> ${DateTime.now().toIso8601String()}");
        Equipment equipment =  convertToEquipment(scanResult);
//        equipment.equipmentNumber= Random().nextInt(100).toString();
        print(" 8 >>>>> ${DateTime.now().toIso8601String()}");

        bool isAddedToGrid =  addScannedCarToGrid(equipment);
        print(" 9 >>>>> ${DateTime.now().toIso8601String()}");

        LoadingDialog?.hide(context);

        if(!isAddedToGrid){
          _showToastError("Equipment is already in the list.");
          return;
        }

        print(" 10 >>>>> ${DateTime.now().toIso8601String()}");
        checkAndUpdateScannedCarWithExistingInfo(equipment);
        print("11>>>>> ${DateTime.now().toIso8601String()}");
       }

    }  catch (e,stc) {
      print("12>>>>> ${DateTime.now().toIso8601String()}");
      print(stc);
      print("13>>>>> ${DateTime.now().toIso8601String()}");
      LoadingDialog?.hide(context);
      print("14>>>>> ${DateTime.now().toIso8601String()}");
      _showToastError("No tag scanned or an invalid tag is scanned.");
      print("15>>>>> ${DateTime.now().toIso8601String()}");
      print("Failed to read Tag: '${e.message}'.");
    }
  }

 Equipment convertToEquipment(String dataAscii)   {
    print("dataAscii:$dataAscii");
    String bitSequence = '';
    for (int i = 0; i < dataAscii.length; i++) {
      var char = dataAscii[i];  // convert the hex to binary, add leading zeros if necessary
      bitSequence = bitSequence + int.parse(char, radix: 16).toRadixString(2).padLeft(4, '0');
    }
    print("bitSequence:$bitSequence");
    // Equipment Initial 7 - 25 bits
    String equipInitial = getEquipmentInitial(bitSequence.substring(7, 26));
    // Equipment Number 26 - 45 bits
    String equipNumber = getEquipmentNumber(bitSequence.substring(26, 46));
    print("Equipment:$equipInitial $equipNumber");
    return  Equipment(equipmentInitial:equipInitial,equipmentNumber: equipNumber );
  }

  bool addScannedCarToGrid(Equipment scannedEquipment)   {
      // Add leading zeros if equipment number length is less than 6
    if (scannedEquipment.equipmentNumber.length < 6) {
      print("adding leading zeros...");
      scannedEquipment.equipmentNumber = scannedEquipment.equipmentNumber.padLeft(6, '0');
    }
     List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"];

    var firstWhere = scannedEquipList.firstWhere((element) => element.equipmentInitial==scannedEquipment.equipmentInitial
        && scannedEquipment.equipmentNumber==element.equipmentNumber,orElse: ()=>null);
     if(firstWhere==null){
      scannedEquipment.id = Uuid().v1();
      scannedEquipment.isSelected = false;
       scannedEquipment.yardId = "-";
      scannedEquipment.trackId = "-";
      scannedEquipList.add(scannedEquipment);
      scannedEquipment.sequence=scannedEquipList.length;

      BlocProvider.of<AEIScanBloc>(context).add(AEIScanFetchDataWithLastSetCriteria());
      return true;
    }else{
      return false;
    }

  }


  void checkAndUpdateScannedCarWithExistingInfo(Equipment equipment1)   {
    print("starting addScannedCarToGrid2 method >>>>> ${DateTime.now().toIso8601String()}");
    String equipInitial=equipment1.equipmentInitial;
    String equipNumber=equipment1.equipmentNumber;
      AddRailCarRepo().getYardTrack(equipInitial, equipNumber).then((value) {
        print("Exist equipment :$value ${DateTime.now().toIso8601String()}");
      if (value!=null) {
        List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"];
        scannedEquipList.remove(equipment1);
        value.isInbounded=true;
        value.sequence=  equipment1.sequence;
        scannedEquipList.add(value);
      }
      (value??equipment1).isDataVerified=true;
      BlocProvider.of<AEIScanBloc>(context).add(AEIScanFetchDataWithLastSetCriteria());
      print("End addScannedCarToGrid2 method >>>>> ${DateTime.now().toIso8601String()}");
      return value;
    });


  }

  String getEquipmentInitial(String equipInitBits) {
    print("equipInitBits:$equipInitBits");

    // convert equipment initial binary value to decimal value
    String decimalString = int.parse(equipInitBits, radix: 2).toRadixString(
        10);
    print("decimalString:$decimalString");

    int value = int.parse(decimalString);
    print("value:$value");

    // Calculate N1 as a integer
    int N1 = (value / (27 * 27 * 27)).floor();
    print("N1:$N1");

    int N2 = ((value - (N1 * 27 * 27 * 27)) / (27 * 27)).floor();
    print("N2:$N2");

    int N3 = ((value - ((N1 * 27 * 27 * 27) + (N2 * 27 * 27))) / 27).floor();
    print("N3:$N3");

    int N4 = value - ((N1 * 27 * 27 * 27) + (N2 * 27 * 27) + (N3 * 27));
    print("N4:$N4");

    Map<int, String> v1Map = {
      0: "A", 1: "B", 2: "C", 3: "D", 4: "E", 5: "F", 6: "G", 7: "H", 8: "I", 9: "J", 10: "K", 11: "L", 12: "M", 13: "N",
      14: "O", 15: "P", 16: "Q", 17: "R", 18: "S", 19: "T", 20: "U", 21: "V", 22: "W", 23: "X", 24: "Y", 25: "Z"
    };
    Map<int, String> v2Map = {
      0: "", 1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "F", 7: "G", 8: "H", 9: "I", 10: "J", 11: "K", 12: "L", 13: "M",
      14: "N", 15: "O", 16: "P", 17: "Q", 18: "R", 19: "S", 20: "T", 21: "U", 22: "V", 23: "W", 24: "X", 25: "Y", 26: "Z"
    };

    String equipInitial = v1Map[N1] + v2Map[N2] + v2Map[N3] + v2Map[N4];

    print("equipInitial:$equipInitial");
    return equipInitial;
  }

  String getEquipmentNumber(String equipNumberBits) {
    print("equipNumberBits:$equipNumberBits");

    String equipmentNumber = int.parse(equipNumberBits, radix: 2)
        .toRadixString(10);
    print("equipmentNumber:$equipmentNumber");

    return equipmentNumber;
  }

  void _showToastError(String errorText) {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Colors.redAccent, // color: Color(0xff7fae1b),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.error),
          SizedBox(
            width: 12.0,
          ),
          Text(
            errorText,
            style: TextStyle(
              // ignore: deprecated_member_use
              color: Theme.of(context).textTheme.body1.color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  void _showToastSuccess(successText) {
    Widget toast = Container(
      width: ScreenUtil().setWidth(552),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Color(0xff7fae1b),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text(
            successText,
            style: TextStyle(
              // ignore: deprecated_member_use
              color: Theme.of(context).textTheme.body1.color,
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }
}
