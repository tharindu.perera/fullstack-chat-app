import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_data_table_souce.dart';

class AEIScanDataSource extends CustomDataTableSource {
  final List<Equipment> _inputList;
  List<Equipment> selectedEquipList =  MemoryData.dataMap["selectedEquipList"];
  int _selectedCount = 0;

  AEIScanDataSource(this._inputList);

  onSelectedRow(bool selected, Equipment selectedEquipment) async {
    List<Equipment> selectedEquipList2 =  MemoryData.dataMap["selectedEquipList"];
    selectedEquipList2.isNotEmpty?print(selectedEquipList2[0]):null;
    if (selectedEquipList.contains(selectedEquipment)) {
      notifyListeners();
      selectedEquipList.remove(selectedEquipment);
    } else {
      selectedEquipList.add(selectedEquipment);
    }
    print(">>>${selectedEquipList2.length}");
    notifyListeners();
  }

  @override
  CustomDataRow getRow(int index) {
    final cellVal = _inputList[index];
    return CustomDataRow.byIndex(
        index: index,
        selected: cellVal.isSelected,
        onSelectChanged: (bool value) {
          if (cellVal.isSelected != value) {
            cellVal.isSelected = value;
            notifyListeners();
          }
          onSelectedRow(value, cellVal);
        },
        cells: [
          CustomDataCell(Text(
            '${cellVal.sequence ?? ''}',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: ScreenUtil().setHeight(15),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal,
              letterSpacing: ScreenUtil().setWidth(1.75),
            ),
          )),
          CustomDataCell(Text(
            '${cellVal.equipmentInitial ?? ''}',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: ScreenUtil().setHeight(15),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal,
              letterSpacing: ScreenUtil().setWidth(1.75),
            ),
          )),
          CustomDataCell(Text(
            '${cellVal.equipmentNumber ?? ''}',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: ScreenUtil().setHeight(15),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal,
              letterSpacing: ScreenUtil().setWidth(1.75),
            ),
          )),
          CustomDataCell(cellVal.isDataVerified
              ? Text(
                  '${MemoryData.inMemoryYardMap[cellVal.yardId]?.yardName ?? cellVal.yardId ?? '-'} ',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: ScreenUtil().setHeight(15),
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                    letterSpacing: ScreenUtil().setWidth(1.75),
                  ),
                )
              : Text(
                  'Fetching ....  ',
                  style: TextStyle(
                    color: Colors.amberAccent,
                    fontFamily: 'Roboto',
                    fontSize: ScreenUtil().setHeight(15),
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                    letterSpacing: ScreenUtil().setWidth(1.75),
                  ),
                )),
          CustomDataCell(cellVal.isDataVerified
              ? Text(
                  '${MemoryData.inMemoryTrackMap[cellVal.trackId]?.trackName ?? cellVal.trackId ?? '-'} ',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: ScreenUtil().setHeight(15),
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                    letterSpacing: ScreenUtil().setWidth(1.75),
                  ),
                )
              : Text(
                  'Fetching ....  ',
                  style: TextStyle(
                    color: Colors.amberAccent,
                    fontFamily: 'Roboto',
                    fontSize: ScreenUtil().setHeight(15),
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                    letterSpacing: ScreenUtil().setWidth(1.75),
                  ),
                )),
        ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _inputList.length ?? 0;

  @override
  int get selectedRowCount => _selectedCount ?? 0;
}
