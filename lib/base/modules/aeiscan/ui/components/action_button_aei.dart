import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/dao/outage_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/outage_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/ui/assign_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/ui/assign_defect.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/ui/assign_switchlist.dart';
import 'package:mbase/base/modules/listview/actions/comment/ui/comment.dart';
import 'package:mbase/base/modules/listview/actions/inspect_railcar/ui/inspect_railcar.dart';
import 'package:mbase/base/modules/listview/actions/loadcar/ui/loadcar.dart';
import 'package:mbase/base/modules/listview/actions/move_railcar/ui/move_railcar.dart';
import 'package:mbase/base/modules/listview/actions/outbound_railcar/ui/outbound_railcar.dart';
import 'package:mbase/base/modules/listview/actions/test/ui/test.dart';
import 'package:mbase/base/modules/listview/actions/unloadcar/ui/unloadcar.dart';
import 'package:mbase/base/modules/listview/listview/ui/components/bottomsheet_item.dart';
import 'package:mbase/base/modules/load_vcf/repository/load_vcf_repository.dart';
import 'package:mbase/base/modules/load_vcf/ui/load_vcf_edit.dart';
import 'package:mbase/base/modules/seal/update_seal/ui/update_seal.dart';
import 'package:mbase/base/modules/unloadvcf/repository/unloadvcf_repository.dart';
import 'package:mbase/base/modules/unloadvcf/ui/unloadvcf_edit.dart';
import 'package:mbase/env.dart';
import 'package:provider/provider.dart';

class ActionButtonAEI extends StatefulWidget {
  @override
  _ActionButtonAEIState createState() => _ActionButtonAEIState();
}

class _ActionButtonAEIState extends State<ActionButtonAEI> {
  ThemeChanger _themeChanger;
  String selectedRailcar = null;
  FToast fToast;
  @override
  Widget build(BuildContext context) {
    fToast = FToast(context);
    _themeChanger = Provider.of<ThemeChanger>(context);
    return Container(
      width: ScreenUtil().setWidth(133),
      height: ScreenUtil().setHeight(50),
      child: FlatButton(
          onPressed: () {
            checkIfSelected();
          },
          child: Text(
            'ACTIONS',
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.25),
                color: Colors.white),
          ),
          color: Color(0xFF508be4),
          textColor: Colors.white,
          disabledColor: Colors.grey,
          disabledTextColor: Colors.black,
          splashColor: Color(0xFF508be4)),
    );
  }

  void checkIfSelected() {
     List<Equipment> list = MemoryData.dataMap["selectedEquipList"];

    if (list.isNotEmpty) {
      checkSelectedEquipInbounded(context, list);
    } else {
      _showToastError(["Please select railcar(s) to perform actions"]);
    }
  }

  void checkSelectedEquipInbounded(context123, List<Equipment>  selectedList) {
    List<String> errorList = new List<String>();
    bool isInbounded = true;
    selectedList.forEach((element) {
      if (element.placedDate==null   ) {
        isInbounded = false;
        errorList.add("Equipment '${element.equipmentInitial} ${element.equipmentNumber}' is not inbounded.");
      }
    });

     if (isInbounded) {
      showBottomDrawer(context123, selectedList);
    } else {
      _showToastError(errorList);
    }
  }

  void _showToastErrorList(List<String> errorList, context) {
    fToast = FToast(context);
    Widget toast = Container(
        width: ScreenUtil().setWidth(552),
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
        decoration: BoxDecoration(
          color: Colors.redAccent,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: errorList.map((e) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.warning),
                SizedBox(
                  width: 12.0,
                ),
                Expanded(
                  child: Text(
                    e,
                    style: TextStyle(
                      // ignore: deprecated_member_use
                      color: Theme.of(context).textTheme.body1.color,
                    ),
                  ),
                ),
              ],
            );
          }).toList(),
        ));

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }


  void showBottomDrawer(context123, List<Equipment> selectedList) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            alignment: Alignment.center,
            width: ScreenUtil().setWidth(1024),
            height: ScreenUtil().setHeight(505),
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  checkBottomItemWithUserProfile("List_View:Move_Railcar")
                      ? BottomSheetItem(
                      imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                          ? "assets/images/move-railcar-dark.png"
                          : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/move-railcar-light.png" : null,
                      title: 'MOVE RAILCAR(S)',
                      ontap: () {
                        if (selectedList.length > 0) {
                          Navigator.of(context).pop();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => MoveRailcar(selectedList, APP_CONST.AEI_SCAN,context:BlocProvider.of<AEIScanBloc>(context123) ),
                              ));
                        }
                      })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Inspect_Railcar")
                      ? BottomSheetItem(
                      imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                          ? "assets/images/inspection-dark.png"
                          : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/inspection-light.png" : null,
                      title: 'INSPECT RAILCAR',
                      ontap: () {
                        if (selectedList.length == 1) {
                          Navigator.of(context).pop();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => InspectRailCar(selectedList),
                              ));
                        }
                      })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Load_Railcar")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/outbound-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/outbound-light.png" : null,
                          title: 'LOAD RAILCAR(S)',
                          ontap: () {
                            if (selectedList.length > 0) {
                              List<String> errorList = [];
                              int counter = 0;
                              for (Equipment equipment in selectedList) {
                                if (counter++ > 5) {
                                  break;
                                }
                            if (equipment.compartmentList.isNotEmpty
                                && double.parse(equipment.compartmentList[0].currentAmount??"0") > 0) {
                                  errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber}");
                                }
                              }
                              if (errorList.isNotEmpty) {
                                _showToastError([]..add("Following railcars are already loaded")..addAll(errorList));
                              } else {
                                Navigator.of(context).pop();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => LoadRailCar(selectedList, APP_CONST.AEI_SCAN, context123, null, null, null),
                                    ));
                              }

                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Unload_Railcar")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/unload-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/unload-light.png" : null,
                          title: 'UNLOAD RAILCAR(S)',
                          ontap: () {
                            if (selectedList.length > 0) {
                              List<String> errorList = [];
                              int counter = 0;
                              for (Equipment equipment in selectedList) {
                                if (counter++ > 5) {
                                  break;
                                }
                                if (equipment.compartmentList.isEmpty || (equipment.compartmentList.isNotEmpty
                                    && double.parse(equipment.compartmentList[0].currentAmount ?? "0") <= 0)) {
                                  errorList.add("${equipment.equipmentInitial + " " + equipment.equipmentNumber}");
                                }
                              }
                              if (errorList.isNotEmpty) {
                                _showToastError([]..add("Following railcars are already empty")..addAll(errorList));
                              } else {
                                Navigator.of(context).pop();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => UnLoadRailCar(selectedList),
                                    ));
                              }
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Assign_To_Switchlist")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/switch-list-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/switch-list-light.png" : null,
                          title: 'ASSIGN TO SWITCH LIST',
                          ontap: () {
                            if (selectedList.length > 0) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<AEIScanBloc>(context123),
                                            child: AssignSwitchListPage(selectedList),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Assign_Defect")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/assign-defect-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/assign-defect-light.png" : null,
                          title: 'ASSIGN DEFECT',
                          ontap: () {
                            if (selectedList.length == 1) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<AEIScanBloc>(context123),
                                            child: AssignDefect(selectedList, APP_CONST.AEI_SCAN),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Assign_Block")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/assign-block-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/assign-block-light.png" : null,
                          title: 'ASSIGN BLOCK',
                          ontap: () {
                            if (selectedList.length == 1) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<AEIScanBloc>(context123),
                                            child: AssignBlock(selectedList, APP_CONST.AEI_SCAN),
                                          )));
                            }
                          })
                      : Container(),

                  checkBottomItemWithUserProfile("List_View:Leave_Comment")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/leave-comment-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/leave-comment-light.png" : null,
                          title: 'LEAVE A COMMENT',
                          ontap: () {
                            if (selectedList.length == 1) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<AEIScanBloc>(context123),
                                            child: LeaveComment(selectedList, APP_CONST.AEI_SCAN),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Seal")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/seal-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/seal-light.png" : null,
                          title: 'ADD SEAL',
                          ontap: () {
                            if (selectedList.length == 1) {
                              selectedRailcar = selectedList[0].equipmentInitial + " " + selectedList[0].equipmentNumber;
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<AEIScanBloc>(context123),
                                            child: UpdateSeal(selectedList[0]),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Outbound_Railcar")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/outbound-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/outbound-light.png" : null,
                          title: 'OUTBOUND RAILCAR(S)',
                          ontap: () {
                            if (selectedList.length > 0) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider.value(
                                            value: BlocProvider.of<AEIScanBloc>(context123),
                                            child: OutboundRailCar(selectedList, APP_CONST.AEI_SCAN),
                                          )));
                            }
                          })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Load_VCF")
                      ? BottomSheetItem(
                      imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                          ? "assets/images/LoadVCF-dark.png"
                          : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/LoadVCF-light.png" : null,
                      title: 'LOAD VCF',
                      ontap: () async {
                        if (selectedList.length > 1) {
                          _showToastErrorList([]..add("Please select only one car"), context123);
                        } else if (selectedList.length == 1) {
                          var equip = selectedList[0];
                          ProductModel productModel = ProductModel();
                          bool validProduct = true;
                          if (equip.compartmentList.isNotEmpty && equip.compartmentList[0]?.productId != null) {
                             productModel = await CommonDao().fetchProductById(equip.compartmentList[0].productId);
                            // if (productModel != null && productModel.dryOrLiquid != APP_CONST.LIQUID) {
                            //   validProduct = false;
                            // }
                          }
                          if (!validProduct) {
                            _showToastErrorList([]..add("Railcar does not have liquid product"), context123);
                            return;
                          }

                          AssetMasterDataModel assetMasterModel = await LoadVCFRepository().fetchAssetMasterData(equip.equipmentInitial, equip.equipmentNumber);
                          OutageModel  outtageModel = await OutageDao().fetchOutageById(equip.assetMasterId);
                          if (null != assetMasterModel && null != assetMasterModel.gallon_capacity && null != assetMasterModel.load_limit &&  null != outtageModel) {
                            selectedRailcar = equip.equipmentInitial + " " + equip.equipmentNumber;
                            Navigator.of(context).pop();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BlocProvider.value(
                                      value: BlocProvider.of<AEIScanBloc>(context123),
                                      child: ProcessLoadVCF(equip, assetMasterModel,productModel),
                                    )));
                          } else {
                            if (null == assetMasterModel) {
                              _showToastErrorList([]..add("Railcar does not have Asset Information"), context123);
                            } else if (assetMasterModel.gallon_capacity == null) {
                              _showToastErrorList([]..add("Railcar does not have Gallon Capacity"), context123);
                            } else if (assetMasterModel.load_limit == null) {
                              _showToastErrorList([]..add("Railcar does not have Load Limit"), context123);
                            }
                            else if(null == outtageModel){
                              _showToastErrorList([]..add("Railcar does not have Outage Details"), context123);
                            }
                          }
                        }
                      })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Unload_VCF")
                      ? BottomSheetItem(
                      imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                          ? "assets/images/UnloadVCF-dark.png"
                          : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/UnloadVCF-light.png" : null,
                      title: 'UNLOAD VCF',
                      ontap: () async {
                        if (selectedList.length > 1) {
                          _showToastErrorList([]..add("Please select only one car"), context123);
                        } else if (selectedList.length == 1) {
                          var equip = selectedList[0];
                          if (equip.compartmentList.isNotEmpty
                              && double.parse(equip.compartmentList[0].currentAmount ?? "0") > 0) {
                            ProductModel productModel = await CommonDao().fetchProductById(equip.compartmentList[0].productId);
                            if (productModel != null && productModel.dryOrLiquid == APP_CONST.LIQUID) {
                              AssetMasterDataModel assetMasterModel = await UnloadvcfRepository().fetchAssetMasterModel(equip.assetMasterId);
                              if (null != assetMasterModel && null != assetMasterModel.gallon_capacity) {
                                selectedRailcar = equip.equipmentInitial + " " + equip.equipmentNumber;
                                Navigator.of(context).pop();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => BlocProvider.value(
                                          value: BlocProvider.of<AEIScanBloc>(context123),
                                          child: EditUnloadVcf(equip, assetMasterModel),
                                        )));
                              } else {
                                _showToastErrorList([]..add("Railcar does not have Gallon Capacity"), context123);
                              }
                            } else {
                              _showToastErrorList([]..add("Railcar does not have liquid product"), context123);
                            }

                          } else {
                            _showToastErrorList([]..add("Please select only a loaded car"), context123);
                          }
                        }
                      })
                      : Container(),
                  checkBottomItemWithUserProfile("List_View:Test")
                      ? BottomSheetItem(
                          imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? "assets/images/outbound-dark.png"
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? "assets/images/outbound-light.png" : null,
                          title: 'TEST',
                          ontap: () {
                            Navigator.of(context).pop();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BlocProvider.value(
                                          value: BlocProvider.of<AEIScanBloc>(context123),
                                          child: Test(),
                                        )));
                          })
                      : Container(),
                ],
              ),
            ),
          );
        });
  }

  bool checkBottomItemWithUserProfile(String screenActionID) {
    return env.userProfile.screenActionIdList[env.userProfile.facilityId].contains(screenActionID);
  }



  void _showToastError(List<String> errorList) {
    Widget toast = Container(
        width: ScreenUtil().setWidth(552),
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
        decoration: BoxDecoration(
          color: Colors.redAccent,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: errorList.map((e) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.warning),
                SizedBox(
                  width: 12.0,
                ),
                Text(
                  e,
                  style: TextStyle(
                    // ignore: deprecated_member_use
                    color: Theme.of(context).textTheme.body1.color,
                  ),
                ),
              ],
            );
          }).toList(),
        ));

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }
}
