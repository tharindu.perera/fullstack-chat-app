import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/screen_factory.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/ui/inbound/inbound_railcar_screen.dart';

class InboundButton extends StatefulWidget {
  @override
  _InboundButtonState createState() => _InboundButtonState();
}

class _InboundButtonState extends State<InboundButton> {
  String selectedRailcar = null;
  FToast fToast;

  @override
  Widget build(BuildContext context) {
    fToast = FToast(context);

    return Container(
      width: ScreenUtil().setWidth(133),
      height: ScreenUtil().setHeight(50),
      child: FlatButton(
          onPressed: () {
            checkIfSelected();
          },
          child: Text(
            'INBOUND',
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: ScreenUtil().setHeight(14),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                letterSpacing: ScreenUtil().setWidth(1.25),
                color: Colors.white),
          ),
          color: Color(0xFF508be4),
          textColor: Colors.white,
          disabledColor: Colors.grey,
          disabledTextColor: Colors.black,
          splashColor: Color(0xFF508be4)),
    );
  }

  void checkIfSelected() {
    List<Equipment> list = MemoryData.dataMap["selectedEquipList"];

    if (list.isNotEmpty) {
      checkSelectedEquipInbounded(list);
    } else {
      _showToastError(["Please select railcar(s) to perform actions"]);
    }
  }

  void checkSelectedEquipInbounded(List<Equipment> selectedList) {
    List<String> errorList = new List<String>();
    bool isInbounded = false;
    selectedList.forEach((element) {
      if (element.placedDate !=null) {
        isInbounded = true;
        errorList.add(
            "Equipment '${element.equipmentInitial} ${element.equipmentNumber}' is already inbounded.");
      }
    });

    print("isInbounded:$isInbounded");
    if (!isInbounded) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => BlocProvider.value(
              value: BlocProvider.of<AEIScanBloc>(context),
              child: ScreenFactory(InboundCar(selectedList)),
            ),
          ));
    } else {
      _showToastError(errorList);
    }
  }

  void prepareNotSelectedError() {
    List<String> errorList = new List<String>();
    errorList.add("Please select railcar(s) to inbound");
    _showToastError(errorList);
  }

  void _showToastError(List<String> errorList) {
    Widget toast = Container(
        width: ScreenUtil().setWidth(552),
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
        decoration: BoxDecoration(
          color: Colors.redAccent,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: errorList.map((e) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.warning),
                SizedBox(
                  width: 12.0,
                ),
                Text(
                  e,
                  style: TextStyle(
                    // ignore: deprecated_member_use
                    color: Theme.of(context).textTheme.body1.color,
                  ),
                ),
              ],
            );
          }).toList(),
        ));

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }
}
