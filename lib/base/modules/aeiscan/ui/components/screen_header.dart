import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/screen_factory.dart';
import 'package:mbase/base/modules/aeiscan/ui/components/scan_button.dart';
import 'package:mbase/base/modules/aeiscan/ui/inbound/show_pop_func.dart';
import 'package:mbase/base/modules/appcards/ui/home.dart';

class ScreenHeaderAEI extends StatelessWidget {
  const ScreenHeaderAEI({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    void warnBeforeExit() {
      List<Equipment> scannedEquipList = MemoryData.dataMap["scannedEquipList"];
      if (scannedEquipList == null || scannedEquipList.length == 0) {
        //Navigator.of(context).pop();
        Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(Home())));
      } else {
        popUpDialog(context);
      }
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(20),
                  child: Container(
                      child: ListTile(
                        leading: GestureDetector(
                            onTap: () {
                              warnBeforeExit();
                            },
                            child: Icon(
                              Icons.arrow_back,
                            )),
                        title: Text('AEI Scan Railcars',
                            style: TextStyle(
                                fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                fontFamily: 'Roboto',
                                color: Theme.of(context)
                                    .textTheme
                                // ignore: deprecated_member_use
                                    .headline
                                    .color)),
                      ))),
            ],
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(20),
                    child: Container(
                        child: GestureDetector(
                          onTap: () {
                            warnBeforeExit();
                          },
                          child: RichText(
                            text: TextSpan(
                                style:
                                // ignore: deprecated_member_use
                                Theme.of(context).textTheme.body1,
                                children: <InlineSpan>[
                                  TextSpan(
                                      text: 'CANCEL',
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: ScreenUtil().setWidth(1.25),
                                          fontFamily: 'Roboto',
                                          color: Color(0xFF3e8aeb)))
                                ]),
                          ),
                        ))),
                Padding(
                    padding: EdgeInsets.only(left: 0, top: 14, right: 24, bottom: 14),
                    ),
                ScanButton(),
              ],
            )
          ],
        ),
      ],
    );
  }
}
