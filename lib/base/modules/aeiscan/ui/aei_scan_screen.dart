import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/card/card.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_state.dart';
import 'package:mbase/base/modules/aeiscan/repository/aei_scan_repository.dart';
import 'package:mbase/base/modules/aeiscan/ui/components/action_button_aei.dart';
import 'package:mbase/base/modules/aeiscan/ui/components/aei_scan_grid.dart';
import 'package:mbase/base/modules/aeiscan/ui/components/detail_section.dart';
import 'package:mbase/base/modules/aeiscan/ui/components/inbound_button.dart';
import 'package:mbase/base/modules/aeiscan/ui/components/screen_header.dart';

class AEIScanScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    MemoryData.dataMap["scannedEquipList"]=List<Equipment>();
    MemoryData.dataMap["selectedEquipList"]=List<Equipment>();

    return BlocProvider<AEIScanBloc>(
      create: (context) => AEIScanBloc(AEIScanRepository())..add(AEIScanInitialDataLoad()),
      child: BlocBuilder<AEIScanBloc, AEIScanState>(builder: (context, state) {
        return Scaffold(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24), vertical: ScreenUtil().setHeight(24)),
                    child: CardUI(
                      content: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                              child: Column(
                                children: <Widget>[
                                  ScreenHeaderAEI(),
                                  SizedBox(
                                    width: ScreenUtil().setWidth(928),
                                    child: Divider(thickness: ScreenUtil().setWidth(1)),
                                  ),
                                  SizedBox(height: ScreenUtil().setHeight(24)),
                                  Row(children: [DetailSection(),
                                    //ScanButton(),
                                    //SizedBox(width: ScreenUtil().setWidth(10)),
                                    ActionButtonAEI(),
                                    SizedBox(width: ScreenUtil().setWidth(10)),
                                    Expanded(child: InboundButton())]),
                                  (state is AEIScanInProgress) ? CircularProgressIndicator() : AEIScanGrid()
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
      }),
    );
  }
}
