import 'package:mbase/base/modules/aeiscan/ui/components/aei_scan_ds.dart';

abstract class AEIScanState {
  AEIScanDataSource dataSource = AEIScanDataSource([]);
  AEIScanState(this.dataSource);

  @override
  String toString() {
    return this.runtimeType.toString();
  }
}

class AEIScanInitState extends AEIScanState {
  AEIScanInitState({AEIScanDataSource dataSource})
      : super(dataSource ?? AEIScanDataSource([]));
}

class AEIScanInProgress extends AEIScanState {
  AEIScanInProgress({AEIScanDataSource dataSource})
      : super(dataSource ?? AEIScanDataSource([]));
}

class AEIScanDataLoaded extends AEIScanState {
  AEIScanDataLoaded({AEIScanDataSource dataSource})
      : super(dataSource ?? AEIScanDataSource([]));
}
