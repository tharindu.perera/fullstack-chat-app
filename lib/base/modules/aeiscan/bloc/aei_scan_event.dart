import 'package:flutter/material.dart';
import 'package:mbase/base/modules/aeiscan/ui/components/aei_scan_ds.dart';

@immutable
abstract class AEIScanEvent {
  AEIScanEvent();
}

class AEIScanInitialDataLoad extends AEIScanEvent {
  AEIScanInitialDataLoad() : super();
}

// ignore: must_be_immutable
class AEIScanFetchDataWithCriteria extends AEIScanEvent {
  Map<String, dynamic> criteria;
  AEIScanFetchDataWithCriteria(this.criteria ) : super( );
}

class AEIScanFetchDataWithLastSetCriteria extends AEIScanEvent {
  AEIScanFetchDataWithLastSetCriteria() : super();
}

// ignore: must_be_immutable
class AEIScanGridSortedClicked extends AEIScanEvent {
  var ascending;
  var columnIndex;

  AEIScanGridSortedClicked(this.columnIndex, this.ascending) : super();
}

// ignore: must_be_immutable
class AEIScanRefreshData extends AEIScanEvent {
  AEIScanDataSource ds;
  AEIScanRefreshData(this.ds) : super();
}




