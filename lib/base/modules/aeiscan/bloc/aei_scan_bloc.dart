import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_event.dart';
import 'package:mbase/base/modules/aeiscan/bloc/aei_scan_state.dart';
import 'package:mbase/base/modules/aeiscan/repository/aei_scan_repository.dart';
import 'package:mbase/base/modules/aeiscan/ui/components/aei_scan_ds.dart';

class AEIScanBloc extends Bloc<AEIScanEvent, AEIScanState> {
  AEIScanRepository _repository;
  Map<String, dynamic> _lastSearchCriteria;
  StreamSubscription streamSubscription;
  bool ascending = true;
  int sortColumnIndex = 0;

  AEIScanBloc(this._repository) : super(AEIScanInitState()) {}

  @override
  Stream<AEIScanState> mapEventToState(AEIScanEvent event) async* {
    if (event is AEIScanInitialDataLoad) {
      yield* _mapListViewFetchDataWithCriteria(null);
    }

    if (event is AEIScanFetchDataWithCriteria) {
      this._lastSearchCriteria = event.criteria;
      yield* _mapListViewFetchDataWithCriteria(event.criteria);
    }

    if (event is AEIScanFetchDataWithLastSetCriteria) {
      yield* _mapListViewFetchDataWithCriteria(this._lastSearchCriteria);
    }

    if (event is AEIScanGridSortedClicked) {
      yield* _mapListViewFetchDataWithCriteria(this._lastSearchCriteria, ascending: event.ascending, sortColumnIndex: event.columnIndex);
    }

    if (event is AEIScanRefreshData) {
      yield AEIScanDataLoaded(dataSource: event.ds);
    }
  }

  Stream<AEIScanState> _mapListViewFetchDataWithCriteria(criteria, {ascending: true, sortColumnIndex: 0}) async* {
    this.ascending = ascending;
    this.sortColumnIndex = sortColumnIndex;
    yield AEIScanInProgress();
    streamSubscription?.cancel();
    streamSubscription = _repository.getRecordByCriteria(ascending, sortColumnIndex).listen((event) async {
      add(AEIScanRefreshData(AEIScanDataSource(event)));
    });
  }
}
