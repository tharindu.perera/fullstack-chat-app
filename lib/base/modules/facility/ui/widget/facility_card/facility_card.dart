import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FacilityCard extends StatelessWidget {
  FacilityCard(
      {this.imagePath,
      this.subtitleText,
      this.titleText,
      this.trailingIcon,
      this.ontap});

  final String imagePath;
  final String titleText;
  final String subtitleText;
  final IconData trailingIcon;
  final Function ontap;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);

    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).orientation == Orientation.portrait
              ? ScreenUtil().setWidth(25)
              : ScreenUtil().setWidth(25),
          vertical: MediaQuery.of(context).orientation == Orientation.portrait
              ? ScreenUtil().setHeight(29.5)
              : ScreenUtil().setHeight(29.5)),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minWidth: MediaQuery.of(context).orientation == Orientation.portrait
              ? ScreenUtil().setWidth(317)
              : ScreenUtil().setWidth(200),
        ),
        child: Container(
          color: Theme.of(context).canvasColor,
          child: GestureDetector(
            onTap: ontap,
            child: ListTile(
              leading: Image.asset(
                imagePath,
                width: ScreenUtil().setWidth(35),
                height: ScreenUtil().setHeight(36),
              ),
              title: Text(titleText,
                  style: TextStyle(
                      fontSize:
                          ScreenUtil().setSp(16, allowFontScalingSelf: true),
                      fontFamily: 'Roboto',
                      color: Theme.of(context).textTheme.body1.color,
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      letterSpacing: ScreenUtil().setWidth(0.5))),
              subtitle: Text(subtitleText,
                  style: TextStyle(
                      fontSize:
                          ScreenUtil().setSp(12, allowFontScalingSelf: true),
                      fontFamily: 'Roboto',
                      letterSpacing: ScreenUtil().setWidth(0.4),
                      fontWeight: FontWeight.normal,
                      color: Theme.of(context).textTheme.headline.color)),
              trailing: Icon(
                trailingIcon,
                size: ScreenUtil().setWidth(24),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
