import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:mbase/base/core/common_base/model/facility_model.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/repositories/facility_repository.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/screen_factory.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/appcards/ui/home.dart';
import 'package:mbase/base/modules/facility/ui/widget/facility_card/facility_card.dart';
import 'package:mbase/env.dart';
import 'package:provider/provider.dart';

class Facility extends StatefulWidget {
  bool fromLoging = false;

  Facility(this.fromLoging);

  @override
  _Facility createState() => _Facility();
}

class _Facility extends State<Facility> {
  final String darkimage = "assets/images/pin-point-dark.png";
  final String lightimage = "assets/images/pin-point-light.png";
  List<FacilityModel> facilityList = [];
  String initialState = "LOADING";
  ThemeChanger _themeChanger;

  @override
  void initState() {
    super.initState();
    FacilityRepository().fetchFacilityList(env.userProfile.facilityIdList).then((value) async{
      facilityList = value;
      initialState = facilityList.isEmpty ? "EMPTY" : "LOADED";
      if (facilityList.isNotEmpty && facilityList.length == 1) {
       await MemoryData.initializeData(facilityList[0]);
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => ScreenFactory(Home()),
            ));
      } else if (facilityList.isNotEmpty && env.userProfile.default_facility != null && widget.fromLoging) {
        await   MemoryData.initializeData(facilityList.singleWhere((element) => element.id == env.userProfile.default_facility));
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => ScreenFactory(Home()),
            ));
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);
    ScreenUtil.init(context, width: 1024, height: 768);
    return Scaffold(
        appBar: MainAppBar(),
        body: Container(
          child: Center(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Flex(
                  direction: Axis.vertical,
                  children: <Widget>[
                    SizedBox(height:  ScreenUtil().setHeight(144.5)),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: ScreenUtil().setWidth(180),
                          minHeight:  ScreenUtil().setHeight(28)),
                      child: AutoSizeText('Facility Selection', style: Theme.of(context).textTheme.headline5),
                    ),
                    SizedBox(
                      height:  ScreenUtil().setHeight(78),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(182)),
                      child: Builder(
                        builder: (context) {
                          if (initialState == "LOADING") {
                            return CircularProgressIndicator();
                          } else if (initialState == "EMPTY") {
                            return Text("No Facility Configured");
                          } else {
                            return  showFacilities();
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),

        )

    );
  }

  Widget showFacilities() {
    return GridView.count(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      crossAxisCount: 2,
      childAspectRatio: (3 / 1),
      children: facilityList
          .map((facility) => FacilityCard(
                imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? darkimage : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? lightimage : null,
                titleText: facility.facilityName,
                subtitleText: '',
                trailingIcon: Icons.keyboard_arrow_right,
                ontap: () async {
                  setState(() {
                    this.initialState = "LOADING";
                  });
//                  Loader.show(context);
                  await MemoryData.initializeData(facility);
//                  Loader.hide();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => ScreenFactory(Home()),
                      ));
                },
              ))
          .toList(),
    );
  }
}
