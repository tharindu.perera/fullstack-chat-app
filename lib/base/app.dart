import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/modules/facility/ui/facility.dart';

class App extends StatefulWidget {
  static num headLine5Font = 24.0;
  static num headLine6Font = 20.0;
  static num bodyText1Font = 16.0;
  static num bodyText2Font = 14.0;
  static num captionFont = 12.0;

  @override
  _AppState createState() => _AppState();

  App({Key key}) : super(key: key);
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);
    App.headLine5Font = ScreenUtil().setSp(24, allowFontScalingSelf: true);
    App.headLine6Font = ScreenUtil().setSp(20, allowFontScalingSelf: true);
    App.bodyText1Font = ScreenUtil().setSp(16, allowFontScalingSelf: true);
    App.bodyText2Font = ScreenUtil().setSp(14, allowFontScalingSelf: true);
    App.captionFont = ScreenUtil().setSp(12, allowFontScalingSelf: true);

    return Facility(true);
  }
}
