import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class NavigationState extends Equatable {
  final String RouteName;

  const NavigationState({@required this.RouteName});

  @override
  List<Object> get props => [];
}

class AppInitializingState extends NavigationState {}

class HomeState extends NavigationState {}

class HomePage2State extends NavigationState {}

class ListViewState extends NavigationState {}

class InspectQuestState extends NavigationState {}

class RailCarListViewState extends NavigationState {}

class SwitchListState extends NavigationState {}

class InspectionState extends NavigationState {}

class AddSealState extends NavigationState {}
