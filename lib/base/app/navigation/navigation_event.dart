import 'package:equatable/equatable.dart';

abstract class NavigationEvent extends Equatable {
  const NavigationEvent();

  @override
  List<Object> get props => [];
}

class AppInitializingEvent extends NavigationEvent {}

class LoginPageEvent extends NavigationEvent {}

class HomePageEvent extends NavigationEvent {}

class Home2PageEvent extends NavigationEvent {}

class ListViewEvent extends NavigationEvent {}

class InspectQuestEvent extends NavigationEvent {}

class RailCarListViewEvent extends NavigationEvent {}

class SwitchListEvent extends NavigationEvent {}

class InspectionEvent extends NavigationEvent {}

class AddSealEvent extends NavigationEvent {}
