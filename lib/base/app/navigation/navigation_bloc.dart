import 'dart:async';

import 'package:mbase/base/app/navigation/navigation_event.dart';
import 'package:mbase/base/app/navigation/navigation_state.dart';
import 'package:mbase/base/core/common_base/bloc/scy_bloc.dart';

class NavigationBloc extends SCYBloC<NavigationEvent, NavigationState> {
  NavigationBloc(NavigationState initialState) : super(initialState);

  NavigationState get initialState {
    return HomeState();
  }

  @override
  void listeningToGlobal(state) {}

  @override
  Stream<NavigationState> mapEventToState(NavigationEvent event) async* {
    if (event is AppInitializingEvent) {
      yield AppInitializingState();
    }
    if (event is HomePageEvent) {
      yield HomeState();
    }
    if (event is Home2PageEvent) {
      yield HomePage2State();
    }

    if (event is RailCarListViewEvent) {
      yield RailCarListViewState();
    }

    if (event is SwitchListEvent) {
      yield SwitchListState();
    }

    if (event is InspectionEvent) {
      yield InspectionState();
    }
    if (event is AddSealEvent) {
      yield AddSealState();
    }
  }
}
