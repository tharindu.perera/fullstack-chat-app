import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:mbase/base/app/authentication/bloc/authentication_bloc.dart';
import 'package:mbase/base/app/authentication/bloc/authentication_event.dart';
import 'package:mbase/base/app/login/bloc/login_events.dart';
import 'package:mbase/base/app/login/bloc/login_states.dart';
import 'package:mbase/base/core/common_base/repositories/user_repository.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/core/keys/global-keys.dart';
import 'package:mbase/env.dart';
import 'package:password_hash/password_hash.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc(LoginState initialState) : super(initialState);

  LoginState get initialState => LoginInitial();

  UserRepository userRepository = new UserRepository();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();
      onLoginButtonPressed(event.username, event.password, event.context);
    }
  }

  onLoginButtonPressed(String userName, String password, BuildContext context) async {
    print(">>>>keyClokUrl ${env.baseUrl}");
    StreamSubscription listen;
    try {
      keyGlobals.loginPageScaffoldKey.currentState.hideCurrentSnackBar();
      keyGlobals.loginPageScaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          "Logging In .....",
          textAlign: TextAlign.center,
        ),
        duration: Duration(seconds: 100),
        backgroundColor: Colors.blue,
      ));

      print("start onLoginButtonPressed() in LoginBloc, username:$userName");

      if ((await DataConnectionChecker().connectionStatus) == DataConnectionStatus.connected) {
        print("APP has data connection");
        FirebaseAuth firebaseAuth = FirebaseAuth.instance;
//        UserCredential userCredential = await firebaseAuth.signInWithEmailAndPassword(email: userName, password: password);
        UserCredential userCredential;
        var body = jsonEncode({"username": userName, "password": password});
        http.Response res = await http.post(env.baseUrl, headers: {"Content-Type": "application/json"}, body: body);
        if (res.statusCode == 200) {
          var body = jsonDecode(res.body);
          userCredential = await firebaseAuth.signInWithCustomToken(body["access_token"]);
        } else {
          throw Exception("Invalid Login. Please enter a Valid Username and Password");
        }
        final User firebaseUser = userCredential.user;
        print(" firebase  user found : $firebaseUser");
        String uid = firebaseUser.uid;
        print("getting user profile doc from firestore");
        env.userProfile = await userRepository.getUserProfileByFireBaseUID(uid);
        print("UserProfile doc = ${env.userProfile} ");
        PBKDF2 generator = new PBKDF2();
        String salt = Salt.generateAsBase64String(5);
        List<int> digest = generator.generateKey(password, salt, 1000, 32);
        Map<String, dynamic> map = Map();
        map["uid"] = uid;
        map["salt"] = salt;
        map["digest"] = digest.toString();
        await (await SharedPreferences.getInstance()).setString(userName, json.encode(map));
        BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn(userName));
      } else {
        print("APP has no data connection");
        var previouslySavedUserCredinfo = (await SharedPreferences.getInstance()).getString(userName);
        if (previouslySavedUserCredinfo == null) {
          throw Exception(
              "User has not logged with data connection( at least one time).Please try to log in with data connection"); // user name key not found in sharedfrefeences . May be user previously not logged or logic error
        }
        Map valueMap = json.decode(previouslySavedUserCredinfo);
        String uid = valueMap["uid"];
        String salt = valueMap["salt"];
        String digestSaved = valueMap["digest"];
        var generator = new PBKDF2();
        List<int> digest = generator.generateKey(password, salt, 1000, 32);
        if (digestSaved == digest.toString()) {
          print("getting user profile doc from local cache");
          await DatabaseProvider.firestore.disableNetwork();
          env.userProfile = await userRepository.getUserProfileByFireBaseUID(uid);
          print(">>>> env.userProfile doc = ${env.userProfile} ");
          BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn(userName));
          listen = env.dataConnectionChecker.onStatusChange.listen((event) async {
            if (DataConnectionStatus.connected == event) {
              FirebaseAuth firebaseAuth = FirebaseAuth.instance;
//        UserCredential userCredential = await firebaseAuth.signInWithEmailAndPassword(email: userName, password: password);
              UserCredential userCredential;
              var body = jsonEncode({"username": userName, "password": password});
              http.Response res = await http.post(env.baseUrl, headers: {"Content-Type": "application/json"}, body: body);
              if (res.statusCode == 200) {
                var body = jsonDecode(res.body);
                userCredential = await firebaseAuth.signInWithCustomToken(body["access_token"]);
              } else {
                throw Exception("Incorrect user name or password");
              }
              final User firebaseUser = await userCredential.user;
              await DatabaseProvider.firestore.enableNetwork();
              listen?.cancel();
            }
          });
        } else {
          throw Exception("Incorrect user name or password");
        }
      }

      keyGlobals.loginPageScaffoldKey.currentState?.hideCurrentSnackBar();
    } catch (error) {
      print("Login Error: $error");
      keyGlobals.loginPageScaffoldKey.currentState?.hideCurrentSnackBar();
      keyGlobals.loginPageScaffoldKey.currentState?.showSnackBar(SnackBar(
        content: Text("${error.toString().replaceAll("Exception:", "")}",
          textAlign: TextAlign.center,
        ),
        duration: Duration(seconds: 5),
        backgroundColor: Colors.red,
      ));
    }
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text("Username/Password entered is invalid."),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
