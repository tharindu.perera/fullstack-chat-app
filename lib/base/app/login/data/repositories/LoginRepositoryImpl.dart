import 'package:mbase/base/app/login/data/data_source/local_data_provider.dart';
import 'package:mbase/base/app/login/domain/repositories/login_repo.dart';

class LoginRepositoryImpl extends LoginRepository {
  final LoginLocalDataProvider localStorage;

  LoginRepositoryImpl(this.localStorage);

  @override
  Future<bool> hasToken() async {
    return localStorage.hasToken();
  }

  @override
  void persistToken(String token) {
    localStorage.persistToken(token);
  }

  @override
  void deleteToken() {
    localStorage.deleteToken();
  }
}
