import 'dart:async';

import 'package:mbase/base/app/login/data/data_source/local_data_provider.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefLoginLocalDataProviderImpl implements LoginLocalDataProvider {
  final String SESSION_KEY_NAME = "SESSION_KEY_NAME";

  final SharedPreferences preferences;

  SharedPrefLoginLocalDataProviderImpl({@required this.preferences});

  @override
  Future<bool> hasToken() async {
    var string = preferences.getString(SESSION_KEY_NAME);
    return string != null ? true : false;
  }

  @override
  void persistToken(String token) {
    preferences.setString(SESSION_KEY_NAME, token);
  }

  @override
  void deleteToken() {
    preferences.remove(SESSION_KEY_NAME);
  }
}
