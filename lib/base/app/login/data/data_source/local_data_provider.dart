abstract class LoginLocalDataProvider {
  Future<bool> hasToken();

  void persistToken(String token);

  void deleteToken();
}
