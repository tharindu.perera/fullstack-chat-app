import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/app/login/bloc/login_bloc.dart';
import 'package:mbase/base/app/login/bloc/login_events.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/keys/global-keys.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class LoginPage extends StatelessWidget {
  //Bloc bloc;
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);
    final theme = Provider.of<ThemeChanger>(context);
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    return MaterialApp(
        theme: theme.getTheme(),
        home: Scaffold(
          key: keyGlobals.loginPageScaffoldKey,
          body: Container(
            child: Form(
              key: _formKey,
              autovalidate: true,
              child: SingleChildScrollView(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: ScreenUtil().setHeight(128),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Image(
                          fit: BoxFit.contain,
                          width: ScreenUtil().setWidth(340),
                          height: ScreenUtil().setHeight(116),
                          image: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                              ? AssetImage('assets/images/login-kaleris-dark.png')
                              : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? AssetImage('assets/images/login-kaleris-light.png') : null,
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(40),
                      ),
                      ConstrainedBox(
                          constraints: BoxConstraints(
                            minWidth: ScreenUtil().setWidth(130),
                            minHeight: ScreenUtil().setHeight(2),
                          ),
                          child: Text(
                            'Get Started!',
                            style: TextStyle(
                                color: Theme.of(context).textTheme.bodyText1.color, fontFamily: 'Roboto', fontWeight: FontWeight.bold, fontStyle: FontStyle.normal, fontSize: ScreenUtil().setSp(24, allowFontScalingSelf: true)),
                          )),
                      SizedBox(
                        height: ScreenUtil().setSp(32, allowFontScalingSelf: true),
                      ),
                      ConstrainedBox(
                         constraints: BoxConstraints(
                          maxWidth: ScreenUtil().setWidth(340),
                        ),
                        child: TextFormField(
                          validator: (String arg) {
                            if (arg.isEmpty)
                              return 'Please enter a valid username';
                            else
                              return null;
                          },
                          controller: _usernameController,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(50),
                          ],
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true)),
                          decoration: InputDecoration(
                              errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                              filled: true,
                              fillColor: Color(0xfff7f7f7),
                              labelText: 'Username',
                              labelStyle: TextStyle(
                                  fontFamily: 'Roboto',
                                  color: Colors.black,
                                  fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                  letterSpacing: ScreenUtil().setWidth(0.25),
                                  fontWeight: FontWeight.normal,
                                  fontStyle: FontStyle.normal)),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setSp(32, allowFontScalingSelf: true),
                      ),
                      ConstrainedBox(
                        constraints: BoxConstraints(
                          maxWidth: ScreenUtil().setWidth(340),
                        ),
                        child: TextFormField(
                          validator: (String arg) {
                            if (arg.isEmpty)
                              return 'Please enter a valid password';
                            else
                              return null;
                          },
                          keyboardType: TextInputType.visiblePassword,
                          controller: _passwordController,
                          style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true)),
                          obscureText: true,
                          decoration: InputDecoration(
                            errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                            filled: true,
                            fillColor: Color(0xfff7f7f7),
                            labelText: 'Password',
                            labelStyle: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Roboto',
                                fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                letterSpacing: ScreenUtil().setWidth(0.25),
                                fontWeight: FontWeight.normal,
                                fontStyle: FontStyle.normal),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(32)),
                      SizedBox(
                        width: ScreenUtil().setWidth(340),
                        height: ScreenUtil().setWidth(48),
                        //height: ScreenUtil().setWidth(48),
                        child: SizedBox(
                          width: 100,
                          height: 50,
                          child: RaisedButton(

                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(1.0),
                            ),
                            onPressed: () => {
                           _formKey.currentState.validate()? BlocProvider.of<LoginBloc>(context).add(
                            LoginButtonPressed(username: _usernameController.text, password: _passwordController.text, context: context),
                            ):null
                            },
                            child: FittedBox(child:
                            Text(
                              'SIGN IN',
                              style: TextStyle(color: Colors.white, fontFamily: 'Roboto', fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true)),
                            )),
                            //icon: Icon(Icons.train, color: Colors.white),
                            color: Colors.blue,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
