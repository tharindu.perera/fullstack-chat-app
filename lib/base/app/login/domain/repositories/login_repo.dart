abstract class LoginRepository {
  Future<bool> hasToken();

  void persistToken(String token);

  void deleteToken();
}
