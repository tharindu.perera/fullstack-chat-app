import 'package:mbase/base/app/authentication/bloc/authentication_bloc.dart';
import 'package:mbase/base/app/authentication/data/data_source/local_data_provider.dart';
import 'package:mbase/base/app/authentication/data/data_source/shared_local_data_provider.dart';
import 'package:mbase/base/app/authentication/data/repositories/AuthenticationRepositoryImpl.dart';
import 'package:mbase/base/app/authentication/domain/repositories/authentication_repository.dart';
import 'package:mbase/base/app/authentication/domain/service/authentication_service.dart';
import 'package:mbase/base/injection_container.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'authentication/bloc/authentication_bloc.dart';
import 'authentication/bloc/authentication_state.dart';

Future<void> init() async {
  final sharedPreferences = await SharedPreferences.getInstance();
  final AuthenticationLoading authenticationLoading = AuthenticationLoading();

  sl.registerFactory(() => AuthenticationBloc(sl(), authenticationLoading));

  // Use cases
  sl.registerLazySingleton(() => AuthenticationService(sl()));
  // Repository
  sl.registerLazySingleton<AuthRepository>(() => AuthRepositoryImpl(sl()));
  sl.registerLazySingleton<AuthenticationLocalDataProvider>(() =>
      SharedPreferencesLocalDataProviderImpl(preferences: sharedPreferences));
}
