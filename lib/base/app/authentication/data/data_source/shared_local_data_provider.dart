import 'dart:async';

import 'package:mbase/base/app/authentication/data/data_source/local_data_provider.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesLocalDataProviderImpl
    implements AuthenticationLocalDataProvider {
  final String SESSION_KEY_NAME = "SESSION_KEY_NAME";

  final SharedPreferences preferences;

  SharedPreferencesLocalDataProviderImpl({@required this.preferences});

  @override
  Future<String> hasToken() async {
    var string = preferences.getString(SESSION_KEY_NAME);
    return string ;
  }

  @override
  void persistToken(name) {
    preferences.setString(SESSION_KEY_NAME, name);
  }

  @override
  void deleteToken() {
    preferences.remove(SESSION_KEY_NAME);
  }
}
