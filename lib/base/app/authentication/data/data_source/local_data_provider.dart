abstract class AuthenticationLocalDataProvider {
  Future<String> hasToken();

  void persistToken(string);

  void deleteToken();
}
