import 'package:mbase/base/app/authentication/data/data_source/local_data_provider.dart';
import 'package:mbase/base/app/authentication/domain/repositories/authentication_repository.dart';

class AuthRepositoryImpl extends AuthRepository {
  final AuthenticationLocalDataProvider localStorage;

  AuthRepositoryImpl(this.localStorage);

  @override
  Future<String> hasToken() async {
    return localStorage.hasToken();
  }

  @override
  void persistToken(string) {
    localStorage.persistToken(string);
  }

  @override
  void deleteToken() {
    localStorage.deleteToken();
  }
}
