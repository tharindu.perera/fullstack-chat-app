abstract class AuthRepository {
  Future<String> hasToken();

  void persistToken(string);

  void deleteToken();
}
