import 'package:mbase/base/app/authentication/domain/repositories/authentication_repository.dart';

class AuthenticationService {
  AuthRepository _repository;

  AuthenticationService(this._repository) : super();

  Future<String> hasToken() {
    return _repository.hasToken();
  }

  persistToken(string) {
    _repository.persistToken(string);
  }

  deleteToken() {
    _repository.deleteToken();
  }
}

