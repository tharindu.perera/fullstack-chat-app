import 'dart:async';
import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:mbase/base/app/authentication/bloc/authentication_event.dart';
import 'package:mbase/base/app/authentication/bloc/authentication_state.dart';
import 'package:mbase/base/app/authentication/domain/service/authentication_service.dart';
import 'package:mbase/base/core/common_base/bloc/scy_bloc.dart';
import 'package:mbase/base/core/common_base/repositories/user_repository.dart';
import 'package:mbase/env.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenticationBloc extends SCYBloC<AuthenticationEvent, AuthenticationState> {
  final AuthenticationService authenticationService;
  final AuthenticationLoading authenticationLoading;

  AuthenticationBloc(this.authenticationService, this.authenticationLoading) : super(authenticationLoading) {
    Future(() async {
      final String token = await authenticationService.hasToken(); //this is the username
      print("openinig app. checking saved authenticated  token (username). token(username) value ?  --> $token");
      var username = token;
      if (username != null && username.isNotEmpty) {
        print("user has alreday logged in");
        var shardValue = await (await SharedPreferences.getInstance()).getString(token);
        if (shardValue == null) {
          // user name key not found in sharedfrefeences . May be user previously not logged or logic error
          print("shardValue is null. user name key not found in sharedfrefeences . May be user previously has not logged in or logic error");
          add(HasNotLogged());
        } else {
          Map valueMap = json.decode(shardValue);
          String uid = valueMap["uid"];
          print("uid : $uid");
          print("getting user profile doc from local cache");
          env.userProfile = await UserRepository().getUserProfileByFireBaseUID(uid);
          print("firestore userProfile doc = ${env.userProfile} ");
          add(HasLogged());
        }
      } else {
        print("user is redirected to login page ");
        add(HasNotLogged());
      }
    });
  }

  @override
  Stream<AuthenticationState> mapEventToState(AuthenticationEvent event) async* {
    if (event is HasLogged) {
      yield AuthenticationAuthenticated();
    }
    if (event is HasNotLogged) {
      yield AuthenticationUnauthenticated();
    }
    if (event is LoggedIn) {
      yield AuthenticationLoading();
      await authenticationService.persistToken(event.uid);
      yield AuthenticationAuthenticated();
    }
    if (event is LoggedOut) {
      yield AuthenticationLoading();
      await authenticationService.deleteToken();
      print("sign out using FirebaseAuth");
      FirebaseAuth firebaseAuth = FirebaseAuth.instance;
      await firebaseAuth.signOut();
      await authenticationService.deleteToken();
      print("logged out");
      yield AuthenticationUnauthenticated();
    }
  }

//  @override
//  void listeningToGlobal(state) {
//    if (state.bloc is AppBarBloc) {
//      if ((state.transition.event.page == "logout")) {
//        add(LoggedOut());
//      }
//    }
//  }
}
