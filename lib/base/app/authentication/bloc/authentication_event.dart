import 'package:equatable/equatable.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class LoggedOut extends AuthenticationEvent {}

class HasNotLogged extends AuthenticationEvent {}

class HasLogged extends AuthenticationEvent {}

class LoggedIn extends AuthenticationEvent {
  final String uid;

  const LoggedIn(this.uid);
}
