//PLEASE ADD ALL GLOBAL CONSTANTS HERE
//SO THAT DEVELOPERS CAN SEE THEM IN ONE PLACE

class APP_CONST {
  static const String InspectionImagePath = "Pictures/scy";

  //DataBase operation types
  static const String ADD_CAR_DB_OPERATION = "ADD_EQUIPMENT";
  static const String OUTBOUND_OPERATION = "OUTBOUND_OPERATION";
  static const String ADD_COMMENT_OPERATION = "ADD_COMMENT";
  static const String ASSIGN_BLOCK_OPERATION = "ASSIGN_BLOCK";
  static const String ASSIGN_DEFECT_OPERATION = "ADD_DEFECT";
  static const String LOAD_CAR_OPERATION = "LOAD_CAR_OPERATION";
  static const String LOAD_EQUIPMENT_MASTER_REC_UPDATE =
      "LOAD_EQUIPMENT_MASTER_REC_UPDATE";
  static const String UNLOAD_EQUIPMENT_MASTER_REC_UPDATE =
      "UNLOAD_EQUIPMENT_MASTER_REC_UPDATE";
  static const String UNLOAD_CAR_OPERATION = "UNLOAD_CAR_OPERATION";
  static const String SWITCHLIST_EQUIPMENT = "SWITCHLIST_EQUIPMENT";
  static const String SWITCHLIST_HEADER = "SWITCHLIST_HEADER";
  static const String ADD_SEAL = "ADD_SEAL";
  static const String INSPECTION = "INSPECTION";
  static const String LOAD_VCF = "LOAD_VCF";
  static const String UNLOAD_VCF = "UNLOAD_VCF";
  static const String SWITCHLIST_OPERATION = "SWITCHLIST";
  static const String SWITCHLIST_START_OPERATION = "START_SWITCHLIST";
  static const String OFFLINE = "OFFLINE";

  //Switch list document status on start / edit / complete
  static const String SWL_START = "SWL_START";
  static const String SW_UPDATE_RAILCAR = "UPDATE_RAILCAR";
  static const String SW_RAILCAR_COMPLETE = "RAILCAR_COMPLETE";

  static const String RAILCAR_COMPLETE = "COMPLETE_RAILCAR";
  static const String RAILCAR_UPDATE = "EDIT_RAILCAR";

  static const String MOVE_RAILCAR = "MOVE_RAILCAR";

  //Transaction StatusTransitionWidget
  static const String TRANSACTION_STATUS_PENDING = "PENDING";
  static const String TRANSACTION_STATUS_ACCEPTED = "ACCEPTED";
  static const String TRANSACTION_STATUS_REJECTED = "REJECTED";
  static const String TRANSACTION_STATUS_ERROR = "ERROR";
  static const String TRANSACTION_STATUS_APPLICATION_ERROR =
      "APPLICATION_ERROR";

  //Inspection Status
  static const String INSPECTION_STATUS_SAVED = "SAVED";
  static const String INSPECTION_STATUS_COMPLETED = "COMPLETED";
  static const String INSPECTION_STATUS_FAILED = "FAILED";

  //ListView actions
  static const String ASSIGN_RAILCAR = "ASSIGN_RAILCAR";
  static const String SWITCHLIST_ASSIGNED_RAILCAR = "ADD_RAILCAR";

  //Switch List Equipment Status
  static const String SWITCHLIST_EQUIPMENT_STATUS_NOT_STARTED = "NOT STARTED";
  static const String SWITCHLIST_EQUIPMENT_STATUS_COMPLETED = "COMPLETED";
  static const String SWITCHLIST_EQUIPMENT_STATUS_IN_PROGRESS = "IN PROGRESS";
  static const String SWITCH_LIST_EQUIPMENT_STATUS_OUTBOUNDED = "OUTBOUNDED";
  static const String SWITCHLIST_EQUIPMENT_STATUS_NEW = "NEW";
  static const String SWITCHLIST_EDIT_RAILCAR = "EDIT_RAILCAR";
  static const String SWITCHLIST_COMPLETE_RAILCAR = "COMPLETE_RAILCAR";

  //DATA SOURCE
  static const String MOBILITY = "MOBILITY";
  static const String SCY = "SCY";

  //Visibility
  static const String PUBLIC = "PUBLIC";

  //Action source
  static const String LIST_VIEW = "LIST_VIEW";
  static const String YARD_VIEW = "YARD_VIEW";
  static const String AEI_SCAN = "AEI_SCAN";

  //Temperature scales
  static const String F = "1";
  static const String C = "2";

  //product dry or liquid
  static const String LIQUID = "LIQUID";

  //ListView Search Filter
  static const String ListView_Filter = "ListView_Filter";
  static const String Filter_By_Product = "Filter_By_Product";
  static const String Filter_By_Block_To = "Filter_By_Block_To";
  static const String Filter_By_Inspection = "Filter_By_Inspection";
  static const String Filter_By_InboundShipper = "Filter_By_InboundShipper";
  static const String Filter_By_InboundConsignee = "Filter_By_InboundConsignee";
  static const String Filter_By_OutboundShipper = "Filter_By_OutboundShipper";
  static const String Filter_By_OutboundConsignee = "Filter_By_OutboundConsignee";
  static const String Filter_By_EquipmentGroup = "Filter_By_EquipmentGroup";
  static const String Filter_By_Defective = "Filter_By_Defective";
  static const String Filter_By_HasBOL = "Filter_By_HasBOL";
  static const String Filter_By_HasComments = "Filter_By_HasComments";
  static const String Filter_By_Hazardous = "Filter_By_Hazardous";
  static const String Filter_By_OnOrder = "Filter_By_OnOrder";
  static const String Filter_By_VCFCalculated = "Filter_By_VCFCalculated";
  static const String Filter_By_LoadAll = "Filter_By_LoadAll";
  static const String Filter_By_LoadEmpty = "Filter_By_LoadEmpty";
  static const String Filter_By_Loaded = "Filter_By_Loaded";


  //ListView Filter By Yards & Tracks
  static const String Filter_By_YardAndTrack = "Filter_By_YardAndTrack";
  static const String Filter_By_Yard = "Filter_By_Yard";
  static const String Filter_By_Track = "Filter_By_Track";

  static const String DOT_SPEC= "DOT SPEC";

  //VCF Types
  static const String TABLE_24E = "ASTM24E";
  static const String TABLE_D4311 = "D4311";
  static const String Benzene_D1555 = "Benzene-D1555";
  static const String Cumene_D1555 = "Cumene-D1555";
  static const String Cyclohexane_D1555 = "Cyclohexane-D1555";
  static const String Ethylbenzene_D1555 = "Ethylbenzene-D1555";
  static const String Styrene_D1555 = "Styrene-D1555";
  static const String Toluene_D1555 = "Toluene-D1555";
  static const String m_XyleneA_D1555 = "m-XyleneA-D1555";
  static const String o_Xylene_D1555 = "o-Xylene-D1555";
  static const String p_Xylene_D1555 = "p-Xylene-D1555";
  static const String Hyd300350_D1555 = "300350AroHyd-D1555";
  static const String Hyd350400_D1555 = "350400AroHyd-D1555";
  static const String TABLE_24D = "24D";
  static const String TABLE_SULFUR = "SULFUR";

}
