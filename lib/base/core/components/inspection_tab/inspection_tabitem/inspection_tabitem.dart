class InspectionTabItem {
  InspectionTabItem({this.title});

  final String title;

  static List<InspectionTabItem> getTabName() {
    return (<InspectionTabItem>[
      InspectionTabItem(title: 'Incomplete'),
      InspectionTabItem(title: 'Complete'),
      InspectionTabItem(title: 'Failed')
    ]);
  }
}
