//import 'package:flutter/material.dart';
//import 'package:flutter_screenutil/flutter_screenutil.dart';
//
//class BottomSheetItem extends StatelessWidget {
//  BottomSheetItem({this.title, this.ontap, this.icon, this.imagePath});
//
//  final String imagePath;
//  final IconData icon;
//  final Function ontap;
//  final String title;
//
//  @override
//  Widget build(BuildContext context) {
//    return Padding(
//      padding: EdgeInsets.symmetric(vertical: 10),
//      child: Container(
//        width: 400,
//        child: ListTile(
//            leading: Image.asset(
//              imagePath,
//              width: ScreenUtil().setWidth(36),
//              height: ScreenUtil().setHeight(36),
//            ),
//            title: Text(title,
//                style: TextStyle(
//                    fontFamily: 'Roboto',
//                    fontSize: ScreenUtil().setHeight(16),
//                    fontStyle: FontStyle.normal,
//                    fontWeight: FontWeight.normal,
//                    letterSpacing: ScreenUtil().setWidth(0.5),
//                    color: Theme.of(context).textTheme.headline.color)),
//            onTap: ontap),
//      ),
//    );
//  }
//}
