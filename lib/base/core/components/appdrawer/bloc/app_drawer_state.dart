import 'package:equatable/equatable.dart';

class AppDrawerState extends Equatable {
  final String page;

  const AppDrawerState(this.page);

  @override
  String toString() {
    return 'AppDrawerState{page: $page}';
  }

  @override
   List<Object> get props => [page];
}
