import 'dart:async';

import 'package:mbase/base/core/common_base/bloc/scy_bloc.dart';
import 'package:mbase/base/core/components/appdrawer/bloc/app_drawer_event.dart';
import 'package:mbase/base/core/components/appdrawer/bloc/app_drawer_state.dart';

class AppDrawerBloc extends SCYBloC<AppDrawerEvent, AppDrawerState> {
  AppDrawerBloc(AppDrawerState initialState) : super(initialState);

  @override
  AppDrawerState get initialState => AppDrawerState("Home");

  @override
  String toString() {
    return 'AppDrawerBloc{}';
  }

  @override
  Stream<AppDrawerState> mapEventToState(
    AppDrawerEvent event,
  ) async* {
    if (event.page == "Home") {
      yield AppDrawerState("Home");
    }

    if (event.page == "Home2") {
      yield AppDrawerState("Home2");
    }
    if (event.page == "Home3") {
      yield AppDrawerState("Home3");
    }
    if (event.page == "ListViewWidget") {
      yield AppDrawerState("ListViewWidget");
    }
    if (event.page == "InspectionQuestionnaireWidget") {
      yield AppDrawerState("InspectionQuestionnaireWidget");
    }

    if (event.page == "RailCarListView") {
      yield AppDrawerState("RailCarListView");
    }
    if (event.page == "SwitchList") {
      yield AppDrawerState("SwitchList");
    }

    if (event.page == "Inspection") {
      yield AppDrawerState("Inspection");
    }

    if (event.page == "AddSeal") {
      yield AppDrawerState("AddSeal");
    }

    if (event.page == "logout") {
      yield AppDrawerState("logout");
    }
  }
}
