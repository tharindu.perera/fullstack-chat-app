import 'package:equatable/equatable.dart';

class AppDrawerEvent extends Equatable {
  final String page;

  const AppDrawerEvent(this.page);

  @override
  List<Object> get props => [page];

  @override
  String toString() {
    return 'AppDrawerEvent{page: $page}';
  }
}
