import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/components/appdrawer/bloc/app_drawer_bloc.dart';
import 'package:mbase/base/core/components/appdrawer/bloc/app_drawer_event.dart';
import 'package:mbase/base/core/components/drawer_item/drawer_item.dart';
import 'package:mbase/base/core/components/screen_factory.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/keys/global-keys.dart';
import 'package:mbase/base/modules/aeiscan/ui/aei_scan_screen.dart';
import 'package:mbase/base/modules/appcards/ui/home.dart';
import 'package:mbase/base/modules/facility/ui/facility.dart';
import 'package:mbase/base/modules/inspection/ui/inspection_landing.dart';
import 'package:mbase/base/modules/listview/listview/ui/listview_screen.dart';
import 'package:mbase/base/modules/listview/yardview/ui/yardview_screen.dart';
import 'package:mbase/base/modules/load_vcf/ui/load_vcf_railcar_selection.dart';
import 'package:mbase/base/modules/notification/ui/notification_screen.dart';
import 'package:mbase/base/modules/seal/seal/ui/seal.dart';
import 'package:mbase/base/modules/switchlist/switchlist/ui/switchlists.dart';
import 'package:mbase/base/modules/unloadvcf/ui/unloadvcf_railcar_select.dart';
import 'package:mbase/env.dart';
import 'package:provider/provider.dart';

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  final String inspectiondark = "assets/images/inspection-dark.png";
  final String inspectionlight = "assets/images/inspection-light.png";
  final String loadcardark = "assets/images/list-view-dark.png";
  final String loadcarlight = "assets/images/list-view-light.png";
  final String switchlistdark = "assets/images/switch-list-dark.png";
  final String switchlistlight = "assets/images/switch-list-light.png";
  final String sealdark = "assets/images/seal-dark.png";
  final String seallight = "assets/images/seal-light.png";
  final String notificationdark = "assets/images/notification-black.png";
  final String notificationlight = "assets/images/notification.png";
  final String facilitydark = "assets/images/facility-blue.png";
  final String facilitylight = "assets/images/facility-black.png";
  final String homedark = "assets/images/home-blue.png";
  final String homelight = "assets/images/home-black.png";
  final String aeiDark = "assets/images/aei-dark.png";
  final String aeiLight = "assets/images/aei-light.png";
  final String loadVCFDark = "assets/images/LoadVCF-dark.png";
  final String loadVCFLight = "assets/images/LoadVCF-light.png";
  final String unloadVCFDark = "assets/images/UnloadVCF-dark.png";
  final String unloadVCFLight = "assets/images/UnloadVCF-light.png";

  bool _logoutClicked = false;

  @override
  void initState() {
    super.initState();
  }

  bool checkCardWithUserProfile(String screen_id) {
    return env.userProfile.screenIdList[env.userProfile.facilityId].contains(screen_id);
  }

  @override
  void dispose() {
    this._logoutClicked ? BlocProvider.of<AppDrawerBloc>(keyGlobals.scaffoldKey.currentContext).add(AppDrawerEvent("logout")) : null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    ScreenUtil.init(context, width: 1024, height: 768);

    return Container(
      width: ScreenUtil().setWidth(262),
      child: Drawer(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(
                height: ScreenUtil().setHeight(700),
                child:ListView(
                  //  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      height: ScreenUtil().setHeight(56),
                      child: DrawerHeader(),
                    ),
                    DrawerItem(
                      title: "Home",
                      imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? homedark : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? homelight : null,
                      ontap: () {
                        Navigator.pop(context);
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(Home())));
//              Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                      },
                    ),
                    checkCardWithUserProfile("Screen:ListView") ?
                    DrawerItem(
                        title: "List View",
                        imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? loadcardark : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? loadcarlight : null,
                        ontap: (){
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(ListViewScreen())));
                        })
                        : Container(),
                    checkCardWithUserProfile("Screen:ListView") ?
                    DrawerItem(
                        title: "Yard View",
                        imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? loadcardark : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? loadcarlight : null,
                        ontap: (){
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(YardViewScreen())));
                        })
                        : Container(),
                    checkCardWithUserProfile("Screen:SwitchList") ?
                    DrawerItem(
                        title: "Switch List",
                        imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? switchlistdark : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? switchlistlight : null,
                        ontap:() {
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => ScreenFactory(SwitchListsBlocProvider())));
                        }
                    )
                        : Container(),
                    checkCardWithUserProfile("Screen:Inspection")
                        ? DrawerItem(
                        title: "Inspection",
                        imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? inspectiondark : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? inspectionlight : null,
                        ontap:() {
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => ScreenFactory(InspectionLandingBlocProvider())));
                        }
                    )
                        : Container(),
                    checkCardWithUserProfile("Screen:Seal")
                        ? DrawerItem(
                        title: "Seal",
                        imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? sealdark : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? seallight : null,
                        ontap: () {
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(AddSeal())));
                        })
                        : Container(),
                    checkCardWithUserProfile("Screen:LoadVCF")
                        ? DrawerItem(
                        title: "Load VCF",
                        imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? loadVCFDark : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? loadVCFLight : null,
                        ontap: () {
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(AddLoadVCF())));
                        })
                        : Container(),
                    checkCardWithUserProfile("Screen:UnloadVCF")
                        ? DrawerItem(
                        title: "Unload VCF",
                        imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? unloadVCFDark : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? unloadVCFLight : null,
                        ontap: () {
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(UnloadVcfSelectRailcar())));
                        })
                        : Container(),
                    checkCardWithUserProfile("Screen:AEIScan")
                        ? DrawerItem(
                        title: "AEI Scan",
                        imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? aeiDark : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? aeiLight : null,
                        ontap: () {
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(AEIScanScreen())));
                        })
                        : Container(),
                    DrawerItem(
                        title: "Notifications",
                        imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? notificationlight : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? notificationdark : null,
                        ontap: () {
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(NotificationScreen())));
                        }),
                    DrawerItem(
                        title: "Facility Selection",
                        imagePath: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? facilitydark : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? facilitylight : null,
                        ontap: () {
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>  Facility(false)));
                        }),

                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top:ScreenUtil().setHeight(30) ,bottom: ScreenUtil().setHeight(16) ),
                child: Center(
                  child: Text("Powered By Kaleris (version ${env.version})",
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Roboto',
                        fontSize: ScreenUtil().setSp(11, allowFontScalingSelf: true),
                        //letterSpacing: ScreenUtil().setWidth(-0.22)
                      )),
                ),
              )

            ],
          )
      ),
    );
  }
}
