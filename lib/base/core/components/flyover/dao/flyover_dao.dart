import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/facility_model.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/env.dart';

class FlyOverDao {
  FirebaseFirestore _firestore = DatabaseProvider.firestore;

  Future<FacilityModel> fetchFacilityData() async {
    CollectionReference collection = _firestore.collection("facility");
    try {
      QuerySnapshot qShot = await collection
          .where("client_id", isEqualTo: env.userProfile.clientId)
          .where("id", isEqualTo: env.userProfile.facilityId)
          .get();
      if (qShot.docs.isNotEmpty) {
        return FacilityModel.fromMap(qShot.docs[0].data());
      } else {
        return null;
      }
    } on Exception {
      throw Exception(
          'Error in fetching Facility by Id :${env.userProfile.id}');
    }
  }

  Future<Equipment> fetchRailCarDetails(
      {String equipmentInitial, String equipmentNumber}) async {
    CollectionReference collection = _firestore.collection("equipment");
    try {
      QuerySnapshot qShot = await collection
          .where("equipment_initial", isEqualTo: equipmentInitial)
          .where("equipment_number", isEqualTo: equipmentNumber)
          .where("facility_id", isEqualTo: env.userProfile.facilityId)
          .get();
      if (qShot.docs.isNotEmpty) {
        return Equipment.fromMap(qShot.docs[0].data());
      } else {
        return null;
      }
    } on Exception {
      throw Exception(
          'Error in fetching Equipment Details by Id :$equipmentInitial $equipmentNumber');
    }
  }
}
