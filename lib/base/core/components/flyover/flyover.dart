import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:provider/provider.dart';

import 'flyover_repository/repository.dart';

enum FlyOverAction { done, forward, backward }

class FlyOver extends StatefulWidget {
  FlyOver(List<Equipment> selectedEquipments) {
    this.selectedEquipments = selectedEquipments;
  }

  List<Equipment> selectedEquipments = List<Equipment>();

  @override
  _FlyOverState createState() => _FlyOverState();
}

class _FlyOverState extends State<FlyOver> {
  ThemeChanger _themeChanger;
  FlyOverRepository flyOverRepository = FlyOverRepository();
  Map<String, String> flyOverDataMap = {};
  bool isDataLoaded = false;
  Equipment railcar = null;
  int current_iteration = 0;
  bool showPop=false;
  bool isTiPH=false;
  bool isHazmat=false;
  bool isInspFailed=false;
  bool isInspthru=false;
  bool isInspInProg=false;

//  List<String> selectedRailCarNumbers = [];

  @override
  void initState() {
    super.initState();
    fetchFlyOverData(widget.selectedEquipments.first);
    selectedRailcar();
  }

  selectedRailcar() {
    setState(() {
      railcar = widget.selectedEquipments[current_iteration];
    });
  }

  void changeRailCar({FlyOverAction action}) {
    switch (action) {
      case FlyOverAction.backward:
        current_iteration--;
        railcar = widget.selectedEquipments[current_iteration];
        fetchFlyOverData(railcar);
        break;

      case FlyOverAction.forward:
        current_iteration++;
        railcar = widget.selectedEquipments[current_iteration];
        fetchFlyOverData(railcar);
        break;
    }
    setState(() {});
  }

  void fetchFlyOverData(equipment) async {
      isTiPH=false;
      isHazmat=false;
      isInspFailed=false;
      isInspthru=false;
      isInspInProg=false;
    Loader.show(context);
    flyOverDataMap = await flyOverRepository.fetchFlyOverFacilityConfiguration(
        equipment: equipment);
    if (flyOverDataMap != null) {
      if(flyOverDataMap.containsKey("Inspected")) {
       if( flyOverDataMap["Inspected"] == "Started" ){
         isInspInProg=true;
      }else if(flyOverDataMap["Inspected"] == "COMPLETED"){
         isInspthru=true;
       }else if(flyOverDataMap["Inspected"] == "FAILED"){
         isInspFailed=true;
       }
        Loader.hide();
        setState(() {
          showPop=true;
        });
      }

      if(flyOverDataMap.containsKey("Hazmat Class")) {
        if( flyOverDataMap["Hazmat Class"] !=null && (flyOverDataMap["Hazmat Class"].startsWith("49") || flyOverDataMap["Hazmat Class"].startsWith("48"))){
          isHazmat=true;
        }
      }


    }
  }

  List<Widget> flyOverItemList() {
    List<Widget> flyOverItemList = [];
    for (var key in flyOverDataMap.entries) {
      Widget item = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
      Text(
        "${key?.key} :",
        style: TextStyle(
            color: Theme.of(context).textTheme.bodyText1.color,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.bold,
            fontSize: ScreenUtil().setSp(12, allowFontScalingSelf: true)),
      ),
      SizedBox(
        width: 5,
      ),
      Text(key.value == null || key.value.trim() == "" ? "N/A" : key.value,
          style: TextStyle(
              fontStyle: FontStyle.normal,
              fontFamily: 'Roboto',
              letterSpacing: ScreenUtil().setWidth(0.25),
              color: Theme.of(context).textTheme.bodyText1.color,
              fontSize: ScreenUtil().setSp(12, allowFontScalingSelf: true)))
        ],
      );
      flyOverItemList.add(item);
    }
    isDataLoaded = true;
    return flyOverItemList;
  }

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);
    print("---------rebuilding ${showPop}------");
    return SingleChildScrollView(scrollDirection: Axis.vertical,
      child: showPop ? AlertDialog(
        backgroundColor: Colors.transparent,
        elevation: 0,
        content: Container(color:  Theme.of(context).canvasColor,
          child: Column(mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    current_iteration > 0
                        ? Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            width: ScreenUtil().setWidth(20),
                            height: ScreenUtil().setHeight(20),
                            color: Colors.grey,
                            child: Center(
                              child: GestureDetector(
                                  onTap: () {
                                    changeRailCar(action: FlyOverAction.backward);
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: 15,
                                  )),
                            ),
                          )
                        : Container(),
                    Container(
                      child: Text(
                        railcar.equipmentInitial + " " + railcar.equipmentNumber,
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontStyle: FontStyle.normal,
                            fontFamily: 'Roboto',
                            letterSpacing: ScreenUtil().setWidth(0.5),
                            color: Theme.of(context).textTheme.bodyText1.color,
                            fontSize:
                                ScreenUtil().setSp(20, allowFontScalingSelf: true)),
                      ),
                    ),
                    SizedBox(width: 10,),
                    isTiPH?
                    Icon(
                      Icons.warning,
                      color: Colors.yellow,

                    ):Container(),
                    isHazmat?Icon(
                      Icons.warning,
                      color: Colors.red,

                    ):Container(),
                    isInspthru?
                    Icon(
                      Icons.check,
                      color: Colors.blue,

                    ):Container(),
                    isInspInProg?
                    Icon(
                      Icons.rate_review,
                      color: Colors.blue,

                    ):Container(),
                    isInspFailed?
                    Icon(
                      Icons.warning,
                      color: Colors.blue,

                    ):Container(),
                    current_iteration != widget.selectedEquipments.length - 1
                        ? Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            width: ScreenUtil().setWidth(20),
                            height: ScreenUtil().setHeight(20),
                            color: Colors.grey,
                            child: Center(
                              child: GestureDetector(
                                  onTap: () {
                                    changeRailCar(action: FlyOverAction.forward);
                                  },
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.white,
                                    size: 15,
                                  )),
                            ),
                          )
                        : Container()
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "${current_iteration + 1} of ${widget.selectedEquipments.length} ",
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Roboto',
                      letterSpacing: ScreenUtil().setWidth(0.5),
                      color: Theme.of(context).textTheme.bodyText1.color,
                      fontSize: ScreenUtil().setSp(12, allowFontScalingSelf: true)),
                ),
              ),
              ConstrainedBox(
                constraints:   BoxConstraints(
                  minWidth: 200,
                  minHeight: 200,
                  maxWidth:MediaQuery.of(context).size.width,
                  maxHeight: MediaQuery.of(context).size.height-200,
                ),
                child:
                Padding(
                  padding: const EdgeInsets.fromLTRB( 50,30,50,30),
                  child: SingleChildScrollView( scrollDirection: Axis.horizontal,
                  child: Wrap(
                      direction: Axis.vertical,
                      alignment: WrapAlignment.start,
                    spacing: 10.0,
                    runSpacing: 20.0,
                      children: flyOverItemList()),
                  ),
                )
              ),
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(FlyOverAction.done);
                  },
                  child: Text(
                    'DONE',
                    style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: ScreenUtil().setHeight(14),
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        letterSpacing: ScreenUtil().setWidth(1.25),
                        color: Colors.white),
                  ),
                  color: Color(0xFF508be4),
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black,
                  splashColor: Color(0xFF3e8aeb))
            ],
          ),
        ),

      ):Container(),
    );
  }
}
