import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/dao/asset_master_data_dao.dart';
import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/dao/spot_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/event_history.dart';
import 'package:mbase/base/core/common_base/model/facility_model.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/common_base/model/uom.dart';
import 'package:mbase/base/core/common_base/model/uom_conversion.dart';
import 'package:mbase/base/core/components/flyover/dao/flyover_dao.dart';
import 'package:mbase/base/core/components/flyover/dto/flyover_dto.dart';
import 'package:mbase/base/core/util/format_dates.dart';
import 'package:mbase/base/modules/inspection/repository/inspection_details_repository.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/dao/assign_block_dao.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/assigned_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/dao/assign_defect_dao.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/model/assigned_defect.dart';
import 'package:mbase/base/modules/listview/actions/comment/dao/comment_dao.dart';
import 'package:mbase/base/modules/listview/actions/comment/model/comment_model.dart';
import 'package:mbase/base/modules/load_vcf/dao/load_vcf_dao.dart';
import 'package:mbase/base/modules/load_vcf/model/load_vcf_model.dart';
import 'package:mbase/base/modules/seal/dao/seal_dao.dart';
import 'package:mbase/base/modules/seal/model/seal_model.dart';
import 'package:mbase/base/modules/unloadvcf/dao/unloadvcf_dao.dart';
import 'package:mbase/base/modules/unloadvcf/model/unloadvcf_model.dart';

class FlyOverRepository {
  FlyOverDao flyOverDao = FlyOverDao();

  Future<Map<String, String>> fetchFlyOverFacilityConfiguration(
      {Equipment equipment}) async {
    try {
      FlyOverDto dto = FlyOverDto();
      FacilityModel flyOverConfigurations =
          await flyOverDao.fetchFacilityData();
      if (flyOverConfigurations != null &&
          flyOverConfigurations.flyover_field_list.length > 0) {
        flyOverConfigurations?.flyover_field_list
            ?.forEach((element) => dto.flyOverDto[element] = null);
        ProductModel productModel;
        print("equipment>>${equipment}");
        var weightInPounds;
        var weightInTon;
        var uomTonConversion;
        if (equipment.compartmentList.isNotEmpty) {
          try {
            flyOverConfigurations.flyover_field_list.contains("L/E Status")
                ? {
              dto.flyOverDto["L/E Status"] = double.parse(
                  equipment?.compartmentList[0].currentAmount ??
                      "0") >
                  0
                  ? "L"
                  : 'E'
            }
                : null;
          } catch (ex, stackTrace) {
            dto.flyOverDto["L/E Status"] = "NO DATA";
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }

          try {
            if (flyOverConfigurations.flyover_field_list.contains("Product")) {
              dto.flyOverDto["Product"] =
                  equipment?.compartment_summary?.map((e) => e.product_name)
                      ?.join(" | ");
            }
          } catch (ex, stackTrace) {
            dto.flyOverDto["Product"] = "NO DATA";
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }
          try {
//            UOM uom = MemoryData.uomMap[equipment?.compartmentList[0].uomId];
//            flyOverConfigurations.flyover_field_list.contains("Quantity")
//                ? dto.flyOverDto["Quantity"] =
//            "${NumberFormat('###,###,##0').format(double.parse(
//                equipment?.compartmentList[0].currentAmount ?? "0"))} ${uom
//                .unit}"
//                : null;

            double totalWeightInPounds=0;
            equipment.compartment_summary.forEach((element) {
              totalWeightInPounds +=double.parse(element.total_quantity_in_pounds);
            });
            flyOverConfigurations.flyover_field_list.contains("Quantity")
                ? dto.flyOverDto["Quantity"] =
            "${NumberFormat('###,###,##0').format(totalWeightInPounds)} lbs"
                : null;
          } catch (ex, stackTrace) {
            dto.flyOverDto["Quantity"] = "NO DATA";
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }
          try {
            productModel = (await CommonDao()
                .fetchProductById(equipment?.compartmentList[0]?.productId));
          } catch (ex, stackTrace) {
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }

          print(">>>>>productModel : ${productModel}");
          try {
            print("calculating weightInPounds");
//            if (MemoryData.uomMap[equipment?.compartmentList[0].uomId].unit ==
//                "Pounds") {
//              weightInPounds = double.parse(
//                  equipment?.compartmentList[0].currentAmount ?? "0");
//            } else {
//              String uomName =
//                  MemoryData.uomMap[equipment?.compartmentList[0].uomId].unit;
//              var uomConversion = productModel.inventoryConversionListList
//                  .firstWhere((element) {
//                if (element.from_uom_name == uomName &&
//                    element.to_uom_name == "Pounds") {
//                  return true;
//                } else {
//                  return false;
//                }
//              }, orElse: () => null);
//              print("uomName ${uomName}");
//              print("uomConversion ${uomConversion}");
//              print("uomConversion?.conversion_facto ${uomConversion
//                  ?.conversion_factor}");
//              weightInPounds = double.parse(
//                  equipment?.compartmentList[0].currentAmount ?? "0") *
//                  double.parse(uomConversion?.conversion_factor ?? "0");
//            }
            weightInPounds=0;
            print("  equipment = $equipment");
          equipment.compartment_summary.forEach((element) {
            weightInPounds +=double.parse(element.total_quantity_in_pounds);
          });
            if (flyOverConfigurations.flyover_field_list.contains(
                "Weight in Pounds")) {
              dto.flyOverDto["Weight in Pounds"] =
                  NumberFormat('###,###,##0').format(weightInPounds);
              print("  weightInPounds = $weightInPounds");

            }
          } catch (ex, stackTrace) {
            dto.flyOverDto["Weight in Pounds"] = "NO DATA";
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }

          //Net Weight
          if (flyOverConfigurations.flyover_field_list.contains("Net Wt")){
            print("calculating net weight.....");
            try {
               UOMConversion uomConversion=   MemoryData.uomConversionByName["PoundsMetric Tons"];
              if(uomConversion==null) {
                print("################# Warning -> Pounds to Tons conversion not found #################");
              }
                weightInTon=  (double.parse(uomConversion?.conversion_factor??"0") * weightInPounds);
                dto.flyOverDto["Net Wt"] =
                "${NumberFormat('###,###,##0').format(weightInTon)} Tons | ${NumberFormat(
                    '###,###,##0').format(weightInPounds)} lbs";

            } catch (ex, stackTrace) {
              dto.flyOverDto["Net Wt"] = "NO DATA";
              print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
              print(stackTrace.toString());
            }
        }
        } else {
          flyOverConfigurations.flyover_field_list.contains("L/E Status")
              ? {dto.flyOverDto["L/E Status"] = 'E'}
              : null;
          flyOverConfigurations.flyover_field_list.contains("Net Weight")
              ? {dto.flyOverDto["Net Wt"] = null}
              : null;
          flyOverConfigurations.flyover_field_list.contains("Weight in Pounds")
              ? {dto.flyOverDto["Weight in Pounds"] = null}
              : null;

          flyOverConfigurations.flyover_field_list.contains("Quantity")
              ? dto.flyOverDto["Quantity"] = null
              : null;
        }

        try {
          flyOverConfigurations.flyover_field_list.contains("Last Cont Prod")
              ? {
                  dto.flyOverDto["Last Cont Prod"] = (await CommonDao()
                          .fetchProductById(equipment?.lastContainedProductId))
                      ?.productName
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Last Cont Prod"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        if (equipment.asset_location_type_descr == "Track") {
          dto.flyOverDto.remove("Spot");
          try {
//                  flyOverConfigurations.flyover_field_list.contains("Sequence")? {dto.flyOverDto[ "Sequence"] =  equipment?.sequenceRailcar == null ? null : ( equipment?.sequenceRailcar.toString() +" of "+(await EquipmentDAO().geEquipmentByTrackID(equipment?.trackId,equipment.yardId)).length.toString())  }:null;
            flyOverConfigurations.flyover_field_list.contains("Sequence")
                ? {
                    dto.flyOverDto["Sequence"] =
                        equipment?.sequenceRailcar == null
                            ? null
                            : equipment?.sequenceRailcar.toString()
                  }
                : null;
          } catch (ex, stackTrace) {
            dto.flyOverDto["Sequence"] = "NO DATA";
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }
        } else if (equipment.asset_location_type_descr == "Spot") {
          dto.flyOverDto.remove("Sequence");
          try {
            var spotModel = await SpotDao().fetchSpotById(equipment.spotId);
            flyOverConfigurations.flyover_field_list.contains("Spot")
                ? {dto.flyOverDto["Spot"] = spotModel?.spotName}
                : null;
          } catch (ex, stackTrace) {
            dto.flyOverDto["Spot"] = "NO DATA";
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }
        }

        flyOverConfigurations.flyover_field_list.contains("On Order")
            ? {
                dto.flyOverDto["On Order"] =
                    equipment?.purchaseOrderId != null ? "Yes" : "No"
              }
            : null;
        try {
          flyOverConfigurations.flyover_field_list.contains("Inbound Date")
              ? {
                  dto.flyOverDto["Inbound Date"] =
                      FormatDates().dateFormatToString(equipment?.placedDate)
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Inbound Date"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Inb Shipper")
              ? {
                  dto.flyOverDto["Inb Shipper"] =
                      equipment?.inbound_shipper_name
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Inb Shipper"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Inb Consignee")
              ? {
                  dto.flyOverDto["Inb Consignee"] =
                      equipment?.inbound_consignee_name?.toString()
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Inb Consignee"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Inb Origin")
              ? {
                  dto.flyOverDto["Inb Origin"] =
                      equipment?.inbound_wb_origin?.toString()
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Inb Origin"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Inb Dest")
              ? {
                  dto.flyOverDto["Inb Dest"] =
                      equipment?.inbound_wb_destination?.toString()
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Inb Dest"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Outb Shipper")
              ? {
                  dto.flyOverDto["Outb Shipper"] =
                      equipment?.outbound_shipper_name
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Outb Shipper"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Outb Consignee")
              ? {
                  dto.flyOverDto["Outb Consignee"] =
                      equipment?.outbound_consignee_name
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Outb Consignee"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Fac Cust")
              ? {
                  dto.flyOverDto["Fac Cust"] =
                      equipment?.inbound_consignee_name?.toString()
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Fac Cust"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Outb Origin")
              ? {dto.flyOverDto["Outb Origin"] = equipment?.outbound_origin}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Outb Origin"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Outb Dest")
              ? {dto.flyOverDto["Outb Dest"] = equipment?.outbound_destination}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Outb Dest"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Shipment Type")
              ? {
                  dto.flyOverDto["Shipment Type"] =
                      equipment?.shipment_type_desc
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Shipment Type"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list
                  .contains("Permanent/Free Runner")
              ? {
                  dto.flyOverDto["Permanent/Free Runner"] =
                      equipment?.service_code
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Permanent/Free Runner"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Train Name")
              ? {dto.flyOverDto["Train Name"] = equipment?.shipment_type_name}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Train Name"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Inbound Seals")
              ? {dto.flyOverDto["Inbound Seals"] = equipment?.inbound_seals}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Inbound Seals"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Product Grade")
              ? {dto.flyOverDto["Product Grade"] = equipment?.product_grade}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Product Grade"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("IB Product")
              ? {dto.flyOverDto["IB Product"] = equipment?.ib_product}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["IB Product"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list
                  .contains("Hazmat Shipping Name")
              ? {
                  dto.flyOverDto["Hazmat Shipping Name"] =
                      equipment?.hazmat_shipping_name
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Hazmat Shipping Name"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list
                  .contains("Hazmat Shipping Qualifier")
              ? {
                  dto.flyOverDto["Hazmat Shipping Qualifier"] =
                      equipment?.hazmat_shipment_info_qualifier
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Hazmat Shipping Qualifier"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          var duration = DateTime.now().difference(equipment?.placedDate);
          flyOverConfigurations.flyover_field_list.contains("Time Online")
              ? {
                  dto.flyOverDto["Time Online"] =
                      "${duration.inDays} D, ${duration.inHours.remainder(24)} H, ${duration.inMinutes.remainder(60)} M"
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Time Online"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          if( flyOverConfigurations.flyover_field_list.contains("Inspected")){
         var ins=   (await InspectionDetailsRepo() .fetchInspectionByRailcar(equipment));
          if(ins!=null){
            dto.flyOverDto["Inspected"] = ins?.inspectionStatus=="SAVED"?"Started": ins?.inspectionStatus;
          }
          }
        } catch (ex, stackTrace) {
          dto.flyOverDto["Inspected"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
//asset master data
        try {
          var assetMasterlist = (await AssetMasterDataDao()
              .fetchAssetById(equipment.assetMasterId));
          AssetMasterDataModel assetMasterDataModel =
          assetMasterlist.isEmpty ? null : assetMasterlist.first;
          flyOverConfigurations.flyover_field_list.contains("Load Limit")
              ? {
            dto.flyOverDto["Load Limit"] = assetMasterDataModel
                ?.load_limit !=
                null
                ? "${NumberFormat('###,###,##0.##').format(
                double.parse(assetMasterDataModel?.load_limit))} lbs "
                : null
          }
              : null;

          try {
            flyOverConfigurations.flyover_field_list.contains("Compartments")
                ? dto.flyOverDto["Compartments"] =
                assetMasterDataModel.compartments ?? "0"
                : null;
          } catch (ex, stackTrace) {
            dto.flyOverDto["Compartments"] = "NO DATA";
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }

          flyOverConfigurations.flyover_field_list.contains("AAR Car Type")
              ? {
            dto.flyOverDto["AAR Car Type"] =
                assetMasterDataModel?.aar_car_type
          }
              : null;

          if(flyOverConfigurations.flyover_field_list.contains("Tare Wt")) {

            if(assetMasterDataModel?.tare_weight!=null){
              String tareweightInPounds=assetMasterDataModel.tare_weight;
              UOMConversion uomConversion=   MemoryData.uomConversionByName["PoundsMetric Tons"];
              if(uomConversion==null) {
                print("################# Warning -> Pounds to Tons conversion not found #################");
              }
                String tarewWightInTons=  (double.parse(uomConversion?.conversion_factor??"0") * double.parse(tareweightInPounds??"0")).toString();
                dto.flyOverDto["Tare Wt"] = "${tarewWightInTons} Tons"+ "| ${tareweightInPounds} lbs";

             }else{
              dto.flyOverDto["Tare Wt"] = "0 Tons"+ "| 0 lbs";
            }

          }

          if (flyOverConfigurations.flyover_field_list.contains("Gross Wt")){
            var tareWeight=double.parse(assetMasterDataModel?.tare_weight??"0");
            if(tareWeight==0){
              dto.flyOverDto["Gross Wt"] =  "${NumberFormat('###,###,##0').format(weightInTon??0)} Tons | ${NumberFormat('###,###,##0').format(weightInPounds??0)} lbs";;
            }else{
              UOMConversion uomConversion=   MemoryData.uomConversionByName["PoundsMetric Tons"];
              dto.flyOverDto["Gross Wt"] =  "${NumberFormat('###,###,##0').format(weightInTon??0+(tareWeight*(double.parse(uomConversion?.conversion_factor??"0"))))} Tons | ${NumberFormat('###,###,##0').format(weightInPounds??0+tareWeight)} lbs";
            }
          }

          flyOverConfigurations.flyover_field_list
                  .contains("Outside Car Length")
              ? {
                  dto.flyOverDto["Outside Car Length"] =
                      (assetMasterDataModel?.outside_length_feet ?? "0") +
                          " ft. " +
                          (assetMasterDataModel?.outside_length_inches ?? "0") +
                          " in."
                }
              : null;

          flyOverConfigurations.flyover_field_list.contains("Owner")
              ? {dto.flyOverDto["Owner"] = assetMasterDataModel?.owner}
              : null;
          flyOverConfigurations.flyover_field_list.contains("Equip Group")
              ? {
                  dto.flyOverDto["Equip Group"] =
                      equipment?.equipment_group_list?.join(" | ")
                }
              : null;
          flyOverConfigurations.flyover_field_list.contains("Hoppers and Gates")
              ? {
                  dto.flyOverDto["Hoppers and Gates"] =
                      "Hoppers  ${assetMasterDataModel?.outlets ?? 0} \n Gates  ${assetMasterDataModel?.bottom_outlet_fitting ?? 0}"
                }
              : null;
        } catch (ex, stackTrace) {
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          var defectlist = await AssignDefectDAO()
              .fetchRailcarDefectsByRailcarForFlyover(equipment.assetMasterId);
          print(".>>>>${defectlist.length}");
          AssignedDefect assignedDefect =
              defectlist.isEmpty ? null : defectlist.first;
          print(".>>>>${assignedDefect?.defect_name}");
          print(".>>>>${assignedDefect}");
          flyOverConfigurations.flyover_field_list.contains("Defects")
              ? {dto.flyOverDto["Defects"] = assignedDefect?.defect_name}
              : null;

          print(".>>>{dto.flyOverDto[]  >${dto.flyOverDto["Defects"] }");

        } catch (ex, stackTrace) {
          dto.flyOverDto["Defects"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          var blockList = await AssignBlockDAO()
              .fetchRailcarBlocksByAssetMaterID(equipment.assetMasterId);
          AssignedBlock railcarBlock =
              blockList.isEmpty ? null : blockList.first;
          flyOverConfigurations.flyover_field_list.contains("Block To")
              ? {dto.flyOverDto["Block To"] = railcarBlock?.blockName}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Block To"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          QuerySnapshot querySnapshot =
              await SealDAO().getSeal(assetmasterId: equipment.assetMasterId);
          SealModel seal = querySnapshot.docs.isNotEmpty
              ? SealModel.fromMap(querySnapshot.docs.first.data())
              : null;
          String sealText = "";

          if (seal != null) {
            seal.seal_text.forEach((element) {
              element != null ? sealText += element + "\n" : "";
            });
          }
          flyOverConfigurations.flyover_field_list.contains("Seals")
              ? {dto.flyOverDto["Seals"] = sealText}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Seals"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          var commentsForEquipment =
              await CommentDAO().getCommentsForFlyOver(equipment);

          Comment permComment = commentsForEquipment.isNotEmpty
              ? commentsForEquipment.firstWhere(
                  (element) => element.commentType == "Permanent",
                  orElse: () => null)
              : null;
          Comment comment = commentsForEquipment.isNotEmpty
              ? commentsForEquipment.firstWhere(
                  (element) => element.commentType == "FacilityVisit",
                  orElse: () => null)
              : null;
          Comment fleetComment = commentsForEquipment.isNotEmpty
              ? commentsForEquipment.firstWhere(
                  (element) => element.commentType == "Trax",
                  orElse: () => null)
              : null;
          Comment fleetComment2 = commentsForEquipment.isNotEmpty
              ? commentsForEquipment.firstWhere(
                  (element) => element.commentType == "Trax",
                  orElse: () => null)
              : null;
          flyOverConfigurations.flyover_field_list
                  .contains("Permanent Comments")
              ? {dto.flyOverDto["Permanent Comments"] = permComment?.comment}
              : null;
          flyOverConfigurations.flyover_field_list.contains("Comments")
              ? {dto.flyOverDto["Comments"] = comment?.comment}
              : null;
          flyOverConfigurations.flyover_field_list.contains("FleetComments")
              ? {dto.flyOverDto["FleetComments"] = fleetComment?.comment}
              : null;
          flyOverConfigurations.flyover_field_list.contains("Fleet Comments 2")
              ? {dto.flyOverDto["Fleet Comments 2"] = fleetComment2?.comment2}
              : null;

          flyOverConfigurations.flyover_field_list.contains("Hazmat Class")
              ? {dto.flyOverDto["Hazmat Class"] = productModel?.stcc}
              : null;
        } catch (ex, stackTrace) {
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }

        try {
          var eventHistoryList =
              await CommonDao().fetchEventHistoryByEquipment(equipment);
          flyOverConfigurations.flyover_field_list
                  .contains("Last Modified Date")
              ? {
                  dto.flyOverDto["Last Modified Date"] =
                      eventHistoryList.isNotEmpty
                          ? FormatDates().dateFromStringWithFormatted(
                              eventHistoryList.first.event_date)
                          : null
                }
              : null;
          flyOverConfigurations.flyover_field_list.contains("Last Load Date")
              ? {
                  dto.flyOverDto["Last Load Date"] = eventHistoryList.isNotEmpty
                      ? FormatDates().dateFromStringWithFormatted(
                          eventHistoryList
                              .firstWhere(
                                  (element) => element.event_type_id == "3",
                                  orElse: () => null)
                              ?.event_date)
                      : null
                }
              : null;
          flyOverConfigurations.flyover_field_list.contains("Last Unload Date")
              ? {
                  dto.flyOverDto["Last Unload Date"] =
                      eventHistoryList.isNotEmpty
                          ? FormatDates().dateFromStringWithFormatted(
                              eventHistoryList
                                  .firstWhere(
                                      (element) => element.event_type_id == "6",
                                      orElse: () => null)
                                  ?.event_date)
                          : null
                }
              : null;
          flyOverConfigurations.flyover_field_list.contains("Loading Notes")
              ? {
                  dto.flyOverDto["Loading Notes"] = eventHistoryList.isNotEmpty
                      ? eventHistoryList
                          .firstWhere((element) => element.event_type_id == "3",
                              orElse: () => null)
                          ?.note_list
                      : null
                }
              : null;

          try {
            if (flyOverConfigurations.flyover_field_list.contains("VCF-U - Net Weight")) {
              print("Calculating VCF-U - Net Weight .....");
//              eventHistoryList.forEach((element) { print(element);});
                  EventHistory ev = eventHistoryList.isNotEmpty ? eventHistoryList.firstWhere(
                    (element) => element.event_type_code == "ARAS", orElse: () => null) : null;
//                  print("VCF-U - Net Weight ev:" + ev.toString());
                  double convertedQuantity = ev?.quantity != null ? double.parse(ev.quantity) : null;
//                  print("VCF-U - Net Weight convertedQuantity1:" + convertedQuantity.toString());
                  if (ev?.quantity != null && ev?.uom_id != null && ev?.product_id != null) {
                    UOM uom = MemoryData.uomMap[ev.uom_id];
                    print("uom dryOrLiquid:" + uom.dryOrLiquid);
                    if (uom.dryOrLiquid == "LIQUID") {
                      ProductModel productModel = (await CommonDao().fetchProductById(ev.product_id));

                      var uomConversion = productModel.inventoryConversionListList
                          .firstWhere((element) {
                        if (element.from_uom_id == ev.uom_id && element.to_uom_name == "Pounds") {
                          return true;
                        } else {
                          return false;
                        }
                      }, orElse: () => null);
                      print("uomConversion conversion_factor:" + uomConversion?.conversion_factor);

                      convertedQuantity = double.parse(ev.quantity) * double.parse(uomConversion?.conversion_factor ?? "0");
                      print("convertedQuantity2:" + convertedQuantity.toString());
                    }
                    dto.flyOverDto["VCF-U - Net Weight"] = "${NumberFormat('#,###,##0.##').format(convertedQuantity)} lbs";
                  } else {
                    dto.flyOverDto["VCF-U - Net Weight"] = "N/A";
                  }
            }
          } catch (ex, stackTrace) {
            dto.flyOverDto["VCF-U - Net Weight"] = "N/A";
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }
        } catch (ex, stackTrace) {
          print("ERROR >>>>>>>>>>>>>>>>>>> ${ex.toString()}");
          print(stackTrace.toString());
        }

        try {
          flyOverConfigurations.flyover_field_list.contains("Inb PO")
              ? {
                  dto.flyOverDto["Inb PO"] = equipment != null &&
                          equipment.order_line_asset_list.isNotEmpty &&
                          equipment?.order_line_asset_list[0]
                                  ["inbound_outbound_ind"] ==
                              "I"
                      ? equipment?.order_line_asset_list[0]["purchase_order"]
                      : null ?? null
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Inb PO"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("Outb PO")
              ? {
                  dto.flyOverDto["Outb PO"] = equipment != null &&
                          equipment.order_line_asset_list.isNotEmpty &&
                          equipment?.order_line_asset_list[0]
                                  ["inbound_outbound_ind"] ==
                              "O"
                      ? equipment?.order_line_asset_list[0]["purchase_order"]
                      : null ?? null
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Outb PO"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list.contains("RC Status")
              ? {dto.flyOverDto["RC Status"] = equipment?.rc_status}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["RC Status"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
        try {
          flyOverConfigurations.flyover_field_list
                  .contains("Time since Waybilled")
              ? {dto.flyOverDto["Time since Waybilled"] = null}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["Time since Waybilled"] = "NO DATA";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }

        if (dto.flyOverDto["Last Load Date"] != null) {
          try {
            var durationSinceLastLoad = DateTime.now().difference(FormatDates()
                .dateFromStringAndFormat(
                    dto.flyOverDto["Last Load Date"], "MM-dd-yyyy HH:mm"));
            flyOverConfigurations.flyover_field_list.contains("Time Since Load")
                ? {
                    dto.flyOverDto["Time Since Load"] =
                        "${durationSinceLastLoad.inDays} D, ${durationSinceLastLoad.inHours.remainder(24)} H, ${durationSinceLastLoad.inMinutes.remainder(60)} M"
                  }
                : null;
          } catch (ex, stackTrace) {
            dto.flyOverDto["Time Since Load"] = "NO DATA";
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }
        }

        if (dto.flyOverDto["Last Unload Date"] != null) {
          try {
            var durationSinceLsstLoad = DateTime.now().difference(FormatDates()
                .dateFromStringAndFormat(
                    dto.flyOverDto["Last Unload Date"], "MM-dd-yyyy HH:mm"));
            flyOverConfigurations.flyover_field_list
                    .contains("Time Since Unload")
                ? {
                    dto.flyOverDto["Time Since Unload"] =
                        "${durationSinceLsstLoad.inDays} D, ${durationSinceLsstLoad.inHours.remainder(24)} H, ${durationSinceLsstLoad.inMinutes.remainder(60)} M"
                  }
                : null;
          } catch (ex, stackTrace) {
            dto.flyOverDto["Time Since Unload"] = "NO DATA";
            print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
            print(stackTrace.toString());
          }
        }

        var loadVCF = await LoadVCFDAO().getLoadVCF(LoadVCFModel(
            asset_master_id: equipment.assetMasterId,
            facility_visit_id: equipment.facilityVisitId));

        try {
          flyOverConfigurations.flyover_field_list
                  .contains("VCF-L - Net Gallons")
              ? {dto.flyOverDto["VCF-L - Net Gallons"] = "${NumberFormat('#,###,##0.##')
              .format(double.parse(loadVCF?.net_gallons??"0"))}"
          }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["VCF-L - Net Gallons"] = "N/A";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }

        try {
          flyOverConfigurations.flyover_field_list
                  .contains("VCF-L - Gross Gallons")
              ? {
                  dto.flyOverDto["VCF-L - Gross Gallons"] =
                  "${NumberFormat('#,###,##0.##').format(double.parse(loadVCF?.gross_gallons??"0"))}"
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["VCF-L - Gross Gallons"] = "N/A";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }

        try {
          flyOverConfigurations.flyover_field_list
                  .contains("VCF-L - Net Weight")
              ? {dto.flyOverDto["VCF-L - Net Weight"] = "${NumberFormat('#,###,##0.##')
              .format(double.parse(loadVCF?.product_weight??"0"))} lbs"}
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["VCF-L - Net Weight"] = "N/A";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }

        var unLoadVCF = await UnloadVcfDAO().getExistingUnloadVcf(
            UnloadVcfModel(
                asset_master_id: equipment.assetMasterId,
                facility_visit_id: equipment.facilityVisitId));

        try {
          flyOverConfigurations.flyover_field_list
                  .contains("VCF-U - Net Gallons")
              ? {dto.flyOverDto["VCF-U - Net Gallons"] =
                "${NumberFormat('#,###,##0.##').format(double.parse(unLoadVCF?.net_gallons??"0"))}"
          }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["VCF-U - Net Gallons"] = "N/A";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }

        try {
          flyOverConfigurations.flyover_field_list
                  .contains("VCF-U - Gross Gallons")
              ? {
                  dto.flyOverDto["VCF-U - Gross Gallons"] =
                  "${NumberFormat('#,###,##0.##').format(double.parse(unLoadVCF?.gross_gallons??"0"))}"
                }
              : null;
        } catch (ex, stackTrace) {
          dto.flyOverDto["VCF-U - Gross Gallons"] = "N/A";
          print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
          print(stackTrace.toString());
        }
      }

      print("FlyOverDto ${dto.flyOverDto}");

      return dto.flyOverDto;
    } catch (ex, stackTrace) {
      print("ERROR >>>>>>>>>>>>>>>>>>>${ex.toString()}");
      print(stackTrace.toString());
      return {"Error": ex.toString()};
    }
  }
}
