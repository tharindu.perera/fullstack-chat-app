import 'package:flutter/material.dart';

class CustomListtile extends StatelessWidget {
  const CustomListtile(
      {this.thumbnail, this.title, this.subtitle, this.trailing});

  final Widget thumbnail;
  final Widget title;
  final Widget subtitle;
  final Widget trailing;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: thumbnail,
          ),
          Expanded(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    title,
                    const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
                    subtitle
                  ],
                ),
              )),
          trailing
        ],
      ),
    );
  }
}
