import 'package:flutter/material.dart';

class TabComment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: Colors.white,
        child: Text(
          "Comment",
          style: TextStyle(fontSize: 22, color: Colors.blue),
        ),
      ),
    );
  }
}
