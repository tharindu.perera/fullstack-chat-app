import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mbase/base/core/components/tab_bar/tab_bar_item/tab_bar_item.dart';
import 'package:mbase/base/core/components/tab_bar/tab_body/tab_comment.dart';
import 'package:mbase/base/core/components/tab_bar/tab_body/tab_document.dart';
import 'package:mbase/base/core/components/tab_bar/tab_body/tab_equipment.dart';
import 'package:mbase/base/core/components/tab_bar/tab_body/tab_event.dart';
import 'package:mbase/base/core/components/tab_bar/tab_body/tab_railcar.dart';

class CustomTabBar extends StatefulWidget {
  @override
  CustomTabBarState createState() => CustomTabBarState();
}

class CustomTabBarState extends State<CustomTabBar> with SingleTickerProviderStateMixin {
  List<TabBarItem> tabitem;
  TabController controller;

  @override
  void initState() {
    super.initState();
    tabitem = TabBarItem.getTabName();

    controller = new TabController(length: tabitem.length, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: TabBar(
              controller: controller,
              tabs: tabitem.map((tabitem) {
                return Tab(
                  text: tabitem.title,
                );
              }).toList()),
        ),
        Expanded(
          child: TabBarView(
            controller: controller,
            children: <Widget>[
              TabComment(),
              TabRailCar(),
              TabEvent(),
              TabEquipment(),
              TabDocument(),
            ],
          ),
        )
      ],
    );
  }
}

//
//
