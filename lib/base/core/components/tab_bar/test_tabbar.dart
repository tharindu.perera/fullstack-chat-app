import 'package:flutter/material.dart';
import 'package:mbase/base/core/components/tab_bar/tab_bar_item/tab_bar_item.dart';

class TestTabBar extends StatefulWidget {
  @override
  _TestTabBarState createState() => _TestTabBarState();
}

class _TestTabBarState extends State<TestTabBar> with SingleTickerProviderStateMixin {
  List<TabBarItem> tabitem;
  TabController controller;

  @override
  void initState() {
    super.initState();
    tabitem = TabBarItem.getTabName();

    controller = new TabController(length: tabitem.length, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          TabBar(
              controller: controller,
              tabs: tabitem.map((tabitem) {
                return Tab(
                  text: tabitem.title,
                );
              }).toList()),
        ],
      ),
    );
  }
}
