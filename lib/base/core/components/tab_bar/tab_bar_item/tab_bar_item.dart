class TabBarItem {
  TabBarItem({this.title});

  final String title;

  static List<TabBarItem> getTabName() {
    return (<TabBarItem>[
      TabBarItem(title: 'Comments'),
      TabBarItem(title: 'Railcar History'),
      TabBarItem(title: 'Events'),
      TabBarItem(title: 'Equipment (Umler)'),
      TabBarItem(title: 'Documents')
    ]);
  }
}
