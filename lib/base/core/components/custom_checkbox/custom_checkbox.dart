import 'package:flutter/material.dart';

class CustomCheckbox extends StatelessWidget {
  CustomCheckbox(
      {@required this.checkValue, @required this.onTick, this.titleText});

  final bool checkValue;
  final Function onTick;
  final Widget titleText;

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      activeColor: Color(0xFF3e8aeb),
      title: titleText,
      value: checkValue,
      onChanged: onTick,
      controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
    );
  }
}
