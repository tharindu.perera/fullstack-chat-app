import 'package:flutter/material.dart';

class HomeCard extends StatelessWidget {
  HomeCard({@required this.ontap, @required this.icon, @required this.label});

  final IconData icon;
  final String label;
  final Function ontap;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 400.0,
      height: 200.0,
      child: GestureDetector(
        onTap: ontap,
        child: Container(
          margin: EdgeInsets.only(left: 15.0, right: 15.0),
          decoration: BoxDecoration(
              color: Theme.of(context).canvasColor,
              borderRadius: BorderRadius.circular(1.0)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                icon,
                size: 80.0,
              ),
              SizedBox(
                height: 15.0,
              ),
              Text(
                label,
                style: Theme.of(context).textTheme.headline,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
