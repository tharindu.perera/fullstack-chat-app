import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/modules/listview/actions/assign_switchlist/model/assigned_switchlist_model.dart';

class SwitchListModel {
  SwitchListModel(
      {this.SwitchListName,
      this.Cars,
      this.Complete,
      this.SwitchCrew,
      this.StartDate,
      this.TargetDate,
      this.SwitchType,
      this.id,
      this.reference,
      this.Status,
      this.immediateParentId,
      this.switchListEquipmentList,
      this.permission_id,
      this.parent_id,
      this.transaction_status,
      this.source,
      this.facility_id});

  var immediateParentId;

  final String SwitchListName;
  int Cars;
  String Complete;
  final String SwitchCrew;
  final DateTime StartDate;
  final DateTime TargetDate;
  final String SwitchType;
  String permission_id;
  String facility_id;
  String source;
  String transaction_status;
  String parent_id;
  String id;
  String Status;
  List<SwitchListEquipmentModel> switchListEquipmentList = [];

  DocumentReference reference;

  SwitchListModel.fromMap(Map<String, dynamic> map, {this.reference})
      : id = map['id'],
        SwitchListName = map['switch_list_name'],
        Cars = null,
        Complete = map['switch_list_type'],
        immediateParentId = map['immediateParentId'],
        StartDate = map['target_start_date'] != null ? DateTime.parse(map['target_start_date']) : null,
        TargetDate = map['target_complete_date'] != null ? DateTime.parse(map['target_complete_date']) : null,
        SwitchCrew = map['switch_crew'],
        SwitchType = map['switch_list_type'],
        Status = map['switch_list_status'],
        transaction_status = map['transaction_status'],
        source = map['source'],
        permission_id = map['permission_id'],
        parent_id = map['parent_id'],
        facility_id = map['facility_id'];

  SwitchListModel.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data(), reference: snapshot.reference);

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "switch_list_name": SwitchListName == null ? null : SwitchListName,
         null: Cars == null ? null : Cars,
         null : Complete == null ? null :Complete,
        "switch_list_type": Complete == null ? null : Complete.toString(),
        "target_start_date": StartDate == null ? null : StartDate.toString(),
        "immediateParentId": immediateParentId == null ? null : immediateParentId,
        "target_complete_date": TargetDate == null ? null : TargetDate.toString(),
        "switch_crew": SwitchCrew == null ? null : SwitchCrew,
        "switch_list_type": SwitchType == null ? null : SwitchType,
        "switch_list_status": Status == null ? null : Status,
        "permission_id": permission_id == null ? null : permission_id,
        "parent_id": parent_id == null ? null : parent_id,
        "transaction_status": transaction_status == null ? null : transaction_status,
        "source": source == null ? null : source,
        "facility_id": facility_id == null ? null : facility_id,
      };

  @override
  String toString() {
    return 'SwitchListModel{id: $id,switch_list_name:${SwitchListName} ,facility_id: $Cars,switch_list_type: $Complete ,'
        'target_start_date :$StartDate , target_complete_date: $TargetDate,  switch_crew: $SwitchCrew, '
        'switch_list_type: $SwitchType, switch_list_status::$Status}';
  }
}
