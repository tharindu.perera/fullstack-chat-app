import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/components/switchlist_table/tempory_model/switchlist_model.dart';
//import 'package:mbase/base/modules/switchlist/switchlist_detail/ui/switchlist_details.dart';

class SwitchListTable extends StatefulWidget {
  @override
  _SwitchListTableState createState() => _SwitchListTableState();
}

class _SwitchListTableState extends State<SwitchListTable> {
  List<SwitchListModel> listData;
  List data;

  @override
  void initState() {
    super.initState();
    //listData = SwitchListModel.getData();
  }

  @override
  Widget build(BuildContext context) {
    var formatter = new DateFormat('yyyy-MM-dd');

    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            child: FutureBuilder(
              future: DefaultAssetBundle.of(context).loadString('json_files/switchlist.json'),
              builder: (context, snapshot) {
                List mydata = jsonDecode(snapshot.data);

                return DataTable(
                  columns: [
                    DataColumn(label: Text('SWITCHLIST', style: Theme.of(context).textTheme.body1), numeric: false, tooltip: 'SWITCHLIST'),
                    DataColumn(
                        label: Text(
                          'CARS',
                          style: Theme.of(context).textTheme.body1,
                        ),
                        numeric: false,
                        tooltip: 'CARS'),
                    DataColumn(
                        label: Text(
                          '%COMPLETE',
                          style: Theme.of(context).textTheme.body1,
                        ),
                        numeric: false,
                        tooltip: '%COMPLETE'),
                    DataColumn(
                        label: Text(
                          'SWITCH CREW',
                          style: Theme.of(context).textTheme.body1,
                        ),
                        numeric: false,
                        tooltip: 'SWITCH CREW'),
                    DataColumn(
                        label: Text(
                          'START DATE',
                          style: Theme.of(context).textTheme.body1,
                        ),
                        numeric: false,
                        tooltip: 'START DATE'),
                    DataColumn(
                        label: Text(
                          'SWITCH TYPE',
                          style: Theme.of(context).textTheme.body1,
                        ),
                        numeric: false,
                        tooltip: 'SWITCH TYPE'),
                    DataColumn(
                        label: Text(
                          'STATUS',
                          style: Theme.of(context).textTheme.body1,
                        ),
                        numeric: false,
                        tooltip: 'STATUS'),
                  ],
                  rows: mydata
                      .map((mydata) => DataRow(cells: [
                            DataCell(Text(
                              mydata['switchlist'].toString(),
                              style: Theme.of(context).textTheme.body1,
                            )),
                            DataCell(Text(
                              mydata['cars'].toString(),
                              style: Theme.of(context).textTheme.body1,
                            )),
                            DataCell(Text(
                              mydata['complete'].toString(),
                              style: Theme.of(context).textTheme.body1,
                            )),
                            DataCell(Text(
                              mydata['crew'].toString(),
                              style: Theme.of(context).textTheme.body1,
                            )),
                            DataCell(Text(
                              formatter.format(DateTime.now()),
                              style: Theme.of(context).textTheme.body1,
                            )),
                            DataCell(Text(
                              mydata['type'].toString(),
                              style: Theme.of(context).textTheme.body1,
                            )),
                            DataCell(
//
                                ListTile(
                              leading: Text(
                                mydata['status'].toString(),
                                style: Theme.of(context).textTheme.body1,
                              ),
                              trailing: GestureDetector(
                                onTap: () {
//                                  Navigator.push(
//                                      context,
//                                      MaterialPageRoute(
//                                          builder: (context) =>
//                                              SwitchListDetails()));
                                },
                                child: Icon(
                                  Icons.arrow_right,
                                  color: Color(0xFF223f7a),
                                ),
                              ),
                            )),
                          ]))
                      .toList(),
                );
              },
            ),
          ),
        ));
  }
}
