import 'dart:async';

import 'package:mbase/base/core/common_base/bloc/scy_bloc.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/app_bar/app_bar_event.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/app_bar/app_bar_state.dart';

class AppBarBloc extends SCYBloC<AppBarEvent, AppBarState> {
  AppBarBloc(AppBarState initialState) : super(initialState);

  @override
  Stream<AppBarState> mapEventToState(AppBarEvent event) async* {
    if (event is AppBarDataSyncedEvent) {
      yield AppBarState( text: event.datetime);
    }

//    else if (event is HideAppBarEvent) {
//      yield AppBarState(show: false, text: this.state.text);
//    } else if (event is ChangAppBarTextEvent) {
//      yield AppBarState(show: this.state.show, text: event.text);
//    } else if (event is LogoutEvent) {
//      print("LogoutEvent!!!!!!! ${event.page}");
//      yield AppBarState(show: true, text: event.page);
//    }
  }

//  @override
//  void listeningToGlobal(state) {
//    if (state.bloc is NavigationBloc) {
//      if ((state.transition.event is LoginPageEvent)) {
//        add(HideAppBarEvent());
//      } else {
//        add(ShowAppBarEvent());
//      }
//      if ((state.transition.event is Home2PageEvent)) {
//        add(ChangAppBarTextEvent("Home2"));
//      }
//      if ((state.transition.event is HomePageEvent)) {
//        add(ChangAppBarTextEvent("Home"));
//      }
//    }
//  }
}
