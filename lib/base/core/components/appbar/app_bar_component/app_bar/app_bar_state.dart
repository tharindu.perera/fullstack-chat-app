import 'package:equatable/equatable.dart';

class AppBarState extends Equatable {
  final String text;

  const AppBarState({this.text = ""});

  @override
  List<Object> get props => [text];
}
