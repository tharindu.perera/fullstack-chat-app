import 'package:equatable/equatable.dart';

abstract class AppBarEvent extends Equatable {
  const AppBarEvent();

  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class AppBarDataSyncedEvent extends AppBarEvent {
  String datetime;
  AppBarDataSyncedEvent(this.datetime);
}
//
//class HideAppBarEvent extends AppBarEvent {
//  HideAppBarEvent();
//}
//
//class LogoutEvent extends AppBarEvent {
//  final String page;
//
//  LogoutEvent({this.page});
//}
//
//class ChangAppBarTextEvent extends AppBarEvent {
//  final String text;
//
//  ChangAppBarTextEvent(this.text);
//}
