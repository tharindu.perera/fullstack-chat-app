import 'dart:async';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';
import 'package:mbase/base/core/components/appbar/appbar_dropdown/appbar_dropdown.dart';
import 'package:mbase/base/core/components/screen_factory.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/modules/appcards/ui/home.dart';
import 'package:mbase/env.dart';
import 'package:provider/provider.dart';

import 'app_bar/app_bar_bloc.dart';
import 'app_bar/app_bar_state.dart';

class MainAppBar extends StatefulWidget implements PreferredSizeWidget {
  MainAppBar({Key key})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  _MainAppBarState createState() => _MainAppBarState();
}

class _MainAppBarState extends State<MainAppBar> {
  StreamSubscription<DataConnectionStatus> dataConnectionSubscription;
  String lastUpdateText = "";
  StreamSubscription snapshotSyncSub;
  bool isDataConnected = env.isDataConnectedForAppBar;

  @override
  void initState() {
    super.initState();
    snapshotSyncSub = DatabaseProvider.firestore.snapshotsInSync().listen((event) {
      lastUpdateText = DateFormat('MM/dd/yyyy HH:mm:ss').format(DateTime.now());
      setState(() {});

    });
//    env.dataConnectionChecker.connectionStatus.then((value) => isDataConnected = value == DataConnectionStatus.connected ? true : false);
    dataConnectionSubscription = env.dataConnectionChecker.onStatusChange.listen((DataConnectionStatus status) {
      setState(() {
        isDataConnected = status == DataConnectionStatus.connected ? true : false;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    snapshotSyncSub?.cancel();
    dataConnectionSubscription?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    final String Online = 'assets/images/portable_wifi_online.png';
    final String Offline = 'assets/images/portable_wifi_offline.png';
    final String kaleris_black_logo = 'assets/images/kaleris-micro-logo-green-black.png';
    final String kaleris_white_logo = 'assets/images/kaleris-micro-logo-green-white.png';

    final Widget WidgetOnline = Image.asset(Online);
    final Widget WidgetOffline = Image.asset(Offline);
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    String facilityText = MemoryData.facilityModel != null && MemoryData.facilityModel.facilityName != null ? "Facility - " + MemoryData.facilityModel.facilityName : "";

    return AppBar(
      iconTheme: Theme.of(context).appBarTheme.actionsIconTheme,
      title: Row(
        children: <Widget>[
          Container(
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenFactory(Home())));
                }, // handle your image tap here
                child: Image(
                    width: 100.0,
                    height: 100.0,
                    image: _themeChanger.getTheme().primaryColor == Color(0xff182e42)
                        ? AssetImage(kaleris_white_logo)
                        : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? AssetImage(kaleris_black_logo) : null)),
              ),
          SizedBox(
            width: 20.0,
          ),
          Text("User - " + env.userProfile.id, style: Theme.of(context).textTheme.bodyText1),
          SizedBox(
            width: 20.0,
          ),
          Text(facilityText, style: Theme.of(context).textTheme.bodyText1),
          SizedBox(
            width: 20.0,
          ),
         // env.flavor.index != 2 ? Expanded(child: Text(env.flavor.toString() ?? "", style: Theme.of(context).textTheme.bodyText1)) : Container(),
          SizedBox(
            width: 20.0,
          ),
          Text(
            '',
            style: TextStyle(fontSize: 20.0, color: Color(0xff3e8aeb)),
          ),
          SizedBox(
            width: 20.0,
          ),
        ],
      ),
      actions: <Widget>[
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: BlocBuilder<AppBarBloc, AppBarState>(
                cubit: env.appbarbloc,
                builder: (context, state) {
                  return Center(child: Text('Last Updated: ${lastUpdateText}', style: Theme.of(context).textTheme.body1));
                })),
        Container(
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            child: isDataConnected ? Container(width: 30, height: 30, child: WidgetOnline) : Container(width: 24, height: 24, child: WidgetOffline),
          ),
        ),
        AppBarDropdown(),
      ],
    );
  }
}
