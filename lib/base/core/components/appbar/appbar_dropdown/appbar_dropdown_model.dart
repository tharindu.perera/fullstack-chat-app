class AppbarDropdownModel {
  const AppbarDropdownModel({this.itemKey, this.itemValue});

  final String itemKey;
  final String itemValue;
}
