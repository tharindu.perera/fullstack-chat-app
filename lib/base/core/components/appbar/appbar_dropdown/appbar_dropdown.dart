import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mbase/base/app/authentication/bloc/authentication_bloc.dart';
import 'package:mbase/base/app/authentication/bloc/authentication_event.dart';
import 'package:mbase/base/core/components/appbar/appbar_dropdown/appbar_dropdown_model.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/config/global_theme/theme_file.dart';
import 'package:provider/provider.dart';

const List<AppbarDropdownModel> menulist = <AppbarDropdownModel>[
  const AppbarDropdownModel(itemKey: "01", itemValue: "DarK Mode"),
  const AppbarDropdownModel(itemKey: "02", itemValue: "Light Mode"),
  const AppbarDropdownModel(itemKey: "03", itemValue: "Sign out"),
];

class AppBarDropdown extends StatefulWidget {
  @override
  _AppBarDropdownState createState() => _AppBarDropdownState();
}

class _AppBarDropdownState extends State<AppBarDropdown> {
  @override
  void dispose() {
    super.dispose();
  }

  onSignout() {
//    BlocProvider.of<AppBarBloc>(keyGlobals.globalKey.currentContext).add(LogoutEvent(page: "logout"));
    Navigator.popUntil(context, ModalRoute.withName('/'));
    BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    GlobalTheme globalTheme = GlobalTheme();

    return Column(mainAxisSize: MainAxisSize.min,mainAxisAlignment: MainAxisAlignment.center,
        children: [
      PopupMenuButton<String>(
        icon: CircleAvatar(
            radius: 30.0,
            backgroundColor: Colors.white,
            child: Icon(
              Icons.person,
              color: Colors.black,
            )),
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          PopupMenuItem<String>(
            value: "value 02",
            child: Text("Light", style: Theme.of(context).textTheme.body1),
          ),
          PopupMenuItem<String>(
            value: "value 01",
            child: Text("Dark", style: Theme.of(context).textTheme.body1),
          ),
          PopupMenuItem<String>(
            value: "value 03",
            child: Text("Sign out", style: Theme.of(context).textTheme.body1),
          ),
        ],
        onSelected: (String value) {
          value == "value 03" ? onSignout() : value == "value 02" ? _themeChanger.setTheme(globalTheme.lightTheme) : value == "value 01" ? _themeChanger.setTheme(globalTheme.darkTheme) : null;
        },
      ),
    ]);
  }
}
