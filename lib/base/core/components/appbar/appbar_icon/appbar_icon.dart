import 'package:flutter/material.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:provider/provider.dart';

class AppBarIcon extends StatelessWidget {
  AppBarIcon({
    @required this.icon,
    this.onPress,
    this.tooltip,
  });

  final Function onPress;
  final IconData icon;
  final String tooltip;

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    return IconButton(
      icon: Icon(icon,  color: Colors.black38),
      tooltip: tooltip,
      //color:colour,
      onPressed: onPress,
    );
  }
}
