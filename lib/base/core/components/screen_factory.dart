import 'package:flutter/material.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/main_app_bar.dart';
import 'package:mbase/base/core/components/appdrawer/app_drawer.dart';

class ScreenFactory extends StatelessWidget {
 final Widget _widget;

  ScreenFactory(this._widget);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar(),
      drawer: AppDrawer(),
      body: _widget,
    );
  }
}
