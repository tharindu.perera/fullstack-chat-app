import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomDropdownMap extends StatelessWidget {
  CustomDropdownMap(
      {@required this.itemList,
      @required this.SelectedValue,
      this.Ontap,
      this.hintText});

  final LinkedHashMap<String, String> itemList;
  final Function Ontap;
  final String SelectedValue;
  final String hintText;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);

    return Container(
      width: ScreenUtil().setWidth(213),
      height: ScreenUtil().setHeight(48),
      decoration: ShapeDecoration(
        color: Theme.of(context).canvasColor,
        shape: RoundedRectangleBorder(
          side: BorderSide(
              width: 1.0, style: BorderStyle.solid, color:
          Color(0xffc0c5cc)),
          borderRadius: BorderRadius.all(Radius.circular(4.0)),
        ),
      ),
      child: DropdownButtonHideUnderline(
        child: ButtonTheme(
          alignedDropdown: true,
          child: DropdownButton<String>(
              hint: Text(
                hintText,
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Roboto',
                    fontStyle: FontStyle.normal,
                    fontSize:
                        ScreenUtil().setSp(14, allowFontScalingSelf: true)),
              ),
              items: itemList
                  .map((value, description) {
                    return MapEntry(
                        description,
                        DropdownMenuItem<String>(
                          value: value,
                          child: Text(description),
                        ));
                  })
                  .values
                  .toList(),
              value: SelectedValue,
              onChanged: Ontap),
        ),
      ),
    );
  }
}
