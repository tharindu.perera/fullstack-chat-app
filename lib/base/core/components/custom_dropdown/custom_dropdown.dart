import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:provider/provider.dart';

class CustomDropdown extends StatelessWidget {
  CustomDropdown({@required this.itemList, this.SelectedValue, this.Ontap, this.error: false, this.dropdownWidth = 213.0, this.hintText})
  :assert(
  hintText != null,
  'A non-null String must be provided to a hintText.',
  );

  final List<String> itemList;
  final Function Ontap;
  final String SelectedValue;
  final String hintText;
  final double dropdownWidth;
  bool error = false;
  ThemeChanger _themeChanger;

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);

//    ScreenUtil.init(context, width: 1024, height: 768);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: dropdownWidth,
          height: ScreenUtil().setHeight(48),
          decoration: ShapeDecoration(
            color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
            //color: Theme.of(context).canvasColor,
            shape: RoundedRectangleBorder(
              side: BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            ),
          ),
          child: DropdownButtonHideUnderline(
            child: ButtonTheme(
              // buttonColor: Colors.amberAccent,
              alignedDropdown: true,
              child: DropdownButton<String>(
                  isExpanded: true,
                  hint: Text(
                    hintText,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xfff5f5f5) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Roboto',
                        fontStyle: FontStyle.normal,
                        fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true)),
                  ),
                  items: itemList.map((String value) {
                    return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value,
                            overflow: TextOverflow.visible,
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 15,
                                fontStyle: FontStyle.normal,
                                letterSpacing: ScreenUtil().setWidth(0.25),
                                color: Theme.of(context).textTheme.bodyText1.color)));
                  }).toList(),
                  value: SelectedValue,
                  onChanged: Ontap),
            ),
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(5),
        ),
        error
            ? Text('This field is mandatory',
                style: TextStyle(fontSize: ScreenUtil().setSp(12, allowFontScalingSelf: true), fontFamily: 'Roboto', fontStyle: FontStyle.normal, letterSpacing: 0.4, color: Color(0xfff34336)))
            : SizedBox(
                width: 0,
                height: 0,
              )
      ],
    );
  }
}
