import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/location_model.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:provider/provider.dart';

class CustomDropdownGeneric<T extends Drop> extends StatelessWidget {
  CustomDropdownGeneric({@required this.itemList, this.SelectedValue, this.Ontap, this.error: false, this.dropdownWidth = 213.0, this.hintText});

  final List<T> itemList;
  final Function Ontap;
  final T SelectedValue;
  final String hintText;
  final double dropdownWidth;
  bool error = false;
  ThemeChanger _themeChanger;

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);

    ScreenUtil.init(context, width: 1024, height: 768);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: dropdownWidth,
          height: ScreenUtil().setHeight(48),
          decoration: ShapeDecoration(
            color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff172636) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xfff5f5f5) : null,
            //color: Theme.of(context).canvasColor,
            shape: RoundedRectangleBorder(
              side: BorderSide(width: 1.0, style: BorderStyle.solid, color: Color(0xffc0c5cc)),
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            ),
          ),
          child: DropdownButtonHideUnderline(
            child: ButtonTheme(
              // buttonColor: Colors.amberAccent,
              alignedDropdown: true,
              child: DropdownButton<T>(
                  isExpanded: true,
                  hint: Text(
                    hintText,
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                        color: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xfff5f5f5) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xff172636) : null,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Roboto',
                        fontStyle: FontStyle.normal,
                        fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true)),
                  ),
                  items: itemList.map((T value) {
                    return DropdownMenuItem<T>(
                        value: value,
                        child: Text(value.dropDownDisplayName,
                            overflow: TextOverflow.visible,
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 14,
                                fontStyle: FontStyle.normal,
                                letterSpacing: ScreenUtil().setWidth(0.25),
                                color: Theme.of(context).textTheme.bodyText1.color)));
                  }).toList(),
                  value: SelectedValue,
                  onChanged: Ontap),
            ),
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(5),
        ),
        error
            ? Text('This field is mandatory',
                style: TextStyle(fontSize: ScreenUtil().setSp(12, allowFontScalingSelf: true), fontFamily: 'Roboto', fontStyle: FontStyle.normal, letterSpacing: 0.4, color: Color(0xfff34336)))
            : SizedBox(
                width: 0,
                height: 0,
              )
      ],
    );
  }
}
