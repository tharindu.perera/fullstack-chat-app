import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DrawerItem extends StatelessWidget {
  DrawerItem({@required this.title, @required this.ontap, this.imagePath});

  final Function ontap;
  final String title;
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768);
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(23.9),
          vertical: ScreenUtil().setHeight(24)),
      decoration: BoxDecoration(),
      child: InkWell(
        splashColor: Color(0xFFf9f9f9),
        onTap: ontap,
        child: Container(
          //  height: 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Image.asset(
                    imagePath,
                    width: ScreenUtil().setWidth(24),
                    height: ScreenUtil().setHeight(24),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(28),
                  ),
                  Text(title,
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil()
                              .setSp(16, allowFontScalingSelf: true),
                          letterSpacing: ScreenUtil().setWidth(0.5)))
                ],
              ),
              // Icon(Icons.arrow_right)
            ],
          ),
        ),
      ),
    );
  }
}
