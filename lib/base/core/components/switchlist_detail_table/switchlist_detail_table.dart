//import 'package:flutter/material.dart';
//import 'package:flutter_screenutil/flutter_screenutil.dart';
//import 'package:intl/intl.dart';
//import 'package:provider/provider.dart';
//import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
//import 'package:mbase/base/core/config/global_theme/theme.dart';
//import 'package:mbase/base/modules/switchlist/edit_switchlist/ui/edit_switchlists.dart';
//import 'package:mbase/base/modules/switchlist/switchlist_detail/data/SwitchListPayload.dart';
//import 'package:mbase/base/modules/switchlist/switchlist_detail/model/switchlist_detail_model.dart';
//
//class SwitchListDetailTable extends StatefulWidget {
//  @override
//  _SwitchListDataTable createState() => _SwitchListDataTable();
//}
//
//class _SwitchListDataTable extends State<SwitchListDetailTable> {
//  List<SwitchListDetailModel> listData;
//  List<SwitchListEditPayload> selectedRow;
//
//  @override
//  void initState() {
//    super.initState();
//    listData = SwitchListDetailModel.getData();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    var formatter = new DateFormat('yyyy-MM-dd');
//    ScreenUtil.init(context, width: 1024, height: 768);
//    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
//
//    return SingleChildScrollView(
//        scrollDirection: Axis.vertical,
//        child: SingleChildScrollView(
//          scrollDirection: Axis.horizontal,
//          child: Container(
//            child: CustomDataTable(
//              rowColor: _themeChanger.getTheme().primaryColor ==
//                      Color(0xff182e42)
//                  ? Color(0xff1d2e40)
//                  : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)
//                      ? Color(0xffffffff)
//                      : null,
//              columnColor: Color(0xff274060),
//              checkboxBorderColor: _themeChanger.getTheme().primaryColor ==
//                      Color(0xff182e42)
//                  ? Colors.white
//                  : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5)
//                      ? Color.fromRGBO(0, 0, 0, 0.54)
//                      : null,
//              dataRowHeight: 100.0,
//              columns: [
//                CustomDataColumn(
//                    label: Text(
//                      'EQUIPMENT',
//                      style: TextStyle(
//                        color: Colors.white,
//                        fontFamily: 'Roboto',
//                        fontSize: ScreenUtil().setHeight(14),
//                        fontStyle: FontStyle.normal,
//                        fontWeight: FontWeight.w500,
//                        letterSpacing: ScreenUtil().setWidth(1.43),
//                      ),
//                    ),
//                    numeric: false,
//                    tooltip: 'EQUIPMENT'),
//                CustomDataColumn(
//                    label: Text('SWITCHED',
//                        style: TextStyle(
//                          color: Colors.white,
//                          fontFamily: 'Roboto',
//                          fontSize: ScreenUtil().setHeight(14),
//                          fontStyle: FontStyle.normal,
//                          fontWeight: FontWeight.w500,
//                          letterSpacing: ScreenUtil().setWidth(1.43),
//                        )),
//                    numeric: false,
//                    tooltip: 'SWITCHED'),
//                CustomDataColumn(
//                    label: Text('STATUS',
//                        style: TextStyle(
//                          color: Colors.white,
//                          fontFamily: 'Roboto',
//                          fontSize: ScreenUtil().setHeight(14),
//                          fontStyle: FontStyle.normal,
//                          fontWeight: FontWeight.w500,
//                          letterSpacing: ScreenUtil().setWidth(1.43),
//                        )),
//                    numeric: false,
//                    tooltip: 'STATUS'),
//                CustomDataColumn(
//                    label: Text('FROM',
//                        style: TextStyle(
//                          color: Colors.white,
//                          fontFamily: 'Roboto',
//                          fontSize: ScreenUtil().setHeight(14),
//                          fontStyle: FontStyle.normal,
//                          fontWeight: FontWeight.w500,
//                          letterSpacing: ScreenUtil().setWidth(1.43),
//                        )),
//                    numeric: false,
//                    tooltip: 'FROM'),
//                CustomDataColumn(
//                    label: Text('TO',
//                        style: TextStyle(
//                          color: Colors.white,
//                          fontFamily: 'Roboto',
//                          fontSize: ScreenUtil().setHeight(14),
//                          fontStyle: FontStyle.normal,
//                          fontWeight: FontWeight.w500,
//                          letterSpacing: ScreenUtil().setWidth(1.43),
//                        )),
//                    numeric: false,
//                    tooltip: 'TO'),
//                CustomDataColumn(
//                  label: Text(''),
//                ),
//              ],
//              rows: listData
//                  .map((listData) => CustomDataRow(cells: [
//                        CustomDataCell(
//                            //Text(listData.Equipment)
//                            ListTile(
//                          leading: Text(listData.Equipment,
//                              style: TextStyle(
//                                color: Theme.of(context).textTheme.body1.color,
//                                fontFamily: 'Roboto',
//                                fontSize: ScreenUtil().setHeight(16),
//                                fontStyle: FontStyle.normal,
//                                fontWeight: FontWeight.normal,
//                                letterSpacing: ScreenUtil().setWidth(0.5),
//                              )),
//                          trailing: Icon(Icons.message),
//                        )),
//                        CustomDataCell(Text(formatter.format(DateTime.now()),
//                            style: TextStyle(
//                              color: Theme.of(context).textTheme.body1.color,
//                              fontFamily: 'Roboto',
//                              fontSize: ScreenUtil().setHeight(16),
//                              fontStyle: FontStyle.normal,
//                              fontWeight: FontWeight.normal,
//                              letterSpacing: ScreenUtil().setWidth(0.5),
//                            ))),
//                        CustomDataCell(Text(listData.Status,
//                            style: TextStyle(
//                              color: Theme.of(context).textTheme.body1.color,
//                              fontFamily: 'Roboto',
//                              fontSize: ScreenUtil().setHeight(16),
//                              fontStyle: FontStyle.normal,
//                              fontWeight: FontWeight.normal,
//                              letterSpacing: ScreenUtil().setWidth(0.5),
//                            ))),
//                        CustomDataCell(ListTile(
//                          title: Text(listData.From,
//                              style: TextStyle(
//                                color: Theme.of(context).textTheme.body1.color,
//                                fontFamily: 'Roboto',
//                                fontSize: ScreenUtil().setHeight(16),
//                                fontStyle: FontStyle.normal,
//                                fontWeight: FontWeight.normal,
//                                letterSpacing: ScreenUtil().setWidth(0.5),
//                              )),
//                          subtitle: Text(listData.From_Track_Spot,
//                              style: TextStyle(
//                                color: Theme.of(context).textTheme.body1.color,
//                                fontFamily: 'Roboto',
//                                fontSize: ScreenUtil().setHeight(16),
//                                fontStyle: FontStyle.normal,
//                                fontWeight: FontWeight.normal,
//                                letterSpacing: ScreenUtil().setWidth(0.5),
//                              )),
//                        )),
//                        CustomDataCell(ListTile(
//                          title: Text(listData.To,
//                              style: TextStyle(
//                                color: Theme.of(context).textTheme.body1.color,
//                                fontFamily: 'Roboto',
//                                fontSize: ScreenUtil().setHeight(16),
//                                fontStyle: FontStyle.normal,
//                                fontWeight: FontWeight.normal,
//                                letterSpacing: ScreenUtil().setWidth(0.5),
//                              )),
//                          subtitle: Text(listData.To_Track_Spot,
//                              style: TextStyle(
//                                color: Theme.of(context).textTheme.body1.color,
//                                fontFamily: 'Roboto',
//                                fontSize: ScreenUtil().setHeight(16),
//                                fontStyle: FontStyle.normal,
//                                fontWeight: FontWeight.normal,
//                                letterSpacing: ScreenUtil().setWidth(0.5),
//                              )),
//                        )),
//                        CustomDataCell(ListTile(
//                            leading: FlatButton(
//                                shape: RoundedRectangleBorder(
//                                    side: BorderSide(
//                                        color: Color.fromRGBO(0, 0, 0, 0.12),
//                                        width: 1,
//                                        style: BorderStyle.solid)),
//                                onPressed: () {
//                                  Navigator.push(
//                                      context,
//                                      MaterialPageRoute(
//                                          builder: (context) =>
//                                              EditSwitchList()));
//                                },
//                                child: Text('Edit',
//                                    style: TextStyle(
//                                        fontFamily: 'Roboto',
//                                        fontSize: ScreenUtil().setHeight(14),
//                                        fontStyle: FontStyle.normal,
//                                        fontWeight: FontWeight.w500,
//                                        letterSpacing:
//                                            ScreenUtil().setWidth(1.25),
//                                        color: Color(0xFF3e8aeb))),
//                                color: Colors.white,
//                                //textColor: Colors.white,
//                                disabledColor: Colors.grey,
//                                disabledTextColor: Colors.black,
//                                splashColor: Color(0xFF3e8aeb)),
//                            title: FlatButton(
//                              onPressed: listData.Complete
//                                  ? null
//                                  : () {
//                                      print("demo");
//                                    },
//                              child: Text('Complete',
//                                  style: TextStyle(
//                                      fontFamily: 'Roboto',
//                                      fontSize: ScreenUtil().setHeight(14),
//                                      fontStyle: FontStyle.normal,
//                                      fontWeight: FontWeight.w500,
//                                      letterSpacing:
//                                          ScreenUtil().setWidth(1.25),
//                                      color: Colors.white)),
//                              color: Color(0xFF3e8aeb),
//                              textColor: Colors.white,
//                              disabledColor: Colors.grey,
//                              disabledTextColor: Colors.black,
//                              splashColor: Color(0xFF3e8aeb),
//                            )))
//                      ]))
//                  .toList(),
//            ),
//          ),
//        ));
//  }
//}
