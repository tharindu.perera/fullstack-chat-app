import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/common_base/model/switchlist.dart';
import 'package:mbase/base/core/components/custom_data_table/custom_data_table.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_data_table_souce.dart';
import 'package:mbase/base/core/components/custom_paginated_datatable/custom_paginated_datatable.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/inspection/repository/inspection_details_repository.dart';
import 'package:mbase/base/modules/inspection/ui/inspection_details.dart';
import 'package:provider/provider.dart';

class InspectionTable extends StatefulWidget {
  final List<InspectionRailcarModel> listData;

  const InspectionTable({Key key, this.listData}) : super(key: key);

  @override
  _InspectionTableState createState() => _InspectionTableState();
}

class _InspectionTableState extends State<InspectionTable> {
  List<InspectionRailcarModel> listData;
  List<Map<String, dynamic>> itemList = [];
  InspectionRailcarModel inspectionRailcarModel;
  int _sortColumnIndex = 0;
  bool _sortAscending = true;

  var formatter = new DateFormat('yyyy-MM-dd hh:mm');

  @override
  void initState() {
    super.initState();
    listData = widget.listData;
  }

  _loadDetailsPage(InspectionRailcarModel inspectionRailcarModel) async {
    inspectionRailcarModel.inspectionTemplate = await InspectionDetailsRepo().fetchInspectionTemplateByID(inspectionRailcarModel.inspectionTemplateId);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => InspectionDetails(
                  inspectionRailcarModel: inspectionRailcarModel,
                ))).then((value) =>  setState(() {}));
  }

  onSortColumn(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        listData.sort((a, b) => a.equipmentId.compareTo(b.equipmentId));
      } else {
        listData.sort((a, b) => b.equipmentId.compareTo(a.equipmentId));
      }
    } else if (columnIndex == 1) {
      if (ascending) {
        listData.sort((a, b) => a.type.compareTo(b.type));
      } else {
        listData.sort((a, b) => b.type.compareTo(a.type));
      }
    } else if (columnIndex == 2) {
      if (ascending) {
        listData.sort((a, b) => a.product_id.compareTo(b.product_id));
      } else {
        listData.sort((a, b) => b.product_id.compareTo(a.product_id));
      }
    } else if (columnIndex == 3) {
      if (ascending) {
        listData.sort((a, b) => a.createdDt.compareTo(b.createdDt));
      } else {
        listData.sort((a, b) => b.createdDt.compareTo(a.createdDt));
      }
    } else if (columnIndex == 4) {
      if (ascending) {
        listData.sort((a, b) => a.inspectionUser.compareTo(b.inspectionUser));
      } else {
        listData.sort((a, b) => b.inspectionUser.compareTo(a.inspectionUser));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);
    ScreenUtil.init(context, width: 1024, height: 768);

    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            width: ScreenUtil().setWidth(928),
            child: CustomPaginatedDataTable(
              sortArrowColor: Colors.white,
              cardBackgroundColor: Theme.of(context).canvasColor,
              footerTextColor: Theme.of(context)
                  .textTheme
              // ignore: deprecated_member_use
                  .body1
                  .color,
              rowColor: _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Color(0xff1d2e40) : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color(0xffffffff) : null,

              columnColor: Color(0xff274060),
              checkboxBorderColor:
              _themeChanger.getTheme().primaryColor == Color(0xff182e42) ? Colors.white : _themeChanger.getTheme().primaryColor == Color(0xfff5f5f5) ? Color.fromRGBO(0, 0, 0, 0.54) : null,
              dataRowHeight: ScreenUtil().setHeight(47),
              // headingRowHeight: 60,
              horizontalMargin: ScreenUtil().setWidth(19),
              columnSpacing: ScreenUtil().setWidth(52),
              header: Text(''),
              columns: [
                CustomDataColumn(
                    label: Text('RAILCAR',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'RailCar ID'),
                CustomDataColumn(
                    label: Text('EQUIPMENT TYPE',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'Equipment Type'),
                CustomDataColumn(
                    label: Text('PRODUCT',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'Product'),
                CustomDataColumn(
                    label: Text('START DATE',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'Date'),
                CustomDataColumn(
                    label: Text('USER',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.43),
                        )),
                    onSort: (columnIndex, ascending) {
                      setState(() {
                        _sortColumnIndex = columnIndex;
                        _sortAscending = !_sortAscending;
                      });
                      onSortColumn(columnIndex, ascending);
                      //  listData = [];
                    },
                    numeric: false,
                    tooltip: 'User'),
              ],
              sortAscending: _sortAscending,
              sortColumnIndex: _sortColumnIndex,
              source: InspectionDataSource(context: context, listData: listData),

              rowsPerPage: 5,

            ),
          ),
        ));
  }
}


class InspectionDataSource extends CustomDataTableSource {
  final DateTime now = DateTime.now();
  final DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm');
  final List <InspectionRailcarModel> listData;
 // final List<NewSwitchList> switchListData;
  int _selectedCount = 0;
  BuildContext context;

  InspectionDataSource({this.listData, this.context});

  @override
  CustomDataRow getRow(int index) {
    InspectionRailcarModel cellVal = listData[index];
    return CustomDataRow.byIndex(index: index, cells: [
    CustomDataCell(Text(cellVal.equipment ?? "",
        style: TextStyle(
          color: Theme.of(context).textTheme.bodyText1.color,
          fontFamily: 'Roboto',
          fontSize: ScreenUtil().setHeight(16),
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          letterSpacing: ScreenUtil().setWidth(0.5),
        ))),
    CustomDataCell(Text(cellVal.type ?? "",
    style: TextStyle(
    color: Theme.of(context).textTheme.body1.color,
    fontFamily: 'Roboto',
    fontSize: ScreenUtil().setHeight(16),
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    letterSpacing: ScreenUtil().setWidth(0.5),
    ))),
    CustomDataCell(Text(cellVal.product_id ?? "",
    style: TextStyle(
    color: Theme.of(context).textTheme.bodyText1.color,
    fontFamily: 'Roboto',
    fontSize: ScreenUtil().setHeight(16),
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    letterSpacing: ScreenUtil().setWidth(0.5),
    ))),
    CustomDataCell(Text(formatter.format(cellVal.createdDt).toString() ?? "",
    style: TextStyle(
    color: Theme.of(context).textTheme.bodyText1.color,
    fontFamily: 'Roboto',
    fontSize: ScreenUtil().setHeight(16),
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    letterSpacing: ScreenUtil().setWidth(0.5),
    ))),
    CustomDataCell(ListTile(
    leading: Text(cellVal.inspectionUser ?? "",
    style: TextStyle(
    color: Theme.of(context).textTheme.bodyText1.color,
    fontFamily: 'Roboto',
    fontSize: ScreenUtil().setHeight(16),
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    letterSpacing: ScreenUtil().setWidth(0.5),
    )),
    trailing: GestureDetector(
  //  onTap: () => _loadDetailsPage(listData),
    child: Icon(
    Icons.arrow_right,
    color: Color(0xFF223f7a),
    ),
    ),
    ))
        // SwitchlistNavigatorButton(
        // status: cellVal.Status,
        // switchListModel: cellVal)

    ]);
  }

  @override
  // TODO: implement isRowCountApproximate
  bool get isRowCountApproximate => false;

  @override
  // TODO: implement rowCount
  int get rowCount => (listData.length ?? 0).ceil();

  @override
  // TODO: implement selectedRowCount
  int get selectedRowCount => _selectedCount;
}
