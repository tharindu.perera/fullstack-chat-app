import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomTabBarNav extends StatefulWidget {
  List<String> tabitem;
  List<Widget> tabbody;

  CustomTabBarNav({this.tabbody, this.tabitem});

  @override
  CustomTabBarState createState() => CustomTabBarState();
}

class CustomTabBarState extends State<CustomTabBarNav>
    with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
     super.initState();

    controller = new TabController(length: widget.tabitem.length, vsync: this);
  }

  @override
  void dispose() {
     controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768, allowFontScaling: true);

    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: TabBar(
              controller: controller,
              isScrollable: true,
              tabs: widget.tabitem.map((tabitem) {
                return Tab(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                        minWidth: ScreenUtil().setWidth(41),
                        minHeight: ScreenUtil().setHeight(24)),
                    child: Text(
                      tabitem,
                      style: TextStyle(
                          fontSize: ScreenUtil().setHeight(20),
                          fontFamily: 'Roboto',
                          letterSpacing: ScreenUtil().setWidth(0.25),
                          color: Theme.of(context).textTheme.body1.color),
                    ),
                  ),
                );
              }).toList()),
        ),
        Expanded(
          child: TabBarView(controller: controller, children: widget.tabbody),
        )
      ],
    );
  }
}
