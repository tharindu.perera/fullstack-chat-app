class CustomTabBarItem {
  CustomTabBarItem({this.title});

  final String title;
  CustomTabBarItem.fromJson(Map<String, dynamic> value)
      : title = value["title"];
}
