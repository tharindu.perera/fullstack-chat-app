import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/util/formatter.dart';
import 'package:provider/provider.dart';

enum DialogAction { yes, abort }

class CustomDialog {
  static Future<List<Object>> cstmDialog(BuildContext context, String type, String title, String body,) async {
    final _textController = TextEditingController();

    DialogAction action = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

//      ****  Load Note Dialog ***** //
          Widget addLoadAlert() {
            return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: AlertDialog(
                title: Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Roboto',
                      letterSpacing: ScreenUtil().setWidth(0.5),
                      color: Theme.of(context).textTheme.body1.color,
                      fontSize:
                          ScreenUtil().setSp(34, allowFontScalingSelf: true)),
                ),
                content: Container(
                    width: ScreenUtil().setWidth(609),
                    height: ScreenUtil().setHeight(399),
                    child: Column(
                      children: <Widget>[
                        Container(
                            child: ListTile(
                                leading: RichText(
                          text: TextSpan(
                            text: 'Railcar:',
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'Roboto',
                                letterSpacing: ScreenUtil().setWidth(0.5),
                                color: Theme.of(context).textTheme.body1.color,
                                fontSize: ScreenUtil()
                                    .setSp(16, allowFontScalingSelf: true)),
                            children: <InlineSpan>[
                              WidgetSpan(
                                  child: Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(16)),
                                child: Text(body,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                        fontFamily: 'Roboto',
                                        letterSpacing:
                                            ScreenUtil().setWidth(0.25),
                                        color: Theme.of(context)
                                            .textTheme
                                            .body1
                                            .color,
                                        fontSize: ScreenUtil().setSp(20,
                                            allowFontScalingSelf: true))),
                              )),
                            ],
                          ),
                        )
                            )),
                        SizedBox(
                          height: ScreenUtil().setHeight(16),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(529),
                          height: ScreenUtil().setHeight(163),
                          color: Theme.of(context).scaffoldBackgroundColor,
                          child: TextField(
                            decoration: InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide:
                                        new BorderSide(color: Colors.teal)),
                                hintText: 'Enter text here ...',
                                hintStyle: TextStyle(
                                    fontSize: ScreenUtil()
                                        .setSp(14, allowFontScalingSelf: true),
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.italic,
                                    letterSpacing:
                                    ScreenUtil().setWidth(0.25))),
                            controller: _textController,
                            inputFormatters: [UpperCaseFormatter()],
                            style: TextStyle(color: Colors.black),
                            maxLines: 10,
                            keyboardType: TextInputType.multiline,
                          ),
                        )
                      ],
                    )),
                actions: <Widget>[
                  Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(12),
                        vertical: ScreenUtil().setHeight(24),
                      ),
                      child: Container(
                          child: GestureDetector(
                            onTap: () =>
                                Navigator.of(context).pop(DialogAction.abort),
                            child: RichText(
                              text: TextSpan(
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      // ignore: deprecated_member_use
                                      .body1,
                                  children: <InlineSpan>[
                                    TextSpan(
                                        text: 'CANCEL',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setHeight(14),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing:
                                            ScreenUtil().setWidth(1.25),
                                        color: Color(0xFF3e8aeb)))
                              ]),
                        ),
                      ))),
                  Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(12),
                          vertical: ScreenUtil().setHeight(24)),
                      child: Container(
                        width: ScreenUtil().setWidth(180),
                        height: ScreenUtil().setHeight(48),
                        child: FlatButton(
                            onPressed: () =>
                                Navigator.of(context).pop(DialogAction.yes),
                            child: Text(
                              'SUBMIT NOTE',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  color: Colors.white),
                            ),
                            color: Color(0xFF508be4),
                            textColor: Colors.white,
                            disabledColor: Color(0xFF3e8aeb),
                            disabledTextColor: Colors.black,
                            splashColor: Color(0xFF3e8aeb)),
                      )),
                ],
              ),
            );
          }

          //      ****  UnLoad Edit Dialog ***** //
          Widget editUnLoadAlert() {
            return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: AlertDialog(
                title: Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Roboto',
                      letterSpacing: ScreenUtil().setWidth(0.5),
                      color: Theme
                          .of(context)
                          .textTheme
                          // ignore: deprecated_member_use
                          .body1
                          .color,
                      fontSize:
                          ScreenUtil().setSp(34, allowFontScalingSelf: true)),
                ),
                content: Container(
                    width: ScreenUtil().setWidth(609),
                    height: ScreenUtil().setHeight(200),
                    child: Column(
                      children: <Widget>[
                        Container(
                            child: ListTile(
                                leading: RichText(
                                  text: TextSpan(
                                    text: 'Enter unload amount for railcar:',
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontStyle: FontStyle.normal,
                                        fontFamily: 'Roboto',
                                        letterSpacing: ScreenUtil().setWidth(0.5),
                                        color: Theme
                                            .of(context)
                                            .textTheme
                                            // ignore: deprecated_member_use
                                            .body1
                                            .color,
                                        fontSize: ScreenUtil()
                                            .setSp(16, allowFontScalingSelf: true)),
                                    children: <InlineSpan>[
                                      WidgetSpan(
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: ScreenUtil().setWidth(16)),
                                            child: Text(body,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontStyle: FontStyle.normal,
                                                    fontFamily: 'Roboto',
                                                    letterSpacing:
                                                    ScreenUtil().setWidth(0.25),
                                                    color: Theme
                                                        .of(context)
                                                        .textTheme
                                                        // ignore: deprecated_member_use
                                                        .body1
                                                        .color,
                                                    fontSize: ScreenUtil().setSp(20,
                                                        allowFontScalingSelf: true))),
                                          )),
                                      //     TextSpan(text: body, style: TextStyle(fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ))),
                        SizedBox(
                          height: ScreenUtil().setHeight(48),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(300),
                          height: ScreenUtil().setHeight(48),
                          color: Theme.of(context).scaffoldBackgroundColor,
                          child: TextField(

                            decoration: InputDecoration(

                                border: new OutlineInputBorder(
                                    borderSide:
                                    new BorderSide(color: Colors.teal)),
                                hintText: 'Unload Amount',
                                hintStyle: TextStyle(
                                  color: Theme.of(context).textTheme.bodyText1.color,
                                    fontSize: ScreenUtil()
                                        .setSp(14, allowFontScalingSelf: true),
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing:
                                    ScreenUtil().setWidth(0.25))),
                            controller: _textController,
                            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                            style: TextStyle(color:Theme.of(context).textTheme.bodyText1.color ),
                          ),
                        )
                      ],
                    )),
                actions: <Widget>[
                  Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(12),
                        vertical: ScreenUtil().setHeight(24),
                      ),
                      child: Container(
                          child: GestureDetector(
                            onTap: () =>
                                Navigator.of(context).pop(DialogAction.abort),
                            child: RichText(
                              text: TextSpan(
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      // ignore: deprecated_member_use
                                      .body1,
                                  children: <InlineSpan>[
                                    TextSpan(
                                        text: 'CANCEL',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setHeight(14),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing:
                                            ScreenUtil().setWidth(1.25),
                                        color: Color(0xFF3e8aeb)))
                              ]),
                        ),
                      ))),
                  Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(12),
                          vertical: ScreenUtil().setHeight(24)),
                      child: Container(
                        width: ScreenUtil().setWidth(180),
                        height: ScreenUtil().setHeight(48),
                        child: FlatButton(
                            onPressed: () =>
                                Navigator.of(context).pop(DialogAction.yes),
                            child: Text(
                              'SUBMIT UNLOAD AMOUNT',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  color: Colors.white),
                            ),
                            color: Color(0xFF508be4),
                            textColor: Colors.white,
                            disabledColor: Color(0xFF3e8aeb),
                            disabledTextColor: Colors.black,
                            splashColor: Color(0xFF3e8aeb)),
                      )),
                ],
              ),
            );
          }

          // *** Load Edit Dialog **** //
          Widget editLoadAlert() {
            return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: AlertDialog(
                title: Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Roboto',
                      letterSpacing: ScreenUtil().setWidth(0.5),
                      color: Theme
                          .of(context)
                          .textTheme
                      // ignore: deprecated_member_use
                          .body1
                          .color,
                      fontSize:
                      ScreenUtil().setSp(34, allowFontScalingSelf: true)),
                ),
                content: Container(
                    width: ScreenUtil().setWidth(609),
                    height: ScreenUtil().setHeight(200),
                    child: Column(
                      children: <Widget>[
                        Container(
                            child: ListTile(
                                leading: RichText(
                                  text: TextSpan(
                                    text: 'Enter load amount for railcar:',
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontStyle: FontStyle.normal,
                                        fontFamily: 'Roboto',
                                        letterSpacing: ScreenUtil().setWidth(0.5),
                                        color: Theme
                                            .of(context)
                                            .textTheme
                                        // ignore: deprecated_member_use
                                            .body1
                                            .color,
                                        fontSize: ScreenUtil()
                                            .setSp(16, allowFontScalingSelf: true)),
                                    children: <InlineSpan>[
                                      WidgetSpan(
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: ScreenUtil().setWidth(16)),
                                            child: Text(body,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontStyle: FontStyle.normal,
                                                    fontFamily: 'Roboto',
                                                    letterSpacing:
                                                    ScreenUtil().setWidth(0.25),
                                                    color: Theme
                                                        .of(context)
                                                        .textTheme
                                                    // ignore: deprecated_member_use
                                                        .body1
                                                        .color,
                                                    fontSize: ScreenUtil().setSp(20,
                                                        allowFontScalingSelf: true))),
                                          )),
                                      //     TextSpan(text: body, style: TextStyle(fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ))),
                        SizedBox(
                          height: ScreenUtil().setHeight(48),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(300),
                          height: ScreenUtil().setHeight(48),
                          color: Theme.of(context).scaffoldBackgroundColor,
                          child: TextField(
                            decoration: InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide:
                                    new BorderSide(color: Colors.teal)),
                                hintText: 'Load Amount',
                                hintStyle: TextStyle(
                                    fontSize: ScreenUtil()
                                        .setSp(14, allowFontScalingSelf: true),
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing:
                                    ScreenUtil().setWidth(0.25))),
                            controller: _textController,
                            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                            style: TextStyle(color: Colors.black),
                          ),
                        )
                      ],
                    )),
                actions: <Widget>[
                  Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(12),
                        vertical: ScreenUtil().setHeight(24),
                      ),
                      child: Container(
                          child: GestureDetector(
                            onTap: () =>
                                Navigator.of(context).pop(DialogAction.abort),
                            child: RichText(
                              text: TextSpan(
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                  // ignore: deprecated_member_use
                                      .body1,
                                  children: <InlineSpan>[
                                    TextSpan(
                                        text: 'CANCEL',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setHeight(14),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing:
                                            ScreenUtil().setWidth(1.25),
                                            color: Color(0xFF3e8aeb)))
                                  ]),
                            ),
                          ))),
                  Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(12),
                          vertical: ScreenUtil().setHeight(24)),
                      child: Container(
                        width: ScreenUtil().setWidth(180),
                        height: ScreenUtil().setHeight(48),
                        child: FlatButton(
                            onPressed: () =>
                                Navigator.of(context).pop(DialogAction.yes),
                            child: Text(
                              'SUBMIT LOAD AMOUNT',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  color: Colors.white),
                            ),
                            color: Color(0xFF508be4),
                            textColor: Colors.white,
                            disabledColor: Color(0xFF3e8aeb),
                            disabledTextColor: Colors.black,
                            splashColor: Color(0xFF3e8aeb)),
                      )),
                ],
              ),
            );
          }


          //      ****  Edit Note Dialog ***** //
          Widget editNoteAlert() {
            return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: AlertDialog(
                title: Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Roboto',
                      letterSpacing: ScreenUtil().setWidth(0.5),
                      color: Theme
                          .of(context)
                          .textTheme
                          // ignore: deprecated_member_use
                          .body1
                          .color,
                      fontSize:
                      ScreenUtil().setSp(34, allowFontScalingSelf: true)),
                ),
                content: Container(
                    width: ScreenUtil().setWidth(609),
                    height: ScreenUtil().setHeight(399),
                    child: Column(
                      children: <Widget>[
                        Container(
                            child: ListTile(
                                leading: RichText(
                                  text: TextSpan(
                                    text: 'Railcar:',
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontStyle: FontStyle.normal,
                                        fontFamily: 'Roboto',
                                        letterSpacing: ScreenUtil().setWidth(0.5),
                                        color: Theme
                                            .of(context)
                                            .textTheme
                                            // ignore: deprecated_member_use
                                            .body1
                                            .color,
                                        fontSize: ScreenUtil()
                                            .setSp(16, allowFontScalingSelf: true)),
                                    children: <InlineSpan>[
                                      WidgetSpan(
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: ScreenUtil().setWidth(16)),
                                            child: Text(body,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontStyle: FontStyle.normal,
                                                    fontFamily: 'Roboto',
                                                    letterSpacing:
                                                    ScreenUtil().setWidth(0.25),
                                                    color: Theme
                                                        .of(context)
                                                        .textTheme
                                                        // ignore: deprecated_member_use
                                                        .body1
                                                        .color,
                                                    fontSize: ScreenUtil().setSp(20,
                                                        allowFontScalingSelf: true))),
                                          )),
                                      //     TextSpan(text: body, style: TextStyle(fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ))),
                        SizedBox(
                          height: ScreenUtil().setHeight(16),
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              width: ScreenUtil().setWidth(377),
                              height: ScreenUtil().setHeight(140),
                              color: Theme.of(context).scaffoldBackgroundColor,
                              child: TextField(
                                decoration: InputDecoration(
                                    border: new OutlineInputBorder(
                                        borderSide:
                                            new BorderSide(color: Colors.teal)),
                                    hintText:
                                        'Lorem ipsum dolor sit ipsim.....',
                                    hintStyle: TextStyle(
                                        fontSize: ScreenUtil().setSp(14,
                                            allowFontScalingSelf: true),
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.normal,
                                        fontStyle: FontStyle.italic,
                                        letterSpacing:
                                        ScreenUtil().setWidth(0.25))),
                                controller: _textController,
                                inputFormatters: [UpperCaseFormatter()],
                                style: TextStyle(color: Colors.black),
                                maxLines: 10,
                                keyboardType: TextInputType.multiline,
                              ),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(28),
                            ),
                            Container(
                                width: ScreenUtil().setWidth(124),
                                height: ScreenUtil().setHeight(48),
                                child: RaisedButton.icon(
                                    onPressed: () {
                                      print("edit clicked");
                                    },
                                    icon: Icon(
                                      Icons.edit,
                                      size: 18,
                                      color: Color(0xFF4468be),
                                    ),
                                    label: Text(
                                      'Edit',
                                      style: TextStyle(
                                          fontStyle: FontStyle.normal,
                                          fontFamily: 'Roboto',
                                          fontSize: ScreenUtil().setHeight(14),
                                          letterSpacing:
                                              ScreenUtil().setWidth(1.25),
                                          color: Color(0xFF4468be)),
                                    )))
                          ],
                        )
                      ],
                    )),
                actions: <Widget>[
                  Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(12),
                        vertical: ScreenUtil().setHeight(24),
                      ),
                      child: Container(
                          child: GestureDetector(
                            onTap: () =>
                                Navigator.of(context).pop(DialogAction.abort),
                            child: RichText(
                              text: TextSpan(
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      // ignore: deprecated_member_use
                                      .body1,
                                  children: <InlineSpan>[
                                    TextSpan(
                                        text: 'CANCEL',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setHeight(14),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing:
                                            ScreenUtil().setWidth(1.25),
                                        color: Color(0xFF3e8aeb)))
                              ]),
                        ),
                      ))),
                  Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(12),
                          vertical: ScreenUtil().setHeight(24)),
                      child: Container(
                        width: ScreenUtil().setWidth(180),
                        height: ScreenUtil().setHeight(48),
                        child: FlatButton(
                            onPressed: () =>
                                Navigator.of(context).pop(DialogAction.yes),
                            child: Text(
                              'SUBMIT NOTE',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  color: Colors.white),
                            ),
                            color: Color(0xFF508be4),
                            textColor: Colors.white,
                            disabledColor: Color(0xFF3e8aeb),
                            disabledTextColor: Colors.black,
                            splashColor: Color(0xFF3e8aeb)),
                      )),
                ],
              ),
            );
          }

          // ** Add rail car unsaved changes dialog **//
          Widget addRailCarUnsavedChanges() {
            return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: AlertDialog(
                title: Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Roboto',
                      letterSpacing: ScreenUtil().setWidth(0.5),
                      color: Theme
                          .of(context)
                          .textTheme
                          // ignore: deprecated_member_use
                          .body1
                          .color,
                      fontSize:
                          ScreenUtil().setSp(34, allowFontScalingSelf: true)),
                ),
                content: Container(
                    width: ScreenUtil().setWidth(500),
                    height: ScreenUtil().setHeight(200),
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Expanded(
                            child: ListTile(
                            contentPadding: EdgeInsets.symmetric(vertical: 0.0),
                              leading:Text('Changes made so far will not be saved',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'Roboto',
                                                letterSpacing: ScreenUtil().setWidth(0.5),
                                                color: Theme
                                                    .of(context)
                                                    .textTheme
                                                    // ignore: deprecated_member_use
                                                    .body1
                                                    .color,
                                                fontSize: ScreenUtil()
                                                    .setSp(16, allowFontScalingSelf: true)),

                              ),
                            ),
                          ),
                        ),
                        Container(
                          child: ListTile(
                            leading:Text('Are you sure you want to exit without saving your changes ?',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontStyle: FontStyle.normal,
                                  fontFamily: 'Roboto',
                                  letterSpacing: ScreenUtil().setWidth(0.5),
                                  color: Theme
                                      .of(context)
                                      .textTheme
                                  // ignore: deprecated_member_use
                                      .body1
                                      .color,
                                  fontSize: ScreenUtil()
                                      .setSp(16, allowFontScalingSelf: true)),
                            ),
                            contentPadding: EdgeInsets.symmetric(vertical: 0.0),

                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(48),
                        ),
                      ],
                    )),
                actions: <Widget>[
                  Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(12),
                        vertical: ScreenUtil().setHeight(24),
                      ),
                      child: Container(
                          child: GestureDetector(
                            onTap: () =>
                                Navigator.of(context).pop(DialogAction.abort),
                            child: RichText(
                              text: TextSpan(
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      // ignore: deprecated_member_use
                                      .body1,
                                  children: <InlineSpan>[
                                    TextSpan(
                                        text: 'KEEP EDITING',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setHeight(14),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing:
                                            ScreenUtil().setWidth(1.25),
                                        color: Color(0xFF3e8aeb)))
                              ]),
                        ),
                      ))),
                  Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(12),
                          vertical: ScreenUtil().setHeight(24)),
                      child: Container(
                        width: ScreenUtil().setWidth(262),
                        height: ScreenUtil().setHeight(48),
                        child: FlatButton(
                            onPressed: () =>
                                Navigator.of(context).pop(DialogAction.yes),
                            child: Text(
                              'YES, LEAVE WITHOUT SAVING',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  color: Colors.white),
                            ),
                            color: Color(0xFF508be4),
                            textColor: Colors.white,
                            disabledColor: Color(0xFF3e8aeb),
                            disabledTextColor: Colors.black,
                            splashColor: Color(0xFF3e8aeb)),
                      )),
                ],
              ),
            );
          }

          //Request Successful
          Widget requestSuccessful() {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              title: Text(title),
              content: Text(body),
              actions: <Widget>[
                FlatButton(
                    onPressed: () =>
                        Navigator.of(context).pop(DialogAction.yes),
                    child: Text(
                      'Okay',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: ScreenUtil().setHeight(14),
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          letterSpacing: ScreenUtil().setWidth(1.25),
                          color: Colors.white),
                    ),
                    color: Color(0xFF508be4),
                    textColor: Colors.white,
                    disabledColor: Color(0xFF3e8aeb),
                    disabledTextColor: Colors.black,
                    splashColor: Color(0xFF3e8aeb))
              ],
            );
          }

          //Widget View Comment
          Widget viewComment() {
            return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: AlertDialog(
                title: Text(
                  "View Comment | $title",
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Roboto',
                      letterSpacing: ScreenUtil().setWidth(0.5),
                      color: Theme.of(context).textTheme.body1.color,
                      fontSize:
                          ScreenUtil().setSp(34, allowFontScalingSelf: true)),
                ),
                content: Container(
                    width: ScreenUtil().setWidth(609),
                    height: ScreenUtil().setHeight(399),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          //leading: Icon(Icons.account_circle,size:50, color: Color(0xFF508be4),),
                          title: Text('User ABC', style: TextStyle(
                              fontSize: ScreenUtil().setSp(16, allowFontScalingSelf: true),
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Roboto',
                              color: Theme
                                  .of(context)
                                  .textTheme
                                  // ignore: deprecated_member_use
                                  .body1
                                  .color

                          ),

                          ),

                          trailing: RichText(
                            text: TextSpan(
                              text: 'Last Updated:',
                              style: TextStyle(
                                fontStyle: FontStyle.normal,
                                fontSize: ScreenUtil()
                                    .setSp(14, allowFontScalingSelf: true),
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.normal,
                                color: Theme
                                    .of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color,

                              ),
                              children: <InlineSpan>[
                                TextSpan(text: ' 03/04/2019 12:24', style: TextStyle(
                                  fontStyle: FontStyle.normal,
                                  fontSize: ScreenUtil().setSp(14, allowFontScalingSelf: true),
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.normal,
                                  color: Theme
                                      .of(context)
                                      .textTheme
                                      // ignore: deprecated_member_use
                                      .body1
                                      .color,
                                )),
                              ],
                            ),
                          ),
                          subtitle: Container(
                            height: ScreenUtil().setHeight(300),
                            margin: EdgeInsets.symmetric(
                                horizontal: ScreenUtil().setWidth(14),
                                vertical: ScreenUtil().setHeight(14)),
                            decoration: BoxDecoration(

                                border: Border.all(color: Theme
                                    .of(context)
                                    .textTheme
                                    // ignore: deprecated_member_use
                                    .body1
                                    .color)
                            ),
                            child:
                            TextField(
                              maxLines: 20,
                              style: TextStyle(
                                fontSize: ScreenUtil()
                                    .setSp(14, allowFontScalingSelf: true),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                fontFamily: 'Roboto',
                              ),
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(left: 10, top: 10),
                                enabledBorder: const OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Color(0xffc0c5cc), width: 0.1),
                                ),
                                filled: true,
                                fillColor: Colors.white,
                                hintStyle: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.normal,
                                  letterSpacing: ScreenUtil().setWidth(0.25),
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
                actions: <Widget>[
                  Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(12),
                          vertical: ScreenUtil().setHeight(24)),
                      child: Container(
                        width: ScreenUtil().setWidth(180),
                        height: ScreenUtil().setHeight(48),
                        child: FlatButton(
                            onPressed: () =>
                                Navigator.of(context).pop(DialogAction.yes),
                            child: Text(
                              'CLOSE',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  color: Colors.white),
                            ),
                            color: Color(0xFF508be4),
                            textColor: Colors.white,
                            disabledColor: Color(0xFF3e8aeb),
                            disabledTextColor: Colors.black,
                            splashColor: Color(0xFF3e8aeb)),
                      )),
                ],
              ),
            );
          }

          //      ****  Confirm To Continue Dialog ***** //
          Widget confirmToContinue() {
            return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: AlertDialog(
                title: Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Roboto',
                      letterSpacing: ScreenUtil().setWidth(0.5),
                      color: Theme
                          .of(context)
                          .textTheme
                      // ignore: deprecated_member_use
                          .body1
                          .color,
                      fontSize:
                      ScreenUtil().setSp(34, allowFontScalingSelf: true)),
                ),

                content: Container(
                    width: ScreenUtil().setWidth(609),
                    height: ScreenUtil().setHeight(100),
                    child: Column(
                      children: <Widget>[
                        Container(
                            child: ListTile(
                                leading: RichText(
                                  text: TextSpan(
                                    children: <InlineSpan>[
                                      WidgetSpan(
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: ScreenUtil().setWidth(16)),
                                            child: SingleChildScrollView(
                                              child: SizedBox(width: 500,
                                                child: Text(body,
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.w500,
                                                        fontStyle: FontStyle.normal,
                                                        fontFamily: 'Roboto',
                                                        letterSpacing:
                                                        ScreenUtil().setWidth(0.25),
                                                        color: Theme
                                                            .of(context)
                                                            .textTheme
                                                        // ignore: deprecated_member_use
                                                            .body1
                                                            .color,
                                                        fontSize: ScreenUtil().setSp(16,
                                                            allowFontScalingSelf: true))),
                                              ),
                                            ),
                                          )),
                                      //     TextSpan(text: body, style: TextStyle(fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ))),
                      ],
                    )),
                actions: <Widget>[
                  Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(12),
                        vertical: ScreenUtil().setHeight(24),
                      ),
                      child: Container(
                          child: GestureDetector(
                            onTap: () =>
                                Navigator.of(context).pop(DialogAction.abort),
                            child: RichText(
                              text: TextSpan(
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                  // ignore: deprecated_member_use
                                      .body1,
                                  children: <InlineSpan>[
                                    TextSpan(
                                        text: 'NO',
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: ScreenUtil().setHeight(14),
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing:
                                            ScreenUtil().setWidth(1.25),
                                            color: Color(0xFF3e8aeb)))
                                  ]),
                            ),
                          ))),
                  Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(12),
                          vertical: ScreenUtil().setHeight(24)),
                      child: Container(
                        width: ScreenUtil().setWidth(180),
                        height: ScreenUtil().setHeight(48),
                        child: FlatButton(
                            onPressed: () =>
                                Navigator.of(context).pop(DialogAction.yes),
                            child: Text(
                              'YES',
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: ScreenUtil().setHeight(14),
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: ScreenUtil().setWidth(1.25),
                                  color: Colors.white),
                            ),
                            color: Color(0xFF508be4),
                            textColor: Colors.white,
                            disabledColor: Color(0xFF3e8aeb),
                            disabledTextColor: Colors.black,
                            splashColor: Color(0xFF3e8aeb)),
                      )),

                ],
              ),
            );
          }

          return type == "add_load"
              ? addLoadAlert()
              : type == "edit_unload"
              ? editUnLoadAlert()
              : type == "edit_load"
              ? editLoadAlert()
              : type == "edit_note"
              ? editNoteAlert()
              : type == "add_railcar"
              ? addRailCarUnsavedChanges()
              : type == "success"
              ? requestSuccessful()
              : type == "view_comment"
              ? viewComment()
              : type == "confirm_to_continue"
              ? confirmToContinue()
              : Container();
        });
    if (action == null) {
      action = DialogAction.abort;
    }
    List<Object> returnList = new List<Object>();
    returnList.add(action);
    returnList.add(_textController.text);
    return returnList;
  }
}
