import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CardUI extends StatelessWidget {
  CardUI({this.content,this.floatingWidget,this.floatingActionButtonLocation});

  final Widget content;
  final Widget floatingWidget;
  final FloatingActionButtonLocation floatingActionButtonLocation;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 1024, height: 768, allowFontScaling: true);
    return Scaffold(
      body: Card(
          child: ConstrainedBox(
        constraints: BoxConstraints(minWidth: ScreenUtil().setWidth(976), minHeight: ScreenUtil().setHeight(678)),
        child: Container(
          alignment: Alignment.topCenter,
          color: Theme.of(context).canvasColor,
          child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: content),
        ),
      )),
      floatingActionButtonLocation:floatingActionButtonLocation ,
      floatingActionButton: floatingWidget,
    );
  }
}
