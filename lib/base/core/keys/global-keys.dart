import 'package:flutter/material.dart';

KeyGlobals keyGlobals = new KeyGlobals();

class KeyGlobals {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final loginPageScaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey globalKey = GlobalKey();
}
