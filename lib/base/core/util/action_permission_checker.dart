import 'package:mbase/env.dart';

bool checkUserPermission(List screenActionID) {
  bool screenActionIDMatched = false;
  env.userProfile.screenActionIdList[env.userProfile.facilityId]
      .forEach((element) {
    if (screenActionID.contains(element)) {
      screenActionIDMatched = true;
    }
  });
  return screenActionIDMatched;
}
