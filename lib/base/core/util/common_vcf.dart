import 'package:mbase/base/core/common_base/model/outage_model.dart';
import 'package:mbase/base/core/common_base/model/vcf_header_model.dart';
import 'package:mbase/base/core/common_base/model/vcf_model.dart';

class CommonFunctions {

  OutageDetails getTheOutageAmount(OutageModel outageModel, String measurement) {
    OutageDetails toReturn = null;
    List<OutageDetails> outageDetails = outageModel.details;
    var measurementNum = double.parse(measurement);
    List<OutageDetails> outageDetailsFiltered = outageDetails.where((i) => double.parse(i.measurement) >= measurementNum).toList();
    outageDetailsFiltered.sort((a, b) => double.parse(a.measurement).compareTo(double.parse(b.measurement)));

    if (outageDetailsFiltered != null && outageDetailsFiltered.length > 0) {
      toReturn = outageDetailsFiltered[0];
    }
    return toReturn;
  }

  OutageDetails getMeasurementAmount(OutageModel outageModel, String outage) {
    OutageDetails toReturn = null;
    List<OutageDetails> outageDetails = outageModel.details;
    var outageNum = double.parse(outage);
    List<OutageDetails> outageDetailsFiltered = outageDetails.where((i) => double.parse(i.outage_amount) >= outageNum).toList();
    outageDetailsFiltered.sort((a, b) => double.parse(a.outage_amount).compareTo(double.parse(b.outage_amount)));

    if (outageDetailsFiltered != null && outageDetailsFiltered.length > 0) {
      toReturn = outageDetailsFiltered[0];
    }
    return toReturn;
  }

  VCFDetailModel getTheVcfDetails( List<VCFDetailModel> vcfDetailsModel, String gravity, String observedTemp) {
    VCFDetailModel toReturn = null;

    if (vcfDetailsModel == null || vcfDetailsModel.length == 0) {
      return toReturn;
    }
    var nearestGravity = double.parse(gravity);
    var observedTempNum = double.parse(observedTemp);
    var nearestTemperature= double.parse(observedTemp);


    //fetch nearest lowest gravity value
    List<VCFDetailModel> vcfDetailsModelFiltered = vcfDetailsModel.where((i) => double.parse(i.gravity) <= nearestGravity).toList();
    vcfDetailsModelFiltered.sort((a, b) => double.parse(a.gravity).compareTo(double.parse(b.gravity)));

    nearestGravity = double.parse(vcfDetailsModelFiltered[vcfDetailsModelFiltered.length - 1].gravity);

    //fetch nearest lowest temperature value
    vcfDetailsModelFiltered = vcfDetailsModel.where((i) => double.parse(i.temperature) <= observedTempNum).toList();
    vcfDetailsModelFiltered.sort((a, b) => double.parse(a.temperature).compareTo(double.parse(b.temperature)));
    nearestTemperature = double.parse(vcfDetailsModelFiltered[vcfDetailsModelFiltered.length - 1].temperature);

    vcfDetailsModelFiltered = vcfDetailsModel.where((i) => double.parse(i.gravity) == nearestGravity && double.parse(i.temperature) == nearestTemperature && i.temperature_scale == "F").toList();
    if (vcfDetailsModelFiltered != null && vcfDetailsModelFiltered.length > 0) {
      toReturn = vcfDetailsModelFiltered[0];
    }
    return toReturn;
  }
}
