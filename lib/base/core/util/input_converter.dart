import 'dart:math';

import 'package:dartz/dartz.dart';
import 'package:mbase/base/core/error/exceptions.dart';

class InputConverter {
  Either<DataException, int> stringToUnsignedInteger(String str) {
    try {
      final integer = int.parse(str);
      if (integer < 0) throw FormatException();
      return Right(integer);
    } on FormatException {
      return Left(DataException(e.toString()));
    }
  }
}
