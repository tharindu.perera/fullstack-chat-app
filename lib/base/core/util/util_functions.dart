import 'dart:math';

import 'package:mbase/base/core/common_base/model/in_memory_data_model.dart';

import 'nullCheckUtil.dart';

class Util {
  static double decimalPoints(double val, int places) {
    try {
      double mod = pow(10.0, places);
      return ((val * mod).round().toDouble() / mod);
    } catch (err) {
      print('Error in decimal points rounding up');
      return null;
    }

  }

  static int doubleToInt(double val) {
    if (val != null) {
      return val.truncate();
    } else {
      return null;
    }
  }

  static int nearestRound(double val) {
    if (val != null) {
      return val.round();
    } else {
      return null;
    }
  }

  static double decimalPointsStr(String val, int places) {
    double value = Util.doubleSafetyParse(val);
    return decimalPoints(value, places);
  }

  static double doubleSafetyParse(String val) {
    try {
      return double.tryParse(val);
    } catch (err) {
      print('Error in double parse function');
      return null;
   }
  }

  static int intSafetyParse(String val) {
    try {
      return int.tryParse(val);
    } catch (err) {
      print('Error in int parse function');
      return null;
    }
  }

  static String removeDecimalZeroFormat(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
  }

  static double truncateToHundreths(num value, num point) => (value * pow(10, point)).truncateToDouble() / pow(10, point);


  static String convertValueFromToUom(String value, String fromUomId, String toUomId, int decimalPoints) {
    if (isNullOrEmpty(value) || isNullOrEmpty(fromUomId) || isNullOrEmpty(toUomId)) {
      return value;
    }
    var uomConversion = MemoryData.uomConversion[fromUomId + toUomId];
    if (uomConversion != null) {
      if (decimalPoints != null) {
        value = (double.parse(uomConversion.conversion_factor) * double.parse(value)).toStringAsFixed(decimalPoints);
      } else {
        value = (double.parse(uomConversion.conversion_factor) * double.parse(value)).toString();
      }
    }
    return value;
  }

  static String convertCelciusToFarenhite(String value) {
    if (isNullOrEmpty(value)) {
      return value;
    }
    try {
      var fvalue = (double.parse(value) * (9/5)) + 32;
      return fvalue.round().toString();
    } catch (err) {
      return value;
    }
  }
  static String convertApiToSpecificGravity(String strValue) {
    if (isNullOrEmpty(strValue)) {
      return null;
    }

    try {
      double value = double.parse(strValue);
      var specific = 141.5 / (value + 131.5);
      return Util.decimalPoints(specific, 4).toString();
    } catch (err) {
      return null;
    }
  }

  static bool isWinterMonth() {
    int startMonth = 12;
    int startDate = 31;
    int endMonth = 03;
    int endDate = 19;

    DateTime now = DateTime.now();
    int currentMonth = now.month;
    int currentDate = now.day;

    if (currentMonth >= startMonth && currentDate >= startDate
        && currentMonth <= endMonth && currentDate <= endDate) {
      return true;
    } else {
      return false;
    }
  }
}
