import 'package:intl/intl.dart';

class FormatDates {
  String dateFormatShortMonthDayYear(String date) {
    return DateFormat.yMMMd().format(DateTime.parse(date));
  }

  String dateFormatDayNumber(String date) {
    return DateFormat.d().format(DateTime.parse(date));
  }

  String dateFormatShortDayName(String date) {
    return DateFormat.E().format(DateTime.parse(date));
  }

  DateTime dateFromString(String date) {
    if(date==null) return null;
    return  DateTime.parse(date);
  }

  DateTime dateFromStringAndFormat(String date,String format) {
    if(date==null) return null;
    return  DateFormat(format).parse(date);
  }

  String dateFromStringWithFormatted(String date) {
    if(date==null) return null;
    return  dateFormatToString(dateFromString(date));
  }

  String dateFormatToString(DateTime date) {
    return DateFormat('MM-dd-yyyy HH:mm').format(date);
  }
}
