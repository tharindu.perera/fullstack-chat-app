import 'package:intl/intl.dart';

DateTime getLocalDateTimeFromUTCStr(String str){
  var dateTime = DateFormat("yyyy-MM-dd HH:mm:ss").parse( DateTime.parse(str).toString(), true);
  return dateTime.toLocal();
}

DateTime getUTCTimeFromLocalDateTimeStr(String str){
  var dateTime = DateTime.parse(str);
  return dateTime.toUtc();
}

DateTime getUTCTimeFromLocalDateTime(DateTime time){
  return time.toUtc();
}