String setNullToEmptyString(value){
  return value??"";
}

int setNullToZero(value){
  return value??0;
}

bool setNullToFalse(value){
  return value??false;
}

bool isNullOrEmpty(value) {
  return value == null || value == "";
}

int convertStringToInt(value) {
  if (value == null) {
    return 0;
  } else {
    return int.parse(value);
  }
}