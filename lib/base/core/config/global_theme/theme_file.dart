import 'package:flutter/material.dart';
import 'package:mbase/base/app.dart';

class GlobalTheme {
  //Theme Configuration for the Dark Theme
  ThemeData darkTheme = ThemeData.dark().copyWith(
      primaryColor: Color(0xff182e42),
      canvasColor: Color(0xff182e42),
      scaffoldBackgroundColor: Color(0xff121a23),
      textTheme: TextTheme(
        headline5: TextStyle(
            fontSize: App.headLine5Font,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            color: Colors.white),
        headline6: TextStyle(
            fontSize: App.headLine6Font,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w500,
            letterSpacing: 0.25,
            color: Colors.white),
        bodyText1: TextStyle(
            fontSize: App.bodyText1Font,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.normal,
            letterSpacing: 0.5,
            color: Colors.white),
        bodyText2: TextStyle(
            fontSize: App.bodyText2Font,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.normal,
            letterSpacing: 0.5,
            color: Colors.white),
        caption: TextStyle(
            fontSize: App.captionFont,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.normal,
            letterSpacing: 0.4,
            color: Colors.white),
      ),
      secondaryHeaderColor: Colors.red,
      appBarTheme: AppBarTheme(
          color: Color(0xff182e42),
          actionsIconTheme: IconThemeData(color: Colors.white)),

      iconTheme: IconThemeData(
        color: Colors.white
         )
          );


  //Theme Configuration for the Light Theme
  ThemeData lightTheme = ThemeData.light().copyWith(
      primaryColor: Color(0xfff5f5f5),
      canvasColor: Color(0xfff5f5f5),
      scaffoldBackgroundColor: Color(0xffe7ecf4),
      textTheme: TextTheme(
        headline5: TextStyle(
            fontSize: App.headLine5Font,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            color: Colors.black),
        headline6: TextStyle(
            fontSize: App.headLine6Font,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w500,
            letterSpacing: 0.25,
            color: Colors.black),
        bodyText1: TextStyle(
            fontSize: App.bodyText1Font,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.normal,
            letterSpacing: 0.5,
            color: Colors.black),
        bodyText2: TextStyle(
            fontSize: App.bodyText2Font,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.normal,
            letterSpacing: 0.5,
            color: Colors.black),
        caption: TextStyle(
            fontSize: App.captionFont,
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.normal,
            letterSpacing: 0.4,
            color: Colors.black),
      ),
      appBarTheme: AppBarTheme(
          color: Color(0xfff5f5f5),
          actionsIconTheme: IconThemeData(color: Colors.black)),


  );
}
