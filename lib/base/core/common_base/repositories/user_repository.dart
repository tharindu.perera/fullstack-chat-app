import 'package:mbase/base/core/common_base/dao/user_dao.dart';
import 'package:mbase/base/core/common_base/model/user_profile_model.dart';
import 'package:mbase/base/core/common_base/repositories/base_repository.dart';

class UserRepository extends BaseRepository {
  UserDAO userDAO = UserDAO();

  Future<UserProfile> getUserProfileByFireBaseUID(String uid) async {
    return userDAO.getRecordByID(uid);
  }

  Future<UserProfile> insert(UserProfile record) async {
    return userDAO.add(record);
  }

  Future<UserProfile> update(UserProfile record) async {
    return userDAO.update(record);
  }
}
