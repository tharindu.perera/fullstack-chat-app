import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:mbase/base/core/common_base/dao/offline_dao.dart';
import 'package:mbase/base/core/common_base/model/offline.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/env.dart';
import 'package:uuid/uuid.dart';

int s = 0;

abstract class OfflineDataRepository {
  static CollectionReference collection_offline_mobile = DatabaseProvider.firestore.collection("offline_data_user");

  static void persistToDataStore(dynamic obj, CollectionReference collection) {
    assert(obj.id != null);
    assert(obj.id != "");
    var map = obj.toMap();
    if (env.isDataConnected) {
      map["offline"] = "false";
    } else {
      map["counter"] = DateTime.now().millisecondsSinceEpoch;
      map["offline_data_user"] = env.userProfile.id;
      map["offline"] = "false"; //this is temp
      collection_offline_mobile.doc(obj.id).set(map);
    }
    print("${map.toString()}");
    collection.doc(obj.id).set(map);
  }

  static Future<bool> saveToFireStore() async {
    var query = collection_offline_mobile.where("offline_data_user", isEqualTo: env.userProfile.id).orderBy("counter");
    var resultSet = await query.get();
    print("Records count in offline DB  >>${resultSet.docs.length}");
    bool isConnected = true;
    env.dataConnectionChecker.onStatusChange.listen((status) {
      isConnected = status == DataConnectionStatus.connected ? true : false;
    });

    Offline offline = Offline();
    offline.operation_type = APP_CONST.OFFLINE;
    offline.user = env.userProfile.userName;
    offline.createdDt = DateTime.now();
    offline.id = Uuid().v1();

    while (resultSet.docs.isNotEmpty) {
      print(">>>###### Time[ ${DateTime.now()} ] Records count in offline DB (in while looop) >>${resultSet.docs.length}");
      resultSet.docs.forEach((QueryDocumentSnapshot element) {
        offline.actions.add(element.data());
      });

      if (offline.actions.isNotEmpty && isConnected) {
        OfflineDao.add(offline);
        resultSet.docs.forEach((QueryDocumentSnapshot element) {
          element.reference.delete();
        });
        resultSet = await query.get();
      }
    }

    return Future.value(true);
  }
}
