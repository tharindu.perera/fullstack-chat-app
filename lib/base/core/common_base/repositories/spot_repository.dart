import 'package:mbase/base/core/common_base/dao/spot_dao.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';

class SpotRepository {
  SpotDao spotDao=SpotDao();
  SpotRepository() ;

  Future<List<SpotModel>> fetchAllSpots() async {
    return await spotDao.fetchAllSpots();
  }

  Future<List<SpotModel>> fetchSpotsByTrackId(String trackId) async {
    return await spotDao.fetchSpotsByTrackId(trackId);
  }
}
