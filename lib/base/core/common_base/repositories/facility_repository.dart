import 'package:mbase/base/core/common_base/dao/facility_config_dao.dart';
import 'package:mbase/base/core/common_base/dao/facility_dao.dart';
import 'package:mbase/base/core/common_base/model/facility_configuration_model.dart';
import 'package:mbase/base/core/common_base/model/facility_model.dart';

class FacilityRepository {
  FacilityDao facilityDao = FacilityDao();
  FacilityConfigurationDao facilityConfigurationDao = FacilityConfigurationDao();

  Future<List<FacilityModel>> fetchFacilityList(List list) async {
    return facilityDao.fetchFacilityList(list);
  }

  Future<FacilityConfigurationModel> fetchFacilityConfiguration(facilityId) async {
    return facilityConfigurationDao.fetchFacilityConfiguration(facilityId);
  }
}
