import 'package:mbase/base/core/common_base/dao/yard_dao.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';

class YardRepository {
  YardDao _yardDao = YardDao();

  YardRepository();

  Future<List<YardModel>> fetchAllYards() async {
    return await _yardDao.fetchAllYards();
  }
}
