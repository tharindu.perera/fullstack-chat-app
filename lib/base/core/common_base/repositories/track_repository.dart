
import 'package:mbase/base/core/common_base/dao/track_dao.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';


class TrackRepository {
  TrackDAO _trackDAO=TrackDAO();
  TrackRepository() ;

  Future<List<TrackModel>> fetchAllTracks() async {
    return await _trackDAO.fetchAllTracks();
  }

  Future<List<TrackModel>> fetchTracksByYardId(String yardId) async {
    return await _trackDAO.fetchTracksByYardId(yardId);
  }

  Future<List<TrackModel>> fetchTracksWithoutSpots() async {
    return await _trackDAO.fetchTracksWithoutSpots();
  }
}
