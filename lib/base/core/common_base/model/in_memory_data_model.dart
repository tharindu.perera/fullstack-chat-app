import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/common_dao.dart';
import 'package:mbase/base/core/common_base/dao/equipment_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/facility_configuration_model.dart';
import 'package:mbase/base/core/common_base/model/facility_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/uom.dart';
import 'package:mbase/base/core/common_base/model/uom_conversion.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/common_base/repositories/facility_repository.dart';
import 'package:mbase/base/core/common_base/repositories/spot_repository.dart';
import 'package:mbase/base/core/common_base/repositories/track_repository.dart';
import 'package:mbase/base/core/common_base/repositories/yard_repository.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/modules/inspection/model/inspection_template_model.dart';
import 'package:mbase/env.dart';

class MemoryData {
  static Map<String, TrackModel> inMemoryTrackMap;

  static Map<String, YardModel> inMemoryYardMap;

  static Map<String, SpotModel> inMemorySpotMap;

  static FacilityConfigurationModel facilityConfiguration;
  static FacilityModel facilityModel;
  static Map<String, TrackModel> inMemoryTracksWithoutSpotsMap;
  static Map dataMap;
  static Map<String, UOMConversion> uomConversion;
  static Map<String, UOMConversion> uomConversionByName;
  static Map<String, UOM> uomMap;
  static List<InspectionTemplate> inspectionTemplateList;
  static  List<Equipment> equipmentList ;

  static Future<dynamic> initializeData(FacilityModel facility) async {
    inMemoryTrackMap = {};
    inMemoryYardMap = {};
    inMemorySpotMap = {};
    inMemoryTracksWithoutSpotsMap = {};
    env.userProfile.facilityId = facility.id;
    facilityModel = facility;
    dataMap = {"scannedEquipList":List<Equipment>()};
    dataMap = {"selectedEquipList":List<Equipment>()};
    uomConversion = {};
    uomConversionByName = {};
    uomMap = {};
    inspectionTemplateList = [];

    FirebaseFirestore firebaseFirestore = DatabaseProvider.firestore;
    CollectionReference inspectionRailcarCollection =
    firebaseFirestore.collection("inspection_template");
    inspectionRailcarCollection
        .where("facility_id", isEqualTo: facility.id)
        .get()
        .then((value) => inspectionTemplateList =
        value.docs.map((e) => InspectionTemplate.fromMap(e.data())).toList());


    EquipmentDAO().getRecords().then((value) => equipmentList=value);

    uomMap = await CommonDao().fetchUOMList().then((uomlist) {
      Map<String, UOM> uomTemp = {};
      uomlist.forEach((uom) {
        if (["Metric Tons", "Pounds", "Net Tons", "Kilograms", "Bundles","Each"].contains(uom.unit)) {
          uom.dryOrLiquid = "DRY";
        } else {
          uom.dryOrLiquid = "LIQUID";
        }
        uomTemp[uom.id] = uom;
      });
      return uomTemp;
    });

    uomConversion = await CommonDao().fetchUOMConversionList().then((uomconvarionlist) {
      print("fetchUOMConversionList");
      Map<String, UOMConversion> uomConversionTemp = {};
      uomconvarionlist.forEach((uomConversion) {
        uomConversionTemp[uomConversion.uom_from_id + uomConversion.uom_to_id] = uomConversion;
        uomConversionByName[(uomMap[uomConversion.uom_from_id ]?.unit??"--") + (uomMap[uomConversion.uom_to_id ]?.unit??"--") ] = uomConversion;
      });
      return uomConversionTemp;
    });

    facilityConfiguration = await FacilityRepository().fetchFacilityConfiguration(facility.id);

    var yardList = await YardRepository().fetchAllYards();
    yardList.forEach((element) {
      inMemoryYardMap.putIfAbsent(element.id, () => element);
    });

    var trackList = await TrackRepository().fetchAllTracks();
    trackList.forEach((element) {
      inMemoryTrackMap.putIfAbsent(element.id, () => element);
    });

    var spotList = await SpotRepository().fetchAllSpots();
    spotList.forEach((element) {
      inMemorySpotMap.putIfAbsent(element.id, () => element);
    });

    var trackWithoutSpotsList = await TrackRepository().fetchTracksWithoutSpots();
    trackWithoutSpotsList.forEach((element) {
      inMemoryTracksWithoutSpotsMap.putIfAbsent(element.id, () => element);
    });
    return Future.value(null);
  }
}
