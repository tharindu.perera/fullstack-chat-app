class UOMConversion {
  UOMConversion({
    this.id,
    this.client_id,
    this.conversion_factor,
    this.uom_from_id,
    this.uom_to_id,
    this.updated_dt,
  });

  String client_id;
  String id;
  String conversion_factor;
  String uom_from_id;
  String uom_to_id;
  String updated_dt;

  factory UOMConversion.fromMap(Map<String, dynamic> json) {
    return UOMConversion(
      client_id: json["client_id"] == null ? null : json["client_id"],
      id: json["id"] == null ? null : json["id"],
      conversion_factor: json["conversion_factor"] == null ? null : json["conversion_factor"],
      uom_from_id: json["uom_from_id"] == null ? null : json["uom_from_id"],
      uom_to_id: json["uom_to_id"] == null ? null : json["uom_to_id"],
      updated_dt: json["updated_dt"] == null ? null : json["updated_dt"],
    );
  }

  @override
  String toString() {
    return 'UOMConversion{client_id: $client_id, id: $id, conversion_factor: $conversion_factor, uom_from_id: $uom_from_id, uom_to_id: $uom_to_id, updated_dt: $updated_dt}';
  }

}
