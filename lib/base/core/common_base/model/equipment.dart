import 'package:equatable/equatable.dart';
import 'package:mbase/base/core/common_base/model/additional_field.dart';
import 'package:mbase/base/core/common_base/model/location_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';
import 'package:mbase/base/core/util/date_time_util.dart';
import 'package:mbase/base/modules/listview/actions/comment/model/comment_model.dart';

// ignore: must_be_immutable
class Equipment extends Equatable {
  Equipment(
      {this.equipmentInitial,
      this.equipmentNumber,
      this.equipmentType,
      this.facilityId,
      this.yardId,
      this.trackId,
      this.spotId,
      this.sequenceType,
      this.assetMasterId,
      this.activeAssetId,
      this.facilityVisitId,
      this.assetLocationTypeId,
      this.placedDate,
      this.isSelected,
      this.outboundedDateTime,
      this.transactionStatus,
      this.id,
      this.isOutbounded: false,
      this.isInbounded: false,
      this.permission_id,
      this.parent_id,
      this.createdDt,
      this.updatedDt,
      this.operationType,
      this.blockToId,
      this.blockToName,
      this.source,
      this.sequence,
      this.weight_limit,
      this.lastContainedProductId,
      this.purchaseOrderId,
      this.inbound_shipper_name,
      this.inbound_consignee_name,
      this.inbound_wb_origin,
      this.outbound_shipper_name,
      this.inbound_wb_destination,
      this.outbound_consignee_name,
      this.outbound_origin,
      this.outbound_destination,
      this.shipment_type_desc,
      this.shipment_type_name,
      this.service_code,
      this.inbound_seals,
      this.product_grade,
      this.ib_product,
      this.hazmat_shipment_info_qualifier,
      this.hazmat_shipping_name,
      this.purchaseOrderName,
      this.scypk,
      this.rc_status,
      this.asset_location_type_descr,
      this.order_line_asset_list,
      this.move,
      this.equipment_group_list,
      this.bol_control_number});

  String equipmentInitial;
  String equipmentNumber;
  String equipmentType;
  String facilityId;
  String yardId;
  String trackId;
  String spotId;
  String sequenceType;
  String assetMasterId;
  String activeAssetId;
  String facilityVisitId;
  String assetLocationTypeId;
  String asset_location_type_descr;
  String weight_limit;
  DateTime placedDate;
  bool isSelected = false;
  DateTime outboundedDateTime;
  String transactionStatus;
  String operationType;
  String id;
  DateTime createdDt;
  DateTime updatedDt;
  String source;
  bool isOutbounded = false;
  bool isInbounded = false;
  bool assignToSelection = false;
  String permission_id;
  String parent_id;
  String user;
  String immediateParentId;
  List<Compartment> compartmentList = [];
  List<CompartmentSummary> compartment_summary = [];
  String blockToId;
  String blockToName;
  int sequence=0;//for scan grid
  String sequenceRailcar;
  String lastContainedProductId;
  String purchaseOrderId;
  String purchaseOrderName;
  List<String> equipment_group_list;
  String scypk;
  Move move = Move();
  YardModel toYard; //only for addsw
  TrackModel toTrack; //only for addsw
  SpotModel tospot; //only for addsw
  String inbound_shipper_name; //only for addsw
  String inbound_consignee_name; //only for addsw
  String inbound_wb_origin; //only for addsw
  String outbound_shipper_name; //only for addsw
  String inbound_wb_destination; //only for addsw
  String outbound_consignee_name; //only for addsw
  String outbound_origin; //only for addsw
  String outbound_destination; //only for addsw
  String shipment_type_desc; //only for addsw
  String service_code; //only for addsw
  String shipment_type_name; //only for addsw
  String inbound_seals; //only for addsw
  String product_grade; //only for addsw
  String ib_product; //only for addsw
  String hazmat_shipment_info_qualifier; //only for addsw
  String hazmat_shipping_name; //only for addsw
  List<dynamic> order_line_asset_list; //only for addsw
  String rc_status; //only for addsw
  List<Comment> comments = []; // read only value for view comment
  String bol_control_number;
  bool isDataVerified=false;//this filed is used by AEI Scan

  @override
  List<Object> get props => [id];

  Equipment.fromMap(Map<String, dynamic> map)
      : assert(map['equipment_initial'] != null),
        equipmentInitial = map['equipment_initial'],
        equipmentNumber = map['equipment_number'],
        equipmentType = map['equipment_type'],
        facilityId = map['facility_id'],
        yardId = map['yard_id'],
        trackId = map['track_id'],
        spotId = map['spot_id'],
        sequenceType = map['sequence_type'],
        assetMasterId = map['asset_master_id'],
        activeAssetId = map['scy_pk'],
        facilityVisitId = map['facility_visit_id'],
        assetLocationTypeId = map['asset_location_type_id'],
        rc_status = map['rc_status'],
        order_line_asset_list = map['order_line_asset_list'] == null
            ? []
            : map['order_line_asset_list'],
        placedDate = map['placed_date'] != null
            ? getLocalDateTimeFromUTCStr(map['placed_date'])
            : null,
        outboundedDateTime = map['outbounded_date_time'] != null
            ? getLocalDateTimeFromUTCStr(map['outbounded_date_time'])
            : null,
        transactionStatus = map['transaction_status'],
        operationType = map["operation_type"],
        parent_id = map["parent_id"],
        immediateParentId = map["immediateParentId"],
        weight_limit = map["weight_limit"],
        id = map['id'],
        permission_id = map["permission_id"],
        createdDt = map['created_dt'] != null
            ? getLocalDateTimeFromUTCStr(map['created_dt'])
            : null,
        updatedDt = map['updated_dt'] != null
            ? getLocalDateTimeFromUTCStr(map['updated_dt'])
            : null,
        isOutbounded = map["is_outbounded"] ?? false,
        user = map["user"],
        source = map['source'],
        blockToId = map['blockToId'],
        blockToName = map['blockToName'],
        lastContainedProductId = map['last_contained_product_id'],
        purchaseOrderId = map['purchase_order_id'],
        purchaseOrderName = map['purchase_order_name'],
        sequenceRailcar = map['sequence'],
        asset_location_type_descr = map['asset_location_type_descr'],
        scypk = map['scy_pk'],
        inbound_shipper_name = map['inbound_shipper_name'],
        inbound_consignee_name = map['inbound_consignee_name'],
        inbound_wb_origin = map['inbound_wb_origin'],
        outbound_shipper_name = map['outbound_shipper_name'],
        outbound_consignee_name = map['outbound_consignee_name'],
        inbound_wb_destination = map['inbound_wb_destination'],
        outbound_origin = map['outbound_origin'],
        outbound_destination = map['outbound_destination'],
        shipment_type_desc = map['shipment_type_desc'],
        service_code = map['service_code'],
        shipment_type_name = map['shipment_type_name'],
        inbound_seals = map['inbound_seals'],
        product_grade = map['product_grade'],
        ib_product = map['ib_product'],
        hazmat_shipping_name = map['hazmat_shipping_name'],
        hazmat_shipment_info_qualifier = map['hazmat_shipment_info_qualifier'],
        bol_control_number = map['bol_control_number'],
        equipment_group_list = map['equipment_group_list'] == null
            ? null
            : List<String>.from(map['equipment_group_list'].map((x) => x)),
        move = map['move'] == null ? new Move() : Move().fromMap(map['move']),
        compartmentList = map['compartments'] == null
            ? []
            : List<Compartment>.from(
                map["compartments"].map((x) => Compartment.fromMap(x))) ,
        compartment_summary = map['compartment_summary'] == null
            ? []
            : List<CompartmentSummary>.from(
                map["compartment_summary"].map((x) => CompartmentSummary.fromMap(x)));

  Map<String, dynamic> toMap() => {
        "equipment_initial": equipmentInitial == null ? null : equipmentInitial,
        "equipment_number": equipmentNumber == null ? null : equipmentNumber,
        "equipment_type": equipmentType == null ? null : equipmentType,
        "facility_id": facilityId == null ? null : facilityId,
        "yard_id": yardId == null ? null : yardId,
        "track_id": trackId == null ? null : trackId,
        "spot_id": spotId == null ? null : spotId,
        "rc_status": rc_status == null ? null : rc_status,
        "sequence_type": sequenceType == null ? null : sequenceType,
        "asset_master_id": assetMasterId == null ? null : assetMasterId,
        "active_asset_id": activeAssetId == null ? null : activeAssetId,
        "facility_visit_id": facilityVisitId == null ? null : facilityVisitId,
        "asset_location_type_id":
            assetLocationTypeId == null ? null : assetLocationTypeId,
        "placed_date": placedDate == null ? null : getUTCTimeFromLocalDateTime(placedDate).toString(),
        "outbounded_date_time":
            outboundedDateTime == null ? null : getUTCTimeFromLocalDateTime(outboundedDateTime).toString(),
        "transaction_status":
            transactionStatus == null ? null : transactionStatus,
        "is_outbounded": isOutbounded == null ? null : isOutbounded,
        "operation_type": operationType == null ? null : operationType,
        "weight_limit": weight_limit == null ? null : weight_limit,
        "id": id == null ? null : id,
        "permission_id": permission_id == null ? null : permission_id,
        "parent_id": parent_id == null ? null : parent_id,
        "asset_location_type_descr": asset_location_type_descr == null ? null : asset_location_type_descr,
        "immediateParentId":
            immediateParentId == null ? null : immediateParentId,
        "created_dt": createdDt == null ? null : getUTCTimeFromLocalDateTime(createdDt).toString(),
        "updated_dt": updatedDt == null ? null : getUTCTimeFromLocalDateTime(createdDt).toString(),
        "source": source == null ? null : source,
        "user": user == null ? null : user,
        "blockToId": blockToId == null ? null : blockToId,
        "order_line_asset_list":
            order_line_asset_list == null ? null : order_line_asset_list,
        "blockToName": blockToName == null ? null : blockToName,
        "last_contained_product_id":
            lastContainedProductId == null ? null : lastContainedProductId,
        "purchase_order_id": purchaseOrderId == null ? null : purchaseOrderId,
        "purchase_order_name":
            purchaseOrderName == null ? null : purchaseOrderName,
        "inbound_shipper_name":
            inbound_shipper_name == null ? null : inbound_shipper_name,
        "inbound_consignee_name":
            inbound_consignee_name == null ? null : inbound_consignee_name,
        "outbound_shipper_name":
            outbound_shipper_name == null ? null : outbound_shipper_name,
        "outbound_consignee_name":
            outbound_consignee_name == null ? null : outbound_consignee_name,
        "bol_control_number":
            bol_control_number == null ? null : bol_control_number,
        "sequence": sequenceRailcar,
        "move": move?.toMap(),
        "compartments": compartmentList == null
            ? null
            : compartmentList.map((i) => i.toMap()).toList(),
        "compartment_summary": compartment_summary == null
            ? null
            : compartment_summary.map((i) => i.toMap()).toList(),
        "equipment_group_list":
            equipment_group_list == null ? null : equipment_group_list,
      };

  @override
  String toString() {
    return 'Equipment{equipmentInitial: $equipmentInitial, equipmentNumber: $equipmentNumber, equipmentType: $equipmentType, facilityId: $facilityId, yardId: $yardId, trackId: $trackId, spotId: $spotId, sequenceType: $sequenceType, assetMasterId: $assetMasterId, activeAssetId: $activeAssetId, facilityVisitId: $facilityVisitId, assetLocationTypeId: $assetLocationTypeId, asset_location_type_descr: $asset_location_type_descr, weight_limit: $weight_limit, placedDate: $placedDate, isSelected: $isSelected, outboundedDateTime: $outboundedDateTime, transactionStatus: $transactionStatus, operationType: $operationType, id: $id, createdDt: $createdDt, updatedDt: $updatedDt, source: $source, isOutbounded: $isOutbounded, isInbounded: $isInbounded, assignToSelection: $assignToSelection, permission_id: $permission_id, parent_id: $parent_id, user: $user, immediateParentId: $immediateParentId, compartmentList: $compartmentList, compartment_summary: $compartment_summary, blockToId: $blockToId, blockToName: $blockToName, sequence: $sequence, sequenceRailcar: $sequenceRailcar, lastContainedProductId: $lastContainedProductId, purchaseOrderId: $purchaseOrderId, purchaseOrderName: $purchaseOrderName, equipment_group_list: $equipment_group_list, scypk: $scypk, move: $move, toYard: $toYard, toTrack: $toTrack, tospot: $tospot, inbound_shipper_name: $inbound_shipper_name, inbound_consignee_name: $inbound_consignee_name, inbound_wb_origin: $inbound_wb_origin, outbound_shipper_name: $outbound_shipper_name, inbound_wb_destination: $inbound_wb_destination, outbound_consignee_name: $outbound_consignee_name, outbound_origin: $outbound_origin, outbound_destination: $outbound_destination, shipment_type_desc: $shipment_type_desc, service_code: $service_code, shipment_type_name: $shipment_type_name, inbound_seals: $inbound_seals, product_grade: $product_grade, ib_product: $ib_product, hazmat_shipment_info_qualifier: $hazmat_shipment_info_qualifier, hazmat_shipping_name: $hazmat_shipping_name, order_line_asset_list: $order_line_asset_list, rc_status: $rc_status, comments: $comments, bol_control_number: $bol_control_number, isDataVerified: $isDataVerified}';
  }


}

//////////////////////   Compartment class  ////////////////////

// ignore: must_be_immutable
class Compartment extends Equatable {
  Compartment();

  String productId;
  String productName;
  String compartmentId;
  String uomValue;
  String uomId;
  String purchaseOrderId;
  String purchaseOrderName;
  String shipmentTypeId;
  String shipmentName;
  DateTime loadDate;
  DateTime unloadDate;
  String loadFromId;
  String unload_to_id;
  String unload_to_name;
  String loadFromName;
  String loadAmount;
  String unload_amount;
  String currentAmount;
  String customer_id;
  bool edited;
  List<AdditionFiled> additionalFields;
  LocationModel loadFrom; //this was added for new load unload logic
  bool markAsEmpty = false;
  String reason_code;
  String reason_code_id;

  @override
  List<Object> get props => [compartmentId];

  Map<String, dynamic> toMap() => {
        "compartmentId": compartmentId == null ? null : compartmentId,
        "uomId": uomId == null ? null : uomId,
        "uomValue": uomValue == null ? null : uomValue,
        "purchase_order_id": purchaseOrderId == null ? null : purchaseOrderId,
        "customer_id": customer_id == null ? null : customer_id,
        "purchaseOrderName":
            purchaseOrderName == null ? null : purchaseOrderName,
        "load_from_id": loadFromId == null ? null : loadFromId,
        "loadFromName": loadFromName == null ? null : loadFromName,
        "unload_to_name": unload_to_name == null ? null : unload_to_name,
        "unload_to_id": unload_to_id == null ? null : unload_to_id,
        "product_id": productId == null ? null : productId,
        "productName": productName == null ? null : productName,
        "load_amount": loadAmount == null ? null : loadAmount,
        "current_amount": currentAmount == null ? null : currentAmount,
        "unload_amount": unload_amount == null ? null : unload_amount,
        "load_date_time": loadDate == null ? null : getUTCTimeFromLocalDateTime(loadDate).toString(),
        "unload_date_time": unloadDate == null ? null : getUTCTimeFromLocalDateTime(unloadDate).toString(),
        "shipment_type_id": shipmentTypeId == null ? null : shipmentTypeId,
        "shipment_name": shipmentName == null ? null : shipmentName,
        "additional_fields": additionalFields == null
            ? null
            : additionalFields.map((i) => i.toMap()).toList(),
        "mark_as_empty": markAsEmpty == null ? null : markAsEmpty,
        "reason_code": reason_code == null ? null : reason_code,
        "reason_code_id": reason_code_id == null ? null : reason_code_id
      };

  Compartment.fromMap(Map<String, dynamic> map)
      : compartmentId = map['compartmentId'] ?? map['compartment_id'],
        uomId = map['uomId'] ?? map['uom_id'],
        uomValue = map['uomValue'],
        purchaseOrderId = map['purchase_order_id'],
        purchaseOrderName = map['purchaseOrderName'],
        customer_id = map['customer_id'],
        loadFromId = map['load_from_id'],
        loadFromName = map['loadFromName'],
        unload_to_id = map['unload_to_id'],
        unload_to_name = map['unload_to_name'],
        productId = map['product_id'],
        productName = map['productName'],
        currentAmount = map['current_amount'],
        loadDate = map['load_date_time'] != null
            ? getLocalDateTimeFromUTCStr(map['load_date_time'])
            : null,
        unloadDate = map['unload_date_time'] != null
            ? getLocalDateTimeFromUTCStr(map['unload_date_time'])
            : null,
        shipmentTypeId = map['shipment_type_id'],
        shipmentName = map['shipment_name'],
        additionalFields = map['additional_fields'] == null
            ? []
            : List<AdditionFiled>.from(
                map["additional_fields"].map((x) => AdditionFiled.fromMap(x)));

  @override
  String toString() {
    return 'Compartment{productId: $productId, productName: $productName, compartmentId: $compartmentId, '
        'uomValue: $uomValue, uomId: $uomId, purchaseOrderId: $purchaseOrderId, '
        'purchaseOrderName: $purchaseOrderName, shipmentTypeId: $shipmentTypeId, '
        'shipmentName: $shipmentName, loadDate: $loadDate, unloadDate: $unloadDate, '
        'loadFromId: $loadFromId, unload_to_id: $unload_to_id, unload_to_name: $unload_to_name, '
        'loadFromName: $loadFromName, loadAmount: $loadAmount, unload_amount: $unload_amount, '
        'currentAmount: $currentAmount, customer_id: $customer_id, edited: $edited, '
        'additionalFields: $additionalFields}';
  }
}

///Compartment summary
// ignore: must_be_immutable
class CompartmentSummary {
  CompartmentSummary();

  String compartment_id;
  String product_id;
  String product_name;
  String quantity_uom_id;
  String quantity_uom_name;
  String sequence;
  String total_quantity;
  String total_quantity_in_pounds;



  Map<String, dynamic> toMap() => {
    "compartment_id": compartment_id == null ? null : compartment_id,
    "product_id": product_id == null ? null : product_id,
    "product_name": product_name == null ? null : product_name,
    "quantity_uom_id": quantity_uom_id == null ? null : quantity_uom_id,
    "quantity_uom_name": quantity_uom_name == null ? null : quantity_uom_name,
    "sequence": sequence == null ? null : sequence,
    "total_quantity": total_quantity == null ? null : total_quantity,
    "total_quantity_in_pounds": total_quantity_in_pounds == null ? null : total_quantity_in_pounds,

  };

  CompartmentSummary.fromMap(Map<String, dynamic> map)
      : compartment_id = map['compartment_id']  ,
        product_id = map['product_id']  ,
        product_name = map['product_name'],
        quantity_uom_id = map['quantity_uom_id'],
        quantity_uom_name = map['quantity_uom_name'],
        sequence = map['sequence'],
        total_quantity = map['total_quantity'],
        total_quantity_in_pounds = map['total_quantity_in_pounds'] ;

  @override
  String toString() {
    return 'CompartmentSummary{compartment_id: $compartment_id, product_id: $product_id, product_name: $product_name, quantity_uom_id: $quantity_uom_id, quantity_uom_name: $quantity_uom_name, sequence: $sequence, total_quantity: $total_quantity, total_quantity_in_pounds: $total_quantity_in_pounds}';
  }


}

//////////////////////   MOVE class  ////////////////////
class Move {
  String from_spot_id;
  String from_track_id;
  String from_yard_id;
  String to_spot_id;
  String to_yard_id;
  String to_track_id;
  String railcar_move_date;
  String move_direction;

  Move(
      {this.from_spot_id,
      this.from_track_id,
      this.from_yard_id,
      this.to_yard_id,
      this.to_track_id,
      this.to_spot_id,
      this.railcar_move_date,
      this.move_direction});

  Move fromMap(Map<String, dynamic> json) {
    return Move(
      railcar_move_date:
          json["railcar_move_date"] == null ? null : getLocalDateTimeFromUTCStr( json["railcar_move_date"]).toString(),
      move_direction:
          json["move_direction"] == null ? null : json["move_direction"],
      from_spot_id: json["from_spot_id"] == null ? null : json["from_spot_id"],
      from_yard_id: json["from_yard_id"] == null ? null : json["from_yard_id"],
      from_track_id:
          json["from_track_id"] == null ? null : json["from_track_id"],
      to_yard_id: json["to_yard_id"] == null ? null : json["to_yard_id"],
      to_track_id: json["to_track_id"] == null ? null : json["to_track_id"],
      to_spot_id: json["to_spot_id"] == null ? null : json["to_spot_id"],
    );
  }

  Map<String, dynamic> toMap() => {
        "railcar_move_date":
            railcar_move_date == null ? null : getUTCTimeFromLocalDateTimeStr(railcar_move_date).toString(),
        "move_direction": move_direction == null ? null : move_direction,
        "from_spot_id": from_spot_id == null ? null : from_spot_id,
        "from_yard_id": from_yard_id == null ? null : from_yard_id,
        "from_track_id": from_track_id == null ? null : from_track_id,
        "to_yard_id": to_yard_id == null ? null : to_yard_id,
        "to_track_id": to_track_id == null ? null : to_track_id,
        "to_spot_id": to_spot_id == null ? null : to_spot_id,
      };

  @override
  String toString() {
    return 'MoveRailCarList{'
        'from_spot_id: $from_spot_id, from_track_id: $from_track_id, from_yard_id: $from_yard_id,'
        '  to_spot_id: $to_spot_id,  }';
  }
}
