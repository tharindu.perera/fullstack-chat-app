import 'dart:convert';

ShipmentTypeModel shipmentTypeModelModelFromMap(String str) => ShipmentTypeModel.fromMap(json.decode(str));

class ShipmentTypeModel {
  ShipmentTypeModel({
    this.id,
    this.shipmentTypeDescription,
    this.createdDt,
    this.updatedDt,
  });

  String id;
  String shipmentTypeDescription;
  String createdDt;
  String updatedDt;

  factory ShipmentTypeModel.fromMap(Map<String, dynamic> json) => ShipmentTypeModel(
        id: json["id"] == null ? null : json["id"],
        shipmentTypeDescription: json["shipment_type_description"] == null ? null : json["shipment_type_description"],
        createdDt: json["created_dt"] == null ? null : json["created_dt"],
        updatedDt: json["updated_dt"] == null ? null : json["updated_dt"],
      );
}
