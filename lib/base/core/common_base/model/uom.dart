class UOM {
  UOM({
    this.id,
    this.unit,
    this.dryOrLiquid,
  });

  String id;
  String unit;
  String dryOrLiquid;

  factory UOM.fromMap(Map<String, dynamic> json) => UOM(unit: json["unit"] == null ? null : json["unit"], id: json["id"] == null ? null : json["id"]);

  @override
  String toString() {
    return 'UOM{id: $id, unit: $unit, dryOrLiquid: $dryOrLiquid}';
  }

}
