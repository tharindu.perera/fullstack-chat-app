import 'dart:convert';

SpotModel spotModelFromMap(String str) => SpotModel.fromMap(json.decode(str));

class SpotModel {
  SpotModel({
    this.id,
    this.spotName,
    this.trackId,
    this.yardId,
    this.facilityId,
    this.sequence,
  });

  String id;
  String spotName;
  String trackId;
  String yardId;
  String facilityId;
  String sequence;

  factory SpotModel.fromMap(Map<String, dynamic> json) => SpotModel(
        spotName: json["spot_name"] == null ? null : json["spot_name"],
        trackId: json["track_id"] == null ? null : json["track_id"],
        yardId: json["yard_id"] == null ? null : json["yard_id"],
        id: json["id"] == null ? null : json["id"],
        facilityId: json["facility_id"] == null ? null : json["facility_id"],
        sequence: json["sequence"] == null ? null : json["sequence"],
      );
}
