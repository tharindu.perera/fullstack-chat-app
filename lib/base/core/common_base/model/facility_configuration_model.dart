import 'dart:convert';

FacilityConfigurationModel facilityConfigurationModelFromMap(String str) => FacilityConfigurationModel.fromMap(json.decode(str));

String facilityConfigurationModelToMap(FacilityConfigurationModel data) => json.encode(data.toMap());

class FacilityConfigurationModel {
  FacilityConfigurationModel({
    this.id,
    this.facilityId,
    this.clientId,
    this.defaultInboundYardId,
    this.defaultInboundTrackId,
    this.outboundRailCarDefect,
    this.whenInboundingRailcarNotInUmler,
    this.allowProductToGoBelowZeroAtLocation,
    this.requirePurchaseOrderWhenLoadingAnAsset,
    this.overrideUmlerLoadLimitWhenLoadingRailcar,
    this.requireLocationWhenLoadingAsset,
    this.requireBlockToWhenLoadingRailcar,
    this.whenLoadingRailcarWithoutSeal,
    this.whenLoadingRailcarWithoutInspection,
    this.allowToSetGravityFromProductAttributeInLoadVCF,
    this.allowToSetGravityFromProductAttributeInUnloadVCF,
    this.whenActualOutageInchesLessThanMinOutageInches,
    this.whenProductWeightGreaterThanLoadLimit,
    this.whenLoadingRailcarWithFailedInspection,
    this.productLoadingRestrictions,
    this.customerRestrictions,
    this.overLoadLimit,
    this.requireLocationWhenUnloadingAsset,
    this.showMarkAsEmptyDuringUnloadingRailcar,
    this.whenRailcarUnloadAmountIsGreaterThanWaybillAmount,
    this.createdDt,
    this.updatedDt,
  });

  String id;
  String facilityId;
  String clientId;
  String defaultInboundYardId;
  String defaultInboundTrackId;
  String whenInboundingRailcarNotInUmler;
  String outboundRailCarDefect;
  String allowProductToGoBelowZeroAtLocation;
  String requirePurchaseOrderWhenLoadingAnAsset;
  String overrideUmlerLoadLimitWhenLoadingRailcar;
  String requireLocationWhenLoadingAsset;
  String requireBlockToWhenLoadingRailcar;
  String whenLoadingRailcarWithoutSeal;
  String whenLoadingRailcarWithoutInspection;
  String allowToSetGravityFromProductAttributeInLoadVCF;
  String allowToSetGravityFromProductAttributeInUnloadVCF;
  String whenActualOutageInchesLessThanMinOutageInches;
  String whenProductWeightGreaterThanLoadLimit;
  String whenLoadingRailcarWithFailedInspection;
  String productLoadingRestrictions;
  String customerRestrictions;
  String overLoadLimit;
  String requireLocationWhenUnloadingAsset;
  String showMarkAsEmptyDuringUnloadingRailcar;
  String whenRailcarUnloadAmountIsGreaterThanWaybillAmount;
  String createdDt;
  String updatedDt;

  factory FacilityConfigurationModel.fromMap(Map<String, dynamic> json) => FacilityConfigurationModel(
        id: json["id"] == null ? null : json["id"],
        facilityId: json["facility_id"] == null ? null : json["facility_id"],
        clientId: json["client_id"] == null ? null : json["client_id"],
        defaultInboundYardId: json["default_inbound_yard_id"] == null ? null : json["default_inbound_yard_id"],
        defaultInboundTrackId: json["default_inbound_track_id"] == null ? null : json["default_inbound_track_id"],
        outboundRailCarDefect: json["outbound_railcar_defect"] == null ? null : json["outbound_railcar_defect"],
        whenInboundingRailcarNotInUmler: json["when_inbounding_railcar_not_in_umler"] == null ? null : json["when_inbounding_railcar_not_in_umler"],
        allowProductToGoBelowZeroAtLocation: json["allow_product_to_go_below_zero_at_location"] == null ? null : json["allow_product_to_go_below_zero_at_location"],
        requirePurchaseOrderWhenLoadingAnAsset: json["require_purchase_order_when_loading_an_asset"] == null ? null : json["require_purchase_order_when_loading_an_asset"],
        overrideUmlerLoadLimitWhenLoadingRailcar: json["override_umler_load_limit_when_loading_a_railcar"] == null ? null : json["override_umler_load_limit_when_loading_a_railcar"],
        requireLocationWhenLoadingAsset: json["require_location_when_loading_asset"] == null ? null : json["require_location_when_loading_asset"],
        requireBlockToWhenLoadingRailcar: json["require_block_to_when_loading_railcar"] == null ? null : json["require_block_to_when_loading_railcar"],
        whenLoadingRailcarWithoutSeal: json["when_loading_railcar_without_seal"] == null ? null : json["when_loading_railcar_without_seal"],
        whenLoadingRailcarWithoutInspection: json["when_loading_railcar_without_inspection"] == null ? null : json["when_loading_railcar_without_inspection"],
        allowToSetGravityFromProductAttributeInLoadVCF: json["use_product_attributes_specific_gravity_on_load_vcf"] == null ? null : json["use_product_attributes_specific_gravity_on_load_vcf"],
        allowToSetGravityFromProductAttributeInUnloadVCF: json["use_product_attributes_specific_gravity_on_unload_vcf"] == null ? null : json["use_product_attributes_specific_gravity_on_unload_vcf"],
        whenActualOutageInchesLessThanMinOutageInches: json["when_actual_outage_inches_less_than_min_outage_inches"] == null ? null : json["when_actual_outage_inches_less_than_min_outage_inches"],
        whenProductWeightGreaterThanLoadLimit: json["when_product_weight_greater_than_load_limit"] == null ? null : json["when_product_weight_greater_than_load_limit"],
        whenLoadingRailcarWithFailedInspection: json["when_loading_railcar_with_failed_inspection"] == null ? null : json["when_loading_railcar_with_failed_inspection"],
        productLoadingRestrictions: json["product_loading_restrictions"] == null ? null : json["product_loading_restrictions"],
        customerRestrictions: json["customer_restrictions"] == null ? null : json["customer_restrictions"],
        overLoadLimit: json["over_load_limit"] == null ? null : json["over_load_limit"],
        requireLocationWhenUnloadingAsset: json["require_location_when_unloading_asset"] == null ? null : json["require_location_when_unloading_asset"],
        showMarkAsEmptyDuringUnloadingRailcar: json["show_mark_as_empty_during_unloading_railcar"] == null ? null : json["show_mark_as_empty_during_unloading_railcar"],
        whenRailcarUnloadAmountIsGreaterThanWaybillAmount:
            json["when_railcar_unload_amount_is_greater_than_waybill_amount"] == null ? null : json["when_railcar_unload_amount_is_greater_than_waybill_amount"],
        createdDt: json["created_dt"] == null ? null : json["created_dt"],
        updatedDt: json["updated_dt"] == null ? null : json["updated_dt"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "facility_id": facilityId == null ? null : facilityId,
        "client_id": clientId == null ? null : clientId,
        "default_inbound_yard_id": defaultInboundYardId == null ? null : defaultInboundYardId,
        "default_inbound_track_id": defaultInboundTrackId == null ? null : defaultInboundTrackId,
        "outbound_railcar_defect": outboundRailCarDefect == null ? null : outboundRailCarDefect,
        "when_inbounding_railcar_not_in_umler": whenInboundingRailcarNotInUmler == null ? null : whenInboundingRailcarNotInUmler,
        "allow_product_to_go_below_zero_at_location": allowProductToGoBelowZeroAtLocation == null ? null : allowProductToGoBelowZeroAtLocation,
        "require_purchase_order_when_loading_an_asset": requirePurchaseOrderWhenLoadingAnAsset == null ? null : requirePurchaseOrderWhenLoadingAnAsset,
        "override_umler_load_limit_when_loading_a_railcar": overrideUmlerLoadLimitWhenLoadingRailcar == null ? null : overrideUmlerLoadLimitWhenLoadingRailcar,
        "require_location_when_loading_asset": requireLocationWhenLoadingAsset == null ? null : requireLocationWhenLoadingAsset,
        "require_block_to_when_loading_railcar": requireBlockToWhenLoadingRailcar == null ? null : requireBlockToWhenLoadingRailcar,
        "when_loading_railcar_without_seal": whenLoadingRailcarWithoutSeal == null ? null : whenLoadingRailcarWithoutSeal,
        "when_loading_railcar_without_inspection": whenLoadingRailcarWithoutInspection == null ? null : whenLoadingRailcarWithoutInspection,
        "when_loading_railcar_with_failed_inspection": whenLoadingRailcarWithFailedInspection == null ? null : whenLoadingRailcarWithFailedInspection,
        "use_product_attributes_specific_gravity_on_load_vcf": allowToSetGravityFromProductAttributeInLoadVCF == null ? null : allowToSetGravityFromProductAttributeInLoadVCF,
        "use_product_attributes_specific_gravity_on_unload_vcf": allowToSetGravityFromProductAttributeInUnloadVCF == null ? null : allowToSetGravityFromProductAttributeInUnloadVCF,
        "when_actual_outage_inches_less_than_min_outage_inches": whenActualOutageInchesLessThanMinOutageInches == null ? null : whenActualOutageInchesLessThanMinOutageInches,
        "when_product_weight_greater_than_load_limit": whenProductWeightGreaterThanLoadLimit == null ? null : whenProductWeightGreaterThanLoadLimit,
        "product_loading_restrictions": productLoadingRestrictions == null ? null : productLoadingRestrictions,
        "customer_restrictions": customerRestrictions == null ? null : customerRestrictions,
        "over_load_limit": overLoadLimit == null ? null : overLoadLimit,
        "require_location_when_unloading_asset": requireLocationWhenUnloadingAsset == null ? null : requireLocationWhenUnloadingAsset,
        "show_mark_as_empty_during_unloading_railcar": showMarkAsEmptyDuringUnloadingRailcar == null ? null : showMarkAsEmptyDuringUnloadingRailcar,
        "when_railcar_unload_amount_is_greater_than_waybill_amount": whenRailcarUnloadAmountIsGreaterThanWaybillAmount == null ? null : whenRailcarUnloadAmountIsGreaterThanWaybillAmount,
        "created_dt": createdDt == null ? null : createdDt,
        "updated_dt": updatedDt == null ? null : updatedDt,
      };

  @override
  String toString() {
    return 'FacilityConfigurationModel{id: $id, facilityId: $facilityId, clientId: $clientId, defaultInboundYardId: $defaultInboundYardId, defaultInboundTrackId: $defaultInboundTrackId}';
  }

}
