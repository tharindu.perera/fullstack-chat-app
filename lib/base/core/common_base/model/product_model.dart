import 'dart:convert';

ProductModel productModelFromMap(String str) =>
    ProductModel.fromMap(json.decode(str));

class ProductModel {
  ProductModel(
      {this.type,
      this.productId,
      this.productName,
      this.facilityId,
      this.createdDt,
      this.updatedDt,
      this.source,
      this.dryOrLiquid,
      this.uom_id,
      this.hazardous,
      this.stcc,
      this.inventoryConversionListList});

  String type;
  String productId;
  String productName;
  String facilityId;
  String createdDt;
  String updatedDt;
  String source;
  String dryOrLiquid;
  String uom_id;
  String hazardous;
  String stcc;
  List<InventoryConversionList> inventoryConversionListList = [];

  factory ProductModel.fromMap(Map<String, dynamic> json) => ProductModel(
      type: json["type"] == null ? null : json["type"],
      productId: json["product_id"] == null ? null : json["product_id"],
      productName: json["product_name"] == null ? null : json["product_name"],
      facilityId: json["facility_id"] == null ? null : json["facility_id"],
      createdDt: json["created_dt"] == null ? null : json["created_dt"],
      updatedDt: json["updated_dt"] == null ? null : json["updated_dt"],
      source: json["source"] == null ? null : json["source"],
      dryOrLiquid: json["dry_liquid"] == null ? null : json["dry_liquid"],
      uom_id: json["uom_id"] == null ? null : json["uom_id"],
      hazardous: json["hazardous"] == null ? null : json["hazardous"],
      stcc: json["stcc"] == null ? null : json["stcc"],
      inventoryConversionListList: json['inventory_conversion_list'] == null
          ? []
          : List<InventoryConversionList>.from(json["inventory_conversion_list"]
              .map((x) => InventoryConversionList.fromMap(x))));

  @override
  String toString() {
    return 'ProductModel{type: $type, productId: $productId, productName: $productName, facilityId: $facilityId, createdDt: $createdDt, updatedDt: $updatedDt, source: $source, dryOrLiquid: $dryOrLiquid, uom_id: $uom_id, hazardous: $hazardous, stcc: $stcc, inventoryConversionListList: $inventoryConversionListList}';
  }


}

class InventoryConversionList {
  InventoryConversionList({
    this.conversion_factor,
    this.from_uom_id,
    this.from_uom_name,
    this.inventory_conversion_id,
    this.product_id,
    this.product_name,
    this.to_uom_id,
    this.to_uom_name,
  });

  String conversion_factor;
  String from_uom_id;
  String from_uom_name;
  String inventory_conversion_id;
  String product_id;
  String product_name;
  String to_uom_id;
  String to_uom_name;

  factory InventoryConversionList.fromMap(
          Map<String, dynamic> json) =>
      InventoryConversionList(
          conversion_factor: json["conversion_factor"] == null
              ? null
              : json["conversion_factor"],
          from_uom_id: json["from_uom_id"] == null ? null : json["from_uom_id"],
          from_uom_name:
              json["from_uom_name"] == null ? null : json["from_uom_name"],
          inventory_conversion_id: json["inventory_conversion_id"] == null
              ? null
              : json["inventory_conversion_id"],
          product_id: json["product_id"] == null ? null : json["product_id"],
          product_name:
              json["product_name"] == null ? null : json["product_name"],
          to_uom_id: json["to_uom_id"] == null ? null : json["to_uom_id"],
          to_uom_name:
              json["to_uom_name"] == null ? null : json["to_uom_name"]);

  @override
  String toString() {
    return 'InventoryConversionList{conversion_factor: $conversion_factor, from_uom_id: $from_uom_id, from_uom_name: $from_uom_name, inventory_conversion_id: $inventory_conversion_id, product_id: $product_id, product_name: $product_name, to_uom_id: $to_uom_id, to_uom_name: $to_uom_name}';
  }


}
