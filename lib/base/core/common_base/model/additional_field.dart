class AdditionFiled {
  String additionalFieldName;
  String additionalFieldValue;

  AdditionFiled(this.additionalFieldName, this.additionalFieldValue);

  Map<String, String> toMap() {
    return {
      "additionalFieldName": additionalFieldName == null ? null : additionalFieldName,
      "additionalFieldValue": additionalFieldValue == null ? null : additionalFieldValue,
    };
  }

  AdditionFiled.fromMap(Map<String, dynamic> map)
      : additionalFieldName = map['additionalFieldName'],
        additionalFieldValue = map['additionalFieldValue'];
}