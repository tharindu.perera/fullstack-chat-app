import 'dart:convert';

ProductAttributesModel productAttributesModelFromMap(String str) =>
    ProductAttributesModel.fromMap(json.decode(str));

class ProductAttributesModel {
  ProductAttributesModel(
      {this.id,
      this.product_id,
      this.product_name,
      this.product_stcc,
      this.facility_id,
      this.vcf_header_id,
      this.min_outage,
      this.safety_outage,
      this.gravity,
      this.density,
      this.max_reference_temp,
      this.max_reference_temp_scale,
      this.winter_temp_flag,
      this.winter_max_reference_temp,
      this.winter_max_reference_temp_scale,
      this.gravity_type,
      this.created_dt,
      this.updated_dt});

  String id;
  String product_id;
  String product_name;
  String product_stcc;
  String facility_id;
  String vcf_header_id;
  String min_outage;
  String safety_outage;
  String gravity;
  String density;
  String max_reference_temp;
  String max_reference_temp_scale;
  String winter_temp_flag;
  String winter_max_reference_temp;
  String winter_max_reference_temp_scale;
  String gravity_type;
  DateTime created_dt;
  DateTime updated_dt;

  ProductAttributesModel.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        product_id = map['product_id'],
        product_name = map['product_name'],
        product_stcc = map['product_stcc'],
        facility_id = map['facility_id'],
        vcf_header_id = map['vcf_header_id'],
        min_outage = map['min_outage'],
        safety_outage = map['safety_outage'],
        gravity = map['gravity'],
        density = map['density'],
        max_reference_temp = map['max_reference_temp'],
        max_reference_temp_scale = map['max_reference_temp_scale'],
        winter_temp_flag = map['winter_temp_flag'],
        winter_max_reference_temp = map['winter_max_reference_temp'],
        winter_max_reference_temp_scale =
            map['winter_max_reference_temp_scale'],
        gravity_type = map['gravity_type'],
        created_dt = map['created_dt'] == null
            ? null
            : DateTime.parse(map['created_dt']),
        updated_dt = map['updated_dt'] == null
            ? null
            : DateTime.parse(map['updated_dt']);

  @override
  String toString() {
    return 'ProductAttributesModel{id: $id, product_id: $product_id, product_name: $product_name, gravity: $gravity, min outage: $min_outage, safety outage: $safety_outage, VCF Id: $vcf_header_id}';
  }
}
