import 'dart:convert';

class Drop {
  String dropDownDisplayName;
}

LocationModel locationModelFromMap(String str) => LocationModel.fromMap(json.decode(str));

class LocationModel extends Drop {
  LocationModel({
    this.facility_id,
    this.id,
    this.areaId,
    this.name,
    this.createdDt,
    this.updatedDt,
    this.inventory,
    this.measure_id,
  });

  String facility_id;
  String id;
  String areaId;
  String name;
  String createdDt;
  String updatedDt;
  List<LocationInventory> inventory = [];

  //folowing fields are dymanicaly generated accrodingly  load unload logic
  String measure_id;
  String measure_name;
  String product_id;
  String product_name;
  String customer_id;
  String customer_name;
  String quantity;

  factory LocationModel.fromMap(Map<String, dynamic> json) => LocationModel(
        id: json["id"] == null ? null : json["id"],
        areaId: json["area_id"] == null ? null : json["area_id"],
        name: json["name"] == null ? null : json["name"],
        measure_id: json["uom_id"] == null ? null : json["uom_id"],
        facility_id: json["facility_id"] == null ? null : json["facility_id"],
        createdDt: json["created_dt"] == null ? null : json["created_dt"],
        updatedDt: json["updated_dt"] == null ? null : json["updated_dt"],
        inventory: json['inventory'] == null ? [] : List<LocationInventory>.from(json["inventory"].map((x) => LocationInventory.fromMap(x))),
      );

  @override
  String toString() {
    return 'LocationModel{facility_id: $facility_id, id: $id, areaId: $areaId, name: $name, createdDt: $createdDt, updatedDt: $updatedDt, inventory: $inventory}';
  }
}

class LocationInventory {
  LocationInventory();

  String inventory_id;
  String product_id;
  String product_name;
  String customer_id;
  String customer_name;
  String quantity;
  String measure_id;
  String measure_name;

  Map<String, dynamic> toMap() => {
        "inventory_id": inventory_id,
        "product_id": product_id,
        "product_name": product_name,
        "customer_id": customer_id,
        "customer_name": customer_name,
        "quantity": quantity,
        "measure_id": measure_id,
        "measure_name": measure_name,
      };

  LocationInventory.fromMap(Map<String, dynamic> map)
      : inventory_id = map['inventory_id'],
        product_id = map['product_id'] ?? map['product_id'],
        product_name = map['product_name'],
        customer_id = map['customer_id'],
        customer_name = map['customer_name'],
        quantity = map['quantity'],
        measure_id = map['measure_id'],
        measure_name = map['measure_name'];

  @override
  String toString() {
    return 'LocationInventory{inventory_id: $inventory_id, product_id: $product_id, product_name: $product_name, customer_id: $customer_id, customer_name: $customer_name, quantity: $quantity, measure_id: $measure_id}';
  }
}
