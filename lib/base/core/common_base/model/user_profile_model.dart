// To parse this JSON data, do

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

UserProfile userProfileModelFromMap(String str) => UserProfile.fromMap(json.decode(str));

class UserProfile {
  UserProfile({
    this.firstName,
    this.lastName,
    this.id,
    this.scyPk,
    this.clientId,
    this.userName,
    this.screenIdList,
    this.screenActionIdList,
    this.active,
    this.theme,
    this.documentCreatedDateTime,
    this.facilityIdList,
    this.default_facility,
  });

  String firstName;
  String lastName;
  String id;
  String userName;
  String facilityId;
  String default_facility;
  String active;
  String clientId;
  String scyPk;
  String theme;
  String documentCreatedDateTime;
  Map<String, List<dynamic>> screenIdList;
  List<String> facilityIdList;
  Map<String, List<dynamic>> screenActionIdList;

  factory UserProfile.fromMap(Map<String, dynamic> json) => UserProfile(
        scyPk: json["scy_pk"] == null ? null : json["scy_pk"],
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        id: json["id"] == null ? null : json["id"],
        userName: json["user_name"] == null ? null : json["user_name"],
        facilityIdList: json["facility_id_list"] == null ? null : List<String>.from(json["facility_id_list"].map((x) => x)),
        screenIdList: json["screen_id_list"] == null ? null : Map<String, List<dynamic>>.from(json["screen_id_list"]),
        screenActionIdList: json["screen_action_id_list"] == null ? null : Map<String, List<dynamic>>.from(json["screen_action_id_list"]),
        active: json["active"] == null ? null : json["active"],
        theme: json["theme"] == null ? null : json["theme"],
        documentCreatedDateTime: json["document_created_date_time"] == null ? null : json["document_created_date_time"],
        clientId: json["client_id"] == null ? null : json["client_id"],
        default_facility: json["default_facility"] == null ? null : json["default_facility"],
      );

  factory UserProfile.fromSnapshot(DocumentSnapshot snapshot) {
    return UserProfile.fromMap(snapshot.data());
  }

  @override
  String toString() {
    return 'UserProfile{id: $id, userName: $userName, facilityId: $facilityId, clientId: $clientId, scyPk: $scyPk, screenIdList: $screenIdList, facilityIdList: $facilityIdList, screenActionIdList: $screenActionIdList}';
  }
}
