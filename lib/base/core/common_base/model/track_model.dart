// To parse this JSON data, do

import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';

TrackModel trackModelFromMap(String str) => TrackModel.fromMap(json.decode(str));

class TrackModel extends Equatable {
  @override
  List<Object> get props => [id,trackName];
  TrackModel({
    this.id,
    this.trackName,
    this.yardId,
    this.facilityId,
    this.documentCreatedDateTime,
  });

  String id;
  String trackName;
  String yardId;
  String documentCreatedDateTime;
  String facilityId;
  List<SpotModel> spotList;

  factory TrackModel.fromMap(Map<String, dynamic> json) => TrackModel(
        id: json["id"] == null ? null : json["id"],
        trackName: json["track_name"] == null ? null : json["track_name"],
        yardId: json["yard_id"] == null ? null : json["yard_id"],
        facilityId: json["facility_id"] == null ? null : json["facility_id"],
        documentCreatedDateTime: json["document_created_date_time"] == null ? null : json["document_created_date_time"],
      );

  Map<String, dynamic> toMap() => {
        "track_id": id == null ? null : id,
        "track_name": trackName == null ? null : trackName,
        "yard_id": yardId == null ? null : yardId,
        "facility_id": facilityId == null ? null : facilityId,
        "document_created_date_time": documentCreatedDateTime == null ? null : documentCreatedDateTime,
      };

  @override
  String toString() {
    return 'TrackModel{id: $id, trackName: $trackName, yardId: $yardId, documentCreatedDateTime: $documentCreatedDateTime, facilityId: $facilityId}';
  }
}
