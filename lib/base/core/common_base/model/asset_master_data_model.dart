import 'dart:convert';

AssetMasterDataModel assetMasterDataModelFromMap(String str) =>
    AssetMasterDataModel.fromMap(json.decode(str));


class AssetMasterDataModel {
  AssetMasterDataModel(
      {this.id,
      this.equip_init,
      this.equip_nbr,
      this.tare_weight,
      this.additional_identifier,
      this.shipping_contain_spec,
      this.tare_weight_uom_id,
      this.load_limit,
      this.gallon_capacity,
      this.created_dt,
      this.aar_car_type,
      this.owner,
      this.outside_length_feet,
      this.equipment_group,
      this.compartments,
      this.outlets,
      this.bottom_outlet_fitting,
      this.updated_dt});

  String id;
  String equip_init;
  String equip_nbr;
  String tare_weight;
  String additional_identifier;
  String shipping_contain_spec;
  String tare_weight_uom_id;
  String load_limit;
  String gallon_capacity;
  String outside_length_feet;
  String outside_length_inches;
  String aar_car_type;
  String owner;
  String equipment_group;
  String outlets;
  String bottom_outlet_fitting;
  String compartments;
  DateTime created_dt;
  DateTime updated_dt;

  AssetMasterDataModel.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        equip_init = map['equip_init'],
        equip_nbr = map['equip_nbr'],
        tare_weight = map['tare_weight'],
        additional_identifier = map['additional_identifier'],
        shipping_contain_spec = map['shipping_contain_spec'],
        tare_weight_uom_id = map['tare_weight_uom_id'],
        load_limit = map['load_limit'],
        gallon_capacity = map['gallon_capacity'],
        aar_car_type = map['aar_car_type'],
        owner = map['owner'],
        equipment_group = map['equipment_group'],
        outside_length_feet = map['outside_length_feet'],
        bottom_outlet_fitting = map['bottom_outlet_fitting'],
        outlets = map['outlets'],
        outside_length_inches = map['outside_length_inches'],
        compartments = map['compartments'],
        created_dt = map['created_dt'] == null
            ? null
            : DateTime.parse(map['created_dt']),
        updated_dt = map['updated_dt'] == null
            ? null
            : DateTime.parse(map['updated_dt']);

  @override
  String toString() {
    return 'AssetMasterDataModel{id: $id, equip_init: $equip_init, equip_nbr: $equip_nbr}';
  }
}
