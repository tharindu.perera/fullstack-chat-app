

import 'package:mbase/base/core/common_base/model/location_model.dart';
import 'package:mbase/base/core/util/date_time_util.dart';

class NewSwitchList extends Drop {
  String createdDt;
  String source;
  String operation_type;
  String id;
  String facility_id;
  String user;
  String target_complete_date;
  String target_start_date;
  String switch_crew;
  String switch_list_name;
  String switch_list_status;
  String action_type;
  String scy_pk;
  String parent_id;
  String immediateParentId;
  String permission_id;
  String transaction_status;
  int Cars;
  String complete;
  String switch_crew_name;


  List<NewSwitchListEquipment> railcars = List<NewSwitchListEquipment>();
  String get dropDownDisplayName => switch_list_name;

  NewSwitchList(
      {
        this.target_start_date,
        this.source,
        this.target_complete_date,
        this.switch_list_status,
        this.switch_crew,
        this.switch_list_name,
        this.action_type,
        this.createdDt,
        this.id,
        this.facility_id,
        this.user,
        this.scy_pk,
        this.parent_id,
        this.immediateParentId,
        this.permission_id,
        this.railcars,
        this.Cars,
        this.transaction_status,
        this.operation_type,
        this.complete,
        this.switch_crew_name

      });

  factory NewSwitchList.fromJson(Map<String, dynamic> json) => NewSwitchList(
    action_type: json["action_type"] == null ? null : json["action_type"],
    switch_crew: json["switch_crew"] == null ? null : json["switch_crew"],
    switch_list_status: json["switch_list_status"] == null ? null : json["switch_list_status"],
    switch_list_name: json["switch_list_name"] == null ? null : json["switch_list_name"],
    createdDt: json['created_dt'] != null ? json['created_dt'] : null,
    //createdDt: json['created_dt'] != null ? DateTime.parse(json['created_dt']) : null,
    source: json["source"] == null ? null : json["source"],
    id: json["id"] == null ? null : json["id"],
    facility_id: json["facility_id"] == null ? null : json["facility_id"],
    user: json["user"] == null ? null : json["user"],
    scy_pk: json["scy_pk"] == null ? null : json["scy_pk"],
    Cars : 0,
    permission_id: json["permission_id"] == null ? null : json["permission_id"],
    transaction_status: json["transaction_status"] == null ? null : json["transaction_status"],
    operation_type: json["operation_type"] == null ? null : json["operation_type"],
    complete: json["Complete"] == null ? null : json["Complete"],
    //target_complete_date: json["target_complete_date"] != null ? DateTime.parse(json['target_complete_date']) : null,
    target_complete_date: json["target_complete_date"] != null ?getLocalDateTimeFromUTCStr(json["target_complete_date"]).toString()  : null,
    target_start_date: json["target_start_date"] != null ? getLocalDateTimeFromUTCStr(json["target_start_date"]).toString()  : null,
    parent_id: json["parent_id"] != null ? json["parent_id"]  : null,
    immediateParentId:  json["immediateParentId"] != null ? json["immediateParentId"]  : null,
    switch_crew_name:  json["switch_crew_name"] != null ? json["switch_crew_name"]  : null,

    railcars: json["railcars"] == null ? null : List<NewSwitchListEquipment>.from(json["railcars"].map((x) => NewSwitchListEquipment().fromMap(x))),

  );

  Map<String, dynamic> toMap() => {
     "created_dt": createdDt == null ? null : getUTCTimeFromLocalDateTimeStr(createdDt).toString(),
    "source": source == null ? null : source,
    "id": id == null ? null : id,
    "facility_id": facility_id == null ? null : facility_id,
    "user": user == null ? null : user,
    "action_type": action_type == null ? null : action_type,
    "switch_crew": switch_crew == null ? null : switch_crew,
    "switch_list_status": switch_list_status == null ? null : switch_list_status,
    "switch_list_name": switch_list_name == null ? null : switch_list_name,
    "scy_pk": scy_pk == null ? null : scy_pk,
    "parent_id": parent_id == null ? null : parent_id,
    "immediateParentId": immediateParentId == null ? null : immediateParentId,
    "operation_type": operation_type == null ? null : operation_type,
    "transaction_status": transaction_status == null ? null : transaction_status,
    "Complete": complete == null ? null : complete,
     "target_complete_date": target_complete_date == null ? null : getUTCTimeFromLocalDateTimeStr(target_complete_date).toString(),
     "target_start_date": target_start_date == null ? null : getUTCTimeFromLocalDateTimeStr(target_start_date).toString(),
    "permission_id": permission_id == null ? null : permission_id,
    "railcars": railcars == null ? null :railcars.map((i) => i.toMap()).toList(),
    "switch_crew_name": switch_crew_name == null ? null : switch_crew_name,

  };

  @override
  String toString() {
    return 'NewSwitchList{createdDt: $createdDt, source: $source, operation_type: $operation_type, id: $id, facility_id: $facility_id, user: $user, target_complete_date: $target_complete_date, target_start_date: $target_start_date, switch_crew: $switch_crew, switch_list_name: $switch_list_name, switch_list_status: $switch_list_status, action_type: $action_type, scy_pk: $scy_pk, parent_id: $parent_id, immediateParentId: $immediateParentId, permission_id: $permission_id, transaction_status: $transaction_status, Cars: $Cars, Complete: $complete, data: $railcars}';
  }
}

class NewSwitchListEquipment {
  String id;
  String equipment_id;
  String scy_pk;
  String asset_master_id;
  String equipment_initial;
  String equipment_number;
  String from_spot_id;
  String from_track_id;
  String from_yard_id;
  String to_yard_id;
  String to_track_id;
  String railcar_action_type;
  String to_spot_id;
  String switch_list_comment;
  String switch_list_equipment_status;
  String transaction_status;
  String permission_id;
  String parent_id;
  String railcar_completed_date;
  String placed_date;



  NewSwitchListEquipment({
    this.id,this.equipment_id,
    this.scy_pk, this.asset_master_id, this.equipment_initial, this.equipment_number, this.from_spot_id,
  this.from_track_id,this.from_yard_id,this.to_spot_id,this.to_track_id,this.to_yard_id,this.switch_list_comment,this.switch_list_equipment_status,
    this.railcar_action_type,this.transaction_status,this.permission_id,this.parent_id,this.railcar_completed_date,
    this.placed_date

  });

  NewSwitchListEquipment fromMap(Map<String, dynamic> json) {
    return NewSwitchListEquipment(
      id: json["id"] == null ? null : json["id"],
      scy_pk: json["scy_pk"] == null ? null : json["scy_pk"],
      asset_master_id: json["asset_master_id"] == null ? null : json["asset_master_id"],
      equipment_initial: json["equipment_initial"] == null ? null : json["equipment_initial"],
      equipment_number: json["equipment_number"] == null ? null : json["equipment_number"],
      from_spot_id: json["from_spot_id"] == null ? null : json["from_spot_id"],
      from_yard_id: json["from_yard_id"] == null ? null : json["from_yard_id"],
      from_track_id: json["from_track_id"] == null ? null : json["from_track_id"],
      to_yard_id: json["to_yard_id"] == null ? null : json["to_yard_id"],
      to_track_id: json["to_track_id"] == null ? null : json["to_track_id"],
      to_spot_id: json["to_spot_id"] == null ? null : json["to_spot_id"],
      switch_list_comment: json["switch_list_comment"] == null ? null : json["switch_list_comment"],
      switch_list_equipment_status: json["switch_list_equipment_status"] == null ? null : json["switch_list_equipment_status"],
      railcar_action_type: json["action_type"] == null ? null : json["action_type"],
      transaction_status: json["transaction_status"] == null ? null : json["transaction_status"],
      permission_id: json["permission_id"] == null ? null : json["permission_id"],
      parent_id: json["parent_id"] == null ? null : json["parent_id"],
      equipment_id: json["equipment_id"] == null ? null : json["equipment_id"],
      railcar_completed_date: json["railcar_completed_date"] == null ? null :  getLocalDateTimeFromUTCStr(json["railcar_completed_date"]).toString(),
      placed_date: json["placed_date"] == null ? null : getLocalDateTimeFromUTCStr(json["placed_date"]).toString(),

    );
  }

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "scy_pk": scy_pk == null ? null : scy_pk,
    "asset_master_id": asset_master_id == null ? null : asset_master_id,
    "equipment_initial": equipment_initial == null ? null : equipment_initial,
    "equipment_number": equipment_number == null ? null : equipment_number,
    "from_spot_id": from_spot_id == null ? null : from_spot_id,
    "from_yard_id": from_yard_id == null ? null : from_yard_id,
    "from_track_id": from_track_id == null ? null : from_track_id,
    "to_yard_id": to_yard_id == null ? null : to_yard_id,
    "to_track_id": to_track_id == null ? null : to_track_id,
    "to_spot_id": to_spot_id == null ? null : to_spot_id,
    "switch_list_comment": switch_list_comment == null ? null : switch_list_comment,
    "switch_list_equipment_status": switch_list_equipment_status == null ? null : switch_list_equipment_status,
    "railcar_action_type": railcar_action_type == null ? null : railcar_action_type,
    "transaction_status": transaction_status == null ? null : transaction_status,
    "permission_id": permission_id == null ? null : permission_id,
    "parent_id": parent_id == null ? null : parent_id,
    "equipment_id": equipment_id == null ? null : equipment_id,
    "railcar_completed_date":railcar_completed_date == null ? null :railcar_completed_date,
    "placed_date":placed_date == null ? null : getUTCTimeFromLocalDateTimeStr(placed_date).toString()

  };

  @override
  String toString() {
    return 'NewSwitchListEquipment{scy_pk: $scy_pk, asset_master_id: $asset_master_id, equipment_initial: $equipment_initial, equipment_number: $equipment_number, from_spot_id: $from_spot_id, from_track_id: $from_track_id, from_yard_id: $from_yard_id, to_yard_id: $to_yard_id, to_track_id: $to_track_id, railcar_action_type: $railcar_action_type, to_spot_id: $to_spot_id, switchlist_comment: $switch_list_comment, switchlist_status: $switch_list_equipment_status, transaction_status: $transaction_status, permission_id: $permission_id, parent_id: $parent_id}';
  }
}
