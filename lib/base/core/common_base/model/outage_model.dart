import 'dart:convert';

OutageModel outageModelFromMap(String str) =>
    OutageModel.fromMap(json.decode(str));

class OutageModel {
  OutageModel(
      {this.id,
      this.equip_init,
      this.equip_nbr,
      this.tare_weight,
      this.additional_identifier,
      this.tare_weight_uom_id,
      this.load_limit,
      this.gallon_capacity,
      this.details,
      this.created_dt,
      this.updated_dt});

  String id;
  String equip_init;
  String equip_nbr;
  String tare_weight;
  String additional_identifier;
  String tare_weight_uom_id;
  String load_limit;
  String gallon_capacity;
  List<OutageDetails> details = [];
  DateTime created_dt;
  DateTime updated_dt;

  OutageModel.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        equip_init = map['equip_init'],
        equip_nbr = map['equip_nbr'],
        tare_weight = map['tare_weight'],
        additional_identifier = map['additional_identifier'],
        tare_weight_uom_id = map['tare_weight_uom_id'],
        load_limit = map['load_limit'],
        gallon_capacity = map['gallon_capacity'],
        details = map['details'] == null
            ? []
            : List<OutageDetails>.from(
                map["details"].map((x) => OutageDetails.fromMap(x))),
        created_dt = map['created_dt'] == null
            ? null
            : DateTime.parse(map['created_dt']),
        updated_dt = map['updated_dt'] == null
            ? null
            : DateTime.parse(map['updated_dt']);

  @override
  String toString() {
    return 'OutageModel{id: $id, equip_init: $equip_init, equip_nbr: $equip_nbr}';
  }
}

class OutageDetails {
  String asset_outage_detail_id;
  String measurement;
  String outage_amount;

  OutageDetails(
      {this.asset_outage_detail_id, this.measurement, this.outage_amount});

  static OutageDetails fromMap(Map<String, dynamic> json) {
    return OutageDetails(
        asset_outage_detail_id: json["asset_outage_detail_id"] == null
            ? null
            : json["asset_outage_detail_id"],
        measurement: json["measurement"] == null ? null : json["measurement"],
        outage_amount:
            json["outageAmount"] == null ? null : json["outageAmount"]);
  }

  @override
  String toString() {
    return 'OutageDetails{asset_outage_detail_id: $asset_outage_detail_id, measurement: $measurement, outage_amount: $outage_amount}';
  }
}
