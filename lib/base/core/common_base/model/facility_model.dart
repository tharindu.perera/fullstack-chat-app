import 'dart:convert';

FacilityModel facilityFromMap(String str) => FacilityModel.fromMap(json.decode(str));

String facilityToMap(FacilityModel data) => json.encode(data.toMap());

class FacilityModel {
  FacilityModel({this.id, this.clientId, this.facilityName, this.cityName, this.stateName,this.flyover_field_list,this.equipment_group_list});

  String id;
  String clientId;
  String facilityName;
  String cityName;
  String stateName;
  List<String> flyover_field_list;
  List<String> equipment_group_list;


  factory FacilityModel.fromMap(Map<String, dynamic> json) => FacilityModel(
        id: json["id"] == null ? null : json["id"],
        clientId: json["client_id"] == null ? null : json["client_id"],
        facilityName: json["facility_name"] == null ? null : json["facility_name"],
        cityName: json["city_name"] == null ? null : json["city_name"],
        stateName: json["state_name"] == null ? null : json["state_name"],
        flyover_field_list: json["flyover_field_list"] == null ? null : List<String>.from(json["flyover_field_list"].map((x) => x)),
        equipment_group_list: json["equipment_group_list"] == null ? null : List<String>.from(json["equipment_group_list"].map((x) => x))

  );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "client_id": clientId == null ? null : clientId,
        "facility_name": facilityName == null ? null : facilityName,
        "city_name": cityName == null ? null : cityName,
        "state_name": stateName == null ? null : stateName,
        "equipment_group_list": equipment_group_list == null ? null : equipment_group_list,

  };

  @override
  String toString() {
    return 'FacilityModel{id: $id, clientId: $clientId, facilityName: $facilityName, cityName: $cityName, stateName: $stateName,equipment_group_list: $equipment_group_list}';
  }


}
