import 'dart:convert';

VCFModel vcfModelFromMap(String str) =>
    VCFModel.fromMap(json.decode(str));

class VCFModel {
  String id;
  List<VCFDetails> details = [];
  String description;
  DateTime created_dt;

  VCFModel({this.id, this.details, this.description, this.created_dt});

  VCFModel.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        description = map['description'],
        details = map['details'] == null ? []
            : List<VCFDetails>.from(map["details"].map((x) => VCFDetails.fromMap(x))),
        created_dt = map['created_dt'] == null
            ? null
            : DateTime.parse(map['created_dt']);

  @override
  String toString() {
    return 'VCFModel{id: $id, description: $description}';
  }
}

class VCFDetails {
  String vcf_detail_id;
  String temperature;
  String temperature_scale;
  String gravity;
  String vcf;

  VCFDetails(
      {this.vcf_detail_id,
      this.temperature,
      this.temperature_scale,
      this.gravity,
      this.vcf});

  static VCFDetails fromMap(Map<String, dynamic> json) {
    return VCFDetails(
        vcf_detail_id:
            json["vcf_detail_id"] == null ? null : json["vcf_detail_id"],
        temperature: json["temperature"] == null ? null : json["temperature"],
        temperature_scale: json["temperature_scale"] == null
            ? null
            : json["temperature_scale"],
        gravity: json["gravity"] == null ? null : json["gravity"],
        vcf: json["vcf"] == null ? null : json["vcf"]);
  }

  @override
  String toString() {
    return 'VCFDetails{vcf_detail_id: $vcf_detail_id, temperature: $temperature, gravity: $gravity, vcf: $vcf, temperature_scale: $temperature_scale}';
  }
}
