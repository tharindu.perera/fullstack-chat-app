// To parse this JSON data, do

import 'dart:convert';

import 'package:equatable/equatable.dart';

YardModel yardModelFromMap(String str) => YardModel.fromMap(json.decode(str));

String yardModelToMap(YardModel data) => json.encode(data.toMap());

class YardModel extends Equatable {
  @override
  List<Object> get props => [id,yardName];

  YardModel({
    this.id,
    this.yardName,
    this.facilityId,
    this.documentCreatedDateTime,
  });

  String id;
  String yardName;
  String facilityId;
  String documentCreatedDateTime;

  factory YardModel.fromMap(Map<String, dynamic> json) => YardModel(
        id: json["id"] == null ? null : json["id"],
        yardName: json["yard_name"] == null ? null : json["yard_name"],
        facilityId: json["facility_id"] == null ? null : json["facility_id"],
        documentCreatedDateTime: json["document_created_date_time"] == null ? null : json["document_created_date_time"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "name": yardName == null ? null : yardName,
        "facility_id": facilityId == null ? null : facilityId,
        "document_created_date_time": documentCreatedDateTime == null ? null : documentCreatedDateTime,
      };

  @override
  String toString() {
    return 'YardModel{id: $id, yardName: $yardName, facilityId: $facilityId, documentCreatedDateTime: $documentCreatedDateTime}';
  }
}
