class ReasonCode {
  ReasonCode({
    this.code,
    this.reason_code_id,
  });

  String code;
  String reason_code_id;

  ReasonCode fromMap(Map<String, dynamic> json) {
    return ReasonCode(
      code: json["code"] == null ? null : json["code"],
      reason_code_id: json["reason_code_id"] == null ? null : json["reason_code_id"],
    );
  }

  Map<String, dynamic> toMap() => {
        "code": code == null ? null : code,
        "reason_code_id": reason_code_id == null ? null : reason_code_id,
      };

  @override
  String toString() {
    return 'ReasonCode{code: $code, reason_code_id: $reason_code_id}';
  }
}
