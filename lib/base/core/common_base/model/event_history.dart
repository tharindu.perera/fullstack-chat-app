import 'package:mbase/base/core/util/date_time_util.dart';

class EventHistory {
  EventHistory({
    this.active_asset_id,
    this.asset_id,
    this.bol_control_nbr,
    this.created_dt,
    this.equip_init,
    this.equip_nbr,
    this.event_date,
    this.event_type_code,
    this.event_type_description,
    this.event_type_id,
    this.facility_id,
    this.facility_visit_id,
    this.from_asset_location_id,
    this.from_compartment_seq,
    this.from_customer_id,
    this.from_inventory_location_id,
    this.id,
    this.inventory_id,
    this.load_status,
    this.product_id,
    this.quantity,
    this.scy_pk,
    this.to_asset_location_id,
    this.to_asset_location_type,
    this.to_compartment_seq,
    this.to_customer_id,
    this.to_inventory_location_id,
    this.to_inventory_location_type,
    this.uom_id,
    this.note_list,
  });

  String active_asset_id;
  String asset_id;
  String bol_control_nbr;
  String created_dt;
  String equip_init;
  String equip_nbr;
  String event_date;
  String event_type_code;
  String event_type_description;
  String event_type_id;
  String facility_id;
  String facility_visit_id;
  String from_asset_location_id;
  String from_compartment_seq;
  String from_customer_id;
  String from_inventory_location_id;
  String id;
  String inventory_id;
  String load_status;
  String product_id;
  String quantity;
  String scy_pk;
  String to_asset_location_id;
  String to_asset_location_type;
  String to_compartment_seq;
  String to_customer_id;
  String to_inventory_location_id;
  String to_inventory_location_type;
  String uom_id;
  String note_list;

  factory EventHistory.fromMap(Map<String, dynamic> json) => EventHistory(
        active_asset_id:
            json["active_asset_id"] == null ? null : json["active_asset_id"],
        asset_id: json["asset_id"] == null ? null : json["asset_id"],
        bol_control_nbr:
            json["bol_control_nbr"] == null ? null : json["bol_control_nbr"],
        created_dt: json["created_dt"] == null ? null : getLocalDateTimeFromUTCStr(json["created_dt"]).toString(),
        equip_init: json["equip_init"] == null ? null : json["equip_init"],
        equip_nbr: json["equip_nbr"] == null ? null : json["equip_nbr"],
        event_date: json["event_date"] == null ? null : getLocalDateTimeFromUTCStr(json["event_date"]).toString(),
        event_type_code:
            json["event_type_code"] == null ? null : json["event_type_code"],
        event_type_description: json["event_type_description"] == null
            ? null
            : json["event_type_description"],
        event_type_id:
            json["event_type_id"] == null ? null : json["event_type_id"],
        facility_id: json["facility_id"] == null ? null : json["facility_id"],
        facility_visit_id: json["facility_visit_id"] == null
            ? null
            : json["facility_visit_id"],
        from_asset_location_id: json["from_asset_location_id"] == null
            ? null
            : json["from_asset_location_id"],
        from_compartment_seq: json["from_compartment_seq"] == null
            ? null
            : json["from_compartment_seq"],
        from_customer_id:
            json["from_customer_id"] == null ? null : json["from_customer_id"],
        from_inventory_location_id: json["from_inventory_location_id"] == null
            ? null
            : json["from_inventory_location_id"],
        id: json["id"] == null ? null : json["id"],
        inventory_id:
            json["inventory_id"] == null ? null : json["inventory_id"],
        load_status: json["load_status"] == null ? null : json["load_status"],
        product_id: json["product_id"] == null ? null : json["product_id"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        scy_pk: json["scy_pk"] == null ? null : json["scy_pk"],
//        note_list: json["note_list"] == null ? null : json["note_list"],
        to_asset_location_id: json["to_asset_location_id"] == null
            ? null
            : json["to_asset_location_id"],
        to_asset_location_type: json["to_asset_location_type"] == null
            ? null
            : json["to_asset_location_type"],
        to_compartment_seq: json["to_compartment_seq"] == null
            ? null
            : json["to_compartment_seq"],
        to_customer_id:
            json["to_customer_id"] == null ? null : json["to_customer_id"],
        to_inventory_location_id: json["to_inventory_location_id"] == null
            ? null
            : json["to_inventory_location_id"],
        to_inventory_location_type: json["to_inventory_location_type"] == null
            ? null
            : json["to_inventory_location_type"],
        uom_id: json["uom_id"] == null ? null : json["uom_id"],
      );

  @override
  String toString() {
    return 'EventHistory{active_asset_id: $active_asset_id, asset_id: $asset_id, bol_control_nbr: $bol_control_nbr, created_dt: $created_dt, equip_init: $equip_init, equip_nbr: $equip_nbr, event_date: $event_date, event_type_code: $event_type_code, event_type_description: $event_type_description, event_type_id: $event_type_id, facility_id: $facility_id, facility_visit_id: $facility_visit_id, from_asset_location_id: $from_asset_location_id, from_compartment_seq: $from_compartment_seq, from_customer_id: $from_customer_id, from_inventory_location_id: $from_inventory_location_id, id: $id, inventory_id: $inventory_id, load_status: $load_status, product_id: $product_id, quantity: $quantity, scy_pk: $scy_pk, to_asset_location_id: $to_asset_location_id, to_asset_location_type: $to_asset_location_type, to_compartment_seq: $to_compartment_seq, to_customer_id: $to_customer_id, to_inventory_location_id: $to_inventory_location_id, to_inventory_location_type: $to_inventory_location_type, uom_id: $uom_id}';
  }
}
