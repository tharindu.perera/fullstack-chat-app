import 'dart:convert';

VCFHeaderModel vcfModelFromMap(String str) =>
    VCFHeaderModel.fromMap(json.decode(str));

class VCFHeaderModel {
  String id;
  String description;
  DateTime created_dt;

  VCFHeaderModel({this.id, this.description, this.created_dt});

  VCFHeaderModel.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        description = map['description'],
        created_dt = map['created_dt'] == null
            ? null
            : DateTime.parse(map['created_dt']);

  @override
  String toString() {
    return 'VCFModel{id: $id, description: $description}';
  }
}

class VCFDetailModel {
  String vcf_header_id;
  String vcf_detail_id;
  String temperature;
  String temperature_scale;
  String gravity;
  String vcf;

  VCFDetailModel(
      {this.vcf_detail_id,
      this.vcf_header_id,
      this.temperature,
      this.temperature_scale,
      this.gravity,
      this.vcf});

  static VCFDetailModel fromMap(Map<String, dynamic> json) {
    return VCFDetailModel(
        vcf_header_id:
            json["vfc_header_id"] == null ? null : json["vfc_header_id"],
        vcf_detail_id:
            json["id"] == null ? null : json["id"],
        temperature: json["temperature"] == null ? null : json["temperature"],
        temperature_scale: json["temperature_scale"] == null
            ? null
            : json["temperature_scale"],
        gravity: json["gravity"] == null ? null : json["gravity"],
        vcf: json["vcf"] == null ? null : json["vcf"]);
  }

  @override
  String toString() {
    return 'VCFDetails{vcf_header_id: $vcf_header_id, vcf_detail_id: $vcf_detail_id, temperature: $temperature, gravity: $gravity, vcf: $vcf, temperature_scale: $temperature_scale}';
  }
}
