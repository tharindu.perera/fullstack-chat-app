class AssignedEntryField {
  AssignedEntryField({
    this.id,
    this.facilityId,
    this.eventTypeId,
    this.assetTypeId,
    this.entryFieldId,
    this.required,
    this.isDeleted,
    this.entryFieldType,
    this.avoidDuplicates,
    this.label,
  });

  String id;
  String facilityId;
  String eventTypeId;
  String assetTypeId;
  String entryFieldId;
  String required;
  String isDeleted;
  String entryFieldType;
  String avoidDuplicates;
  String label;

  factory AssignedEntryField.fromMap(Map<String, dynamic> json) => AssignedEntryField(
        id: json["id"] == null ? null : json["id"],
        facilityId: json["facility_id"] == null ? null : json["facility_id"],
        eventTypeId: json["event_type_id"] == null ? null : json["event_type_id"],
        assetTypeId: json["asset_type_id"] == null ? null : json["asset_type_id"],
        entryFieldId: json["entry_field_id"] == null ? null : json["entry_field_id"],
        required: json["required"] == null ? null : json["required"],
        isDeleted: json["isDeleted"] == null ? null : json["isDeleted"],
        entryFieldType: json["entry_field_type"] == null ? null : json["entry_field_type"],
        avoidDuplicates: json["avoidDuplicates"] == null ? null : json["avoidDuplicates"],
        label: json["label"] == null ? null : json["label"],
      );

  @override
  String toString() {
    return 'AssignedEntryField{id: $id, lable: $label}';
  }
}
