import 'dart:convert';

TankCarSpecificationModel tankCarSpecificationModelFromMap(String str) =>
    TankCarSpecificationModel.fromMap(json.decode(str));

class TankCarSpecificationModel {
  TankCarSpecificationModel(
      {this.id,
      this.shipping_contain_spec,
      this.summer_temp,
      this.temp_scale,
      this.winter_temp,
      this.created_dt});

  String id;
  String shipping_contain_spec;
  String summer_temp;
  String temp_scale;
  String winter_temp;
  DateTime created_dt;

  TankCarSpecificationModel.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        shipping_contain_spec = map['shipping_contain_spec'],
        summer_temp = map['summer_temp'],
        temp_scale = map['temp_scale'],
        winter_temp = map['winter_temp'],
        created_dt = map['created_dt'] == null
            ? null
            : DateTime.parse(map['created_dt']);

  @override
  String toString() {
    return 'ProductAttributesModel{id: $id, shipping_contain_spec: $shipping_contain_spec, summer_temp: $summer_temp, temp_scale: $temp_scale,  winter_temp: $winter_temp }';
  }
}
