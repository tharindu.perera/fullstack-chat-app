import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class Offline extends Equatable {
  Offline({
        this.id,
        this.createdDt,
        this.user
  });

  String id;
  DateTime createdDt;
  String user;
  String operation_type;
  List actions=[];

  @override
  List<Object> get props => [id];

  Map<String, dynamic> toMap() => {
    "id": id,
    "createdDt": createdDt == null ? null : createdDt.toUtc().toString(),
    "user": user,
    "operation_type": operation_type,
    "actions": actions,
  };




}
