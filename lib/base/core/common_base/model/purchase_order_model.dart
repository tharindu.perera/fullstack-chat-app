// To parse this JSON data, do

import 'dart:convert';

PurchaseOrderModel purchaseOrderModelFromMap(String str) => PurchaseOrderModel.fromMap(json.decode(str));

class PurchaseOrderModel {
  PurchaseOrderModel({
    this.id,
    this.order_line_id,
    this.facility_id,
    this.customer_id,
    this.client_id,
    this.purchaseOrderNumber,
    this.order_status_id,
    this.productId,
    this.quantity,
    this.remainingQuantity,
    this.uomId,
    this.documentCreatedDateTime,
    this.order_status,
    this.inbound_outbound_ind,
  });

  String id;
  String order_line_id;
  String purchaseOrderNumber;
  String facility_id;
  String customer_id;
  String client_id;
  String order_status_id;
  String productId;
  String quantity;
  String remainingQuantity;
  String uomId;
  String documentCreatedDateTime;
  String order_status;
  String inbound_outbound_ind;

  factory PurchaseOrderModel.fromMap(Map<String, dynamic> json) => PurchaseOrderModel(
        id: json["id"] == null ? null : json["id"],
        purchaseOrderNumber: json["purchase_order_number"] == null ? null : json["purchase_order_number"],
        order_line_id: json["order_line_id"] == null ? null : json["order_line_id"],
        customer_id: json["customer_id"] == null ? null : json["customer_id"],
        client_id: json["client_id"] == null ? null : json["client_id"],
        order_status_id: json["order_status_id"] == null ? null : json["order_status_id"],
        facility_id: json["facility_id"] == null ? null : json["facility_id"],
        productId: json["product_id"] == null ? null : json["product_id"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        remainingQuantity: json["remaining_quantity"] == null ? null : json["remaining_quantity"],
        uomId: json["uom_id"] == null ? null : json["uom_id"],
        documentCreatedDateTime: json["document_created_date_time"] == null ? null : json["document_created_date_time"],
        order_status: json["order_status"] == null ? null : json["order_status"],
        inbound_outbound_ind: json["inbound_outbound_ind"] == null ? null : json["inbound_outbound_ind"],
      );

  @override
  String toString() {
    return 'PurchaseOrderModel{purchaseOrderId: $id}';
  }
}
