import 'ReasonCode.dart';

class ClientModel {
  ClientModel({this.id, this.client_name, this.created_dt, this.updated_dt, this.reason_codes});

  String id;
  String client_name;
  String created_dt;
  String updated_dt;
  List<ReasonCode> reason_codes = List<ReasonCode>();


  factory ClientModel.fromMap(Map<String, dynamic> json) => ClientModel(
    id: json["id"] == null ? null : json["id"],
    client_name: json["client_name"] == null ? null : json["client_name"],
    created_dt: json["created_dt"] == null ? null : json["created_dt"],
    updated_dt: json["updated_dt"] == null ? null : json["updated_dt"],
    reason_codes: json["reason_codes"] == null ? null : List<ReasonCode>.from(json["reason_codes"].map((x) => ReasonCode().fromMap(x))),
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "client_name": client_name == null ? null : client_name,
    "created_dt": created_dt == null ? null : created_dt,
    "updated_dt": updated_dt == null ? null : updated_dt,
    "reason_codes": reason_codes == null ? null :reason_codes.map((i) => i.toMap()).toList(),
  };

  @override
  String toString() {
    return 'FacilityModel{id: $id, client_name: $client_name, created_dt: $created_dt, updated_dt: $updated_dt, reason_codes: $reason_codes}';
  }


}