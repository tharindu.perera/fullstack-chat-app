class ProductRestrictionModel {
  ProductRestrictionModel({
    this.id,
    this.allow_disallow,
    this.restricted_product_id,
    this.facility_id,
    this.last_contained_product_id,
    this.createdDt,
    this.updatedDt
  });

  String id;
  String allow_disallow;
  String restricted_product_id;
  String facility_id;
  String last_contained_product_id;
  String createdDt;
  String updatedDt;

  ProductRestrictionModel.fromJson(Map<String, dynamic> value)
      : id = value["id"],
        allow_disallow = value["allow_disallow"],
        restricted_product_id = value["restricted_product_id"],
        facility_id = value["facility_id"],
        last_contained_product_id = value["last_contained_product_id"],
        createdDt = value["created_dt:"],
        updatedDt = value["updated_dt:"];

  factory ProductRestrictionModel.fromMap(Map<String, dynamic> json) => ProductRestrictionModel(
    id: json["id"] == null ? null : json["id"],
    allow_disallow: json["allow_disallow"] == null ? null : json["allow_disallow"],
    restricted_product_id: json["restricted_product_id"] == null ? null : json["restricted_product_id"],
    facility_id: json["facility_id"] == null ? null : json["facility_id"],
    last_contained_product_id: json["last_contained_product_id"] == null ? null : json["last_contained_product_id"],
    createdDt: json["created_dt"] == null ? null : json["created_dt"],
    updatedDt: json["updated_dt"] == null ? null : json["updated_dt"],
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "allow_disallow": allow_disallow == null ? null : allow_disallow,
    "restricted_product_id": restricted_product_id == null ? null : restricted_product_id,
    "facility_id": facility_id == null ? null : facility_id,
    "last_contained_product_id":last_contained_product_id == null? null: last_contained_product_id,
    "created_dt": createdDt == null ? null : createdDt,
    "updated_dt": updatedDt == null ? null : updatedDt,
  };
}
