class CustomernModel {
  CustomernModel({
    this.id,
    this.name,
    this.client_id,
    this.active,
    this.customer_type_id,
    this.is_restricted,
    this.createdDt,
    this.updatedDt,
    this.customer_location_list
  });

  String id;
  String name;
  String client_id;
  String active;
  String customer_type_id;
  String is_restricted;
  String createdDt;
  String updatedDt;
  List<String> customer_location_list;
  CustomernModel.fromJson(Map<String, dynamic> value)
      : id = value["id"],
        name = value["name"],
        client_id = value["client_id"],
        active = value["active"],
        customer_type_id = value["customer_type_id"],
        is_restricted = value["is_restricted"],
        createdDt = value["created_dt:"],
        updatedDt = value["updated_dt:"],
        customer_location_list = value['customer_location_list'];

  factory CustomernModel.fromMap(Map<String, dynamic> json) => CustomernModel(
      id: json["id"] == null ? null : json["id"],
      name: json["name"] == null ? null : json["name"],
      client_id: json["client_id"] == null ? null : json["client_id"],
      active: json["active"] == null ? null : json["active"],
      customer_type_id: json["customer_type_id"] == null ? null : json["purchase_order_number"],
      is_restricted: json["is_restricted"] == null ? null : json["is_restricted"],
      customer_location_list: json["customer_location_list"] == null ? null : List<String>.from(json["customer_location_list"].map((x) => x)),
      createdDt: json["createdDt"] == null ? null : json["createdDt"],
      updatedDt: json["updatedDt"] == null ? null : json["updatedDt"]);
}
