import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/dao/spot_dao.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/track_model.dart';

class TrackDAO extends BaseDao {
  TrackDAO() : super("track");

  Future<List<TrackModel>> fetchAllTracks() async {
    QuerySnapshot qShot = await BaseQuery().get();
    return qShot.docs.map((doc) => TrackModel.fromMap(doc.data())).toList();
  }

  Future<List<TrackModel>> fetchTracksByYardId(String yardId) async {
    QuerySnapshot qShot =
        await BaseQuery().where("yard_id", isEqualTo: yardId).get();
    return qShot.docs.map((doc) => TrackModel.fromMap(doc.data())).toList();
  }

  Future<List<TrackModel>> fetchTracksWithoutSpots() async {
    QuerySnapshot qShot = await BaseQuery().get();
    List<TrackModel> trackList =  qShot.docs.map((doc) => TrackModel.fromMap(doc.data())).toList();

    SpotDao spotDao = new SpotDao();
    List<TrackModel> removeTracks = [];
    for(TrackModel trackModel in trackList) {
      List<SpotModel> spotList = await spotDao.fetchSpotsByTrackId(trackModel.id);

      if (spotList.length > 0) {
        removeTracks.add(trackModel);
      }
    }

    for (TrackModel trackModel2 in removeTracks) {
      trackList.remove(trackModel2);
    }
    return trackList;
  }
}
