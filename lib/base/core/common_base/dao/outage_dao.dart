import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/outage_model.dart';
import 'package:mbase/base/core/common_base/model/product_attributes_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/vcf_model.dart';

class OutageDao extends BaseDao {
  OutageDao() : super("outage");

  Future<List<OutageModel>> fetchAllOutages() async {
    QuerySnapshot qShot = await collection.get();
    return qShot.docs.map((doc) => OutageModel.fromMap(doc.data())).toList();
  }

  Future<OutageModel> fetchOutageById(String assetMasterid) async {
    try {
      QuerySnapshot qShot = await collection.where("asset_master_id", isEqualTo: assetMasterid).get();
      if (qShot.docs.isNotEmpty) {
        return OutageModel.fromMap(qShot.docs[0].data());
      } else {
        return null;
      }
    } on Exception {
      throw Exception('Error in fetching Outage by Asset master Id :$assetMasterid');
    }
  }

  Future<OutageDetails> fetchOutageDetailsByAssetIdAndMeasurement(String assetMasterid, String measurement) async {
      try {
        QuerySnapshot qShot = await collection.where('asset_master_id', isEqualTo: assetMasterid).get();

        if (qShot.docs.isNotEmpty) {
          OutageModel outageModel = OutageModel.fromMap(qShot.docs[0].data());
          return outageModel.details.firstWhere((element) => double.parse(measurement) - 0.10 <= double.parse(element.measurement)
          && double.parse(measurement) + 0.10 >= double.parse(element.measurement), orElse: () => null);
        } else {
          return null;
        }
      } on Exception {
        throw Exception('Error in fetching VCF Details by Asset master Id :$assetMasterid and measurement: $measurement');
      }
    }

  Future<OutageDetails> fetchOutageDetailsByAssetIdAndOutage(String assetMasterid, String outage) async {
      try {
        QuerySnapshot qShot = await collection.where('asset_master_id', isEqualTo: assetMasterid).get();

        if (qShot.docs.isNotEmpty) {
          OutageModel outageModel = OutageModel.fromMap(qShot.docs[0].data());
          return outageModel.details.firstWhere((element) => double.parse(outage) + 10 >= double.parse(element.outage_amount)
              && double.parse(outage) - 10 <= double.parse(element.outage_amount), orElse: () => null);
        } else {
          return null;
        }
      } on Exception {
        throw Exception('Error in fetching VCF Details by Asset master Id :$assetMasterid and outage: $outage');
      }
    }
}
