import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/facility_model.dart';
import 'package:mbase/env.dart';

class FacilityDao extends BaseDao {
  FacilityDao() : super("facility");

  Future<List<FacilityModel>>  fetchFacilityList(ids) async {
      return (await collection.where("id", whereIn: ids)
          .where("client_id", isEqualTo: env.userProfile.clientId).get()).docs
          .map((e) => FacilityModel.fromMap(e.data())).toList();
  }
}
