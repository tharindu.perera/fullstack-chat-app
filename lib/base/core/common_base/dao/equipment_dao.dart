import 'dart:async';

import "package:async/async.dart" show StreamZip;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/facility_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/common_base/repositories/offline_data_repository.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/modules/inspection/model/inspection_railcar_model.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/assigned_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_defect/model/assigned_defect.dart';
import 'package:mbase/base/modules/listview/actions/comment/model/comment_model.dart';
import 'package:mbase/base/modules/load_vcf/model/load_vcf_model.dart';
import 'package:uuid/uuid.dart';

import '../../../../env.dart';

class EquipmentDAO extends BaseDao {
  EquipmentDAO() : super("equipment");
  List<Equipment> filteredRailcars = [];

  @override
  Future<Equipment> getRecordByID(String recordID) async {
    return await BaseQuery()
        .where("equipment_id", isEqualTo: recordID)
        .get()
        .then((value) => Equipment.fromMap(value.docs[0].data() ?? null));
  }

  Future<List<Equipment>> geEquipmentByTrackID(String trackId,String yardId) async {
    print("Fetching Equipment By TrackID ......");
    return (await BaseQuery()
        .where("track_id", isEqualTo: trackId)
        .where("yard_id", isEqualTo: yardId)
        .where("permission_id", isEqualTo: APP_CONST.PUBLIC)
        .get())
        .docs
        .map((snapshot) {
      return Equipment.fromMap(snapshot.data());
    }).toList();
  }

    Future<Equipment> add(Equipment equipment) async {
    equipment.id = Uuid().v1();
    OfflineDataRepository.persistToDataStore(equipment, collection);
    return equipment;
  }

  Future<Equipment> checkForSpotAvailability(
      {String Yard, String Track, String Spot}) async {
    try {
      QuerySnapshot snapshot = await BaseQuery()
          .where("move.to_yard_id", isEqualTo: Yard)
          .where("move.to_track_id", isEqualTo: Track)
          .where("move.to_spot_id", isEqualTo: Spot)
          .get();
      if (snapshot.docs.isNotEmpty) {
        return Equipment.fromMap(snapshot.docs[0].data());
      } else {
        return null;
      }
    } on Exception {
      throw Exception('Error in checking Equipment Number');
    }
  }

  Future<Equipment> update(Equipment equipment) async {
    OfflineDataRepository.persistToDataStore(equipment, collection);
    return equipment;
  }

  Future<List<Equipment>> getRecords() async {
    return (await BaseQuery()
            .where("permission_id", isEqualTo: APP_CONST.PUBLIC)
            .get())
        .docs
        .map((snapshot) {
      return Equipment.fromMap(snapshot.data());
    }).toList();
  }

  Future<Equipment> getEquipmentByInitAndNumber(
      String equipmentInitial, String equipmentNumber) async {
    try {
      QuerySnapshot snapshot = await BaseQuery()
          .where("equipment_initial", isEqualTo: equipmentInitial)
          .where("equipment_number", isEqualTo: equipmentNumber)
          .get();
      if (snapshot.docs.isNotEmpty) {
        return Equipment.fromMap(snapshot.docs[0].data());
      } else {
        return null;
      }
    } on Exception {
      throw Exception('Error in checking Equipment Number');
    }
  }

  Future<List<Equipment>> validateSpotSelection(
      {String yardId, String trackId, List<String> spots}) async {
    try {
      QuerySnapshot snapshot = await BaseQuery()
          .where("yard_id", isEqualTo: yardId)
          .where("track_id", isEqualTo: trackId)
          .where("spot_id", whereIn: spots)
          .get();
      if (snapshot.docs.isNotEmpty) {
        return snapshot.docs.map((doc) {
          return Equipment.fromMap(doc.data());
        }).toList();
      } else {
        return null;
      }
    } on Exception {
      throw Exception('Error in checking Equipment Number');
    }
  }

  Future<bool> checkEquipmentNumber(
      String equipmentInitial, String equipmentNumber) async {
    try {
      QuerySnapshot qShot3 = await BaseQuery()
          .where("equipment_initial", isEqualTo: equipmentInitial)
          .where("equipment_number", isEqualTo: equipmentNumber)
          .get();
      return qShot3.docs.length > 0;
    } on Exception {
      throw Exception('Error in checking Equipment Number');
    }
  }

  Future<Equipment> getYardTrack(  String equipmentInitial, String equipmentNumber) async {
    try {
       return BaseQuery()
          .where("equipment_initial", isEqualTo: equipmentInitial)
          .where("equipment_number", isEqualTo: equipmentNumber).limit(1)
          .get().then((value) {
        if (value.docs.isNotEmpty) {
          return Equipment.fromMap(value.docs[0].data());
        } else {
          return null;
        }
      });

    } on Exception {
      throw Exception('Error in checking getYardTrack');
    }
  }

  Stream<List<Equipment>> getRecordByCriteria(Map<String, dynamic> criteria) {
    var query = BaseQuery();
    Stream<List<Equipment>> filteredStream;
    bool filterApplied = false;

    if (criteria.containsKey(APP_CONST.ListView_Filter) || criteria.containsKey(APP_CONST.Filter_By_YardAndTrack)) {
      filteredRailcars =[];
      return filterBySelectionOrder(criteria: criteria);
    }

    criteria?.forEach((key, value) {
      if (value is List) {
        query = query.where(key, whereIn: value);
      } else {
        query = query.where(key, isEqualTo: value);
      }
    });

    if (filterApplied) {
      return filteredStream;
    }

    return query.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return Equipment.fromMap(doc.data());
      }).toList();
    });
  }

  Stream<List<Equipment>> filterBySelectionOrder(
      {Map<String, dynamic> criteria}) {
    String selectedInsecptionStatus = null;
    String selectedBlockToId = null;
    StreamController<List<Equipment>> controller =
        StreamController<List<Equipment>>();

    criteria.entries.forEach((element) {
      if (element.key == APP_CONST.Filter_By_Inspection) {
        selectedInsecptionStatus = element.value;
      } else if (element.key == APP_CONST.Filter_By_Block_To) {
        selectedBlockToId = element.value;
      }
    });
    //Equipment Stream
    Query equipmentListQuery = DatabaseProvider.firestore
        .collection("equipment")
        .where("permission_id", whereIn: [
      APP_CONST.PUBLIC,
      env.userProfile.id
    ]).where("facility_id", isEqualTo: env.userProfile.facilityId);

    var equipmentStream = equipmentListQuery.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return Equipment.fromMap(doc.data());
      }).toList();
    });

    //Inspection Stream
    Query inspectionListQuery = DatabaseProvider.firestore
        .collection("inspection_railcar")
        .where("permission_id", whereIn: [APP_CONST.PUBLIC, env.userProfile.id])
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .where("inspection_status", isEqualTo: selectedInsecptionStatus);

    var inspectionStream = inspectionListQuery.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return InspectionRailcarModel.fromJson(doc.data());
      }).toList();
    });

    //Assigned Defect Stream
    Query equipmentDefectListQuery = DatabaseProvider.firestore
        .collection("assigned_defect")
        .where("permission_id",isEqualTo:APP_CONST.PUBLIC)
        .where("facility_id", isEqualTo: env.userProfile.facilityId);

    var assignedDefectStream = equipmentDefectListQuery.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return AssignedDefect.fromMap(doc.data());
      }).toList();
    });

    //Product Stream
    Query productListQuery = DatabaseProvider.firestore
        .collection("product")
        .where("facility_id", isEqualTo: env.userProfile.facilityId);

    var productStream = productListQuery.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return ProductModel.fromMap(doc.data());
      }).toList();
    });

    //Railcar Comments Stream
    Query equipmentCommentListQuery = DatabaseProvider.firestore
        .collection("listview_comment")
        .where("permission_id",isEqualTo:APP_CONST.PUBLIC);
       // .where("facility_id", isEqualTo: env.userProfile.facilityId);

    var assignedCommentsStream = equipmentCommentListQuery.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        print("fetched comments railcars ---->>>> ${snapshot.docs.length}");
        return Comment.fromMap(doc.data());
      }).toList();
    });

    //Equipment Group Stream
    Query equipmentGroupListQuery = DatabaseProvider.firestore
        .collection("facility")
        .where("client_id", isEqualTo: env.userProfile.clientId)
        .where("id", isEqualTo: env.userProfile.facilityId);

    var equipmentGroupStream = equipmentGroupListQuery.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        print("fetched comments railcars ---->>>> ${snapshot.docs.length}");
        return FacilityModel.fromMap(doc.data());
      }).toList();
    });

    //Load VCF Stream
    Query loadVCFListQuery = DatabaseProvider.firestore
        .collection("load_vcf")
        .where("permission_id", isEqualTo: APP_CONST.PUBLIC)
        .where("facility_id", isEqualTo: env.userProfile.facilityId);

    var loadVCFStream = loadVCFListQuery.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return LoadVCFModel.fromMap(doc.data());
      }).toList();
    });

    //Block to Stream
    Query blockToListQuery = DatabaseProvider.firestore
        .collection("assigned_block")
        .where("permission_id", whereIn: [APP_CONST.PUBLIC, env.userProfile.id])
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .where("railcar_block_id", isEqualTo: selectedBlockToId);

    var blockToStream = blockToListQuery.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return AssignedBlock.fromMap(doc.data());
      }).toList();
    });


    new StreamZip([equipmentStream, inspectionStream, blockToStream,assignedDefectStream,assignedCommentsStream,equipmentGroupStream
      ,productStream,loadVCFStream]).map((ab) {
      List<Equipment> castedEquipList = ab[0] as List<Equipment>;
      List<InspectionRailcarModel> castedInspectionList =
          ab[1] as List<InspectionRailcarModel>;
      List<AssignedBlock> castedAssignedBlockToList =
          ab[2] as List<AssignedBlock>;
      List<AssignedDefect> castedAssignedDefectList = ab[3] as List<AssignedDefect>;

      List<Comment> castedAssignedCommentList = ab[4] as List<Comment>;

      List<FacilityModel> castedEquipmentGroupList = ab[5] as List<FacilityModel>;
      List<ProductModel> castedProductList = ab[6] as List<ProductModel>;
      List<LoadVCFModel> castedLoadVCFList = ab[7] as List<LoadVCFModel>;

      criteria.entries.forEach((element) {
        if(element.key==APP_CONST.Filter_By_Block_To && filteredRailcars.isEmpty ){
          print("APP_CONST.Filter_By_Block_To && filteredRailcars == isEmpty");
          filterByBlock(castedEquipList: castedEquipList,castedAssignedBlockToList: castedAssignedBlockToList);
        }
        else if(element.key==APP_CONST.Filter_By_Block_To && filteredRailcars.isNotEmpty ){
          print("APP_CONST.Filter_By_Block_To && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByBlock(castedEquipList: duplicateFilteredRailcars,castedAssignedBlockToList: castedAssignedBlockToList);
        }
        else if(element.key==APP_CONST.Filter_By_Inspection && filteredRailcars.isEmpty ){
          print("APP_CONST.Filter_By_Inspection && filteredRailcars  isEmpty");
          filterByInspection(castedEquipList: castedEquipList,castedInspectionList: castedInspectionList);
        }
        else if(element.key==APP_CONST.Filter_By_Inspection && filteredRailcars.isNotEmpty){
          print("APP_CONST.Filter_By_Inspection && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByInspection(castedEquipList: duplicateFilteredRailcars,castedInspectionList: castedInspectionList);
        }
        else if(element.key==APP_CONST.Filter_By_Product && filteredRailcars.isEmpty ){
          print("APP_CONST.Filter_By_Product && filteredRailcars isEmpty");
          filterByProduct(castedEquipList: castedEquipList,selectedProduct: criteria[APP_CONST.Filter_By_Product]);
        }
        else if(element.key==APP_CONST.Filter_By_Product && filteredRailcars.isNotEmpty ){
          print("APP_CONST.Filter_By_Product && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByProduct(castedEquipList: duplicateFilteredRailcars,selectedProduct: criteria[APP_CONST.Filter_By_Product]);
        }
        else if(element.key == APP_CONST.Filter_By_Defective && filteredRailcars.isEmpty){
          print("Filter_By_Defective && filteredRailcars isEmpty");
          filterByDefect(castedEquipList: castedEquipList,castedAssignedDefectToList: castedAssignedDefectList);
        }
        else if(element.key == APP_CONST.Filter_By_Defective && filteredRailcars.isNotEmpty){
          print("Filter_By_Defective && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByDefect(castedEquipList: duplicateFilteredRailcars,castedAssignedDefectToList: castedAssignedDefectList);
        }
        else if(element.key == APP_CONST.Filter_By_HasComments && filteredRailcars.isEmpty){
          print("Filter_By_HasComments && filteredRailcars isEmpty");
          filterByHasComments(castedEquipList: castedEquipList,castedAssignedCommentList: castedAssignedCommentList);
        }
        else if(element.key == APP_CONST.Filter_By_HasComments && filteredRailcars.isNotEmpty){
          print("Filter_By_HasComments && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByHasComments(castedEquipList: duplicateFilteredRailcars,castedAssignedCommentList: castedAssignedCommentList);
        }
        else if(element.key == APP_CONST.Filter_By_EquipmentGroup && filteredRailcars.isEmpty){
          print("Filter_By_EquipmentGroup && filteredRailcars isEmpty");
          filterByEquipmentGroup(castedEquipList: castedEquipList,castedEquipmentGroup: castedEquipmentGroupList,selectedEquipmentGroup:criteria[APP_CONST.Filter_By_EquipmentGroup]);
        }
        else if(element.key == APP_CONST.Filter_By_EquipmentGroup && filteredRailcars.isNotEmpty){
          print("Filter_By_EquipmentGroup && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByEquipmentGroup(castedEquipList: duplicateFilteredRailcars,castedEquipmentGroup: castedEquipmentGroupList,selectedEquipmentGroup:criteria[APP_CONST.Filter_By_EquipmentGroup]);
        }
        else if(element.key == APP_CONST.Filter_By_Hazardous && filteredRailcars.isEmpty){
          print("Filter_By_Hazardous && filteredRailcars isEmpty");
          filterByHazardous(castedEquipList: castedEquipList,castedProductList:castedProductList);
        }
        else if(element.key == APP_CONST.Filter_By_Hazardous && filteredRailcars.isNotEmpty){
          print("Filter_By_Hazardous && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByHazardous(castedEquipList: duplicateFilteredRailcars,castedProductList:castedProductList);
        }
        else if(element.key == APP_CONST.Filter_By_OnOrder && filteredRailcars.isEmpty){
          print("Filter_By_OnOrder && filteredRailcars isEmpty");
          filterByOnOrder(castedEquipList: castedEquipList);
        }
        else if(element.key == APP_CONST.Filter_By_OnOrder && filteredRailcars.isNotEmpty){
          print("Filter_By_OnOrder && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByOnOrder(castedEquipList: duplicateFilteredRailcars);
        }
        else if(element.key == APP_CONST.Filter_By_VCFCalculated && filteredRailcars.isEmpty){
          print("Filter_By_VCFCalculated && filteredRailcars isEmpty");
          filterByVCFCalculated(castedEquipList: castedEquipList,castedLoadVCFList: castedLoadVCFList);
        }
        else if(element.key == APP_CONST.Filter_By_VCFCalculated && filteredRailcars.isNotEmpty){
          print("Filter_By_VCFCalculated && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByVCFCalculated(castedEquipList: duplicateFilteredRailcars,castedLoadVCFList: castedLoadVCFList);
        }
        else if(element.key == APP_CONST.Filter_By_HasBOL && filteredRailcars.isEmpty){
          print("Filter_By_HasBOL && filteredRailcars isEmpty");
          filterByHasBol(castedEquipList: castedEquipList);
        }
        else if(element.key == APP_CONST.Filter_By_HasBOL && filteredRailcars.isNotEmpty){
          print("Filter_By_HasBOL && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByHasBol(castedEquipList: duplicateFilteredRailcars);
        }
        else if(element.key == APP_CONST.Filter_By_Loaded && filteredRailcars.isEmpty){
          print("Filter_By_Loaded && filteredRailcars isEmpty");
          filterByLoaded(castedEquipList: castedEquipList);
        }
        else if(element.key == APP_CONST.Filter_By_Loaded && filteredRailcars.isNotEmpty){
          print("Filter_By_Loaded && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByLoaded(castedEquipList: duplicateFilteredRailcars);
        }
        else if(element.key == APP_CONST.Filter_By_LoadEmpty && filteredRailcars.isEmpty){
          print("Filter_By_Loaded && filteredRailcars isEmpty");
          filterByEmpty(castedEquipList: castedEquipList);
        }
        else if(element.key == APP_CONST.Filter_By_LoadEmpty && filteredRailcars.isNotEmpty){
          print("Filter_By_Loaded && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByEmpty(castedEquipList: duplicateFilteredRailcars);
        }
        else if(element.key == APP_CONST.Filter_By_LoadAll && filteredRailcars.isEmpty){
          print("Filter_By_LoadAll && filteredRailcars isEmpty");
          filterByLoadAll(castedEquipList: castedEquipList);
        }
        else if(element.key == APP_CONST.Filter_By_LoadAll && filteredRailcars.isNotEmpty){
          print("Filter_By_LoadAll && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByLoadAll(castedEquipList: duplicateFilteredRailcars);
        }
        else if(element.key == APP_CONST.Filter_By_InboundShipper && filteredRailcars.isEmpty){
          print("Filter_By_InboundShipper && filteredRailcars isEmpty");
          filterByInboundShipperList(castedEquipList:castedEquipList,selectedInboundShipper:criteria[APP_CONST.Filter_By_InboundShipper] );
        }
        else if(element.key == APP_CONST.Filter_By_InboundShipper && filteredRailcars.isNotEmpty){
          print("Filter_By_InboundShipper && filteredRailcars isEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByInboundShipperList(castedEquipList:duplicateFilteredRailcars,selectedInboundShipper:criteria[APP_CONST.Filter_By_InboundShipper] );
        }
        else if(element.key == APP_CONST.Filter_By_OutboundShipper && filteredRailcars.isEmpty){
          print("Filter_By_OutboundShipper && filteredRailcars isEmpty");
          filterByOutboundShipperList(castedEquipList:castedEquipList,selectedOutboundShipper:criteria[APP_CONST.Filter_By_OutboundShipper] );
        }
        else if(element.key == APP_CONST.Filter_By_OutboundShipper && filteredRailcars.isNotEmpty){
          print("Filter_By_OutboundShipper && filteredRailcars isNotEmpty");
          List<Equipment> duplicateFilteredRailcars = List.from(filteredRailcars);
          filterByOutboundShipperList(castedEquipList:duplicateFilteredRailcars,selectedOutboundShipper:criteria[APP_CONST.Filter_By_OutboundShipper] );
        }

        else if(element.key == APP_CONST.Filter_By_YardAndTrack && filteredRailcars.isEmpty){
          filterBySelectedYardAndTrack(castedEquipList: castedEquipList,selectedYards:criteria[APP_CONST.Filter_By_Yard],
              selectedTracks: criteria[APP_CONST.Filter_By_Track]);
        }

      });
      print("filteredRailcars %%%%%%%%%%%%%%%%% ${filteredRailcars.toSet().toList().length}");
      return filteredRailcars.toSet().toList();
    }).listen((event) {
      controller.add(event);
    });

  //  equipmentStream.map((event) {});
    return controller.stream;
  }


  filterBySelectedYardAndTrack({List<Equipment> castedEquipList,List<String> selectedYards,List<String> selectedTracks}){

    if(selectedYards !=null && selectedTracks !=null&& selectedYards.length >0  && selectedTracks.length >0 ) {
      filteredRailcars = [];
      castedEquipList?.forEach((equipment) {
        // if(equipment.yardId ==selectedYard  && equipment.trackId == selectedTrack){
        //   filteredRailcars.add(equipment);
        // }
        if(selectedYards.contains(equipment.yardId) && selectedTracks.contains(equipment.trackId) ){
             filteredRailcars.add(equipment);
        }
      });
    }
   else if(selectedYards !=null && selectedYards.length >0 ){
      filteredRailcars = [];
      castedEquipList?.forEach((equipment) {
        if(selectedYards.contains(equipment.yardId)){
          filteredRailcars.add(equipment);
        }
      });
    }
   else if(selectedTracks !=null && selectedTracks.length > 0 ){
      filteredRailcars = [];
      castedEquipList?.forEach((equipment) {
        if(selectedTracks.contains(equipment.trackId) ){
          filteredRailcars.add(equipment);
        }
      });
    }
  }

  filterByOnOrder({List<Equipment> castedEquipList}){
    filteredRailcars = [];
    castedEquipList?.forEach((equipment) {
      if(equipment?.purchaseOrderId != null){
        filteredRailcars.add(equipment);
      }
    });
  }

  filterByLoaded({List<Equipment> castedEquipList}){
    filteredRailcars = [];
    castedEquipList.forEach((equipment) {
    if(equipment?.compartmentList?.length>0 && double.parse(equipment.compartmentList[0].currentAmount ?? "0") > 0 ){
      filteredRailcars.add(equipment);
    }
  });
  }

  filterByHasBol({List<Equipment> castedEquipList}){
    filteredRailcars = [];
    castedEquipList.forEach((equipment) {
      if(equipment?.bol_control_number != null){
        filteredRailcars.add(equipment);
      }
    });
  }

  filterByEmpty({List<Equipment> castedEquipList}){
    filteredRailcars = [];
    castedEquipList.forEach((equipment) {
      if(equipment?.compartmentList?.length>0 && double.parse(equipment.compartmentList[0].currentAmount ?? "0") < 0 ){
        filteredRailcars.add(equipment);
      }
    });
  }

  filterByInboundShipperList({List<Equipment> castedEquipList,String selectedInboundShipper}){
    filteredRailcars = [];
    castedEquipList.forEach((equipment) {
     if(equipment.inbound_shipper_name == selectedInboundShipper){
       filteredRailcars.add(equipment);
     }
    });
  }

  filterByInboundConsigneeList({List<Equipment> castedEquipList,String selectedInboundConsignee}){
    filteredRailcars = [];
    castedEquipList.forEach((equipment) {
      if(equipment.inbound_consignee_name == selectedInboundConsignee){
        filteredRailcars.add(equipment);
      }
    });
  }

  filterByOutboundShipperList({List<Equipment> castedEquipList,String selectedOutboundShipper}){
    filteredRailcars = [];
    castedEquipList.forEach((equipment) {
      if(equipment.outbound_shipper_name == selectedOutboundShipper){
        filteredRailcars.add(equipment);
      }
    });
  }


  filterByLoadAll({List<Equipment> castedEquipList}){
    filteredRailcars = [];
    castedEquipList.forEach((equipment) {
      if(equipment?.compartmentList?.length>0 && (double.parse(equipment.compartmentList[0].currentAmount ?? "0") > 0
      || double.parse(equipment.compartmentList[0].currentAmount ?? "0") < 0)
      ){
        filteredRailcars.add(equipment);
      }
    });
  }

  filterByVCFCalculated({List<Equipment> castedEquipList,List<LoadVCFModel>castedLoadVCFList}){
    filteredRailcars = [];
    castedEquipList?.forEach((equipment) {
      castedLoadVCFList?.forEach((vcf) {
        if(equipment.assetMasterId == vcf.asset_master_id){
          filteredRailcars.add(equipment);
        }
      });
    });
  }

  filterByEquipmentGroup({List<Equipment> castedEquipList,
    List<FacilityModel> castedEquipmentGroup, String selectedEquipmentGroup}){
    filteredRailcars = [];
    castedEquipList?.forEach((equipment) {
      castedEquipmentGroup.forEach((equip_grp) {
        if(equipment.equipment_group_list != null && equip_grp.equipment_group_list !=null){
          if(equipment?.equipment_group_list?.contains(selectedEquipmentGroup) && equip_grp?.equipment_group_list?.contains(selectedEquipmentGroup)){
            filteredRailcars.add(equipment);
          }
        }

      });
    });
  }


  filterByHazardous({List<Equipment> castedEquipList,
    List<ProductModel> castedProductList}){
    filteredRailcars = [];
    castedEquipList?.forEach((equipment) {
      if( equipment?.compartmentList?.length>0 ) {
        castedProductList.forEach((product) {
          if(product?.productId == equipment?.compartmentList?.first?.productId && product?.hazardous == "1"){
            filteredRailcars.add(equipment);
          }
        });
      }
      });

}

  filterByHasComments({List<Equipment> castedEquipList,
    List<Comment> castedAssignedCommentList}){
    filteredRailcars = [];
    castedEquipList?.forEach((equipment) {
      castedAssignedCommentList.forEach((comment) {
        if (comment?.assetMasterId == equipment?.assetMasterId) {
          filteredRailcars.add(equipment);
        }
      });
    });
  }

  filterByDefect({List<Equipment> castedEquipList,
    List<AssignedDefect> castedAssignedDefectToList}){
    filteredRailcars = [];
    castedEquipList?.forEach((equipment) {
      castedAssignedDefectToList.forEach((defect) {
        if (defect?.asset_master_id == equipment?.assetMasterId) {
          filteredRailcars.add(equipment);
        }
      });
    });
  }


  filterByBlock(
      {List<Equipment> castedEquipList,
      List<AssignedBlock> castedAssignedBlockToList}){
    filteredRailcars = [];
    castedEquipList?.forEach((equipment) {
      castedAssignedBlockToList.forEach((block) {
        if (block?.asset_master_id == equipment?.assetMasterId) {
          filteredRailcars.add(equipment);
        }
      });
    });
  }

  filterByProduct({List<Equipment>castedEquipList, String selectedProduct}){
    filteredRailcars = [];
    castedEquipList?.forEach((equipment) {
       if (equipment?.compartmentList?.isNotEmpty && selectedProduct == equipment?.compartmentList[0]?.productName) {
          filteredRailcars.add(equipment);
        }
    });
    print('filterByProduct ----> ${filteredRailcars.length}');
  }

  filterByInspection({List<Equipment> castedEquipList,
    List<InspectionRailcarModel> castedInspectionList}){
    filteredRailcars = [];
    castedEquipList.forEach((equipment) {
      castedInspectionList?.forEach((inspection) {
        if (inspection?.asset_master_id == equipment?.assetMasterId) {
          filteredRailcars.add(equipment);
        }
      });
    });
    print('filterByInspection ----> ${filteredRailcars.length}');
  }

  Future<List<Equipment>> getRecordsByCriteria(
      Map<String, dynamic> criteria) async {
    var query = BaseQuery();
    criteria?.forEach((key, value) {
      if (value is List) {
        query = query.where(key, whereIn: value);
      } else {
        query = query.where(key, isEqualTo: value);
      }
    });

    return (await query.get()).docs.map((snapshot) {
      return Equipment.fromMap(snapshot.data());
    }).toList();
  }
}
