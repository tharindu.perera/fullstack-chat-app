import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';

class SpotDao extends BaseDao {
  SpotDao() : super("spot");

  Future<List<SpotModel>> fetchAllSpots() async {
    QuerySnapshot qShot = await BaseQuery().get();
    return qShot.docs.map((doc) => SpotModel.fromMap(doc.data())).toList();
  }

  Future<SpotModel> fetchSpotById(id) async {
    print("Fetching Spot By Id ......");
    QuerySnapshot qShot = await BaseQuery().where("id", isEqualTo: id).get();
    if (qShot.docs.isNotEmpty) {
      return SpotModel.fromMap(qShot.docs.first.data());
    }
    {
      return null;
    }
  }

  Future<List<SpotModel>> fetchSpotsByTrackId(String trackId) async {
    QuerySnapshot qShot = await BaseQuery().where("track_id", isEqualTo: trackId).get();
    return qShot.docs.map((doc) => SpotModel.fromMap(doc.data())).toList();
  }
}
