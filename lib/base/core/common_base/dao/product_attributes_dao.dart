import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/product_attributes_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';

class ProductAttributesDao extends BaseDao {
  ProductAttributesDao() : super("product_attribute");

  Future<List<ProductAttributesModel>> fetchAllProductAttributes(String facilityId) async {
    QuerySnapshot qShot;
    if (null != facilityId && facilityId.isNotEmpty) {
      qShot = await collection.where("facility_id", isEqualTo: facilityId).get();
    } else {
      qShot = await collection.orderBy("product_name", descending: false).get();
    }
    List<ProductAttributesModel> unsortedList = qShot.docs.map((doc) => ProductAttributesModel.fromMap(doc.data())).toList();
    unsortedList..sort((a,b)=> a.product_name.toLowerCase().compareTo(b.product_name.toLowerCase()));
    return unsortedList;
  }

  Future<List<ProductAttributesModel>> fetchAssetById(String id) async {
    QuerySnapshot qShot = await collection.where("id", isEqualTo: id).get();
    return qShot.docs.map((doc) => ProductAttributesModel.fromMap(doc.data())).toList();
  }

  Future<List<ProductAttributesModel>> fetchAssetByProductId(String productId) async {
    QuerySnapshot qShot = await collection.where("product_id", isEqualTo: productId).get();
    return qShot.docs.map((doc) => ProductAttributesModel.fromMap(doc.data())).toList();
  }
}
