import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/facility_configuration_model.dart';

class FacilityConfigurationDao extends BaseDao {
  FacilityConfigurationDao() : super("facility_configuration");

  Future<FacilityConfigurationModel> fetchFacilityConfiguration(facilityId) async {
    var querySnapshot = await collection.where("facility_id", isEqualTo: facilityId).get();
    if (querySnapshot.size > 0) {
      return (await collection.where("facility_id", isEqualTo: facilityId).get()).docs.map((e) => FacilityConfigurationModel.fromMap(e.data())).first;
    } else {
      return null;
    }
  }
}
