import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';

class AssetMasterDataDao extends BaseDao {
  AssetMasterDataDao() : super("asset_master");

  Future<List<AssetMasterDataModel>> fetchAllAssetMasterData() async {
    QuerySnapshot qShot = await collection.get();
    return qShot.docs
        .map((doc) => AssetMasterDataModel.fromMap(doc.data()))
        .toList();
  }

  Future<List<AssetMasterDataModel>> fetchAssetById(String id) async {
    print("Fetching Asset By Id......");

    QuerySnapshot qShot = await collection.where("id", isEqualTo: id).get();
    return qShot.docs
        .map((doc) => AssetMasterDataModel.fromMap(doc.data()))
        .toList();
  }

  Future<AssetMasterDataModel> fetchAssetByEquipmentIntialAndNumber(
      String initials, String number) async {
    try {
      QuerySnapshot qShot = await collection
          .where("equip_init", isEqualTo: initials)
          .where("equip_nbr", isEqualTo: number)
          .get();
      if (qShot.docs.isNotEmpty) {
        return AssetMasterDataModel.fromMap(qShot.docs[0].data());
      } else {
        return null;
      }
    } on Exception {
      throw Exception('Error in checking getYardTrack');
    }
  }
}
