import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/asset_master_data_model.dart';
import 'package:mbase/base/core/common_base/model/product_attributes_model.dart';
import 'package:mbase/base/core/common_base/model/spot_model.dart';
import 'package:mbase/base/core/common_base/model/vcf_model.dart';

class VCFDao extends BaseDao {
  VCFDao() : super('vcf');

  Future<List<VCFModel>> fetchAllVCF() async {
    QuerySnapshot qShot = await collection.get();
    return qShot.docs.map((doc) => VCFModel.fromMap(doc.data())).toList();
  }

  Future<VCFModel> fetchVCFById(String vcfId) async {
    try {
      QuerySnapshot qShot = await collection.where('id', isEqualTo: vcfId).get();
      if (qShot.docs.isNotEmpty) {
        return VCFModel.fromMap(qShot.docs[0].data());
      } else {
        return null;
      }
    } on Exception {
      throw Exception('Error in fetching VCF by ID :$vcfId');
    }
  }

  Future<VCFDetails> fetchVCFDetailsByGravityAndTemp(String vcfId, String gravity, String temp) async {
    try {
      QuerySnapshot qShot = await collection.where('id', isEqualTo: vcfId).get();
      
      if (qShot.docs.isNotEmpty) {
        VCFModel vcfModel = VCFModel.fromMap(qShot.docs[0].data());
        return vcfModel.details.firstWhere((element) => double.parse(gravity) == double.parse(element.gravity)
            && double.parse(element.temperature) == double.parse(temp), orElse: () => null);
      } else {
        return null;
      }
    } on Exception {
      throw Exception('Error in fetching VCF by ID :$vcfId');
    }
  }
}
