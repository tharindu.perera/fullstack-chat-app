import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/tank_specification_model.dart';
import 'package:mbase/base/core/util/nullCheckUtil.dart';

class TankCarSpecDao extends BaseDao {
  TankCarSpecDao() : super("tank_car_spec");

  Future<List<TankCarSpecificationModel>> fetchAllTankCarSpecs() async {
    QuerySnapshot qShot = await collection.get();
    return qShot.docs
        .map((doc) => TankCarSpecificationModel.fromMap(doc.data()))
        .toList();
  }

  Future<TankCarSpecificationModel> fetchAssetByShippingContainSpec(String dotNumber) async {
    try {
      // String fromQuery;
      // if (dotNumber.length >= 4) {
      //   fromQuery = dotNumber.substring(0,4);
      // } else {
      //   fromQuery = dotNumber;
      // }

      // QuerySnapshot qShot = await collection
      //     .where('shipping_contain_spec', isGreaterThanOrEqualTo: fromQuery)
      //     .where('shipping_contain_spec', isLessThan: dotNumber)
      //     .get();


      QuerySnapshot qShot = await collection.get();
      List<TankCarSpecificationModel> list =  qShot.docs
          .map((doc) => TankCarSpecificationModel.fromMap(doc.data()))
          .toList();

      if (qShot.docs.isNotEmpty) {
          for (int i = 0; i < qShot.docs.length; i++) {
            TankCarSpecificationModel temp = TankCarSpecificationModel.fromMap(qShot.docs[i].data());
            if (temp != null && !isNullOrEmpty(temp.shipping_contain_spec)) {
              if (dotNumber.startsWith(temp.shipping_contain_spec, 0)) {
                return temp;
              }
            }
          }

          return null;
      } else {
        return null;
      }
    } on Exception {
      throw Exception('Error in fetching DOT Spec temperature by DOT number :$dotNumber');
    }
  }
}
