import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/env.dart';

abstract class BaseDao {
  FirebaseFirestore _firestore = DatabaseProvider.firestore;

  CollectionReference collection;

  BaseDao(collectionName) {
    collection = _firestore.collection(collectionName);
    
  }

  Query BaseQuery() {
    return collection.where("facility_id", isEqualTo: env.userProfile.facilityId);
  }
}
