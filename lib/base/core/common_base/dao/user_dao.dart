import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/user_profile_model.dart';

class UserDAO extends BaseDao {
  var userProfileCollection;

  UserDAO() : super("user_profile") {
    userProfileCollection = collection;
  }

  Future<UserProfile> getRecordByID(String uid) async {
    return await userProfileCollection.doc(uid).get().then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        return UserProfile.fromSnapshot(documentSnapshot);
      } else {
        return null;
      }
    });
  }

  Future<UserProfile> add(UserProfile record) {
    //    firestore.collection("user_profile").doc("e4gu3XcmLphucolPnndosH448Dj2").set({
//      "type": "User_Profile",
//      "first_name": "Chandika",
//      "last_name": "Gunawardhana",
//      "user_id": "111112",
//      "user_name": "chandika",
//      "facility_id_list" : ["Facility:1", "Facility:2", "Facility:3"],
//      "screen_id_list": ["Screen:ListView", "Screen:SwitchList", "Screen:Inspection", "Screen:Seal"],
//      "screen_action_id_list": ["ListView:InspectRailcars", "ListView:LoadRailcar"],
//      "active": "Y",
//      "client_id": "Client:1",
//      "created_dt": "20200622 130710",
//      "updated_dt": "20200622 190710",
//      "_rev": "1-2955a244f781dd337c8f465bdf6136a5",
//      "_id": "User_Profile:111112",
//      "source": "scy",
//      "channels": ["111112","Client:1_Facility:1_Equipment","organization_facility"],
//      "channel": "111112"
//    });
  }

  Future<UserProfile> update(UserProfile record) {
    userProfileCollection.doc('ABC123').update({'company': 'Stokes and Sons'}).then((value) => print("User Updated")).catchError((error) => print("Failed to update user: $error"));
  }
}
