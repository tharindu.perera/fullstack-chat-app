import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/vcf_header_model.dart';

class VCFHeaderDao extends BaseDao {
  VCFHeaderDao() : super('vcf_header');

  Future<List<VCFHeaderModel>> fetchAllVCF() async {
    QuerySnapshot qShot = await collection.get();
    return qShot.docs.map((doc) => VCFHeaderModel.fromMap(doc.data())).toList();
  }

  Future<VCFHeaderModel> fetchVCFById(String vcfId) async {
    try {
      QuerySnapshot qShot =
          await collection.where('id', isEqualTo: vcfId).get();
      if (qShot.docs.isNotEmpty) {
        return VCFHeaderModel.fromMap(qShot.docs[0].data());
      } else {
        return null;
      }
    } on Exception {
      throw Exception('Error in fetching VCF by ID :$vcfId');
    }
  }
}
