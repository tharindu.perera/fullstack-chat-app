import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/yard_model.dart';

class YardDao extends BaseDao {
  YardDao() : super("yard");

  Future<List<YardModel>> fetchAllYards() async {
    QuerySnapshot qShot = await BaseQuery().get();
    return qShot.docs.map((doc) {
      return YardModel.fromMap(doc.data());
    }).toList();
  }
}
