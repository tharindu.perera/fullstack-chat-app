import 'package:mbase/base/core/common_base/model/offline.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';

abstract class OfflineDao {
  static Future<void> add(Offline offline) async {
    return DatabaseProvider.firestore.collection("offline_data").doc(offline.id).set(offline.toMap());
  }
}
