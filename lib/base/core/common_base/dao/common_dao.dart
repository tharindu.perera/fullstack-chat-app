import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/model/assigned_entry_field.dart';
import 'package:mbase/base/core/common_base/model/client_model.dart';
import 'package:mbase/base/core/common_base/model/customer_model.dart';
import 'package:mbase/base/core/common_base/model/equipment.dart';
import 'package:mbase/base/core/common_base/model/event_history.dart';
import 'package:mbase/base/core/common_base/model/location_model.dart';
import 'package:mbase/base/core/common_base/model/product_model.dart';
import 'package:mbase/base/core/common_base/model/product_restriction_model.dart';
import 'package:mbase/base/core/common_base/model/purchase_order_model.dart';
import 'package:mbase/base/core/common_base/model/shipment_type_model.dart';
import 'package:mbase/base/core/common_base/model/uom.dart';
import 'package:mbase/base/core/common_base/model/uom_conversion.dart';
import 'package:mbase/base/core/database_provider/database_provider.dart';
import 'package:mbase/base/modules/inspection/model/inspection_template_model.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/assigned_block.dart';
import 'package:mbase/base/modules/listview/actions/assign_block/model/railcar_block.dart';
import 'package:mbase/env.dart';

class CommonDao {
  FirebaseFirestore _firestore = DatabaseProvider.firestore;

  Future<List<PurchaseOrderModel>> fetchPurchaseOrderList(id) async {
    if (id == null) {
      return null;
    }
    CollectionReference collection = _firestore.collection("purchase_order");
    return (await collection
        .where("facility_id", isEqualTo: id)
        .orderBy("purchase_order_number")
        .get()).docs.map((e) => PurchaseOrderModel.fromMap(e.data())).toList();
  }

  Stream<List<InspectionTemplate>>   fetchInspectionTemplates(id)   {
   Query query=  _firestore.collection("inspection_template").where("facility_id", isEqualTo: id) ;
   return query.snapshots().map((snapshot) {
     return snapshot.docs.map((doc) {
       return InspectionTemplate.fromMap(doc.data());
     }).toList();
   });
  }

  Future<List<EventHistory>> fetchEventHistoryByEquipment(Equipment equipment) async {
    print("Fetching Event History By Equipment ......");
    CollectionReference collection = _firestore.collection("event_history");
    return (await collection.where("facility_visit_id", isEqualTo: equipment.facilityVisitId)
        .where("event_type_id", whereIn:   ["1", "3", "5", "6", "11", "13", "27", "28", "30", "32"])
        .orderBy("event_date",descending: true)
        .get()).docs.map((e) {
             var eventHistory = EventHistory.fromMap(e.data());
             return eventHistory;
        }).toList();
  }

  Future<List<PurchaseOrderModel>> fetchOpenPurchaseOrders(id) async {
    if (id == null) {
      return null;
    }
    CollectionReference collection = _firestore.collection("purchase_order");
    return (await collection.where("facility_id", isEqualTo: id)
        .where("order_status", isEqualTo: "Open")
        .orderBy("purchase_order_number")
        .get()).docs.map((e) => PurchaseOrderModel.fromMap(e.data())).toList();
  }

  Future<ProductModel> fetchProductById(String id) async {
    print("Fetching product ......");
    if (id == null) {
      return null;
    }
    CollectionReference collection = _firestore.collection("product");

    try {
      QuerySnapshot qShot = await collection
          .where("product_id", isEqualTo: id)
          .where("facility_id", isEqualTo: env.userProfile.facilityId)
          .get();
      if (qShot.docs.isNotEmpty) {
        return ProductModel.fromMap(qShot.docs[0].data());
      } else {
        return null;
      }
    } on Exception {
      throw Exception('Error in fetching Product by Id :$id');
    }


  }

  Future<String> fetchProductNameById(String id) async {
    if (id == null) {
      return null;
    }
    CollectionReference collection = _firestore.collection("product");
    var x = await collection
        .where("product_id", isEqualTo: id)
        .get();
    var productName = x.docs.map((e) => ProductModel.fromMap(e.data())).map((e) => e.productName).toString();
    return productName;
  }

  Future<bool> checkLoadingProductRestricted(String lastContainedProductId, String newProductId) async {
    print("start checkLoadingProductRestricted lastContainedProductId:$lastContainedProductId, newProductId:$newProductId");

    bool isRestricted = false;
    CollectionReference collection = _firestore.collection("product_restriction");
    var documents = await collection
        .where("last_contained_product_id", isEqualTo: lastContainedProductId)
        .where("restricted_product_id", isEqualTo: newProductId)
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .get();
    String allowDisallow = documents.docs.map((e) =>
        ProductRestrictionModel.fromMap(e.data())).map((e) => e.allow_disallow).toString();
    print("allowDisallow:$allowDisallow");
    if (allowDisallow == "(D)") {
      isRestricted = true;
    }
    print("isRestricted:$isRestricted");
    return isRestricted;
  }

  Future<List<Equipment>> fetchRailCarList(id) async {
    CollectionReference collection = _firestore.collection("equipment");
    return (await collection
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .where("id", isEqualTo: id)
        .get()).docs.map((e) => Equipment.fromMap(e.data())).toList();
  }

  Future<List<ProductModel>> fetchProductsList() async {
    CollectionReference collection = _firestore.collection("product");
    return (await collection
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .get()).docs.map((e) => ProductModel.fromMap(e.data())).toList();
  }

  Future<List<ClientModel>> fetchReasonCodeList() async {
    CollectionReference collection = _firestore.collection("client");
    return (await collection
        .where("id", isEqualTo:env.userProfile.clientId)
        .get()).docs.map((e) => ClientModel.fromMap(e.data())).toList();
  }



  Future<List<ProductModel>> getProductsByCriteria(Map<String, dynamic> criteria) async {
    Query collection = _firestore.collection("product");
    collection.where("facility_id", isEqualTo: env.userProfile.facilityId);
    criteria?.forEach((key, value) {
      if (value is List) {
        collection = collection.where(key, whereIn: value);
      } else {
        collection = collection.where(key, isEqualTo: value);
      }
    });

    return (await collection
        .get()).docs.map((e) => ProductModel.fromMap(e.data())).toList();
  }

  Future<List<ShipmentTypeModel>> fetchShipmentTypeList() async {
    CollectionReference collection = _firestore.collection("shipment_type");
    return (await collection.get()).docs.map((e) => ShipmentTypeModel.fromMap(e.data())).toList();
  }

  Future<List<LocationModel>> fetchLocationList() async {
    CollectionReference collection = _firestore.collection("location");
    return (await collection
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .orderBy("name")
        .get()).docs.map((e) => LocationModel.fromMap(e.data())).toList();
  }

  Future<UOM> fetchUOM(id) async {
    if (id == null) {
      return null;
    }
    CollectionReference collection = _firestore.collection("uom");
    return id != null ? (await collection
        .where("id", isEqualTo: id)
        .get()).docs.map((e) => UOM.fromMap(e.data())).toList()?.first : null;
  }

  Future<UOM> fetchUOMByUnit(unit) async {
    if (unit == null) {
      return null;
    }
    CollectionReference collection = _firestore.collection("uom");
    return unit != null ? (await collection
        .where("unit", isEqualTo: unit)
        .get()).docs.map((e) => UOM.fromMap(e.data())).toList()?.first : null;
  }

  Future<List<UOM>> fetchUOMList() async {
    CollectionReference collection = _firestore.collection("uom");
    return (await collection
        .orderBy("unit")
        .get()).docs.map((e) => UOM.fromMap(e.data())).toList();
  }

  Future<List<UOMConversion>> fetchUOMConversionList() async {
    CollectionReference collection = _firestore.collection("uom_conversion");
    return (await collection
        .where("client_id", whereIn: [env.userProfile.clientId])
        .orderBy("uom_from_id")
        .get()).docs.map((e) => UOMConversion.fromMap(e.data())).toList();
  }

  Future<List<AssignedEntryField>> fetchAdditionalFields(String facilityId, String type) async {
    CollectionReference collection = _firestore.collection("assigned_entry_field");
    return (await collection
        .where("facility_id", isEqualTo: facilityId)
        .where("entry_field_type", isEqualTo: type)
        .get()).docs.map((e) => AssignedEntryField.fromMap(e.data())).toList();
  }

//  Future<List<ProductRestrictionModel>> fetchProductLoadingRestrictionsForLoading(Equipment equipment, LoadRailCarDTO dto) async {
//    CollectionReference collection = _firestore.collection("product_restriction");
//    return (await collection
//            .where("facility_id", isEqualTo: env.userProfile.facilityId)
//            .where("last_contained_product_id", isEqualTo: equipment.last_contain_product_id)
//            .where("allow_disallow", isEqualTo: "D")
//            .where("restricted_product_id", isEqualTo: dto.selectedProduct.productId)
//            .get())
//        .docs
//        .map((e) => ProductRestrictionModel.fromMap(e.data()))
//        .toList();
//  }

  Future<List<CustomernModel>>fetchAllCustomers()async {
    CollectionReference collection = _firestore.collection("customer");
    return (await collection
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .get()).docs.map((e) => CustomernModel.fromMap(e.data())).toList();
     }



  Future<CustomernModel> fetchCustomer(String customer_id) {
    if (customer_id == null) {
      return null;
    }
    CollectionReference collection = _firestore.collection("customer");
    return collection
        .where("client_id", isEqualTo: env.userProfile.facilityId)
        .where("id", isEqualTo: customer_id)
        .get().then((value) {
      List list = value.docs.map((e) => CustomernModel.fromJson(e.data())).toList();
      return list.isEmpty ? null : list.first;
    });
  }

  // Returns Assigned Block To Data For The Railcar Assert Master Id
  Future <AssignedBlock> fetchAssignedBlocks({String facility_visit_id, String facilityId}) {
    CollectionReference collection = _firestore.collection("assigned_block");
    if (facility_visit_id == null) {
      return null;
    }
    return collection
        .where("facility_id", isEqualTo: facilityId)
        .where("scy_pk", isEqualTo: facility_visit_id)
        .get().then((value) {
      List list = value.docs.map((e) => AssignedBlock.fromMap(e.data())).toList();
      return list.isEmpty ? null : list.first;
    });

  }


  // Returns Railcar Block Data For The Railcar Block Id
  Future <RailcarBlock> fetchRailcarBlockData({String railcar_block_id ,String facilityId}){
    CollectionReference collection = _firestore.collection("railcar_block");

    if (railcar_block_id == null) {
      return null;
    }
    return collection
        .where("facility_id", isEqualTo: facilityId)
        .where("id", isEqualTo: railcar_block_id)
        .get().then((value) {
      List list = value.docs.map((e) => RailcarBlock.fromMap(e.data())).toList();
      return list.isEmpty ? null : list.first;
    });

  }

  Future <ProductModel> fetchProductDetailsByProductId({String product_id}){
    CollectionReference collection = _firestore.collection("product");
    if(product_id == null){
      return null;
    }
    return collection
        .where("facility_id", isEqualTo: env.userProfile.facilityId)
        .where("product_id", isEqualTo:product_id )
        .get().then((value) {
      List list = value.docs.map((e) => ProductModel.fromMap(e.data())).toList();
      print("list---> ${list}");
      return list.isEmpty ? null : list.first;
    });
  }

  // Future<AssetMasterDataModel> fetchUmlerDetails({String asset_master_id, String facility_id}){
  // CollectionReference collection = _firestore.collection("asset_master");
  // if (asset_master_id == null) {
  //   return null;
  // }
  //
  // return collection
  //     .where("facility_id", isEqualTo: facility_id)
  //     .where("id", isEqualTo: asset_master_id)
  //     .get().then((value) {
  //   List list = value.docs.map((e) => AssetMasterDataModel.fromMap(e.data())).toList();
  //   return list.isEmpty ? null : list.first;
  // });
  //
   }





