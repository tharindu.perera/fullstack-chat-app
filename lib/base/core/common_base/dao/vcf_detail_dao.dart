import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mbase/base/core/common_base/dao/base_dao.dart';
import 'package:mbase/base/core/common_base/model/vcf_header_model.dart';

class VCFDetailDao extends BaseDao {
  VCFDetailDao() : super('vcf_detail');

  Future<List<VCFDetailModel>> fetchAllByHeaderID(String headerId) async {
    QuerySnapshot qShot = await collection.where('vfc_header_id', isEqualTo: headerId).get();
    return qShot.docs.map((doc) => VCFDetailModel.fromMap(doc.data())).toList();
  }

  Future<List<VCFDetailModel>> fetchAllByTemperatureLimit(String temperature) async {
    double tempValue = double.parse(temperature.split(".")[0]);
    double from = tempValue - 2;
    double to = tempValue + 2;

    if (tempValue > 120) {
      from = 120;
    } else if (tempValue >= 98 && tempValue < 100) {
      from = 98;
      to = 99.9;
    } else if (tempValue >= 100 && tempValue <= 102) {
      from = 100;
      to = 102;
    }
    String fromQuery = from.toStringAsFixed(0);
    String toQuery = to.toStringAsFixed(3);

    QuerySnapshot qShot = await collection
        .where('temperature', isGreaterThanOrEqualTo: fromQuery)
        .where('temperature', isLessThan: toQuery +'z')
        .get();
    return qShot.docs.map((doc) => VCFDetailModel.fromMap(doc.data())).toList();
  }


}
