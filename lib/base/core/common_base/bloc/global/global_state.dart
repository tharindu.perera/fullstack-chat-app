import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GlobalState extends Equatable {
  final Bloc bloc;
  final Transition transition;

  const GlobalState({this.bloc, this.transition});

  @override
  List<Object> get props => [bloc, transition];

  @override
  String toString() {
    return 'GlobalState{core.base_classes.bloc: $bloc, transition: $transition}';
  }
}
