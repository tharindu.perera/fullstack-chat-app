import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GlobalEvent extends Equatable {
  final Bloc bloc;
  final Transition transition;

  GlobalEvent({this.bloc, this.transition});

  @override
  List<Object> get props => [bloc, transition];

  @override
  String toString() {
    return 'GlobalEvent{core.base_classes.bloc: $bloc, transition: $transition}';
  }
}
