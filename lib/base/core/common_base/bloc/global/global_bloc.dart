import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:mbase/base/core/common_base/bloc/global/global_event.dart';
import 'package:mbase/base/core/common_base/bloc/global/global_state.dart';

class GlobalBloc extends Bloc<GlobalEvent, GlobalState> {
  GlobalBloc(GlobalState initialState) : super(initialState);

  GlobalState get initialState => GlobalState();

  @override
  Stream<GlobalState> mapEventToState(GlobalEvent event) async* {
    yield GlobalState(transition: event.transition, bloc: event.bloc);
  }
}
