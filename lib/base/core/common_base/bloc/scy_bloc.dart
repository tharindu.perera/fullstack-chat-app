import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mbase/base/core/common_base/bloc/global/global_bloc.dart';
import 'package:mbase/base/core/common_base/bloc/global/global_state.dart';

abstract class SCYBloC<Event, State> extends Bloc<Event, State> {
  StreamSubscription navigationBlocSubscription;
  GlobalBloc _globalBloc;

  @mustCallSuper
  SCYBloC(State initialState) : super(initialState) {
//    _globalBloc =
//        BlocProvider.of<GlobalBloc>(keyGlobals.globalKey.currentContext);
//    navigationBlocSubscription = _globalBloc.listen((state) {
//      listeningToGlobal(state);
//    });
  }

  @override
  void onTransition(Transition<Event, State> transition) {
    super.onTransition(transition);
//    _globalBloc.add(GlobalEvent(bloc: this, transition: transition));
  }

  @override
  @mustCallSuper
  Future<void> close() {
//    navigationBlocSubscription.cancel();
    return super.close();
  }

  void listeningToGlobal(GlobalState state) {}

}
