import 'package:equatable/equatable.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

abstract class CustomError extends Equatable implements Exception {
  final String msg;

  @override
  List<Object> get props {
    return [msg];
  }

  CustomError(this.msg);
}

class ServerException extends CustomError {
  ServerException(String msg) : super(msg);
}

class CacheException extends CustomError {
  CacheException(String msg) : super(msg);
}

class DataException extends CustomError {
  DataException(String msg) : super(msg);
}

class UserException extends CustomError {
  UserException(String msg) : super(msg);
}
