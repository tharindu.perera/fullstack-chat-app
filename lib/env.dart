import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mbase/base/core/common_base/model/user_profile_model.dart';
import 'package:mbase/base/core/common_base/repositories/offline_data_repository.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/app_bar/app_bar_bloc.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/app_bar/app_bar_state.dart';
import 'package:mbase/base/core/keys/global-keys.dart';
import 'package:meta/meta.dart';

enum BuildFlavor { development, qa, production, version }

BuildEnvironment env;

class BuildEnvironment {
  bool isDataConnected = true;
  bool isDataConnectedForAppBar = true;
  final String version;
  final String baseUrl;
  final BuildFlavor flavor;
  ScaffoldFeatureController scaffoldFeatureController;
  UserProfile userProfile;
  FirebaseAuth firebaseAuth;
  AppBarBloc appbarbloc;
  DataConnectionChecker dataConnectionChecker;
  bool isDataSyncingToFireBase = false;

  BuildEnvironment._init({this.flavor, this.baseUrl, this.appbarbloc, this.version, this.dataConnectionChecker}) : assert(dataConnectionChecker != null);

  /// Sets up the top-level [env] getter on the first call only.
  static Future<void> init({@required flavor, @required baseUrl, @required version, @required dataConnectionChecker}) async {
    env = BuildEnvironment._init(flavor: flavor, baseUrl: baseUrl, appbarbloc: AppBarBloc(AppBarState(text: "")), version: version, dataConnectionChecker: dataConnectionChecker);
    env.dataConnectionChecker.checkInterval = Duration(seconds: 2);
    env.dataConnectionChecker.connectionStatus.then((value) => env.isDataConnectedForAppBar = value == DataConnectionStatus.connected ? true : false);
    print("Sleeping for 5 seconds for the dependecnies geting clear env data connection checker....");
    await Future.delayed(const Duration(seconds: 5), () => {});
    print("woke up afetr 5 seconds  ....");

    env.dataConnectionChecker.onStatusChange.listen((DataConnectionStatus status) async {
      env.isDataConnected = status == DataConnectionStatus.connected ? true : false;
      env.isDataConnectedForAppBar = status == DataConnectionStatus.connected ? true : false;
      print("##########  data connection chanegd: Is data connection available ?  ${env.isDataConnected ? 'yes' : 'No'}");
      if (env.isDataConnected && !env.isDataSyncingToFireBase) {
        env.isDataConnected = false;
        env.isDataSyncingToFireBase = true;
        OfflineDataRepository.saveToFireStore().then((value) async {
          print(">>> Time[ ${DateTime.now()} ] then in env  ");
          var conStatus = await env.dataConnectionChecker.connectionStatus;
          env.isDataConnected = conStatus == DataConnectionStatus.connected ? true : false;
          env.isDataSyncingToFireBase = false;
        }).catchError((err) async {
          print(">>> Time[ ${DateTime.now()} ] catch error  in env  : $err");
          var conStatus = await env.dataConnectionChecker.connectionStatus;
          env.isDataConnected = conStatus == DataConnectionStatus.connected ? true : false;
          env.isDataSyncingToFireBase = false;

          keyGlobals.loginPageScaffoldKey.currentState?.showSnackBar(SnackBar(
            content: Text(
              "Offline Data Syncing Has An Error",
              textAlign: TextAlign.center,
            ),
            duration: Duration(seconds: 5),
            backgroundColor: Colors.red,
          ));
        });
      }
      print("Done connection checker");
    });
  }
}
