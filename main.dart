import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mbase/base/app.dart';
import 'package:mbase/base/app/authentication/bloc/authentication_bloc.dart';
import 'package:mbase/base/app/authentication/bloc/authentication_state.dart';
import 'package:mbase/base/app/login/bloc/login_bloc.dart';
import 'package:mbase/base/app/login/bloc/login_states.dart';
import 'package:mbase/base/app/login/ui/LoginPage.dart';
import 'package:mbase/base/app/navigation/navigation_bloc.dart';
import 'package:mbase/base/app/navigation/navigation_state.dart';
import 'package:mbase/base/core/common_base/bloc/global/global_bloc.dart';
import 'package:mbase/base/core/common_base/bloc/global/global_state.dart';
import 'package:mbase/base/core/common_base/model/user_profile_model.dart';
import 'package:mbase/base/core/common_base/simple-bloc-delegate.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/app_bar/app_bar_bloc.dart';
import 'package:mbase/base/core/components/appbar/app_bar_component/app_bar/app_bar_state.dart';
import 'package:mbase/base/core/components/splash_component/splash_page.dart';
import 'package:mbase/base/core/config/global_theme/theme.dart';
import 'package:mbase/base/core/config/global_theme/theme_file.dart';
import 'package:mbase/base/core/keys/global-keys.dart';
import 'package:mbase/base/injection_container.dart' as di;
import 'package:provider/provider.dart';

ScaffoldFeatureController scaffoldFeatureController;
UserProfile userProfile;
FirebaseAuth firebaseAuth;
AppBarBloc appbarbloc= AppBarBloc(AppBarState(text: ""));

void main() async {
  Bloc.observer = SimpleBlocDelegate();
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseApp firebaseApp = await Firebase.initializeApp();
  firebaseAuth = FirebaseAuth.instanceFor(app: firebaseApp);
  await di.init();

  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<GlobalBloc>(create: (context) => GlobalBloc(GlobalState())),
      BlocProvider<AuthenticationBloc>(create: (context) {
        return di.sl();
      }),
    ],
    child: SCYApp(),
  ));
}

class SCYApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ThemeChanger>(
      builder: (context, child) {
        return MaterialApp(
          theme: Provider.of<ThemeChanger>(context).getTheme(),
          home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
              // ignore: missing_return
              builder: (context, state) {
            print("Main app status $state");
            if (state is AuthenticationLoading) {
              return SplashPage();
            } else if (state is AuthenticationAuthenticated) {
              return MultiBlocProvider(
                providers: [
                  BlocProvider<AppBarBloc>(create: (context) => appbarbloc),
                  BlocProvider<NavigationBloc>(create: (context) => NavigationBloc(HomeState())),
                ],
                child: App(),
              );
            } else if (state is AuthenticationUnauthenticated) {
              return BlocProvider<LoginBloc>(create: (context) => LoginBloc(LoginInitial()), child: LoginPage());
            }
          }),
          key: keyGlobals.globalKey,
          debugShowCheckedModeBanner: false,
        );
      },
      create: (_) => ThemeChanger(GlobalTheme().darkTheme),
    );
  }
}
