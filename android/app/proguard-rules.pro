-ignorewarnings
-keep class * {
    public private *;
}
-dontwarn org.slf4j.**

-dontwarn javax.xml.**

-dontwarn org.apache.**