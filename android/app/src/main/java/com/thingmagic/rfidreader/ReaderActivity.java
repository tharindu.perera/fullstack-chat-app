package com.thingmagic.rfidreader;

import com.thingmagic.Reader;
import com.thingmagic.util.LoggerUtil;
import android.os.AsyncTask;
import com.thingmagic.*;
import com.thingmagic.ReaderConnect;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import com.thingmagic.rfidreader.TagRecord;
import com.thingmagic.rfidreader.services.UsbService;
import com.wabtec.flutter_scy.MainActivity;

public class ReaderActivity {

	private static final String TAG = "ReaderActivity";
	private static String readerName = null;
	public static Reader reader=null;
	private static ReadThread readThread;
	private static long queryStartTime = 0;
	private static long queryStopTime = 0;
	private static long readRatePerSec=0;
	private static int totalTagCount=0;
	private static String tagReadFromDevice = null;


	public static void handlePermissions(MainActivity mainActivity) {
		try {
			String query = "";
			UsbService usbService = new UsbService();
			query = usbService.getThingMagicDeviceName(mainActivity);
			LoggerUtil.debug(TAG, "Selected query for permission: " + query);
			if (query == null) {
				return;
			}

			usbService.setUsbPermission(query, mainActivity);

		} catch (Exception e) {
			LoggerUtil.error(TAG, "Exception", e);
		}
	}

	public static String getThingMagicTagData(MainActivity mainActivity) {
		String returnString = "No Tag";
		tagReadFromDevice = null;
		String query = "";
		try {
			UsbService usbService = new UsbService();
			query = usbService.getThingMagicDeviceName(mainActivity);
			LoggerUtil.debug(TAG, "Selected query 3: " + query);
			if (query == null) {
				return returnString;
			}
			readerName = query.split("\n")[0];

			usbService.setUsbManager(query, mainActivity);
			query = "tmr:///"+ query.split("/")[1];
			System.out.println("query :"+ query);

//			ReaderConnectionThread readerConnectionThread = new ReaderConnectionThread(
//					query, "Connect");
//			readerConnectionThread.execute().get();

			try {
				reader = ReaderConnect.connect(query);
			} catch (ReaderCommException ex) {
				//retry again for communication exception
				LoggerUtil.error(TAG, "ReaderCommException", ex);
				reader = ReaderConnect.connect(query);
			}
			LoggerUtil.debug(TAG, "Reader Connected");
			readFromTagReader();

			for (int i=0; i<10; i++) {
				if (tagReadFromDevice != null) {
					break;
				}
				Thread.sleep(1000);
			}

		} catch (Exception e) {
			LoggerUtil.error(TAG, "Exception", e);
		}
		return tagReadFromDevice;
	}

	public static String readFromTagReader() {
		String returnString = "No Tag";
		try{
			String readerModel=(String) reader.paramGet("/reader/version/model");
			if(readerModel.equalsIgnoreCase("M6e Micro")){
				SimpleReadPlan  simplePlan = new SimpleReadPlan(new int[] {1,2}, TagProtocol.ATA);
				reader.paramSet("/reader/read/plan", simplePlan);

				int powerMax = (int) reader.paramGet("/reader/radio/powerMax");
				reader.paramSet("/reader/radio/readPower", powerMax);
			}
			String operation = "";
			operation = "syncRead";
			readThread = new ReadThread(reader, operation );
			readThread.execute().get();
		}catch(Exception ex){
			LoggerUtil.error(TAG, "Exception", ex);
		}
		return returnString;
	}

	private static class ReaderConnectionThread extends
			AsyncTask<Void, Void, String> {
		private String uriString = "";
		private String operation;
		private boolean operationStatus = true;
		ReadListener readListener = new PrintListener();

		public ReaderConnectionThread(String requestedQuery, String operation) {
			this.uriString = requestedQuery;
			this.operation = operation;
		}

		@Override
		protected void onPreExecute() {
			LoggerUtil.debug(TAG, "** onPreExecute **");
		}

		@Override
		protected String doInBackground(Void... params) {
			String exception = "Exception :";
			try {
				if (operation.equalsIgnoreCase("Connect")) {
					reader = ReaderConnect.connect(uriString);
					LoggerUtil.debug(TAG, "Reader Connected");
				} else {
					reader.destroy();
					LoggerUtil.debug(TAG, "Reader Disconnected");
				}

			} catch (Exception ex) {
				operationStatus = false;
				if (ex.getMessage().contains("Connection is not created")
						|| ex.getMessage().startsWith("Failed to connect")) {
					exception += "Failed to connect to " + readerName;
				} else {
					exception += ex.getMessage();
				}
				LoggerUtil.error(TAG, "Exception while Connecting :", ex);
			}
			return exception;
		}

		@Override
		protected void onPostExecute(String exception) {
		}

		class PrintListener implements ReadListener {
			public void tagRead(Reader param2Reader, TagReadData param2TagReadData) {
				LoggerUtil.debug(TAG, "** onPostExecute **" + param2TagReadData);
			}
		}
	}


	public static class ReadThread extends AsyncTask<Void, Integer, String> {

		private String operation;
		private Long duration;
		private static boolean exceptionOccur = false;
		private static String exception = "";
		private static boolean reading = true;
		private static Reader reader;
		static ReadExceptionListener exceptionListener = new TagReadExceptionReceiver();
		static ReadListener readListener = new PrintListener();


		public ReadThread(Reader reader,
						  String operation)
		{
			this.operation = operation;
			this.reader=reader;
		}

		@Override
		protected void onPreExecute()
		{

		}

		@Override
		protected String doInBackground(Void... params)
		{
			String tagRead = null;
			Long startTime = System.currentTimeMillis();
			try
			{
				if (operation.equalsIgnoreCase("syncRead")) {
					queryStartTime = System.currentTimeMillis();
					TagReadData[] tagReads = reader.read(1000);
					queryStopTime = System.currentTimeMillis();

					if (tagReads == null || tagReads.length == 0) {
						return tagRead;
					}
					LoggerUtil.debug(TAG, "TagReadData[] tagReads = mReader.read(timeOut) : " + tagReads.toString());
					tagRead = parseTag(tagReads[0],false);
				}
			}
			catch (Exception ex)
			{
				exception = ex.getMessage();
				exceptionOccur = true;
				LoggerUtil.error(TAG, "Exception while reading :", ex);
			} finally {
				Long endTime = System.currentTimeMillis();
				duration = (endTime - startTime) / 1000;
			}
			tagReadFromDevice = tagRead;
			return tagRead;
		}

		static class PrintListener implements ReadListener
		{
			public void tagRead(Reader r, final TagReadData tr)
			{
				readThread.parseTag(tr, true);
			}
		}

		static class TagReadExceptionReceiver implements ReadExceptionListener
		{
			public void tagReadException(Reader r, ReaderException re)
			{
				if(re.getMessage().equalsIgnoreCase("No connected antennas found")
						|| re.getMessage().contains("The module has detected high return loss")
						||	re.getMessage().contains("Tag ID buffer full")
						|| re.getMessage().contains("No tags found"))
				{
					//exception = "No connected antennas found";
					/* Continue reading */
				}else{
					exception=re.getMessage();
					exceptionOccur = true;
					readThread.setReading(false);
					readThread.publishProgress(-1);
				}
			}
		}

		private  void refreshReadRate(){
			new Thread(new Runnable() {

				@Override
				public void run() {
					while (isReading())
					{
						try {
							/* Refresh tagDetails for every 900 milli sec */
							Thread.sleep(900);
							publishProgress(0);
						} catch (InterruptedException e) {
							LoggerUtil.error(TAG,"Exception ", e);
						}
					}
				}
			}).start();
		}
		private static void calculateReadrate()
		{
			long elapsedTime = (System.currentTimeMillis() - queryStartTime) ;
			if(!isReading()){
				elapsedTime = queryStopTime- queryStartTime;
			}

			long tagReadTime = elapsedTime/ 1000;
			if(tagReadTime == 0)
			{
				readRatePerSec = (long) ((totalTagCount) / ((double) elapsedTime / 1000));
			}
			else
			{
				readRatePerSec = (long) ((totalTagCount) / (tagReadTime));
			}
		}


		private String parseTag(TagReadData tr, boolean publishResult)
		{
			String epcString = tr.getTag().epcString();
			LoggerUtil.debug(TAG, "Read tag =================: " +epcString);
			return epcString;
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {

		}

		public static boolean isReading() {
			return reading;
		}
		public void setReading(boolean reading) {
			ReadThread.reading = reading;
		}
	}
}
