import 'package:flutter_test/flutter_test.dart';
import 'package:mbase/base/core/common_base/model/vcf_header_model.dart';
import 'package:mbase/base/core/constants/app_constants.dart';
import 'package:mbase/base/modules/vcf_table/vcf_handler.dart';

void main() {
  VCFHandler vcfHandler = new VCFHandler();
  VCFHeaderModel vcfModel = new VCFHeaderModel();

  group('Table D1555', () {
    test('Benzene test 1', () {
      vcfModel.description = APP_CONST.Benzene_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '43'), 1.0111);
    });

    test('Benzene test 2', () {
      vcfModel.description = APP_CONST.Benzene_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '44'), 1.0104);
    });

    test('Cumene test', () {
      vcfModel.description = APP_CONST.Cumene_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '43'), 1.0092);
    });

    test('Cyclohexane test', () {
      vcfModel.description = APP_CONST.Cyclohexane_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '44'), 1.0106);
    });

    test('Ethylbenzene test', () {
      vcfModel.description = APP_CONST.Ethylbenzene_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '5'), 1.0306);
    });

    test('Styrene test', () {
      vcfModel.description = APP_CONST.Styrene_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '15'), 1.0242);
    });

    test('Toluene  test', () {
      vcfModel.description = APP_CONST.Toluene_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '-5'), 1.0383);
    });

    test('m-Xylene A test', () {
      vcfModel.description = APP_CONST.m_XyleneA_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '5'), 1.0293);
    });

    test('o-Xylene test', () {
      vcfModel.description = APP_CONST.o_Xylene_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '5'), 1.0288);
    });

    test('p-Xylene test', () {
      vcfModel.description = APP_CONST.p_Xylene_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '56'), 1.0022);
    });

    test('300 to 350 Aromatic Hydrocarbons test', () {
      vcfModel.description = APP_CONST.Hyd300350_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '5'), 1.0285);
    });

    test('350 to 400° Aromatic Hydrocarbons test', () {
      vcfModel.description = APP_CONST.Hyd350400_D1555;
      expect(vcfHandler.getVCF(vcfModel, '0', '5'), 1.0267);
    });
  });

  group('Table Sulfur', () {
    test('Sulfur test 1', () {
      vcfModel.description = APP_CONST.TABLE_SULFUR;
      expect(vcfHandler.getVCF(vcfModel, '0', '100'), 0.9671);
    });

    test('Sulfur test 2', () {
      vcfModel.description = APP_CONST.TABLE_SULFUR;
      expect(vcfHandler.getVCF(vcfModel, '0', '235'), 0.8922);
    });

    test('Sulfur test 3', () {
      vcfModel.description = APP_CONST.TABLE_SULFUR;
      expect(vcfHandler.getVCF(vcfModel, '0', '250'), 0.8858);
    });
  });

  group('Table D4311', () {
    test('D4311 test 1', () {
      vcfModel.description = APP_CONST.TABLE_D4311;
      expect(vcfHandler.getVCF(vcfModel, '0.968', '0'), 1.0211);
    });

    test('D4311 test 2', () {
      vcfModel.description = APP_CONST.TABLE_D4311;
      expect(vcfHandler.getVCF(vcfModel, '0.967', '0'), 1.0211);
    });

    test('D4311 test 3', () {
      vcfModel.description = APP_CONST.TABLE_D4311;
      expect(vcfHandler.getVCF(vcfModel, '0.966', '0'), 1.0241);
    });

    test('D4311 test 4', () {
      vcfModel.description = APP_CONST.TABLE_D4311;
      expect(vcfHandler.getVCF(vcfModel, '0.967', '10'), 1.0176);
    });

    test('D4311 test 10', () {
      vcfModel.description = APP_CONST.TABLE_D4311;
      expect(vcfHandler.getVCF(vcfModel, '0.966', '10'), 1.0201);
    });
  });

  group('Table 24D', () {
    test('24D test', () {
      vcfModel.description = APP_CONST.TABLE_24D;
      expect(vcfHandler.getVCF(vcfModel, '0.8017', '-57'), 1.0501);
    });

    test('24D test', () {
      vcfModel.description = APP_CONST.TABLE_24D;
      expect(vcfHandler.getVCF(vcfModel, '0.8017', '-55'), 1.0493);
    });
    test('24D test', () {
      vcfModel.description = APP_CONST.TABLE_24D;
      expect(vcfHandler.getVCF(vcfModel, '0.8017', '-49'), 1.0467);
    });
  });
}
