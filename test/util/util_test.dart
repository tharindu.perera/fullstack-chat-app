import 'package:flutter_test/flutter_test.dart';
import 'package:mbase/base/core/util/util_functions.dart';


void main() {

  group('Util Tests', () {
    test('Truncate Decimals', () {
      expect(Util.truncateToHundreths(1.3423, 2), 1.34);
    });

    test('Truncate Decimals', () {
      expect(Util.truncateToHundreths(1.3567423, 3), 1.356);
    });
  });
}